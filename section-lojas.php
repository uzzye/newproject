<?php

include_once("config.php");

?>
<section class="site-section border-bottom" id="lojas">
  <div class="anchor lojas" data-tpad="0"></div>
  <div class="container">
    <div class="row mb-5">
      <div class="col-md-5 text-left">
        <h2 class="section-title mb-3"><?php echo $arrayConteudos["lojas-titulo"]["descricao"]; ?></h2>
        <p class="lead"><?php echo $arrayConteudos["lojas-descricao"]["descricao"]; ?></p>
      </div>
    </div>
    <div class="row">
      
      <?php

      carrega_classe("lojas_virtuais");

      $itens = new lojas_virtuais();
      $itens = $itens->get_array_ativos("","ranking ASC");

      if(is_array($itens) && sizeof($itens) > 0) {
        foreach($itens as $item) {
          $it_id = intval($item->get_var("id"));
          //$it_tit = stripslashes(get_output($item->get_var("titulo_" . $_CONFIG["ref_lang"])));
          //if(trim($it_tit) == "") {$it_tit = stripslashes(get_output($item->get_var("titulo_pt")));}
          //$it_titurl = gera_titulo_amigavel(get_output($item->get_var("titulo_pt")));
          $it_tit = stripslashes(get_output($item->get_var("titulo")));
          $it_titurl = gera_titulo_amigavel(get_output($item->get_var("titulo")));
          $it_bg = stripslashes(get_output($item->get_var("logo")));

          $it_url = stripslashes(get_output($item->get_var("url")));
          
          $it_img = $it_bg;
          $it_folder = ROOT_SERVER . ROOT . $item->get_upload_folder("logo");
          
          ?>

          <div class="col-md-6 col-lg-3 mb-4">
            <div class="team-member">
              <figure>
                <ul class="social">
                  <?php if(trim($it_url) != "") { ?><li><a href="<?php echo $it_url; ?>" target="_blank"><span class="fa fa-link"></span></a></li><?php } ?>
                  <?php if(trim($it_url_fb) != "") { ?><li><a href="<?php echo $it_url_fb; ?>" target="_blank"><span class="icon-facebook"></span></a></li><?php } ?>
                  <?php if(trim($it_url_tw) != "") { ?><li><a href="<?php echo $it_url_tw; ?>" target="_blank"><span class="icon-twitter"></span></a></li><?php } ?>
                  <?php if(trim($it_url_in) != "") { ?><li><a href="<?php echo $it_url_in; ?>" target="_blank"><span class="icon-linkedin"></span></a></li><?php } ?>
                  <?php if(trim($it_url_ig) != "") { ?><li><a href="<?php echo $it_url_ig; ?>" target="_blank"><span class="icon-instagram"></span></a></li><?php } ?>
                </ul>
                <img src="<?php echo $it_folder . $it_img; ?>" alt="<?php echo $it_tit; ?>" class="img-fluid">
              </figure>
              <div class="p-3">
                <h3><?php echo $it_tit; ?></h3>
                <span class="position"><?php echo $it_sub; ?></span>
              </div>
            </div>
          </div>

          <?php
        }
      }

      ?>
      
    </div>
  </div>
</section>