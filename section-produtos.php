<?php

include_once("config.php");

?>
<section class="site-section" id="produtos">
  <div class="anchor sobre" data-tpad="0"></div>
  <div class="container">
    <div class="row mb-5">
      <div class="col-12 text-center">
        <h3 class="section-sub-title"><?php echo $arrayConteudos["produtos-titulo"]["descricao"]; ?></h3>
        <h2 class="section-title mb-3"><?php echo $arrayConteudos["produtos-subtitulo"]["descricao"]; ?></h2>
        <p><?php echo $arrayConteudos["produtos-descricao"]["descricao"]; ?></p>
      </div>
    </div>

    <?php

    carrega_classe("produtos");

    $produtos = new produtos();
    $produtos = $produtos->get_array_ativos("destaque = 1","ranking DESC",1);

    if(is_array($produtos) && sizeof($produtos) > 0) {
      $produto = $produtos[0];

      //$it_tit = stripslashes(get_output($produto->get_var("titulo_" . $_CONFIG["ref_lang"])));
      //if(trim($it_tit) == "") {$it_tit = stripslashes(get_output($produto->get_var("titulo_pt")));}
      //$it_titurl = gera_titulo_amigavel(get_output($produto->get_var("titulo_pt")));
      $it_tit = stripslashes(get_output($produto->get_var("titulo")));
      $it_titurl = gera_titulo_amigavel(get_output($produto->get_var("titulo")));
      $it_id = intval($produto->get_var("id"));
      $it_descr = stripslashes(get_output($produto->get_var("chamada_" . $_CONFIG["ref_lang"])));
      if(trim($it_descr) == "") {$it_descr = stripslashes(get_output($produto->get_var("chamada_pt")));}
      $it_bg = stripslashes(get_output($produto->get_var("imagem")));
      
      $it_img = $it_bg;
      $it_folder = ROOT_SERVER . ROOT . $produto->get_upload_folder("imagem");
      
      ?>
      <div class="cta-big-image">
        <div class="container">
          
          <img data-src="<?php echo $it_folder . $it_img; ?>" alt="<?php echo $it_tit; ?>" class="img-fluid lazy">
          <div class="img-box">
            <h2><?php echo $it_tit; ?></h2>
            <p><?php echo $it_descr; ?></p>
            <p><a href="<?php echo ROOT_SERVER . ROOT; ?>produtos/<?php echo $it_titurl; ?>/<?php echo $it_id; ?>" class="btn btn-outline-dark"><?php echo get_lang("_VER_MAIS"); ?></a></p>
          </div>
        </div>  
      </div>
      <?php

      $produtos = new produtos();
      $produtos = $produtos->get_array_ativos("id != " . intval($it_id),"ranking DESC",4);

      if(is_array($produtos) && sizeof($produtos) > 0) {

      ?>
      <div class="row produto-top">
        <?php

        foreach($produtos as $produto) {
          $it_id = intval($produto->get_var("id"));
          //$it_tit = stripslashes(get_output($produto->get_var("titulo_" . $_CONFIG["ref_lang"])));
          //if(trim($it_tit) == "") {$it_tit = stripslashes(get_output($produto->get_var("titulo_pt")));}
          //$it_titurl = gera_titulo_amigavel(get_output($produto->get_var("titulo_pt")));
          $it_tit = stripslashes(get_output($produto->get_var("titulo")));
          $it_titurl = gera_titulo_amigavel(get_output($produto->get_var("titulo")));
          $it_descr = stripslashes(get_output($produto->get_var("chamada_" . $_CONFIG["ref_lang"])));
          if(trim($it_descr) == "") {$it_descr = stripslashes(get_output($produto->get_var("chamada_pt")));}
          $it_bg = stripslashes(get_output($produto->get_var("imagem")));
          
          $it_img = $it_bg;
          $it_folder = ROOT_SERVER . ROOT . $produto->get_upload_folder("imagem");
          
          ?>
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
            <div class="h-entry">
              <a href="<?php echo ROOT_SERVER . ROOT; ?>produtos/<?php echo $it_titurl; ?>/<?php echo $it_id; ?>"><img data-src="<?php echo $it_folder . $it_img; ?>" alt="<?php echo $it_tit; ?>" class="img-fluid lazy"></a>
              <h2 class="font-size-regular"><a href="<?php echo ROOT_SERVER . ROOT; ?>produtos/<?php echo $it_titurl; ?>/<?php echo $it_id; ?>" class="text-black"><?php echo $it_tit; ?></a></h2>
              <p><?php echo $it_descr; ?></p>
              <p><a href="<?php echo ROOT_SERVER . ROOT; ?>produtos/<?php echo $it_titurl; ?>/<?php echo $it_id; ?>"><?php echo get_lang("_VER_MAIS_NOTICIAS"); ?></a></p>
            </div> 
          </div>
          <?php
        }

        ?>
        <div class="row align-items-center justify-content-center w-100"><a href="<?php echo ROOT_SERVER . ROOT; ?>produtos" class="btn btn-primary mr-2 mb-2"><?php echo get_lang("_VEJA_TODOS"); ?></a></div>      
      </div>
      <?php 
      }
    }
    ?>
  </div>
</section>