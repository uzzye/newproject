<?php 

$_CONFIG["noSQL"] = false;

include_once("config.php");

ob_clean();

?>
<!doctype html>
<html lang="<?php echo $_CONFIG["lang"]; ?>">
 	<head>
		<!-- MetaTags -->
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<?php /*<meta http-equiv="Expires" content="<?php echo gmdate ("D, d M Y H:i:s", time() + 48*60*60); ?>" />*/ ?>
		<meta http-equiv="Last-Modified" content="<?php echo date("Y-m-"); ?>01">
		<meta http-equiv="Cache-Control" content="max-age=2592000, public" />
		<meta http-equiv="Pragma" content="public">
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="apple-mobile-web-app-capable" content="yes" /> 
		<meta name="resource-types" content="document" /> 
		<meta name="classification" content="Internet" /> 
		<meta name="robots" content="ALL" /> 
		<meta name="distribution" content="Global" /> 
		<meta name="rating" content="General" /> 
		<meta name="author" content="Uzzye Com. Dig. Ltda." /> 
		<meta property="og:title" content="<?php
		if(isset($_TITULO_PAGINA) && trim($_TITULO_PAGINA) <> ""){
			echo $_TITULO_PAGINA . " - ";
		} 
		echo $_CONFIG["titulo"]; ?>" />
		<meta property="og:description" content="<?php
		if(isset($_DESCRICAO_PAGINA) && trim($_DESCRICAO_PAGINA) <> ""){
			echo $_DESCRICAO_PAGINA;
		} else {
			echo $_CONFIG["descricao"];
		} ?>" /> 
		<meta name="abstract" content="<?php
		if(isset($_DESCRICAO_PAGINA) && trim($_DESCRICAO_PAGINA) <> ""){
			echo $_DESCRICAO_PAGINA;
		} else {
			echo $_CONFIG["descricao"];
		} ?>" /> 
		<meta name="description" content="<?php
		if(isset($_DESCRICAO_PAGINA) && trim($_DESCRICAO_PAGINA) <> ""){
			echo $_DESCRICAO_PAGINA;
		} else {
			echo $_CONFIG["descricao"];
		} ?>" /> 
		<meta name="keywords" content="<?php
		if(isset($_PALAVRAS_CHAVE_PAGINA) && trim($_PALAVRAS_CHAVE_PAGINA) <> ""){
			echo $_PALAVRAS_CHAVE_PAGINA;
		} else {
			echo $_CONFIG["palavras-chave"];
		} ?>"/> 
		<!-- Geolocalização -->
		<link rel="alternate" hreflang="<?php echo $_CONFIG["ref_lang"]; ?>" href="<?php echo ROOT_SERVER . ROOT; ?>" />
		<?php if(trim($_CONFIG["geolocalizacao"]) <> ""){?>
		<?php echo $_CONFIG["geolocalizacao"]; ?>
		<?php } ?>
		<?php
		
		if(trim($_IMG_SHARE) == "") {			
			carrega_classe("banners");
			$banners = new banners();
			$banners = $banners->get_array_ativos("","ranking ASC, data_atualizacao DESC, data_criacao DESC");
			if(is_array($banners) && sizeof($banners) > 0) {
				$banner = $banners[0];
				$_IMG_SHARE = ROOT_SERVER . ROOT . $banner->get_upload_folder("imagem_bg") . stripslashes(get_output($banner->get_var("imagem_bg")));
			}
		}
		
		if(trim($_IMG_SHARE) != "") {
		?>
			<meta property="og:image" content="<?php echo $_IMG_SHARE; ?>" />
			<link rel="image_src" href="<?php echo $_IMG_SHARE; ?>" /><?php
		} else {
			if(file_exists("img/imgShare.png")){
				?>
				<link rel="image_src" href="<?php echo ROOT_SERVER . ROOT; ?>img/imgShare.png" sizes="1200x630" />
				<?php
				}
				else{?>
				<link rel="image_src" href="<?php echo ROOT_SERVER . ROOT . ROOT_CMS; ?>_estilo/images/logo_cms.png" sizes="177x43" />
			<?php } ?>
			<?php 
			/*

			// Load a custom share from some CMS module
			<link rel="image_src" href="<?php echo ROOT_SERVER . ROOT . ROOT_CMS; ?>_estilo/images/logo_cms.png" />
			<meta property="og:image" content="<?php echo ROOT_SERVER . ROOT . ROOT_CMS; ?>_estilo/images/logo_cms.png" />
			<meta property="og:image:type" content="image/png">
			<meta property="og:image:width" content="1200">
			<meta property="og:image:height" content="630">
			<?php
			*/
		}
		?>
		<meta name="theme-color" content="<?php echo $projectColors["theme"]; ?>">
    	<!-- Favicons -->
		<link rel="apple-touch-icon" href="<?php echo ROOT_SERVER . ROOT; ?>img/touchIcon.png" sizes="120x120" />
		<link rel="shortcut icon" href="<?php echo ROOT_SERVER . ROOT; ?>favicon.ico?<?php echo Date("YmdHis"); ?>" type="image/x-icon" />
		<link rel="icon" href="<?php echo ROOT_SERVER . ROOT; ?>favicon.png?<?php echo Date("YmdHis"); ?>" sizes="16x16" type="image/png" />
		<?php /*<link rel="canonical" href="<?php echo ROOT_SERVER . ROOT; ?>">*/ ?>

		<title><?php if(isset($_TITULO_PAGINA) && trim($_TITULO_PAGINA) <> ""){
			echo strip_tags($_TITULO_PAGINA) . " - ";
		} 
		echo strip_tags($_CONFIG["titulo"]); ?></title>
		
		<!-- Bootstrap core CSS -->
		<link href="<?php echo ROOT_SERVER . ROOT; ?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo ROOT_SERVER . ROOT; ?>vendor/datepicker/bootstrap-datepicker3.standalone.css" rel="stylesheet">		

		<!-- Custom CSS -->
		<?php if(intval($_REQUEST["replica"]) == 1) { ?>
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_SERVER . ROOT; ?>css/replica.css?<?php echo Date("Y-m-d"); ?>" />
		<?php } else { ?>
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_SERVER . ROOT; ?>css/main.css?<?php echo Date("Y-m-d"); ?>" />
		<?php } ?>

		<!-- JavaScript -->
		<script language="javascript">	
			let ROOT_CMS = "<?php echo ROOT_CMS; ?>";
			let ROOT_SITE = "<?php echo ROOT_SITE; ?>";
			let ROOT_SERVER = "<?php echo ROOT_SERVER; ?>";
			let ROOT = "<?php echo ROOT; ?>";	
			let UPLOAD_FOLDER = "<?php echo UPLOAD_FOLDER; ?>";
			let LANG = "<?php echo $_CONFIG["lang"]; ?>";
		</script>
		<?php if(file_exists(ROOT_SITE . "lang/" . $_CONFIG["lang"] . ".js.php")) {
			include_once(ROOT_SITE . "lang/" . $_CONFIG["lang"] . ".js.php");
		} ?>
		<?php

		$arrayConteudos = getArrayConteudos($sqlWhereIdiomas);
		$arrayLinks = getArrayLinks($sqlWhereIdiomas);
		$arrayScripts = array();

		?>
	</head>
	<?php

	ob_flush();
	flush();
	ob_implicit_flush(1);

	$_CONFIG["noSQL"] = false;

	?>
	<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">

		<div id="fb-root"></div>
		<script async defer crossorigin="anonymous" src="https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v3.3&appId=389935955206446&autoLogAppEvents=1"></script>

		<div class="site-wrap">

			<div class="site-mobile-menu site-navbar-target">
				<div class="site-mobile-menu-header">
					<div class="site-mobile-menu-close mt-3">
					<span class="icon-close2 js-menu-toggle"></span>
					</div>
				</div>
				<div class="site-mobile-menu-body"></div>
			</div>

			<header class="site-navbar js-sticky-header site-navbar-target" role="banner">

				<div class="container">
					<div class="row align-items-center">
					
					<div class="col-6 col-xl-2">
						<h1 class="mb-0 site-logo"><a href="<?php echo ROOT_SERVER . ROOT; ?>" class="h2 mb-0"><div class="logo"></div></a></h1>
					</div>

					<div class="col-12 col-md-10 d-none d-xl-block">
						<nav class="site-navigation position-relative text-right" role="navigation">

						<ul class="site-menu main-menu js-clone-nav mr-auto d-none d-lg-block">
							<li><a href="<?php echo ROOT_SERVER . ROOT; ?>sobre/" class="nav-link"><?php echo get_lang("_SOBRE"); ?></a></li><div class="d-none d-xl-inline"> . </div>
							<li><a href="<?php echo ROOT_SERVER . ROOT; ?>produtos/" class="nav-link"><?php echo get_lang("_PRODUTOS"); ?></a></li><div class="d-none d-xl-inline"> . </div>
							<li><a href="<?php echo ROOT_SERVER . ROOT; ?>contato/" class="nav-link"><?php echo get_lang("_CONTATO"); ?></a></li>
							<?php if(trim($arrayLinks["loja"]["url"]) != "") { ?>
							<div class="d-none d-xl-inline">.  </div><li><a href="<?php echo $arrayLinks["loja"]["url"]; ?>" target="<?php echo $arrayLinks["loja"]["target"]; ?>" title="<?php echo $arrayLinks["loja"]["titulo"]; ?>" class="nav-link"><?php echo $arrayLinks["loja"]["titulo"]; ?></a></li>
							<?php } ?>
							<?php if(trim($arrayLinks["facebook"]["url"]) != "") { ?>
							<li class="social-item"><a href="<?php echo $arrayLinks["facebook"]["url"]; ?>" target="<?php echo $arrayLinks["facebook"]["target"]; ?>" class="social-icon"><span class="icon-facebook"></span></a></li>
							<?php } ?>
							<?php if(trim($arrayLinks["instagram"]["url"]) != "") { ?>
							<li class="social-item"><a href="<?php echo $arrayLinks["instagram"]["url"]; ?>" target="<?php echo $arrayLinks["instagram"]["target"]; ?>" class="social-icon"><span class="icon-instagram"></span></a></li>
							<?php } ?>
							<?php if(trim($arrayLinks["youtube"]["url"]) != "") { ?>
							<li class="social-item"><a href="<?php echo $arrayLinks["youtube"]["url"]; ?>" target="<?php echo $arrayLinks["youtube"]["target"]; ?>" class="social-icon"><span class="icon-youtube"></span></a></li>
							<?php } ?>
							<?php /*if(trim($_CONFIG["ref_lang"]) != "en") { ?>
							<li class="lang-item"><a href="<?php echo ROOT_SERVER . ROOT; ?>lang/en" class="lang-icon" title="English"><span class="icon-en"></span></a></li>
							<?php } ?>
							<?php if(trim($_CONFIG["ref_lang"]) != "es") { ?>
							<li class="lang-item"><a href="<?php echo ROOT_SERVER . ROOT; ?>lang/es" class="lang-icon" title="Español"><span class="icon-es"></span></a></li>
							<?php } ?>
							<?php if(trim($_CONFIG["ref_lang"]) != "pt") { ?>
							<li class="lang-item"><a href="<?php echo ROOT_SERVER . ROOT; ?>lang/pt-br" class="lang-icon" title="Português"><span class="icon-pt"></span></a></li>
							<?php }*/ ?>
						</ul>
						</nav>
					</div>


					<div class="col-6 d-inline-block d-xl-none ml-md-0 py-3" style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle float-right"><span class="icon-menu h3"></span></a></div>

					</div>
				</div>
			
			</header>

			<main>
				<div class="detail"></div>