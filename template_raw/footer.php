		</main><!-- /.container -->
		<footer class="lazy">
      		<div class="anchor contato" data-tpad="-100"></div>
			
			<div class="container">

				<div class="row">
					<div class="col-12 col-lg-6">
						<a href="<?php echo ROOT_SERVER . ROOT; ?>">
							<img class="lazy" data-src="<?php echo ROOT_SERVER . ROOT; ?>img/logo.png" border="0" />
						</a>
					</div>
					<div class="col-12 col-lg-3 ml-auto mt-4 mt-lg-0">
						<ul class="list-unstyled">
							<li><a href="<?php echo ROOT_SERVER . ROOT; ?>sobre/"><?php echo get_lang("_SOBRE"); ?></a></li>
							<li><a href="<?php echo ROOT_SERVER . ROOT; ?>produtos/"><?php echo get_lang("_PRODUTOS"); ?></a></li>
							<li><a href="<?php echo ROOT_SERVER . ROOT; ?>contato/"><?php echo get_lang("_CONTATO"); ?></a></li>
							<?php if(trim($arrayLinks["loja"]["url"]) != "") { ?>
							<li><a href="<?php echo $arrayLinks["loja"]["url"]; ?>" target="<?php echo $arrayLinks["loja"]["target"]; ?>" title="<?php echo $arrayLinks["loja"]["titulo"]; ?>"><?php echo $arrayLinks["loja"]["titulo"]; ?></a></li>
							<?php } ?>
						</ul>
					</div>
					<div class="col-12 col-lg-3 mt-4 mt-lg-0">
						<ul>
							<?php if(trim($arrayLinks["facebook"]["url"]) != "") { ?>
							<li class="social-item"><a href="<?php echo $araryLinks["facebook"]["url"]; ?>" target="<?php echo $araryLinks["facebook"]["url"]; ?>" class="social-icon"><span class="icon-facebook"></span></a></li>
							<?php } ?>
							<?php if(trim($arrayLinks["instagram"]["url"]) != "") { ?>
							<li class="social-item"><a href="<?php echo $araryLinks["instagram"]["url"]; ?>" target="<?php echo $araryLinks["instagram"]["url"]; ?>" class="social-icon"><span class="icon-instagram"></span></a></li>
							<?php } ?>
							<?php if(trim($arrayLinks["youtube"]["url"]) != "") { ?>
							<li class="social-item"><a href="<?php echo $araryLinks["youtube"]["url"]; ?>" target="<?php echo $araryLinks["youtube"]["url"]; ?>" class="social-icon"><span class="icon-youtube"></span></a></li>
							<?php } ?>
						</ul>
						<?php /*<ul class="mt-4 mt-lg-2">
							<?php if(trim($_CONFIG["ref_lang"]) != "en") { ?>
							<li class="lang-item"><a href="<?php echo ROOT_SERVER . ROOT; ?>lang/en" class="lang-icon" title="English"><span class="icon-en"></span></a></li>
							<?php } ?>
							<?php if(trim($_CONFIG["ref_lang"]) != "es") { ?>
							<li class="lang-item"><a href="<?php echo ROOT_SERVER . ROOT; ?>lang/es" class="lang-icon" title="Español"><span class="icon-es"></span></a></li>
							<?php } ?>
							<?php if(trim($_CONFIG["ref_lang"]) != "pt") { ?>
							<li class="lang-item"><a href="<?php echo ROOT_SERVER . ROOT; ?>lang/pt-br" class="lang-icon" title="Português"><span class="icon-pt"></span></a></li>
							<?php } ?>
						</ul>*/ ?>
					</div>
				</div>
			</div>
      <!-- /.container -->
		</footer>
				
		<div class="copyright">
			<div class="container">
				<div class="row py-2">
					<div class="col-md-12">
						<p><?php echo $arrayConteudos["copyright"]["descricao"]; ?></p>
						<div class="sign">
							<div class="uzzye"><a href="https://www.uzzye.com" target="_blank" class="linkFull" title="Uzzye Com. Dig. Ltda."></a></div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php include_once("_lgpd_confirm.php"); ?>

		<!-- Fonts -->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/v4-shims.css">
		<?php //<link href="https://fonts.googleapis.com/css2?family=Oswald:wght@200;400&display=swap" rel="stylesheet"> ?>
		<link rel="stylesheet" href="<?php echo ROOT_SERVER . ROOT; ?>fonts/icomoon/style.css">
		<link rel="stylesheet" href="<?php echo ROOT_SERVER . ROOT; ?>fonts/flaticon/font/flaticon.css">

		<!-- Custom CSS -->
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_SERVER . ROOT; ?>css/jquery.fancybox.min.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_SERVER . ROOT; ?>css/swiper.min.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_SERVER . ROOT; ?>css/dropify.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_SERVER . ROOT; ?>css/helpers.css" />

		<?php if(trim($_CONFIG["codigo_css"]) <> ""){?>
		<style type="text/css">
		<?php echo $_CONFIG["codigo_css"]; ?>
		</style>
		<?php } ?>
		<?php if(trim($_CONFIG["codigo_analytics"]) <> ""){
			echo $_CONFIG["codigo_analytics"];
		} ?>

		<?php if(trim($_CONFIG["codigo_javascript"]) <> ""){?>
		<script type="text/javascript" language="javascript">
		<?php echo $_CONFIG["codigo_javascript"]; ?>
		</script>
		<?php } ?>

		<script type="text/javascript" src="<?php echo ROOT_SERVER . ROOT; ?>js/frameworks/lazysizes.min.js"></script>

		<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

		<?php /*

		// Uncomment to activate Uz JS Framework

		<!-- CSS -->
		<!--[if IE 7]>
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_SERVER . ROOT; ?>css/iehacks/ie7hacks.css" />
		<![endif]-->
		<!--[if IE]>
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_SERVER . ROOT; ?>css/iehacks/iehacks.css" />
		<style type="text/css">
			body { behavior:url(<?php echo ROOT_SERVER . ROOT; ?>css/iehacks/csshover3.htc); }
		</style>
		<![endif]-->
		<!--[if lt IE 9]><script async src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
		*/ ?>

		<?php //<script src="https://maps.google.com/maps/api/js?sensor=true&key=AIzaSyDLgXx-Xv5YfCPYrCACeZQ8qD8kT-ozqGw"></script> ?>
		<?php /*<script type="text/javascript" src="<?php echo ROOT_SERVER . ROOT; ?>js/dist/defer.min.js"></script>*/ ?>

		<script type="text/javascript" src="<?php echo ROOT_SERVER . ROOT; ?>js/dist/uz.min.js"></script>
		<?php // Minify "main.js" for dist version and uncomment below - or - run "gulp" (dist version) to build it into uz.min.js ?>
		<?php /*<script type="text/javascript" src="<?php echo ROOT_SERVER . ROOT; ?>js/dist/main.min.js"></script>*/ ?>
		<script type="text/javascript" src="<?php echo ROOT_SERVER . ROOT; ?>js/dist/main.js"></script>
		
		<?php
		if(is_array($arrayScripts) && sizeof($arrayScripts) > 0) {
			for($w=0; $w<sizeof($arrayScripts); $w++) {
				?><script type="text/javascript" src="<?php echo $arrayScripts[$w]; ?>"></script><?php
			}
		}
		?>
		<script type="text/javascript">
			loaded();
		</script>

	</body>
</html>