// Script "gulp" para compactar arquivos. Antes de rodar o "gulp", rodar o "compass compile" (SASS) e o "gulp" dentro da pasta "cms" (CMS)
// Concatena o uz-init.min.js junto com o main.min.js
'use strict';

// Carrega dependências

 //gulpfile.js
var gulp = require('gulp');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
//var resolveDependencies = require('gulp-resolve-dependencies');
//var less = require('gulp-less');
var minifyCSS = require('gulp-csso');

// Chamada de compactação dos arquivos de Tween Lite
gulp.task('tweenlite', function(){
    return gulp.src(['js/frameworks/greensock/plugins/CSSPlugin.min.js', 'js/frameworks/greensock/easing/EasePack.min.js', 'js/frameworks/greensock/TweenLite.min.js', 'js/frameworks/greensock/TimelineLite.min.js', 'js/frameworks/greensock/plugins/ScrollToPlugin.min.js'])
      .pipe(concat('uz-tweenlite.js'))
      .pipe(gulp.dest('js/custom'))
      .pipe(rename('uz-tweenlite.min.js'))
      .pipe(uglify())
      .pipe(gulp.dest('js/custom'));
});

// Chamada de compactação dos arquivos de Tween
gulp.task('tween', gulp.series('tweenlite', function(){
    return gulp.src(['js/frameworks/greensock/TweenMax.min.js', 'js/frameworks/greensock/TimelineMax.min.js', 'js/frameworks/greensock/plugins/ScrollToPlugin.min.js', 'js/frameworks/ScrollMagic.min.js', 'js/frameworks/plugins/jquery.ScrollMagic.min.js', 'js/frameworks/plugins/animation.gsap.min.js', 'js/frameworks/plugins/animation.velocity.min.js'])
      .pipe(concat('uz-tween.js'))
      .pipe(gulp.dest('js/custom'))
      .pipe(rename('uz-tween.min.js'))
      .pipe(uglify())
      .pipe(gulp.dest('js/custom'));
}));

// Chamada de compactação dos arquivos de JS, versão Lite (sem video.min.js e outros arquivos configuráveis ao final da linha de return) e versão Normal. Ignora arquivos de "_off"
//script paths
var jsFiles = 'js/vendor/*.js',
    jsDest = 'js/dist';
gulp.task('lite', gulp.series( 'tween', function(){
    return gulp.src(['js/frameworks/jquery-1.9.1.min.js', 'js/frameworks/plugins/jquery-migrate-1.2.1.min.js', jsFiles, 'js/custom/uz-tweenlite.min.js', 'js/core/**/*.js', '!js/vendor/_off/*.js', '!js/vendor/video.min.js', 'cms/_js/dist/uz-init.min.js', 'js/dist/main.min.js'])
      .pipe(concat('uz-lite.js'))
      .pipe(gulp.dest(jsDest))
      .pipe(rename('uz-lite.min.js'))
      .pipe(uglify())
      .pipe(gulp.dest(jsDest));
}));
jsFiles = 'js/vendor/**/*.js',
gulp.task('js', gulp.series( 'lite', function() {
    return gulp.src(['js/frameworks/jquery-1.9.1.min.js', 'js/frameworks/plugins/jquery-migrate-1.2.1.min.js', jsFiles, 'js/custom/uz-tween.min.js', 'js/core/**/*.js', '!js/vendor/_off/*.js', 'cms/_js/dist/uz-init.min.js', 'js/dist/main.min.js'])
      /*pipe(resolveDependencies({
        pattern: /\* @require [\s-]*(.*?\.js)/g,
          log: true
      }))*/
      .pipe(concat('uz.js'))
      .pipe(gulp.dest(jsDest))
      .pipe(rename('uz.min.js'))
      .pipe(uglify())
      .pipe(gulp.dest(jsDest));
}));

// Chamada de compactação dos arquivos de CSS. Primeiro compila os CSS exceto "_off" e "uz", depois concatena o "uz" com o "main", compilado pelo "compass compile" (SASS)
//css paths
var cssFiles = 'css/*.css',
    cssDest = 'css';
gulp.task('precss', function(){
    return gulp.src([cssFiles, '!css/uz.css', '!css/uz.min.css', '!css/_off/*.css', '!css/main.css'])
      .pipe(concat('uz.css'))
      .pipe(gulp.dest(cssDest))
});
gulp.task('css', gulp.series( 'precss', function(){
    return gulp.src(['css/uz.css', 'css/main.css'])
      .pipe(concat('uz.css'))
      .pipe(gulp.dest(cssDest))
      .pipe(rename('uz.min.css'))
      //.pipe(less())
      .pipe(minifyCSS())
      .pipe(gulp.dest(cssDest))
}));

/*gulp.task('default', function(){
  // Default task code
  console.log('GULP GULP GULP')
});*/

// Configura task padrão ao chamar "gulp"
gulp.task('default', gulp.series( 'js', 'css' ));