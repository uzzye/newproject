<?php

function link_voltar()
{
	$str = "javascript:history.go(-1);";
	return $str;
}

function show_loading($id_aux = "",$display = "")
{
	?><div class='load' id="loading_image_<?php echo $id_aux;?>" style="display:<?echo $display;?>"><img src="<?php echo ROOT; ?>_estilo/images/geral/loaderGeral.gif" /></div><?php
}

/* converte_data($date_time,$formato = 0)
   
   $date_time no formato YYYY-MM-DD HH:MM:SS
   $formato = valor = modelo = exemplo
   $formato = [0] = DD mmm = 16 mai
   $formato = [1] = DD mmm AAAA - HH:MMh= 16 mai 1987 - 10:30h
   $formato = [2] = DD.MM.AAAA = 16.05.1987
   $formato = [3] = DD mmm AAAA = 16 Mai 1987
   $formato = [4] = DD.MM = 16.05
   $formato = [5] = SSSSS DD de MMMMMM = Sábado 16 de Maio
   $formato = [6] = MMMMMM de AAAA = maio de 2016
   $formato = [7] = AAAA/MM = 2016/05
   16.05.1987
*/
function converte_data($date_time,$formato = 0,$lang = "pt")
{
  $retorno = $date_time;
  switch($lang) {
  	default:
  	case "pt":
  		$locale = "pt_BR";
  		break;
  	case "us":
  		$locale = "en_US";
  		break;
  	case "es":
  		$locale = "es_ES";
  		break;
  }
  //$date_time = substr($date_time,0,10);	
  $timeStamp = strtotime($date_time);  
  switch($formato)
  {
	  default:
	  {
        //
		break;  
	  }
	  case 0:
	  {
	  	$oldlocale = setlocale(LC_ALL, NULL); 
		setlocale(LC_ALL, $locale);   
		$retorno = strtolower(strftime("%d %b", $timeStamp));
		setlocale(LC_ALL, $oldlocale);   
		break;
	  }
	  case 1:
	  {
	  	$oldlocale = setlocale(LC_ALL, NULL); 
		setlocale(LC_ALL, $locale);   
		$retorno = strtolower(strftime("%d %b %Y - %H:%Mh", $timeStamp));
		setlocale(LC_ALL, $oldlocale);
		break;
	  }
  	  case 2:
	  {
	  	$oldlocale = setlocale(LC_ALL, NULL); 
		setlocale(LC_ALL, $locale);   
		$retorno = strtolower(strftime("%d.%m.%Y", $timeStamp));
		setlocale(LC_ALL, $oldlocale);   
		break;
	  }
  	  case 3:
	  {
	  	$oldlocale = setlocale(LC_ALL, NULL); 
		setlocale(LC_ALL, $locale);   
		$retorno = strtolower(strftime("%d", $timeStamp)) . " ";
		$retorno .= ucfirst(strtolower(strftime("%b", $timeStamp))) . " ";
		$retorno .= strtolower(strftime("%Y", $timeStamp));
		setlocale(LC_ALL, $oldlocale);
		break;
	  }
  	  case 4:
	  {
	  	$oldlocale = setlocale(LC_ALL, NULL); 
		setlocale(LC_ALL, $locale);   
		$retorno = strtolower(strftime("%d.%m", $timeStamp));
		setlocale(LC_ALL, $oldlocale);   
		break;
	  }
  	  case 5:
	  {
	  	$oldlocale = setlocale(LC_ALL, NULL); 
		setlocale(LC_ALL, $locale);
		$retorno = str_replace("-feira","",ucfirst(strtolower(strftime("%A", $timeStamp)))) . " ";   
		$retorno .= strftime("%d", $timeStamp) . " de ";
		$retorno .= ucfirst(strtolower(strftime("%B", $timeStamp))) . " ";
		setlocale(LC_ALL, $oldlocale);
		break;
	  }
  	  case 6:
	  {
	  	$oldlocale = setlocale(LC_ALL, NULL); 
		setlocale(LC_ALL, $locale);
		$retorno = strtolower(strftime("%B", $timeStamp)) . " ";
		$retorno .= " de ";
		$retorno .= strtolower(strftime("%Y", $timeStamp));
		setlocale(LC_ALL, $oldlocale);
		break;
	  }
  	  case 7:
	  {
	  	$oldlocale = setlocale(LC_ALL, NULL); 
		setlocale(LC_ALL, $locale);
		$retorno = strtolower(strftime("%Y", $timeStamp)) . "/";
		$retorno .= strtolower(strftime("%m", $timeStamp));
		setlocale(LC_ALL, $oldlocale);
		break;
	  }
  }
  return $retorno;
}

/* converte_hora($hora,$formato = 0)
   
   $hora no formato HH:MM:SS
   $formato = valor = modelo = exemplo
   $formato = [0] = HHhMM = 20h30
*/
function converte_hora($hora,$formato = 0)
{
  $retorno = $hora;	
  $data_hora = date("Y-m-d") . " " . $hora;
  $timeStamp = strtotime($data_hora);
  switch($formato)
  {
	  default:
	  {
        //
		break;  
	  }
	  case 0:
	  {
		$oldlocale = setlocale(LC_ALL, NULL); 
		setlocale(LC_ALL, 'pt_BR');   		
		$retorno = @strtolower(strftime("%H", $timeStamp) . "h" . strftime("%M", $timeStamp));
		setlocale(LC_ALL, $oldlocale);
		break;
	  }
  }
  return $retorno;
}

/*function monta_jquery_paginacao()
{
	?>
	$(".paginacao").find(".btnPrimeiro").css("position","absolute").css("left","0");
	$(".paginacao").find(".btnUltimo").css("position","absolute").css("right","0");
	$(".paginacao").css("margin-left",(($(".paginacao").parent().width()/2)-($(".paginacao").width()/2)) + "px");
	
	$(".paginas").width(($(".paginas").find("ul").find("li").length*24) + "px");
	$(".paginas").css("position","absolute");
	$(".paginas").css("left","-20px");
	$(".paginas").css("margin-left",(($(".paginacao").width()/2)-($(".paginas").width()/4)) + "px");
	<?php
}

function monta_paginacao($i_qtd_pg,$url_paginacao,$ajax = false,$estilo = 1)
{			
	global $mysql, $_CONFIG;
	if(intval($estilo) == 0){$estilo = 1;}

	//?>
	//<link rel="stylesheet" type="text/css" href="<?php echo ROOT; ?>_estilo/paginacao<?php echo $estilo;?>.css" />
	//?>
	<script type="text/javascript">
		$(document).ready(function(){
			<?php monta_jquery_paginacao(); ?>

			$(".paginas ul li a, .btnPrimeiro, .btnUltimo").click = null;
			$(".paginas ul li a, .btnPrimeiro, .btnUltimo").click(function(){
				showLoading();
				$.ajax({
					url: $(this).attr("href"),
					success: function(data) {
						$('#listagem').html(data);
						$('#listagem').show();
						hideLoading();

						atualizaAddThis();
					}
				});
				return false;
			});

			$(".paginacao").find(".btnPrimeiro").css("position","absolute").css("left","0");
			$(".paginacao").find(".btnUltimo").css("position","absolute").css("right","0");
			$(".paginacao").css("margin-left",(($(".paginacao").parent().width()/2)-($(".paginacao").width()/2)) + "px");
	
			$(".paginas").width(($(".paginas").find("ul").find("li").length*30) + "px");
			$(".paginas").css("position","absolute");
			$(".paginas").css("margin-left",(($(".paginacao").width()/2)-($(".paginas").width()/4)) + "px");
		});	
	</script>
	<div style="width:100%; float:left">
	<?php
  
  	if (isset($_GET["pagina"])){  	
  		$pagina = $_REQUEST["pagina"]-1;
  	}else{
		$pagina = 0;
  	}
  
  	$link = ""; 
   
  	if(!$ajax)
  	{
  		if(substr($link,-1) != "/") {
	 		$link .= "/";
	  	}
	  	$link .= "pg/";
  	}
	  
	if($i_qtd_pg>1 && $i_qtd_pg<=$_CONFIG["qtd_group_pag"])
	{ ?>
<!-- ****************************************** -->
		<div class="paginacao">
		
			<div><a href="<?php echo $url_paginacao . $link . (1); ?>" class="btnPrimeiro"></a></div>
			
			<div class="paginas">
				<ul>
				<?php 
				$w=0;
				while ($w<$i_qtd_pg)
				{	
					if($pagina==$w){?>
						<li class="ativo"><?php echo ($w+1) ?><?php 
						if($w<$i_qtd_pg-1){ echo " . ";}
						?></li>
					<?php }else{?>
						<li><a href="<?php echo $url_paginacao . $link . ($w+1); ?>"><?php echo ($w+1) ?></a><?php 
						if($w<$i_qtd_pg-1){ echo " . ";}
						?></li>
					<?php }
					$w++;
				}
				?>
				</ul>
			</div>
			
			<div><a href="<?php echo $url_paginacao . $link. ($i_qtd_pg) ?>" class="btnUltimo"></a></div>
		</div>
<!-- ****************************************** --> 
           
<?php
  }
  else if($i_qtd_pg>1)
  {
?>
<!-- ****************************************** -->
		<div class="paginacao">
			
			<div style="float:left"><a href="<?php echo $url_paginacao . $link . (1); ?>" class="btnPrimeiro"></a></div>
			
			<div class="paginas">
				<ul>
				<?php
				
				$str_pg_anterior = "";
				$str_pg_posterior = "";
				
				$i_qtd_aux = ($i_qtd_pg>3)?3:$i_qtd_pg;
				$array_pg = array();
												
				if($pagina < 2)
				{
					$w=0;
					while($w<$i_qtd_aux)
					{
						$array_pg[sizeof($array_pg)] = ($w);
						$w++;
					}
					
					if($i_qtd_pg>3)
					{
						$i_pg_aux = (($array_pg[sizeof($array_pg)-1] + 7)>($i_qtd_pg-1))?($i_qtd_pg-1):(($array_pg[sizeof($array_pg)-1] + 7));
						$str_pg_posterior = " <li>...</li><li><a href=\"" . $url_paginacao . $link . ($i_pg_aux+1) . "\">" . ($i_pg_aux+1) . "</a></li>";
					}		
				}				
				else if($pagina > $i_qtd_pg-$_CONFIG["qtd_group_pag"])
				{		
					$w=0;
					$iCount = 3;
					while($w<$i_qtd_aux)
					{
						$array_pg[sizeof($array_pg)] = ($i_qtd_pg-$iCount);
						$w++;
						$iCount--;
					}
					
					if($i_qtd_pg>3)
					{
						$i_pg_aux = (($array_pg[0] - 7)<0)?0:(($array_pg[0] - 7));
						$str_pg_anterior = "<li><a href=\"" . $url_paginacao . $link . ($i_pg_aux+1) . "\">" . ($i_pg_aux+1) . "</a></li><li>...</li>";
					}		
				}
				else
				{			
					$w=$i_qtd_aux;
					while($w>0)
					{
						$array_pg[sizeof($array_pg)] = ($pagina-($w-2));
						$w--;
					}
					
					$i_pg_aux = (($array_pg[0] - 7)<0)?0:(($array_pg[0] - 7));
					$str_pg_anterior = "<li><a href=\"" . $url_paginacao . $link . ($i_pg_aux+1) . "\">" . ($i_pg_aux+1) . "</a></li><li>...</li> ";
					
					$i_pg_aux = (($array_pg[sizeof($array_pg)-1] + 7)>($i_qtd_pg-1))?($i_qtd_pg-1):(($array_pg[sizeof($array_pg)-1] + 7));
					$str_pg_posterior = " <li>...</li><li><a href=\"" . $url_paginacao . $link . ($i_pg_aux+1) . "\">" . ($i_pg_aux+1) . "</a></li>";
				}
					
				echo $str_pg_anterior;
				for($w=0;$w<sizeof($array_pg);$w++)
				{
					$pg_aux = $array_pg[$w];
					if($pg_aux == $pagina)
					{
						?><li class="ativo"><?php echo ($pagina+1); ?><?php 
						if($w<sizeof($array_pg)-1){ echo " . ";}
						?></li><?php
					}
					else
					{
						if($w==sizeof($array_pg)-1)
						{
						?><li><a href="<?php echo $url_paginacao . $link . ($pg_aux+1) ?>"><?php echo ($pg_aux+1); ?></a></li><?php
						}
						else
						{
						?><li><a href="<?php echo $url_paginacao . $link . ($pg_aux+1) ?>"><?php echo ($pg_aux+1); ?></a> . </li><?php
						}
					}
				}
				echo $str_pg_posterior;
				?>
				</ul>
			</div>
			
			<div style="float:left">
			<a href="<?php echo $url_paginacao . $link. ($i_qtd_pg) ?>" class="btnUltimo"></a>
			</div>
		</div>

<!-- ****************************************** -->          
<?php
  }
  else
  {
  	?><div class="linkTopListagem"></div><?php
  } 
  ?>
  </div>
<?php }*/

/* CSS PAGINACAO */
//.paginacao {width:880px; height:26px; position:relative; float:left; /*margin:10px 0 10px 10px;*/ margin:0px; font-weight:normal; font-family:"SegoeUINormal"; font-size:16px; font-style:italic;}
//.paginacao .btnPrimeiro {background:url(images/pgPrev.png) bottom left no-repeat; height:22px; width:18px; display:block; float:left; margin:0px; margin-top:3px;}
//.paginacao .btnUltimo {background:url(images/pgNext.png) bottom left no-repeat; height:22px; width:18px; float:right; display:block; float:left; margin:0px; margin-top:3px;}
//.paginacao ul {margin:0; padding:0; width:100%; height:100%; float:left; position:relative;}
//.paginacao li {display: block; float:left; list-style:none; padding:0; margin:auto; *display: inline; width:28px; height:22px;}
//.paginacao li. a {display:block; width:100%; height:100%; font-weight:normal; font-family:"SegoeUINormal"; font-size:16px; font-style:italic;}
//.paginacao li.link{color:#2d1500;}
//.paginacao .paginas {width:870px; margin:2px 0 0 30px; float:left; text-align:center; color:#2d1500;}
//.paginacao .paginas li {display: block; float:left; list-style:none; padding:0; margin:auto; padding-left:0px; *display:inline; width:28px; height:22px;}
//.paginacao .paginas .ativo {height:22px; width:38px; color:#fff; background:url(images/bgPaginacao.png) no-repeat top left; padding-left:0px;}
//.paginacao .paginas a {text-decoration: none; color:#2d1500; font-weight:normal; font-family:"SegoeUINormal"; font-size:16px; font-style:italic;}
//.paginacao .paginas a:hover {color:#2d1500; font-weight:normal; font-family:"SegoeUINormal"; font-size:16px; font-style:italic;}

//.paginacao .btnPrimeiroC {/*background:#f47820 url(images/btn_prev2.png) bottom left no-repeat; height:52px; width:54px; display:block; float:left; margin:0px; margin-top:-15px;*/}
//.paginacao .btnUltimoC {/*background:#f47820 url(images/btn_next2.png) bottom left no-repeat; height:52px; width:54px; float:right; display:block; float:left; margin:0px; margin-top:-15px;*/}
/********************************/

function monta_jquery_paginacao()
{
	?>
	$(".paginacao").find(".btnPrimeiro").css("position","absolute").css("left","0");
	$(".paginacao").find(".btnUltimo").css("position","absolute").css("right","0");
	//$(".paginacao").css("margin-left",(($(".paginacao").parent().width()/2)-($(".paginacao").width()/2)) + "px");
	
	$(".paginas").width(($(".paginas").find("ul").find("li").length*28+10) + "px");
	$(".paginas").css("position","absolute");
	//$(".paginas").css("left","-20px");
	//$(".paginas").css("margin-left",(($(".paginacao").width()/2)-($(".paginas").width()/4)) + "px");
	$(".paginacao").width($(".paginas").width() + 60 + "px");
	<?php
}

function monta_paginacao($i_qtd_pg,$url_paginacao,$ajax = false,$deeplink = false,$estilo = 1, $divisoria = " . ")
{			
	global $mysql, $_CONFIG;
	if(intval($estilo) == 0){$estilo = 1;}

	/*?>
	<link rel="stylesheet" type="text/css" href="<?php echo ROOT; ?>_estilo/paginacao<?php echo $estilo;?>.css" />
	*/?>
	<script type="text/javascript">
		$(document).ready(function(){
			<?php monta_jquery_paginacao(); 
			
			if(!$deeplink){?>

			$(".paginas ul li a, .btnPrimeiro, .btnUltimo").click = null;
			$(".paginas ul li a, .btnPrimeiro, .btnUltimo").click(function(){
				showLoading();
				$.ajax({
					url: $(this).attr("href"),
					success: function(data) {
						$('#listagem').html(data);
						$('#listagem').show();
						hideLoading();

						//atualizaAddThis();
					}
				});
				return false;
			});
			
			<?php 
			}
			?>
		});	
	</script>
	<div style="width:100%; float:left">
	<?php
  
  	if (isset($_GET["pagina"])){  	
  		$pagina = $_REQUEST["pagina"]-1;
  	}else{
		$pagina = 0;
  	}
  
  	$link = ""; 
   
  	if(!$ajax)
  	{
  		if(substr($link,-1) != "/") {
	 		$link .= "/";
	  	}
	  	$link .= "pg/";
  	}
	  
	if($i_qtd_pg>1 && $i_qtd_pg<=$_CONFIG["qtd_group_pag"])
	{ ?>
<!-- ****************************************** -->
		<div class="paginacao">
		
			<div><a <?php if($deeplink){echo "rel=\"address\" ";}?>href="<?php echo $url_paginacao . $link . (1); ?>" class="btnPrimeiro"></a></div>
			
			<div class="paginas">
				<ul>
				<?php 
				$w=0;
				while ($w<$i_qtd_pg)
				{	
					if($pagina==$w){?>
						<li class="ativo"><?php echo ($w+1) ?><?php 
						if($w<$i_qtd_pg-1){ echo " ";}
						?></li>
					<?php }else{?>
						<li><a <?php if($deeplink){echo "rel=\"address\" ";}?>href="<?php echo $url_paginacao . $link . ($w+1); ?>"><?php echo ($w+1) ?></a><?php 
						if($w<$i_qtd_pg-1){ echo " ";}
						?></li>
					<?php }
					$w++;
				}
				?>
				</ul>
			</div>
			
			<div><a <?php if($deeplink){echo "rel=\"address\" ";}?>href="<?php echo $url_paginacao . $link. ($i_qtd_pg) ?>" class="btnUltimo"></a></div>
		</div>
<!-- ****************************************** --> 
           
<?php
  }
  else if($i_qtd_pg>1)
  {
?>
<!-- ****************************************** -->
		<div class="paginacao">
			
			<div style="float:left"><a <?php if($deeplink){echo "rel=\"address\" ";}?>href="<?php echo $url_paginacao . $link . (1); ?>" class="btnPrimeiro"></a></div>
			
			<div class="paginas">
				<ul>
				<?php
				
				$str_pg_anterior = "";
				$str_pg_posterior = "";
				
				$i_qtd_aux = ($i_qtd_pg>$_CONFIG["qtd_group_pag"])?$_CONFIG["qtd_group_pag"]:$i_qtd_pg;
				$array_pg = array();
												
				if($pagina < $_CONFIG["qtd_group_pag"]-1)
				{
					$w=0;
					while($w<$i_qtd_aux)
					{
						$array_pg[sizeof($array_pg)] = ($w);
						$w++;
					}
					
					if($i_qtd_pg>$_CONFIG["qtd_group_pag"])
					{
						$i_pg_aux = (($array_pg[sizeof($array_pg)-1] + ceil($_CONFIG["qtd_group_pag"]/2))>($i_qtd_pg-1))?($i_qtd_pg-1):(($array_pg[sizeof($array_pg)-1] + ceil($_CONFIG["qtd_group_pag"]/2)));
						$str_pg_posterior = " <li>...</li><li><a ". (($deeplink)?"rel=\"address\" ":"") . "href=\"" . $url_paginacao . $link . ($i_pg_aux+1) . "\">" . ($i_pg_aux+1) . "</a></li>";
					}		
				}				
				else if($pagina > $i_qtd_pg-$_CONFIG["qtd_group_pag"])
				{		
					$w=0;
					$iCount = $_CONFIG["qtd_group_pag"];
					while($w<$i_qtd_aux)
					{
						$array_pg[sizeof($array_pg)] = ($i_qtd_pg-$iCount);
						$w++;
						$iCount--;
					}
					
					if($i_qtd_pg>$_CONFIG["qtd_group_pag"])
					{
						$i_pg_aux = (($array_pg[0] - ceil($_CONFIG["qtd_group_pag"]/2))<0)?0:(($array_pg[0] - ceil($_CONFIG["qtd_group_pag"]/2)));
						$str_pg_anterior = "<li><a ". (($deeplink)?"rel=\"address\" ":"") . "href=\"" . $url_paginacao . $link . ($i_pg_aux+1) . "\">" . ($i_pg_aux+1) . "</a></li><li>...</li>";
					}		
				}
				else
				{			
					$w=$i_qtd_aux;
					while($w>0)
					{
						$array_pg[sizeof($array_pg)] = ($pagina-($w-ceil($_CONFIG["qtd_group_pag"]/2)));
						$w--;
					}
					
					$i_pg_aux = (($array_pg[0] - ceil($_CONFIG["qtd_group_pag"]/2))<0)?0:(($array_pg[0] - ceil($_CONFIG["qtd_group_pag"]/2)));
					$str_pg_anterior = "<li><a ". (($deeplink)?"rel=\"address\" ":"") . "href=\"" . $url_paginacao . $link . ($i_pg_aux+1) . "\">" . ($i_pg_aux+1) . "</a></li><li>...</li> ";
					
					$i_pg_aux = (($array_pg[sizeof($array_pg)-1] + ceil($_CONFIG["qtd_group_pag"]/2))>($i_qtd_pg-1))?($i_qtd_pg-1):(($array_pg[sizeof($array_pg)-1] + ceil($_CONFIG["qtd_group_pag"]/2)));
					$str_pg_posterior = " <li>...</li><li><a ". (($deeplink)?"rel=\"address\" ":"") . "href=\"" . $url_paginacao . $link . ($i_pg_aux+1) . "\">" . ($i_pg_aux+1) . "</a></li>";
				}
					
				echo $str_pg_anterior;
				for($w=0;$w<sizeof($array_pg);$w++)
				{
					$pg_aux = $array_pg[$w];
					if($pg_aux == $pagina)
					{
						?><li class="ativo"><?php echo ($pagina+1); ?><?php 
						if($w<sizeof($array_pg)-1){ echo $divisoria;}
						?></li><?php
					}
					else
					{
						if($w==sizeof($array_pg)-1)
						{
						?><li><a <?php if($deeplink){echo "rel=\"address\" ";}?>href="<?php echo $url_paginacao . $link . ($pg_aux+1) ?>"><?php echo ($pg_aux+1); ?></a></li><?php
						}
						else
						{
						?><li><a <?php if($deeplink){echo "rel=\"address\" ";}?>href="<?php echo $url_paginacao . $link . ($pg_aux+1) ?>"><?php echo ($pg_aux+1); ?></a><?php echo $divisoria; ?></li><?php
						}
					}
				}
				echo $str_pg_posterior;
				?>
				</ul>
			</div>
			
			<div style="float:left">
			<a <?php if($deeplink){echo "rel=\"address\" ";}?>href="<?php echo $url_paginacao . $link. ($i_qtd_pg) ?>" class="btnUltimo"></a>
			</div>
		</div>

<!-- ****************************************** -->          
<?php
  }
  else
  {
  	?><div class="linkTopListagem"></div><?php
  } 
  ?>
  </div>
<?php }

?>