<?php

include_once("config.php");

?>
<section class="site-section border-bottom bg-light" id="diferenciais">
  <div class="anchor diferenciais" data-tpad="0"></div>
  <div class="container">
    <div class="row mb-5">
      <div class="col-12 text-center">
        <h2 class="section-title mb-3 text-white"><?php echo $arrayConteudos["diferenciais"]["titulo"]; ?></h2>
      </div>
    </div>
    <div class="row align-items-stretch">

      <?php

      carrega_classe("diferenciais");

      $itens = new diferenciais();
      $itens = $itens->get_array_ativos("","ranking ASC");

      if(is_array($itens) && sizeof($itens) > 0) {
        $iDelay = 0;
        foreach($itens as $item) {
          $it_id = intval($item->get_var("id"));
          $it_tit = stripslashes(get_output($item->get_var("titulo_" . $_CONFIG["ref_lang"])));
          if(trim($it_tit) == "") {$it_tit = stripslashes(get_output($item->get_var("titulo_pt")));}
          $it_descr = stripslashes(get_output($item->get_var("descricao_" . $_CONFIG["ref_lang"])));
          if(trim($it_descr) == "") {$it_descr = stripslashes(get_output($item->get_var("descricao_pt")));}
          $it_icon = stripslashes(get_output($item->get_var("icone_classe")));
          $it_img = stripslashes(get_output($item->get_var("icone")));
          
          //data-aos="fade-up"
          ?>
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos-delay="<?php echo $iDelay; ?>">
            <div class="unit-4 d-flex">
              <div class="unit-4-icon mr-4"><span class="text-primary">
                <?php if(trim($it_img) != "") {
                  ?><img class="lazy" data-src="<?php echo ROOT_SERVER . ROOT . $item->get_upload_folder("icone") . $it_img; ?>" /><?php
                } else if(trim($it_icon) != "") {
                  ?><i class="<?php echo $it_icon; ?>"></i><?php
                } ?>
              </span></div>
              <div>
                <h3><?php echo $it_tit; ?></h3>
                <p><?php echo $it_descr; ?></p>
                <?php /*<p><a href="#">Learn More</a></p>*/ ?>
              </div>
            </div>
          </div>
          <?php

          $iDelay += 100;
        }
      }
      ?>

    </div>
  </div>
</section>