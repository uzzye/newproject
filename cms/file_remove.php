<?php

include_once("config.php");

include_once("verificaLogado.php");

ob_clean();

// Um arquivo em específico, sem alterar no banco
if(trim($_REQUEST["file_url"]) != "" && trim($_REQUEST["file_url"]) != "undefined" &&
   trim($_REQUEST["file_url"]) != "." && trim($_REQUEST["file_url"]) != ".." && 
   trim($_REQUEST["file_url"]) != "./" && trim($_REQUEST["file_url"]) != "../" &&
   trim($_REQUEST["file_url"]) != "/" && stristr(trim($_REQUEST["file_url"]), UPLOAD_FOLDER . "temp/") === false)
{
	@unlink($_REQUEST["file_url"]);
	die();
} else {
	// Através de folder e file, mudando no banco conforme parâmetros ou não
	$folder = UPLOAD_FOLDER;
	if(trim($_REQUEST["folder"]) != "" && trim($_REQUEST["folder"]) != $folder && trim($_REQUEST["folder"]) != "undefined") {
	      $folder = rawurldecode($_REQUEST["folder"]);
	}

	if(trim($_REQUEST["file"]) <> "" && trim($_REQUEST["file"]) <> "undefined")
	{
		@unlink($folder . $_REQUEST["file"]);
		
		if(trim($_REQUEST["db_table"]) <> "" && trim($_REQUEST["db_field"]) <> "" &&
		   trim($_REQUEST["db_table"]) <> "undefined" && trim($_REQUEST["db_field"]) <> "undefined")
		{
			$db->exec_query("DELETE FROM " . $_REQUEST["db_table"] . " WHERE " . $_REQUEST["db_field"] . " = \"" . $_REQUEST["file"] . "\" ");		
		}
		else if(trim($_REQUEST["modulo"]) <> "" && trim($_REQUEST["modulo"]) <> "undefined")
		{
			if(intval($_REQUEST["id"]) > 0) {
				$modAux = trim($_REQUEST["modulo"]);

				$_REQUEST["field"] = str_replace("crop_","",$_REQUEST["field"]);

				carrega_classe($modAux);
			
				$obj = new $modAux();
				/*$obj->inicia_dados();
				$obj->set_var("id",$_REQUEST["id"]);
				$obj->carrega_dados();
				$obj->set_var($_REQUEST["field"],"");	
				$obj->edita(false);*/

				//var_dump($obj->nome_tabela); die();
				//var_dump($obj->model->nome_tabela); die();

				$sqlCmd = "UPDATE " . $obj->model->nome_tabela . " SET " . $_REQUEST["field"] . " = NULL WHERE id = " . intval($_REQUEST["id"]) . " ";

				//echo $sqlCmd; die();
				$array_result = $db->exec_query($sqlCmd, true);
				$result = $array_result[0];

				unset($obj);
				die();
			}
		}
	}

	// Todos os arquivos do modulo/id específico
	else {
		if(trim($_REQUEST["modulo"]) <> "" && trim($_REQUEST["modulo"]) <> "undefined" &&
		   trim($_REQUEST["id"]) <> "" && trim($_REQUEST["id"]) <> "undefined" && intval($_REQUEST["id"]) > 0)
		{
			$classAux = trim($_REQUEST["modulo"]);
			carrega_classe($classAux);
			if(class_exists($classAux)) {
				$objAux = new $classAux();
				$objAux->inicia_dados();
				$objAux->set_var("id", intval($_REQUEST["id"]));
				$objAux->carrega_dados();

				// Na v3.0 o exclui já remove arquivos

				$objAux->exclui(true);

				die();
			}
		}
	}
}

?>