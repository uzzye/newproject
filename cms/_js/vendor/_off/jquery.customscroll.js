/*!
 * VERSION: beta 1.6.0
 * DATE: 2013-02-09
 * JavaScript 
 * UPDATES AND DOCS AT: http://www.greensock.com
 *
 * @license Copyright (c) 2008-2013, GreenSock. All rights reserved.
 * This work is subject to the terms in http://www.greensock.com/terms_of_use.html or for 
 * Club GreenSock members, the software agreement that was issued with your membership.
 * 
 * @author: Jack Doyle, jack@greensock.com
 **/
(window._gsQueue||(window._gsQueue=[])).push(function(){_gsDefine("plugins.ScrollToPlugin",["plugins.TweenPlugin"],function(a){var b=function(){a.call(this,"scrollTo"),this._overwriteProps.pop()},c=b.prototype=new a("scrollTo"),d=document.documentElement,e=window,f=b.max=function(a,b){var c="x"===b?"Width":"Height",f="scroll"+c,g="client"+c,h=document.body;return a===e||a===d||a===h?Math.max(d[f],h[f])-Math.max(d[g],h[g]):a[f]-a["offset"+c]},g=a.prototype.setRatio;return c.constructor=b,b.API=2,c._onInitTween=function(a,b,c){return this._wdw=a===e,this._target=a,this._tween=c,"object"!=typeof b&&(b={y:b}),this._autoKill=b.autoKill!==!1,this.x=this.xPrev=this.getX(),this.y=this.yPrev=this.getY(),null!=b.x?this._addTween(this,"x",this.x,"max"===b.x?f(a,"x"):b.x,"scrollTo_x",!0):this.skipX=!0,null!=b.y?this._addTween(this,"y",this.y,"max"===b.y?f(a,"y"):b.y,"scrollTo_y",!0):this.skipY=!0,!0},c.getX=function(){return this._wdw?null!=e.pageXOffset?e.pageXOffset:null!=d.scrollLeft?d.scrollLeft:document.body.scrollLeft:this._target.scrollLeft},c.getY=function(){return this._wdw?null!=e.pageYOffset?e.pageYOffset:null!=d.scrollTop?d.scrollTop:document.body.scrollTop:this._target.scrollTop},c._kill=function(b){return b.scrollTo_x&&(this.skipX=!0),b.scrollTo_y&&(this.skipY=!0),a.prototype._kill.call(this,b)},c.setRatio=function(a){g.call(this,a);var b=this._wdw||!this.skipX?this.getX():this.xPrev,c=this._wdw||!this.skipY?this.getY():this.yPrev,d=c-this.yPrev,f=b-this.xPrev;this._autoKill&&(!this.skipX&&(f>7||-7>f)&&(this.skipX=!0),!this.skipY&&(d>7||-7>d)&&(this.skipY=!0),this.skipX&&this.skipY&&this._tween.kill()),this._wdw?e.scrollTo(this.skipX?b:this.x,this.skipY?c:this.y):(this.skipY||(this._target.scrollTop=this.y),this.skipX||(this._target.scrollLeft=this.x)),this.xPrev=this.x,this.yPrev=this.y},a.activate([b]),b},!0)}),window._gsDefine&&_gsQueue.pop()();

/*
	Author	: Chrysto Panayotov
	Date	: 2/28/13
*/

$(function(){
	
	var $window = $(window);
	var isTweening = false;
	
	/*document.onmousewheel = function(){ customScroll(); }
	if(document.addEventListener){
	    document.addEventListener('DOMMouseScroll', customScroll, false);
	}*/

	var mousewheelevt=(/Firefox/i.test(navigator.userAgent))? "DOMMouseScroll" : "mousewheel" //FF doesn't recognize mousewheel as of FF3.x
 
	if (document.attachEvent) //if IE (and Opera depending on user setting)
	    document.attachEvent("on"+mousewheelevt, function(e){customScroll();})
	else if (document.addEventListener) //WC3 browsers
	    document.addEventListener(mousewheelevt, function(e){customScroll();}, false)
	 
	function customScroll(event){
	   bRollinDownTheRiver = false;
	   
	   var delta = 0;
	   
	   if (!event){
		   event = window.event;
	   }
	   
	   if (event.wheelDelta) {
		   delta = event.wheelDelta/120;
	   } else if(event.detail) {
		   delta = -event.detail/3;
	   }
	   
	   if (delta){
	   		
	   		//console.log(isTweening);
	   		
	   		//if(!isTweening){
		   		
		   		//isTweening = true;
		   		
			   	var scrollTop = $window.scrollTop();
			   	var finScroll = scrollTop - parseInt(delta*100) * 3;
			   
			   	//console.log(finScroll);
			   	   					   			   
			   	TweenMax.to($(window), 1.5, {
				   	scrollTo : { y: finScroll, autoKill:true },
				   	ease: Power4.easeOut,
				   	autoKill: true,
				   	overwrite: 5,
				   	onComplete: function(){ 
				   		//console.log(isTweening);
				   		//isTweening = false; 
				   	}
				});
		   
		   //}
	   }
	   
	   if (event.preventDefault){
		   event.preventDefault();
	   }
	   
	   event.returnValue = false;
	     				    
	}
	
});