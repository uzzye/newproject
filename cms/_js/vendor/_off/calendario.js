$(function(e) {
	$(".calendario input").each(function(e){
		var elAux = $(this);

		// SE NÃO FOR DATAHORA
		if(elAux.data("time") != "true"){
			// SE NÃO FOR SOMENTE LEITURA
			if(elAux.prop("readonly") != true){
				// SE FOR DATA DE FIM, NADA FAZ, POIS JÁ É CONFIGURADA NA DE INÍCIO
				if(elAux.attr("id_inicio") != "" && elAux.attr("id_inicio") != undefined){
				}
				else {
					// SE É DATA DE INÍCIO E TEM FIM, CONFIGURA AMBOS
					if(elAux.attr("id_fim") != "" && elAux.attr("id_fim") != undefined && $("#" + elAux.attr("id_fim")).prop("readonly") != true){
						addDatePicker("#" + elAux.attr("id") + ",#" + elAux.attr("id_fim"),elAux.attr("id"),elAux.attr("id_fim"));
					}
					else {
					// SE É DATA SIMPLES
						addDatePicker("#" + elAux.attr("id"));
					}
				}
			}
		}
	});
});

function addDatePicker(elements,minDate,maxDate,showOnFocus)
{
	var showOn = "focus";
	var bimg = "_estilo/images/geral/calendar_2.png";
	if(showOnFocus && showOnFocus == true)
	{		
		/*showOn = "focus";
		bimg = "";*/
	}
	var dates = $(elements).datepicker({
		showOn: showOn,
		buttonImage: bimg,
		buttonImageOnly: true,
		showOtherMonths: true,
		selectOtherMonths: true,
		changeMonth: true,
		changeYear: true,
		dateFormat: 'dd/mm/yy',
		dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'],
		dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
		dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
		nextText: _PROXIMO,
		prevText: _ANTERIOR,
		numberOfMonths: 1,
		onSelect: function(selectedDate) {
			var option = (this.id == "data_inicio" || this.id == "data_inicial") ? "minDate" : "maxDate";
			//var option = "";
			if(minDate)
			{
				option = (this.id == minDate) ? "minDate" : "maxDate";
			}
			if(maxDate)
			{
				option = (this.id == maxDate) ? "maxDate" : "minDate";
			}			
			var instance = $(this).data("datepicker");
			var date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
			dates.not(this).datepicker("option", option, date);

			if($(this).attr("onchange") != "") {
				setTimeout($(this).attr("onchange") + "()",1);
			}
		}
	});
}
