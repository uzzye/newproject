function AJAX() {
	
	this.Updater=carregarDados;
	function carregarDados(caminhoRetorno,idResposta,metodo,mensagem) {

		var conteudo=document.getElementById(idResposta)
		conteudo.innerHTML= mensagem;

		var xmlhttp = getXmlHttp();

		//Abre a url
		xmlhttp.open(metodo.toUpperCase(), caminhoRetorno,true);

		//Executada quando o navegador obtiver o código
		xmlhttp.onreadystatechange=function() {

			if (xmlhttp.readyState==4){

				//Lê o texto
				var texto=xmlhttp.responseText;

				//Desfaz o urlencode
				texto=texto.replace(/\+/g," ");
				texto=unescape(texto);

				//Exibe o texto no div conteúdo

				var conteudo=document.getElementById(idResposta);
				conteudo.innerHTML=texto;

			}
		}
		xmlhttp.send(null);
	}
}

function getXmlHttp() {
	var xmlhttp;
	try{
		xmlhttp = new XMLHttpRequest();
	}catch(ee){
		try{
			xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
		}catch(e){
			try{
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}catch(E){
				xmlhttp = false;
			}
		}
	}
	return xmlhttp;
}

function unlink_image(src)
{
	var xmlhttp = getXmlHttp();
	xmlhttp.open("GET","unlink_image.php?src=" + escape(src),true);
	xmlhttp.send(null);
}

function unlink_all_images(src,modulo)
{
	var xmlhttp = getXmlHttp();
	xmlhttp.open("GET","unlink_image.php?dir=" + escape(src) + "&modulo=" + escape(modulo),true);
	xmlhttp.send(null);
}

/*function file_remove(idDiv,src,modulo,id,field,db_table,db_field,folder)
{
	if (window.confirm(_DESEJAREMOVERARQUIVODEFINITIVAMENTE))
	{
		$.blockUI({ message: '<h1>Por favor aguarde...</h1>' });
	 		
		var xmlhttp = getXmlHttp();
		xmlhttp.open("GET","file_remove.php?file=" + escape(src) + "&modulo=" + escape(modulo) + "&id=" + escape(id) + "&field=" + escape(field) + "&db_table=" + escape(db_table) + "&db_field=" + escape(db_field) + "&folder=" + escape(folder),true);
		xmlhttp.send(null);
	
		document.getElementById(idDiv).innerHTML = "";
	
		$.unblockUI({ message: '<h1>Por favor aguarde...</h1>' });
	}
}*/

//v 3.0
function file_remove(folder,file,modulo,id,field)
{
	showLoading(iTimeAuxFade);

	var urlAux = ROOT_SERVER + ROOT + "file_remove.php?folder=" + escape(folder) + "&file=" + escape(file) + "&modulo=" + escape(modulo) + "&id=" + escape(id) + "&field=" + escape(field) + "";
	console.log(urlAux);
	$.ajax({ 
        url: urlAux
    }).done(function(data) {
		hideLoading(iTimeAuxFade);
		swal("Removido!", "O arquivo foi removido.", "success");
    });
}
function file_remove_modulo(modulo,id)
{
	showLoading(iTimeAuxFade);

	var urlAux = ROOT_SERVER + ROOT + "file_remove.php?modulo=" + escape(modulo) + "&id=" + escape(id) + "";
	console.log(urlAux);
	$.ajax({ 
        url: urlAux
    }).done(function(data) {
		hideLoading(iTimeAuxFade);
		swal("Removido!", "O arquivo foi removido.", "success");
    });
}
function file_remove_url(file_url, doSwal)
{
	showLoading(iTimeAuxFade);

	var urlAux = ROOT_SERVER + ROOT + "file_remove.php?file_url=" + escape(file_url) + "";
	console.log(urlAux);
	$.ajax({ 
        url: urlAux
    }).done(function(data) {
    	if(doSwal || doSwal == undefined) {
			swal("Removido!", "O arquivo foi removido.", "success");
		}
		hideLoading(iTimeAuxFade);
    });
}

function getListBoxSel(idAux)
{
	var arraySel = new Array(3);
	var strSel = "";

	if(document.getElementById(idAux)){
		var lb = document.getElementById(idAux);
		
		if(lb.options[0].value == "" || lb.options[0].value == "0")
		{
			arraySel[1] = lb.options[0].value;
			arraySel[2] = lb.options[0].innerHTML;
		}

		if(!lb.options[0].selected){						
			for(var i=0;i<lb.options.length;i++)
			{
				if(lb.options[i].selected)
				{
					if(strSel != "")
					{
						strSel += ",";
					}
					strSel += lb.options[i].value;
				}
			}
		}
	}

	arraySel[0] = strSel;

	return arraySel;
}

function refreshListBox(idLB,modulo,value_field,html_field,sqlWhere,sqlOrderBy,exprValor,exprDisplay,sqlTableJoin,sqlFieldJoinFrom,sqlFieldJoinTo,selValues)
{
	showLoading(iTimeAuxFade);

	var arraySel = getListBoxSel(idLB);

	var strSel = arraySel[0];
	var rootVal = arraySel[1];
	var root = arraySel[2];

	var urlAux = "get_lb_options.php?modulo=" + escape(modulo) + "&value_field=" + escape(value_field) + "&html_field=" + escape(html_field);
	if(sqlWhere != "undefined" && sqlWhere != "" && sqlWhere != undefined){
		urlAux += "&sqlWhere=" + escape(sqlWhere);}
	if(sqlOrderBy != "undefined" && sqlOrderBy != "" && sqlOrderBy != undefined){
		urlAux += "&sqlOrderBy=" + escape(sqlOrderBy);}
	if(strSel != "undefined" && strSel != "" && strSel != undefined){
		urlAux += "&sel=" + escape(strSel);}
	if(root != "undefined" && root != "" && root != undefined){
		urlAux += "&root=" + escape(root);}
	if(exprValor != "undefined" && exprValor != "" && exprValor != undefined){
		urlAux += "&exprValor=" + escape(exprValor);}
	if(exprDisplay != "undefined" && exprDisplay != "" && exprDisplay != undefined){
		urlAux += "&exprDisplay=" + escape(exprDisplay);}
	if(sqlTableJoin != "undefined" && sqlTableJoin != "" && sqlTableJoin != undefined){
		urlAux += "&sqlTableJoin=" + escape(sqlTableJoin);}
	if(sqlFieldJoinFrom != "undefined" && sqlFieldJoinFrom != "" && sqlFieldJoinFrom != undefined){
		urlAux += "&sqlFieldJoinFrom=" + escape(sqlFieldJoinFrom);}
	if(sqlFieldJoinTo != "undefined" && sqlFieldJoinTo != "" && sqlFieldJoinTo != undefined){
		urlAux += "&sqlFieldJoinTo=" + escape(sqlFieldJoinTo);}

	setTimeout(function() {
		$.ajax({ 
	        url: urlAux,
	        async: false,
	        success: function(data) {
				if(document.getElementById(idLB))
				{
					select_innerHTML(document.getElementById(idLB),data);
					/* Select2 Init*/
					if($("#" + idLB).hasClass("select2")) {
						try {
							$("#" + idLB).select2("destroy");
						} catch(e) {
							console.log(e.message);
						}
						$("#" + idLB).select2();
					}
				}

				hideLoading(iTimeAuxFade);
			}
		});
	}, iTimeAuxFade);
}

function select_innerHTML(objeto,innerHTML){
	/******
	* select_innerHTML - corrige o bug do InnerHTML em selects no IE
	* Veja o problema em: http://support.microsoft.com/default.aspx?scid=kb;en-us;276228
	* Versão: 2.1 - 04/09/2007
	* Autor: Micox - Náiron José C. Guimarães - micoxjcg@yahoo.com.br
	* @objeto(tipo HTMLobject): o select a ser alterado
	* @innerHTML(tipo string): o novo valor do innerHTML
	*******/
    objeto.innerHTML = ""
    var selTemp = document.createElement("micoxselect")
    var opt;
    selTemp.id="micoxselect1"
    document.body.appendChild(selTemp)
    selTemp = document.getElementById("micoxselect1")
    selTemp.style.display="none"
    if(innerHTML.toLowerCase().indexOf("<option")<0){//se não é option eu converto
        innerHTML = "<option>" + innerHTML + "</option>"
    }
    innerHTML = innerHTML.replace(/<option/g,"<span").replace(/<\/option/g,"</span");
    innerHTML = innerHTML.replace(/<OPTION/g,"<span").replace(/<\/OPTION/g,"</span");
    selTemp.innerHTML = innerHTML
      
    
    for(var i=0;i<selTemp.childNodes.length;i++){
	  var spantemp = selTemp.childNodes[i];
	  
	        if(spantemp.tagName){     
	            opt = document.createElement("OPTION")
	    
	   if(document.all){ //IE
	    objeto.add(opt)
	   }else{
	    objeto.appendChild(opt)
	   }       
	    
	   //getting attributes
	   for(var j=0; j<spantemp.attributes.length ; j++){
	    var attrName = spantemp.attributes[j].nodeName;
	    var attrVal = spantemp.attributes[j].nodeValue;
	    if(attrVal){
	     try{
	      opt.setAttribute(attrName,attrVal);
	      opt.setAttributeNode(spantemp.attributes[j].cloneNode(true));
	     }catch(e){}
	    }
	   }
	   //getting styles
	   if(spantemp.style){
	    for(var y in spantemp.style){
	     try{opt.style[y] = spantemp.style[y];}catch(e){}
	    }
	   }
	   //value and text
	   opt.value = spantemp.getAttribute("value")
	   opt.text = spantemp.innerHTML
	   //IE
	   opt.selected = spantemp.getAttribute('selected');
	   opt.className = spantemp.className;
	  } 
	 }    
	 document.body.removeChild(selTemp)
	 selTemp = null
}