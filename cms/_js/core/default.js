// Use if iSroll needs it
/* if (document.addEventListener){
	document.addEventListener('DOMContentLoaded', loaded, false);
} else {
	loaded();
} */

function loaded() {

	initMenu();

	$(document).ready(function(e){
		loadieEl = $("body");
		$(".loadie").css("width","0px").show();
		loadieEl.loadie(0);
		$("#loadie-percent").html("0%");
		loadieEl.loadie(0.1);
		$("#loadie-percent").html("10%");

		if(navigator.platform == "iPad") {
			$("a").each(function() { // have to use an `each` here - either a jQuery `each` or a `for(...)` loop
				var onClick; // this will be a function
				var firstClick = function() {
					onClick = secondClick;
					return false;
				};
				var secondClick = function() {
					onClick = firstClick;
					return true;
				};
				onClick = firstClick;
				$(this).click(function() {
					return onClick();
				});
			});
		}

	    $('body').on( 'DOMMouseScroll mousewheel', function ( event ) {
			if(verificaRolagem) {
				verificaRolagem(event);
			}
		});
			
		// SCROLLORAMA
		try {
			if(ScrollMagic && ScrollMagic != undefined) {
				scrollController = new ScrollMagic.Controller();
				prepareScrollorama();
			}
		} catch (e) {
			console.log(e.message);
		}

		/* carregou site */
		doLoaded(e);

		hideLoading(iTimeAuxFade, true);
		
	// RESIZE

		$(window).resize(function(e){
			
			if(atualizaConteudo){
				atualizaConteudo(e);
			} else {
				if(atualizaSections){
					atualizaSections(e);
				}
			}

			if(atualizaBG){
				atualizaBG(e);
			}

			if(atualizaBGInterna){
				atualizaBGInterna(e);
			}

			ajustaScrollers(e);

			if(myScrolls != undefined & Object.size(myScrolls) > 0){
				for (var key in myScrolls) {
				    if (key === 'length' || !myScrolls.hasOwnProperty(key)) continue;
				    myScrolls[key].refresh();
				}
			}

			if(scrollController){
				scrollController.destroy(true);
			}
			scrollController = null;
			try {
				if(ScrollMagic && ScrollMagic != undefined) {
					scrollController = new ScrollMagic.Controller();
					prepareScrollorama();			
				}
			} catch (e) {
				console.log(e.message);
			}	

			if(scrollController){
				scrollController.update(true);
			}

			atualizaConteudo(e);
			setTimeout(function(e){
				atualizaConteudo(e);
			},500);
		});

		window.onorientationchange = function(e){			
			
			if(atualizaConteudo){
				atualizaConteudo(e);
			} else {
				if(atualizaSections){
					atualizaSections(e);
				}
			}

			if(atualizaBG){
				atualizaBG(e);
			}

			if(atualizaBGInterna){
				atualizaBGInterna(e);
			}

			ajustaScrollers(e);

			if(myScrolls != undefined & Object.size(myScrolls) > 0){
				for (var key in myScrolls) {
				    if (key === 'length' || !myScrolls.hasOwnProperty(key)) continue;
				    myScrolls[key].refresh();
				}
			}
		
			if(scrollController){
				scrollController.destroy(true);
			}
			scrollController = null;
			try {
				if(ScrollMagic && ScrollMagic != undefined) {
					scrollController = new ScrollMagic.Controller();
					prepareScrollorama();			
				}
			} catch (e) {
				console.log(e.message);
			}	

			if(scrollController){
				scrollController.update(true);
			}

			atualizaConteudo(e);
			setTimeout(function(e){
				atualizaConteudo(e);
			},500);
		};
		
		try {
			verificaParallax();
		} catch (e) {
			//console.log(e.message);
		}
	});
}

function doLoaded(e) {
	if(AJAXCallback){
		AJAXCallback(e);
	}

	if(loadInterna){
		loadInterna(e);
	}

	if(atualizaConteudo){
		atualizaConteudo(e);
	} else {
		if(atualizaSections){
			atualizaSections(e);
		}
	}

	if(atualizaBG){
		atualizaBG(e);
	}

	if(atualizaBGInterna){
		atualizaBGInterna(e);
	}

	ajustaScrollers(e);

	if(myScrolls != undefined & Object.size(myScrolls) > 0){
		for (var key in myScrolls) {
		    if (key === 'length' || !myScrolls.hasOwnProperty(key)) continue;
		    myScrolls[key].refresh();
		}
	}

	if(scrollController) {
		scrollController.update(true);
	}

    if($(".pageSize").length > 0) {
    	windowHeight = $(".pageSize").height();
    	windowWidth = $(".pageSize").width();
    }
	
	// Speed
	$(".page").css("visibility","visible");

	// OUTROS EVENTOS

    // MOUSE SCROLL

    var $window = $(window);
	var scrollTime = 1;
	var scrollDistance = 333;

	$(document).on({"touchstart" : function(){
		if(bRollinDownTheRiver) {
			$('html, body').stop();
		}
	}});

	$window.on("mousewheel DOMMouseScroll", function(event){
		if(!isMobile && window.innerWidth > 900) {
			if(scrollController) {
				//scrollController.triggerCheckAnim();
				scrollController.update(true);
			}
	  
			//event.preventDefault();
			event.stopPropagation();

			if(verificaRolagem) {
				verificaRolagem(event);
			}
		} else {

			if(verificaRolagem) {
				verificaRolagem(event);
			}
			
		}
	});
}

function rolaPagina(idAux, idNav, event){
	var idPagAnt = AJAX_paginaAtual;
    AJAX_paginaAtual = idAux;

    if(AJAX_paginaAtual != ""){

        var iSpeed = 2;

       	if(idNav != "") {
       		var navigator = $("#" + idNav);

       		if(navigator.length > 0) {
		        navigator.find(".itNav").removeClass("active");
		        navigator.find("#itNav_" + AJAX_paginaAtual).addClass("active");
		    }
	    }
        
        var offset = 0;
        var sAux = "";
	    if($(".anchor." + idAux).length > 0) {
            sAux = $(".anchor." + idAux).offset().top - offset;
            if($(".anchor." + idAux).data("tpad") && $(".anchor." + idAux).data("tpad") != "") {
            	sAux = parseFloat(sAux) + parseFloat($(".anchor." + idAux).data("tpad"));
            }
	    } else if($("#" + idAux).length > 0){
            sAux = $("#" + idAux).offset().top - offset;
            if($("#" + idAux).data("tpad") && $("#" + idAux).data("tpad") != "") {
            	sAux = parseFloat(sAux) + parseFloat($("#" + idAux).data("tpad"));
            }
        }

        if(sAux != "") {
	        // MOUSEWHEEL 

	        $(document).on( 'mousewheel DOMMouseScroll', function (e) {
	        	$('html, body').stop();
	            clearTimeout(toRoll);
				
				if(verificaRolagem) {
					verificaRolagem(event);
				}
	        });

			clearTimeout(toRoll);

			$('html, body').stop().animate({
	        	scrollTop: sAux
	        }, {
	        	duration: (iSpeed*1000),
	        	easing: "easeInOutExpo",
	        	complete: function(e){
    				bRollinDownTheRiver = false;
					onCompleteTween(idAux, e);
	        	}, 
	        	progress: function(e){
        			bRollinDownTheRiver = true;
	        		if(scrollController) {
		    			//scrollController.triggerCheckAnim();
						scrollController.update(true);
		    		}
	        	}
	        });
        }
    }
}

function onCompleteTween(idAux, event) {
	var iSpeed = 2;    
    var offset = 0;
    var sAux = "";

    if($(".anchor." + idAux).length > 0) {
        sAux = $(".anchor." + idAux).offset().top - offset;
        if($(".anchor." + idAux).data("tpad") && $(".anchor." + idAux).data("tpad") != "") {
        	sAux = parseFloat(sAux) + parseFloat($(".anchor." + idAux).data("tpad"));
        }
    } else if($("#" + idAux).length > 0){
        sAux = $("#" + idAux).offset().top - offset;
        if($("#" + idAux).data("tpad") && $("#" + idAux).data("tpad") != "") {
        	sAux = parseFloat(sAux) + parseFloat($("#" + idAux).data("tpad"));
        }
    }

	clearTimeout(toRoll);

	if($(window).scrollTop() != sAux) {

		$('html, body').stop().animate({
	    	scrollTop: sAux
	    }, {
	    	duration: (iSpeed*1000),
	    	easing: "easeOutExpo",
	    	complete: function(e){
	    		bRollinDownTheRiver = false;
	    		//$(document).unbind('mousewheel DOMMouseScroll');
	    	}, 
	    	progress: function(e){
	            bRollinDownTheRiver = true;
	    		if(scrollController) {
	    			//scrollController.triggerCheckAnim();
					scrollController.update(true);
	    		}
	    	}
	    });
	} else {
	   	bRollinDownTheRiver = false;		
	}
}

function showLoading(iTime){
	if(iTime && iTime > 0){
		$("#loader-wrapper").fadeIn(iTime);
	} else {
		$("#loader-wrapper").show();
	}
}

function hideLoading(iTime, bIni){
	if(bIni){
		$(".page").css("visibility","visible");
	} else {	
		if(iTime && iTime > 0){
			$("#loader-wrapper").fadeOut(iTime);
		} else {
			$("#loader-wrapper").hide();
		}
	}
}

function atualizaSections(e){
	// SIZES

	/*windowWidth = window.innerWidth
	|| document.documentElement.clientWidth
	|| document.body.clientWidth;*/
	windowWidth = document.documentElement.clientWidth
	|| document.body.clientWidth;

	windowHeight = window.innerHeight
	|| document.documentElement.clientHeight
	|| document.body.clientHeight;

    if($(".pageSize").length > 0) {
    	windowHeight = $(".pageSize").height();
    	windowWidth = $(".pageSize").width();
    }

	if(windowWidth < 520){
		$("body").addClass("minus");
	} else {
		$("body").removeClass("minus");
	}

	var wBase = windowWidth;
	var hBase = windowHeight;
	var attrWAux = "width";
	var attrHAux = "height";
	
	$(".fullScreen, .fullHeight, .fullWidth").each(function(e){
		attrWAux = "width";
		attrHAux = "height";
		if($(this).data("attr-width") != "" && $(this).data("attr-width") != undefined) {attrWAux = $(this).data("attr-width");}
		if($(this).data("attr-height") != "" && $(this).data("attr-height") != undefined) {attrHAux = $(this).data("attr-height");}

		var wAux = wBase;
		var hAux = hBase;
		if(parseFloat($(this).css("padding-top").replace("px","")) > 0) {hAux = hAux - parseFloat($(this).css("padding-top").replace("px",""));}
		if(parseFloat($(this).css("padding-bottom").replace("px","")) > 0) {hAux = hAux - parseFloat($(this).css("padding-bottom").replace("px",""));}
		if(parseFloat($(this).css("padding-left").replace("px","")) > 0) {wAux = wAux - parseFloat($(this).css("padding-left").replace("px",""));}
		if(parseFloat($(this).css("padding-right").replace("px","")) > 0) {wAux = wAux - parseFloat($(this).css("padding-right").replace("px",""));}
		if(parseFloat($(this).css("margin-top").replace("px","")) > 0) {hAux = hAux - parseFloat($(this).css("margin-top").replace("px",""));}
		if(parseFloat($(this).css("margin-bottom").replace("px","")) > 0) {hAux = hAux - parseFloat($(this).css("margin-bottom").replace("px",""));}
		if(parseFloat($(this).css("margin-left").replace("px","")) > 0) {wAux = wAux - parseFloat($(this).css("margin-left").replace("px",""));}
		if(parseFloat($(this).css("margin-right").replace("px","")) > 0) {wAux = wAux - parseFloat($(this).css("margin-right").replace("px",""));}
		if($(this).hasClass("fullScreen") || $(this).hasClass("fullWidth")) {
			$(this).css(attrWAux,wAux.toString() + "px");
		}
		if($(this).hasClass("fullScreen") || $(this).hasClass("fullHeight")) {
			$(this).css(attrHAux,hAux.toString() + "px");
		}
	});

	if($(".fullScreenBox").length > 0) {
		$(".fullScreenBox").each(function(e){
			attrWAux = "width";
			attrHAux = "height";
			if($(this).data("attr-width") != "" && $(this).data("attr-width") != undefined) {attrWAux = $(this).data("attr-width");}
			if($(this).data("attr-height") != "" && $(this).data("attr-height") != undefined) {attrHAux = $(this).data("attr-height");}

			var wAux = wBase;
			var hAux = hBase;
			$(this).css(attrWAux,wAux.toString() + "px");
			$(this).css(attrHAux,hAux.toString() + "px");
		});
	}

	if($(".fullScreenParent").length > 0) {
		$(".fullScreenParent").each(function(e){
			attrWAux = "width";
			attrHAux = "height";
			if($(this).data("attr-width") != "" && $(this).data("attr-width") != undefined) {attrWAux = $(this).data("attr-width");}
			if($(this).data("attr-height") != "" && $(this).data("attr-height") != undefined) {attrHAux = $(this).data("attr-height");}

			var wAux = $(this).parent().innerWidth();
			var hAux = $(this).parent().innerHeight();
			$(this).css(attrWAux,wAux.toString() + "px");
			$(this).css(attrHAux,hAux.toString() + "px");
		});
	}

	$(".square").each(function(e){
		attrWAux = "width";
		attrHAux = "height";
		if($(this).data("attr-width") != "" && $(this).data("attr-width") != undefined) {attrWAux = $(this).data("attr-width");}
		if($(this).data("attr-height") != "" && $(this).data("attr-height") != undefined) {attrHAux = $(this).data("attr-height");}

		var wAux = $(this).innerWidth();
		$(this).css(attrHAux,wAux.toString() + "px");
	});

	if($(".wide").length > 0) {
		$(".wide").each(function(e){
			attrWAux = "width";
			attrHAux = "height";
			if($(this).data("attr-width") != "" && $(this).data("attr-width") != undefined) {attrWAux = $(this).data("attr-width");}
			if($(this).data("attr-height") != "" && $(this).data("attr-height") != undefined) {attrHAux = $(this).data("attr-height");}
			
			var wAux = $(this).innerWidth();
			var hAux = Math.floor((wAux*9)/16);
			if($(this).hasClass("img") && !$(this).closest(".gallery").hasClass("gal-prod")) {
				//hAux = Math.floor((wAux*6)/16);
			}

			$(this).css(attrHAux,hAux.toString() + "px");
		});
	}

	$(".middle").each(function(e){
		if($(this).is("img")){
			$(this).load(function(e){
				$(this).css("margin-top",($(this).parent().height()/2 - $(this).height()/2).toString() + "px").css("visibility","visible");
			});
			$(this).css("margin-top",($(this).parent().height()/2 - $(this).height()/2).toString() + "px").css("visibility","visible");
		} else {
			$(this).css("margin-top",($(this).parent().height()/2 - $(this).height()/2).toString() + "px").css("visibility","visible");
		}
	});
	$(".center").each(function(e){
		if($(this).is("img")){
			$(this).load(function(e){
				$(this).css("margin-left",($(this).parent().width()/2 - $(this).width()/2).toString() + "px").css("visibility","visible");
			});
			$(this).css("margin-left",($(this).parent().width()/2 - $(this).width()/2).toString() + "px").css("visibility","visible");
		} else {
			$(this).css("margin-left",($(this).parent().width()/2 - $(this).width()/2).toString() + "px").css("visibility","visible");
		}
	});

	// AJUSTA MENU

	var wGeral = windowWidth;
	//if(isMobile && wGeral <= 1080) {
	//if(isMobile || wGeral <= 1080)
	{
		$(".topo .menu").css("height",windowHeight.toString() + "px");

		if($(".topo .btnMenu a").length > 0) {
			$(".topo .btnMenu a").unbind("click").click(function(e){
				e.preventDefault();
				e.stopPropagation();
				if($(".topo .menu").hasClass("opened")) {
					$(".topo .menu").removeClass("opened");
					$(".topo .btnMenu").removeClass("opened");
					$(".topo .logo").removeClass("opened");
					$(".topo").removeClass("opened");
				} else {
					$(".topo .menu").addClass("opened");
					$(".topo .btnMenu").addClass("opened");
					$(".topo .logo").addClass("opened");
					$(".topo").addClass("opened");
				}
		
				if(verificaRolagem) {
					verificaRolagem(e);
				}
			});

			$(document).unbind("click").click(function(e){
				$(".topo .menu").removeClass("opened");
				$(".topo .btnMenu").removeClass("opened");
				$(".topo .logo").removeClass("opened");
				$(".topo").removeClass("opened");

				$(".subMenu").each(function(e){
					if($(this).css("display") != "none") {
						//$(this).fadeOut(iTimeAuxFade);
						$(this).hide();
					} 
				});
		
				if(verificaRolagem) {
					verificaRolagem(e);
				}
			});
		}
	} /*else {
		$(".topo .menu").css("height","50px");
	}*/

	// KENNY

	setHeight();

	if(verificaRolagem) {
		verificaRolagem(e);
	}

	if(scrollController) {
		//scrollController.triggerCheckAnim();
		scrollController.update(true);
	}
}

/***** Full height function start *****/
var setHeight = function () {
	var height = $(window).height();
	$('.full-height').css('height', (height));
	$('.page-wrapper').css('min-height', (height));
	
	/*Vertical Tab Height Cal Start*/
	var verticalTab = $(".vertical-tab");
	if( verticalTab.length > 0 ){ 
		for(var i = 0; i < verticalTab.length; i++){
			var $this =$(verticalTab[i]);
			$this.find('ul.nav').css(
			  'min-height', ''
			);
			$this.find('.tab-content').css(
			  'min-height', ''
			);
			height = $this.find('ul.ver-nav-tab').height();
			$this.find('ul.nav').css(
			  'min-height', height + 40
			);
			$this.find('.tab-content').css(
			  'min-height', height + 40
			);
		}
	}
	/*Vertical Tab Height Cal End*/
};
/***** Full height function end *****/

function verificaSetas(idAux,iPagAux){
	if(idAux){
		if(iPagAux <= 0){
			if($("#btnAnt-" + idAux).css("display") != "none"){
				$("#btnAnt-" + idAux).fadeOut(iTimeAuxFade);
			}
		} else {
			if($("#btnAnt-" + idAux).css("display") == "none"){
				$("#btnAnt-" + idAux).fadeIn(iTimeAuxFade);
			}
		}

		var iQtdAux = $("#scroller-" + idAux).find("ul").eq(0).children().length-1;
		if(myScrolls[idAux]) {
			iQtdAux = myScrolls[idAux].pages.length-1;
		}
		if(iPagAux >= iQtdAux){
			if($("#btnProx-" + idAux).css("display") != "none"){
				$("#btnProx-" + idAux).fadeOut(iTimeAuxFade);
			}
		} else {
			if($("#btnProx-" + idAux).css("display") == "none"){
				$("#btnProx-" + idAux).fadeIn(iTimeAuxFade);
			}
		}
	}
}

function initMenu(){
	window.onscroll = function(){
		if(verificaRolagem) {
			verificaRolagem(event);
		}
	}
	window.onscroll();
}

function verificaParallax() {
	if(!isMobile && window.innerWidth > 900)
	{
		if($('.bgParallax').length > 0) {
			$('.bgParallax').css("background-repeat","no-repeat");
			$('.bgParallax').css("background-attachment","fixed");
	 
			$('.bgParallax').each(function(e){
				var yAux = 0;
				if($(this).find("img.parallax-img").length > 0) {
					var hAux = 0;
					var hEAux = parseFloat($(this).find("img.parallax-img").height());
					if(hEAux > 0) {
						var hPAux = parseFloat($(this).parent().height());
						if(hPAux > hEAux) {
							hAux = Math.floor(hPAux - hEAux)/2;
						} else {
							hAux = Math.floor(hEAux - hPAux)/2;
						}
					}
					yAux += hAux*-1;
				}				
				if($(this).data("ypad") && $(this).data("ypad") != "") {
					yAux += parseFloat($(this).offset().top)*-1 + parseFloat($(this).data("ypad"));
				}

				if(window.innerWidth < 990) {
					yAux = yAux*0.1;
				}

				var iSpeed = 1;
				if(!isMobile && window.innerWidth > 900)
				{
					if($(this).data("speed") && $(this).data("speed") != "") {
						iSpeed = $(this).data("speed");
					}

					if(iSpeed != 1 && window.innerWidth <= 900) {
						iSpeed = iSpeed * 0.1;
					}
				}

				var align = "50%";
				if($(this).data("align") != undefined) {
					align = $(this).data("align");
				}

				$(this).bgParallax(align,
					iSpeed,
					yAux
				);
			});
		}
	} else {
		if($('.bgParallax').length > 0) {
			$('.bgParallax').css("background-position","center center");
			$('.bgParallax').css("background-repeat","no-repeat");
			$('.bgParallax').css("background-attachment","scroll");
		}
	}
}