/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For the complete reference:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.toolbarGroups = [
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
		{ name: 'links' },
		{ name: 'insert' },
		{ name: 'forms' },
		{ name: 'tools' },
		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'others' },
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
		{ name: 'styles' },
		{ name: 'colors' }
		//{ name: 'about' }
	];

	// Remove some buttons, provided by the standard plugins, which we don't
	// need to have in the Standard(s) toolbar.
	config.removeButtons = 'Underline,Subscript,Superscript';

	// Se the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';

	// Make dialogs simpler.
	config.removeDialogTabs = 'image:advanced;link:advanced';

	config.language = 'pt-br';
	config.toolbar = 'Full';
    config.enterMode = CKEDITOR.ENTER_BR;
    config.shiftEnterMode = CKEDITOR.ENTER_P;
    config.autoParagraph = false;

	//config.skin = 'minimalist';
	config.skin = 'bootstrapck';

	// CKFinder
	/*config.filebrowserBrowseUrl = ROOT + '_js/vendor/ckeditor/ckfinder/ckfinder.html',
    config.filebrowserImageBrowseUrl = ROOT + '_js/vendor/ckeditor/ckfinder/ckfinder.html?type=Images',
    config.filebrowserFlashBrowseUrl = ROOT + '_js/vendor/ckeditor/ckfinder/ckfinder.html?type=Flash',
    config.filebrowserUploadUrl = ROOT + '_js/vendor/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&amp;type=Files',
    config.filebrowserImageUploadUrl = ROOT + '_js/vendor/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&amp;type=Images',
    config.filebrowserFlashUploadUrl = ROOT + '_js/vendor/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&amp;type=Flash'*/

    // KCFinder
    config.filebrowserBrowseUrl = ROOT + '_js/vendor/ckeditor/kcfinder/browse.php?type=files';
    config.filebrowserImageBrowseUrl = ROOT + '_js/vendor/ckeditor/kcfinder/browse.php?type=images';
    config.filebrowserFlashBrowseUrl = ROOT + '_js/vendor/ckeditor/kcfinder/browse.php?type=flash';
    config.filebrowserUploadUrl = ROOT + '_js/vendor/ckeditor/kcfinder/upload.php?type=files';
    config.filebrowserImageUploadUrl = ROOT + '_js/vendor/ckeditor/kcfinder/upload.php?type=images';
    config.filebrowserFlashUploadUrl = ROOT + '_js/vendor/ckeditor/kcfinder/upload.php?type=flash';

    config.extraPlugins = 'colorbutton,justify';

    config.extraAllowedContent = 'style;*[id,rel](*){*}';
};