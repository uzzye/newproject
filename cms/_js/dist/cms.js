var bFinishLoad = false;

var bCanSubmit = true;
// Disable auto discover for all elements:
Dropzone.autoDiscover = false;

$(document).ready(function(){
  kenny();
  $('.la-anim-1').addClass('la-animate');
});
/*****Ready function end*****/

/*****Load function start*****/
$(window).load(function(){
	$(".preloader-it").delay(500).fadeOut("slow");
});

AJAXCallback = function(e, AJAX_tipoAux, AJAX_titAux, AJAX_idAux){
	// PADRÃO

	loadieEl.loadie(0.33);
	$("#loadie-percent").html("33%");
	showLoading();

	if(atualizaConteudo){
		atualizaConteudo(e);
	} else {
		if(atualizaSections){
			atualizaSections(e);
		}
	}

	if(atualizaBG){
		atualizaBG(e);
	}

	if(atualizaBGInterna){
		atualizaBGInterna(e);
	}

	ajustaScrollers(e);

	if(myScrolls != undefined & Object.size(myScrolls) > 0){
		for (var key in myScrolls) {
		    if (key === 'length' || !myScrolls.hasOwnProperty(key)) continue;
		    myScrolls[key].refresh();
		}
	}

	if(scrollController){
		scrollController.update(true);
	}

    $('.dragging').unbind("click").click(function (e) {
        e.preventDefault();
    });

    // OUTROS EVENTOS

	// FORM
 
	if($(".form-cadastro-padrao").not(".no-ajax").length > 0) {
		$(".form-cadastro-padrao").not(".no-ajax").each(function(e){
			var formPadrao = $(this);
			var options = { 
			    url: formPadrao.data("url-post"), 
			    beforeSerialize: function($form, options) {
			    	//console.log(formPadrao.data("url-post"));
					
					showLoading(iTimeAuxFade);
					
			    	// Remove image uploads 
			    	if($(".image-file").length > 0) {
						$(".image-file").remove();
					}

					// Saves all contents

					var x = 0; //x is the number of instances
					for(var instances in CKEDITOR.instances){ x ++;}
					if(x > 0) {CKEDITOR.instances.body.updateElement();}
				
					tinymce.triggerSave();

					/*if($(".tinymce").length > 0) {		
						$(".tinymce").each(function(e){
							var idAux = $(this).attr("id");
							tinymce.get(idAux).save();
						});
					}*/
			    },			    
			    beforeSubmit: function(arr, $form, options) { 
				    // The array of form data takes the following form: 
				    // [ { name: 'username', value: 'jresig' }, { name: 'password', value: 'secret' } ] 
				     
				    // return false to cancel submit                  
				    options.headers = {
				    	'Access-Control-Allow-Origin': '*',
						'Access-Control-Allow-Methods': '*',
						'Access-Control-Allow-Headers': 'api-key,content-type,accept',
						'Access-Control-Allow-Credentials': true,
						'Accept': '*/*'
				   	}

			    	if(formPadrao.hasClass("blocked")) {
			    		e.preventDefault();
			    		e.stopPropagation();
			    		return false; 
			    	} else {
			    		formPadrao.find(".btn-submit").find(".btn-text").html(_lang["_AGUARDE"]);
						formPadrao.addClass("blocked");
					} 
			    },
			    success: function(data) {
			    	formPadrao.find(".btn-submit").find(".btn-text").each(function(e){
			    		$(this).html($(this).data("label"));
			    	});
					formPadrao.removeClass("blocked");

			        if ($(data).find(".alert-success").length > 0) {
						formPadrao.find("input").each(function(e){
							if($(this).attr("type") == "button" ||
							   $(this).attr("type") == "hidden" ||
							   $(this).attr("type") == "submit")
							{
								// NADA
							}
							else if($(this).attr("type") == "checkbox" ||
							   $(this).attr("type") == "radio")
							{
								//$(this).iCheck('uncheck');
							} else {
								$(this).val("");
							}
						});

						formPadrao.find("textarea").val("");

						formPadrao.find("select").each(function(e){
							$(this).find("option").eq(0).attr('selected','selected');
							$(this).trigger('update.fs');
						});

						if(formPadrao.data("url-return") && formPadrao.data("url-return") != "") {
							$.ajax({ 
		                        url: formPadrao.data("url-return"), 
		                        success: function(newData) {
		                        	AJAX_bSkip = true;
		                        	window.location = formPadrao.data("url-return").replace("/ajax/","/#/");

			                        AJAX_paginaAtual = window.location;

									$("#cp-main").html(newData);
									$("#cp-main").find(".heading-bg").after(data);

									if(AJAXCallback){
		                            	AJAXCallback(e, AJAX_tipoAux, AJAX_titAux, AJAX_idAux);
		                            } 

		                            if(atualizaConteudoInterna){
										atualizaConteudoInterna(e);
									}

									if(data.search("<!--reload-->") > -1) {
			                        	setTimeout(function(e){
											window.location.reload(true);
										},iTimeAuxFade);
									}

									document.body.scrollTop = document.documentElement.scrollTop = 0;
								}
							});
						} else {
							hideLoading(iTimeAuxFade);
						}
					} else {
						$("#cp-main").find(".row.alert-display").remove();
						$("#cp-main").find(".heading-bg").after(data);

						hideLoading(iTimeAuxFade);
					}
			    } 
			}; 
			formPadrao.ajaxForm(options);
		});
	}

	// AJAX
	if($('a[rel=\"address\"]').length > 0) {
        $('a[rel=\"address\"]').each(function(){
        	if($(this).attr("href") != undefined && $(this).attr("href") != ""){
            	$(this).attr("href",$(this).attr("href").replace(ROOT_SERVER+ROOT,""));
            }
        });
    }

    // APENAS UMA VEZ
    if(AJAX_bInicial) {

    	//$.address.crawlable(true);
	    $.address.init(function(event) {        
	        // Initializes plugin support for links
	        $("a[rel=\"address\"]").address();
	        $('a[rel=\"address\"]').click(function(){
	            event.preventDefault();
	        });
	    }).change(function(event) {
	    	if(event.path == "" || event.path == "/"){
	        	event.path = "/"
	        }

	        var AJAX_url = "";

			var cpAux = $("#cp-main");

	        var array_aux = event.path.split("/");
	        var AJAX_tipoAux = array_aux[1];
	        var AJAX_acaoAux = array_aux[2];
	        var AJAX_titAux = array_aux[3];
	        var AJAX_idAux = array_aux[4];

	        var extra_param = window.location.href.split("?");
	        //console.log(extra_param);

	        if(!AJAX_bSkip) {
	        	//if(AJAX_tipoAux != "" && AJAX_tipoAux != undefined){

				if(AJAX_paginaAtual != event.path) {
	        		if(AJAX_tipoAux == "") {
					    //AJAX_url = ROOT + "ajax/cms_home";
					    AJAX_url = ROOT + "ajax/" + CMS_MODULO;
				    } else {
				    	AJAX_url = ROOT + "ajax";
				    	for(var i=0; i<array_aux.length; i++) {
				    		if(array_aux[i] != "") {
				    			AJAX_url += "/" + array_aux[i];
				    		}
				    	}
				    }

				    if(extra_param.length > 1) {
				    	AJAX_url += "?" + extra_param[1];
				    }
			        
			        //console.log(AJAX_url);
			        if(AJAX_url != "") {
	        			$(".loadie").css("width","0px").show();
						loadieEl.loadie(0);
						$("#loadie-percent").html("0%");
						showLoading(iTimeAuxFade);

				    	$(".page.wrapper").removeClass("slide-nav-toggle");
				    	verificaMenuButton(0);

				    	AJAX_bLoading = true;

		        		$.ajax({ 
	                        url: AJAX_url,
	                        success: function(data) {
	                        	if($('a[rel=\"address\"]').length > 0) {
		                            $('a[rel=\"address\"]').each(function(){
		                            	if($(this).attr("href") != undefined && $(this).attr("href") != ""){
		                                	$(this).attr("href",$(this).attr("href").replace(ROOT_SERVER+ROOT,""));
		                                }
		                            });
		                        }

		                        //if(cpAux.css("display") != "none")
		                        {
		                        	cpAux.fadeOut(iTimeAuxFade);
		                        }

		                        setTimeout(function(e){

									AJAX_bLoading = false;
			
									cpAux.html(data);

									if(AJAXCallback){
		                            	AJAXCallback(e, AJAX_tipoAux, AJAX_titAux, AJAX_idAux);
		                            } 

		                            if(atualizaConteudoInterna){
										atualizaConteudoInterna(e);
									}

									//if(cpAux.css("display") == "none")
									{
			                        	cpAux.fadeIn(iTimeAuxFade);
			                        }

			                        if(data.search("<!--reload-->") > -1) {
			                        	setTimeout(function(e){
											window.location.reload(true);
										},iTimeAuxFade);
									}                

		                        },iTimeAuxFade);

		                        AJAX_paginaAtual = event.path;
		                	}
		                }); 
				    }

				    rolaPagina(AJAX_tipoAux);
		        }

				AJAX_bInicial = false;
	        }
	        AJAX_bSkip = false;
	    });
	}

	// KENNY
	
	kenny();

	// MENU COLLAPSE
	if($(".side-nav a").length > 0) {
		$(".side-nav a").on("click", function(e) {
			if(!$(this).data("target") || $(this).data("target") == "") {
				$(".page.wrapper").removeClass("slide-nav-toggle");
				verificaMenuButton();
			}
		});
	}
	$(".page-wrapper, .navbar-fixed-top").on("click", function(e){
		$(".page.wrapper").removeClass("slide-nav-toggle");
		verificaMenuButton();
	});

	// EXCLUIR EM MASSA

	if($(".btn-excluir-massa").length > 0) {
		$(".btn-excluir-massa").unbind("click").click(function(e){
			var btnAux = $(this);
			var urlAux = btnAux.data("url");

			var regids = "";
			btnAux.closest(".panel-body").find(".select-reg:checked").not(".select-all-reg").each(function(e){
				if(regids != "") {regids += ",";}
				regids += $(this).attr("value");
			});

			swal({   
	            title: _lang["_SWAL_VOCE_TEM_CERTEZA"],   
	            text: _lang["_SWAL_NAO_SERA_POSSIVEL_RECUPERAR_PLURAL"],  
	            type: "warning",  
	            showCancelButton: true,   
	            confirmButtonColor: "#fcb03b",   
	            confirmButtonText: _lang["_SWAL_SIM_REMOVER"],   
	            cancelButtonText: _lang["_SWAL_NAO_CANCELAR"],   
	            closeOnConfirm: false,   
	            closeOnCancel: true,
	            allowOutsideClick: true
	        }, function(isConfirm){   
	            if (isConfirm) {  
	            	showLoading(iTimeAuxFade);

	            	$.ajax({ 
		                url: urlAux,
		                data: {
		                	"regids": regids
		               	}
		            }).done(function(data) {

						if ($(data).find(".alert-success").length > 0) {
							btnAux.closest(".panel-body").find(".select-reg").prop("checked",false);
							btnAux.closest(".panel-body").find(".btn-excluir-massa").addClass("hidden");

	                		var idAux = btnAux.closest(".panel-body").find(".table.dataTable").attr("id");
							var table = dataTables[idAux];

							var arrAux = regids.split(",");
							for(var i=0; i<arrAux.length; i++) {
								idAux = arrAux[i];
								table.row($("#checkbox-registro-" + idAux).closest("tr[role=row]")).remove().draw();
							}

							swal(_lang["_SWAL_REMOVIDOS"], _lang["_SWAL_REGISTROS_REMOVIDOS"], "success");   
						} else {
							swal(_lang["_SWAL_OPS"], _lang["_SWAL_ALGO_ACONTECEU"], "error");   
						}

		                $(".row.alert-display").remove();
		                $(".row.heading-bg").after(data);

		                document.body.scrollTop = document.documentElement.scrollTop = 0;

		                hideLoading(iTimeAuxFade);
		            });
	            } else {     
	               //swal("Cancelado", "Nenhum registro foi removido.", "error");   
	            } 
	        });
			return false;
		});
	}

	// ATUALIZA BOTÕES DA TABELA

	refreshTabelaRegistros();

    // LOAD
	
    //loadieEl.loadie(0.75);
	if($("img").length > 0) {
		var iPass = 0.5 / $("img").length;
		var iTotal = 0.5;
		$("img").imagesLoaded().progress(function( instance, image ) {
		    var result = image.isLoaded ? 'loaded' : 'broken';
		    //console.log( 'image is ' + result + ' for ' + image.img.src );
		    iTotal += iPass;
    		loadieEl.loadie(iTotal);
			$("#loadie-percent").html((Math.ceil(iTotal*100)).toString() + "%");
    		//sleep(100);

    		$(instance).removeClass("showOnLoad");
		}).always( function( instance ) {
	    	finishLoad(e);
		});
	}
	else {
    	finishLoad();
	}

	$(window).focus(function(e){
		if(!bFinishLoad) {
			finishLoad(e);
		}
	});
}

function atualizaConteudo(e){
	if(atualizaSections){
		atualizaSections(e);
	}

	// SCROLLERS

	if($(".wrapper.withIScroll").length > 0) {
		$(".wrapper.withIScroll").each(function(e){
			$(this).css("width",$(this).parent().innerWidth().toString() + "px");
			
			if($(this).find(".itPag").length > 0) {
				$(this).find(".itPag").css("width",$(this).innerWidth().toString() + "px");

				/*$(this).find(".itPag").css("height","auto");
				var hMaior = 0;
				$(this).find(".itPag").each(function(e){
					var hAux = $(this).innerHeight();
					if(hAux > hMaior) {
						hMaior = hAux;
					}
				});	
				$(this).find(".itPag").css("height",hMaior.toString() + "px");*/
			}
		});
	}

	ajustaScrollers(e);

	if(myScrolls != undefined & Object.size(myScrolls) > 0){
		for (var key in myScrolls) {
		    if (key === 'length' || !myScrolls.hasOwnProperty(key)) continue;
		    myScrolls[key].refresh();
		}
	}

	// Atualiza tabela de registros

	refreshTabelaRegistros();
	setTimeout(function(e) {
		refreshTabelaRegistros();
	}, iTimeAuxFade);

	// OUTROS EVENTOS

	verificaMenuButton(0);

	if(atualizaSections){
		atualizaSections(e);
	}
}

function finishLoad(e) {

    $('.dragging').unbind("click").click(function (e) {
        e.preventDefault();
    });

	// SCROLLER

	if($('.wrapper.withIScroll').length > 0){
		$('.wrapper.withIScroll').each(function(e){
			var elAux = $(this);
			var arrAux = $(this).attr("id").split("-");
			var idAux = arrAux[1].toString();// + "-" + arrAux[2].toString();

			myScrolls[idAux] = new IScroll('#wrapper-' + idAux, {
				snap: true,
				momentum: false,
				hScrollbar: false,
				vScrollbar: false,
				hScroll: true,
				vScroll: false,
				scrollX: true,
				scrollY: false,
				wheelAction : 'none',
				eventPassthrough: true,
				preventDefault: false
			});
			
			myScrolls[idAux].on('scrollStart',function(e){
            	$('#wrapper-' + idAux + ' img').addClass("dragging");
            	//$('#wrapper-' + idAux + ' a').addClass("dragging");
        	});
			
			myScrolls[idAux].on('scrollEnd',function(e){
            	$('#wrapper-' + idAux + ' img').removeClass("dragging");
            	//$('#wrapper-' + idAux + ' a').removeClass("dragging");
            	
				if($("#nav-" + idAux + " .liNav").length > 0){
					$("#nav-" + idAux + " .liNav").removeClass("ativo");
					$("#nav-" + idAux + " .liNav").eq(this.currentPage.pageX).addClass("ativo");
				}

				verificaSetas(idAux,this.currentPage.pageX);
			});

			if($("#nav-" + idAux + " .liNav").length > 0){
				$("#nav-" + idAux + " .liNav").removeClass("ativo");
				$("#nav-" + idAux + " .liNav").eq(0).addClass("ativo");
			}

			if($("#nav-" + idAux + " .liNav").length > 0){
				if($('#wrapper-' + idAux + ' .itPag').length > 0){
					if($('#wrapper-' + idAux + ' .itPag').length == 1){
						$("#nav-" + idAux + "").css("visibility","hidden");
					}

					$("#nav-" + idAux + " .liNav a").unbind("click").click(function(e){
						var iIndexAux = $("#nav-" + idAux + " .liNav").index($(this).parent());

						myScrolls[idAux].goToPage(iIndexAux,0);
					});
				}
			}

			$(".setas #btnAnt-" + idAux + "").unbind("click").click(function(e){
				myScrolls[idAux].prev();
			});
			$(".setas #btnProx-" + idAux + "").unbind("click").click(function(e){
				myScrolls[idAux].next();
			});

			ajustaScrollers(e);

			verificaSetas(idAux,myScrolls[idAux].currentPage.pageX);

			if(myScrolls[idAux]){
				myScrolls[idAux].refresh();
			}
		});
	}

	atualizaConteudo(e);

	if(!AJAX_bLoading) {
		loadieEl.loadie(1.0);
		$("#loadie-percent").html("100%");
		hideLoading(iTimeAuxFade);
	}

	// SCROLLORAMA

	if(scrollController) {
		setTimeout(function(e){
			scrollController.update(true);
		},iTimeAuxFade);

		scrollController.update(true);
	}

	verificaMenuButton(0);

	bFinishLoad = true;
}

function refreshTabelaRegistros() {

	// CHECKBOXES DE LISTAGEM

	if($(".dataTables_wrapper th .checkbox").length > 0) {
		$(".dataTables_wrapper th .checkbox").on("click", function(e){
			e.stopPropagation();
		});
	}

	if($(".select-reg").length > 0) {
		$(".select-reg").unbind("change").change(function(e){
			if($(this).hasClass("select-all-reg")) {
				$(this).closest(".panel-body").find(".select-reg").prop("checked",$(this).prop("checked"));
			}

			var checks = $(this).closest(".panel-body").find(".select-reg:checked").not(".select-all-reg");
			
			if(checks.length > 0) {
				$(this).closest(".panel-body").find(".btn-excluir-massa").removeClass("hidden");
			} else {
				$(this).closest(".panel-body").find(".btn-excluir-massa").addClass("hidden");
			}
		});
	}

	// ATIVA/DESATIVA

	if($(".acao-toggle-ativo").length > 0) {
		$(".acao-toggle-ativo").unbind("click").click(function(e){
			e.preventDefault();
			e.stopPropagation();

			var url = $(this).data("url");
			var btnAcao = $(this);

			showLoading(iTimeAuxFade);

			$.ajax({ 
                url: url,
                success: function(data) {
            		btnAcao.closest("tr[role=row]").find(".col-ativo").html(data);
					$(".row.alert-display").remove();

					hideLoading(iTimeAuxFade);
            	}
            });

        	if(btnAcao.find(".fa").hasClass("fa-toggle-on")) {
        		btnAcao.find(".fa").removeClass("fa-toggle-on").removeClass("text-success").addClass("fa-toggle-off").addClass("text-danger");
        	} else if(btnAcao.find(".fa").hasClass("fa-toggle-off")) {
        		btnAcao.find(".fa").removeClass("fa-toggle-off").removeClass("text-danger").addClass("fa-toggle-on").addClass("text-success");
        	}
        });
    }

    // ACOES TOGGLE

    if($(".acao-toggle").length > 0) {
		$(".acao-toggle").unbind("click").click(function(e){
			e.preventDefault();
			e.stopPropagation();

			var url = $(this).data("url");
			var funcAux = $(this).data("function");
			var ajaxAux = $(this).data("ajax");
			var btnAcao = $(this);

			var processa = true;

			if(btnAcao.data("condition") && btnAcao.data("condition") != "") {
				processa = eval(btnAcao.data("condition"));
			}
			
			if(processa) {
				var hasSwal = true;
				if(btnAcao.data("swal-condition") && btnAcao.data("swal-condition") != "") {
					hasSwal = eval(btnAcao.data("swal-condition"));
				}

				if(hasSwal) {
					var textAux = _lang["_SWAL_CONFIRMA_ALTERACAO"];
					if(btnAcao.data("swal-text") && btnAcao.data("swal-text") != "") {
						textAux = btnAcao.data("swal-text");
					}
					var valAux = "";
					if(btnAcao.data("swal-value") && btnAcao.data("swal-value") != "") {
						valAux = btnAcao.data("swal-value");
					}
					var placeAux = "";
					if(btnAcao.data("swal-placeholder") && btnAcao.data("swal-placeholder") != "") {
						placeAux = btnAcao.data("swal-placeholder");
					}
					var typeAux = "warning";
					if(btnAcao.data("swal-type") && btnAcao.data("swal-type") != "") {
						typeAux = btnAcao.data("swal-type");
					}
					swal({   
			            title: _lang["_SWAL_VOCE_TEM_CERTEZA"],   
			            text: textAux,
			            type: typeAux,   
		  				inputPlaceholder: placeAux,
		  				inputValue: valAux,
			            showCancelButton: true, 
			            confirmButtonColor: "#fcb03b",   
			            confirmButtonText: _lang["_SWAL_SIM_CONFIRMAR"],   
			            cancelButtonText: _lang["_SWAL_NAO_CANCELAR"],   
			            closeOnConfirm: true,   
			            closeOnCancel: true,
			            allowOutsideClick: true
			        }, function(isConfirm){

						var dataAux = {
							"prompt_value": $(".sweet-alert.show-input input").val()	
						};

			        	if (isConfirm) {
			            	showLoading(iTimeAuxFade);     

							if(ajaxAux == undefined || ajaxAux.toString() == "true") {  
								$.ajax({ 
									url: url,
									data: dataAux,
									success: function(returnData) {
										eval(funcAux);

										document.body.scrollTop = document.documentElement.scrollTop = 0;

										hideLoading(iTimeAuxFade);
									}
								});
							} else {
								if(window.location.href == url) {
									window.location.reload(true);
								} else {
									window.location = url;
								}
							}
			            } else {     
			               //swal("Cancelado", "Nenhum registro foi removido.", "error");   
			            } 
			        });
					return false; 
				} else {
					showLoading(iTimeAuxFade);   
					
					if(ajaxAux == undefined || ajaxAux.toString() == "true") {  
						$.ajax({ 
							url: url,
							success: function(returnData) {
								eval(funcAux);

								document.body.scrollTop = document.documentElement.scrollTop = 0;

								hideLoading(iTimeAuxFade);
							}
						});
					} else {
						if(window.location.href == url) {
							window.location.reload(true);
						} else {
							window.location = url;
						}
					}
				}
			}
        });
    }

	// DELETE CONFIRM

	if($(".acao-excluir").length > 0) {
		$(".acao-excluir").unbind("click").click(function(e){
			e.preventDefault();
			e.stopPropagation();

			var url = $(this).data("url");
			var btnAcao = $(this);

		    swal({   
	            title: _lang["_SWAL_VOCE_TEM_CERTEZA"],   
	            text: _lang["_SWAL_NAO_SERA_POSSIVEL_RECUPERAR"],  
	            type: "warning",   
	            showCancelButton: true,   
	            confirmButtonColor: "#fcb03b",   
	            confirmButtonText: _lang["_SWAL_SIM_REMOVER"],   
	            cancelButtonText: _lang["_SWAL_NAO_CANCELAR"],   
	            closeOnConfirm: false,   
	            closeOnCancel: true,
	            allowOutsideClick: true
	        }, function(isConfirm){   
	            if (isConfirm) {
	            	showLoading(iTimeAuxFade);     
	            	$.ajax({ 
                        url: url,
                        success: function(data) {
	                		if ($(data).find(".alert-success").length > 0) {
	                			var idAux = btnAcao.closest(".table.dataTable").attr("id");
								var table = dataTables[idAux];
								table.row(btnAcao.closest("tr[role=row]")).remove().draw();

								swal(_lang["_SWAL_REMOVIDO"], _lang["_SWAL_REGISTRO_REMOVIDO"], "success");   
							} else {
								swal(_lang["_SWAL_OPS"], _lang["_SWAL_ALGO_ACONTECEU"], "error");   
							}

			                $(".row.alert-display").remove();
			                $(".row.heading-bg").after(data);

			                document.body.scrollTop = document.documentElement.scrollTop = 0;

			                hideLoading(iTimeAuxFade);
	                	}
	                });
	            } else {     
	               //swal("Cancelado", "Nenhum registro foi removido.", "error");   
	            } 
	        });
			return false;
		});
	}

	if(Object.size(dataTables) > 0) {
		for(key in dataTables) {
			if (key === 'length' || !dataTables.hasOwnProperty(key)) continue;
			dataTables[key].columns.adjust().responsive.recalc();
		}
	}


	// Tooltips

	if($(".dataTables_wrapper").length > 0) {
		$(".dataTables_wrapper").each(function(e){

			/*Tooltip*/
			if( $(this).find('[data-toggle="tooltip"]').length > 0 ) {
				$(this).find('[data-toggle="tooltip"]').tooltip({
					html: true
				});
			}
			
			/*Popover*/
			if( $(this).find('[data-toggle="popover"]').length > 0 ) {
				$(this).find('[data-toggle="popover"]').popover()
			}
		});
	}
}

function updateMapMarker(map, marker, field){
	map.setCenter(marker.getPosition().lat(), marker.getPosition().lng());
	field.val(marker.getPosition().lat() + "," + marker.getPosition().lng());
}

/*Cropperjs Init*/

window.onload = function () {
  var Cropper = window.Cropper;

  document.body.onkeydown = function (event) {
  var e = event || window.event;

  if (!cropper || this.scrollTop > 300) {
    return;
  }

  switch (e.charCode || e.keyCode) {
    case 37:
      e.preventDefault();
      cropper.move(-1, 0);
      break;

    case 38:
      e.preventDefault();
      cropper.move(0, -1);
      break;

    case 39:
      e.preventDefault();
      cropper.move(1, 0);
      break;

    case 40:
      e.preventDefault();
      cropper.move(0, 1);
      break;
  }
};
};

/***** Kenny function start *****/
var kenny = function(master){

	(function (factory) {
	  if (typeof define === 'function' && define.amd) {
	    // AMD. Register as anonymous module.
	    define(['jquery'], factory);
	  } else if (typeof exports === 'object') {
	    // Node / CommonJS
	    factory(require('jquery'));
	  } else {
	    // Browser globals.
	    factory(jQuery);
	  }
	})(function ($) {

		'use strict';

		var console = window.console || { log: function () {} };

		if(!master || master == undefined) {
			master = $("body");
		}

		/*Cropper*/
		if(master.find("#cropper-modal").length > 0) {
			$("#cropper-modal").one('hidden.bs.modal', function () {
				if (cropper) {
		        	cropper.destroy();
		        }
		    });

			var cropperContainer = document.querySelector('#cropper-img-container');
			var cropperImage = cropperContainer.getElementsByTagName('img').item(0);
			//var download = document.getElementById('crop-download');
			var actions = document.getElementById('crop-actions');
			var dataX = document.getElementById('crop-dataX');
			var dataY = document.getElementById('crop-dataY');
			var dataHeight = document.getElementById('crop-dataHeight');
			var dataWidth = document.getElementById('crop-dataWidth');
			var dataRotate = document.getElementById('crop-dataRotate');
			var dataScaleX = document.getElementById('crop-dataScaleX');
			var dataScaleY = document.getElementById('crop-dataScaleY');
			var dataZoom = document.getElementById('crop-dataZoom');
			var cropperOptions = {
		      aspectRatio: 16 / 9,
		      strict: 1,
		      viewMode: 1,
		      preview: '.img-preview',
		      ready: function (e) {
		        //console.log(e.type);
		      },
		      cropstart: function (e) {
		        //console.log(e.type, e.detail.action);
		      },
		      cropmove: function (e) {
		        //console.log(e.type, e.detail.action);
		      },
		      cropend: function (e) {
		        //console.log(e.type, e.detail.action);
		      },
		      crop: function (e) {
		        var data = e.detail;

		        //console.log(e.type);
		        dataX.value = Math.round(data.x);
		        dataY.value = Math.round(data.y);
		        dataHeight.value = Math.round(data.height);
		        dataWidth.value = Math.round(data.width);
		        dataRotate.value = typeof data.rotate !== 'undefined' ? data.rotate : '';
		        dataScaleX.value = typeof data.scaleX !== 'undefined' ? data.scaleX : '';
		        dataScaleY.value = typeof data.scaleY !== 'undefined' ? data.scaleY : '';

		        var image = cropper.getImageData();
				var zoomLevel = image.width / image.naturalWidth;
		        dataZoom.value = typeof zoomLevel !== 'undefined' ? zoomLevel : '';
		      },
		      zoom: function (e) {
		        //console.log(e.type, e.detail.ratio);
		      }
		    };

		    // Buttons
			if (!document.createElement('canvas').getContext) {
			    $('button[data-method="getCroppedCanvas"]').prop('disabled', true);
			}

			if (typeof document.createElement('cropper').style.transition === 'undefined') {
			    $('button[data-method="rotate"]').prop('disabled', true);
			    $('button[data-method="scale"]').prop('disabled', true);
			}

			// Download
			/*if (typeof download.download === 'undefined') {
			    download.className += ' disabled';
			}*/

			// Options
			/*actions.querySelector('.docs-toggles').onchange = function (event) {
			  var e = event || window.event;
			  var target = e.target || e.srcElement;
			  var cropBoxData;
			  var canvasData;
			  var isCheckbox;
			  var isRadio;

			  if (!cropper) {
			    return;
			  }

			  if (target.tagName.toLowerCase() === 'label') {
			    target = target.querySelector('input');
			  }

			  isCheckbox = target.type === 'checkbox';
			  isRadio = target.type === 'radio';

			  if (isCheckbox || isRadio) {
			    if (isCheckbox) {
			      cropperOptions[target.name] = target.checked;
			      cropBoxData = cropper.getCropBoxData();
			      canvasData = cropper.getCanvasData();

			      cropperOptions.ready = function () {
			        console.log('ready');
			        cropper.setCropBoxData(cropBoxData).setCanvasData(canvasData);
			      };
			    } else {
			      cropperOptions[target.name] = target.value;
			      cropperOptions.ready = function () {
			        console.log('ready');
			      };
			    }

			    // Restart
			    cropper.destroy();
			    cropper = new Cropper(cropperImage, cropperOptions);
			  }
			};*/


			// Methods
			actions.querySelector('.docs-buttons').onclick = function (event) {
			  var e = event || window.event;
			  var target = e.target || e.srcElement;
			  var result;
			  var input;
			  var data;

			  if (!cropper) {
			    return;
			  }

			  while (target !== this) {
			    if (target.getAttribute('data-method')) {
			      break;
			    }

			    target = target.parentNode;
			  }

			  if (target === this || target.disabled || target.className.indexOf('disabled') > -1) {
			    return;
			  }

			  data = {
			    method: target.getAttribute('data-method'),
			    target: target.getAttribute('data-target'),
			    option: target.getAttribute('data-option'),
			    secondOption: target.getAttribute('data-second-option')
			  };

			  if (data.method) {
			    if (typeof data.target !== 'undefined') {
			      input = document.querySelector(data.target);

			      if (!target.hasAttribute('data-option') && data.target && input) {
			        try {
			          data.option = JSON.parse(input.value);
			        } catch (e) {
			          //console.log(e.message);
			        }
			      }
			    }

			    if (data.method === 'getCroppedCanvas') {
			      data.option = JSON.parse(data.option);
			    }

			    result = cropper[data.method](data.option, data.secondOption);

			    switch (data.method) {
			      case 'scaleX':
			      case 'scaleY':
			        target.setAttribute('data-option', -data.option);
			        break;

			      case 'getCroppedCanvas':
			        if (result) {

			          // Bootstrap's Modal
			          $('#getCroppedCanvasModal').modal().find('.modal-body').html(result);

			          /*if (!download.disabled) {
			            download.href = result.toDataURL('image/jpeg');
			          }*/
			        }

			        break;

			      case 'destroy':
			        cropper = null;
			        break;
			    }

			    if (typeof result === 'object' && result !== cropper && input) {
			      try {
			        input.value = JSON.stringify(result);
			      } catch (e) {
			        //console.log(e.message);
			      }
			    }

			  }
			};
		}
		
		/*Counter Animation*/
		var counterAnim = master.find('.counter-anim');
		if( counterAnim.length > 0 ){
			counterAnim.counterUp({ delay: 10,
	        time: 100});
		}
		
		/*Tooltip*/
		if( master.find('[data-toggle="tooltip"]').length > 0 ) {
			master.find('[data-toggle="tooltip"]').tooltip({
				html: true
			});
		}
		
		/*Popover*/
		if( master.find('[data-toggle="popover"]').length > 0 ) {
			master.find('[data-toggle="popover"]').popover()
		}
		
		/*Progress Bar Animation*/
		var progressAnim = master.find('.progress-anim');
		if( progressAnim.length > 0 ){
			for(var i = 0; i < progressAnim.length; i++){
				var $this = $(progressAnim[i]);
				$this.waypoint(function() {
				var progressBar = $this.find(".progress-bar");
				for(var i = 0; i < progressBar.length; i++){
					$this = $(progressBar[i]);
					$this.css("width", $this.attr("aria-valuenow") + "%");
				}
				}, {
				  triggerOnce: true,
				  offset: 'bottom-in-view'
				});
			}
		}	
		
		/*Panel Remove*/
		master.find('.clickable').on('click',function(e){
			e.preventDefault();
			e.stopPropagation();
			var effect = $(this).data('effect');
				$(this).closest('.panel')[effect]();
			return false;	
		});
		
		/*Accordion js*/
		master.find('.panel-collapse').unbind("show.bs.collapse").on('show.bs.collapse', function () {
			$(this).siblings('.panel-heading').addClass('activestate');
		});
		master.find('.panel-collapse').unbind("hide.bs.collapse").on('hide.bs.collapse', function () {
			$(this).siblings('.panel-heading').removeClass('activestate');
		});
		
		/*Sidebar Navigation*/
		if(master.find( "#toggle_nav_btn" ).length > 0) {
			master.find( "#toggle_nav_btn" ).unbind("click").on( "click", function() {
				master.find(".page.wrapper").toggleClass('slide-nav-toggle');
				verificaMenuButton();
				return false;
			});
		}
		if(master.find( "#open_right_sidebar" ).length > 0) {
			master.find( "#open_right_sidebar" ).unbind("click").on( "click", function() {
				master.find(".page.wrapper").toggleClass('open-right-sidebar');
				return false;
			});
		}

		// Tabs
		if(master.find(".nav-tabs a").length > 0) {
			master.find(".nav-tabs a").unbind("click").on("click", function(e) {
				e.preventDefault();
				e.stopPropagation();
				//console.log($(this).attr("rel"),$(this).data("toggle"));
				master.find($(this).attr("rel")).removeClass('active');
				master.find($(this).data("toggle")).addClass('active');
			});
		}

		/* TinyMCE */

		if(master.find(".tinymce").length > 0) {		
			master.find(".tinymce").each(function(e){
				var idAux = $(this).attr("id");
				tinymce.remove('#' + idAux);
				tinymce.init({
					selector: '#' + idAux,
					language: 'pt_BR',
					language_url : ROOT_SERVER + ROOT + '_js/plugins/plugins/langs/pt_BR.js',
					height: 300,
					force_br_newlines : false,
					force_p_newlines : false,
					forced_root_block : '',
					plugins: [
						'advlist autolink lists link image charmap print preview anchor',
						'searchreplace visualblocks code fullscreen jbimages',
						'insertdatetime media table contextmenu paste code textcolor colorpicker'
					],
					toolbar: 'insertfile undo redo | styleselect | bold italic underline forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages',
					relative_urls: false,
					paste_data_images: true,
					images_upload_url: ROOT_SERVER + ROOT + "tinymce_postAcceptor.php",
					images_upload_base_path: ROOT_SERVER + UPLOAD_FOLDER + "original",
					images_upload_credentials: true
				});

				if($(this).data("sourceid") && $(this).data("sourceid") != "") {
					var sourceid = $(this).data("sourceid");

					tinymce.get($(this).data("sourceid")).on('blur', function(e) {
						var data = tinymce.get(sourceid).getContent();
				        tinymce.get(idAux).setContent(data);
				    });
				}	

				if($(this).data("destinyid") && $(this).data("destinyid") != "") {
					var destinyid = $(this).data("destinyid");

					tinymce.get(idAux).on('blur', function(e) {
						var data = tinymce.get(idAux).getContent();
				        tinymce.get(destinyid).setContent(data);
				    });
				}		
			});
		}

		/* CKEditor */
		if(master.find(".ckeditor").length > 0) {		
			master.find(".ckeditor").each(function(e){
				var idAux = $(this).attr("id");
				CKEDITOR.replace(idAux, { customConfig: ROOT_SERVER + ROOT + '_js/plugins/ckeditor/config.js' });
			});
		}

		/* Dropify */
		if(master.find('.dropify').length > 0) {
			master.find('.dropify').each(function(e){
				/* Basic Init*/
				var elAux = $(this);

				var drEvent = elAux.dropify({
					messages: dropify_messages,
					error: dropify_error
				});

				drEvent.on('dropify.beforeClear', function(event, element){
					event.preventDefault();
					event.stopPropagation();
					if(element.input.prop("readonly")) {
						return false
					} else {
						swal({   
					        title: _lang["_SWAL_VOCE_TEM_CERTEZA"],   
					        text: _lang["_SWAL_NAO_SERA_POSSIVEL_RECUPERAR"],  
					        type: "warning",  
					        showCancelButton: true,   
					        confirmButtonColor: "#fcb03b",   
					        confirmButtonText: _lang["_SWAL_SIM_REMOVER"],   
					        cancelButtonText: _lang["_SWAL_NAO_CANCELAR"],   
					        closeOnConfirm: false,   
					        closeOnCancel: true,
					        allowOutsideClick: true
					    }, function(isConfirm){   
					        if (isConfirm) {  
								file_remove(elAux.data("folder"), elAux.data("file"), elAux.data("modulo"), elAux.data("id"), elAux.attr("name"));
								
								element.resetFile();
					            element.input.val('');
					            element.resetPreview();

					            element.input.trigger($.Event("dropify.afterClear"), [element]);
							} else {
					           //swal("Cancelado", "Nenhum arquivo foi removido.", "error");   
					           return false;
					        } 
						});
						return false;
					}
				});
			});
		}
		if(master.find(".btn-download").length > 0) {
			master.find(".btn-download").on("click", function(e){
				e.preventDefault();
				e.stopPropagation();

				window.location = $(this).data("url");
			});
		}
		if(master.find(".btn-crop").length > 0) {
			master.find(".btn-crop").on("click", function(e){			
				if($(this).find(".image-original-value").val() != "") {
					var btnCrop = $(this);
					var elAux = $(this).find(".image-original-value");
					showLoading(iTimeAuxFade);
					setTimeout(function(e) {
						//cropper = new Cropper($('#crop-cropper'), elAux);
						
						// Import image
						var URL = window.URL || window.webkitURL;
						var blobURL;

						if (URL) {
							var file = elAux.val();
							if(cropper) {
								cropper.destroy();
							}
							resetCropModal();
							
							cropperOptions.cropBoxMovable = true;

						// Seta qualidade, arquivo e label da imagem
							$("#cropper-image-src").val(file);
							var cropQuality = "";
							var cropWidth = "";
							var cropHeight = "";

							if((btnCrop.data("quality") && btnCrop.data("quality") != "")) {
								cropQuality = btnCrop.data("quality");
							}
							if((btnCrop.data("width") && btnCrop.data("width") != "")) {
								cropWidth = btnCrop.data("width");
							}
							if((btnCrop.data("height") && btnCrop.data("height") != "")) {
								cropHeight = btnCrop.data("height");
							}
							$("#cropper-image-quality").val(cropQuality);
							$("#cropper-image-width").val(cropWidth);
							$("#cropper-image-height").val(cropHeight);
							$("#cropper-modulo").val(btnCrop.data("modulo"));
							$("#cropper-field").val(btnCrop.data("field"));
							$("#cropper-randcod").val($(".form-cadastro-padrao").find("#randcod").val());
							$("#crop-label").html(btnCrop.data("label"));

						// Configura tamanho do crop
							var cropWidth = 0;
							var cropHeight = 0;

							if((btnCrop.data("width") && btnCrop.data("width") != "") ||
							   (btnCrop.data("height") && btnCrop.data("height") != "")) {
    							cropperOptions.cropBoxResizable = false;

    							cropWidth = parseInt(btnCrop.data("width"));
								cropHeight = parseInt(btnCrop.data("height"));
    						}

    					// Configura aspect ratio do crop
							var cropAR = null;

							if((btnCrop.data("aspect") && btnCrop.data("aspect") != "")) {
								cropAR = btnCrop.data("aspect");
							}

						// No carregar, aplica
							cropperOptions.ready = function(e) {
								//alert(cropWidth + " x " + cropHeight);
								if(cropWidth > 0 || cropHeight > 0) {
									cropper.setAspectRatio(null);
									var bSetAR = false;
									if(cropWidth > 0 && cropHeight > 0) {
										bSetAR = true;
									}

									if(cropWidth==0){cropWidth=999999;}
									if(cropHeight==0){cropHeight=999999;}
									cropper.setCropBoxData({ left: 0, top: 0, width: cropWidth, height: cropHeight });

									if(bSetAR) {
										var dataAux = cropper.getCropBoxData();
										//cropper.setAspectRatio(dataAux.width/dataAux.height);
										cropper.setAspectRatio(cropWidth/cropHeight);
									}
								} else {
									cropper.setAspectRatio(cropAR);
								}		
								cropper.moveTo(0,0);
							};
    
					    	imageURLtoBlob(file, function(data){
							    var bImg = false;
							    if (file.type) {
							        bImg = /^image\/\w+$/.test(file.type);
							    } else {
							        bImg = /\.(jpg|jpeg|png|gif)$/.test(file);
							    }

							    if(bImg) {
								    blobURL = URL.createObjectURL(data);
						    		if (cropper) {
								    	URL.revokeObjectURL(data);
								        //cropper.reset().replace(blobURL);
								        cropper.destroy();
								        //alert("reuse");
								    } 
								    //else
								    {
										cropperImage.src = blobURL;
										cropper = new Cropper(cropperImage, cropperOptions);
										//alert("new");
									}
							    } else {
							    	swal(_lang["_SWAL_AVISO"], _lang["_SWAL_SELECIONAR_IMAGEM"], "error");
		      						$("#cropper-modal").modal('hide');
		      						resetCropModal();
							    }

							    hideLoading(iTimeAuxFade);
							});
						} else {
							resetCropModal();
							hideLoading(iTimeAuxFade);
						}
					},iTimeAuxFade);
				} else {
					e.preventDefault();
					e.stopPropagation();
				}
			});
		}

		if(master.find(".modal-crop").length > 0) {
			// FORM

			var modalAux = master.find(".modal-crop");
			var formAux = modalAux.find("#form_cropper");

			var options = { 
			    url: formAux.data("url"), 
			    beforeSubmit: function(arr, $form, options) { 
				    // The array of form data takes the following form: 
				    // [ { name: 'username', value: 'jresig' }, { name: 'password', value: 'secret' } ] 
				     
				    // return false to cancel submit                  
				    options.headers = {
				    	'Access-Control-Allow-Origin': '*',
						'Access-Control-Allow-Methods': '*',
						'Access-Control-Allow-Headers': 'api-key,content-type,accept',
						'Access-Control-Allow-Credentials': true,
						'Accept': '*/*'
				   	}

			    	if(formAux.hasClass("blocked")) {
			    		e.preventDefault();
			    		e.stopPropagation();
			    		return false; 
			    	} else {
			    		modalAux.find(".btn-submit").find(".btn-text").html(_lang["_AGUARDE"]);
						formAux.addClass("blocked");

						showLoading(iTimeAuxFade);
					} 
			    },
			    success: function(data) { 
			    	modalAux.find(".btn-submit").find(".btn-text").html(_lang["_SALVAR"]);
					formAux.removeClass("blocked");

					// DEBUG
					//modalAux.find(".modal-title").html(data);
					// FIM DEBUG

			        if (data.message != "") {
			        	
			        	// Add data de volta nos .btn-crop
			        	var sIDAux = $("#cropper-modulo").val() + "_" + $("#cropper-field").val();
			        	if($("#image-value-" + sIDAux).length > 0) {
			        		$("#image-value-" + sIDAux).val(data.result);
			        	}

			        	$("#cropper-modal").modal('hide');
		      			resetCropModal();

			        	swal(_lang["_SWAL_SUCESSO"], _lang["_SWAL_CROP_IMAGEM"], "success");
					} else {
						swal("Erro", data.message, "error");   
					}

					hideLoading(iTimeAuxFade);
			    } 
			}; 
			formAux.ajaxForm(options);

			modalAux.find(".btn-submit").unbind("click").click(function(e) {
				e.preventDefault();
				e.stopPropagation();
				formAux.trigger("submit");
			});
		}

		/* Image File */
		if(master.find(".image-file").length > 0) {
			master.find(".image-file").each(function(e){
				var file = $(this);
				file.on("change", function(e){
					e.stopPropagation();

					var formData = new FormData();
					formData.append('file', file[0].files[0]);
					formData.append('randcod', file.closest(".form-cadastro-padrao").find(".randcod").val());
					formData.append('field_id', file.attr("id"));

					showLoading(iTimeAuxFade);

					$.ajax({
				       url : ROOT_SERVER + ROOT + 'file_upload.php',
				       type : 'POST',
				       data : formData,
				       processData: false,  // tell jQuery not to process the data
				       contentType: false,  // tell jQuery not to set contentType
				       success : function(data) {
				           //console.log(data);
				           //alert(data);

				           if(file.closest(".form-group").find(".btn-crop").length > 0) {
				           		file.closest(".form-group").find(".btn-crop").each(function(e){
				           			$(this).find(".image-value").val(data);
				           			$(this).find(".image-original-value").val(data);
				           		});
				           }
				           
				           hideLoading(iTimeAuxFade);
				       }
					});
				});

				var drEvent = file.dropify({
					messages: dropify_messages,
					error: dropify_error
				});

				drEvent.on('dropify.beforeClear', function(event, element){
					event.preventDefault();
					event.stopPropagation();
					if(element.input.prop("readonly")) {
						return false;
					} else {
						swal({   
					        title: _lang["_SWAL_VOCE_TEM_CERTEZA"],   
					        text: _lang["_SWAL_NAO_SERA_POSSIVEL_RECUPERAR"],  
					        type: "warning",  
					        showCancelButton: true,   
					        confirmButtonColor: "#fcb03b",   
					        confirmButtonText: _lang["_SWAL_SIM_REMOVER"],   
					        cancelButtonText: _lang["_SWAL_NAO_CANCELAR"],   
					        closeOnConfirm: false,   
					        closeOnCancel: true,
					        allowOutsideClick: true
					    }, function(isConfirm){   
					        if (isConfirm) {  

					        	 if(file.closest(".form-group").find(".btn-crop").length > 0) {
					           		file.closest(".form-group").find(".btn-crop").each(function(e){
					           			if($(this).find(".image-value").val() != "") {
					           				if($(this).find(".image-value").val().search("/original/") == -1 || CMS_MODULO == "cms_imagens") {
						           				file_remove_url($(this).find(".image-value").val(), false);
						           			}
						           		}
						           		$(this).find(".image-value").val("NULL");
						           		if($(this).find(".image-original-value").val() != "") {
						           			if($(this).find(".image-original-value").val().search("/original/") == -1 || CMS_MODULO == "cms_imagens") {
						           				file_remove_url($(this).find(".image-original-value").val(), false);
						           			}
						           		}
						           		$(this).find(".image-original-value").val("NULL");
					           		});
					            }	

					            if(element.input.data("default-file") && element.input.data("default-file") != "") {
					           		if(element.input.data("default-file").search("/original/") == -1 || CMS_MODULO == "cms_imagens") {
							        	file_remove_url(element.input.data("default-file"), false);
						           		element.input.data("default-file","");
						           	}
					            }
								
							    element.resetFile();
					            element.input.val('');
					            element.resetPreview();

					            element.input.trigger($.Event("dropify.afterClear"), [element]);

								swal(_lang["_SWAL_REMOVIDO"], _lang["_SWAL_REGISTRO_REMOVIDO"], "success");
							} else {
					           //swal("Cancelado", "Nenhum arquivo foi removido.", "error");   
					           return false;
					        } 
						});
						return false;
					}
				});
			});
		}
		
		/*Todo*/
		/*var random = Math.random();
		$(document).on("click","#add_todo",function (e) {
			if (!$('.new-todo input').val().length == 0) {
					$('<li class="todo-item"><div class="checkbox checkbox-success"><input type="checkbox" id="checkbox'+random+'"/><label for="checkbox'+random+'">' + $('.new-todo input').val() + '</label></div></li>').insertAfter(".todo-list .todo-item:last-child");
					$('.new-todo input').val('');
			} else{
					alert('Please enter task!');
			}
			return false;
		});*/
		
		/*Chat*/
		/*$("#input_msg_send").keypress(function (e) {
			if ((e.which == 13)&&(!$(this).val().length == 0)) {
				$('<li class="self"><div class="self-msg-wrap"><div class="msg block pull-right">' + $(this).val() + '<div class="msg-per-detail mt-5"><span class="msg-time txt-grey">4:30pm</span></div></div></div><div class="clearfix"></div></li>').insertAfter(".chat-content > ul li:last-child");
				$(this).val('');
			} else if(e.which == 13) {
				alert('Please type somthing!');
			}
		});
		$(document).on("click",".chat-cmplt-wrap .chat-data",function (e) {
			$(".recent-chat-box-wrap").fadeIn("fast");
			$(".chat-cmplt-wrap").addClass('chat-box-slide');
			setTimeout(function(){
				$(".chat-box-wrap").fadeOut("slow");
			},300);
			
			return false;
		});
		$(document).on("click",".chat-cmplt-wrap .goto-chat-list",function (e) {
			e.preventDefault();
			$(".chat-box-wrap").fadeIn("slow");
			setTimeout(function(){
				$(".chat-cmplt-wrap").removeClass('chat-box-slide');
				$(".recent-chat-box-wrap").fadeOut("slow");
			},300);
			
			return false;
		});*/

		/*Slimscroll*/
		if(master.find('.nicescroll-bar').length > 0) {
			master.find('.nicescroll-bar').slimscroll({height:'100%',color: '#3cb878',disableFadeOut : true});
		}
		if(master.find('.message-nicescroll-bar').length > 0) {
			master.find('.message-nicescroll-bar').slimscroll({height:'320px',color: '#3cb878'});
		}

		/* Select2 Init*/
		if(master.find(".select2").length > 0) {
			master.find(".select2").each(function(e){
				if(!$(this).hasClass("select2-hidden-accessible")) {
					$.fn.modal.Constructor.prototype.enforceFocus = function() {};
					if($(this).attr("readonly")) {
						$(this).select2({
							language: "pt-BR",
							dropdownParent: $(this).parent()
						}).enable(true);
					} else {
						$(this).select2({
							language: "pt-BR",
							dropdownParent: $(this).parent()
						});
					}
				}
			});
		}

		/* Bootstrap Select Init*/
		if(master.find(".selectpicker").length > 0) {
			master.find('.selectpicker').selectpicker();
		}

		/* Masks */
		if(master.find('.form-number').length > 0) {
			master.find('.form-number').maskMoney();
		}

		/* Date */
		if(master.find(".form-date").length > 0) {
			master.find(".form-date").each(function(e){
				$(this).parent().datetimepicker({
					locale: 'pt-br',
					//useCurrent: false,
					format: $(this).data("format"),
					icons: {
	                    time: 'fa fa-clock-o',
			            date: 'fa fa-calendar',
			            up: 'fa fa-arrow-up',
			            down: 'fa fa-arrow-down',
			            previous: 'fa fa-arrow-left',
			            next: 'fa fa-arrow-right',
			            today: 'fa fa-calendar-check-o',
			            clear: 'fa fa-trash',
			            close: 'fa fa-times'
	                },
	                tooltips: date_tooltips
				}).on('dp.show', function() {
				if($(this).data("DateTimePicker").date() === null)
					$(this).data("DateTimePicker").date(moment());
				}).data("DateTimePicker").date();
			});
		}

		/* Color picker */
		if(master.find(".colorpicker").length > 0) {
			master.find('.colorpicker').each(function(e){
				if(!$(this).data('colorpicker')) {
					$(this).colorpicker();
				}
			});
		}

		/* Switchery Init*/
		var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
		master.find('.js-switch').each(function() {
			new Switchery($(this)[0], $(this).data());
		});

		/* Bootstrap-TouchSpin Init*/
		if(master.find(".vertical-spin").length > 0) {
			master.find(".vertical-spin").TouchSpin({
				verticalbuttons: true,
				verticalupclass: 'ti-plus',
				verticaldownclass: 'ti-minus'
			});
			var vspinTrue = master.find(".vertical-spin").TouchSpin({
				verticalbuttons: true
			});
			if (vspinTrue) {
				master.find('.vertical-spin').prev('.bootstrap-touchspin-prefix').remove();
			}
		}

		/* Map initialization js*/
		try {
			if(master.find(".map-canvas").length > 0) {
				master.find(".map-canvas").each(function(e){
					var elAux = $(this);
					var elSearch = $(this).parent().find(".geo-search");
					var elCoord = $(this).parent().find(".geo-coord");
					var readonly = elCoord.prop("readonly");

					var latIni = -29.167831;
					var lngIni = -51.189884;

					var bUsaGeo = true;

					if(elCoord.val().trim() != ""){
						var posAux = elCoord.val().split(",");
						latIni = posAux[0];
						lngIni = posAux[1];

						bUsaGeo = false;
					}

					var scrollwheel = true;
					if(isMobile || window.innerWidth < 960) {
						scrollwheel = false;
					}

					var mapStyle=[{featureType:"water",elementType:"geometry",stylers:[{color:"#e9e9e9"},{lightness:17}]},{featureType:"landscape",elementType:"geometry",stylers:[{color:"#f5f5f5"},{lightness:20}]},{featureType:"road.highway",elementType:"geometry.fill",stylers:[{color:"#ffffff"},{lightness:17}]},{featureType:"road.highway",elementType:"geometry.stroke",stylers:[{color:"#ffffff"},{lightness:29},{weight:.2}]},{featureType:"road.arterial",elementType:"geometry",stylers:[{color:"#ffffff"},{lightness:18}]},{featureType:"road.local",elementType:"geometry",stylers:[{color:"#ffffff"},{lightness:16}]},{featureType:"poi",elementType:"geometry",stylers:[{color:"#f5f5f5"},{lightness:21}]},{featureType:"poi.park",elementType:"geometry",stylers:[{color:"#dedede"},{lightness:21}]},{elementType:"labels.text.stroke",stylers:[{visibility:"on"},{color:"#ffffff"},{lightness:16}]},{elementType:"labels.text.fill",stylers:[{saturation:36},{color:"#333333"},{lightness:40}]},{elementType:"labels.icon",stylers:[{visibility:"off"}]},{featureType:"transit",elementType:"geometry",stylers:[{color:"#f2f2f2"},{lightness:19}]},{featureType:"administrative",elementType:"geometry.fill",stylers:[{color:"#fefefe"},{lightness:20}]},{featureType:"administrative",elementType:"geometry.stroke",stylers:[{color:"#fefefe"},{lightness:17},{weight:1.2}]}];
					//var mapStyle=[{featureType:"landscape.natural.landcover",elementType:"labels.text.stroke",stylers:[{visibility:"on"}]}];

					var map = new GMaps({
				        div: '#' + elAux.attr("id"),
				    	lat: latIni,
				    	lng: lngIni,

				    	zoom: 16,
						center: new google.maps.LatLng(latIni,lngIni),
						mapTypeControl: true,
						scrollwheel: scrollwheel,
						draggable: true,
						panControl:false,
						scaleControl: false,
						zoomControl: false,
						streetViewControl:false,
						navigationControl: false,
						mapTypeId: google.maps.MapTypeId.ROADMAP,
					    styles: mapStyle
				    });
				    myMaps[elAux.attr("id")] = map;
				    
				    var marker;

				   	google.maps.event.addDomListener(window, "resize", function() {
						var center = map.map.getCenter();
						google.maps.event.trigger(map.map, "resize");
						map.map.setCenter(center);
					});

				    // Adiciona marcador padrão
				    map.removeMarkers();
					marker = map.addMarker({
						lat: map.getCenter().lat(),
						lng: map.getCenter().lng(),
						icon: ROOT_SERVER + ROOT + "_estilo/images/pin-dark.png",
						draggable: !readonly,
						dragend: function(e) {
							updateMapMarker(map, marker, elCoord);
						}
					});

					updateMapMarker(map, marker, elCoord);

					elCoord.on("blur", function(e){
						e.stopPropagation();

						if(elCoord.val().trim() != ""){
							var posAux = elCoord.val().split(",");
							latIni = posAux[0];
							lngIni = posAux[1];

							// Adiciona marcador padrão
							map.removeMarkers();
							marker = map.addMarker({
								lat: latIni,
								lng: lngIni,
								icon: ROOT_SERVER + ROOT + "_estilo/images/pin-dark.png",
								draggable: !readonly,
								dragend: function(e) {
									if(elSearch.length > 0) {
										elSearch.val("");
									}
									updateMapMarker(map, marker, elCoord);
								}
							});

							updateMapMarker(map, marker, $(this));
						}
					});

					if(elSearch.length > 0) {
						elSearch.on("blur", function(e){
							e.stopPropagation();

							if($(this).val() != "") {
								var searchAux = $(this).val();
								GMaps.geocode({
									address: searchAux,
									callback: function(results, status) {
										if (status == 'OK') {
									  		var latlng = results[0].geometry.location;
											map.removeMarkers();
									  		marker = map.addMarker({
									    		lat: latlng.lat(),
									    		lng: latlng.lng(),
									    		icon: ROOT_SERVER + ROOT + "_estilo/images/pin-dark.png",
									    		draggable: !readonly,
												dragend: function(e) {
													if(elSearch.length > 0) {
														elSearch.val("");
													}
													updateMapMarker(map, marker, elCoord);
												}
									  		});

											updateMapMarker(map, marker, elCoord);
										}
									}
								});
							}
						});
					}

					if(bUsaGeo){
						// Tenta pegar o marcador da localização do usuário
					    GMaps.geolocate({
							success: function(position) {
								map.removeMarkers();
								marker = map.addMarker({
									lat: position.coords.latitude,
									lng: position.coords.longitude,
									icon: ROOT_SERVER + ROOT + "_estilo/images/pin-dark.png",
									draggable: !readonly,
									dragend: function(e) {
										if(elSearch.length > 0) {
											elSearch.val("");
										}
										updateMapMarker(map, marker, elCoord);
									}
								});

								updateMapMarker(map, marker, elCoord);
							},
							error: function(error) {
							},
							not_supported: function() {
							},
							always: function() {
							}
						});
					}
				});
			}
		} catch (e) {
        	//console.log(e.message);
      	}

		/*$("input[name='tch1']").TouchSpin({
			min: 0,
			max: 100,
			step: 0.1,
			decimals: 2,
			boostat: 5,
			maxboostedstep: 10,
			postfix: '%'
		});
		$("input[name='tch2']").TouchSpin({
			min: -1000000000,
			max: 1000000000,
			stepinterval: 50,
			maxboostedstep: 10000000,
			prefix: '$'
		});
		$("input[name='tch3']").TouchSpin();

		$("input[name='tch3_22']").TouchSpin({
			initval: 40
		});

		$("input[name='tch5']").TouchSpin({
			prefix: "pre",
			postfix: "post"
		});*/

		/* Multiselect Init*/
		/*$('#pre-selected-options').multiSelect();      
		$('#optgroup').multiSelect({ selectableOptgroup: true });
		$('#public-methods').multiSelect();
		$('#select-all').click(function(){
		$('#public-methods').multiSelect('select_all');
		return false;
		});
		$('#deselect-all').click(function(){
		$('#public-methods').multiSelect('deselect_all');
		return false;
		});
		$('#refresh').on('click', function(){
		$('#public-methods').multiSelect('refresh');
		return false;
		});
		$('#add-option').on('click', function(){
		$('#public-methods').multiSelect('addOption', { value: 42, text: 'test 42', index: 0 });
		return false;
		});*/

		/* Bootstrap switch Init*/
		if(master.find('.bs-switch').length > 0) {
			master.find('.bs-switch').bootstrapSwitch('state', true);
			/*$('#check_box_value').text($("#check_box_switch").bootstrapSwitch('state'));

			$('#check_box_switch').on('switchChange.bootstrapSwitch', function () {
				$("#check_box_value").text($('#check_box_switch').bootstrapSwitch('state'));
			});*/
		}

		atualizaConteudo();
	});
};
/***** Kenny function end *****/

function resetCropModal() {
	$("#cropper-image-src").val("");
	$("#cropper-image-width").val("");
	$("#cropper-image-height").val("");
	$("#cropper-image-quality").val("");
	$("#cropper-modulo").val("");
	$("#cropper-field").val("");
	$("#cropper-randcod").val("");

	$("#crop-label").html("");
}

function verificaMenuButton(iTime){
	if(!iTime || iTime == undefined) {
		iTime = 600;
	}
	setTimeout(function(e){
		var elAux = $(".navbar-fixed-top .toggle-left-nav-btn .fa");
		if(elAux.length > 0) {
			if($(".fixed-sidebar-left").css("margin-left").replace("px","").toString() == "0") {
				elAux.removeClass("fa-bars").addClass("fa-close");
			} else {
				elAux.removeClass("fa-close").addClass("fa-bars");
			}
		}
	},iTime);
}

function prepareScrollorama() {

	var bFromTo = false;
	if(window.innerWidth > 990 && !bScrollorama)
	{
		bFromTo = false;
	}

	bScrollorama = true;
	
	/*// BUILD EXEMPLO
	if($("section#exemplo .box").length > 0) {
		var iDelayAux = 0;
		$("section#exemplo .box").each(function(e){
			var elAux = $(this); 

			var lAux = elAux.data("left");
			if(lAux == undefined && lAux != "") {
				lAux = "0px";
			}
			var tAux = elAux.data("top");
			if(tAux == undefined && tAux != "") {
				tAux = "0px";
			}

			var tween = TweenMax.fromTo(elAux, 1, 
				{css: {opacity: 0, marginTop: tAux, marginLeft: lAux}, delay: 0, immediateRender: true},
				{css: {opacity: 1, marginTop: "0px", marginLeft: "0px"}, delay: iDelayAux, ease: Linear.easeNone, immediateRender: true});
 
			var scene = new ScrollMagic.Scene({triggerElement: elAux, triggerHook: 'onEnter', offset: -500, duration: 1000})
				.setTween(tween)
				.addTo(scrollController);
		});
	} */
}