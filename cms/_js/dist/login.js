AJAXCallback = function(e, AJAX_tipoAux, AJAX_titAux, AJAX_idAux){
	// PADRÃO

	loadieEl.loadie(0.33);
	$("#loadie-percent").html("33%");

	if(atualizaConteudo){
		atualizaConteudo(e);
	} else {
		if(atualizaSections){
			atualizaSections(e);
		}
	}

	if(atualizaBG){
		atualizaBG(e);
	}

	if(atualizaBGInterna){
		atualizaBGInterna(e);
	}

	ajustaScrollers(e);

	if(myScrolls != undefined & myScrolls.length > 0){
		for (var key in myScrolls) {
		    if (key === 'length' || !myScrolls.hasOwnProperty(key)) continue;
		    myScrolls[key].refresh();
		}
	}

	if(scrollController){
		scrollController.update(true);
	}

    $('.dragging').unbind("click").click(function (e) {
        e.preventDefault();
    });

    // OUTROS EVENTOS

	// FORM
 
	if($("#formLogin").length > 0) {
		var options = { 
		    url: ROOT_SERVER + ROOT + "ajax/cms_usuarios/login", 
		    beforeSubmit: function(arr, $form, options) { 
			    // The array of form data takes the following form: 
			    // [ { name: 'username', value: 'jresig' }, { name: 'password', value: 'secret' } ] 
			     
			    // return false to cancel submit                  
			    options.headers = {
			    	'Access-Control-Allow-Origin': '*',
					'Access-Control-Allow-Methods': '*',
					'Access-Control-Allow-Headers': 'api-key,content-type,accept',
					'Access-Control-Allow-Credentials': true,
					'Accept': '*/*'
			   	}

				showLoading(iTimeAuxFade);

		    	if($("#formLogin").hasClass("blocked")) {
		    		e.preventDefault();
		    		e.stopPropagation();
		    		return false; 
		    	} else {
		    		$("#formLogin #btnEnviarLogin span").html("Aguarde...");
					$("#formLogin").addClass("blocked");
				} 
		    },
		    success: function(data) { 
		    	$("#formLogin #btnEnviarLogin span").html("Entrar");
				$("#formLogin").removeClass("blocked");

		        $("#retornoPostLogin").html(data);

		        if (data.search('<!--SUCESSO-->') > -1) {
					$("#formLogin input").each(function(e){
						if($(this).attr("type") == "button" ||
						   $(this).attr("type") == "hidden" ||
						   $(this).attr("type") == "submit")
						{
							// NADA
						}
						else if($(this).attr("type") == "checkbox" ||
						   $(this).attr("type") == "radio")
						{
							//$(this).iCheck('uncheck');
						} else {
							$(this).val("");
						}
					});

					$("#formLogin textarea").val("");

					$("#formLogin select").each(function(e){
						$(this).find("option").eq(0).attr('selected','selected');
						$(this).trigger('update.fs');
					});

					// Redirect
					window.location = ROOT_SERVER + ROOT;
				} else {
					hideLoading(iTimeAuxFade);
				}
		    } 
		}; 
		$("#formLogin").ajaxForm(options);
	}

	// INPUTS

	$("input").each(function(e){
		if($(this).data("pattern") || $(this).data("alias")) {
			var maskPattern = $(this).data("pattern");
			var maskAlias = $(this).data("alias");
			if(maskAlias) {
				$(this).inputmask({alias: maskAlias, placeholder: '', showMaskOnHover: false});
			} else if(maskPattern) {
				$(this).inputmask({mask: maskPattern, placeholder: '', showMaskOnHover: false});
			}
		}

		/*if($(this).attr("type") == "checkbox" ||
		   $(this).attr("type") == "radio") {
			$(this).iCheck({
    			checkboxClass: 'icheckbox',
    			radioClass: 'iradio'
    		});
		}*/
	});

    // SELECTRICS

    if($(".selectric").length > 0) {
    	$(".selectric").selectric({
    		 disableOnMobile: false
    	});
    }
    if($(".selectric-scroll").length > 0) {
   		$(".selectric-scroll").bind( 'mousewheel DOMMouseScroll', function (e) {
    		var e0 = e.originalEvent,
        	delta = e0.wheelDelta || -e0.detail;

    		this.scrollTop += ( delta < 0 ? 1 : -1 ) * 30;
    		e.preventDefault();
    		e.stopPropagation();
		});
	}

	// PARTICLEGROUND

	$('#particles').particleground({
	    dotColor: '#eee',
	    lineColor: '#eee'
	});

    // LOAD

	if($("img").length > 0) {
		var iPass = 0.67 / $("img").length;
		var iTotal = 0.33;
		$("img").imagesLoaded().progress(function( instance, image ) {
		    var result = image.isLoaded ? 'loaded' : 'broken';
		    //console.log( 'image is ' + result + ' for ' + image.img.src );
		    iTotal += iPass;
    		loadieEl.loadie(iTotal);
			$("#loadie-percent").html((Math.ceil(iTotal*100)).toString() + "%");
    		//sleep(100);
		}).always( function( instance ) {
			finishLoad(e);
		});
	}
	else {
    	loadieEl.loadie(0.50);
		$("#loadie-percent").html("50%");
		finishLoad();
	}

	$(window).focus(function(e){
		if(!bFinishLoad) {
			finishLoad(e);
		}
	});
}

function atualizaConteudo(e){
	if(atualizaSections){
		atualizaSections(e);
	}

	// SCROLLERS

	if($(".wrapper.withIScroll").length > 0) {
		$(".wrapper.withIScroll").each(function(e){
			$(this).css("width",$(this).parent().innerWidth().toString() + "px");
			
			if($(this).find(".itPag").length > 0) {
				$(this).find(".itPag").css("width",$(this).innerWidth().toString() + "px");

				/*$(this).find(".itPag").css("height","auto");
				var hMaior = 0;
				$(this).find(".itPag").each(function(e){
					var hAux = $(this).innerHeight();
					if(hAux > hMaior) {
						hMaior = hAux;
					}
				});	
				$(this).find(".itPag").css("height",hMaior.toString() + "px");*/
			}
		});
	}

	ajustaScrollers(e);

	if(myScrolls != undefined & myScrolls.length > 0){
		for (var key in myScrolls) {
		    if (key === 'length' || !myScrolls.hasOwnProperty(key)) continue;
		    myScrolls[key].refresh();
		}
	}

	// OUTROS EVENTOS

	if(atualizaSections){
		atualizaSections(e);
	}
}

function finishLoad(e) {

    $('.dragging').unbind("click").click(function (e) {
        e.preventDefault();
    });

	// SCROLLER

	if($('.wrapper.withIScroll').length > 0){
		$('.wrapper.withIScroll').each(function(e){
			var elAux = $(this);
			var arrAux = $(this).attr("id").split("-");
			var idAux = arrAux[1].toString();// + "-" + arrAux[2].toString();

			myScrolls[idAux] = new IScroll('#wrapper-' + idAux, {
				snap: true,
				momentum: false,
				hScrollbar: false,
				vScrollbar: false,
				hScroll: true,
				vScroll: false,
				scrollX: true,
				scrollY: false,
				wheelAction : 'none',
				eventPassthrough: true,
				preventDefault: false
			});
			
			myScrolls[idAux].on('scrollStart',function(e){
            	$('#wrapper-' + idAux + ' img').addClass("dragging");
            	//$('#wrapper-' + idAux + ' a').addClass("dragging");
        	});
			
			myScrolls[idAux].on('scrollEnd',function(e){
            	$('#wrapper-' + idAux + ' img').removeClass("dragging");
            	//$('#wrapper-' + idAux + ' a').removeClass("dragging");
            	
				if($("#nav-" + idAux + " .liNav").length > 0){
					$("#nav-" + idAux + " .liNav").removeClass("ativo");
					$("#nav-" + idAux + " .liNav").eq(this.currentPage.pageX).addClass("ativo");
				}

				verificaSetas(idAux,this.currentPage.pageX);
			});

			if($("#nav-" + idAux + " .liNav").length > 0){
				$("#nav-" + idAux + " .liNav").removeClass("ativo");
				$("#nav-" + idAux + " .liNav").eq(0).addClass("ativo");
			}

			if($("#nav-" + idAux + " .liNav").length > 0){
				if($('#wrapper-' + idAux + ' .itPag').length > 0){
					if($('#wrapper-' + idAux + ' .itPag').length == 1){
						$("#nav-" + idAux + "").css("visibility","hidden");
					}

					$("#nav-" + idAux + " .liNav a").unbind("click").click(function(e){
						var iIndexAux = $("#nav-" + idAux + " .liNav").index($(this).parent());

						myScrolls[idAux].goToPage(iIndexAux,0);
					});
				}
			}

			$(".setas #btnAnt-" + idAux + "").unbind("click").click(function(e){
				myScrolls[idAux].prev();
			});
			$(".setas #btnProx-" + idAux + "").unbind("click").click(function(e){
				myScrolls[idAux].next();
			});

			ajustaScrollers(e);

			verificaSetas(idAux,myScrolls[idAux].currentPage.pageX);

			if(myScrolls[idAux]){
				myScrolls[idAux].refresh();
			}
		});
	}

	atualizaConteudo(e);

	if(!AJAX_bLoading) {
		loadieEl.loadie(1.0);
		$("#loadie-percent").html("100%");
		hideLoading(iTimeAuxFade);
	}

	// SCROLLORAMA

	if(scrollController) {
		setTimeout(function(e){
			scrollController.update(true);
		},iTimeAuxFade);

		scrollController.update(true);
	}

	bFinishLoad = true;
}