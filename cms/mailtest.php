<?php 

include_once("config.php");

@ini_set('display_errors', 1);
@ini_set('error_reporting', E_ALL ^ E_NOTICE ^ E_DEPRECATED ^ E_WARNING ^ E_STRICT);

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
//require_once (ROOT_CMS . "classes/vendor/PHPMailerAutoload.php");
	
require 'vendor/autoload.php';

//START DEBUG
if(isset($_REQUEST["uz"])) {
	$_CONFIG["servidor_smtp"] = "mail.uzzye.com";
	$_CONFIG["porta_smtp"] = "587";
	$_CONFIG["usuario_email"] = "mauricio@uzzye.com";
	$_CONFIG["senha_email"] = "DuendeY2014";
	$_CONFIG["secure_smtp"] = "";
} else if(isset($_REQUEST["custom"])) {
	$_CONFIG["servidor_smtp"] = $_REQUEST["servidor_smtp"];
	$_CONFIG["porta_smtp"] = $_REQUEST["porta_smtp"];
	$_CONFIG["usuario_email"] = $_REQUEST["usuario_email"];
	$_CONFIG["senha_email"] = $_REQUEST["senha_email"];
	$_CONFIG["secure_smtp"] = $_REQUEST["secure_smtp"];
}
//END DEBUG

$mail = new PHPMailer(false);

$mail->SMTPDebug = SMTP::DEBUG_SERVER;
$mail->CharSet = 'UTF-8';
$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = $_CONFIG["servidor_smtp"];  // Specify main and backup server
$mail->Port = $_CONFIG["porta_smtp"];
$mail->SMTPAutoTLS = false;

if(trim($_CONFIG["usuario_email"]) != "") {
	$mail->SMTPAuth = true;                               // Enable SMTP authentication
	$mail->Username = $_CONFIG["usuario_email"];                            // SMTP username
	$mail->Password = $_CONFIG["senha_email"];                           // SMTP password
	if(isset($_CONFIG["secure_smtp"])) {
		$mail->SMTPSecure = $_CONFIG["secure_smtp"];
	} else {
		//$mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted
	}
}

$de = EMAIL_DEFAULT_FROM;
$para = "mausalamon@gmail.com";
if(isset($_REQUEST["to"])) {
	$para = $_REQUEST["to"];
}

$mail->From = $de;
$mail->Sender = $de;
$mail->FromName = $_CONFIG["nome_default_from"];

$mail->WordWrap = 50;                                 // Set word wrap to 50 characters
//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
$mail->isHTML(true);                                  // Set email format to HTML
$mail->addCustomHeader('MIME-Version: 1.0');
$mail->addCustomHeader('Content-Type: text/html; charset=UTF-8');

$mail->Subject = "Uz Test"; //'Here is the subject';
$mail->Body    = "<img src=\"" . ROOT_SERVER . ROOT . "img/logo_email.png\" />"; //'This is the HTML message body <b>in bold!</b>';
$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

$mail->ClearAllRecipients();
$mail->addAddress($para); //'ellen@example.com');               // Name is optional
$mailSents = $mail->send();

$mail->ClearAllRecipients();
$mail->ClearAttachments();

die();

?>