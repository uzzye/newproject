<?php

include_once("config.php");

ob_clean();

//<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7" xmlns="http://www.w3.org/1999/xhtml" lang="<?php echo $_CONFIG["lang"]; ?>"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie10 lt-ie9 lt-ie8" xmlns="http://www.w3.org/1999/xhtml" lang="<?php echo $_CONFIG["lang"]; ?>"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie10 lt-ie9" xmlns="http://www.w3.org/1999/xhtml" lang="<?php echo $_CONFIG["lang"]; ?>"><![endif]-->
<!--[if IE 9]><html class="no-js lt-ie10" xmlns="http://www.w3.org/1999/xhtml" lang="<?php echo $_CONFIG["lang"]; ?>"><![endif]-->
<!--[if gt IE 9]><!--><html class="no-js ie-10" xmlns="http://www.w3.org/1999/xhtml" lang="<?php echo $_CONFIG["lang"]; ?>" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml"><!--<![endif]-->
<head prefix="og: http://ogp.me/ns#">
	<title>UzCMS 3 | Login</title>
	<link rel="shortcut icon" href="<?php echo ROOT_SERVER . ROOT; ?>favicon.ico?<?php echo Date("YmdHis"); ?>" type="image/x-icon" />
	<link rel="icon" href="<?php echo ROOT_SERVER . ROOT; ?>favicon.png" />
	<link rel="image_src" href="<?php echo ROOT_SERVER . ROOT; ?>_estilo/images/logo_cms.png" />
	<meta property="og:image" content="<?php echo ROOT_SERVER . ROOT; ?>_estilo/images/logo_cms.png" />
	<meta property="og:image:type" content="image/png">
	<meta property="og:image:width" content="470">
	<meta property="og:image:height" content="246">
	<link rel="apple-touch-icon" href="<?php echo ROOT_SERVER . ROOT; ?>_estilo/images/touchIcon.png" />
	<!-- MetaTags -->
	<meta name="theme-color" content="<?php echo $projectColors["theme"]; ?>" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Cache-Control" content="max-age=0, public" />
	<meta name="author" content="Uzzye" />
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1.5" />
	<meta name="apple-mobile-web-app-capable" content="yes" /> 
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="resource-types" content="document" /> 
	<meta name="classification" content="Internet" /> 
	<meta name="abstract" content="UzCMS. Gerenciador de conteúdo online. Uzzye Com. Dig. Ltda." /> 
	<meta name="description" content="UzCMS. Gerenciador de conteúdo online. Uzzye Com. Dig. Ltda." /> 
	<meta name="keywords" content="uzcms, cms, gerenciador, conteudo, content, inbound, uzzye, caxias do sul, caxias, serra, gaucha, rs, rio grande do sul, brasil, mauricio, salamon, mausalamon"/> 
	<meta name="robots" content="NONE" /> 
	<meta name="distribution" content="Local" /> 
	<meta name="rating" content="General" /> 
	<meta name="author" content="Uzzye" /> 
	<!-- Geolocalização -->
	<link rel="alternate" hreflang="pt" href="<?php echo ROOT_SERVER . ROOT; ?>" />
	<?php if(trim($_CONFIG["geolocalizacao"]) <> ""){?>
	<?php echo $_CONFIG["geolocalizacao"]; ?>
	<?php } ?>
	<!-- JavaScript -->
	<script language="javascript">	
		var ROOT_CMS = "<?php echo ROOT_CMS; ?>";
		var ROOT_SITE = "<?php echo ROOT_SITE; ?>";
		var ROOT_SERVER = "<?php echo ROOT_SERVER; ?>";
		var ROOT = "<?php echo ROOT; ?>";	
		var UPLOAD_FOLDER = "<?php echo UPLOAD_FOLDER; ?>";
	</script>
    <?php if(file_exists(ROOT_CMS . "lang/" . $_CONFIG["lang"] . ".js.php")) {
		include_once(ROOT_CMS . "lang/" . $_CONFIG["lang"] . ".js.php");
	} ?>
	<script type="text/javascript" language="javascript" src="<?php echo ROOT_SERVER . ROOT; ?>_js/plugins/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo ROOT_SERVER . ROOT; ?>_js/plugins/jquery-migrate-1.2.1.min.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo ROOT_SERVER . ROOT; ?>_js/core/vars.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo ROOT_SERVER . ROOT; ?>_js/vendor/bootstrap.min.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo ROOT_SERVER . ROOT; ?>_js/vendor/modernizr-latest.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo ROOT_SERVER . ROOT; ?>_js/plugins/moment-with-locales.min.js"></script>
		
	<!-- CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_SERVER . ROOT; ?>_estilo/uz.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_SERVER . ROOT; ?>_estilo/login.css" />
    <!--[if IE 7]>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_SERVER . ROOT; ?>_estilo/iehacks/ie7hacks.css" />
    <![endif]-->
    <!--[if IE]>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_SERVER . ROOT; ?>_estilo/iehacks/iehacks.css" />
    <style type="text/css">
        body { behavior:url(<?php echo ROOT_SERVER . ROOT; ?>_estilo/iehacks/csshover3.htc); }
    </style>
    <![endif]-->
</head>
<body>
    <!--Preloader-->
    <!-- <div class="preloader-it">
        <div class="la-anim-1"></div>
    </div> -->
    <!--/Preloader-->

    <div id="loader-wrapper">
        <div id="loader"></div>
     
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
     
    </div>
    
	<div class="pageSize"></div>
	<div class="page wrapper">

		<div class="login center middle">
		
			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<h1>
						<a class="linkFull" href="<?php echo ROOT_SERVER . ROOT . ROOT_SITE; ?>" title="<?php echo $_CONFIG["titulo"]; ?>">
							<img src="<?php echo ROOT_SERVER . ROOT; ?>_estilo/images/logo_cms.png" border="0" />
						</a>
					</h1>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form class="form-horizontal" role="form" name="formLogin" id="formLogin" method="post" action="javascript:void(0);">	
										<div class="form-group">
											<label class="control-label" id="retornoPostLogin">
												<?php echo get_lang("_AUTENTICACAO"); ?>
											</label>
										</div>
										<div class="form-group">
											<div class="input-group">
							                    <span class="input-group-addon"><i class="icon-user"></i></span>
							                    <input id="login_usuario" type="text" class="form-control" name="usuario" required value="" placeholder="<?php echo get_lang("_USUARIO"); ?>">
							                </div>
											<div class="input-group">
							                    <span class="input-group-addon"><i class="icon-lock"></i></span>
							                    <input id="login_senha" type="password" class="form-control" name="senha" required value="" placeholder="<?php echo get_lang("_SENHA"); ?>">
							                </div>
							            </div>
						                <div class="form-actions mt-10">
											<div class="row">
												<div class="col-md-12">
													<div class="row">
														<button type="submit" id="btnEnviarLogin" class="col-xs-6 col-sm-4 btn btn-info btn-icon left-icon"> <i class="fa fa-key"></i> <span><?php echo get_lang("_ENTRAR"); ?></span></button>
															
														<div class="col-xs-6 ml-2 mt-5">
															<div class="checkbox checkbox-success">
																<input id="login_lembrar" name="lembrar" type="checkbox" value="1">
																<label for="login_lembrar"><?php echo get_lang("_LEMBRAR_ACESSO"); ?></label>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="sign">
				<a href="http://www.uzzye.com" target="_blank" title="Uzzye">
					<img src="<?php echo ROOT_SERVER . ROOT; ?>_estilo/images/logo-uzzye-sm.png" border="0" alt="Uzzye"/>
				</a>	
                <p class="font-12">UzCMS 3 2019 &copy; Uzzye. All rights reserved.</p>
                <!-- Customizado de Kenny - Pampered by Hencework -->	
			</div>
				
		</div>
	</div>

	<div id="particles"></div>
</body>

<!-- JS -->
<script type="text/javascript" language="javascript" src="https://maps.google.com/maps/api/js?sensor=true&key=AIzaSyDLgXx-Xv5YfCPYrCACeZQ8qD8kT-ozqGw"></script>
<!--[if lt IE 9]>
<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<script type="text/javascript" language="javascript" src="<?php echo ROOT_SERVER . ROOT; ?>_js/dist/uz.min.js"></script>

<script type="text/javascript" language="javascript" src="<?php echo ROOT_SERVER . ROOT; ?>_js/plugins/jquery.particleground.min.js"></script>

<script type="text/javascript" language="javascript" src="<?php echo ROOT_SERVER . ROOT; ?>_js/dist/login.min.js"></script>

<script type="text/javascript">
	loaded();
</script>

</html>
