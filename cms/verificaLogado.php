<?php

// Verifica sessão ou tentativa de login
if((!isset($_SESSION["btLogin"]) || $_SESSION["btLogin"] == false || $_SESSION["ROOT_SESSION"] <> ROOT) &&
   $_REQUEST["modulo"] != "cms_usuarios" &&
   $_REQUEST["acao"] != "login"
  ) {

	// Verifica cookies
	$btLogin = false;
	if(intval($_COOKIE[$projectName . "_idUser"]) > 0) {
		carrega_classe("cms_usuarios");
		$user = new cms_usuarios();
		$btLogin = $user->login(intval($_COOKIE[$projectName . "_idUser"]));
	}

	if(!$btLogin) {
		// Se falhou em todas, garante o logout e mandar para autenticação
		$_SESSION["btLogin"] = false;
		header ("location: " . ROOT_SERVER . ROOT . "login");
		die();
	}
}

?>