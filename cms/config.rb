require 'compass/import-once/activate'
require 'ceaser-easing'

base_path = '.'
sass_dir = '_sass'
css_dir =  '_estilo'
fonts_dir = '_estilo/fonts'
http_fonts_dir = 'newproject/cms/_estilo/fonts'
images_dir = '_estilo/images'
http_images_dir= 'newproject/cms/_estilo/images'
javascripts_dir = '_js'

#add_import_path "."
#add_import_path "angularapp/components"
#add_import_path "angularapp/pages"