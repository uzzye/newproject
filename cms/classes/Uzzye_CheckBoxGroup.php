<?php

class Uzzye_CheckBoxGroup extends Uzzye_Field
{		
	public $checked_value;
	public $fixed_pos;
	public $array_options;

    function __construct($name = "", $id = "", $label = "", $default_value = "", $readonly = "", $style_class = "form-CheckBox", $li_class = "", $sub_label = "")
	{
		$this->type = "checkbox";
		$this->fixed_pos = false;
		
		parent::__construct($name, $id, $label, $default_value, $readonly, $style_class, $li_class, $sub_label);
	}
	
	function get_display_field()
	{
		$result = "";
		$result .= $this->ini_field_set();
		$result .= $this->get_display_label();		
		
		$iCount = 0;
		foreach($this->array_options as $option) {	
			$result .= "<div class=\"checkbox checkbox-custom\">
				<input class=\"" . $this->style_class. "\" type=\"checkbox\" id=\"" . ($this->id) . "\" name=\"" . ($this->name) . "\" value=\"" . ($option[0]) . "\" ";
			if($this->checked_value == $option[0])
			{
				$result .= " checked ";
			}
			if($this->readonly)
			{
				$result .= " readonly onclick=\"this.checked = !this.checked;\" ";
			}
			if($this->required)
			{
				$result .= " required='required' ";
			}
			$result .= " />
				<label for=\"" . ($this->id) . "\">" . $option[1] . "</label>
			</div>";
			$iCount++;
		}
		
		$result .= $this->end_field_set();
		
		return $result;
	}
	
	function get_db_value()
	{
		return intval($this->value);
	}	
	
	function set_value($valor)
	{
		$this->value = intval($valor);
	}

	function set_options($array) {
		$this->array_options = $array;
	}
}

?>