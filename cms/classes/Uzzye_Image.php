<?php

class Uzzye_Image extends Uzzye_Field
{	
	public $sub_type;
	public $src;
	public $border;
	public $width;
	public $height;
	
    function __construct($name = "", $id = "", $label = "", $default_value = "", $readonly = "", $style_class = "form-File", $li_class = "", $sub_label = "")
	{
		$this->type = "image";
		$this->border = "0";
		
		parent::__construct($name, $id, $label, $default_value, $readonly, $style_class, $li_class, $sub_label);
	}
	
	function get_display_field()
	{
		global $modulo;
		
		$result = "";		
		$result .= $this->ini_field_set();
		$result .= $this->get_display_label();		

		$imgs_aux = "";

		if($this->base64) {
			$imgs_aux = "<img src=\"data:image/png;base64," . $this->src . "\" border=\"" . $this->border . "\" id=\"" . $this->id . "\"";
			if(trim($this->width) != ""){
				$imgs_aux .= " width=\"" . $this->width . "\"";
			}
			if(trim($this->height) != ""){
				$imgs_aux .= " height=\"" . $this->height . "\"";
			}
			$imgs_aux .= "/>";
		} else if(trim($this->src) != ""){
			$imgs_aux = "<img src=\"" . $this->src . "\" border=\"" . $this->border . "\" id=\"" . $this->id . "\"";
			if(trim($this->width) != ""){
				$imgs_aux .= " width=\"" . $this->width . "\"";
			}
			if(trim($this->height) != ""){
				$imgs_aux .= " height=\"" . $this->height . "\"";
			}
			$imgs_aux .= "/>";
		}

		
		if(trim($imgs_aux) <> "")
		{
			$result .= "<div class=\"row\">
				<div class=\"col-sm-12\">";
			$result .= $imgs_aux;
			$result .= "</div>
			</div>";
		}
		
		$result .= $this->end_field_set();
		return $result;
	}
	
	function get_db_value()
	{
		return ($this->value);
	}	
	
	function set_value($valor)
	{
		$this->value = ($valor);
		$this->src = $this->value;
	}
}

?>