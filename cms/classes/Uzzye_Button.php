<?php

class Uzzye_Button extends Uzzye_Field
{	
	public $onclick;
	public $icon;

	function __construct($name = "", $id = "", $label = "", $default_value = "", $readonly = "", $style_class = "btn-default", $li_class = "", $sub_label = "")
	{	
		$this->type = "button";
		//$this->style_class = "btn-info";
		
		parent::__construct($name, $id, $label, $default_value, $readonly, $style_class, $li_class, $sub_label);
	}
	
	function get_display_field()
	{
		$result = "";
		if(!$this->readonly)
		{
			if(trim($this->label) == "") {
				$this->li_class .= " mt-21 ";
			}
			$result .= $this->ini_field_set();
			$result .= $this->get_display_label();		
				
			$result .= "<button type=\"button\" class=\"btn " . $this->style_class. "\" id=\"" . ($this->id) . "\" name=\"" . ($this->name) . "\"";

			if($this->readonly)
			{
				$result .= " readonly";
			}	
			if($this->focus)
			{
				$result .= " autofocus='true' ";
			}
			$result .= ">";
			if(trim($this->icon) != "") {
				$result .= "<i class=\"" . ($this->icon) . " pr-0\"></i>";
			}
			if(trim($this->value) != "") {
				$result .= "<span class=\"btn-text\">" . ($this->value) . "</span>";
			}
			$result .= "</button>";
			
			$result .= $this->end_field_set();

			if(!$this->readonly) {
			?>
			<script language='javascript'>

				"use strict"; 

				$(document).ready(function(e) {
					$("#<?php echo ($this->id); ?>").on("click", function(e){
						<?php echo $this->onclick; ?>
					});
				});
			</script>
			<?php
			}
		}
		
		return $result;
	}
	
	function get_db_value()
	{
		global $db;
		return output_decode($db->escape_string($this->value));
	}	
	
	function set_value($valor)
	{
		$this->value = htmlspecialchars(stripslashes(get_output($valor)));
	}
}

?>