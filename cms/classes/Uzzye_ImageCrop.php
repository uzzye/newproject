<?php

class Uzzye_ImageCrop extends Uzzye_Field
{	
	public $array_crop_fields; // v 2.0 fallback
	public $sub_type;
	public $crops; // 0 = field, 1 = label, 2 = aspect, 3 = width, 4 = height, 5 = quality
	public $crop_width;
	public $crop_height;
	public $crop_quality;
	public $crop_extension;
	public $allow_crop;
	public $allow_select;
	public $clone;
	
    function __construct($name = "", $id = "", $label = "", $default_value = "", $readonly = "", $style_class = "form-Text", $li_class = "", $sub_label = "")
	{
		$this->type = "image-crop";

		$this->allow_crop = true;
		$this->allow_select = true;
		$this->clone = false;
		
		parent::__construct($name, $id, $label, $default_value, $readonly, $style_class, $li_class, $sub_label);
		
		$this->style_class = "form-File";
	}
	
	function get_display_field()
	{
		global $modulo, $acao, $db, $_CONFIG;

		$randcod = $_SESSION["GLOBAL_RANDOM_KEY"];

		if(!stristr($this->name, "crop")) {
			$this->name .= "_crop";
		}
		if(!stristr($this->id, "crop")) {
			$this->id .= "_crop";
		}

		$this->label .= "<br/><small class=\"pl-0\">" . get_lang("_TAMANHO_ARQUIVO_RECOMENDADO") . "</small>";
		
		$result = "";
		$result .= $this->ini_field_set();
		$result .= $this->get_display_label();

		// v2.0 fallback
		if(is_array($this->array_crop_fields) && sizeof($this->array_crop_fields) > 0){
			foreach($this->array_crop_fields as $olditem) {
				$this->crops[sizeof($this->crops)] = array($olditem[0],$olditem[1],0,$olditem[3],$olditem[4],$olditem[5]);
			}
		}

		// Monta campo de upload
		$uploadAux = UPLOAD_FOLDER;
		$objAux = null;
		if(file_exists(ROOT_CMS . "classes/models/" . $modulo."_model.php"))
		{
			require_once(ROOT_CMS . "classes/models/" . $modulo."_model.php");
			$classe = $modulo."_model";
			$objAux = new $classe();
			$objAux->inicia_dados();
			$objAux->set_var("id",intval($_REQUEST["id"]));
			$objAux->carrega_dados();

			if(@trim($objAux->upload_folders[str_replace("_crop","",$this->id)]) != "") {
				$uploadAux = $objAux->upload_folders[str_replace("_crop","",$this->id)];
			} else if(trim($objAux->upload_folder) != "") {
				$uploadAux = $objAux->upload_folder;
			}
		}
		$folderAux = $uploadAux;

		// Pega imagem padrão da galeria
		if(trim($this->value) != "") {
			$value = $uploadAux . $this->value;
		}
		$sqlCmd = "SELECT arquivo FROM " . DBTABLE_CMS_IMAGENS . " WHERE modulo_registro = \"" . $modulo . "\" AND id_registro = " . intval($_REQUEST["id"]) . " ";		
		if(is_array($this->crops) && sizeof($this->crops) > 0) {
			$sqlCmd .= " AND campo_registro = \"" . $this->crops[0][0] . "\" ";
		}
		$resCmd = $db->exec_query($sqlCmd);
		$bFoundDefault = false;
		if($db->num_rows($resCmd) > 0) {
			if(trim($db->result_field($resCmd,0,"arquivo")) != "") {
				$value = UPLOAD_FOLDER . "original/" . $db->result_field($resCmd,0,"arquivo");
				$bFoundDefault = true;
			}
		}
		if(!file_exists($value)) {
			$value = "";
		}

		//$preview = $value;
		if($this->clone) {
			$arrName = explode("/",$value);
			$newvalue = UPLOAD_FOLDER . "temp/clone_" . $randcod . "_" . $arrName[sizeof($arrName)-1];
			copy($value, $newvalue);
			$value = $newvalue;
			$preview = $value;
		}
		//else
		{
			if(is_array($this->crops) && (sizeof($this->crops) == 1 || !$bFoundDefault) && $objAux != null) {
				if(trim($objAux->get_var($this->crops[0][0])) != "") {
					//$value = $uploadAux . $objAux->get_var($this->crops[0][0]);
					$preview = $uploadAux . $objAux->get_var($this->crops[0][0]);
				}
			}
		}
		
		//if(!$this->readonly)
		{
			$result .= "<input type=\"file\" class=\"image-file " . $this->style_class. "\" id=\"" . ($this->id) . "\" name=\"" . ($this->name) . "\" data-allowed-file-extensions=\"jpg jpeg png gif bmp tiff jiff webp svg\" data-default-file=\"" . $preview . "\" data-folder=\"" . $uploadAux . "\" data-file=\"" . $value . "\" data-modulo=\"" . $modulo. "\" data-id=\"" . intval($_REQUEST["id"]) . "\" ";
			if($this->required && ($acao != "editar" || trim($preview) == ""))
			{
				//$result .= " required='required' ";
			}
			if($this->readonly)
			{
				$result .= " readonly data-show-remove=\"false\" disabled=\"disabled\" ";
			}
			$result .= "/>";
		} 

		if(!$this->readonly)
		{
			$result .= "<a class=\"btn-select-img\" data-toggle=\"modal\" data-target=\"#responsive-modal_" . $this->id . "_" . $randcod . "\">
				<button type=\"button\" data-toggle=\"tooltip\" title=\"Selecionar no Banco de Imagens\" class=\"btn btn-success btn-icon-anim btn-square\"><i class=\"icon-picture\"></i>" . 
				//"<span class=\"btn-text\">" . get_lang("_SELECIONAR") . "</span>" .
				"</button>
			</a>";
		
			?>
			<!-- /.modal -->
			<div id="responsive-modal_<?php echo $this->id; ?>_<?php echo $randcod; ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;" data-url="<?php echo ROOT_SERVER . ROOT; ?>image_select.php">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							<h5 class="modal-title"><?php echo get_lang("_BANCO_IMAGENS"); ?></h5>
						</div>
						<div class="modal-body">
							<div class="col-sm-12">
								<?php
								// MODAL DATA
								echo get_lang("_AGUARDE");
								?>
							</div>
						</div>
						<div class="modal-footer">
							<?php /*<button type="button" class="btn btn-success btn-submit btn-anim"><i class="icon-rocket"></i> <span class="btn-text"><?php echo get_lang("_SELECIONAR"); ?></span></button>*/ ?>
							<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo get_lang("_FECHAR"); ?></button>
						</div>
					</div>
				</div>
			</div>

			<script language='javascript'>
				/*Responsive Datatable Init*/

				"use strict"; 

				$(document).ready(function(e) {

					// MODAL

					/*if($(".btn-select-img").length > 0) {
						$(".btn-select-img").on("click", function(e){
							e.preventDefault();
							e.stopPropagation();
						});
					}*/
					
					if( $('#responsive-modal_<?php echo $this->id; ?>_<?php echo $randcod; ?>').length > 0 ){
						var modalAux = $('#responsive-modal_<?php echo $this->id; ?>_<?php echo $randcod; ?>');
						$('#responsive-modal_<?php echo $this->id; ?>_<?php echo $randcod; ?>').on('show.bs.modal', function (event) {
							showLoading(iTimeAuxFade);

							$.ajax({ 
		                        url: modalAux.data("url"),
		                        success: function(data) {
		                        	modalAux.find(".modal-body").html(data);

		                        	if(modalAux.find(".imagem-select").length > 0) {
		                        		modalAux.find(".imagem-select").on("click", function(e){
		                        			e.preventDefault();
		                        			e.stopPropagation();

		                        			modalAux.find(".imagens-select .imagem-select.active").removeClass("active");
		                        			$(this).addClass("active");

		                        			selectImageToCrop("<?php echo ($this->id); ?>", $(this).find("img").attr("src"));
		                        		});
		                        	}

		                        	kenny(modalAux);

		                        	hideLoading(iTimeAuxFade);

		                        	// FORM

									modalAux.find(".btn-submit").unbind("click").click(function(e) {
										e.preventDefault();
										e.stopPropagation();

										// Selecionou imagem

										selectImageToCrop("<?php echo ($this->id); ?>", modalAux.find(".imagem-select.active").find("img").attr("src"));
									});
		                        }
	                       	});
						});
						$('#responsive-modal_<?php echo $this->id; ?>_<?php echo $randcod; ?>').on('hide.bs.modal', function (event) {
							modalAux.find(".modal-body").html("<div class=\"col-sm-12\"><?php echo get_lang("_AGUARDE"); ?></div>");
						});
					}				
				});

				function selectImageToCrop(iid, src) {
					var input = $("#" + iid);
					if(input.length > 0 && src && src != "") {
			            if(input.closest(".form-group").find(".btn-crop").length > 0) {
			           		input.closest(".form-group").find(".btn-crop").each(function(e){
			           			$(this).find(".image-value").val(src);
			           			$(this).find(".image-original-value").val(src);
			           		});
			            }

						input.data("default-file", src);
						input.data("data-file", src);
						input.data("dropify").resetPreview();
						input.data("dropify").setPreview(true, src);
						input.data("dropify").clearButton.hide();
					}

					if($('#responsive-modal_<?php echo $this->id; ?>_<?php echo $randcod; ?>').length > 0) {
						$('#responsive-modal_<?php echo $this->id; ?>_<?php echo $randcod; ?>').modal('hide');
					}
				}
			</script>
			<?php
		}
		
		if($this->allow_crop && !$this->readonly) {
			foreach($this->crops as $crop) {

				$cropValue = $uploadAux . $objAux->get_var($crop[0]);
				if($this->clone) {
					$arrName = explode("/",$cropValue);
					$newvalue = UPLOAD_FOLDER . "temp/clone_" . $randcod . "_" . $arrName[sizeof($arrName)-1];
					copy($cropValue, $newvalue);
					$cropValue = $newvalue;
				}

				if(!$bFoundDefault && trim($objAux->get_var($crop[0])) != "") {
					$value = $cropValue;
				}

				if(trim($crop[5]) == "") {
					$crop[5] = "100";
				}

				$result .= "<span class=\"btn-crop\"
				data-modulo=\"" . $modulo . "\"
				data-field=\"" . $crop[0] . "\"
				data-label=\"" . $crop[1] . "\"
				data-aspect=\"" . $crop[2] . "\"
				data-width=\"" . $crop[3] . "\"
				data-height=\"" . $crop[4] . "\"
				data-quality=\"" . $crop[5] . "\"
				data-toggle=\"tooltip\" title=\"";
				if(trim($objAux->get_var($crop[0])) != "") {
					$result .= "<img src='" . $cropValue . "' border='0' width='100%' />";
				}
				$result .= "\">
					<button type=\"button\" class=\"btn btn-default btn-icon\"
						data-target=\"#cropper-modal\"
						data-backdrop=\"static\" 
						data-toggle=\"modal\"
					><i class=\"icon-crop\"></i>" . 
						" <span class=\"btn-text\">" . $crop[1] . "</span>" .
					"</button>
					<input type=\"hidden\" class=\"image-value\" id=\"image-value-" . $modulo . "_" . $crop[0] . "\" name=\"" . $crop[0] . "\" value=\"" . $cropValue . "\" />
					<input type=\"hidden\" class=\"image-original-value\" id=\"image-original-value-" . $modulo . "_" . $crop[0] . "\" name=\"" . $crop[0] . "_original\" value=\"" . $value . "\" />
					<input type=\"hidden\" id=\"image-cluster-" . $modulo . "_" . $crop[0] . "\" name=\"" . $crop[0] . "_cluster\" value=\"" . $this->id . "\" />
				</span>";
			}
		}

		if(trim($value) <> "" && file_exists(trim($value)))
		{
			$result .= "<a class=\"btn-download\" href=\"#\" data-url=\"" . ROOT_SERVER . ROOT . "file_download.php?file=" . str_replace($folderAux,"",$value) . "&folder=" . rawurlencode($folderAux) . "\"data-toggle=\"tooltip\" title=\"Download da Imagem Original\">
				<button type=\"button\" class=\"btn btn-info btn-icon-anim btn-square\"><i class=\"icon-cloud-download\"></i>" . 
				//"<span class=\"btn-text\">" . get_lang("_VISUALIZAR") . "</span>" .
				"</button>
			</a>";
		} else {
			//$result .= "<span class=\"help-block\">" . get_lang("_NENHUMARQUIVOENVIADO") . "</span>";
		}
		
		$result .= $this->end_field_set();
		return $result;
	}
	
	function get_db_value()
	{
		return ($this->value);
	}	
	
	function set_value($valor)
	{
		$this->value = ($valor);
	}
}

?>