<?php

class DB_MySQLi {

    public $host;
    public $user;
    public $senha;
    public $dbase;

    public $query;
    public $link_conn;
    public $resultado;
    public $persistent;
    
    function __construct(){
		// Instancia o Objeto
		$this->persistent = false;
    }

    function conecta(){
    	global $_CONFIG;

    	if(!isset($_CONFIG["noSQL"]) || $_CONFIG["noSQL"] === false) {
	    	if($this->persistent) {
	        	$this->link_conn = mysqli_connect("p:".$this->host,$this->user,$this->senha);
	        } else {
	        	$this->link_conn = mysqli_connect($this->host,$this->user,$this->senha);
	        }
	        if(!$this->link_conn){
	            echo "Falha ao conectar no banco de dados.<br /><b>Erro: ".mysqli_error($this->link_conn)."</b>";
				die();
	        }elseif(!mysqli_select_db($this->link_conn, $this->dbase)){
	            echo "Falha ao selecionar o banco de dados.<br /><b>Erro: ".mysqli_error($this->link_conn)."</b>";
				die();
	        }
	    } else {
	    	$this->link_conn = null;
	    }
    }
	
	function desconecta(){
        return mysqli_close($this->link_conn);
    }

    function exec_query($query, $retorna_id = false){
    	global $_CONFIG;

    	if(!isset($_CONFIG["noSQL"]) || $_CONFIG["noSQL"] === false) {
	        $this->conecta();
	        $this->query = $query;
	        if($this->resultado = mysqli_query($this->link_conn, $this->query)){
				$result = $this->resultado;
				if($retorna_id)
				{
					$result = array($result,intval(mysqli_insert_id($this->link_conn)));				
				}
	            $this->desconecta();

				//var_dump("SUCCESS: " . $query); echo "<br/><br/>";

	            return $result;
	        }else{
	            //echo "Falha ao executar a query: ".$query."<br />Erro: <b>".mysqli_error($this->link_conn)."</b>";
				//die();
				
				//var_dump("FAIL: " . $query); echo "<br/><br/>";
	            
	            $this->desconecta();

	            return null;
	        } 
	    } else {
	    	return null;
	    }       
    }
	
	function num_rows($result){
        //if($this->resultado = $result){
            return @mysqli_num_rows($result);
        /*}else{
            echo "Falha ao gerar o retorno ".$result."<br />Erro: <b>".mysqli_error()."</b>";
			die();
        }*/
    }
	
	function result_field($result,$i,$nome)
	{		
	    $numrows = mysqli_num_rows($result); 
	    if ($numrows && $i <= ($numrows-1) && $i >=0){
	        mysqli_data_seek($result,$i);
	        $resrow = (is_numeric($nome)) ? mysqli_fetch_row($result) : mysqli_fetch_assoc($result);
	        if (isset($resrow[$nome])){
	            return $resrow[$nome];
	        }
	    }
	    return false;
	}
	
	function result_array($result)
	{		
	    $aux = array();
		/*if($this->num_rows($result) > 0)
		{
          if(*/$aux = mysqli_fetch_array($result, MYSQLI_ASSOC);/*){*/
              return $aux;
          /*}else{
              echo "Falha ao gerar array de resultado para " .$result. "<br />Erro: <b>".mysqli_error($this->link_conn)."</b>";
			  die();
          }  
		}
		else
		{
			return $aux;
		}*/
	}	
	
	function result_object($result)
	{		
	    $aux;
        //if(
		   $aux = mysqli_fetch_object($result);
		   //){
            return $aux;
        /*}else{
            echo "Falha ao gerar objeto de resultado para " .$result. "<br />Erro: <b>".mysqli_error($this->link_conn)."</b>";
			die();
        } */ 
	}	
	
	function result_object_array($result)
	{		
	    $array_obj = array();
	    while($obj = mysqli_fetch_object($result))
	    {
	    	$array_obj[sizeof($array_obj)] = $obj;
	    }
	    return $array_obj;
	}	
	
	function escape_string($strOri)
	{
		$this->conecta();
        //if(
			 $str = mysqli_real_escape_string($this->link_conn, $strOri);
			 //){
			$this->desconecta();
            return $str;
        /*}else{
            echo "Falha ao encapsular a string: ".$strOri."<br />Erro: <b>".mysqli_error($this->link_conn)."</b>";
			die();
        }*/       
	}    
	
	function fields_array($result)
	{
		if($this->num_rows($result) > 0)
		{
          /*if(*/
			 $ret = array();			
			 $array_aux = mysqli_fetch_array($result, MYSQLI_ASSOC);	
			 if($array_aux)
			 {
			 	$ret = array_keys($array_aux);
			 }
			 //var_dump($ret); echo "<br/><br/>";
			 //){
              return $ret;
          /*}else{
              echo "Falha ao retornar Array de campos em ".$result."<br />Erro: <b>".mysqli_error()."</b>";
		  	  die();
          }   */
		}
		else
		{
			$ret = array();
            return $ret;
		}
	}

	function error() {
		return mysqli_error($this->link_conn);	
	}
}

?>