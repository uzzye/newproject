<?php

class Uzzye_NumberField extends Uzzye_Field
{	
	public $max_length;
	public $decimals;
	public $prefix;
	public $sufix;
	public $decimal_char;
	public $towsand_char;
	
    function __construct($name = "", $id = "", $label = "", $default_value = "", $readonly = "", $style_class = "form-Text", $li_class = "", $sub_label = "", $max_length = 16, $decimals = 0, $prefix = "", $sufix = "", $decimal_char = ",", $towsand_char = ".")
	{	
		$this->type = "text";
		$this->decimals = $decimals;
		$this->prefix = $prefix;
		$this->sufix = $sufix;
		$this->decimal_char = $decimal_char;
		$this->towsand_char = $towsand_char;
		
		parent::__construct($name, $id, $label, $default_value, $readonly, $style_class, $li_class, $sub_label, $max_length);
	}
	
	function get_display_field()
	{
		$result = "";
		$result .= $this->ini_field_set();
		$result .= $this->get_display_label();		
			
		$result .= "<input type=\"text\" class=\"form-control form-number " . $this->style_class. "\" id=\"" . ($this->id) . "\" name=\"" . ($this->name) . "\" value=\"" . ($this->value) . "\" placeholder=\"" . $this->default_value . "\"";
		if(trim($this->max_length) <> "" && $this->max_length > 0)
		{
			$result .= " maxlength=\"" . ($this->max_length) . "\"";
		}	
		if($this->readonly)
		{
			$result .= " readonly";
		}	
		if($this->required)
		{
			$result .= " required='required' ";
		}

	// Cria máscara
		/*$mask = "";
		if(trim($this->prefix) != "") {
			$mask .= $this->prefix;
		}
		$iCount=0;
		$charSize = ($this->max_length - $this->decimals - strlen($this->prefix) - strlen($this->decimal_char) - strlen($this->sufix));
		$maskAux = "";
		if($charSize > 0) {
			for($w=0; $w<$charSize; $w++) {
				$maskAux .= "9";
				$iCount++;

				if($iCount == 3) {
					$maskAux .= $this->towsand_char;
					$w += strlen($this->towsand_char);
					$iCount = 0;
				}
			}
		}
		$maskAux = strrev($maskAux);
		if(substr($maskAux, 0, 1) == $this->towsand_char) {
			$maskAux = substr($maskAux,1);
		}
		$mask .= "?".$maskAux;
		if($this->decimals > 0) {
			$mask .= $this->decimal_char;
			for($w=0;$w<($this->decimals);$w++) {
				$mask .= "9";
			}
		}
		if(trim($this->sufix) != "") {
			$mask .= $this->sufix;
		}

		if(trim($mask) != "") {
			$result .= " data-mask=\"" . $mask . "\" ";
		}*/

		if(isset($this->decimals) != "") {
			$result .= " data-precision=\"" . $this->decimals . "\" ";
		}
		if(isset($this->sufix) != "") {
			$result .= " data-suffix=\"" . $this->sufix . "\" ";
		}
		if(isset($this->prefix) != "") {
			$result .= " data-prefix=\"" . $this->prefix . "\" ";
		}
		if(isset($this->towsand_char) != "") {
			$result .= " data-thousands=\"" . $this->towsand_char . "\" ";
		}
		if(isset($this->decimal_char) != "") {
			$result .= " data-decimal=\"" . $this->decimal_char . "\" ";
		}

		$result .= "/>";
		
		$result .= $this->end_field_set();
		
		return $result;
	}
	
	function get_db_value()
	{
		global $db;
		
		if(trim($this->prefix) != ""){
			$this->value = str_replace($this->prefix, "", $this->value);
		}
		if(trim($this->sufix) != ""){
			$this->value = str_replace($this->sufix, "", $this->value);
		}
		
		if($this->decimals > 0){
			if(substr($this->value,-$this->decimals-1,1) != "."){
				return (str_replace($this->decimal_char,".",str_replace($this->towsand_char,"",$this->value)));
			} else {return str_replace(",","",$this->value);}
		} else {return $this->value;}
	}	
	
	function set_value($valor)
	{	
		if($this->decimals > 0){
			$this->value = htmlspecialchars(number_format(($valor),$this->decimals,$this->decimal_char,$this->towsand_char));
		}
		else {
			$this->value = htmlspecialchars(($valor));		
		}
	}

	function set_mask($array) {
		if(array_key_exists(0, $array)){
			$this->decimals = $array[0];
		}
		if(array_key_exists(1, $array)){
			$this->decimal_char = $array[1];
		}
		if(array_key_exists(2, $array)){
			$this->towsand_char = $array[2];
		}
		if(array_key_exists(3, $array)){
			$this->prefix = $array[3];
		}
		if(array_key_exists(4, $array)){
			$this->sufix = $array[4];
		}
	}
}

?>