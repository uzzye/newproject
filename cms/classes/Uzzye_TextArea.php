<?php

class Uzzye_TextArea extends Uzzye_Field
{	
	public $size;
	public $rows;
	public $max_length;
	
    function __construct($name = "", $id = "", $label = "", $default_value = "", $readonly = "", $style_class = "textarea", $li_class = "", $sub_label = "", $max_length = 0)
	{
		$this->type = "textarea";
		$this->rows = 5;
		
		parent::__construct($name, $id, $label, $default_value, $readonly, $style_class, $li_class, $sub_label, $max_length);
	}
	
	function get_display_field()
	{
		$result = "";
		$result .= $this->ini_field_set();
		$result .= $this->get_display_label();		
		
		if(trim($this->size) <> "")
		{
			switch($this->size)
			{
				case "P":
					$this->style_class = "textAreaP";
					$this->rows = 5;
					break;
				case "M":
					$this->style_class = "textAreaM";
					$this->rows = 10;
					break;
				case "G":
					$this->style_class = "textAreaG";
					$this->rows = 20;
					break;
			}
		}		
		
		$result .= "<textarea autocomplete=\"off\" id=\"" . ($this->id) . "\" rows=\"" . $this->rows . "\" class=\"form-control " . $this->style_class . "\" name=\"" . ($this->name) . "\"";
		if(trim($this->max_length) <> "" && $this->max_length > 0)
		{
			$result .= " maxlength=\"" . ($this->max_length) . "\"";
		}	
		if($this->readonly)
		{
			$result .= " readonly";
		}		
		if($this->required)
		{
			$result .= " required='required' ";
		}
		$result .= ">" . ($this->value) . "</textarea>";
		
		$result .= $this->end_field_set();
		return $result;
	}
	
	function get_db_value()
	{
		global $db;
		return output_decode($db->escape_string($this->value));
	}	
	
	function set_value($valor)
	{
		$this->value = htmlspecialchars(stripslashes(get_output($valor)));
	}
}

?>