<?php

class Uzzye_DataTable extends Uzzye_Field
{	
	public $clone;
	public $icon;
	public $id_registro;
	public $reference_item;
	public $sql_where;
	public $sql_order;
	public $i_count_field;
	public $b_editando;
	
    function __construct($name = "", $id = "", $label = "", $default_value = "", $readonly = "", $style_class = "sltNormal", $li_class = "", $sub_label = "")
	{
		$this->type = "dataTable";

		$this->i_count_field = 1;
		$this->b_editando = false;
		$this->clone = false;

		parent::__construct($name, $id, $label, $default_value, $readonly, $style_class, $li_class, $sub_label);
	}
	
	function get_display_field()
	{
		global $mysql, $cryptDec;

		if(is_array($this->reference_item) && sizeof($this->reference_item) > 0) {
			$this->name = $this->reference_item[0];
			carrega_classe($this->name);
		}

		?>
		<script type="text/javascript">

		$(document).ready(function(e){
		
			'use strict';

			var iCount = 1;
			var master = $("#table-<?php echo $this->name; ?>");

			resetDataEvents(e);

			master.find(".btnAdd").unbind("click").click(function(e){
				var lista = master.find("table");

				var iQtd = 1;
				if(master.find(".qtdAdd").val().trim() != ""){
					iQtd = parseInt(master.find(".qtdAdd").val());
				}

				for(var i=0;i<iQtd;i++){
					var elAux = lista.find(".itLista").eq(0).clone();

					// Reset select2
					elAux.find(".select2").not("select").remove();
					elAux.find("select").removeClass("select2-hidden-accessible");

					// Fazer reset nos demais kenny?

					//var elAuxId = parseInt(lista.find(".itLista").length)+1;
					//var elAuxId = parseInt(lista.find(".itLista:last").prop("id").split("-")[2])+1;
					var elAuxId = "<?php echo mt_rand(1,99999999999); ?>";
					elAux.prop("id","itLista-<?php echo $this->name; ?>-<?php echo $this->i_count_field; ?>-" + elAuxId.toString());
					
					elAux.find(".id_aux_<?php echo $this->name; ?>").val("");
					elAux.find(".id_aux_<?php echo $this->name; ?>").prop("id","id_aux-" + elAuxId.toString());

					if(elAux.find(".clonable").length > 0) {
						elAux.find(".clonable").each(function(e){
							var elClone = $(this);
							var sIDAux = elClone.prop("id").split("-")[0];
							elClone.prop("id",sIDAux + "-" + elAuxId.toString());

							if(!elClone.hasClass("keep")) {
								if(elClone.attr("type") == "button" ||
								   elClone.attr("type") == "hidden" ||
								   elClone.attr("type") == "submit")
								{
									// NADA
								}
								else if(elClone.attr("type") == "checkbox" ||
								   elClone.attr("type") == "radio")
								{
									//elClone.iCheck('uncheck');
								} else {
									elClone.val("");
								}
								
								elClone.find("option").eq(0).attr('selected','selected');
								elClone.find("option").removeAttr('selected');
								elClone.trigger('update.fs');
							}

							if(elClone.hasClass("idField")) {
								elClone.val("");
							}
						});
					}
					
					elAux.find("label").each(function(e){
						$(this).prop("for",$(this).parent().children().eq(1).prop("id"));
					});

					elAux.insertAfter(lista.find(".itLista:last"));
				
					kenny(elAux);

					resetDataEvents(e);
				}

				resetDataTableIDs(e);
			});

			$(".form-cadastro-padrao").on("submit", function(e){
				resetDataTableIDs(e);
			});

			function resetDataEvents(e){
				master.find("table").find(".btnRem").unbind("click").click(function(e){
					if(master.find("table").find(".itLista").length > 1) {
						$(this).closest(".itLista").remove();
					} else {
						master.find(".btnAdd").trigger("click");
						
						$(this).closest(".itLista").remove();
					}

					resetDataTableIDs(e);
				});

				resetDataTableIDs(e);
			}

			function resetDataTableIDs(e) {
				var iCountAux = 1;
				master.find(".itLista").each(function(e){
					$(this).find(".id_aux_<?php echo $this->name; ?>").val(iCountAux);

					$(this).find("input[type='checkbox']").each(function(e){
						$(this).on("change", function(e) {
							var valAux = "0";
							if($(this).prop("checked")) {
								valAux = "1";
							}
							$(this).closest("td").find("#" + $(this).attr("id").replace("_view","")).val(valAux);
						});
					});

					iCountAux++;
				});

				master.find("table").rowSorter({
					handler: "td.sorter",
					tbody: true
				});

			}
		});
		</script>
		<?php
		
		$result = "";

		if(trim($this->label) != "") {
			// HR
			$inputAux = new Uzzye_HR();		
			$inputAux->label = $this->label;
			$inputAux->icon = $this->icon;
			$result .= $inputAux->get_display_field();
		}
		
		$result .= $this->ini_field_set();
		//$result .= $this->get_display_label();

		$iCount = 0;
		$iCountAux = 0;
		$iCountCampo = 0;

		$values = null;

		$result .= "<div class=\"row\" id=\"table-" . $this->name . "\">";

			$result .= "<table class=\"grid " . $this->name . "\" id=\"lista-" . $this->name . "-" . $this->i_count_field . "\">
			<tbody>";

			$bFields = false;

				$classAux = $this->reference_item[0];
				$campoID = $this->reference_item[1];
				$objAux = new $classAux();

				$objAux->view->monta_campos_form($this->id_registro, $this->readonly, $this->clone, $this->resumido);

				$tableAux = $objAux->get_nome_tabela();

				if(intval($this->id_registro) > 0 && trim($tableAux) != ""){
					$sqlCmd = "SELECT * FROM (" . $tableAux . ")
					WHERE " . $campoID . " = " . intval($this->id_registro) . "
					ORDER BY ranking ASC ";
					$resCmd = $mysql->exec_query($sqlCmd);

					while($iCount<$mysql->num_rows($resCmd))
					{
						$values = array();
						$iCountAux++;
						$values["id_aux_" . $this->name] = $iCountAux;
						if($this->clone) {
							$values["id_" . $this->name] = 0;
							$values["id_registro_" . $this->name] = 0;
						} else {
							$values["id_" . $this->name] = $mysql->result_field($resCmd,$iCount,"id");
							$values["id_registro_" . $this->name] = $mysql->result_field($resCmd,$iCount,$campoID);
						}

						if(is_array($objAux->view->array_form_campos) && sizeof($objAux->view->array_form_campos) > 0) {
							foreach($objAux->view->array_form_campos as $inputAux) {
								if($inputAux->show_in_data_table) {
									$values[$inputAux->name . "_" . $this->name] = stripslashes($mysql->result_field($resCmd,$iCount,$inputAux->name));
								}
							}
						}
						
						if(is_array($values) && sizeof($values) > 0) {
							$resultAux = $this->get_campos($iCount,$values);
							if(trim($resultAux) != "") {
								$result .= $resultAux;
								$bFields = true;
							}
						}
						
						$iCount++;
					}
				}

				if($iCount == 0){
					$resultAux = $this->get_campos($iCount);
					if(trim($resultAux) != "") {
						$result .= $resultAux;
						$bFields = true;
					}

					$iCount++;
				}

				$result .= "</tbody>
				<tfoot>
					<tr valign=\"top\">";

					$result .= "<td colspan=\"100\">";
						$valAux = "1";
						$ref = "qtdAdd-" . $this->name . "-" . $this->i_count_field;
						$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_ADICIONAR"));
						$inputAux->set_value($valAux);
						$inputAux->name = $ref;
						$inputAux->id = $ref;
						$inputAux->style_class = $ref . " qtdAdd";
						$inputAux->li_class = "w10p";
						$count_fields++;
						$result .= $inputAux->get_display_field();

						$ref = "btnAdd-" . $this->name . "-" . $this->i_count_field;
						$inputAux = new Uzzye_Button($ref,$ref," ");
						$inputAux->icon = "icon-plus";
						$inputAux->name = $ref;
						$inputAux->id = $ref;
						$inputAux->style_class = $ref . " btnAdd btn-square btn-icon btn-icon-anim btn-success btn-primary";
						$inputAux->li_class = "w5p";
						$count_fields++;
						$result .= $inputAux->get_display_field();
					$result .= "</td>";

				$result .= "</tr>
				</tfoot>";

			$result .= "</table>";

		$result .= "</div>";
		
		$result .= $this->end_field_set();
		
		if($bFields) {
			return $result;
		} else {
			return "";
		}
	}
	
	function get_db_value()
	{
		return output_decode($this->value);
	}	
	
	function set_value($valor)
	{
		$this->value = get_output($valor);
	}

	function get_campos($iCount,$values = array()){
		$result = "";

		if(is_array($this->reference_item) && sizeof($this->reference_item) > 0) {

			$result .= "<tr class=\"itLista itLista-" . $this->name . "\" id=\"itLista-" . $this->name . "-" . $this->i_count_field . "-" . ($iCount+1). "\" valign=\"top\">";

				$result .= "<td class=\"sorter\" width=\"1\">";

					$inputAux = new Uzzye_Label();
					$inputAux->icon = "fa fa-bars";
					$inputAux->li_class = "w100p sorter mt-30";
					$result .= $inputAux->get_display_field();

					$ref = "id_aux_" . $this->name;
					$valAux = 0;
					if(sizeof($values) > 0){
						$valAux = $values[$ref];
					}
					$inputAux = new Uzzye_HiddenField($ref,$ref,get_lang("_ID"));
					$inputAux->name = $ref . "[]";
					$inputAux->id = $ref . "-" . ($iCount+1);
					$inputAux->set_value($valAux);
					$inputAux->style_class = $ref;
					$count_fields++;
					$result .= $inputAux->get_display_field();

					$ref = "id_" . $this->name;
					$valAux = 0;
					if(sizeof($values) > 0){
						$valAux = $values[$ref];
					}
					$inputAux = new Uzzye_HiddenField($ref,$ref,get_lang("_ID"));
					$inputAux->name = $ref . "[]";
					$inputAux->id = $ref . "-" . ($iCount+1);
					$inputAux->set_value($valAux);
					$inputAux->style_class = $ref . " clonable idField ";
					$count_fields++;
					$result .= $inputAux->get_display_field();

					$ref = "id_registro_" . $this->name;
					$valAux = $this->id_registro;
					if(sizeof($values) > 0){
						$valAux = $values[$ref];
					}
					$inputAux = new Uzzye_HiddenField($ref,$ref,get_lang("_ID"));
					$inputAux->name = $ref . "[]";
					$inputAux->id = $ref . "-" . ($iCount+1);
					$inputAux->set_value($valAux);
					$inputAux->style_class = $ref . " clonable keep";
					$count_fields++;
					$result .= $inputAux->get_display_field();

				$result .= "</td>";

				$result .= "<td>";

					$classAux = $this->reference_item[0];
					$objAux = new $classAux();

					$objAux->view->monta_campos_form($this->id_registro, $this->readonly, $this->clone, $this->resumido);

					$bFields = false;

					if(is_array($objAux->view->array_form_campos) && sizeof($objAux->view->array_form_campos) > 0) {
						foreach($objAux->view->array_form_campos as $inputAux) {
							if($inputAux->show_in_data_table) {
								$ref = $inputAux->name . "_" . $this->name;
								$valAux = "";
								if(sizeof($values) > 0){
									$valAux = $values[$ref];
								}
								$inputAux->name = $ref ."[]";
								$inputAux->style_class .= $ref . " clonable";
								$inputAux->id = $ref . "-" . ($iCount+1);
								
								$bIsCheckbox = false;
								if(get_class($inputAux) == "Uzzye_CheckBox" || get_class($inputAux) == "Uzzye_RadioButton" ||
							       get_class($inputAux) == "Uzzye_CheckBoxGroup" || get_class($inputAux) == "Uzzye_RadioGroup") {
									$inputAux->name = $ref ."_view[]";
									$inputAux->id = $ref . "_view-" . ($iCount+1);
									$inputAux->checked_value = $valAux;
									$bIsCheckbox = true;
					            } else {
									$inputAux->set_value($valAux);
								}

								$count_fields++;
								$result .= $inputAux->get_display_field();

								if($bIsCheckbox) {
									$inputAux = new Uzzye_HiddenField($ref,$ref,"");
									$inputAux->name = $ref . "[]";
									$inputAux->id = $ref . "-" . ($iCount+1);
									$inputAux->set_value($valAux);
									$inputAux->style_class = $ref . " clonable";
									$result .= $inputAux->get_display_field();
								}

								$bFields = true;
							}
						}
					}

				// CAMPO REMOVER

					$ref = "btnRem-" . $this->name;
					$inputAux = new Uzzye_Button($ref,$ref);
					$inputAux->icon = "icon-trash";
					$inputAux->name = $ref . "[]";
					$inputAux->id = $ref . "-" . ($iCount+1);
					$inputAux->style_class = $ref . " btnRem btn-square btn-icon btn-icon-anim btn-danger btn-primary";
					$inputAux->li_class = "w5p";
					$count_fields++;
					$result .= $inputAux->get_display_field();

			// FIM CAMPOS

				$result .= "</td>";
			$result .= "</tr>";
		} else {

			$result .= "<tr valign=\"top\">";
				$result .= "<td>
					Modelo de referência não definido.
				</td>";
			$result .= "</tr>";

		}

		if($bFields) {
			return $result;
		} else {
			return "";
		}
	}
	
	function set_reference_item($refArray = null)
	{
		$this->reference_item = $refArray;
			
		/*$class = $this->reference_item[0];
		$objAux = new $class();

		$sWhereAux = $objAux->view->default_sql_where;
		if(trim($this->sql_where) != "") {
			if(trim($sWhereAux) != "") {
				$sWhereAux .= " AND ";
			}
			$sWhereAux .= $this->sql_where;
		}
		$sOrderAux = $objAux->view->default_sql_order;
		if(trim($this->sql_order) != "") {
			if(trim($sOrderAux) != "") {
				$sOrderAux .= ", ";
			}
			$sOrderAux .= $this->sql_order;
		}*/

		//$objAux->view->default_value_field
		//$objAux->view->default_view_field
		//$sWhereAux,
		//$sOrderAux,
		//$objAux->view->default_value_expr
		//$objAux->view->default_view_expr
	}
}

?>