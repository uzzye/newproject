<?php

class Uzzye_RadioButton extends Uzzye_Field
{	
	public $checked_value;
	public $fixed_pos;

    function __construct($name = "", $id = "", $label = "", $default_value = "", $readonly = "", $style_class = "form-Radio", $li_class = "", $sub_label = "")
	{
		$this->type = "checkbox";
		$this->fixed_pos = false;
		
		parent::__construct($name, $id, $label, $default_value, $readonly, $style_class, $li_class, $sub_label);
	}
	
	function get_display_field()
	{
		$result = "";
		$result .= $this->ini_field_set();
		$result .= $this->get_display_label();		
		
		$result .= "<div class=\"radio-inline\">
			<span class=\"radio radio-info\">
			<input type=\"radio\" class=\"" . $this->style_class. "\" id=\"" . ($this->id) . "\" name=\"" . ($this->name) . "\" value=\"" . ($this->value) . "\" ";
		if($this->checked_value == $this->value)
		{
			$result .= " checked ";
		}
		if($this->readonly)
		{
			$result .= " readonly onclick=\"this.checked = !this.checked; return false;\" ";
		}
		if($this->required)
		{
			$result .= " required='required' ";
		}
		$result .= " />
				<label for=\"" . ($this->id) . "\">" . $this->sub_label . "</label>
			</span>
		</div>";
		
		$result .= $this->end_field_set();
		
		return $result;
	}
	
	function get_db_value()
	{
		return $this->value;
	}	
	
	function set_value($valor)
	{
		$this->value = $valor;
	}
}

?>