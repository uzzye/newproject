<?php

class Uzzye_ImageGallery extends Uzzye_Field
{	
	public $clone;
	public $id_registro;
	public $reference_item;
	public $sql_where;
	public $sql_order;
	public $i_count_field;
	public $b_editando;
	public $display_format;

	function __construct($name = "", $id = "", $label = "", $default_value = "", $readonly = "", $style_class = "form-Text", $li_class = "", $sub_label = "")
	{	
		$this->type = "imageGallery";
		
		parent::__construct($name, $id, $label, $default_value, $readonly, $style_class, $li_class, $sub_label, $max_length);		
		
		$this->li_class = "w100p";
		$this->display_format = "2x1";
	}
	
	function get_display_field()
	{
		global $modulo;

		$result = "";

		if(trim($this->label) != "") {
			// HR
			$inputAux = new Uzzye_HR();		
			$inputAux->label = $this->label;
			$inputAux->icon = "icon-picture";
			$result .= $inputAux->get_display_field();
		}

		$classAux = $this->reference_item[0];

		carrega_classe($classAux);
		$imgs = new $classAux();

		$img_fields = $imgs->model->array_crop_fields;
		$img_field = $img_fields[0];

		$upload_folder = UPLOAD_FOLDER;
		if(@trim($imgs->model->upload_folders[$img_field]) != "") {
		  $upload_folder = $imgs->model->upload_folders[$img_field];
		} else if(@trim($imgs->model->upload_folder) != "") {
		  $upload_folder = $imgs->model->upload_folder;
		}

		$imgs = $imgs->get_array_ativos_controllers($this->reference_item[1] . " = " . intval($this->id_registro));

		if(is_array($imgs) && sizeof($imgs) > 0) {
			$images_result = "<div class=\"imagens-select\">
				<div class=\"row\">";
			//for($w=0;$w<100;$w++)
			{foreach($imgs as $img) {
				$imgTit = stripslashes(get_output($img->get_view_field_value()));
				if(trim($imgTit) == "") {
					$imgTit = stripslashes(get_output($img->get_var($img_field)));
				}

				$imgTag = "";

				if(trim($img->get_var($img_field)) != "") {
				  list($width, $height, $type, $attr) = @getimagesize($upload_folder . $img->get_var($img_field));
				  $classAux = "";
				  if($width >= $height) {
				    $classAux = "wide";
				  } else if($width < $height) {
				    $classAux = "tall";
				  }
				  $imgTag = "<img class='center middle " . $classAux . "' src='" . $upload_folder . $img->get_var($img_field) . "' border='0' width='100' />";
				}

				if(trim($imgTag) != "") {
				  $images_result .= "<div class=\"col-sm-3 col-xs-6 mb-10\">
				    <span class=\"imagem-select square gal\">
				      <a href=\"" . ROOT_SERVER . ROOT . "file_download.php?file=" . rawurlencode($img->get_var($img_field)) . "&folder=" . rawurlencode($upload_folder) . "\" class=\"linkFull\" title=\"Download\">
				      	" . $imgTag . "
				      </a>
				      </span>
				      <span class=\"btn-text\">" . $imgTit . "</span>";
				  if(!$this->readonly) {
				  	$images_result .= " <a class=\"link gal-remove\" href=\"javascript:void(0);\" data-id=\"" . $img->get_var("id") . "\"><i class=\"fa fa-trash text-danger\"></i></a>";
				  }
				  $images_result .= "</div>";
				}
			}}
			$images_result .= "</div>
			</div>";
		} else {
			if($this->id_registro > 0) {
				$images_result = "<div class=\"center\">Nenhum arquivo enviado.</div>";
			}
		}
		
		$result .= $this->ini_field_set();
		//$result .= $this->get_display_label();

		if(!$this->readonly) {
			// 2x1
			if(trim($this->display_format) == "" || $this->display_format == "2x1") {
				$result .= "<div class=\"row\">
					<div class=\"col-sm-12\">
						<div class=\"panel panel-default pa-20 panel-arquivos\">
							" . str_replace("col-sm-3 col-xs-6","col-md-2 col-sm-3 col-xs-6",$images_result) . "
						<input type=\"hidden\" id=\"imgs_list_" . $modulo . "_" . $this->id . "\" name=\"imgs_list_" . $this->reference_item[0] . "\" value=\"\" />
						<div id=\"dropzone_" . $modulo . "_" . $this->id . "\" class=\"dropzone\">
							<div class=\"fallback\">
								<input name=\"dropzone_file_" . $this->id . "\" id=\"dropzone_file_" . $modulo . "_" . $this->id . "\" type=\"file\" multiple />
							</div>
						</div>
					</div>
				</div>";
			}
			// 1x2
			else if($this->display_format == "1x2") {
				$result .= "<div class=\"row\">
					<div class=\"col-sm-4\">
						<input type=\"hidden\" id=\"imgs_list_" . $modulo . "_" . $this->id . "\" name=\"imgs_list_" . $this->reference_item[0] . "\" value=\"\" />
						<div id=\"dropzone_" . $modulo . "_" . $this->id . "\" class=\"dropzone\">
							<div class=\"fallback\">
								<input name=\"dropzone_file_" . $this->id . "\" id=\"dropzone_file_" . $modulo . "_" . $this->id . "\" type=\"file\" multiple />
							</div>
						</div>
					</div>
					<div class=\"col-sm-8\">
						<div class=\"panel panel-default pa-20 panel-arquivos\">
							" . $images_result . "
						</div>		
					</div>
				</div>";
			}
		} else {
			$result .= "<div class=\"row\">
				<div class=\"col-sm-12\">
					<div class=\"panel panel-default pa-20 panel-arquivos\">
						" . $images_result . "
					</div>		
				</div>
			</div>";
		}

		?>
		<script language='javascript'>
			/*Responsive Datatable Init*/

			"use strict"; 

			$(document).ready(function(e) {
				var elAux = $("div#dropzone_<?php echo $modulo . "_" . $this->id; ?>");
				var dropzone = new Dropzone("div#" + elAux.attr("id"), {
					url: ROOT_SERVER + ROOT + "file_upload.php",
					method: 'post',
                    uploadMultiple: true,
                    parallelUploads: 1,
                    maxFiles: 1000,
                    autoProcessQueue: true,
                    addRemoveLinks: true,
                    paramName: "file", // The name that will be used to transfer the file
                   	params: {
                   		randcod: elAux.closest(".form-cadastro-padrao").find(".randcod").val(),
                   		file_id: elAux.attr("id"),
                   		is_dropzone: true,
                   		selfname: true // usa próprio nome do arquivo
                   	},
                    //maxFilesize: 4, // MB
                    acceptedFiles: "image/*",

					dictDefaultMessage: "<?php echo get_lang("_DROPZONE_DEFAULTMESSAGE"); ?>",
					dictFallbackMessage: "<?php echo get_lang("_DROPZONE_FALLBACKMESSAGE"); ?>",
					dictFallbackText: "<?php echo get_lang("_DROPZONE_FALLBACKTEXT"); ?>",
					dictFileTooBig: "<?php echo get_lang("_DROPZONE_FILETOOBIG"); ?>",
					dictInvalidFileType: "<?php echo get_lang("_DROPZONE_INVALIDFILETYPE"); ?>",
					dictResponseError: "<?php echo get_lang("_DROPZONE_RESPONSEERROR"); ?>",
					dictCancelUpload: "<?php echo get_lang("_DROPZONE_CANCELUPLOAD"); ?>",
					dictCancelUploadConfirmation: "<?php echo get_lang("_DROPZONE_CANCELUPLOADCONFIRMATION"); ?>",
					dictRemoveFile: "<?php echo get_lang("_DROPZONE_REMOVEFILE"); ?>",
					dictMaxFilesExceeded: "<?php echo get_lang("_DROPZONE_MAXFILESEXCEEDED"); ?>",
					dictRemoveFileConfirmation: null,

					/*previewTemplate: '<div class="dz-preview dz-file-preview">' +
					  '<div class="dz-details">' +
					      '<div class="dz-filename"><span data-dz-name></span></div>' +
					      '<div class="dz-size" data-dz-size></div>' +
					      '<img data-dz-thumbnail />' +
					    '</div>' +
					    '<div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>' +
					    '<div class="dz-success-mark"><span>✔</span></div>' +
					    '<div class="dz-error-mark"><span>✘</span></div>' +
					    '<div class="dz-error-message"><span data-dz-errormessage></span></div>' +
					    '<span data-dz-remove><i class=\"fa fa-trash rm-10\"></i></span>' +
					'</div>',*/

					init: function() {
					    this.on("addedfile", function(file) {
                            bCanSubmit = false;

                            var formPadrao = $(".form-cadastro-padrao").not(".no-ajax");
                            formPadrao.addClass("blocked");
			    			formPadrao.find(".btn-submit").find(".btn-text").html("Aguarde...");

					    	//console.log(file);
						});
					},
                    accept: function(file, done) {
                        done();
                    }
				});

				dropzone.on("success", function(file, serverFileName) {
                    //file.name;
                    $(file.previewTemplate).find('.dz-filename').data("server-file",serverFileName)

                    var value = $("#imgs_list_<?php echo $modulo . "_" . $this->id; ?>").val();
                    $("#imgs_list_<?php echo $modulo . "_" . $this->id; ?>").val(value + serverFileName + "#;#");

                    //dropzone.processQueue();
                });

                dropzone.on("removedfile", function(file) {
                    var server_file = $(file.previewTemplate).find('.dz-filename').data("server-file") + "#;#";
                    // Do a post request and pass this path and use server-side language to delete the file

                    var value = $("#imgs_list_<?php echo $modulo . "_" . $this->id; ?>").val();
                    $("#imgs_list_<?php echo $modulo . "_" . $this->id; ?>").val(value.replace(server_file,''));

                    unlink_image(server_file);
                });

                dropzone.on("queuecomplete", function() {
                    bCanSubmit = true;

                    var formPadrao = $(".form-cadastro-padrao").not(".no-ajax");
                    formPadrao.removeClass("blocked");
			    	formPadrao.find(".btn-submit").find(".btn-text").each(function(e){
			    		$(this).html($(this).data("label"));
			    	});
                });

                $(".gal-remove").unbind("click").click(function(e){
                	e.preventDefault();
                	e.stopPropagation();

                	var btnAux = $(this);

                	swal({
						title: "<?php echo get_lang("_VOCETEMCERTEZA"); ?>",   
						text: "<?php echo get_lang("_NAOSERAPOSSIVELRECUPERAR"); ?>",
			            type: "warning",  
			            showCancelButton: true,   
			            confirmButtonColor: "#fcb03b",
						confirmButtonText: "<?php echo get_lang("_SIMREMOVER"); ?>",
						cancelButtonText: "<?php echo get_lang("_NAOCANCELAR"); ?>",   
			            closeOnConfirm: true,   
			            closeOnCancel: true,
			            allowOutsideClick: true
			        }, function(isConfirm){   
			            if (isConfirm) {  
                			file_remove_modulo("<?php echo $this->reference_item[0]; ?>",btnAux.data("id"));

                			var panel = btnAux.closest(".imagens-select");

                			btnAux.parent().remove();

                			if(panel.find(".imagem-select").length == 0) {
                				panel.html("<div class=\"center\">Nenhum arquivo enviado.</div>");
                			}
                		}
                	});
                });
			});
		</script>
		<?php

		$result .= $this->end_field_set();
		
		return $result;
	}

	function get_db_value()
	{
		return output_decode($this->value);
	}	
	
	function set_value($valor)
	{
		$this->value = get_output($valor);
	}
	
	function set_reference_item($refArray = null)
	{
		$this->reference_item = $refArray;
			
		/*$class = $this->reference_item[0];
		$objAux = new $class();

		$sWhereAux = $objAux->view->default_sql_where;
		if(trim($this->sql_where) != "") {
			if(trim($sWhereAux) != "") {
				$sWhereAux .= " AND ";
			}
			$sWhereAux .= $this->sql_where;
		}
		$sOrderAux = $objAux->view->default_sql_order;
		if(trim($this->sql_order) != "") {
			if(trim($sOrderAux) != "") {
				$sOrderAux .= ", ";
			}
			$sOrderAux .= $this->sql_order;
		}*/

		//$objAux->view->default_value_field
		//$objAux->view->default_view_field
		//$sWhereAux,
		//$sOrderAux,
		//$objAux->view->default_value_expr
		//$objAux->view->default_view_expr
	}
}

?>