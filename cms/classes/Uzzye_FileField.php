<?php

class Uzzye_FileField extends Uzzye_Field
{	
	public $sub_type;
	public $db_table;
	public $db_field;
	public $base64;
	
    function __construct($name = "", $id = "", $label = "", $default_value = "", $readonly = "", $style_class = "form-Text", $li_class = "", $sub_label = "")
	{
		$this->type = "file";
		
		parent::__construct($name, $id, $label, $default_value, $readonly, $style_class, $li_class, $sub_label);
		
		$this->style_class = "form-File";
		
		$this->db_table = "";
		$this->db_field = "";
	}
	
	function get_display_field()
	{
		global $modulo;
		
		$result = "";
		$result .= $this->ini_field_set();
		$result .= $this->get_display_label();

		$uploadAux = UPLOAD_FOLDER;
		if(file_exists(ROOT_CMS . "classes/models/" . $modulo."_model.php"))
		{
			require_once(ROOT_CMS . "classes/models/" . $modulo."_model.php");
			$classe = $modulo."_model";
			$objAux = new $classe();

			if(@trim($objAux->upload_folders[$this->id]) != "") {
				$uploadAux = $objAux->upload_folders[$this->id];
			} else if(trim($objAux->upload_folder) != "") {
				$uploadAux = $objAux->upload_folder;
			}
		}
		$folderAux = $uploadAux;
		
		//if(!$this->readonly)
		{
			$result .= "<input type=\"file\" class=\"dropify " . $this->style_class. "\" id=\"" . ($this->id) . "\" name=\"" . ($this->name) . "\" data-default-file=\"" . $uploadAux . $this->value . "\" data-folder=\"" . $uploadAux . "\" data-file=\"" . $this->value . "\" data-modulo=\"" . $modulo. "\" data-id=\"" . intval($_REQUEST["id"]) . "\" ";
			if($this->required)
			{
				//$result .= " required='required' ";
			}
			if($this->readonly)
			{
				$result .= " readonly data-show-remove=\"false\" disabled=\"disabled\" ";
			}
			$result .= "/>";
		} 
		
		if($this->base64) {
			$result .= "<a class=\"btn-download\" href=\"#\" data-url=\"" . ROOT_SERVER . ROOT . "file_download.php?base64&modulo=" . $modulo . "&campo=" . ($this->name) . "&id=" . intval($_REQUEST["id"]) . "\" target=\"_blank\">
				<button type=\"button\" class=\"btn btn-info btn-icon-anim btn-square\"><i class=\"icon-cloud-download\"></i>" . 
				//"<span class=\"btn-text\">" . get_lang("_VISUALIZAR") . "</span>" .
				"</button>
			</a>";
		} else if(trim($this->value) <> "" && file_exists($uploadAux . trim($this->value)))
		{
			$result .= "<a class=\"btn-download\" href=\"#\" data-url=\"" . ROOT_SERVER . ROOT . "file_download.php?file=" . str_replace($folderAux,"",$this->value) . "&folder=" . rawurlencode($folderAux) . "\" target=\"_blank\">
				<button type=\"button\" class=\"btn btn-info btn-icon-anim btn-square\"><i class=\"icon-cloud-download\"></i>" . 
				//"<span class=\"btn-text\">" . get_lang("_VISUALIZAR") . "</span>" .
				"</button>
			</a>";
		} else {
			//$result .= "<span class=\"help-block\">" . get_lang("_NENHUMARQUIVOENVIADO") . "</span>";
		}
		
		$result .= $this->end_field_set();
		return $result;
	}
	
	function get_db_value()
	{
		return ($this->value);
	}	
	
	function set_value($valor)
	{
		$this->value = ($valor);
	}
}

?>