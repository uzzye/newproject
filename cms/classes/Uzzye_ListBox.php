<?php

class Uzzye_ListBox extends Uzzye_Field
{	
	public $checked_value;
	public $checked_array;
	public $default_display;
	public $array_options;
	public $size;
	public $multiple;
	public $reference_model;
	public $show_button;
	public $refresh_options;
	public $help_display;  
	public $hide_help;
	public $sql_where;
	public $sql_order;
	/* 0 = campo de valor
	 * 1 = campo de display
	 * 2 = sqlWhere
	 * 3 = sqlOrderBy
	 * 4 = expressão php valor
	 * 5 = expressão php display
	 * */
	
	function __construct($name = "", $id = "", $label = "", $default_value = "", $readonly = "", $style_class = "sltNormal", $li_class = "", $sub_label = "")
	{
		$this->type = "combobox";
		$this->size = 10;
		$this->size = 10;
		$this->li_class = 'w50pu';
		$this->multiple = true;
		$this->style_class = "sltNormal";
		$this->reference_model = false;
		$this->show_button = true;
		$this->refresh_options = array();
		$this->default_display = get_lang("_COMBOSELECIONE");
		$this->help_display = "<span class=\"help-block\"> " . get_lang("_LISTBOXHELP") . "</span>";
		$this->hide_help = false;
		
		parent::__construct($name, $id, $label, $default_value, $readonly, $style_class, $li_class, $sub_label);
	}
	
	function get_display_field()
	{
		global $cryptDec;
		
		$result = "";
		$result .= $this->ini_field_set();
		$result .= $this->get_display_label();		
		
		if($this->multiple)
		{
			$this->style_class .= " sltMulti";	
			if(!is_array($this->checked_array)){
				$this->checked_array = explode(";",$this->checked_value);
			}
		}

		$result .= "<select class=\"form-control select2 " . $this->style_class. "\" ";
		if(trim($this->default_display) != "") {
			$result .= " data-placeholder=\"" . trim($this->default_display) . "\" ";
		}
		$result .= " id=\"" . ($this->id) . "\" name=\"" . ($this->name) . "";
		if($this->multiple)
		{
			$result .= "[]";
		}
		$result .= "\" size=\"" . ($this->size) . "\" ";
		if($this->multiple)
		{
			$this->help_display = "<span class=\"help-block\"> " . get_lang("_LISTBOXHELP") . "</span>";
			$result .= " multiple ";			 
		} else {
			$this->help_display = "<span class=\"help-block\"> " . get_lang("_LISTBOXHELPSINGLE") . "</span>";
		}
		if($this->readonly)
		{
			$result .= " readonly "; // onfocus=\"this.initialSelect = this.selectedIndex;\" onchange=\"this.selectedIndex = this.initialSelect;\"
		}
		if($this->required)
		{
			$result .= " required='required' ";
		}
		$result .= ">
			<optgroup>";
		if(trim($this->default_display) <> "" && trim($this->default_value) <> "")
		{
			$result .= "<option value='" . get_output($this->default_value) . "'>" . get_output($this->default_display) . "</option>";
		} else if(trim($this->default_display) != "") {
			$result .= "<option></option>";
		}
		for($i=0;$i<sizeof($this->array_options);$i++)
		{
			$result .= "<option value=\"" . get_output($this->array_options[$i][0]) . "\" ";
			if(sizeof($this->checked_array) > 0)
			{
				if(in_array($this->array_options[$i][0],$this->checked_array,true))
				{
					$result .= " selected";
				}				
			}
			else if($this->array_options[$i][0] == $this->checked_value)
			{
				$result .= " selected";
			}
			
			$result .= ">" . (trim($this->array_options[$i][1])==""?"&nbsp;":stripslashes(get_output($this->array_options[$i][1]))) . "</option>";
		}
		$result .= "</optgroup>
		</select>";		
		
		if($this->reference_model && trim($this->reference_model <> "") && file_exists(ROOT_CMS . "classes/views/" . $this->reference_model . "_view.php") && $this->show_button && !$this->readonly)
		{
			$label_aux = $this->label;
			if(trim($this->sub_label) <> "")
			{
				$label_aux = $this->sub_label;
			}			
			
			if(!$_REQUEST["resumido"])
			{		
				$randcod = gera_senha();

				$classAux = str_replace("_model","",str_replace("_view","",str_replace("_controller","",$this->reference_model)));

				$result .= "<span class=\"help-block mb-0\">
					<a id=\"link_ref_lb_" . ($this->id) . "\" data-toggle=\"modal\" data-target=\"#responsive-modal_" . $this->id . "_" . $randcod . "\" class=\"link-add-modal\"> <i class=\"fa fa-plus\"></i> " . get_lang("_ADICIONAR") . " " . $label_aux . "</a>
				</span>"; //data-href=\"" . ROOT_SERVER . ROOT . "ajax/" . $this->reference_model . "/incluir\" 

				?>
				<!-- /.modal -->
				<div id="responsive-modal_<?php echo $this->id; ?>_<?php echo $randcod; ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;" data-url="<?php echo ROOT_SERVER . ROOT; ?>ajax/<?php echo $classAux; ?>/modal_form">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								<h5 class="modal-title"><?php echo get_lang("_ADICIONAR") . " " . $label_aux; ?></h5>
							</div>
							<div class="modal-body">
								<div class="col-sm-12">
									<?php
									// MODAL DATA
									echo get_lang("_AGUARDE");
									?>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-success btn-submit btn-anim"><i class="icon-rocket"></i> <span class="btn-text"><?php echo get_lang("_SALVAR"); ?></span></button>
								<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo get_lang("_FECHAR"); ?></button>
							</div>
						</div>
					</div>
				</div>

				<script language='javascript'>
					/*Responsive Datatable Init*/

					"use strict"; 

					$(document).ready(function(e) {

						// MODAL
						
						if( $('#responsive-modal_<?php echo $this->id; ?>_<?php echo $randcod; ?>').length > 0 ){
							var modalAux = $('#responsive-modal_<?php echo $this->id; ?>_<?php echo $randcod; ?>');
							$('#responsive-modal_<?php echo $this->id; ?>_<?php echo $randcod; ?>').on('show.bs.modal', function (event) {
								showLoading(iTimeAuxFade);
								modalAux.find(".btn-submit").show();

								$.ajax({ 
			                        url: modalAux.data("url"),
			                        success: function(data) {
			                        	modalAux.find(".modal-body").html(data);

			                        	kenny(modalAux);

			                        	hideLoading(iTimeAuxFade);

			                        	// FORM

										if(modalAux.find("#form_<?php echo $classAux; ?>_resumido_ajax").length > 0) {
											var formAux = modalAux.find("#form_<?php echo $classAux; ?>_resumido_ajax");

											var options = { 
											    url: formAux.data("url-post"), 
											    beforeSubmit: function(e) {
											    	if(formAux.hasClass("blocked")) {
											    		e.preventDefault();
											    		e.stopPropagation();
											    		return false; 
											    	} else {
											    		modalAux.find(".btn-submit").find(".btn-text").html("<?php echo get_lang("_AGUARDE"); ?>");
														formAux.addClass("blocked");

														showLoading(iTimeAuxFade);
													} 
											    },
											    success: function(data) { 
											    	modalAux.find(".btn-submit").find(".btn-text").html("<?php echo get_lang("_SALVAR"); ?>");
													formAux.removeClass("blocked");

													modalAux.find(".modal-body").html(data);

											        if ($(data).find(".alert-success").length > 0) {
											        	modalAux.find(".btn-submit").hide();
											        	
														formAux.find("input").each(function(e){
															if($(this).attr("type") == "button" ||
															   $(this).attr("type") == "hidden" ||
															   $(this).attr("type") == "submit")
															{
																// NADA
															}
															else if($(this).attr("type") == "checkbox" ||
															   $(this).attr("type") == "radio")
															{
																//$(this).iCheck('uncheck');
															} else {
																$(this).val("");
															}
														});

														formAux.find("textarea").val("");

														formAux.find("select").each(function(e){
															$(this).find("option").eq(0).attr('selected','selected');
															$(this).trigger('update.fs');
														});

														<?php

									                    if(sizeof($this->refresh_options) > 0)
														{	
															echo "refreshListBox(\"" . ($this->id) . "\",\"" . $this->reference_model . "\",\"" . $this->refresh_options[0] . "\",\"" . $this->refresh_options[1] . "\",\"" . $this->refresh_options[2] . "\",\"" . $this->refresh_options[3] . "\",\"" . $cryptDec->phpEncrypt($this->refresh_options[4]) . "\",\"" . $cryptDec->phpEncrypt($this->refresh_options[5]) . "\");";
														}

														?>
													} 
													else {
											    		hideLoading(iTimeAuxFade);
											    	}
											    }
											}; 
											formAux.ajaxForm(options);

											modalAux.find(".btn-submit").unbind("click").click(function(e) {
												e.preventDefault();
												e.stopPropagation();
												formAux.trigger("submit");
											});
										}
			                        }
		                       	});
							});
							$('#responsive-modal_<?php echo $this->id; ?>_<?php echo $randcod; ?>').on('hide.bs.modal', function (event) {
								modalAux.find(".modal-body").html("<div class=\"col-sm-12\"><?php echo get_lang("_AGUARDE"); ?></div>");
							});
						}				
					});
				</script>
				<?php
			}
		}
		
		if(!$this->hide_help) {
			$result .= $this->help_display;
		}
		$result .= $this->end_field_set();
		
		return $result;
	}
	
	function get_db_value()
	{
		return output_decode($this->value);
	}	
	
	function set_value($valor)
	{
		$this->value = get_output($valor);
		$this->checked_value = $valor;
	}
	
	function get_options()
	{
		return ($this->array_options);
	}	
	
	function set_options($valor)
	{
		$this->array_options = ($valor);
	}
	
	function refresh_options($arrayOpts){
		$this->refresh_options = $arrayOpts;
	}
	
	function set_reference_item($class = "")
	{
		if(trim($class) != "") {
			$this->reference_model = $class;
			
			$objAux = new $class();

			$sWhereAux = $objAux->view->default_sql_where;
			if(trim($this->sql_where) != "") {
				if(trim($sWhereAux) != "") {
					$sWhereAux .= " AND ";
				}
				$sWhereAux .= $this->sql_where;
			}
			$sOrderAux = $objAux->view->default_sql_order;
			if(trim($this->sql_order) != "") {
				if(trim($sOrderAux) != "") {
					$sOrderAux .= ", ";
				}
				$sOrderAux .= $this->sql_order;
			}
			
			$this->set_options($objAux->get_array_options(
				$objAux->view->default_value_field,
				$objAux->view->default_view_field,
				$sWhereAux,
				$sOrderAux,
				$objAux->view->default_value_expr,
				$objAux->view->default_view_expr
			));
			$this->refresh_options(array(
				$objAux->view->default_value_field,
				$objAux->view->default_view_field,
				$sWhereAux,
				$sOrderAux,
				$objAux->view->default_value_expr,
				$objAux->view->default_view_expr
			));
		}
	}
}

?>