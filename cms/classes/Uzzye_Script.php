<?php

class Uzzye_Script extends Uzzye_Field
{	
	public $strict;
	public $onready;

	function __construct($name = "", $id = "", $label = "", $default_value = "", $readonly = "", $style_class = "form-Text", $li_class = "", $sub_label = "")
	{	
		$this->type = "script";
		
		parent::__construct($name, $id, $label, $default_value, $readonly, $style_class, $li_class, $sub_label, $max_length);		

		$this->strict = true;
		$this->onready = true;
	}
	
	function get_display_field()
	{
		$result = "";
		
		?>
		<script type="text/javascript">

		<?php

		if($this->onready) {
		?>
		$(document).ready(function(e){
		<?php }

		?>
		
			<?php
			if($this->strict) {
				echo '"use strict"';
			}
			?>

			<?php echo $this->value; ?>

		<?php

		if($this->onready) {
		?>
		});
		<?php }

		?>
		</script>
		<?php
		
		return $result;
	}
	
	function get_db_value()
	{
		return $this->value;
	}	
	
	function set_value($valor)
	{
		$this->value = $valor;
	}
}

?>