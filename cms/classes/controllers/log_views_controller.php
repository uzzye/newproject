<?php

class log_views_controller extends controller{
  
    function __construct(){	
	    $this->nome = "log_views";
		
		parent::__construct();

		$this->mostra_ranking_registro = false;
		$this->mostra_permalink_registro = false;
		$this->mostra_selecao_registro = false;
		$this->mostra_id_registro = false;
		$this->mostra_ativo_registro = false;
		$this->mostra_data_registro = false;
		$this->mostra_views_registro = false;
		$this->mostra_autor_registro = false;
		$this->mostra_lang_table = false;
    }
    
    function acaoPadrao()
	{
		// escrever aqui a acao padrao para o front-end, caso ela não seja definida, senão executa a acaoPadrao do parent
		parent::acaoPadrao();
	}
	
	function lista_registros()
	{
		parent::lista_registros();
		
		$array_campos = array("modulo_registro","id_registro","nome","id_facebook","email");
		$array_nomes = array(get_lang("_MODULO_DO_REGISTRO"),get_lang("_ID_DO_REGISTRO"),get_lang("_NOME"),get_lang("_ID_DO_FACEBOOK"),get_lang("_EMAIL"));			
		$array_foreign_keys = array("","","","","","","","","","","","","","");
		$array_expressoes = array("","","","","","","","","","","","","","");
        $array_where_filtros = array();
        $array_expressoes_sql = array();
		
		$this->view->lista($array_campos,$array_nomes,$array_expressoes,"","",$array_foreign_keys,$array_where_filtros,$array_condicoes_acoes,$array_expressoes_sql);
	}	
}
?>