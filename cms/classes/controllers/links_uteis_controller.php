<?php

class links_uteis_controller extends controller{
  
    function __construct(){	
	    $this->nome = "links_uteis";
		
		parent::__construct();

		$this->mostra_ranking_registro = false;
		$this->mostra_permalink_registro = false;
		$this->mostra_views_registro = false;
    }
    
    function acaoPadrao()
	{
		// escrever aqui a acao padrao para o front-end, caso ela não seja definida, senão executa a acaoPadrao do parent
		parent::acaoPadrao();
	}
	
	function lista_registros()
	{
		parent::lista_registros();
		
		$array_campos = array("referencia","titulo","url","target","id_idioma");
		$array_nomes = array(get_lang("_REFERENCIA"),get_lang("_TITULO"),get_lang("_URL"),get_lang("_TARGET"),get_lang("_IDIOMA"));
		$array_foreign_keys = array("","","","","titulo");
		$array_expressoes = array("","","");
		
		$this->view->lista($array_campos,$array_nomes,$array_expressoes,"","",$array_foreign_keys);
	}	
}
?>