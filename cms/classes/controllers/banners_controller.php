<?php
//include_once(ROOT_CMS . "classes/controllers/teste_controller.php");

class banners_controller extends controller{
  
    function __construct(){	
	    $this->nome = "banners";
		
		parent::__construct();
	
		$this->mostra_permalink_registro = false;
    }
    
    function acaoPadrao()
	{
		// escrever aqui a acao padrao para o front-end, caso ela não seja definida, senão executa a acaoPadrao do parent
		parent::acaoPadrao();
	}
	
	function lista_registros()
	{
		parent::lista_registros();
		
		$array_campos = array("imagem_bg_pt","titulo_pt","url","tempo_duracao","tempo_transicao","ranking","ativo");
		$array_nomes = array(get_lang("_IMAGEM_BG_PT"),get_lang("_TITULO"),get_lang("_URL"),get_lang("_TEMPO_DURACAO"),get_lang("_TEMPO_TRANSICAO"),get_lang("_RANKING"),get_lang("_ATIVO"));			
		$array_foreign_keys = array("","titulo","","","","","","","","","");
		$array_expressoes = array("\$this->controller->getImg(\$objAux)","","","","","","(\$valor==1)?get_lang(\"_SIM\"):get_lang(\"_NAO\");");
		$array_where_filtros = array("","","","","",""," IF(ativo=1,\"" . get_lang("_SIM") . "\",\"" . get_lang("_NAO") . "\") ");
		
		$this->view->lista($array_campos,$array_nomes,$array_expressoes ,"","",$array_foreign_keys,$array_where_filtros,$array_condicoes_acoes);
	}	

	function getImg($objAux) {
		$strAux = "";
		if(trim($objAux->get_var("imagem_bg_pt")) != "") {
			$folderAux = trim($objAux->upload_folders["imagem_bg_pt"]);
			if(trim($folderAux) == "") {
				$folderAux = UPLOAD_FOLDER;
			}
			$filename = $folderAux . stripslashes(get_output($objAux->get_var("imagem_bg_pt")));
			/*$fp = fopen($filename, "r");
			$fileAux = fread($fp, filesize($filename));
			$strAux = "<img src=\"data:image/png;base64," . base64_encode($fileAux) . "\" border=\"0\" width=\"100\" />";*/
			$strAux = "<img src=\"" . ($filename) . "\" border=\"0\" width=\"100\" />";
		}
		return $strAux;
	}	
}
?>