<?php

class cms_imagens_controller extends controller{
	
	function __construct(){	
	    $this->nome = "cms_imagens";
		
		parent::__construct();

		$this->mostra_ranking_registro = false;
		$this->mostra_permalink_registro = false;
		$this->mostra_views_registro = false;
    }
    
    function acaoPadrao()
    {
    	// escrever aqui a acao padrao para o front-end, caso ela não seja definida, senão executa a acaoPadrao do parent
    	parent::acaoPadrao();
    }
	
	function lista_registros()
	{
		parent::lista_registros();
		
		$array_campos = array("titulo","arquivo","modulo_registro","id_registro");
		$array_nomes = array(get_lang("_TITULO"), get_lang("_ARQUIVO"), get_lang("_MODULO_REGISTRO"), get_lang("_ID_REGISTRO"));
		
		$this->view->lista($array_campos,$array_nomes,$array_expressoes ,"","",$array_foreign_keys,$array_where_filtros,$array_condicoes_acoes);	
	}	
}
?>