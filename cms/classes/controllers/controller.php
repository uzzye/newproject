<?php

class controller{
   
    public $nome;
    public $model;
    public $view;
    
    public $acoes;
    public $verifica_perms;
        
	public $mostra_id_registro;
	public $mostra_selecao_registro;
	public $mostra_ranking_registro;
	public $mostra_ativo_registro;
	public $mostra_permalink_registro;
	public $mostra_views_registro;
	public $mostra_autor_registro;
	public $mostra_data_registro;
	public $mostra_lang_table;
   
    function __construct() {
    	global $modulo;
    	
    	$this->verifica_perms = true;
    	
    	//if($this->valida_permissao($modulo))
    	{     	    	    	
    		if(file_exists(ROOT_CMS . "classes/models/" . $this->nome."_model.php"))
    		{require_once(ROOT_CMS . "classes/models/" . $this->nome."_model.php");}
			$classe = $this->nome."_model";
    		if(class_exists($classe)) {
				$this->model = new $classe($this);
			} else {
				$this->model = null;
			}
		
    		if(file_exists(ROOT_CMS . "classes/views/" . $this->nome."_view.php"))
    		{require_once(ROOT_CMS . "classes/views/" . $this->nome."_view.php");}
			$classe = $this->nome."_view";
    		if(class_exists($classe)) {
				$this->view = new $classe($this);
			} else {
				$this->view = null;
			}				
									  
			$this->mostra_autor_registro = true;
			$this->mostra_data_registro = true;
			$this->mostra_ativo_registro = true;
			$this->mostra_id_registro = true;
			$this->mostra_selecao_registro = true;
			$this->mostra_ranking_registro = true;

			$this->mostra_permalink_registro = false;
			$this->mostra_views_registro = false;

			$this->mostra_lang_table = false;
			
			if(trim($this->acoes) == ""){
				$this->acoes = "Listar;Visualizar;Incluir;Editar;Excluir;Clonar";
			}
    	}
	}

	function get_array_ativos_controllers($sqlWhere = "",$sqlOrderBy = "",$sqlLimit = "",$join = false,$deepJoin = false, $groupBy = "", $array_expressoes_sql = "")
	{
		return $this->get_array_ativos($sqlWhere,$sqlOrderBy,$sqlLimit,$join,$deepJoin,$groupBy,$array_expressoes_sql,true);
	}
	
	function get_array_ativos($sqlWhere = "",$sqlOrderBy = "",$sqlLimit = "",$join = false,$deepJoin = false, $groupBy = "", $array_expressoes_sql = "", $return_controllers = false)
	{
		$sqlAtivo = " principal.ativo = 1 ";
		if(trim($sqlWhere) != "")
		{
			$sqlAtivo = " AND principal.ativo = 1 "; 
		}
		$sqlWhere .= $sqlAtivo;		
		return $this->get_array($sqlWhere,$sqlOrderBy,$sqlLimit,$join,$deepJoin,$groupBy,$array_expressoes_sql,$return_controllers);
	}
	
	function get_array_sql($sqlCmd = "")
	{						
		$resCmd = $this->model->get_result_sql($sqlCmd);
		
		$array_claAux = array();
	   
		$class = get_class($this->model);		
		$keys = array_keys(get_class_vars($class));
		$fields_tabela = $this->model->get_fields_name($resCmd);
		
		for($i=0;$i<$this->model->result_num_rows($resCmd);$i++)
		{	  
		  $claAux = new $class();
		  
		  for($w=0;$w<sizeof($claAux->primary_keys);$w++)
		  {
			$pk = $claAux->primary_keys[$w];
			$claAux->set_var($pk,$this->model->get_result_field($resCmd,$i,$pk)); 
		  }	
		  $claAux->carrega_dados();
		  
		  $array_claAux[sizeof($array_claAux)] = $claAux;
		}
				
		return $array_claAux;
	}
	
	function get_array_sql_modulo($sqlCmd = "")
	{						
		$resCmd = $this->model->get_result_sql($sqlCmd);
		
		$array_claAux = array();		
		
		$num_rows = $this->model->result_num_rows($resCmd);
		
		for($i=0;$i<$num_rows;$i++)
		{	  
		  $class = $this->model->get_result_field($resCmd,$i,'nome_modulo') . "_model";		  

		  $keys = array_keys(get_class_vars($class));		  
		  $fields_tabela = $this->model->get_fields_name($resCmd);
		  
		  $claAux = new $class();		  
		  for($w=0;$w<sizeof($claAux->primary_keys);$w++)
		  {		  
			$pk = $claAux->primary_keys[$w];
			$claAux->set_var($pk,$this->model->get_result_field($resCmd,$i,$pk)); 
		  }	
		  $claAux->carrega_dados();
		  
		  $array_claAux[sizeof($array_claAux)] = $claAux;		  
		}
				
		return $array_claAux;
	}
	
	function get_qtd_paginas($sqlWhere = "",$qtdPP,$join = false,$deepJoin = false)
	{
		$resCmd = $this->model->get_result($sqlWhere,"","",$join,$deepJoin);
		$num_rows = $this->model->result_num_rows($resCmd);
		return ceil($num_rows/$qtdPP);		
	}
	
	function get_array($sqlWhere = "",$sqlOrderBy = "",$sqlLimit = "",$join = false,$deepJoin = false,$groupBy = "",$array_expressoes_sql = "", $return_controllers = false)
	{
		$resCmd = $this->model->get_result($sqlWhere,$sqlOrderBy,$sqlLimit,$join,$deepJoin,$groupBy,$array_expressoes_sql);
		
		$array_claAux = array();
	   
		$class = get_class($this->model);

		$keys = array_keys(get_class_vars($class));
		$fields_tabela = $this->model->get_fields_name($resCmd);		
		
		//$class = str_replace("_model","_controller",$class);

		//if($return_controllers)
		{
			$class = str_replace("_model","",$class);
			carrega_classe($class);
		}
		
		for($i=0;$i<$this->model->result_num_rows($resCmd);$i++)
		{	  
		  $claAux = new $class();
		  
		  for($w=0;$w<sizeof($claAux->model->primary_keys);$w++)
		  {
			$pk = $claAux->model->primary_keys[$w];
			$claAux->model->set_var($pk,$this->model->get_result_field($resCmd,$i,$pk)); 
		  }	

		  // PEGA EXPRESSOES SQL E ADICIONA COMO CAMPOS
		  if(is_array($array_expressoes_sql) && sizeof($array_expressoes_sql) > 0)
		  {
		 	$sizeof = sizeof($array_expressoes_sql);
			for($w=0;$w<$sizeof;$w++)
			{
				if(trim($array_expressoes_sql[$w]) != "") {
					$varAux = "sqlExprField".$w;
					$claAux->model->set_var($varAux,$this->model->get_result_field($resCmd,$i,$varAux)); 
				}
			}
		  }

		  $claAux->carrega_dados();

		  //var_dump($claAux); echo "<br/><br/>";
		  
		  if($return_controllers) {
		  	$array_claAux[sizeof($array_claAux)] = $claAux;
		  } else {
		  	$array_claAux[sizeof($array_claAux)] = $claAux->model;
		  }
		}
				
		return $array_claAux;
	}
		
	function incluir()
	{
		global $modulo;
		
		if($this->valida_permissao($modulo,"Incluir")){
		
			$this->view->tipo_msg = $_REQUEST["tipo_msg"];
			$this->view->status = $_REQUEST["status_operacao"];
			$this->view->id = $_REQUEST["id"];
			$this->view->gera_mensagens();
		
			$this->view->monta_formulario();
		}
	}
	
	function do_incluir($clone = false, $dismiss = true)
	{		
		global $modulo;
		
		if($this->valida_permissao($modulo,"Incluir")){
		
			$this->view->monta_campos_form();
			
			$array_form_campos = $this->view->array_form_campos;
			$array_form_refs = $this->view->array_form_refs;
			$array_form_campos_padrao = $this->view->array_form_campos_padrao;
		
			$w=0;
			while($w<sizeof($array_form_campos))
			{
				// TESTAR VALIDACOES		
				
				$field = $array_form_campos[$w];
				if(trim($field->name) != "") {
					if($field->type == "file")
					{				
					}
					else if($field->type == "combobox")
					{				
						if($field->multiple){
							$strAux = "";

							for($p=0;$p<sizeof($_REQUEST[$field->name]);$p++){
								if(trim($strAux) != ""){
									$strAux .= ";";
								}
								$strAux .= $_REQUEST[$field->name][$p];
							}
							$field->value = $strAux;
						} else {
							$field->value = $_REQUEST[$field->name];
						}
					}
					else
					{
						$field->value = $_REQUEST[$field->name];		
					}
					
					$var_name = $field->name;
					$this->model->$var_name = $field->get_db_value();	
				}
				
				$w++;
			}

			$w=0;
			while($w<sizeof($array_form_campos_padrao))
			{
				// TESTAR VALIDACOES		
				
				$field = $array_form_campos_padrao[$w];
				if(trim($field->name) != "") {
					if($field->type == "file")
					{				
					}
					else if($field->type == "combobox")
					{				
						if($field->multiple){
							$strAux = "";

							for($p=0;$p<sizeof($_REQUEST[$field->name]);$p++){
								if(trim($strAux) != ""){
									$strAux .= ";";
								}
								$strAux .= $_REQUEST[$field->name][$p];
							}
							$field->value = $strAux;
						} else {
							$field->value = $_REQUEST[$field->name];
						}
					}
					else
					{
						$field->value = $_REQUEST[$field->name];		
					}
					
					$var_name = $field->name;
					$this->model->$var_name = $field->get_db_value();
				}	
				
				$w++;
			}
			
			$result = $this->model->do_incluir($array_form_campos,$array_form_refs,$array_form_campos_padrao);
			
			$status = (intval($result)>0)?1:0;

			$tipo_msg = "i";

			if($clone){
				$tipo_msg = "c";
			}

			$this->view->tipo_msg = $tipo_msg;
			$this->view->status = $status;
			$this->view->id = $_REQUEST["id"];
			$this->view->gera_mensagens($dismiss);
		
			/*ob_clean();
			if($_REQUEST["resumido"] == "true"){				
				header("Location: index_resumido.php?modulo=".$modulo."&acao=incluir&tipo_msg=".$tipo_msg."&resumido=true&status_operacao=".$status."&id=");//.$result);
				die();
			}
			else
			{		
				if(intval($_REQUEST["add_mais"]) == 1){
					header("Location: index.php?modulo=".$modulo."&acao=incluir&tipo_msg=".$tipo_msg."&status_operacao=".$status."&id=");//.$result);
				} else {
					header("Location: index.php?modulo=".$modulo."&tipo_msg=".$tipo_msg."&status_operacao=".$status."&id=");//.$result);
				}
				die();
			}*/
		}
	}
	
	function visualizar()
	{
		global $modulo;
		
		if($this->valida_permissao($modulo,"Visualizar")){
			// tratar retorno
			$this->view->monta_formulario($_REQUEST["id"],true);
		}
	}

	function clonar()
	{
		global $modulo;
		
		if($this->valida_permissao($modulo,"Clonar")){
			// tratar retorno
			$this->view->monta_formulario($_REQUEST["id"],false,true);
		}
	}

	function do_clonar() {
		$this->do_incluir(true);
	}
	
	function editar()
	{
		global $modulo;
		
		if($this->valida_permissao($modulo,"Editar")){
			// tratar retorno
			$this->view->monta_formulario($_REQUEST["id"]);
		}
	}
	
	function do_editar()
	{
		global $modulo;

		//var_dump($_REQUEST); die(); // DEBUG POST
		
		if($this->valida_permissao($modulo,"Editar")){
			$this->view->monta_campos_form();
			
			$array_form_campos = $this->view->array_form_campos;
			$array_form_refs = $this->view->array_form_refs;		
			$array_form_campos_padrao = $this->view->array_form_campos_padrao;
			
			if(trim($_REQUEST["id"]) != "")
			{
				$this->model->id = $_REQUEST["id"];
				$this->model->carrega_dados();
			}
			
			$w=0;
			while($w<sizeof($array_form_campos))
			{
				// TESTAR VALIDACOES		
				
				$field = $array_form_campos[$w];
				if(trim($field->name) != "") {
					if($field->type == "file")
					{				
					}
					else if($field->type == "combobox")
					{				
						if($field->multiple){
							$strAux = "";

							for($p=0;$p<sizeof($_REQUEST[$field->name]);$p++){
								if(trim($strAux) != ""){
									$strAux .= ";";
								}
								$strAux .= $_REQUEST[$field->name][$p];
							}
							$field->value = $strAux;
						} else {
							$field->value = $_REQUEST[$field->name];
						}
					}
					else
					{
						$field->value = $_REQUEST[$field->name];
					}
					
					$var_name = $field->name;
					$this->model->$var_name = $field->get_db_value();
				}
				
				$w++;
			}

			$w=0;
			while($w<sizeof($array_form_campos_padrao))
			{
				// TESTAR VALIDACOES		
				
				$field = $array_form_campos_padrao[$w];
				if(trim($field->name) != "") {
					if($field->type == "file")
					{				
					}
					else if($field->type == "combobox")
					{				
						if($field->multiple){
							$strAux = "";

							for($p=0;$p<sizeof($_REQUEST[$field->name]);$p++){
								if(trim($strAux) != ""){
									$strAux .= ";";
								}
								$strAux .= $_REQUEST[$field->name][$p];
							}
							$field->value = $strAux;
						} else {
							$field->value = $_REQUEST[$field->name];
						}
					}
					else
					{
						$field->value = $_REQUEST[$field->name];
					}
					
					$var_name = $field->name;
					$this->model->$var_name = $field->get_db_value();
				}
				
				$w++;
			}
			
			$result = $this->model->do_editar($array_form_campos,$array_form_refs,$array_form_campos_padrao);
			
			$status = ($result?1:0);

			$this->view->tipo_msg = "e";
			$this->view->status = $status;
			$this->view->id = $_REQUEST["id"];
			$this->view->gera_mensagens();
	
			/*ob_clean();		
			header("Location: index.php?modulo=".$modulo."&tipo_msg=e&status_operacao=".$status."&id=".$_REQUEST["id"]);
			//die();*/
		}
	}
	
	function excluir()
	{
		$this->do_excluir();
	}

	function do_excluir_massa()
	{
		global $modulo;

		if($this->valida_permissao($modulo,"Excluir")){
			if(trim($_REQUEST["regids"]) != "")
			{
				$result = $this->model->do_excluir_massa();
				$status = ($result?1:0);

				$this->view->tipo_msg = "r";
				$this->view->status = $status;
				$this->view->id = $_REQUEST["id"];
				$this->view->gera_mensagens();
		
				//ob_clean();		
				//header("Location: index.php?modulo=".$modulo."&tipo_msg=r&status_operacao=".$status."&id=".$_REQUEST["id"]);
				//die();
			}
		}
	}
	
	function do_excluir()
	{
		global $modulo;
		
		if($this->valida_permissao($modulo,"Excluir")){
			if(trim($_REQUEST["id"]) != "")
			{
				$this->model->id = $_REQUEST["id"];
				$this->model->carrega_dados();
			}
		
	  	  	$result = $this->model->do_excluir();
			$status = ($result?1:0);

			$this->view->tipo_msg = "r";
			$this->view->status = $status;
			$this->view->id = $_REQUEST["id"];
			$this->view->gera_mensagens();
		
			//ob_clean();		
			//header("Location: index.php?modulo=".$modulo."&tipo_msg=r&status_operacao=".$status."&id=".$_REQUEST["id"]);
			//die();
		}
	}

	function ativar_desativar()
	{
		global $modulo;
		
		if($this->valida_permissao($modulo,"AtivarDesativar")){
			if(trim($_REQUEST["id"]) != "")
			{
				$this->model->id = $_REQUEST["id"];
				$this->model->carrega_dados();
			}

			// Mostra o contrário
			if(intval($this->model->ativo) == 1) {
				echo get_lang("_INATIVO");
			} else {
				echo get_lang("_ATIVO");
			}
		
	  	  	$result = $this->model->do_ativar_desativar();
			$status = ($result?1:0);
		}
	}
	
	function count_reg($sqlWhere = "",$join = true,$deepJoin = false)
	{
		return $this->model->count_reg($sqlWhere,$join,$deepJoin);
	}

	function count_reg_ativos($sqlWhere = "",$join = true,$deepJoin = false)
	{
		return $this->model->count_reg_ativos($sqlWhere,$join,$deepJoin);
	}
	
	function get_array_pg($nrReg)
	{
		if (isset($_GET["pagina"])){
			$pagina = $_REQUEST["pagina"];
		}else{
			$pagina = 0;
		}
				
		$tot_pg = ceil($nrReg/CONFIG_QTD_REG_PP);
		//$qtdPg = CONFIG_QTD_PAG_PAGINACAO;
		$qtdPg = "";
		if($tot_pg <= $qtdPg)
		{
			$qtdPg = $tot_pg;
		}
		
		$paginacao = floor($pagina/$qtdPg)+1;
		
		$array_pg = array();		
		for($w=0;$w<$qtdPg;$w++)
		{			
		    $valor = ($w+1)*$paginacao;
			
			$array_pg[sizeof($array_pg)] = $valor;
		}		
		
		return $array_pg;
	}
	
	function carrega_dados()
	{
		$this->model->carrega_dados();
		return $this->model;
	}
	
	function set_var($var,$value)
	{
		$this->model->$var = $value;
	}
	
	function get_var($var)
	{
		return $this->model->$var;
	}

	function get_var_format($var)
	{
		return $this->view->get_var_format($var);
	}
	
	function get_last_var($var,$bEdit = false)
	{
		if($bEdit){
			$array_obj = $this->get_array("","ranking DESC, IF(!ISNULL(data_atualizacao),data_atualizacao,data_criacao) DESC, data_criacao DESC",1);
		} else {
			$array_obj = $this->get_array("","ranking DESC, data_criacao DESC",1);
		}
		if(is_array($array_obj) && sizeof($array_obj) > 0)
		{	
			$obj = $array_obj[0];
			return $obj->get_var($var);	
		} else {
			return null;
		}
	}
	
	function get_array_options($field_id,$field_display,$sqlWhere = "",$sqlOrderBy = "",$expressao_id = "",$expressao_display = "", $groupBy = "", $arraySel = array())	
	{
		global $acao, $modulo;

		if($acao != "editar" && $_REQUEST["ajax_acao"] != "editar") {
			$sqlSel = "";
			foreach($arraySel as $sel) {
				$sqlSel .= " OR principal.id = \"" . $sel . "\" ";
			}
			if(trim($sqlWhere) != "") {
				if(stristr($sqlWhere, "id=") || stristr($sqlWhere, "id =")) {
					// NADA
				} else {
					$sqlWhere .= " AND (principal.ativo = 1 " . $sqlSel . ")";
				}
			} else {
				$sqlWhere .= "(principal.ativo = 1 " . $sqlSel . ")";
			}
		}
		
		$array_obj = $this->get_array($sqlWhere,$sqlOrderBy,"",false,false,$groupBy);
		$array_options = array();
		$w=0;
		while($w<sizeof($array_obj))
		{	
			$obj = $array_obj[$w];	
			$objAux = $obj;	
			$objAuxRef = $obj;
			
			$val_id = (trim($field_id) <> "")?$obj->$field_id:"";
			$val_display = (trim($field_display) <> "")?$obj->$field_display:"";
			
			if(trim($expressao_id) <> "")
			{
				if(!strstr($expressao_id,"return")){
					$expressao_id = "return " . $expressao_id;}
				if(substr(trim($expressao_id),-1 <> ";")){
					$expressao_id = $expressao_id . ";";}
				$val_id = eval($expressao_id);
			}
			
			if(trim($expressao_display) <> "")
			{
				if(!strstr($expressao_display,"return")){
					$expressao_display = "return " . $expressao_display;}
				if(substr(trim($expressao_display),-1 <> ";")){
					$expressao_display = $expressao_display . ";";}
				$val_display = eval($expressao_display);
			}
			
			$array_options[sizeof($array_options)] = array($val_id,$val_display);
			$w++;
		}
		return $array_options;
	}		
	
	function localiza($array_vars,$array_values,$array_compare = null, $return_controllers = false)	
	{
		return $this->model->localiza($array_vars,$array_values,$array_compare, $return_controllers);
	}
	
	function get_objeto_referencia($campo)
	{
		return $this->model->get_objeto_referencia($campo);
	}
	
	function get_reference_item($modulo)
	{
		return $this->model->get_reference_item($modulo);
	}

	function get_item_data_table($modulo)
	{
		return $this->model->get_item_data_table($modulo);
	}

	function get_item_img_gallery($modulo)
	{
		return $this->model->get_item_img_gallery($modulo);
	}

	function get_item_file_gallery($modulo)
	{
		return $this->model->get_item_file_gallery($modulo);
	}
	
	function get_reference_model($field)
	{
		return $this->model->get_reference_model($field);
	}

	function get_var_referencia($modulo)
	{
		return $this->model->get_var_referencia($modulo);
	}
	
	function get_array_id_ref($nomeTabela,$sqlWhere,$campo_retorno)
	{
		return $this->model->get_array_id_ref($nomeTabela,$sqlWhere,$campo_retorno);
	}
	
	function get_foreign_key_index($campo)
	{
		return $this->model->get_foreign_key_index($campo);
	}

	function get_foreign_key_model($campo)
	{
		return $this->model->get_foreign_key_model($campo);
	}

	function inclui($cms = true,$last_id = false)
	{
		return $this->model->inclui($cms,$last_id);
	}	
	
	function inclui_front($last_id = false)
	{
		return $this->model->inclui(false,$last_id);
	}	
	
	function edita($cms = true,$last_id = false)
	{
		return $this->model->edita($cms,$last_id);
	}	
	
	function exclui()
	{
		return $this->model->exclui();
	}

	function inicia_dados()
	{		
		return $this->model->inicia_dados();
	}
	
	function is_required($field)
	{
		$return = false;
		if(in_array($field,$this->model->array_required_fields,true))
		{
			$return = true;
		}
		return $return;
	}
	
	function acaoPadrao()
	{
		// acaoPadrao passa a ser a chamada de listagem do cms;
		$this->lista_registros();
	}
	
	function lista_registros()
	{
		global $modulo;
		
		//$this->valida_permissao($modulo,"Listar",false);
    	
		/*	
		 * Na view filha deve ser feita a chamada para a função lista que é responsável por montar filtros e grade de registros
		 * Podem ser utilizados os seguintes parâmetros
		 * 
		 * $array_campos -> obrigatório -> array de strings informando os nomes dos atributos da classe que serão utilizados na listagem 
		 *
		 * $array_nomes -> obrigatórios -> array de strings com os nomes de exibição dos títulos. posições do array se equivalem às posições em "array_campos"
		 * 
		 * $array_expressoes -> array de expressões PHP para tratamento do valor final do resultado SQL via função "eval". posições do array se equivalem às posições em "array_campos"
		 * 
		 * $sqlWhere -> string que será colocada na cláusula WHERE do SQL
		 * 
		 * $sqlOrderBy -> string que será colocada na cláusula ORDER BY do SQL
		 * 
		 * $array_foreign_keys -> array de campos de exibição da tabela referenciada pela chave estrangeira. posições do array se equivalem às posições em "array_campos", ou seja, a chave estrangeira é referente ao campo de mesma posição em "array_campos"
		 * 
		 * $array_where_filtros -> array de strings que serão utilizadas na cláusula WHERE do SQL para filtragem. posições do array se equivalem às posições em "array_campos"
		 * 
		 * $array_condicoes_acoes -> array de expressões PHP que devem resultar em TRUE ou FALSE tratadas via função "eval". posições do array se equivalem às posições em "array_acoes_cms" da classe "view"
		 *
		 * $array_expressoes_sql -> array de expressões executadas em SQL para mostrar resultados, semelhante ao $array_expressoes, sendo que o PHP é aplicado em cima do SQL, ou seja, este array deve modificar os valores tratados originalmente no $array_expressoes
		 *
		 * */
	}
	
	function valida_permissao($modulo_aux,$acao_aux = "",$echo = true)
	{
		global $acao;

		if($acao_aux == "logout" || $acao_aux == "logoff" ||
		   $acao == "logout" || $acao == "logoff") {
			return true;
		} else {
			if(strpos($_SESSION["emailLogin"],"@uzzye.com") !== false || !$this->verifica_perms){
				return true;
			}
			else{
				$modulos = new cms_modulos_model();
				$modulos = $modulos->localiza(array("link"),array($modulo_aux));
				if(sizeof($modulos) == 0){
					return true;
				}
				else{
					$idModuloAux = intval($modulos[0]->get_var("id"));
					
					$usuario = new cms_usuarios_model();
	    			/*$usuario->set_var("id",$_SESSION["idLogin"]);
	    			$usuario->carrega_dados();*/
				
					$sqlWhere = "";
					if(trim($acao_aux) != ""){
						$sqlWhere = " AND (LOCATE(\"" . $acao_aux . "\",CONCAT(\";\",acoes,\";\")) OR
						 				   LOCATE(\"Todas\",CONCAT(\";\",acoes,\";\")))";
					}
					
					$array_modulos = $usuario->get_array_id_ref(DBTABLE_CMS_MODULOS_USUARIOS,"id_usuario = " . intval($_SESSION["idLogin"]),"id_modulo");

	    			if(sizeof($array_modulos)>0){
			    		$array_modulos = $usuario->get_array_id_ref(DBTABLE_CMS_MODULOS_USUARIOS,"id_usuario = " . intval($_SESSION["idLogin"]) . $sqlWhere,"id_modulo");
		
		    			if(in_array($idModuloAux,$array_modulos)){
		    				return true;
		    			}
		    			else{
		    				if($echo){
	    						if(trim($acao_aux) == ""){
	    							echo "<div class=\"erroModulo\">" . get_lang("_VOCENAOTEMPERMISSAOACESSOMODULO") . "</div>";
	    						}
	    						else{
	    							//echo "<div class=\"erroModulo\">" . get_lang("_VOCENAOTEMPERMISSAOACAO") . "</div>";
	    						}
	    						//echo "<br>Aguarde enquanto você é redirecionado...";
			    				//echo "<meta http-equiv=\"refresh\" content=\"2;url=" . ROOT_SERVER . ROOT . "\" />";
	    					}
	    		
	    					return false;
		    			}
	    			}
	 	   			else{
	    				$array_modulos = $usuario->get_array_id_ref(DBTABLE_CMS_MODULOS_GRUPOS_USUARIOS,"id_grupo = " . intval($_SESSION["idGrupo"]) . $sqlWhere,"id_modulo");
	    			
	    				if(in_array($idModuloAux,$array_modulos)){
		    				return true;
	    				}
	    				else{
	    					if($echo){
	    						if(trim($acao_aux) == ""){
	    							echo "<div class=\"erroModulo\">" . get_lang("_VOCENAOTEMPERMISSAOACESSOMODULO") . "</div>";
	    						}
	    						else{
	    							//echo "<div class=\"erroModulo\">" . get_lang("_VOCENAOTEMPERMISSAOACAO"). "</div>";
	    						}
	    						//echo "<br>Aguarde enquanto você é redirecionado...";
			    				//echo "<meta http-equiv=\"refresh\" content=\"2;url=" . ROOT_SERVER . ROOT . "\" />";
	    					}
	    		
	    					return false;
	    				}
	    			}
				}
			}
		}
	}
	
	function gera_xml($interno = false,$xml_full = true,$gera_ids = false,$fields = "",$filtros = NULL,$refs = NULL,$not = array(),$html = true,$array_refs = array())
	{
		global $db;

		$str_xml = "";
		if($xml_full)
		{
			$str_xml .= "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
			$str_xml .= "<xml>";
		}
		$str_xml .= "<" . $this->nome . ">";
		
		$array_fields = $this->model->get_fields_name();
		$sWhere = "";
		if($filtros <> NULL && $filtros <> "")
		{
			if(is_array($filtros)){
				foreach($filtros as $filtro)
				{
					if(trim($sWhere) <> "")
					{$sWhere .= " AND ";}
					$sWhere .= " $filtro "; 
				}
			}
			else
			{
				if(trim($sWhere) <> "")
				{$sWhere .= " AND ";}
				$sWhere .= " $filtros ";
			}
		}
		//var_dump($sWhere); die();
		$array_obj = $this->get_array_ativos($sWhere);
		foreach($array_obj as $objAux)
		{
			$str_xml .= "<item>";
			foreach($array_fields as $fieldAux)
			{
				if(((trim($fieldAux) <> "id") &&
				   (trim($fieldAux) <> "data_criacao") &&
				   (trim($fieldAux) <> "data_atualizacao") &&
				   (trim($fieldAux) <> "usuario_criacao") &&
				   (trim($fieldAux) <> "usuario_atualizacao") &&
				   (trim($fieldAux) <> "ativo")) || $interno)	
				{
					if(((substr($fieldAux,0,3) <> "id_") &&
				   	   (substr($fieldAux,0,-3) <> "_id")) || $gera_ids)
					{
						if(!in_array($fieldAux, $not)) {
							$str_xml .= "<" . $fieldAux . ">";
							if($html)
							{ 
								$str_xml .= encode_xml(get_output($objAux->get_var($fieldAux)));
							}
							else
							{							
								//$str_xml .= encode_xml(htmlspecialchars_decode(html_entity_decode($objAux->get_var($fieldAux))));
								$str_xml .= encode_xml(get_output($objAux->get_var($fieldAux)));
							} 
							$str_xml .= "</" . $fieldAux . ">";
						}
					}
				}
			}

			if($refs == true || $refs == "true" || intval($refs) == 1) {
				if(sizeof($this->model->reference_items) > 0)
				{
					foreach($this->model->reference_items as $refAux)
					{
						if(!in_array($refAux[1], $array_refs) && !in_array($refAux[1], $not)) {
							$array_refs[sizeof($array_refs)] = $refAux[1];

							$classAux = $refAux[1] . "_controller";
							$objAux2 = new $classAux();
							
							$filtro_aux = " principal." . $refAux[2] . " = " . $db->escape_string($objAux->get_var($refAux[0])) . " ";
			
							$str_xml .= $objAux2->gera_xml($interno,false,$gera_ids,$fields,$filtro_aux,$refs,$not,$html,$array_refs);
						}
					}
				}
			}
			
			$str_xml .= "</item>";			
		}

		$str_xml .= "</" . $this->nome . ">";
		if($xml_full)
		{
			$str_xml .= "</xml>";
		}
		
		return $str_xml;
	}
	
	function get_sql_filtros($campo, $campo_fk = "", $whereFiltros = "", $sqlWhere = "", $valor_filtro = "", $comparador = "LIKE")
	{		
		if(trim($comparador) == "LIKE")
		{
			if(is_array($valor_filtro))
			{
				for($w=0;$w<sizeof($valor_filtro);$w++){
					$valor_filtro[$w] = "%" . $valor_filtro[$w] . "%";}
			}
			else
			{
				$valor_filtro = "%" . $valor_filtro . "%";
			}
		}
		
		if((is_array($valor_filtro) && sizeof($valor_filtro) > 0 && trim($valor_filtro[0]) <> "") || (!is_array($valor_filtro) && trim($valor_filtro) <> ""))
		{
			if(trim($sqlWhere) <> "")
			{
				$sqlWhere .= " AND ";
			}
				
			if(trim($campo_fk) <> "")
			{
				$i_index = $this->get_foreign_key_index($campo);
				if(is_array($valor_filtro) && sizeof($valor_filtro) > 0 && trim($valor_filtro[0]) <> "")
				{
					$sqlWhere .= " ( ";
					for($w=0;$w<sizeof($valor_filtro);$w++){
						if($w>0){$sqlWhere .= " OR ";}
						$sqlWhere .= " aux" . $i_index . "." . $campo_fk . " " . $comparador . " \"" . $valor_filtro[$w] . "\" ";												
					}
					$sqlWhere .= " ) ";
				}
				else
				{
					$sqlWhere .= " (aux" . $i_index . "." . $campo_fk . " " . $comparador . " \"" . $valor_filtro . "\") ";
				}									
			}
			else
			{
				if(trim($whereFiltros) <> "")
				{
					if(strstr($whereFiltros,"principal."))
					{
						if(is_array($valor_filtro) && sizeof($valor_filtro) > 0 && trim($valor_filtro[0]) <> "")
						{
							$sqlWhere .= " ( ";
							for($w=0;$w<sizeof($valor_filtro);$w++){
								if($w>0){$sqlWhere .= " OR ";}
								$sqlWhere .= " " . $whereFiltros . " " . $comparador . " \"" . $valor_filtro[$w] . "\" ";															
							}
							$sqlWhere .= " ) ";
						}
						else
						{
							$sqlWhere .= " (" . $whereFiltros . " " . $comparador . " \"" . $valor_filtro . "\") ";
						}
					}
					else
					{
						if(is_array($valor_filtro) && sizeof($valor_filtro) > 0 && trim($valor_filtro[0]) <> "")
						{
							$sqlWhere .= " ( ";
							for($w=0;$w<sizeof($valor_filtro);$w++){
								if($w>0){$sqlWhere .= " OR ";}
								$sqlWhere .= " " . str_replace($campo,"principal." . $campo,$whereFiltros) . " " . $comparador . " \"" . $valor_filtro[$w] . "\" ";														
							}
							$sqlWhere .= " ) ";
						}
						else
						{
							$sqlWhere .= " (" . str_replace($campo,"principal." . $campo,$whereFiltros) . " " . $comparador . " \"" . $valor_filtro . "\") ";
						}
					}						
				}
				else
				{
					if(is_array($valor_filtro) && sizeof($valor_filtro) > 0 && trim($valor_filtro[0]) <> "")
					{
						$sqlWhere .= " ( ";
						for($w=0;$w<sizeof($valor_filtro);$w++){
							if($w>0){$sqlWhere .= " OR ";}
							$sqlWhere .= " principal." . $campo . " " . $comparador . " \"" . $valor_filtro[$w] . "\" ";																			
						}
						$sqlWhere .= " ) ";
					}
					else
					{
						$sqlWhere .= " (principal." . $campo . " " . $comparador . " \"" . $valor_filtro . "\") ";
					}
				}
			}
		}
		return $sqlWhere;
	}

	function get_bread_crumbs()
	{		
		global $modulo, $acao;

		$strNavegacao = "";

		$array_obj = new cms_modulos();		
		$array_obj = $array_obj->localiza(array("link"),array($this->nome));
		$objAux = $array_obj[0];
				
		if(intval($objAux->id) > 0)
		{
			/*<li><a href="index.html"><?php echo ($modAux->get_var("nome")); ?></a></li>
                <li class="active"><span><?php echo (get_texto_acao($acao))?></span></li>*/
            $strNavegacao .= "<li>";
            
            if(intval($objAux->modulo_pai) > 0 && trim($objAux->link) <> "") {
            	$strNavegacao .= "<a rel=\"address\" href=\"#/" . $objAux->link . "\">" . get_output($objAux->nome) . "</a>";		
			} else {
				$strNavegacao .= "<span>" . get_output($objAux->nome) . "</span>";		
			}
			
			$strNavegacao .= "</li>";

			$w=0;
	
			while(intval($objAux->modulo_pai) > 0) {
				$strAux = "<li>";

				$objAux = $objAux->get_objeto_referencia("modulo_pai");								

            	if(intval($objAux->modulo_pai) > 0 && trim($objAux->link) <> "") {
					$strAux .= "<a rel=\"address\" href=\"#/" . $objAux->link . "\">" . get_output($objAux->nome) . "</a>";
				} else {
					$strAux .= "<span>" . get_output($objAux->nome) . "</span>";
				}

				$strAux .= "</li>";

	            $strNavegacao = $strAux . $strNavegacao;
		
				$w++;
			}		

			if(trim(get_texto_acao($acao)) != "") {
				$strNavegacao .= "<li>
					<a rel=\"address\" href=\"#/" . $modulo . "/" . $acao . "/\">" . get_texto_acao($acao) . "</a>
				</li>";
			}							
		}
		
		return stripslashes($strNavegacao);
	}

	function modal_form() {
		global $modulo;

		if($this->valida_permissao($modulo,"Incluir")){

			carrega_classe($modulo);
			$objAux = new $modulo();
			$objAux->inicia_dados();
			$objAux->view->monta_formulario("",false,false,true);
		}
	}

	function do_modal_form() {
		global $modulo;

		if($this->valida_permissao($modulo,"Incluir")){
			return $this->do_incluir(false, false);
		}
	}

	function get_nome_tabela()
	{
		return $this->model->get_nome_tabela();
	}

	function get_upload_folder($var = "")
	{
		return $this->model->get_upload_folder($var);
	}

	function exportar_csv()
	{
		$this->exportar("csv");
	}

	function exportar($tipo = "")
	{
		global $modulo;

		if(trim($tipo) == "") {
			$tipo = "csv";
		}
		
		if($this->valida_permissao($modulo,"Exportar")){
			// tratar retorno
			$this->view->monta_exportacao($_REQUEST["id"], $tipo);
		}
	}
	
	function do_exportar()
	{
		global $modulo;

		$tipo = $_REQUEST["tipo"];
		if(trim($tipo) == "") {
			$tipo = "csv";
		}

		$nomeClasse = str_replace("_controller","",$this->nome);

		carrega_classe($nomeClasse);

		$its = new $nomeClasse();
		$keys = $its->model->get_fields_name();
		$sWhere = "(true)";
		if(trim($_REQUEST["data_inicio"]) != "") {
			$sWhere .= " AND data_criacao >= \"" . decode_data($_REQUEST["data_inicio"]) . "\" ";
		} 
		if(trim($_REQUEST["data_fim"]) != "") {
			$sWhere .= " AND data_criacao <= \"" . decode_data($_REQUEST["data_fim"]) . "\" ";
		}
		$its = $its->get_array_ativos_controllers($sWhere,"data_atualizacao DESC, data_criacao DESC");

		$ext = "";

		// Se for CSV
		if(strtoupper($tipo) == "CSV") {
			$ext = "." . strtolower($tipo);

			$temp = tmpfile();

			if(is_array($its) && sizeof($its) > 0){
				$arrAux = array();
				foreach($keys as $key){
					array_push($arrAux, $key);
				}
				fputcsv($temp, $arrAux);
				foreach($its as $it) {
					$arrAux = array();
					foreach($keys as $key){
						$val = strip_tags(stripslashes(get_output($it->get_var_format($key))));
						array_push($arrAux, $val);
					}
					fputcsv($temp, $arrAux);
				}
			}

			fseek($temp, 0);

			$metaDatas = stream_get_meta_data($temp);
		}

		header("Content-type: application/octetstream");
	  	header("Content-type: text/plain; charset=UTF-8");
	  	header("Content-disposition: attachment; filename=" . $nomeClasse . "-" . Date("YmdHis") . "" . strtolower($ext));
	  	header("Pragma: no-cache");
	  	header("Expires: 0");
		ob_clean();
		flush();

		// Se for CSV
		if(strtoupper($tipo) == "CSV") {
			print file_get_contents($metaDatas["uri"]);
		}

		fclose($temp);

		exit;
	}

	function log_view($arrayInfos = null) {
		return $this->model->log_view($arrayInfos);
	}

	function gera_permalink($dbSave = false) {

		$moduloAux = $obj = $objAux = $this->model;

		$value_url = stripslashes(get_output($this->get_var("id")));
		//$view_url = stripslashes(get_output($this->get_var("id")));
		$view_url = "";

		if(trim($this->view->default_value_field) != "") {
			$value_url = stripslashes(get_output($this->get_var($this->view->default_value_field)));
		}
		if(trim($this->view->default_view_field) != "") {
			$view_url = stripslashes(get_output($this->get_var($this->view->default_view_field)));
		}

		if(trim($this->view->default_value_expr) != "") {
			$val = stripslashes(get_output(eval($this->default_value_expr)));
			if(trim($val) != "") {
				$view_url = $val;
			}
		}
		if(trim($this->view->default_view_expr) != "") {
			$val = stripslashes(get_output(eval($this->view->default_view_expr)));
			if(trim($val) != "") {
				$view_url = $val;
			}
		}

		$value_url = gera_titulo_amigavel($value_url);
		if(trim($value_url) != "") {
			$value_url = "/" . $value_url;
		}

		$view_url = gera_titulo_amigavel($view_url);
		if(trim($view_url) != "") {
			$view_url = "/" . $view_url;
		}

		$permalink = $this->nome . $view_url . $value_url;

		if($dbSave) {
			return $this->model->salva_permalink($this->get_var("id"), $permalink);
		} else {
			return $permalink;
		}
	}

	function get_view_field_value() {
		return $this->view->get_view_field_value();
	}

	function get_id_field_value() {
		return $this->view->get_id_field_value();
	}

	function get_registros($array_campos,$array_nomes,$array_expressoes = "",$sqlWhere = "",$sqlOrderBy = "",$array_foreign_keys = "",$array_where_filtros = "",$array_condicoes_acoes = "",$array_expressoes_sql = "") {
		return $this->view->get_registros($array_campos,$array_nomes,$array_expressoes,$sqlWhere,$sqlOrderBy,$array_foreign_keys,$array_where_filtros,$array_condicoes_acoes,$array_expressoes_sql);
	}

	function get_registros_html($array_campos,$array_nomes,$array_expressoes = "",$sqlWhere = "",$sqlOrderBy = "",$array_foreign_keys = "",$array_where_filtros = "",$array_condicoes_acoes = "",$array_expressoes_sql = "") {
		return $this->view->get_registros_html($array_campos,$array_nomes,$array_expressoes,$sqlWhere,$sqlOrderBy,$array_foreign_keys,$array_where_filtros,$array_condicoes_acoes,$array_expressoes_sql);
	}
}
?>