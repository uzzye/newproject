<?php

class cadastros_controller extends controller{
  
    function __construct(){ 
        $this->nome = "cadastros";
        
        parent::__construct();

        $this->mostra_ranking_registro = false;
        $this->mostra_permalink_registro = false;
        $this->mostra_selecao_registro = false;
        $this->mostra_ativo_registro = false;
        $this->mostra_views_registro = false;
        $this->mostra_autor_registro = false;
        $this->mostra_lang_table = false;
        
        $this->mostra_id_registro = true;
        $this->mostra_data_registro = true;
    }
    
    function acaoPadrao()
    {
        // escrever aqui a acao padrao para o front-end, caso ela não seja definida, senão executa a acaoPadrao do parent
        parent::acaoPadrao();
    }
    
    function lista_registros()
    {
        parent::lista_registros();
        
        $array_campos = array("nome","email","url");
        $array_nomes = array(get_lang("_NOME"),get_lang("_EMAIL"),get_lang("_URL"));            
        $array_foreign_keys = array("","","","");
        $array_expressoes = array("","","","");
        $array_where_filtros = array();
        $array_expressoes_sql = array();
        
        $this->view->lista($array_campos,$array_nomes,$array_expressoes,"","",$array_foreign_keys,$array_where_filtros,$array_condicoes_acoes,$array_expressoes_sql);
    }   
}
?>