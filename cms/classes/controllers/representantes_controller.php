<?php
include_once(ROOT_CMS . "classes/controllers/estados_controller.php");
include_once(ROOT_CMS . "classes/estados.php");

class representantes_controller extends controller{
  
    function __construct(){ 
        $this->nome = "representantes";
        
        parent::__construct();
    }
    
    function acaoPadrao()
    {
        // escrever aqui a acao padrao para o front-end, caso ela não seja definida, senão executa a acaoPadrao do parent
        parent::acaoPadrao();
    }
    
    function lista_registros()
    {
        parent::lista_registros();
        
        $array_campos = array("estados","titulo","endereco","email","telefone");
        $array_nomes = array(get_lang("_ESTADOS"),get_lang("_TITULO"),get_lang("_ENDERECO"),get_lang("_EMAIL"),get_lang("_TELEFONE"));            
        $array_foreign_keys = array("","","","","","");
        $array_expressoes = array("","","","","","");
        $array_where_filtros = array();
        $array_expressoes_sql = array();
        
        $this->view->lista($array_campos,$array_nomes,$array_expressoes,"","",$array_foreign_keys,$array_where_filtros,$array_condicoes_acoes,$array_expressoes_sql);
    }   
}
?>