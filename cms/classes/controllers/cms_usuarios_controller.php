<?php

require_once(ROOT_CMS . "classes/controllers/cms_grupos_usuarios_controller.php");

class cms_usuarios_controller extends controller{
	
	private $safeMode;
  
    function __construct(){	
	    $this->nome = "cms_usuarios";
	    $this->safeMode = true;
		
		parent::__construct();

		$this->mostra_ranking_registro = false;
		$this->mostra_permalink_registro = false;
		$this->mostra_views_registro = false;
    }
    
    function acaoPadrao()
    {
    	// escrever aqui a acao padrao para o front-end, caso ela não seja definida, senão executa a acaoPadrao do parent
    	parent::acaoPadrao();
    }
	
	function lista_registros()
	{
		parent::lista_registros();
		
		$array_campos = array("nome","email","disponivel","id_grupo");
		$array_nomes = array(get_lang("_NOME"), get_lang("_EMAIL"), get_lang("_DISPONIVEL"), get_lang("_GRUPO_USUARIOS"));
		$array_foreign_keys = array("","","","titulo");
		$array_expressoes = array("","","(\$valor==1)?get_lang(\"_SIM\"):get_lang(\"_NAO\");");
		$array_where_filtros = array("",""," IF(principal.disponivel=1,\"" . get_lang("_SIM") . "\",\"" . get_lang("_NAO") . "\") ");
		
		/*$array_condicoes_acoes = array(); 
		$array_condicoes_acoes[0] = "(\$moduloAux->disponivel!=1);";*/  
				
		$this->view->lista($array_campos,$array_nomes,$array_expressoes ,"","",$array_foreign_keys,$array_where_filtros,$array_condicoes_acoes);	
	}	
	
	function do_incluir($clone = false, $dismiss = true) {
		global $db, $modulo;
		
		$status = 0;
		
		if($this->safeMode && strpos($_REQUEST["email"],"@uzzye.com") !== false && strpos($_SESSION["emailLogin"],"@uzzye.com") === false){
			//header("Location: index.php?modulo=".$modulo."&tipo_msg=i&status_operacao=".$status."&id=");//.$result);
			//die();
			$this->view->tipo_msg = "i";
			$this->view->status = 0;
			$this->view->id = $_REQUEST["id"];
		}
		else{
			parent::do_incluir();	
		}
	}
	
	function do_editar(){
		global $db, $modulo;
		
		$status = 0;
		
		if($this->safeMode && strpos($_REQUEST["email"],"@uzzye.com") !== false && strpos($_SESSION["emailLogin"],"@uzzye.com") === false){
			//header("Location: index.php?modulo=".$modulo."&tipo_msg=e&status_operacao=".$status."&id=");//.$result);
			//die();
			$this->view->tipo_msg = "e";
			$this->view->status = 0;
			$this->view->id = $_REQUEST["id"];
		}
		else{
			parent::do_editar();	
		}
	}
	
	function do_excluir(){
		global $db, $modulo;
		
		if($this->safeMode){
		
			$usu = new usuarios_controller();
			$usu->inicia_dados();
			$usu->set_var("id",$_REQUEST["id"]);
			$usu->carrega_dados();
		
			$status = 0;
		
			if(strpos($_REQUEST["email"],"@uzzye.com") !== false && strpos($_SESSION["emailLogin"],"@uzzye.com") === false){
				//header("Location: index.php?modulo=".$modulo."&tipo_msg=r&status_operacao=".$status."&id=");//.$result);
				//die();
				$this->view->tipo_msg = "r";
				$this->view->status = 0;
				$this->view->id = $_REQUEST["id"];
			}
			else{
				parent::do_excluir();	
			}
		}
		else{
			parent::do_excluir();	
		}
	}

	// SENHA

	function do_senha() {
		global $modulo, $db;

		$usu = new cms_usuarios();
		$usu->inicia_dados();
		$usu->set_var("id",$_SESSION["idLogin"]);
		$usu->carrega_dados();

		if(md5($_REQUEST["nova_senha"]) != md5($_REQUEST["nova_senha_repetida"])) {
			$this->view->descr_msg = get_lang("_MSG_NEW_PASS_DOESNT_MATCH");
			$this->view->tipo_msg = "e";
			$this->view->status = 0;
			$this->view->id = $_SESSION["idLogin"];
		} else {
			if($usu->get_var("senha") == md5(output_decode($db->escape_string($_REQUEST["senha_atual"])))) {
				// Altera senha
				$usu->set_var("senha",md5(output_decode($db->escape_string($_REQUEST["nova_senha"]))));
				$usu->edita();

				$this->view->descr_msg = get_lang("_MSG_PASS_CHANGED");
				$this->view->tipo_msg = "e";
				$this->view->status = 1;
				$this->view->id = $_SESSION["idLogin"];
			} else {
				$this->view->descr_msg = get_lang("_MSG_CURRENT_PASS_INVALID");
				$this->view->tipo_msg = "e";
				$this->view->status = 0;
				$this->view->id = $_SESSION["idLogin"];
			}
		}

		$this->view->gera_mensagens();
	}

	function senha() {
		global $modulo;
		
		$this->view->tipo_msg = $_REQUEST["tipo_msg"];
		$this->view->status = $_REQUEST["status_operacao"];
		$this->view->id = $_SESSION["idLogin"];
		$this->view->gera_mensagens();
		
		$this->view->monta_formulario_senha();
	}

	// PERFIL

	function do_perfil() {
		global $modulo, $db;

		$_REQUEST["id"] = $_SESSION["idLogin"];

		// Copiado do do_editar
		
		$this->view->campos_permitidos = array(
			"nome" => true,
			"setor" => true,
			"email" => false,
			"crop_avatar" => true,
			"id_idioma" => true
		);

		$this->view->monta_campos_form();
		
		$array_form_campos = $this->view->array_form_campos;
		$array_form_refs = $this->view->array_form_refs;		
		$array_form_campos_padrao = $this->view->array_form_campos_padrao;
		
		if(trim($_REQUEST["id"]) != "")
		{
			$this->model->id = $_REQUEST["id"];
			$this->model->carrega_dados();
		}
		
		$w=0;
		while($w<sizeof($array_form_campos))
		{
			// TESTAR VALIDACOES		
			
			$field = $array_form_campos[$w];
			if(trim($field->name) != "") {
				if($field->type == "file")
				{				
				}
				else if($field->type == "combobox")
				{				
					if($field->multiple){
						$strAux = "";

						for($p=0;$p<sizeof($_REQUEST[$field->name]);$p++){
							if(trim($strAux) != ""){
								$strAux .= ";";
							}
							$strAux .= $_REQUEST[$field->name][$p];
						}
						$field->value = $strAux;
					} else {
						$field->value = $_REQUEST[$field->name];
					}
				}
				else
				{
					$field->value = $_REQUEST[$field->name];
				}
				
				$var_name = $field->name;
				$this->model->$var_name = $field->get_db_value();
			}
			
			$w++;
		}

		$w=0;
		while($w<sizeof($array_form_campos_padrao))
		{
			// TESTAR VALIDACOES		
			
			$field = $array_form_campos_padrao[$w];
			if(trim($field->name) != "") {
				if($field->type == "file")
				{				
				}
				else if($field->type == "combobox")
				{				
					if($field->multiple){
						$strAux = "";

						for($p=0;$p<sizeof($_REQUEST[$field->name]);$p++){
							if(trim($strAux) != ""){
								$strAux .= ";";
							}
							$strAux .= $_REQUEST[$field->name][$p];
						}
						$field->value = $strAux;
					} else {
						$field->value = $_REQUEST[$field->name];
					}
				}
				else
				{
					$field->value = $_REQUEST[$field->name];
				}
				
				$var_name = $field->name;
				$this->model->$var_name = $field->get_db_value();
			}
			
			$w++;
		}
		
		$result = $this->model->do_editar($array_form_campos,$array_form_refs,$array_form_campos_padrao);
		
		$status = ($result?1:0);

		if(intval($_REQUEST["id_idioma"]) > 0) {
			// processa idioma			
			$idiomas = new idiomas();
			$idiomas->inicia_dados();
			$idiomas->set_var("id",intval($_REQUEST["id_idioma"]));
			$idiomas->carrega_dados();
		
			$lang = trim(stripslashes(get_output($idiomas->get_var("identificador"))));
			
			$_SESSION["_config"]["lang"] = trim($lang);
			$_SESSION["_config"]["id_lang"] = intval($_REQUEST["id_idioma"]);
			$refLang = explode("-",$lang);
			$_SESSION["_config"]["ref_lang"] = $refLang[0];			
		}

		$this->view->tipo_msg = "e";
		$this->view->status = $status;
		$this->view->id = $_REQUEST["id"];
		$this->view->gera_mensagens();
		
		echo "<!--reload-->";
	}

	function perfil() {
		global $modulo;
		
		$this->view->tipo_msg = $_REQUEST["tipo_msg"];
		$this->view->status = $_REQUEST["status_operacao"];
		$this->view->id = $_SESSION["idLogin"];
		$this->view->gera_mensagens();

		$_REQUEST["id"] = $_SESSION["idLogin"];

		$this->view->titulo_cadastro = get_lang("_MEU_PERFIL");
		$this->view->campos_permitidos = array(
			"nome" => true,
			"setor" => true,
			"email" => false,
			"crop_avatar" => true,
			"id_idioma" => true
		);
		$this->view->monta_formulario($_REQUEST["id"],false,true);
	}

	// LOGIN

	function login($idUser = null) {
		global $db, $modulo;

		$_SESSION["btLogin"] = false;

		if($idUser) {
			// Pega user do banco
			$strLembrar = 0;

			$consulta = $db->exec_query("SELECT * FROM " . DBTABLE_CMS_USUARIOS . " WHERE id = " . intval($idUser) . " AND ativo = 1") or die($db->error());

			return $this->autenticaLogin($consulta, $strLembrar, false);
		} else {
			// Pega dados do post
			$strUsuario = $_REQUEST["usuario"];
			$strSenha = $_REQUEST["senha"];	
			$strLembrar = $_REQUEST["lembrar"];

			$consulta = $db->exec_query("SELECT * FROM " . DBTABLE_CMS_USUARIOS . " WHERE email <> '' AND senha <> '' AND email = '".$db->escape_string($strUsuario)."' AND senha = MD5('".$db->escape_string($strSenha)."') AND ativo = 1") or die($db->error());

			$this->autenticaLogin($consulta, $strLembrar, true);
			die();
		}
	}

	function autenticaLogin($consulta = null, $strLembrar = 0, $output = false) {	
		global $db, $modulo, $projectName;

		// Não autenticou
		if ($db->num_rows($consulta) <= 0) {
			$_SESSION["btLogin"] = false;

			if($output) {
				ob_clean();
			
				echo "<span class=\"erro\">" . get_lang("_MSG_USER_PASS_INVALID") . "</span>";
				die();
			}
			return false;
		}

		// Logou
		else {
			$rsUsuario = $db->result_array($consulta);

			$_SESSION["idLogin"] = $rsUsuario["id"];
			$_SESSION["btLogin"] = true;
			$_SESSION["strLogin"] = $rsUsuario["email"];
			$_SESSION["strNome"] = $rsUsuario["nome"];
			$_SESSION["avatarLogin"] = $rsUsuario["avatar"];
			$_SESSION["idGrupo"] = $rsUsuario["id_grupo"];
			$_SESSION["emailLogin"] = $rsUsuario["email"];
			$_SESSION["data_ultimoacesso"] = date("Y/m/d H:i:s");
			unset($_SESSION["ROOT_SESSION"]);
			$_SESSION["GLOBAL_RANDOM_KEY"] = intval($_SESSION["idLogin"]) . str_pad(rand(0,99999),5,"0",STR_PAD_LEFT);
			include_once("unlink_all_images.php");			
			$comando = $db->exec_query("UPDATE " . DBTABLE_CMS_USUARIOS . " SET data_ultimoacesso = now() WHERE id = '".$rsUsuario["id"]."'");		

			if(intval($strLembrar) == 1) {
				@setcookie($projectName . "_idUser", $rsUsuario["id"], (time()+86400*30), "/");
			}

			include_once("unlink_all_images.php"); // Limpa imagens com unlink antigas

			// Lang
			if(intval($rsUsuario["id_idioma"]) > 0) {
				// processa idioma			
				$idiomas = new idiomas();
				$idiomas->inicia_dados();
				$idiomas->set_var("id",intval($rsUsuario["id_idioma"]));
				$idiomas->carrega_dados();
			
				$lang = trim(stripslashes(get_output($idiomas->get_var("identificador"))));
				
				$_SESSION["_config"]["lang"] = trim($lang);
				$_SESSION["_config"]["id_lang"] = intval($rsUsuario["id_idioma"]);
				$refLang = explode("-",$lang);
				$_SESSION["_config"]["ref_lang"] = $refLang[0];
			}

			if($output) {
				ob_clean();

				echo "<!--SUCESSO-->" . get_lang("_MSG_AUTHENTICATING");
				die();
			}
			return true;
		}
		
		die();
	}

	function logout() {
		global $db, $modulo, $projectName;

		session_start();
		session_destroy();
		@setcookie($projectName . "_idUser", "", (time()+86400*30), "/");

		$_SESSION["btLogin"] = false;
		ob_clean();
		header ("Location: " . ROOT_SERVER . ROOT . "login");
		die();
	}
}
?>