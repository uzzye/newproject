<?php

class cms_log_entidades_controller extends controller{
  
    function __construct(){	
	    $this->nome = "cms_log_entidades";
		
		parent::__construct();

		$this->mostra_ranking_registro = false;
		$this->mostra_permalink_registro = false;
		$this->mostra_views_registro = false;
    }
    
    function acaoPadrao()
	{
		// escrever aqui a acao padrao para o front-end, caso ela não seja definida, senão executa a acaoPadrao do parent
		parent::acaoPadrao();
	}
	
	function lista_registros()
	{
		parent::lista_registros();
		
		$array_campos = array("nome_entidade","id_registro","acao");
		$array_nomes = array(get_lang("_NOME_ENTIDADE"),get_lang("_ID_REGISTRO"),get_lang("_ACAO"));			
		$array_foreign_keys = array("","","","");
		$array_expressoes = array("","","","");
		
		$this->view->lista($array_campos,$array_nomes,$array_expressoes,"","",$array_foreign_keys);
	}	
}
?>