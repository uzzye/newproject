<?php

class contatos_controller extends controller{
  
    function __construct(){	
	    $this->nome = "contatos";
		
		parent::__construct();

		$this->mostra_ranking_registro = false;
		$this->mostra_permalink_registro = false;
		$this->mostra_selecao_registro = false;
		$this->mostra_ativo_registro = false;
		$this->mostra_views_registro = false;
		$this->mostra_autor_registro = false;
		$this->mostra_lang_table = false;
		
		$this->mostra_id_registro = true;
		$this->mostra_data_registro = true;
    }
    
    function acaoPadrao()
	{
		// escrever aqui a acao padrao para o front-end, caso ela não seja definida, senão executa a acaoPadrao do parent
		parent::acaoPadrao();
	}
	
	function lista_registros()
	{
		parent::lista_registros();
		
		$array_campos = array("nome","email","telefone","referencia","mensagem","conversao");
		$array_nomes = array(get_lang("_NOME"),get_lang("_EMAIL"),get_lang("_TELEFONE"),get_lang("_REFERENCIA"),get_lang("_MENSAGEM"),get_lang("_CONVERSAO"));
		$array_foreign_keys = array("","","","","","","");
		$array_expressoes = array("","","","","","","");
		
		$this->view->lista($array_campos,$array_nomes,$array_expressoes,"","",$array_foreign_keys);
	}	

	function converter() {
		global $modulo;

		if($this->valida_permissao($modulo,"AtivarDesativar")){
			if(trim($_REQUEST["id"]) != "")
			{
				$this->model->id = $_REQUEST["id"];
				$this->model->carrega_dados();
			}
			
			$valorAux = $_REQUEST["prompt_value"];
			$valorAux = trim(str_replace("R$","",$valorAux));
			$valorAux = str_replace(",",".",str_replace(".","",$valorAux));

	  	  	$result = $this->model->do_converter($valorAux);		
	  	  	$status = ($result?1:0);

			// Mostra o contrário
			if(intval($this->model->conversao) == 0) { // contrário
				echo @eval($this->view->custom_expr_masks["conversao"]);
			} else {
				echo "";
			}
		}
	}

	function get_array_conversoes($dataIni = "", $dataFim = "", $conversao = 1) {
		global $db;

		$resAux = $this->model->get_conversoes($dataIni, $dataFim, $conversao);
		$array_aux = $db->result_object_array($resAux);

		return $array_aux;
	}
}
?>