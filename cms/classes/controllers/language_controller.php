<?php

require_once(ROOT_CMS . "classes/controllers/idiomas_controller.php");

class language_controller extends controller{
  
    function __construct(){	
	    $this->nome = "language";
		
		parent::__construct();

		$this->mostra_ranking_registro = true;

		$this->mostra_permalink_registro = false;
		$this->mostra_views_registro = false;
    }
    
    function acaoPadrao()
	{
		// escrever aqui a acao padrao para o front-end, caso ela não seja definida, senão executa a acaoPadrao do parent
		parent::acaoPadrao();
	}
	
	function lista_registros()
	{
		parent::lista_registros();
		
		$array_campos = array("modulo_registro","id_registro","variavel","id_idioma","valor");
		$array_nomes = array(get_lang("_MODULO_REGISTRO"),get_lang("_ID_REGISTRO"),get_lang("_VARIAVEL"),get_lang("_IDIOMA"),get_lang("_VALOR"));			
		$array_foreign_keys = array("","","","titulo");
		$array_expressoes = array("","");
		
		$this->view->lista($array_campos,$array_nomes,$array_expressoes,"","",$array_foreign_keys);
	}	
}
?>