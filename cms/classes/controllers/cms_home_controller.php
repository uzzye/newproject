<?php 

class cms_home_controller {
	
	public $nome;
    public $view;   

    public $ga;     
   
    function __construct() {
    	global $modulo;
    	
    	$this->nome = "cms_home";
    	
    	//if($this->valida_permissao($modulo))
    	{     	    	    	
			$classe = $this->nome."_view";
			$this->view = new $classe($this);			
    	}
    	/*else
    	{
    		echo "Você não tem permissão de acesso a este módulo!";
    		//echo "<br>Aguarde enquanto você é redirecionado...";
    		//echo "<meta http-equiv=\"refresh\" content=\"2;url=" . ROOT_SERVER . ROOT . "\" />";
    	}*/	
                
        $this->ga = null;
	}
      
    function acaoPadrao()
	{
        global $_CONFIG;

        require_once(ROOT_CMS . "classes/vendor/gapi.class.php");

        if(trim($_CONFIG["ga_user"]) != "") {
            try {
                $this->ga = new gapi($_CONFIG["ga_user"],'ga_key.p12');
            } catch(Exception $e) {
                $this->ga = null;
            }
        }

		$this->view->apresenta();
	}

    function get_bread_crumbs()
    {
        global $modulo, $acao;

        return array(
            array("Home", ROOT),
            array(get_texto_acao($acao), "")
        );
    }
}	
?>