<?php

class teste_controller extends controller{
  
    function __construct(){	
	    $this->nome = "teste";
		
		parent::__construct();

		$this->mostra_lang_table = true;
		$this->mostra_views_registro = true;
    }
    
    function acaoPadrao()
    {
    	// escrever aqui a acao padrao para o front-end, caso ela não seja definida, senão executa a acaoPadrao do parent
    	parent::acaoPadrao();
    }
	
	function lista_registros()
	{
		parent::lista_registros();
		
		$array_campos = array("titulo","cms_modulos","valor","data","cor");
		$array_nomes = array(get_lang("_TITULO"),get_lang("_MODULOS"),get_lang("_VALOR"),get_lang("_DATA"),get_lang("_COR"));
		$array_foreign_keys = array("");
		$this->view->lista($array_campos,$array_nomes,$array_expressoes,"","",$array_foreign_keys);
	}	
}
?>