<?php 

class relatorios_controller{
	
	public $nome;
    public $view;        
   
    function __construct() {
    	global $modulo;
    	
    	$this->nome = "relatorios";
    	
    	//if($this->valida_permissao($modulo))
    	{     	    	    	
			$classe = $this->nome."_view";
			$this->view = new $classe($this);			
    	}
    	/*else
    	{
    		echo "Você não tem permissão de acesso a este módulo!";
    		//echo "<br>Aguarde enquanto você é redirecionado...";
    		//echo "<meta http-equiv=\"refresh\" content=\"2;url=" . ROOT_SERVER . ROOT . "\" />";
    	}*/	
	}
      
    function acaoPadrao()
	{
		$this->view->apresenta();
	}

    function processa()
    {
        $this->view->processa();
    }
}	
?>