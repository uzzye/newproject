<?php

class itens_teste_controller extends controller{
  
    function __construct(){	
	    $this->nome = "itens_teste";
		
		parent::__construct();
    }
    
    function acaoPadrao()
    {
    	// escrever aqui a acao padrao para o front-end, caso ela não seja definida, senão executa a acaoPadrao do parent
    	parent::acaoPadrao();
    }
	
	function lista_registros()
	{
		parent::lista_registros();
		
		$array_campos = array("id_teste","titulo");
		$array_nomes = array(get_lang("_TESTE"),get_lang("_TITULO"));
		$array_foreign_keys = array("","");
		$this->view->lista($array_campos,$array_nomes,$array_expressoes,"","",$array_foreign_keys);
	}	
}
?>