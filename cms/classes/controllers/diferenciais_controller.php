<?php

class diferenciais_controller extends controller{
  
    function __construct(){ 
        $this->nome = "diferenciais";
        
        parent::__construct();
    }
    
    function acaoPadrao()
    {
        // escrever aqui a acao padrao para o front-end, caso ela não seja definida, senão executa a acaoPadrao do parent
        parent::acaoPadrao();
    }
    
    function lista_registros()
    {
        parent::lista_registros();
        
        $array_campos = array("titulo_pt");
        $array_nomes = array(get_lang("_TITULO_PT"));
        $array_foreign_keys = array("","");
        $array_expressoes = array("","");
        $array_where_filtros = array();
        $array_expressoes_sql = array();
        
        $this->view->lista($array_campos,$array_nomes,$array_expressoes,"","",$array_foreign_keys,$array_where_filtros,$array_condicoes_acoes,$array_expressoes_sql);
    }   
}
?>