<?php

class arquivos_controller extends controller{
  
    function __construct(){	
	    $this->nome = "arquivos";
		
		parent::__construct();

		$this->mostra_lang_table = false;
		$this->mostra_permalink_registro = false;
		$this->mostra_views_registro = false;
    }
    
    function acaoPadrao()
    {
    	// escrever aqui a acao padrao para o front-end, caso ela não seja definida, senão executa a acaoPadrao do parent
    	parent::acaoPadrao();
    }
	
	function lista_registros()
	{
		parent::lista_registros();
		
		$array_campos = array("id_categoria","titulo_pt","id_idioma");
		$array_nomes = array(get_lang("_CATEGORIA"),get_lang("_TITULO_PT"),get_lang("_IDIOMA"));
		$array_foreign_keys = array("");
		$this->view->lista($array_campos,$array_nomes,$array_expressoes,"","",$array_foreign_keys);
	}	
}
?>