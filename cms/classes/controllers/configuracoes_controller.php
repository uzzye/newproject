<?php
include_once(ROOT_CMS . "classes/controllers/idiomas_controller.php");
 
class configuracoes_controller extends controller{
  
    function __construct(){	
	    $this->nome = "configuracoes";
		
		parent::__construct();

		$this->mostra_ranking_registro = false;
		$this->mostra_permalink_registro = false;
		$this->mostra_views_registro = false;
    }
    
    function acaoPadrao()
	{
		// escrever aqui a acao padrao para o front-end, caso ela não seja definida, senão executa a acaoPadrao do parent
		parent::acaoPadrao();
	}
	
	function lista_registros()
	{
		parent::lista_registros();
		
		$array_campos = array("idioma_padrao","titulo_padrao","descricao_padrao","palavras_chave_padrao","charset_padrao","id_idioma");
		$array_nomes = array(get_lang("_IDIOMA_PADRAO"),get_lang("_TITULO_PADRAO"),get_lang("_DESCRICAO_PADRAO"),get_lang("_PALAVRASCHAVE_PADRAO"),get_lang("_CHARSET_PADRAO"),get_lang("_IDIOMA"));			
		$array_foreign_keys = array("","","","","","titulo");
		$array_expressoes = array("(\$valor==1)?get_lang(\"_SIM\"):get_lang(\"_NAO\");");
		$array_where_filtros = array(" IF(idioma_padrao=1,\"" . get_lang("_SIM") . "\",\"" . get_lang("_NAO") . "\") ");
		
		$this->view->lista($array_campos,$array_nomes,$array_expressoes,"","",$array_foreign_keys,$array_where_filtros);
	}	
}
?>