<?php

class noticias_tags_controller extends controller{
  
    function __construct(){ 
        $this->nome = "noticias_tags";
        
        parent::__construct();

		$this->mostra_ranking_registro = false;
		$this->mostra_permalink_registro = false;
    }
    
    function acaoPadrao()
    {
        // escrever aqui a acao padrao para o front-end, caso ela não seja definida, senão executa a acaoPadrao do parent
        parent::acaoPadrao();
    }
    
    function lista_registros()
    {
        parent::lista_registros();
        
        $array_campos = array("titulo_pt");
        $array_nomes = array(get_lang("_TITULO_PT"));            
        $array_foreign_keys = array("","");
        $array_expressoes = array("","");
        $array_where_filtros = array();
        $array_expressoes_sql = array();
        
        $this->view->lista($array_campos,$array_nomes,$array_expressoes,"","",$array_foreign_keys,$array_where_filtros,$array_condicoes_acoes,$array_expressoes_sql);
    }   
}
?>