<?php

class cms_grupos_usuarios_controller extends controller{
  
    function __construct(){	
	    $this->nome = "cms_grupos_usuarios";
		
		parent::__construct();

		$this->mostra_ranking_registro = false;
		$this->mostra_permalink_registro = false;
		$this->mostra_views_registro = false;
    }
    
    function acaoPadrao()
	{
		// escrever aqui a acao padrao para o front-end, caso ela não seja definida, senão executa a acaoPadrao do parent
		parent::acaoPadrao();
	}
	
	function lista_registros()
	{
		parent::lista_registros();
	
		$array_campos = array("titulo");
		$array_nomes = array(get_lang("_TITULO"));
		$this->view->lista($array_campos,$array_nomes);
	}	
}
?>