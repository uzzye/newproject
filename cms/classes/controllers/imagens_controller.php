<?php

class imagens_controller extends controller{
  
    function __construct(){ 
        $this->nome = "imagens";
        
        parent::__construct();
    }
    
    function acaoPadrao()
    {
        // escrever aqui a acao padrao para o front-end, caso ela não seja definida, senão executa a acaoPadrao do parent
        parent::acaoPadrao();
    }
    
    function lista_registros()
    {
        parent::lista_registros();
        
        $array_campos = array("referencia","titulo","imagem");
        $array_nomes = array(get_lang("_PAGINA"),get_lang("_TITULO"),get_lang("_IMAGEM"));            
        $array_foreign_keys = array("","");
        $array_expressoes = array("","");
        $array_where_filtros = array();
        $array_expressoes_sql = array();
        
        $this->view->lista($array_campos,$array_nomes,$array_expressoes,"","",$array_foreign_keys,$array_where_filtros,$array_condicoes_acoes,$array_expressoes_sql);
    }   
}
?>