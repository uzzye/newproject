<?php

class Uzzye_ComboBox extends Uzzye_Field
{	
	public $checked_value;
	public $checked_array;
	public $default_display;
	public $array_options;
	public $size;
	public $multiple;

    function __construct($name = "", $id = "", $label = "", $default_value = "", $readonly = "", $style_class = "sltNormal", $li_class = "", $sub_label = "")
	{
		$this->type = "combobox";
		$this->size = 1;
		$this->multiple = false;
		
		parent::__construct($name, $id, $label, $default_value, $readonly, $style_class, $li_class, $sub_label);
	}
	
	function get_display_field()
	{
		$result = "";
		$result .= $this->ini_field_set();
		$result .= $this->get_display_label();		
		
		if($this->multiple)
		{
			$this->style_class = "sltMulti";			 
		}
		
		$result .= "<select class=\"form-control select2 " . $this->style_class. "\" ";
		if(trim($this->default_display) != "") {
			$result .= " data-placeholder=\"" . trim($this->default_display) . "\" ";
		}
		$result .= " id=\"" . ($this->id) . "\" name=\"" . ($this->name) . "";	
		if($this->multiple)
		{
			$result .= "[]";
		}
		$result .= "\" size=\"" . ($this->size) . "\" ";
		if($this->multiple)
		{
			$result .= " multiple ";			 
		}
		if($this->readonly)
		{
			$result .= " readonly "; // onfocus=\"this.initialSelect = this.selectedIndex;\" onchange=\"this.selectedIndex = this.initialSelect;\"
		}
		if($this->required)
		{
			$result .= " required='required' ";
		}
		$result .= ">
			<optgroup>";
		if(trim($this->default_display) <> "" && trim($this->default_value) <> "")
		{
			$result .= "<option value='" . get_output($this->default_value) . "'>" . get_output($this->default_display) . "</option>";
		} else if(trim($this->default_display) != "") {
			$result .= "<option></option>";
		}
		
		for($i=0;$i<sizeof($this->array_options);$i++)
		{
			$result .= "<option value=\"" . get_output($this->array_options[$i][0]) . "\" ";
			if(sizeof($this->checked_array) > 0)
			{
				if(in_array($this->array_options[$i][0],$this->checked_array,true))
				{
					$result .= " selected";
				}				
			}
			else if($this->array_options[$i][0] == $this->checked_value)
			{
				$result .= " selected";
			}
			
			$result .= ">" . (trim($this->array_options[$i][1])==""?"&nbsp;":stripslashes(get_output($this->array_options[$i][1]))) . "</option>";
		}
		$result .= "</optgroup>
		</select>";
		
		$result .= $this->end_field_set();
		
		return $result;
	}
	
	function get_db_value()
	{
		return output_decode($this->value);
	}	
	
	function set_value($valor)
	{
		$this->value = get_output($valor);
	}
	
	function get_options()
	{
		return ($this->array_options);
	}	
	
	function set_options($valor)
	{
		$this->array_options = ($valor);
	}
}

?>