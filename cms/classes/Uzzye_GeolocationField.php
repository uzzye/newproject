<?php

class Uzzye_GeolocationField extends Uzzye_Field
{	
	public $search_field;

    function __construct($name = "", $id = "", $label = "", $default_value = "", $readonly = "", $style_class = "form-Text", $li_class = "", $sub_label = "")
	{
		$this->type = "geolocation";	
		$this->search_field = true;
		
		parent::__construct($name, $id, $label, $default_value, $readonly, $style_class, $li_class, $sub_label);
	}
	
	function get_display_field()
	{
		$result = "";

		$result .= $this->ini_field_set();
		$result .= $this->get_display_label();
		
		$result .= "<input type=\"text\" class=\"form-control geo-field geo-coord " . $this->style_class. "\" id=\"" . ($this->id) . "\" name=\"" . ($this->name) . "\" value=\"" . ($this->value) . "\"";
		/*if(trim($this->max_length) <> "")
		{
			$result .= " maxlength=\"" . ($this->max_length) . "\"";
		}*/	
		if($this->readonly)
		{
			$result .= " readonly";
		}	
		if($this->required)
		{
			$result .= " required='required' ";
		}
		$result .= "/>";

		if(!$this->readonly && $this->search_field) {
			$result .= "<input type=\"text\" class=\"form-control geo-field geo-search " . $this->style_class. " search\" id=\"" . ($this->id) . "_search\" name=\"" . ($this->name) . "_search\" placeholder=\"" . get_lang("_DEFAULTMAPSEARCH") . "\" />";
		}

		$result .= "<div id=\"" . ($this->id) . "_map\" style=\"\" class=\"map-canvas\"></div>";
		
		$result .= $this->end_field_set();
		return $result;
	}
	
	function get_db_value()
	{
		return output_decode($this->value);
	}	
	
	function set_value($valor)
	{
		$this->value = get_output($valor);
	}
}

?>