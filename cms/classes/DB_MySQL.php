<?php
class DB_MySQL {

    public $host;
    public $user;
    public $senha;
    public $dbase;

    public $query;
    public $link_conn;
    public $resultado;
    public $persistent;
    
    function __construct(){
		// Instancia o Objeto
		$this->persistent = false;
    }

    function conecta(){
    	global $_CONFIG;

    	if(!isset($_CONFIG["noSQL"]) || $_CONFIG["noSQL"] === false) {
	    	if($this->persistent) {
	        	$this->link_conn = mysql_pconnect($this->host,$this->user,$this->senha);
	        } else {
	        	$this->link_conn = mysql_connect($this->host,$this->user,$this->senha);
	        }
	        if(!$this->link_conn){
	            echo "Falha ao conectar no banco de dados<br /><b>Erro: ".mysql_error()."</b>";
				die();
	        }elseif(!mysql_select_db($this->dbase,$this->link_conn)){
	            echo "Falha ao selecionar o banco de dados:<br /><b>Erro: ".mysql_error()."</b>";
				die();
	        }
	    } else {
	    	$this->link_conn = null;
	    }
    }
	
	function desconecta(){
        return mysql_close($this->link_conn);
    }

    function exec_query($query, $retorna_id = false){
    	global $_CONFIG;

    	if(!isset($_CONFIG["noSQL"]) || $_CONFIG["noSQL"] === false) {
	        $this->conecta();
	        $this->query = $query;
	        if($this->resultado = mysql_query($this->query)){
				$result = $this->resultado;
				if($retorna_id)
				{
					$result = array($result,intval(mysql_insert_id()));				
				}
	            $this->desconecta();
	            return $result;
	        }else{
	            //echo "Falha ao executar a query: ".$query."<br />Erro: <b>".mysql_error()."</b>";
				//die();
	            $this->desconecta();
	        }        
	    } else {
	    	return null;
	    }
    }
	
	function num_rows($result){
        //if($this->resultado = $result){
            return @mysql_num_rows($result);
        /*}else{
            echo "Falha ao gerar o retorno ".$result."<br />Erro: <b>".mysql_error()."</b>";
			die();
        }*/
    }
	
	function result_field($result,$i,$nome)
	{		
	    $aux = "";
		$aux = mysql_result($result,$i,$nome);		
        /*if($aux = mysql_result($result,$i,$nome)){       
            return $aux;
        }else{
            echo "Falha ao gerar resultado de campo ".$nome." do registro " .$i. "<br />Erro: <b>".mysql_error()."</b>";
			die();
        }*/
		return $aux;
	}
	
	function result_array($result)
	{		
	    $aux = array();
		/*if($this->num_rows($result) > 0)
		{
          if(*/$aux = mysql_fetch_array($result);/*){*/
              return $aux;
          /*}else{
              echo "Falha ao gerar array de resultado para " .$result. "<br />Erro: <b>".mysql_error()."</b>";
			  die();
          }  
		}
		else
		{
			return $aux;
		}*/
	}	
	
	function result_object($result)
	{		
	    $aux;
        //if(
		   $aux = mysql_fetch_object($result);
		   //){
            return $aux;
        /*}else{
            echo "Falha ao gerar objeto de resultado para " .$result. "<br />Erro: <b>".mysql_error()."</b>";
			die();
        } */ 
	}	
	
	function result_object_array($result)
	{		
	    $array_obj = array();
	    while($obj = mysql_fetch_object($result))
	    {
	    	$array_obj[sizeof($array_obj)] = $obj;
	    }
	    return $array_obj;
	}	
	
	function escape_string($strOri)
	{
		$this->conecta();
        //if(
			 $str = mysql_real_escape_string($strOri,$this->link_conn);
			 //){
			$this->desconecta();
            return $str;
        /*}else{
            echo "Falha ao encapsular a string: ".$strOri."<br />Erro: <b>".mysql_error()."</b>";
			die();
        }*/       
	}    
	
	function fields_array($result)
	{
		if($this->num_rows($result) > 0)
		{
          /*if(*/
			 $ret = array();			
			 $array_aux = mysql_fetch_array($result);			  
			 if($array_aux)
			 {
			 	$ret = array_keys($array_aux);
			 }
			 //){
              return $ret;
          /*}else{
              echo "Falha ao retornar Array de campos em ".$result."<br />Erro: <b>".mysql_error()."</b>";
		  	  die();
          }   */
		}
		else
		{
			$ret = array();
            return $ret;
		}
	}

	function error() {
		return mysql_error();	
	}
}

?>