<?php

class Uzzye_ColorField extends Uzzye_Field
{	
	public $max_length;
	
    function __construct($name = "", $id = "", $label = "", $default_value = "", $readonly = "", $style_class = "form-Text", $li_class = "", $sub_label = "")
	{
		$this->type = "date";	
		
		parent::__construct($name, $id, $label, $default_value, $readonly, $style_class, $li_class, $sub_label);
		
		$this->max_length = 100;
	}
	
	function get_display_field()
	{
		$result = "";
		$result .= $this->ini_field_set();
		$result .= $this->get_display_label();		
		
		$result .= "<div class=\"colorpicker input-group colorpicker-component colorpicker-element\">";
		$result .= "<input type=\"text\" class=\"form-control form-color " . $this->style_class. "\" id=\"" . ($this->id) . "\" name=\"" . ($this->name) . "\" value=\"" . ($this->value) . "\"";
		/*if(trim($this->max_length) <> "")
		{
			$result .= " maxlength=\"" . ($this->max_length) . "\"";
		}*/	
		if($this->readonly)
		{
			$result .= " readonly";
		}	
		if($this->required)
		{
			$result .= " required='required' ";
		}
		$result .= "/>";

		if(!$this->readonly) {
			$result .= "<span class=\"input-group-addon\"><i class=\"fa fa-tint\" style=\"background-color: rgb(255, 255, 255);\"></i></span>";
		}
		$result .= "</div>";
		
		$result .= $this->end_field_set();
		return $result;
	}
	
	function get_db_value()
	{
		return output_decode($this->value);
	}	
	
	function set_value($valor)
	{
		$this->value = get_output($valor);
	}
}

?>