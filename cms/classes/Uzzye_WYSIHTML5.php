<?php

class Uzzye_WYSIHTML5 extends Uzzye_Field
{	
	public $size;
	public $rows;
	public $max_length;
	public $id_textArea_source;
	public $id_source;
	public $id_destiny;
	public $height = "";
	
    function __construct($name = "", $id = "", $label = "", $default_value = "", $readonly = "", $style_class = "textarea", $li_class = "w100", $sub_label = "", $max_length = 0)
	{	
		$this->type = "textarea";
		
		parent::__construct($name, $id, $label, $default_value, $readonly, $style_class, $li_class, $sub_label, $max_length);
	}
	
	function get_display_field()
	{
		global $modulo;

		if(trim($this->id_textArea_source) != "") {
			$this->id_source = $this->id_textArea_source;
		}

		$result = "";
		$result .= $this->ini_field_set();
		$result .= $this->get_display_label();	

		//$randcod = gera_senha();	

		if(!$this->readonly) {
			if(trim($this->size) <> "")
			{
				switch($this->size)
				{
					case "P":
						$this->style_class = "textAreaP";
						$this->rows = 5;
						break;
					case "M":
						$this->style_class = "textAreaM";
						$this->rows = 10;
						break;
					case "G":
						$this->style_class = "textAreaG";
						$this->rows = 20;
						break;
				}
			}		
			
			$result .= "<textarea id=\"" . ($this->id) . "\" rows=\"" . $this->rows . "\" class=\"tinymce form-control " . $this->style_class . "\" name=\"" . ($this->name) . "\"";
			if(trim($this->max_length) <> "" && $this->max_length > 0)
			{
				$result .= " maxlength=\"" . ($this->max_length) . "\"";
			}	
			if($this->readonly)
			{
				$result .= " readonly";
			}	
			if(trim($this->height) != "")
			{
				$result .= " data-height=\"" . $this->height . "\" ";
			}	
			if(trim($this->id_source) != "")
			{
				$result .= " data-sourceid=\"" . $this->id_source . "\" ";
			}	
			if(trim($this->id_destiny) != "")
			{
				$result .= " data-destinyid=\"" . $this->id_destiny . "\" ";
			}	
			if($this->required)
			{
				//$result .= " required='required' ";
			}
			$result .= ">" . ($this->value) . "</textarea>";
			
			$result .= "<span class=\"help-block\"> " . get_lang("_CKEDITORHELP") . "</span>";
		} else {
			$result .= "<div class=\"row wysi-view\">
				<div class=\"col-sm-12\">" . ($this->value) . "</div>
			</div>";
		}
		$result .= $this->end_field_set();

		return $result;
	}
	
	function get_db_value()
	{
		global $db;
		return output_decode($db->escape_string($this->value));
	}	
	
	function set_value($valor)
	{
		$this->value = get_output(stripslashes($valor));
	}
}

?>