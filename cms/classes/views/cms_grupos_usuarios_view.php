<?php    
	
class cms_grupos_usuarios_view extends view
{	  
		function __construct($owner = null)
		{
			$this->nome = "cms_grupos_usuarios";
			if(function_exists("get_lang")) {
				$this->nome_exibicao = get_lang("_GRUPOS_DE_USUARIOS");
				$this->nome_exibicao_singular = get_lang("_GRUPO_DE_USUARIOS");
			}
		
			parent::__construct($owner);		
		}
	
		function monta_campos_form($id = "", $readonly = false, $clone = false, $resumido = false)
		{
			global $modulo;
		
			$b_editando = false;		
			if(trim($_REQUEST["id"]) <> "")
			{
				$id = $_REQUEST["id"];
				$this->controller->set_var("id",$id);
				$this->controller->carrega_dados();
				$b_editando = true;
			}
		
			$array_form_campos = array();
		
			$count_radio = 0;
			$count_text = 0;
			$count_check = 0;
			$count_combo = 0;
		
			$ref = "titulo";
			$inputAux = new Uzzye_TextField();
			$inputAux->label = get_lang("_TITULO");
			$inputAux->name = $ref;
			$inputAux->id = $ref;
			$inputAux->set_value($this->controller->get_var($ref));
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_text++;
			
			// CAMPO MODULOS
       		$ref = "modulos";	
        	$inputAux = new Uzzye_PermsTable();
			$inputAux->label = get_lang("_PERMISSOES");
			$inputAux->sub_label = get_lang("_HELPPERMISSOES");
			$inputAux->name = $ref;
			$inputAux->li_class = 'w100p';
			$inputAux->id = $ref . "_" . $count_combo;
			$inputAux->id_grupo = intval($id);
			$array_form_refs[sizeof($array_form_refs)] = $inputAux;
			$count_combo++;
		
				$this->array_form_campos = $array_form_campos;
		$this->array_form_refs = $array_form_refs;	
		}
}

?>