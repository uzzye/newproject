<?php    
   
class imagens_view extends view
{     
        function __construct($owner = null)
        {
            global $modulo;

            $this->nome = "imagens";
            if(function_exists("get_lang")) {
                $this->nome_exibicao = get_lang("_IMAGENS");
                $this->nome_exibicao_singular = get_lang("_IMAGEM");
            }
        
            parent::__construct($owner);       

            $this->custom_expr_masks["imagem"] = "return(\"<img src='\" . \$this->get_upload_folder(\"imagem\") . \$valor . \"' border='0' width='100' />\");";    
            $this->custom_expr_masks["referencia"] = "return ucfirst(\$valor);";
        }   
    
        function monta_campos_form()
        {
            global $modulo;
        
            $b_editando = false;       
            if(trim($_REQUEST["id"]) <> "")
            {
                $id = $_REQUEST["id"];
                $this->controller->set_var("id",$id);
                $this->controller->carrega_dados();
                $b_editando = true;
            }
        
            $array_form_campos = array();
        
            $count_fields = 0;
                
                $ref = "referencia";
            $checked = $this->controller->get_var($ref);
            if(trim($_REQUEST[$ref]) <> "")
            {$checked = $_REQUEST[$ref];}
            $inputAux = new Uzzye_ComboBox($ref,$ref,get_lang("_PAGINA"));
            $inputAux->default_display = get_lang("_COMBOSELECIONE");
            $inputAux->default_value = 0;
            $inputAux->checked_value = $checked;
            $inputAux->size = 1;
            $inputAux->multiple = false;
            $inputAux->set_options(array(
                array(output_decode("sobre"),output_decode("Sobre")),
                array(output_decode("enoturismo"),output_decode("Enoturismo"))
            ));
                        $inputAux->set_value($this->controller->get_var($ref));
            $inputAux->li_class = "w50p";
            $inputAux->required = $this->controller->is_required($ref);
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_fields++;
                    
          $ref = "titulo";
          $inputAux = new Uzzye_TextField($ref,$ref,get_lang("_TITULO"));
          $inputAux->set_value($this->controller->get_var($ref));
          $inputAux->li_class = "w50p";
          $inputAux->required = $this->controller->is_required($ref);
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_fields++;   
                
              $ref = "imagem";
          $inputAux = new Uzzye_ImageCrop("crop_" . $ref,"crop_" . $ref,get_lang("_IMAGEM"));
          $inputAux->li_class = "w50p";
            $inputAux->label = get_lang("_IMAGEM") . " (JPG - " . get_lang("_TAMANHORECOMENDAVEL") . " 100% x 1024px - 80%)";
            $inputAux->crops = array(
                array("imagem",get_lang("_IMAGEM"),0,0,1024,80)  
            );
            $inputAux->set_value($b_editando);
            $inputAux->required = $this->controller->is_required($ref);
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_fields++;   
                
          
            $this->array_form_campos = $array_form_campos;

            parent::monta_campos_form($id, $readonly, $clone, $resumido);   
        }
}

?>