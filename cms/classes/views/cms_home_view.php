<?php 

class cms_home_view{
    public $nome_exibicao;
    public $nome_exibicao_singular;
    
    public $owner;
    public $controller; 
    
    public $help_text;
  
    function __construct($owner = null){ 
        global $modulo;
        
        $this->nome = "cms_home";
        if(function_exists("get_lang")) {
            $this->nome_exibicao = get_lang("_HOMETITULO");
            $this->nome_exibicao_singular = get_lang("_HOMETITULO");
        }
        
        $this->owner = $owner;
        $this->controller = $this->owner;
    }
    
    function set_owner($owner){ 
        $this->owner = $owner;
        $this->controller = $this->owner;
    }
    
    function apresenta($nome_relatorio = "")
    {
        global $_DICAS_UTEIS, $_CONFIG, $modulo, $acao;

        $this->heading();

        ?>        
        <!-- Row -->
        <div class="row">
            <?php

            // Dashboard

            // Para conversões customizadas do CMS
                                    
            $this_month = date("Y-m-d");
            $this_month_first_day = substr($this_month,0,4) . "-" . substr($this_month,5,2) . "-01";
            $this_month_last_day = strtotime($this_month_first_day . " +1 month");
            $this_month_last_day = strtotime(date("Y-m-d", $this_month_last_day) . " -1 day");
            $this_month_last_day = date("Y-m-d", $this_month_last_day);

            $last_month = date("Y-m-d", strtotime(date("Y-m-d") . " -1 month"));
            $last_month_first_day = substr($last_month,0,4) . "-" . substr($last_month,5,2) . "-01";
            $last_month_last_day = strtotime($last_month_first_day . " +1 month");
            $last_month_last_day = strtotime(date("Y-m-d", $last_month_last_day) . " -1 day");
            $last_month_last_day = date("Y-m-d", $last_month_last_day);

            $last_month_today = date("Y-m-d", strtotime(date("Y-m-d") . " -1 month"));

            $five_month = date("Y-m-d", strtotime(date("Y-m-d") . " -5 month"));
            $five_month_first_day = substr($five_month,0,4) . "-" . substr($five_month,5,2) . "-01";
            $five_month_last_day = strtotime($five_month_first_day . " +1 month");
            $five_month_last_day = strtotime(date("Y-m-d", $five_month_last_day) . " -1 day");
            $five_month_last_day = date("Y-m-d", $five_month_last_day);

            // DEBUG
            //echo $this_month_first_day . " to " . $this_month_last_day . "<br/>"; echo $last_month_first_day . " to " . $last_month_last_day . "<br/>"; die();

            $ga_props = explode(",",$_CONFIG["ga_props"]);
                                
            // Pega os dados da conta e perfis de site

            try {
                if($accountData = $this->controller->ga != null) {
                    $accountData = $this->controller->ga->requestAccountData();
                    foreach($accountData as $itAux) {
                        if(!in_array($itAux->getProfileId(),$ga_pros)) {
                            $propData[$itAux->getProfileId()] = $itAux;
                        }
                    }
                }
            } catch(Exception $e) {
                // No
            }
                
            ?>
            <div class="col-md-12">
                <div class="row">
                    <?php //<div class="col-md-12">
                        //<div class="alert alert-info alert-style-1">
                            //<i class="ti-info-alt"></i>Informações (ID <?php echo $ga_prop; ?)
                        //</div>
                    //</div> ?>
                    <div class="col-md-4">

                        <div id="weather-card" class="panel panel-default card-view pa-0 weather-info">
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body pa-0">
                                    <div class="row ma-0">
                                        <div class="col-xs-6 pa-0">
                                            <div class="left-block-wrap pa-30">
                                                <p class="block nowday"></p>
                                                <span class="block nowdate"></span>
                                                <div class="left-block  mt-15"></div>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 pa-0">
                                            <div class="right-block-wrap pa-30">
                                                <div class="right-block"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php
                        
                        $bGAStats = false;
                        $controller = new cms_usuarios();

                        // Se pode mostrar stats de Site
                        if($controller->valida_permissao("configuracoes","",false)) {
                            $bGAStats = true;
                        }

                        if($bGAStats) {

                            $iCount = 0;

                            foreach ($ga_props as $ga_prop) {
                                if(trim($ga_prop) != "") {

                                    $ga = $this->controller->ga;
                                    
                                    ?>
                                    <div class="panel panel-default card-view">
                                        <div class="panel-heading">
                                            <div class="pull-left">
                                                <h6 class="panel-title txt-dark">
                                                    <i class="icon-graph mr-10"></i>Views nos Últimos Meses
                                                </h6>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="panel-wrapper collapse in">
                                            <div class="panel-body">
                                                <div class="sm-graph-box">
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <div id="spark-visitas"></div>
                                                        </div>
                                                        <div class="col-xs-6">
                                                            <div class="counter-wrap text-right">
                                                                <?php

                                                                // Total de visualizações nos últimos 5 meses

                                                                $dt_inicio = $five_month_first_day;
                                                                $dt_fim = $this_month_last_day;

                                                                if($ga != null) {
                                                                    $ga->requestReportData($ga_prop, array(
                                                                        'year','month'
                                                                    ), array(
                                                                        'pageviews'
                                                                    ), array(
                                                                        'year','month'
                                                                    ), $filter=null, $dt_inicio, $dt_fim);
                                                                    
                                                                    $strViews = "";
                                                                    $iCount = 0;
                                                                    $results = $ga->getResults();
                                                                    $viewsMes = 0;
                                                                    $viewsMesAnt = 0;
                                                                    foreach($results as $result) {
                                                                        if($iCount == sizeof($results)-1) {
                                                                            $viewsMes = intval($result->getPageViews());
                                                                        } else if($iCount == sizeof($results)-2) {
                                                                            $viewsMesAnt = intval($result->getPageViews());
                                                                        }

                                                                        if(trim($strViews) != "") {
                                                                            $strViews .= ", ";
                                                                        }
                                                                        $strViews .= intval($result->getPageViews());
                                                                        $iCount++;
                                                                    }
                                                                }

                                                                ?>
                                                                <span class="counter-cap"><i class="<?php
                                                                if($viewsMes > $viewsMesAnt) {
                                                                    echo "fa fa-level-up txt-success";
                                                                } else if($viewsMes < $viewsMesAnt) {
                                                                    echo "fa fa-level-down txt-danger";
                                                                } else {
                                                                    echo "fa fa-minus txt-warning";
                                                                }
                                                                ?>"></i></span><span class="counter"><?php
                                                                if($viewsMes >= 1000) {
                                                                    if($viewsMes >= 1000000) {
                                                                        $viewAux = intval($viewsMes/10000)/100;
                                                                        echo $viewAux . "</span><span>m";
                                                                    } else {
                                                                        $viewAux = intval($viewsMes/100)/10;
                                                                        echo $viewAux . "</span><span>k";
                                                                    }
                                                                } else {
                                                                    echo $viewsMes;
                                                                }
                                                                ?></span>
                                                                <script type="text/javascript" language="javascript">
                                                                    var sparkViews = [<?php echo $strViews; ?>];
                                                                </script>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer txt-dark font-12">
                                                <?php if($propData[$ga_prop] != null) { echo $propData[$ga_prop]->getId(); ?> - <?php echo $propData[$ga_prop]->getName(); } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php

                                    $iCount++;
                                }
                            }
                            ?>

                            <div class="panel panel-default card-view">
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-heading">
                                        <div class="pull-left">
                                            <h6 class="panel-title txt-dark">
                                                <i class="fa fa-dollar mr-10"></i>Taxa de Conversão de Leads
                                            </h6>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="panel-wrapper collapse in">
                                        <div class="panel-body">
                                            <div class="sm-graph-box">
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <div id="spark-conversoes"></div>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <div class="counter-wrap text-right">
                                                            <?php

                                                            // Total de visualizações nos últimos 5 meses

                                                            $dt_inicio = $five_month_first_day;
                                                            $dt_fim = $this_month_last_day;

                                                            carrega_classe("contatos");
                                                            $convs = new contatos();
                                                            $convs = $convs->get_array_conversoes($dt_inicio,$dt_fim);

                                                            $strConvs = "";
                                                            $sparkConvsLabels = "";
                                                            
                                                            $iCount = 0;
                                                            $iAux = 5-sizeof($convs);
                                                            
                                                            $convsMes = 0;
                                                            $convsMesAnt = 0;
                                                            $totalMes = 0;
                                                            $totalMesAnt = 0;
                                                            $taxaMes = 0;
                                                            $taxaMesAnt = 0;
                                                            
                                                            foreach($convs as $conv) {
                                                                if($iCount == sizeof($convs)-1) {
                                                                    $convsMes = intval($conv->count_conv_mes);
                                                                    $totalMes = intval($conv->count_total_mes);
                                                                    $taxaMes = intval($conv->taxa_conv_mes*100);
                                                                } else if($iCount == sizeof($convs)-2) {
                                                                    $convsMesAnt = intval($conv->count_conv_mes);
                                                                    $totalMesAnt = intval($conv->count_total_mes);
                                                                    $taxaMesAnt = intval($conv->taxa_conv_mes*100);
                                                                }

                                                                if(trim($strConvs) != "") {
                                                                    $strConvs .= ", ";
                                                                }
                                                                if(trim($sparkConvsLabels) != "") {
                                                                    $sparkConvsLabels .= ", ";
                                                                }
                                                                $strConvs .= intval($conv->count_conv_mes); // valor_conv_mes é para pegar em valor
                                                                $sparkConvsLabels .= $iAux . ": " . "'" . intval($conv->count_conv_mes) . "/" . intval($conv->count_total_mes) . "'";

                                                                $iCount++;
                                                                $iAux++;
                                                            }

                                                            $diff = 5-$iCount;
                                                            while($diff > 0) {
                                                                if(trim($strConvs) != "") {
                                                                    $strConvs = "0, " . $strConvs;
                                                                } else {
                                                                    $strConvs = "0";
                                                                }
                                                                if(trim($sparkConvsLabels) != "") {
                                                                    $sparkConvsLabels = ($diff-1) . ": " . "'0', " . $sparkConvsLabels;
                                                                } else {
                                                                    $sparkConvsLabels = ($diff-1) . ": " . "'0'";
                                                                }
                                                                $diff--;
                                                                $iCount++;
                                                            }

                                                            //$varMes = $convsMes;
                                                            //$varMesAnt = $convsMesAnt;
                                                            //$simba = "";
                                                            $varMes = $taxaMes;
                                                            $varMesAnt = $taxaMesAnt;
                                                            $simba = "%";

                                                            ?>
                                                            <span class="counter-cap"><i class="<?php
                                                            if($varMes > $varMesAnt) {
                                                                echo "fa fa-level-up txt-success";
                                                            } else if($varMes < $varMesAnt) {
                                                                echo "fa fa-level-down txt-danger";
                                                            } else {
                                                                echo "fa fa-minus txt-warning";
                                                            }
                                                            ?>"></i></span><span class="counter"><?php
                                                            if($varMes >= 1000) {
                                                                if($varMes >= 1000000) {
                                                                    $convAux = intval($varMes/10000)/100;
                                                                    echo $convAux . "</span><span>m";
                                                                } else {
                                                                    $convAux = intval($varMes/100)/10;
                                                                    echo $convAux . "</span><span>k";
                                                                }
                                                            } else {
                                                                echo $varMes;
                                                            }
                                                            echo $simba;
                                                            ?></span>
                                                            <script type="text/javascript" language="javascript">
                                                                var sparkConvs = [<?php echo $strConvs; ?>];
                                                            </script>
                                                        </div>  
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer txt-dark font-12">
                                            <?php echo $convsMes; ?> convers<?php echo ($convsMes==1?"ão":"ões"); ?> de <?php echo $totalMes; ?> lead<?php echo ($totalMes==1?"":"s"); ?> no mês atual
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if(!$bGAStats) {
                            ?></div><div class="col-md-4"><?php
                        } ?>

                        <div class="panel panel-default card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-dark">
                                        <i class="icon-exclamation mr-10"></i>Dica Útil
                                    </h6>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <?php

                                    $arrayDicas = array();
                                    $arrayIndex = array();
                                    $iCountMax = 1;

                                    $iCount = 0;                            
                                    while($iCount < $iCountMax && $iCount < sizeof($_DICAS_UTEIS)){
                                        $iIndex = rand(0,sizeof($_DICAS_UTEIS)-1);
                                        if(!in_array($iIndex,$arrayIndex,true)){
                                            $arrayDicas[sizeof($arrayDicas)] = $_DICAS_UTEIS[$iIndex];
                                            $arrayIndex[sizeof($arrayIndex)] = $iIndex;
                                            $iCount++;
                                        }
                                    }
                                    
                                    $iCount = 0;
                                    foreach($arrayDicas as $tip){
                                        ?><p <?php
                                        if($iCount > 0) {
                                            echo " class=\"mt-20\" ";
                                        } ?>>
                                            <i class="icon-arrow-right-circle mr-10"></i><?php echo $tip; ?>
                                        </p><?php
                                        $iCount++;
                                    }
                                    
                                    ?>
                                </div>
                            </div>
                        </div>

                        <?php if(!$bGAStats) {
                            ?></div><div class="col-md-4"><?php
                        } ?>

                        <div class="panel panel-default card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-dark">
                                        <i class="icon-people mr-10"></i>Últimos Acessos no CMS
                                    </h6>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <?php

                                    $usuarios = new cms_usuarios();
                                    $usuarios = $usuarios->get_array_ativos("(
                                        !ISNULL(data_ultimoacesso) AND data_ultimoacesso != \"\" AND data_ultimoacesso != \"0000-00-00 00:00:00\"
                                    )","data_ultimoacesso DESC",3);
                                    $iCount = 0;
                                    foreach($usuarios as $usu)
                                    {
                                        $usu->carrega_dados();
                                        ?>
                                        <p <?php
                                        if($iCount > 0) {
                                            echo " class=\"mt-20\" ";
                                        } ?>>
                                            <i class="icon-user mr-10"></i><?php echo get_output($usu->get_var("nome"));?>
                                            <br />
                                            <i class="icon-calendar mr-10"></i><?php echo encode_data($usu->get_var("data_ultimoacesso"), true);?>
                                        </p>
                                        <?php
                                        $iCount++;
                                    }
                                    ?>    
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php

                    if($bGAStats) {
                        ?>
                        <div class="col-md-8">
                            <?php

                            $iCount = 0;

                            foreach ($ga_props as $ga_prop) {
                                if(trim($ga_prop) != "") {

                                    $ga = $this->controller->ga;
                                    
                                    ?>
                                    <div class="panel panel-info card-view">
                                        <div class="panel-heading">
                                            <div class="pull-left">
                                                <h6 class="panel-title txt-light">
                                                    <i class="icon-chart mr-10"></i>Usuários Diários no Mês Anterior (E Atual Até o Momento)
                                                </h6>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="panel-wrapper collapse in panel-visitas" id="panel-visitas-<?php echo $iCount; ?>">
                                            <div class="panel-body">
                                                <?php

                                                // Mês atual

                                                $dt_inicio = $this_month_first_day;
                                                $dt_fim = $this_month_last_day;

                                                if($ga != null) {
                                                    $ga->requestReportData($ga_prop, array(
                                                        'day'
                                                    ), array(
                                                        'users'
                                                    ), array(
                                                        'day',
                                                    ), $filter=null, $dt_inicio, $dt_fim);
                                                    
                                                    $strVisits = "";
                                                    $iCountV = 0;
                                                    $results = $ga->getResults();
                                                    foreach($results as $result) {
                                                        if(trim($strVisits) != "") {
                                                            $strVisits .= ", ";
                                                        }
                                                        $strVisits .= intval($result->getUsers());
                                                        $iCountV++;
                                                    }

                                                    // Mês anterior

                                                    $dt_inicio = $last_month_first_day;
                                                    $dt_fim = $last_month_last_day;

                                                    $ga->requestReportData($ga_prop, array(
                                                        'day'
                                                    ), array(
                                                        'users'
                                                    ), array(
                                                        'day',
                                                    ), $filter=null, $dt_inicio, $dt_fim);

                                                    $strVisitsAnterior = "";
                                                    $iCountVA = 0;
                                                    foreach($ga->getResults() as $result) {
                                                        if(trim($strVisitsAnterior) != "") {
                                                            $strVisitsAnterior .= ", ";
                                                        }
                                                        $strVisitsAnterior .= intval($result->getUsers());
                                                        $iCountVA++;
                                                    }

                                                    // Percorre visitas atuais preenchendo até a data do anterior, senão o contrário

                                                    if($iCountVA > $iCountV) {
                                                        for($w=$iCountV; $w<$iCountVA; $w++) {
                                                            if(trim($strVisits) != "") {
                                                                $strVisits .= ", ";
                                                            }
                                                            $strVisits .= "0";
                                                            $iCountV++;
                                                        }
                                                    } else {
                                                        for($w=$iCountVA; $w<$iCountV; $w++) {
                                                            if(trim($strVisitsAnterior) != "") {
                                                                $strVisitsAnterior .= ", ";
                                                            }
                                                            $strVisitsAnterior .= "0";
                                                            $iCountVA++;
                                                        }                                            
                                                    }
                                                }
                                                
                                                ?>
                                                <canvas id="chart-visitas-mes-<?php echo $iCount; ?>" class="chart-visitas-mes" height="320" data-count-id="<?php echo $iCount; ?>"></canvas>
                                                <script type="text/javascript" language="javascript">
                                                    chartRecords["chart-visitas-mes-<?php echo $iCount; ?>"] = [<?php echo $strVisits; ?>];
                                                    chartRecords["chart-visitas-mes-anterior-<?php echo $iCount; ?>"] = [<?php echo $strVisitsAnterior; ?>];
                                                </script>
                                            </div>
                                            <div class="panel-footer txt-dark font-12">
                                                <?php if($propData[$ga_prop] != null) { echo $propData[$ga_prop]->getId(); ?> - <?php echo $propData[$ga_prop]->getName(); } ?>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="panel panel-info card-view">
                                        <div class="panel-heading">
                                            <div class="pull-left">
                                                <h6 class="panel-title txt-light">
                                                    <i class="icon-chart mr-10"></i>Principais Conteúdos (Mês Anterior)
                                                </h6>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="panel-wrapper collapse in panel-contents" id="panel-contents-<?php echo $iCount; ?>">
                                            <div class="panel-body">
                                                <table class="ga-contents-table">
                                                    <tr class="header">
                                                        <td>
                                                            Página
                                                        </td>
                                                        <td>
                                                            Visitas
                                                        </td>
                                                    </tr>
                                                    <?php

                                                    // Mês atual

                                                    $dt_inicio = $last_month_first_day;
                                                    $dt_fim = $last_month_last_day;

                                                    if($ga != null) {
                                                        $ga->requestReportData($ga_prop, array(
                                                            'pagePath'
                                                        ), array(
                                                            'pageviews'
                                                        ), array(
                                                            '-pageviews'
                                                        ), $filter=null, $dt_inicio, $dt_fim);
                                                        
                                                        $results = $ga->getResults();
                                                        foreach($results as $result) {
                                                            if(intval($result->getPageViews()) > 0) {
                                                                ?>
                                                                <tr>
                                                                    <td><?php echo trim($result->getPagePath()); ?></td>
                                                                    <td><?php echo intval($result->getPageViews()); ?></td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }
                                                    }
                                                    
                                                    ?>
                                                </table>
                                            </div>
                                            <div class="panel-footer txt-dark font-12">
                                                <?php if($propData[$ga_prop] != null) { echo $propData[$ga_prop]->getId(); ?> - <?php echo $propData[$ga_prop]->getName(); } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php

                                    $iCount++;
                                }
                            }
                            ?>
                        </div>
                    </div>
                <?php } ?>
            </div>

        </div>
        <?php

        ?>
        <script type="text/javascript" language="javascript">

        /*Dashboard Init*/
 
        "use strict";

        /*Current Time Cal*/
        var getCurrentTime = function(){
            var now = moment().locale('pt-BR');
            var nowDate = now.format('L');
            var nowDay = now.format('dddd');
            $('.nowday').html(nowDay);
            $('.nowdate').html(nowDate);
        };

        /*Get Current Weather*/
        var getWeather = function(location, woeid) {
            
            if( $('#weather-card').length > 0 ){ 
                var arrAux = location.split(",");
                var urlAux = "";
                if(arrAux.length > 1) {
                    urlAux ="http://api.openweathermap.org/data/2.5/weather?lat=" + arrAux[0].toString() + "&lon=" + arrAux[1].toString() + "&lang=pt&units=metric&appid=5cf8dd0f84d028504da187dee1a8bf74";
                } else {
                    urlAux ="http://api.openweathermap.org/data/2.5/weather?q=" + location.toString() + "&lang=pt&units=metric&appid=5cf8dd0f84d028504da187dee1a8bf74";
                }

                $.ajax({
                    url: urlAux,
                    dataType: "json"
                }).done(function(data) {
                    var $this = $('#weather-card');
                    var html='<span class="block temprature">' + Math.floor(data.main.temp).toString() + '<span class="unit">&deg;C</span></span>';
                    $this.find('.left-block').html(html);
                    //html='<span class="block temprature-icon"><img src="' + ROOT_CMS + '_estilo/images/weathericons/1.svg"/></span><h6>' + data.name.toString() + '</h6>';
                    html='<span class="block temprature-icon"><img src="http://openweathermap.org/img/wn/' + data.weather[0].icon.toString() + '@2x.png"/></span><h6>' + data.name.toString() + '</h6>';
                    $this.find('.right-block').html(html);
                });

                /*With Forcast*/
                /* $.simpleWeather({
                    location: location,
                    woeid: woeid,
                    unit: 'c',
                    success: function(weather) {
                        var $this = $('#weather-card');
                        var html='<span class="block temprature">'+weather.temp+'<span class="unit">&deg;'+weather.units.temp+'</span></span>';
                        $this.find('.left-block').html(html);
                        //alert(this.id);
                        html='<span class="block temprature-icon"><img src="' + ROOT_CMS + '_estilo/images/weathericons/'+weather.code+'.svg"/></span><h6>'+weather.city+'</h6>';
                        
                        $this.find('.right-block').html(html);
                    },
                    error: function(error) {
                        $this.html('<p>'+error+'</p>');
                    }
                }); */
            }
        };

        var sparklineLogin = function() { 
            if( $('#spark-visitas').length > 0 ){
                $("#spark-visitas").sparkline(sparkViews, {
                    type: 'bar',
                    width: '100%',
                    height: '45',
                    barWidth: '10',
                    resize: true,
                    barSpacing: '10',
                    barColor: '#3cb878',
                    highlightSpotColor: '#3cb878'
                });
            }   

            if( $('#spark-conversoes').length > 0 ){
                $("#spark-conversoes").sparkline(sparkConvs, {
                    tooltipFormat: '{{offset:names}}',
                    tooltipValueLookups: {
                        names: {
                            <?php echo $sparkConvsLabels; ?>
                        }
                    },
                    type: 'line',
                    width: '100%',
                    height: '45',
                    lineColor: '#566FC9',
                    fillColor: '#566FC9',
                    maxSpotColor: '#566FC9',
                    highlightLineColor: 'rgba(0, 0, 0, 0.2)',
                    highlightSpotColor: '#566FC9'
                });
            }   
        };

        var sparkResize;
            $(window).resize(function(e) {
                clearTimeout(sparkResize);
                sparkResize = setTimeout(sparklineLogin, 200);
            });
        sparklineLogin(); 

        /*****Ready function start*****/
        $(document).ready(function(e){
            if($('.chart-visitas-mes').length > 0) {
                $('.chart-visitas-mes').each(function(e){
                    var elAux = $(this);
                    var sCID = elAux.data("count-id");

                    var arrLabels = [];
                    var d = new Date();
                    var lastDayOfMonth = new Date(d.getFullYear(), d.getMonth()+1, 0);
                    //for(var w=1; w<=d.getDate(); w++) {
                    for(var w=1; w<=lastDayOfMonth.getDate(); w++) {
                        arrLabels[arrLabels.length] = w.toString().lpad("0",2);
                    }

                    var ctx = document.getElementById(elAux.attr("id")).getContext("2d");
                    var data = {
                        labels: arrLabels,
                        datasets: [
                        {
                            label: "Visitas no mês atual",
                            backgroundColor: "rgba(60,184,120,0.4)",
                            borderColor: "rgba(60,184,120,0.4)",
                            pointBorderColor: "rgb(60,184,120)",
                            pointHighlightStroke: "rgba(60,184,120,1)",
                            data: chartRecords["chart-visitas-mes-" + sCID],
                            fill: true
                        },
                        {
                            label: "Visitas no mês anterior",
                            backgroundColor: "rgba(252,176,59,0.4)",
                            borderColor: "rgba(252,176,59,0.4)",
                            pointBorderColor: "rgb(252,176,59)",
                            pointBackgroundColor: "rgba(252,176,59,0.4)",
                            data: chartRecords["chart-visitas-mes-anterior-" + sCID],
                            fill: true
                        }
                    ]
                    };
                        
                    var areaChart = new Chart(ctx, {
                        type: "line",
                        data: data,
                        
                        options: {
                            tooltips: {
                                mode:"label"
                            },
                            elements:{
                                point: {
                                    //hitRadius:90
                                }
                            },
                            
                            scales: {
                                yAxes: [{
                                    stacked: false,
                                    gridLines: {
                                        color: "#eee",
                                    },
                                    ticks: {
                                        fontFamily: "Varela Round",
                                        fontColor:"#2f2c2c"
                                    }
                                }],
                                xAxes: [{
                                    stacked: false,
                                    gridLines: {
                                        color: "#eee",
                                    },
                                    ticks: {
                                        fontFamily: "Varela Round",
                                        fontColor:"#2f2c2c"
                                    }
                                }]
                            },
                            animation: {
                                duration:   3000
                            },
                            responsive: true,
                            maintainAspectRatio:false,
                            legend: {
                                display: false,
                            },
                            tooltips: {
                                backgroundColor:'rgba(47,44,44,.9)',
                                cornerRadius:0,
                                footerFontFamily:"'Varela Round'"
                            }
                            
                        }
                    });
                });
            }

            if(navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(
                    function(position) { 
                        getWeather(position.coords.latitude+','+position.coords.longitude); //load weather using your lat/lng coordinates
                        getCurrentTime(); //Get the initial time.
                        setInterval(function(){
                            getWeather(position.coords.latitude+','+position.coords.longitude);
                        }, 300000); //Update time after every 5 min.
                    },
                    function(failure) {
                        $.getJSON('https://ipinfo.io/geo', function(response) { 
                            var loc = response.loc.split(',');
                            var coords = {
                                latitude: loc[0],
                                longitude: loc[1]
                            };
                            getWeather(coords.latitude+','+coords.longitude); //load weather using your lat/lng coordinates
                            getCurrentTime(); //Get the initial time.
                            setInterval(function(){
                                getWeather(coords.latitude+','+coords.longitude);
                            }, 300000); //Update time after every 5 min.
                        });  
                    }
                );
            } else {
                /*getWeather("Caxias do Sul",""); //Get the initial weather.
                getCurrentTime(); //Get the initial time.
                setInterval(function(){
                    getWeather("Caxias do Sul","");
                }, 300000); //Update time after every 5 min.*/
                $.getJSON('https://ipinfo.io/geo', function(response) { 
                    var loc = response.loc.split(',');
                    var coords = {
                        latitude: loc[0],
                        longitude: loc[1]
                    };
                    getWeather(coords.latitude+','+coords.longitude); //load weather using your lat/lng coordinates
                    getCurrentTime(); //Get the initial time.
                    setInterval(function(){
                        getWeather(coords.latitude+','+coords.longitude);
                    }, 300000); //Update time after every 5 min.
                }); 
            }
        });
        </script>            
        <?php
    }
    
    function get_ref_acao_log($abrev)
    {
        switch(strtoupper($abrev))
        {
            default:break;
            case "I": return get_lang("_INCLUSAO");
            case "A": return get_lang("_ALTERACAO");
            case "E": return get_lang("_EXCLUSAO"); 
        }
    }

    function heading() {
        global $projectColors;
        ?>
        <!-- Title -->
        <div class="row heading-bg bg-<?php echo $projectColors["main"]; ?>">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-light">
                    <i class="icon-like"></i> <span class="btn-text"><?php echo ($this->nome_exibicao_singular); ?></span>
                </h5>
            </div>

            <!-- Breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <?php
            
                    $array_nav = $this->controller->get_bread_crumbs();

                    $iCount = 0;
                    foreach($array_nav as $nav) {
                        if($iCount >= sizeof($array_nav)-1) {
                            ?>
                            <li class="active"><span><?php echo $nav[0]; ?></span></li>
                            <?php
                        } else { ?>
                            <li><a href="<?php echo $nav[1]; ?>"><?php echo $nav[0]; ?></a></li>
                            <?php
                        }

                        $iCount++;
                    }

                    ?>
                </ol>
            </div>
            <!-- /Breadcrumb -->
        
        </div>
        <!-- /Title -->
        <?php
    }
}
    
?>