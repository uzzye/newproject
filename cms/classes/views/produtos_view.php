<?php    
include_once(ROOT_CMS . "classes/produtos_categorias.php");
   
class produtos_view extends view
{     
        function __construct($owner = null)
        {
            global $modulo;

            $this->nome = "produtos";
            if(function_exists("get_lang")) {
              $this->nome_exibicao = get_lang("_PRODUTOS");
              $this->nome_exibicao_singular = get_lang("_PRODUTO");
            }
        
            parent::__construct($owner);       

            $this->custom_expr_masks["imagem"] = "return(\"<img src='\" . \$this->get_upload_folder(\"imagem\") . \$valor . \"' border='0' width='200' />\");";

            $this->custom_expr_masks["destaque"] = "if(\$valor == 1) {return \"" . get_lang("_SIM") . "\";} else {return \"" . get_lang("_NAO") . "\";}";
        }   
    
        function monta_campos_form($id = "", $readonly = false, $clone = false, $resumido = false)
        {
            global $modulo;
        
            $b_editando = false;       
            if(trim($_REQUEST["id"]) <> "")
            {
                $id = $_REQUEST["id"];
                $this->controller->set_var("id",$id);
                $this->controller->carrega_dados();
                $b_editando = true;
            }
        
            $array_form_campos = array();
        
            $count_fields = 0;
                    
          $ref = "categorias";
          $checked = $this->controller->get_var($ref);
            if(trim($_REQUEST[$ref]) <> "")
            {$checked = $_REQUEST[$ref];}
            $inputAux = new Uzzye_ListBox($ref,$ref,get_lang("_CATEGORIAS"));
            $inputAux->default_display = get_lang("_COMBOSELECIONE");
            $inputAux->default_value = 0;
            $inputAux->checked_value = $checked;
            $inputAux->set_reference_item("produtos_categorias");
            $inputAux->size = 1;
            $inputAux->multiple = false;
          $inputAux->set_value($this->controller->get_var($ref));
            $inputAux->li_class = "w50p";
            $inputAux->required = $this->controller->is_required($ref);
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_fields++;  
                
              $ref = "titulo";
          $inputAux = new Uzzye_TextField($ref,$ref,get_lang("_TITULO"));
          $inputAux->set_value($this->controller->get_var($ref));
          $inputAux->li_class = "w50p";
          $inputAux->required = $this->controller->is_required($ref);
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_fields++;      
                
              $ref = "imagem";
          $inputAux = new Uzzye_ImageCrop("crop_" . $ref,"crop_" . $ref,get_lang("_IMAGEM"));
          $inputAux->li_class = "w50p";
            $inputAux->clone = $clone;
            $inputAux->label = get_lang("_IMAGEM") . " (" . get_lang("_TAMANHORECOMENDAVEL") . " 720px x 100%)";
            $inputAux->crops = array(
                array("imagem",get_lang("_IMAGEM"),0,720,0)  
            );
            $inputAux->set_value($b_editando);
            $inputAux->required = $this->controller->is_required($ref);
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_fields++;  
                
              $ref = "chamada_pt";
          $inputAux = new Uzzye_CKEditorField($ref,$ref,get_lang("_CHAMADA_PT"));
          $inputAux->set_value($this->controller->get_var($ref));
            $inputAux->li_class = "w100p";
            $inputAux->required = $this->controller->is_required($ref);
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_textarea++; 
                
              $ref = "chamada_en";
          $inputAux = new Uzzye_CKEditorField($ref,$ref,get_lang("_CHAMADA_EN"));
          $inputAux->set_value($this->controller->get_var($ref));
            $inputAux->li_class = "w100p";
            $inputAux->required = $this->controller->is_required($ref);
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_textarea++; 
                
              $ref = "chamada_es";
          $inputAux = new Uzzye_CKEditorField($ref,$ref,get_lang("_CHAMADA_ES"));
          $inputAux->set_value($this->controller->get_var($ref));
            $inputAux->li_class = "w100p";
            $inputAux->required = $this->controller->is_required($ref);
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_textarea++;  
                
              $ref = "descricao_pt";
          $inputAux = new Uzzye_CKEditorField($ref,$ref,get_lang("_DESCRICAO_PT"));
          $inputAux->set_value($this->controller->get_var($ref));
            $inputAux->li_class = "w100p";
            $inputAux->required = $this->controller->is_required($ref);
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_textarea++; 
                
              $ref = "descricao_en";
          $inputAux = new Uzzye_CKEditorField($ref,$ref,get_lang("_DESCRICAO_EN"));
          $inputAux->set_value($this->controller->get_var($ref));
            $inputAux->li_class = "w100p";
            $inputAux->required = $this->controller->is_required($ref);
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_textarea++; 
                
              $ref = "descricao_es";
          $inputAux = new Uzzye_CKEditorField($ref,$ref,get_lang("_DESCRICAO_ES"));
          $inputAux->set_value($this->controller->get_var($ref));
            $inputAux->li_class = "w100p";
            $inputAux->required = $this->controller->is_required($ref);
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_textarea++; 
                
              $ref = "caracteristicas_pt";
          $inputAux = new Uzzye_CKEditorField($ref,$ref,get_lang("_CARACTERISTICAS_PT"));
          $inputAux->set_value($this->controller->get_var($ref));
            $inputAux->li_class = "w100p";
            $inputAux->required = $this->controller->is_required($ref);
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_textarea++; 
                
              $ref = "caracteristicas_en";
          $inputAux = new Uzzye_CKEditorField($ref,$ref,get_lang("_CARACTERISTICAS_EN"));
          $inputAux->set_value($this->controller->get_var($ref));
            $inputAux->li_class = "w100p";
            $inputAux->required = $this->controller->is_required($ref);
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_textarea++; 
                
              $ref = "caracteristicas_es";
          $inputAux = new Uzzye_CKEditorField($ref,$ref,get_lang("_CARACTERISTICAS_ES"));
          $inputAux->set_value($this->controller->get_var($ref));
            $inputAux->li_class = "w100p";
            $inputAux->required = $this->controller->is_required($ref);
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_textarea++; 
                
              $ref = "produtos_relacionados";
          $checked = $this->controller->get_var($ref);
            if(trim($_REQUEST[$ref]) <> "")
            {$checked = $_REQUEST[$ref];}
            $inputAux = new Uzzye_ListBox($ref,$ref,get_lang("_PRODUTOS_RELACIONADOS"));
            $inputAux->default_display = get_lang("_NENHUM");
            $inputAux->default_value = 0;
            $inputAux->checked_value = $checked;
            $inputAux->set_reference_item("produtos_controller");
            $inputAux->size = 1;
            $inputAux->multiple = true;
          $inputAux->set_value($this->controller->get_var($ref));
            $inputAux->li_class = "w50p";
            $inputAux->required = $this->controller->is_required($ref);
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_fields++; 
                
              $ref = "destaque";
          $checked = $this->controller->get_var($ref);
            if(trim($_REQUEST[$ref]) <> "")
            {$checked = $_REQUEST[$ref];}
          $inputAux = new Uzzye_CheckBox($ref,$ref,get_lang("_DESTAQUE"));
          $inputAux->set_value(1);
            $inputAux->default_value = 0;
            $inputAux->checked_value = $checked;
            $inputAux->li_class = "w25p";
            $inputAux->required = $this->controller->is_required($ref);
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_fields++; 
                
              $ref = "mostrar_menu";
          $checked = $this->controller->get_var($ref);
            if(trim($_REQUEST[$ref]) <> "")
            {$checked = $_REQUEST[$ref];}
          $inputAux = new Uzzye_CheckBox($ref,$ref,get_lang("_MOSTRAR_MENU"));
          $inputAux->set_value(1);
            $inputAux->default_value = 0;
            $inputAux->checked_value = $checked;
            $inputAux->li_class = "w25p";
            $inputAux->required = $this->controller->is_required($ref);
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_fields++; 
        
              $ref = "url_loja";
            $inputAux = new Uzzye_TextField($ref,$ref,get_lang("_URL_LOJA"));
            $inputAux->set_value($this->controller->get_var($ref));
            $inputAux->li_class = "w50p";
            $inputAux->required = $this->controller->is_required($ref);
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_fields++;  
              
              $ref = "target_loja";
            $checked = $this->controller->get_var($ref);
            if(trim($_REQUEST[$ref]) <> "")
            {$checked = $_REQUEST[$ref];}
            $inputAux = new Uzzye_ComboBox($ref,$ref,get_lang("_TARGET_LOJA"));
            $inputAux->default_display = get_lang("_COMBOSELECIONE");
            $inputAux->default_value = "";
            $inputAux->checked_value = $checked;
            $inputAux->size = 1;
                  $inputAux->multiple = false;
            $inputAux->set_options(array(array(output_decode(""),output_decode("")),array(output_decode("_blank"),output_decode("_blank"))));
                  $inputAux->set_value($this->controller->get_var($ref));
            $inputAux->li_class = "w50p";
            $inputAux->required = $this->controller->is_required($ref);
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_fields++;  
                    
            $ref = "manual";
            $inputAux = new Uzzye_FileField($ref,$ref,get_lang("_MANUAL"));
            $inputAux->set_value($this->controller->get_var($ref));
            $inputAux->li_class = "w50p";
            $inputAux->required = $this->controller->is_required($ref);
              $array_form_campos[sizeof($array_form_campos)] = $inputAux;
              $count_fields++;
                
            $ref = "produtos_imagens";
            $inputAux = new Uzzye_ImageGallery();
              $inputAux->clone = $clone;
            $inputAux->label = get_lang("_GALERIA");
            $inputAux->name = $ref;
            $inputAux->li_class = 'w100p';
            $inputAux->id = $ref;
            $inputAux->id_registro = intval($id);
            $inputAux->sql_where = "";
            $inputAux->b_editando = $b_editando;
            $inputAux->set_reference_item($this->controller->get_item_img_gallery($ref));
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
                
          
            $this->array_form_campos = $array_form_campos;

            parent::monta_campos_form($id, $readonly, $clone, $resumido);   
        }
}

?>