<?php    
	
class contatos_view extends view
{	  
		function __construct($owner = null)
		{
			global $modulo, $acao, $_CONFIG_FORMAT;
			
			$this->nome = "contatos";
			if(function_exists("get_lang")) {
				$this->nome_exibicao = get_lang("_CONTATOS");
				$this->nome_exibicao_singular = get_lang("_CONTATO");
			
				parent::__construct($owner);

				$this->array_acoes_cms = array(
					"<a rel=\"address\" href=\"#/" . $modulo . "/visualizar/#PARAM#\" class=\"mr-5\" data-toggle=\"tooltip\" data-original-title=\"" . (get_lang("_VISUALIZAR")) . "\"> <i class=\"fa fa-eye text-inverse\"></i> </a>",

					"<a href=\"#\" data-id=\"#PARAM#\" data-url=\"" . ROOT_SERVER . ROOT . "ajax/" . $modulo . "/converter/#PARAM#\" class=\"mr-5 acao-toggle\" data-toggle=\"tooltip\" data-original-title=\"" . (get_lang("_CONVERTER")) . "\" data-function=\"registro_convertido(returnData, btnAcao)\"
						data-swal-condition=\"$(this).find('.fa').hasClass('text-dark')\"
						data-swal-value=\"" . $_CONFIG_FORMAT["valor_conversao_contato"] . "\"
						data-swal-placeholder=\"R$ 0,00\"
						data-swal-type=\"input\"
					> <i class=\"fa fa-dollar #TOGGLE_CLASS#\"></i> </a>"
				);

				$this->array_expr_acoes_cms = array(
					"",

					"return(intval(\$objAux->get_var(\"conversao\")==1)?\"text-success\":\"text-dark\");"
				);

				$this->array_botoes_cms = array(
					"<a rel=\"address\" href=\"#/" . $modulo . "/exportar\"><button type=\"button\" class=\"btn btn-success btn-anim\"><i class=\"icon-cloud-download\"></i><span class=\"btn-text\">" . get_lang("_EXPORTAR") . " " . get_lang("_CSV") . "</span></button></a>"
				);

				if($modulo == "contatos" && $acao != "get_registros" && $acao != "get_registros_html") {
					?>
					<script language="javascript">
					function registro_convertido(data, btnAcao) {
						btnAcao.closest("tr[role=row]").find(".col-conversao").html(data);
						$(".row.alert-display").remove();
			        	if(btnAcao.find(".fa").hasClass("text-success")) {
			        		btnAcao.find(".fa").removeClass("text-success").addClass("text-dark");
			        	} else if(btnAcao.find(".fa").hasClass("text-dark")) {
			        		btnAcao.find(".fa").removeClass("text-dark").addClass("text-success");
			        	}
					}
					</script>
					<?php
				}
			} else {
				parent::__construct($owner);
			}

    		$this->array_perms_acoes_cms = array(
    			"Visualizar",
    			"AtivarDesativar"
			);

			$this->array_perms_botoes_cms = array(
				"Exportar"
			);

			$this->number_masks["valor_conversao"] = array(2,",",".","R$ ");

			$this->custom_expr_masks["conversao"] = "return((floatval(\$this->get_var(\"valor_conversao\")))?get_lang(\"_CONVERTIDO\") . \" (\" . \$this->get_var_format(\"valor_conversao\") . \")\":\"\");";
		}	
	
		function monta_campos_form($id = "", $readonly = false, $clone = false, $resumido = false)
		{
			global $modulo;
		
			$b_editando = false;		
			if(trim($_REQUEST["id"]) <> "")
			{
				$id = $_REQUEST["id"];
				$this->controller->set_var("id",$id);
				$this->controller->carrega_dados();
				$b_editando = true;
			}
		
			$array_form_campos = array();
		
			$count_fields = 0;
				
			$ref = "assunto";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_ASSUNTO"));
	$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w100p";
			$inputAux->readonly = true;
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;
					
			$ref = "nome";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_NOME"));
	$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p";
			$inputAux->readonly = true;
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;		
				
			$ref = "email";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_EMAIL"));
	$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p";
			$inputAux->readonly = true;
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;	
			
			$ref = "telefone";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_TELEFONE"));
	$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w25p";
			$inputAux->readonly = true;
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;
					
			$ref = "cidade";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_CIDADE"));
	$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p";
			$inputAux->readonly = true;
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;	
					
			$ref = "estado";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_ESTADO"));
	$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w25p";
			$inputAux->readonly = true;
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;	
			
			$ref = "id_referencia";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_ID_REFERENCIA"));
	$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w33p";
			$inputAux->readonly = true;
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;

			$ref = "referencia";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_REFERENCIA"));
	$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w66p";
			$inputAux->readonly = true;
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;
			
			$ref = "mensagem";
			$inputAux = new Uzzye_TextArea($ref,$ref,get_lang("_MENSAGEM"));
	$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w100p";
			$inputAux->readonly = true;
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;
			
			/*$ref = "arquivo";
			$inputAux = new Uzzye_FileField($ref,$ref,get_lang("_ARQUIVO"));
	$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w100p";
			$inputAux->readonly = true;
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;*/
				
			$ref = "conversao";
			$inputAux = new Uzzye_CheckBox($ref,$ref,get_lang("_CONVERSAO"));
			$inputAux->default_value = 0;
			$inputAux->checked_value = $this->controller->get_var($ref);		
			$inputAux->set_value(1);
			$inputAux->li_class = "w50p";
			$inputAux->readonly = true;
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;

			$ref = "valor_conversao";
			$inputAux = new Uzzye_NumberField();
			$inputAux->set_mask($this->number_masks[$ref]);
			$inputAux->label = get_lang("_VALOR_CONVERSAO");
			$inputAux->li_class = "w50p";
			$inputAux->name = $ref;
			$inputAux->id = $ref;
			$inputAux->readonly = true;
			$inputAux->set_value($this->controller->get_var($ref));
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_text++;	
				
			$ref = "recebe_novidades";
			$inputAux = new Uzzye_CheckBox($ref,$ref,get_lang("_RECEBE_NOVIDADES"));
			$inputAux->default_value = 0;
			$inputAux->checked_value = $this->controller->get_var($ref);		
			$inputAux->set_value(1);
			$inputAux->li_class = "w50p";
			$inputAux->readonly = true;
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;
			
			$ref = "ip";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_IP"));
	$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p";
			$inputAux->readonly = true;
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;
				
			
	$this->array_form_campos = $array_form_campos;	
		}
}

?>