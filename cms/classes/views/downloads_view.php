<?php    
	
class downloads_view extends view
{	  
		function __construct($owner = null)
		{
			global $modulo, $_CONFIG_FORMAT;

			$this->nome = "downloads";

			if(function_exists("get_lang")) {			
				$this->nome_exibicao = get_lang("_DOWNLOADS");
				$this->nome_exibicao_singular = get_lang("_DOWNLOAD");
			
				parent::__construct($owner);

				$this->array_acoes_cms = array(
					"<a rel=\"address\" href=\"#/" . $modulo . "/visualizar/#PARAM#\" class=\"mr-5\" data-toggle=\"tooltip\" data-original-title=\"" . (get_lang("_VISUALIZAR")) . "\"> <i class=\"fa fa-eye text-inverse\"></i> </a>"
				);

				$this->array_expr_acoes_cms = array(
					""
				);

				$this->array_botoes_cms = array(
					"<a rel=\"address\" href=\"#/" . $modulo . "/exportar\"><button type=\"button\" class=\"btn btn-success btn-anim\"><i class=\"icon-cloud-download\"></i><span class=\"btn-text\">" . get_lang("_EXPORTAR") . " " . get_lang("_CSV") . "</span></button></a>"
				);
			} else {
				parent::__construct($owner);
			}

    		$this->array_perms_acoes_cms = array(
    			"Visualizar"
			);

			$this->array_perms_botoes_cms = array(
				"Exportar"
			);	
		}	
	
		function monta_campos_form($id = "", $readonly = false, $clone = false, $resumido = false)
		{
			global $modulo;
		
			$b_editando = false;		
			if(trim($_REQUEST["id"]) <> "")
			{
				$id = $_REQUEST["id"];
				$this->controller->set_var("id",$id);
				$this->controller->carrega_dados();
				$b_editando = true;
			}
		
			$array_form_campos = array();
		
			$count_fields = 0;
					
			$ref = "modulo";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_MODULO"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$inputAux->readonly = true;

			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;	
				
				$ref = "id_registro";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_ID_DO_REGISTRO"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w25p";
			$inputAux->required = $this->controller->is_required($ref);
			$inputAux->readonly = true;

			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;	
				
				$ref = "arquivo";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_ARQUIVO"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$inputAux->readonly = true;
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;	
				
				$ref = "nome_arquivo";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_NOME_ARQUIVO"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$inputAux->readonly = true;

			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;
				
				$ref = "tamanho";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_TAMANHO"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$inputAux->readonly = true;

			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;
				
				$ref = "id_usuario";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_ID_DO_USUARIO"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w25p";
			$inputAux->required = $this->controller->is_required($ref);
			$inputAux->readonly = true;

			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;	
				
				$ref = "nome";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_NOME"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$inputAux->readonly = true;

			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;	
				
				$ref = "email";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_EMAIL"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$inputAux->readonly = true;

			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;
				
				$ref = "telefone";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_TELEFONE"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$inputAux->readonly = true;

			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;	
				
				$ref = "cidade";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_CIDADE"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$inputAux->readonly = true;

			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;	
				
				$ref = "estado";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_ESTADO"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$inputAux->readonly = true;

			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;	
				
				$ref = "id_referencia";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_ID_DA_REFERENCIA"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w25p";
			$inputAux->required = $this->controller->is_required($ref);
			$inputAux->readonly = true;

			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;	
				
				$ref = "referencia";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_REFERENCIA"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$inputAux->readonly = true;

			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;	
				
				$ref = "ip";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_IP"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$inputAux->readonly = true;

			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;	
				
			
			$this->array_form_campos = $array_form_campos;

            parent::monta_campos_form($id, $readonly, $clone, $resumido);	
		}
}

?>