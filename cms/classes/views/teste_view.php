<?php

include_once(ROOT_CMS . "classes/cms_modulos.php");

class teste_view extends view
{	  
    function __construct($owner = null)
	{
		$this->nome = "teste";
		if(function_exists("get_lang")) {
			$this->nome_exibicao = get_lang("_TESTE");
			$this->nome_exibicao_singular = get_lang("_TESTE");
		}
		
		parent::__construct($owner);	

		$this->number_masks["valor"] = array(2,",",".","R$ ");

		$this->date_masks["data"] = false;

		$this->color_masks["cor"] = "hexa";

		$this->lang_table_vars = array("Título","Descrição");
    }
	
	function monta_campos_form($id = "", $readonly = false, $clone = false, $resumido = false)
	{
		global $modulo;
		
		$b_editando = false;		
		if(trim($_REQUEST["id"]) <> "")
		{
			$id = $_REQUEST["id"];
			$this->controller->set_var("id",$id);
			$this->controller->carrega_dados();
			$b_editando = true;
		}
		
		$array_form_campos = array();

		/*$inputAux = new Uzzye_Script();
		$inputAux->set_value("
		function test() {
			alert(\"aqui\");
		}
		test();
		");
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_text++;*/

		// CAMPO TITULO
		$ref = "titulo";
		$inputAux = new Uzzye_TextField();
		$inputAux->label = get_lang("_TITULO");
		$inputAux->hint = "Título da página, exibido em destaque e letras maiores.";
		$inputAux->max_length = 255;
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->set_value($this->controller->get_var($ref));
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_text++;

		// CAMPO VALOR
		$ref = "valor";
		$inputAux = new Uzzye_NumberField();
		$inputAux->set_mask($this->number_masks[$ref]);
		$inputAux->label = get_lang("_VALOR");
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->set_value($this->controller->get_var($ref));
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_text++;	

		// CAMPO DATA
		$ref = "data";
		$inputAux = new Uzzye_DateField();
		$inputAux->label = get_lang("_DATA");
		$inputAux->time = $this->date_masks[$ref];
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->set_value($this->controller->get_var($ref));
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_text++;	

		// CAMPO COR
		$ref = "cor";
		$inputAux = new Uzzye_ColorField();
		$inputAux->label = get_lang("_COR");
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->set_value($this->controller->get_var($ref));
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_text++;		

		// CAMPO BOTÃO
		$ref = "button";
		$inputAux = new Uzzye_Button();
		//$inputAux->label = get_lang("_CLIQUE");
		$inputAux->value = get_lang("_CLIQUE");
		$inputAux->name = $ref;
		$inputAux->onclick = "alert('aqui');";
		$inputAux->id = $ref;
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_text++;

		// CAMPO HIDDEN
		$ref = "hidden";
		$inputAux = new Uzzye_HiddenField();
		$inputAux->label = get_lang("_CLIQUE");
		$inputAux->value = 1;
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_text++;

		// CAMPO DESCRIÇÃO
		$ref = "descricao";
		$inputAux = new Uzzye_TextArea();
		$inputAux->label = get_lang("_DESCRICAO");
		$inputAux->max_length = 20;
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->set_value($this->controller->get_var($ref));
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_text++;
		
	// CAMPO TEXTO
		$ref = "descricao_html";
		$inputAux = new Uzzye_CKEditorField();		
		$inputAux->label = get_lang("_TEXTO");
		$inputAux->max_length = 200;
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->set_value($this->controller->get_var($ref));
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_textarea++;

		// PARA USAR COM CAMPO
		$ref = "cms_modulos";
		$checked = $this->controller->get_var($ref);
		if(trim($_REQUEST[$ref]) <> "") {
			$checked = $_REQUEST[$ref];
		}
		$inputAux = new Uzzye_ListBox($ref,$ref,get_lang("_MODULOS"),0);
		$inputAux->default_value = 0;
		$inputAux->set_value($checked);
		$inputAux->multiple = true;
		$inputAux->required = true;
		$inputAux->set_reference_item($this->controller->get_reference_model($ref));
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_fields++;

	// HR
		$inputAux = new Uzzye_HR();		
		$inputAux->label = "Outros campos";
		$inputAux->icon = "icon-pin";
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;

	// CAMPO IMAGEM
		$ref = "imagem_crop";
		$inputAux = new Uzzye_ImageCrop();		
		$inputAux->label = get_lang("_IMAGEM");
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->crops = array(
			array("imagem",get_lang("_NORMAL"),0,1920,0,80),
			array("imagem_thumb",get_lang("_THUMB"),0,300,100,80)
		);
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_textarea++;

	// CAMPO ARQUIVO
		$ref = "arquivo";
		$inputAux = new Uzzye_FileField();		
		$inputAux->label = get_lang("_ARQUIVO");
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->set_value($this->controller->get_var($ref));
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_textarea++;

	// CAMPO GEOLOCALIZACAO
		$ref = "geolocalizacao";
		$inputAux = new Uzzye_GeolocationField();		
		$inputAux->label = get_lang("_GEOLOCALIZACAO");
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->set_value($this->controller->get_var($ref));
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_textarea++;

		// TESTE COMO DATATABLE
		$ref = "itens_teste";
    	$inputAux = new Uzzye_DataTable();
    	$inputAux->clone = $clone;
		$inputAux->label = get_lang("_ITENS_TESTE");
		$inputAux->name = $ref;
		$inputAux->li_class = 'w100p';
		$inputAux->id = $ref;
		$inputAux->id_registro = intval($id);
		$inputAux->sql_where = "";
		$inputAux->b_editando = $b_editando;
		$inputAux->set_reference_item($this->controller->get_item_data_table($ref));
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;

		/*$ref = "itens_teste";
		$inputAux = new Uzzye_FileGallery(); // TESTE COMO FILEGALLERY
    	//$inputAux = new Uzzye_ImageGallery(); // TESTE COMO IMAGEGALLERY
    	$inputAux->clone = $clone;
		$inputAux->label = get_lang("_ITENS_TESTE");
		$inputAux->name = $ref;
		$inputAux->li_class = 'w100p';
		$inputAux->id = $ref;
		$inputAux->id_registro = intval($id);
		$inputAux->sql_where = "";
		$inputAux->b_editando = $b_editando;
		$inputAux->set_reference_item($this->controller->get_item_file_gallery($ref));
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;*/

		/*$ref = "imagem";
		$inputAux = new Uzzye_Image();		
		$inputAux->label = get_lang("_IMAGEM");
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->set_value(ROOT_SERVER . ROOT . UPLOAD_FOLDER . "usuarios_avatar_1.jpg");
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_textarea++;*/

		/*$ref = "imagem_thumb_crop";
		$inputAux = new Uzzye_ImageCrop();		
		$inputAux->label = get_lang("_THUMB");
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->crops = array(
			array("imagem_thumb",get_lang("_THUMB"),0,300,100,80)
		);
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_textarea++;*/
		
		/*$ref = "id_idioma";
		$checked = $this->controller->get_var($ref);
		if(trim($_REQUEST[$ref]) <> "") {
			$checked = $_REQUEST[$ref];
		} else if(intval($checked) == 0) {
			$checked = 99;
		}
		$inputAux = new Uzzye_ListBox($ref,$ref,get_lang("_IDIOMA"),0);
		$inputAux->default_display = get_lang("_COMBOSELECIONE");
        $inputAux->default_value = 0;
		$inputAux->checked_value = $checked;
		$inputAux->size = 1;
		$classAux = "idiomas_controller";
		$inputAux->reference_model = "idiomas";
		$objAux = new $classAux();
		$inputAux->set_options($objAux->get_array_options("id","titulo","","titulo ASC"));
		$inputAux->refresh_options(array("id","titulo","","titulo ASC"));
		$inputAux->set_value($checked);
		$inputAux->li_class = "w66p";
		$inputAux->required = true;
		$inputAux->multiple = false;
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_fields++;*/

	// Exemplo de referências
		
		// PARA USAR COM TABELA
		/*$ref = "modulos_teste";
		$checked = $this->controller->get_var_referencia($ref);
		if(trim($_REQUEST[$ref]) <> "") {
			$checked = $_REQUEST[$ref];
		}
		$inputAux = new Uzzye_ListBox($ref,$ref,get_lang("_MODULOS"),0);
		$inputAux->set_value($checked);
		$inputAux->multiple = true;
		$inputAux->required = true;
		$classAux = "cms_modulos_controller";
		$inputAux->reference_model = "cms_modulos";
		$objAux = new $classAux();
		$inputAux->set_options($objAux->get_array_options("id","nome","","nome ASC"));
		$inputAux->refresh_options(array("id","nome","","nome ASC"));
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_fields++;*/

	// Fim exemplo
		
		$this->array_form_campos = $array_form_campos;	

		parent::monta_campos_form($id, $readonly, $clone, $resumido);
	}
}