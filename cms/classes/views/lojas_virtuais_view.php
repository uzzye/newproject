<?php    
   
class lojas_virtuais_view extends view
{     
        function __construct($owner = null)
        {
            global $modulo;

            $this->nome = "lojas_virtuais";
            if(function_exists("get_lang")) {
              $this->nome_exibicao = get_lang("_LOJAS_VIRTUAIS");
              $this->nome_exibicao_singular = get_lang("_LOJA_VIRTUAL");
            }
        
            parent::__construct($owner);   

            $this->custom_expr_masks["logo"] = "return(\"<img src='\" . \$this->get_upload_folder(\"logo\") . \$valor . \"' border='0' width='200' />\");";         
        }   
    
        function monta_campos_form($id = "", $readonly = false, $clone = false, $resumido = false)
        {
            global $modulo;
        
            $b_editando = false;       
            if(trim($_REQUEST["id"]) <> "")
            {
                $id = $_REQUEST["id"];
                $this->controller->set_var("id",$id);
                $this->controller->carrega_dados();
                $b_editando = true;
            }
        
            $array_form_campos = array();
        
            $count_fields = 0;
                    
          $ref = "titulo";
          $inputAux = new Uzzye_TextField($ref,$ref,get_lang("_TITULO"));
          $inputAux->set_value($this->controller->get_var($ref));
          $inputAux->li_class = "w50p";
          $inputAux->required = $this->controller->is_required($ref);
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_fields++;   
                
              $ref = "logo";
          $inputAux = new Uzzye_ImageCrop("crop_" . $ref,"crop_" . $ref,get_lang("_LOGO"));
          $inputAux->li_class = "w50p";
            $inputAux->clone = $clone;
            $inputAux->label = get_lang("_LOGO") . " (PNG - " . get_lang("_TAMANHORECOMENDAVEL") . " 200px x 200px)";
            $inputAux->crops = array(
                array("logo",get_lang("_LOGO"),0,200,200)  
            );
            $inputAux->set_value($b_editando);
            $inputAux->required = $this->controller->is_required($ref);
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_fields++;   
                
              $ref = "url";
          $inputAux = new Uzzye_TextField($ref,$ref,get_lang("_URL"));
          $inputAux->set_value($this->controller->get_var($ref));
          $inputAux->li_class = "w50p";
          $inputAux->required = $this->controller->is_required($ref);
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_fields++;   
                
          
            $this->array_form_campos = $array_form_campos;

            parent::monta_campos_form($id, $readonly, $clone, $resumido);   
        }
}

?>