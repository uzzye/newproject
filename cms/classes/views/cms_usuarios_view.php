<?php

require_once(ROOT_CMS . "classes/views/cms_grupos_usuarios_view.php");

class cms_usuarios_view extends view
{	  
    function __construct($owner = null)
	{
		$this->nome = "cms_usuarios";
		if(function_exists("get_lang")) {
			$this->nome_exibicao = get_lang("_USUARIOS");
			$this->nome_exibicao_singular = get_lang("_USUARIO");
		}
		
		parent::__construct($owner);	
    }	
	
	function monta_campos_form($id = "", $readonly = false, $clone = false, $resumido = false)
	{
		global $modulo;
		
		$b_editando = false;
		$id="";
		if(trim($_REQUEST["id"]) <> "")
		{
			$id = $_REQUEST["id"];
			$this->controller->set_var("id",$id);
			$this->controller->carrega_dados();
			$b_editando = true;
		}
		
		$array_form_campos = array();
		$array_form_refs = array();
		
		$count_radio = 0;
		$count_text = 0;
		$count_check = 0;
		$count_refs = 0;
		$count_combo = 0;
		
	// CAMPO NOME
		$ref = "nome";
		$inputAux = new Uzzye_TextField();
		$inputAux->label = get_lang("_NOME");
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->li_class = 'w50p';
		$inputAux->set_value($this->controller->get_var($ref));
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_text++;
		
	// CAMPO SETOR
		$ref = "setor";
		$inputAux = new Uzzye_TextField();
		$inputAux->label = get_lang("_SETOR");
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->li_class = 'w50pu';
		$inputAux->set_value($this->controller->get_var($ref));	
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_text++;
		
	// CAMPO EMAIL
		$ref = "email";
		$inputAux = new Uzzye_TextField();
		$inputAux->label = get_lang("_EMAIL");
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->li_class = 'w50p';
		$inputAux->set_value($this->controller->get_var($ref));
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;		
		$count_text++;
		
	// CAMPO SENHA
		$ref = "senha";
        $inputAux = new Uzzye_PasswordField();
		$inputAux->label = get_lang("_SENHA");
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->li_class = 'w50p';
		//$inputAux->set_value($this->controller->get_var($ref));
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;				
		$count_text++;
		
	// CAMPO GRUPO
		$ref = "id_grupo";
		$checked = $this->controller->get_var($ref);
		if(trim($_REQUEST[$ref]) <> "")
		{$checked = $_REQUEST[$ref];}
		
        $inputAux = new Uzzye_ComboBox();
		$inputAux->label = get_lang("_GRUPO_USUARIOS");
		$inputAux->size = 1;
		$inputAux->name = $ref;
		$inputAux->id = $ref . "_" . $count_combo;
		$inputAux->default_value = 0;
		$inputAux->default_display = get_lang("_COMBOSELECIONE");
		$inputAux->checked_value = $checked;
		$classAux = "cms_grupos_usuarios_controller";
		$objAux = new $classAux();
		$inputAux->set_options($objAux->get_array_options("id","titulo","","titulo ASC"));
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_combo++;
		
	// CAMPO DISPONÍVEL
		/*$ref = "disponivel";
		$checked = $this->controller->get_var($ref);
		if(trim($_REQUEST[$ref]) <> "")
		{$checked = $_REQUEST[$ref];}
		
        $inputAux = new Uzzye_RadioButton();
		$inputAux->label = get_lang("_DISPONIVEL");
		$inputAux->sub_label = get_lang("_SIM");
		$inputAux->name = $ref;
		$inputAux->id = $ref . "_" . $count_radio;		
		$inputAux->default_value = 0;
		$inputAux->checked_value = $checked;		
		$inputAux->set_value(1);
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_radio++;

        $inputAux = new Uzzye_RadioButton();
		$inputAux->label = get_lang("_DISPONIVEL");
		$inputAux->sub_label = get_lang("_NAO");
		$inputAux->name = $ref;
		$inputAux->id = $ref . "_" . $count_radio;		
		$inputAux->default_value = 0;
		$inputAux->checked_value = $checked;		
		$inputAux->set_value(0);
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_radio++;*/

		$ref = "disponivel";
		$checked = $this->controller->get_var($ref);
		if(trim($_REQUEST[$ref]) <> "")
		{$checked = $_REQUEST[$ref];}
		$inputAux = new Uzzye_RadioGroup();
		$inputAux->label = get_lang("_DISPONIVEL");
		$inputAux->set_options(array(
			array(0, get_lang("_NAO")),
			array(1, get_lang("_SIM"))
		));
		$inputAux->name = $ref;
		$inputAux->id = $ref;		
		$inputAux->default_value = 0;
		$inputAux->checked_value = $checked;		
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_radio++;

		$ref = "id_idioma";
		$checked = $this->controller->get_var($ref);
		if(trim($_REQUEST[$ref]) <> "") {
			$checked = $_REQUEST[$ref];
		} else if(intval($checked) == 0) {
			$checked = 1;
		}
		$inputAux = new Uzzye_ListBox($ref,$ref,get_lang("_IDIOMA"),0);
		$inputAux->default_display = get_lang("_COMBOSELECIONE");
		$inputAux->default_value = 0;
		$inputAux->checked_value = $checked;
		$inputAux->size = 1;
		$classAux = "idiomas_controller";
		$inputAux->reference_model = "idiomas";
		$objAux = new $classAux();
		$inputAux->set_options($objAux->get_array_options("id","titulo","id != 99","titulo ASC"));
		$inputAux->refresh_options(array("id","titulo","id != 99","titulo ASC"));
		$inputAux->set_value($checked);
		$inputAux->li_class = "w50p";
		$inputAux->required = true;
		$inputAux->multiple = false;
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_fields++;
		
		// CAMPO IMAGEM 		
		$ref = "crop_avatar";
		$inputAux = new Uzzye_ImageCrop();
		$inputAux->label = get_lang("_AVATAR") . " (PNG - " . get_lang("_TAMANHORECOMENDAVEL") . ": 1024px x 1024px)";
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->li_class = 'w100p';
		$inputAux->array_crop_fields = array(
			array("avatar",get_lang("_AVATAR"),$this->controller->get_var("avatar"),1024,1024)	
		);
		$inputAux->set_value($this->controller->get_var("avatar"));
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;		
		$count_text++;
		
	// CAMPO MODULOS
        $ref = "modulos";	
        $inputAux = new Uzzye_PermsTable();
		$inputAux->label = get_lang("_PERMISSOES");
		$inputAux->sub_label = get_lang("_HELPPERMISSOES");
		$inputAux->name = $ref;
		$inputAux->li_class = 'w100p';
		$inputAux->id = $ref . "_" . $count_combo;
		$inputAux->id_usuario = intval($id);
		$array_form_refs[sizeof($array_form_refs)] = $inputAux;
		$count_combo++;
		
	// FINALIZACAO
		$this->array_form_campos = $array_form_campos;	
		$this->array_form_refs = $array_form_refs;

		parent::monta_campos_form($id, $readonly, $clone, $resumido);
	}

	function monta_formulario_senha(){
		global $modulo;

		$id = $_SESSION["idLogin"];
				
		$b_editando = false;

		$this->heading();
		?>
		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view">
				<?php

		$this->controller->set_var("id",$id);
		$this->controller->carrega_dados();
		$b_editando = true;

		$this->titulo_cadastro = "<!-- <i class=\"icon-chart mr-10\"></i> -->" . get_lang("_ALTERACAO_SENHA") . " ";


		$this->topo_cadastro($id, $readonly, $resumido, null, ROOT_SERVER . ROOT . "ajax/cms_usuarios/senha");
		
		$ref = "senha_atual";
        $inputAux = new Uzzye_PasswordField();
		$inputAux->label = get_lang("_SENHA_ATUAL");
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->li_class = 'w66p';
		$inputAux->required = true;
		echo $inputAux->get_display_field();

		$ref = "nova_senha";
        $inputAux = new Uzzye_PasswordField();
		$inputAux->label = get_lang("_NOVA_SENHA");
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->li_class = 'w66p';
		$inputAux->required = true;
		echo $inputAux->get_display_field();

		$ref = "nova_senha_repetida";
        $inputAux = new Uzzye_PasswordField();
		$inputAux->label = get_lang("_NOVA_SENHA_REPETIDA");
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->li_class = 'w66p';
		$inputAux->required = true;
		echo $inputAux->get_display_field();

		$this->rodape_cadastro($id, $readonly, $resumido);
		
		?>
				</div>
			</div>
		</div>
		<?php
	}
}