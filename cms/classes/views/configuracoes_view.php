<?php    
include_once(ROOT_CMS . "classes/views/idiomas_view.php");
	
class configuracoes_view extends view
{	  
		function __construct($owner = null)
		{
			$this->nome = "configuracoes";
			if(function_exists("get_lang")) {
				$this->nome_exibicao = get_lang("_CONFIGURACOES");
				$this->nome_exibicao_singular = get_lang("_CONFIGURACAO");
			}
		
			parent::__construct($owner);

			$this->number_masks["valor_conversao_contato"] = array(2,",",".","R$ ");
		}	
	
		function monta_campos_form($id = "", $readonly = false, $clone = false, $resumido = false)
		{
			global $modulo;
		
			$b_editando = false;		
			if(trim($_REQUEST["id"]) <> "")
			{
				$id = $_REQUEST["id"];
				$this->controller->set_var("id",$id);
				$this->controller->carrega_dados();
				$b_editando = true;
			}
		
			$array_form_campos = array();
		
			$count_fields = 0;

			// SITE
	
			$inputAux = new Uzzye_HR();		
			$inputAux->label = "Configurações do Site";
			$inputAux->icon = "icon-globe";
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;			

				$ref = "titulo_padrao";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_TITULO_PADRAO"));
			$inputAux->set_value($this->controller->get_var($ref));
			//$inputAux->label = get_lang("_SITE");
			//$inputAux->sub_label = get_lang("_TITULO_PADRAO");
			$inputAux->label = get_lang("_TITULO_PADRAO");
			$inputAux->li_class = "w100p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;	
				
				$ref = "descricao_padrao";
			$inputAux = new Uzzye_TextArea($ref,$ref,get_lang("_DESCRICAO_PADRAO"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;	
				
				$ref = "palavras_chave_padrao";
			$inputAux = new Uzzye_TextArea($ref,$ref,get_lang("_PALAVRASCHAVE_PADRAO")); 
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;

			$ref = "codigo_css";
			$inputAux = new Uzzye_TextArea($ref,$ref,get_lang("_CODIGO_CSS"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;

			$ref = "codigo_javascript";
			//$varAux = $this->controller->get_var($ref) . $this->controller->get_var("codigo_analytics");
			$varAux = $this->controller->get_var($ref);
			$inputAux = new Uzzye_TextArea($ref,$ref,get_lang("_CODIGO_JAVASCRIPT"));
			$inputAux->set_value($varAux);
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;

			$ref = "codigo_analytics";
			$inputAux = new Uzzye_TextArea($ref,$ref,get_lang("_CODIGO_ANALYTICS"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w66p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;

			$ref = "geolocalizacao";
			$inputAux = new Uzzye_TextArea($ref,$ref,get_lang("_GEOLOCALIZACAO_METAS"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;

			$ref = "mapa_embed";
			$inputAux = new Uzzye_TextArea($ref,$ref,get_lang("_MAPA_EMBED"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;

				$ref = "ga_user";
			$inputAux = new Uzzye_TextField($ref,$ref);
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->label = get_lang("_GA_USER");
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;

				$ref = "ga_props";
			$inputAux = new Uzzye_TextField($ref,$ref);
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->label = get_lang("_GA_PROPS");
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;

				$ref = "valor_conversao_contato";
			$inputAux = new Uzzye_NumberField();
			$inputAux->set_mask($this->number_masks[$ref]);
			$inputAux->label = get_lang("_VALOR_CONVERSAO_CONTATO");
			$inputAux->li_class = "w50p";
			$inputAux->name = $ref;
			$inputAux->id = $ref;
			$inputAux->set_value($this->controller->get_var($ref));
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_text++;	

			// EMAIL

			$inputAux = new Uzzye_HR();		
			$inputAux->label = "Configurações de Email";
			$inputAux->icon = "icon-envelope";
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;

			$ref = "email_padrao_contato";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_EMAIL_PADRAO_P_CONTATO"));
			$inputAux->label = get_lang("_EMAIL");
			//$inputAux->sub_label = get_lang("_EMAIL_PADRAO_P_CONTATO");
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;	
				
				$ref = "email_default_from";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_EMAIL_REMETENTE_PADRAO"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;

			$ref = "nome_default_from";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_NOME_REMETENTE_PADRAO"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w100p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;	

			$ref = "servidor_smtp";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_SERVIDOR_SMTP"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;

			$ref = "porta_smtp";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_PORTA_SMTP"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w25p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;

			$ref = "secure_smtp";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_SECURE_SMTP"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w25p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;

			$ref = "servidor_pop";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_SERVIDOR_POP"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;

			$ref = "porta_pop";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_PORTA_POP"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w25p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;

			$ref = "secure_pop";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_SECURE_POP"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w25p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;

			$ref = "usuario_email";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_USUARIO_EMAIL"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;

			$ref = "senha_email";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_SENHA_EMAIL"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;

			// OUTROS

			$inputAux = new Uzzye_HR();		
			$inputAux->label = "Outras Configurações";
			$inputAux->icon = "icon-pin";
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
				
				/*$ref = "url_amigaveis";
			$checked = $this->controller->get_var($ref);
			if(trim($_REQUEST[$ref]) <> "")
			{$checked = $_REQUEST[$ref];}		
        	$inputAux = new Uzzye_CheckBox();
			$inputAux->label = get_lang("_OUTRAS_CONFIGURACOES");
			$inputAux->sub_label = get_lang("_URL_AMIGAVEL");
			$inputAux->name = $ref;
			$inputAux->id = $ref . "_" . $count_radio;		
			$inputAux->default_value = 0;
			$inputAux->checked_value = $checked;		
			$inputAux->set_value(1);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_radio++;*/	
				
				$ref = "charset_padrao";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_CHARSET_PADRAO"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w66p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;	
				
				/*$ref = "qtd_itens_pp";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_QUANTIDADE_DE_ITENS_POR_PAGINA"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w33p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;	
				
				$ref = "qtd_group_pag";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_QUANTIDADE_AGRUPADA_NA_PAGINACAO"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w33p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;*/	
				
				$ref = "id_idioma";
			$checked = $this->controller->get_var($ref);
			if(trim($_REQUEST[$ref]) <> "") {
				$checked = $_REQUEST[$ref];
			} else if(intval($checked) == 0) {
				$checked = 99;
			}
			$inputAux = new Uzzye_ListBox($ref,$ref,get_lang("_IDIOMA"),0);
			$inputAux->default_display = get_lang("_COMBOSELECIONE");
			$inputAux->default_value = 0;
			$inputAux->checked_value = $checked;
			$inputAux->size = 1;
			$classAux = "idiomas_controller";
			$inputAux->reference_model = "idiomas";
			$objAux = new $classAux();
			$inputAux->set_options($objAux->get_array_options("id","titulo","","titulo ASC"));
			$inputAux->refresh_options(array("id","titulo","","titulo ASC"));
			$inputAux->set_value($checked);
			$inputAux->li_class = "w66p";
			$inputAux->required = true;
			$inputAux->multiple = false;
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;
			
			$ref = "idioma_padrao";
			$checked = $this->controller->get_var($ref);
			if(trim($_REQUEST[$ref]) <> "")
			{
				$checked = $_REQUEST[$ref];
			}
			
			$inputAux = new Uzzye_CheckBox();
			$inputAux->label = get_lang("_IDIOMA_PADRAO");
			$inputAux->sub_label = get_lang("_SIM");
			$inputAux->name = $ref;
			$inputAux->id = $ref . "_" . $count_radio;
			$inputAux->default_value = 0;
			$inputAux->checked_value = $checked;
			$inputAux->set_value(1);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_radio++;
				
			
			$this->array_form_campos = $array_form_campos;	
		}
}

?>