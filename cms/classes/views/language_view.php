<?php    
	
require_once(ROOT_CMS . "classes/views/idiomas_view.php");

class language_view extends view
{	  
		function __construct($owner = null)
		{
			$this->nome = "language";
			if(function_exists("get_lang")) {
				$this->nome_exibicao = get_lang("_LANGUAGE");
				$this->nome_exibicao_singular = get_lang("_LANGUAGE");
			}
		
			parent::__construct($owner);		
		}	
	
		function monta_campos_form($id = "", $readonly = false, $clone = false, $resumido = false)
		{
			global $modulo;
		
			$b_editando = false;		
			if(trim($_REQUEST["id"]) <> "")
			{
				$id = $_REQUEST["id"];
				$this->controller->set_var("id",$id);
				$this->controller->carrega_dados();
				$b_editando = true;
			}
		
			$array_form_campos = array();
		
			$count_fields = 0;
					
			$ref = "modulo_registro";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_MODULO_REGISTRO"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w66p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;	
					
			$ref = "id_registro";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_ID_REGISTRO"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w66p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;
				
				$ref = "variavel";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_VARIAVEL"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w66p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;

			$ref = "id_idioma";
			$inputAux = new Uzzye_ListBox($ref,$ref,get_lang("_IDIOMA"),0);
			$inputAux->default_value = 0;
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->multiple = false;
			$inputAux->set_reference_item("idiomas");
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;
				
				$ref = "valor";
			$inputAux = new Uzzye_WYSIHTML5($ref,$ref,get_lang("_VALOR"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w66p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;				
			
			$this->array_form_campos = $array_form_campos;	
		}
}

?>