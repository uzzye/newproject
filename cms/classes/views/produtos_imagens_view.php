<?php    
include_once(ROOT_CMS . "classes/views/produtos_view.php");
include_once(ROOT_CMS . "classes/produtos.php");
   
class produtos_imagens_view extends view
{     
        function __construct($owner = null)
        {
            global $modulo;

            $this->nome = "produtos_imagens";
            if(function_exists("get_lang")) {
              $this->nome_exibicao = get_lang("_IMAGENS_DE_PRODUTOS");
              $this->nome_exibicao_singular = get_lang("_IMAGEM_DE_PRODUTO");
            }
        
            parent::__construct($owner);          

            $this->custom_expr_masks["imagem"] = "return(\"<img src='\" . \$this->get_upload_folder(\"imagem\") . \$valor . \"' border='0' width='200' />\");";  
        }   
    
        function monta_campos_form($id = "", $readonly = false, $clone = false, $resumido = false)
        {
            global $modulo;
        
            $b_editando = false;       
            if(trim($_REQUEST["id"]) <> "")
            {
                $id = $_REQUEST["id"];
                $this->controller->set_var("id",$id);
                $this->controller->carrega_dados();
                $b_editando = true;
            }
        
            $array_form_campos = array();
        
            $count_fields = 0;
                    
          $ref = "id_produto";
          $checked = $this->controller->get_var($ref);
            if(trim($_REQUEST[$ref]) <> "")
            {$checked = $_REQUEST[$ref];}
            $inputAux = new Uzzye_ListBox($ref,$ref,get_lang("_PRODUTO"));
            $inputAux->default_display = get_lang("_COMBOSELECIONE");
            $inputAux->default_value = 0;
            $inputAux->checked_value = $checked;
            $inputAux->size = 1;
            $inputAux->multiple = false;
              $inputAux->set_reference_item($this->controller->get_reference_model($ref));          $inputAux->set_value($this->controller->get_var($ref));
            $inputAux->li_class = "w50p";
            $inputAux->required = $this->controller->is_required($ref);
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_fields++;   
                
              $ref = "titulo";
          $inputAux = new Uzzye_TextField($ref,$ref,get_lang("_TITULO"));
          $inputAux->set_value($this->controller->get_var($ref));
          $inputAux->li_class = "w50p";
          $inputAux->show_in_data_table = true;
          $inputAux->required = $this->controller->is_required($ref);
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_fields++;   

              $ref = "imagem";
          $inputAux = new Uzzye_ImageCrop("crop_" . $ref,"crop_" . $ref,get_lang("_IMAGEM"));
          $inputAux->li_class = "w50p";
            $inputAux->clone = $clone;
            $inputAux->label = get_lang("_IMAGEM") . " (" . get_lang("_TAMANHORECOMENDAVEL") . " 720px x 100%)";
            $inputAux->crops = array(
                array("imagem",get_lang("_IMAGEM"),0,720,0)  
            );
            $inputAux->set_value($b_editando);
            $inputAux->required = $this->controller->is_required($ref);
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_fields++;    
                
          
            $this->array_form_campos = $array_form_campos;

            parent::monta_campos_form($id, $readonly, $clone, $resumido);   
        }
}

?>