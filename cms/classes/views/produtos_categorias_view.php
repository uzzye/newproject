<?php    
   
class produtos_categorias_view extends view
{     
        function __construct($owner = null)
        {
            global $modulo;

            $this->nome = "produtos_categorias";
            if(function_exists("get_lang")) {
              $this->nome_exibicao = get_lang("_CATEGORIAS");
              $this->nome_exibicao_singular = get_lang("_CATEGORIA");
            }
        
            parent::__construct($owner);       
        }   
    
        function monta_campos_form($id = "", $readonly = false, $clone = false, $resumido = false)
        {
            global $modulo;
        
            $b_editando = false;       
            if(trim($_REQUEST["id"]) <> "")
            {
                $id = $_REQUEST["id"];
                $this->controller->set_var("id",$id);
                $this->controller->carrega_dados();
                $b_editando = true;
            }
        
            $array_form_campos = array();
        
            $count_fields = 0;
                    
          $ref = "titulo_pt";
          $inputAux = new Uzzye_TextField($ref,$ref,get_lang("_TITULO_PT"));
          $inputAux->set_value($this->controller->get_var($ref));
          $inputAux->li_class = "w50p";
          $inputAux->required = $this->controller->is_required($ref);
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_fields++; 
                    
          $ref = "titulo_en";
          $inputAux = new Uzzye_TextField($ref,$ref,get_lang("_TITULO_EN"));
          $inputAux->set_value($this->controller->get_var($ref));
          $inputAux->li_class = "w50p";
          $inputAux->required = $this->controller->is_required($ref);
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_fields++; 
                    
          $ref = "titulo_es";
          $inputAux = new Uzzye_TextField($ref,$ref,get_lang("_TITULO_ES"));
          $inputAux->set_value($this->controller->get_var($ref));
          $inputAux->li_class = "w50p";
          $inputAux->required = $this->controller->is_required($ref);
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_fields++;   
                
          
            $this->array_form_campos = $array_form_campos;

            parent::monta_campos_form($id, $readonly, $clone, $resumido);   
        }
}

?>