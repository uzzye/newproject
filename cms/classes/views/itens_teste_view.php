<?php

include_once(ROOT_CMS . "classes/teste.php");

class itens_teste_view extends view
{	  
    function __construct($owner = null)
	{
		$this->nome = "itens_teste";
		if(function_exists("get_lang")) {
			$this->nome_exibicao = get_lang("_ITENS_TESTE");
			$this->nome_exibicao_singular = get_lang("_ITEM_TESTE");
		}
		
		parent::__construct($owner);		

		$this->default_view_field = "label";
    }
	
	function monta_campos_form($id = "", $readonly = false, $clone = false, $resumido = false)
	{
		global $modulo;
		
		$b_editando = false;		
		if(trim($_REQUEST["id"]) <> "")
		{
			$id = $_REQUEST["id"];
			$this->controller->set_var("id",$id);
			$this->controller->carrega_dados();
			$b_editando = true;
		}
		
		$array_form_campos = array();

		$ref = "id_teste";
		$checked = $this->controller->get_var($ref);
		if(trim($_REQUEST[$ref]) <> "") {
			$checked = $_REQUEST[$ref];
		}
		$inputAux = new Uzzye_ListBox($ref,$ref,get_lang("_TESTE"),0);
		$inputAux->default_value = 0;
		$inputAux->set_value($checked);
		$inputAux->multiple = false;
		$inputAux->set_reference_item($this->controller->get_reference_model($ref));
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_fields++;
		
		// CAMPO TITULO
		$ref = "label";
		//$inputAux = new Uzzye_NumberField();
		$inputAux = new Uzzye_TextField();
		$inputAux->label = get_lang("_LABEL");
		$inputAux->show_in_data_table = true;
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->set_value($this->controller->get_var($ref));
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_text++;

		$ref = "imagem_crop";
		$inputAux = new Uzzye_ImageCrop();		
		$inputAux->label = get_lang("_IMAGEM");
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->crops = array(
			array("imagem",get_lang("_IMAGEM"))
		);
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_textarea++;
		
		$this->array_form_campos = $array_form_campos;	
	}
}