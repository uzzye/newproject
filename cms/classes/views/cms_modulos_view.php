<?php

class cms_modulos_view extends view
{	  
    function __construct($owner = null)
	{
		$this->nome = "cms_modulos";
		if(function_exists("get_lang")) {
			$this->nome_exibicao = get_lang("_MODULOS");
			$this->nome_exibicao_singular = get_lang("_MODULO");
		}
		
		parent::__construct($owner);	

		$this->default_value_field = "id";
		$this->default_view_field = "nome";
    }
	
	function monta_campos_form($id = "", $readonly = false, $clone = false, $resumido = false)
	{
		global $modulo;
		
		$b_editando = false;		
		if(trim($_REQUEST["id"]) <> "")
		{
			$id = $_REQUEST["id"];
			$this->controller->set_var("id",$id);
			$this->controller->carrega_dados();
			$b_editando = true;
		}
		
		$array_form_campos = array();
		
		$count_radio = 0;
		$count_text = 0;
		$count_check = 0;
		$count_combo = 0;
		
		$ref = "nome";
		$inputAux = new Uzzye_TextField();
		$inputAux->label = get_lang("_NOME");
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->li_class = "w50p";
		$inputAux->set_value($this->controller->get_var($ref));
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_text++;
		
		$ref = "nome_en";
		$inputAux = new Uzzye_TextField();
		$inputAux->label = get_lang("_NOME_EN");
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->li_class = "w50p";
		$inputAux->set_value($this->controller->get_var($ref));
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_text++;
		
		$ref = "link";
		$inputAux = new Uzzye_TextField();
		$inputAux->label = get_lang("_LINK");
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->li_class = "w50p";
		$inputAux->set_value($this->controller->get_var($ref));	
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_text++;
		
		$ref = "cor";
		$inputAux = new Uzzye_ColorField();
		$inputAux->label = get_lang("_COR");
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->li_class = "w50p";
		$inputAux->set_value($this->controller->get_var($ref));
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_text++;
		
		$ref = "icon_class";
		$inputAux = new Uzzye_TextField();
		$inputAux->label = get_lang("_ICON_CLASS");
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->li_class = "w50p";
		$inputAux->set_value($this->controller->get_var($ref));
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_text++;
		
		$ref = "modulo_pai";
		$checked = $this->controller->get_var($ref);
		if(trim($_REQUEST[$ref]) <> "")
		{$checked = $_REQUEST[$ref];}
		
        $inputAux = new Uzzye_ListBox();
        $inputAux->reference_model = "modulos";
		$inputAux->label = get_lang("_MODULOPAI");
		$inputAux->size = 1;
		$inputAux->li_class = "w50p";
		$inputAux->multiple = false;
		$inputAux->name = $ref;
		$inputAux->id = $ref . "_" . $count_combo;
		$inputAux->default_value = 0;
		$inputAux->default_display = get_lang("_COMBOSELECIONE");
		$inputAux->checked_value = $checked;
		$inputAux->set_options($this->controller->get_array_options("id","nome","","modulo_pai ASC, nome ASC"));
		$inputAux->refresh_options = array("id","nome","","modulo_pai ASC, nome ASC");
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_combo++;
		
		$this->array_form_campos = $array_form_campos;	
	}
}

?>