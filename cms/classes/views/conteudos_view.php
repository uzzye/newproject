<?php

include_once(ROOT_CMS . "classes/idiomas.php");

class conteudos_view extends view
{	  
    function __construct($owner = null)
	{
		$this->nome = "conteudos";
		if(function_exists("get_lang")) {
			$this->nome_exibicao = get_lang("_CONTEUDOS");
			$this->nome_exibicao_singular = get_lang("_CONTEUDO");
		}
		
		parent::__construct($owner);		
    }
	
	function monta_campos_form($id = "", $readonly = false, $clone = false, $resumido = false)
	{
		global $modulo;
		
		$b_editando = false;		
		if(trim($_REQUEST["id"]) <> "")
		{
			$id = $_REQUEST["id"];
			$this->controller->set_var("id",$id);
			$this->controller->carrega_dados();
			$b_editando = true;
		}
		
		$array_form_campos = array();
		
		// CAMPO NOME DO ARQUIVO
		$ref = "nome_arquivo";
		$inputAux = new Uzzye_TextField();
		$inputAux->label = get_lang("_NOMEARQUIVOPHP");
		$inputAux->max_length = 255;
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->li_class = 'w66p';
		$inputAux->set_value($this->controller->get_var($ref));
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_text++;		
		
		// CAMPO TITULO
		$ref = "titulo";
		//$inputAux = new Uzzye_NumberField();
		$inputAux = new Uzzye_TextField();
		$inputAux->label = get_lang("_TITULO");
		$inputAux->hint = "Título da página, exibido em destaque e letras maiores.";
		$inputAux->max_length = 255;
		/*$inputAux->max_length = 12;
		$inputAux->decimals = 2;*/
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->li_class = 'w66p';
		$inputAux->set_value($this->controller->get_var($ref));
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_text++;
		
	// CAMPO SUBTITULO
		/*$ref = "subtitulo";
		$inputAux = new Uzzye_TextField();
		$inputAux->label = get_lang("_SUBTITULO");
		$inputAux->max_length = 255;
		$inputAux->name = $ref;
		$inputAux->li_class = 'w66p';
		$inputAux->id = $ref;
		$inputAux->set_value($this->controller->get_var($ref));
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_text++;*/			
		
	// CAMPO TEXTO
		$ref = "texto";
		$inputAux = new Uzzye_CKEditorField();		
		$inputAux->label = get_lang("_TEXTO");
		$inputAux->hint = "Conteúdo descritivo da página. Pode ser utilizado texto puro e imagens ou vídeos incorporados.";
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->li_class = 'w100p';
		$inputAux->set_value($this->controller->get_var($ref));
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_textarea++;	
		
	// CAMPO TEXTO CAPA
		/*$ref = "texto_capa";
		$inputAux = new Uzzye_CKEditorField();		
		$inputAux->label = get_lang("_TEXTO_CAPA");
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->li_class = 'w100p';
		$inputAux->set_value($this->controller->get_var($ref));
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_textarea++;	*/
		
		// CAMPO IMAGEM 		
		/*$ref = "crop_banner_topo";
		$inputAux = new Uzzye_ImageCrop();
		$inputAux->label = get_lang("_IMAGEM") . " (" . get_lang("_TAMANHORECOMENDAVEL") . ": 560x185)";	
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->li_class = 'w100p';
		$inputAux->array_crop_fields = array(
												array("banner_topo",get_lang("_IMAGEM"),$this->controller->get_var("banner_topo"),560,185)	
											);
		$inputAux->set_value($b_editando);
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;		
		$count_text++;*/
		
		// CAMPO META DESCRICAO
		$ref = "meta_descricao";
		$inputAux = new Uzzye_TextArea();		
		$inputAux->label = get_lang("_META_DESCRICAO");
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->li_class = 'w50p';
		$inputAux->set_value($this->controller->get_var($ref));
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_textarea++;
		
		// CAMPO META PALAVRAS-CHAVE
		$ref = "meta_palavras_chave";
		$inputAux = new Uzzye_TextArea();		
		$inputAux->label = get_lang("_META_PALAVRAS");
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->li_class = 'w50p';
		$inputAux->set_value($this->controller->get_var($ref));
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_textarea++;
		
		$ref = "id_idioma";
		$checked = $this->controller->get_var($ref);
		if(trim($_REQUEST[$ref]) <> "") {
			$checked = $_REQUEST[$ref];
		} else if(intval($checked) == 0) {
			$checked = 99;
		}
		$inputAux = new Uzzye_ListBox($ref,$ref,get_lang("_IDIOMA"),0);
		$inputAux->default_display = get_lang("_COMBOSELECIONE");
		$inputAux->default_value = 0;
		$inputAux->checked_value = $checked;
		$inputAux->size = 1;
		$classAux = "idiomas_controller";
		$inputAux->reference_model = "idiomas";
		$objAux = new $classAux();
		$inputAux->set_options($objAux->get_array_options("id","titulo","","titulo ASC"));
		$inputAux->refresh_options(array("id","titulo","","titulo ASC"));
		$inputAux->set_value($checked);
		$inputAux->li_class = "w50p";
		$inputAux->required = true;
		$inputAux->multiple = false;
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_fields++;
		
		$this->array_form_campos = $array_form_campos;	
	}
}