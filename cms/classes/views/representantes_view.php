<?php    
include_once(ROOT_CMS . "classes/views/estados_view.php");
include_once(ROOT_CMS . "classes/estados.php");
   
class representantes_view extends view
{     
        function __construct($owner = null)
        {
            global $modulo;

            $this->nome = "representantes";
            if(function_exists("get_lang")) {
              $this->nome_exibicao = get_lang("_REPRESENTANTES");
              $this->nome_exibicao_singular = get_lang("_REPRESENTANTE");
            }
        
            parent::__construct($owner);       
        }   
    
        function monta_campos_form($id = "", $readonly = false, $clone = false, $resumido = false)
        {
            global $modulo;

            ?>
            <script language="javascript">
              $(document).ready(function(e){
                $("#endereco").on("change").change(function(e){
                  e.stopPropagation();
                  $("#geolocalizacao_search").val($(this).val()).trigger("blur");
                });
              });
            </script>
            <?php
        
            $b_editando = false;       
            if(trim($_REQUEST["id"]) <> "")
            {
                $id = $_REQUEST["id"];
                $this->controller->set_var("id",$id);
                $this->controller->carrega_dados();
                $b_editando = true;
            }
        
            $array_form_campos = array();
        
            $count_fields = 0;
                    
          $ref = "estados";
          $checked = $this->controller->get_var($ref);
            if(trim($_REQUEST[$ref]) <> "")
            {$checked = $_REQUEST[$ref];}
            $inputAux = new Uzzye_ListBox($ref,$ref,get_lang("_ESTADOS"));
            $inputAux->default_display = get_lang("_INTERNACIONAL");
            $inputAux->default_value = 0;
            $inputAux->checked_value = $checked;
            $inputAux->size = 1;
            $inputAux->multiple = true;
              $inputAux->set_reference_item($this->controller->get_reference_model($ref));          $inputAux->set_value($this->controller->get_var($ref));
            $inputAux->li_class = "w50p";
            $inputAux->required = $this->controller->is_required($ref);
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_fields++;   
                
              $ref = "titulo";
          $inputAux = new Uzzye_TextField($ref,$ref,get_lang("_TITULO"));
          $inputAux->set_value($this->controller->get_var($ref));
          $inputAux->li_class = "w50p";
          $inputAux->required = $this->controller->is_required($ref);
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_fields++;   
                
              $ref = "endereco";
          $inputAux = new Uzzye_TextArea($ref,$ref,get_lang("_ENDERECO"));
          $inputAux->set_value($this->controller->get_var($ref));
          $inputAux->li_class = "w50p";
          $inputAux->required = $this->controller->is_required($ref);
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_fields++;   
                
              $ref = "email";
          $inputAux = new Uzzye_TextArea($ref,$ref,get_lang("_EMAIL"));
          $inputAux->set_value($this->controller->get_var($ref));
          $inputAux->li_class = "w50p";
          $inputAux->required = $this->controller->is_required($ref);
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_fields++;   
                
              $ref = "telefone";
          $inputAux = new Uzzye_TextArea($ref,$ref,get_lang("_TELEFONE"));
          $inputAux->set_value($this->controller->get_var($ref));
          $inputAux->li_class = "w50p";
          $inputAux->required = $this->controller->is_required($ref);
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_fields++;   
                
              $ref = "geolocalizacao";
          $inputAux = new Uzzye_GeolocationField($ref,$ref,get_lang("_GEOLOCALIZACAO"));
          $inputAux->set_value($this->controller->get_var($ref));
            $inputAux->li_class = "w100p";
            $inputAux->required = $this->controller->is_required($ref);
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_geolocation++; 
                
          
            $this->array_form_campos = $array_form_campos;

            parent::monta_campos_form($id, $readonly, $clone, $resumido);   
        }
}

?>