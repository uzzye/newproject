<?php 

class relatorios_view{
	public $nome_exibicao;
	public $nome_exibicao_singular;
	
	public $owner;
	public $controller;	
    
    public $help_text;
  
    function __construct($owner = null){ 
    	global $modulo;
    	
        $this->nome = "relatorios";
        if(function_exists("get_lang")) {
        	$this->nome_exibicao = get_lang("_RELATORIOS");
        	$this->nome_exibicao_singular = get_lang("_RELATORIO");
        }
    	
		$this->owner = $owner;
    	$this->controller = $this->owner;
   	}
    
	function set_owner($owner){ 
    	$this->owner = $owner;
    	$this->controller = $this->owner;
    }
    
    function apresenta($nome_relatorio = "")
    {
    	?>
    	<h2><?php echo ($this->nome_exibicao_singular); ?></h2>
        <h4><?php echo get_lang("_RELATORIO"); ?></h4>
        <form action="<?php echo ROOT_CMS; ?>relatorio.php?modulo=relatorios&acao=processa" target="_blank" method="POST">
            <input type='hidden' name='relatorio' value='relatorio' />
            <ul class='formulario'>
                <?php

                $ref = "mes";
                $inputAux = new Uzzye_TextField($ref,$ref,get_lang("_MES") . " (" . get_lang("_HELPMES") . ")");
                $inputAux->li_class = "w33p";

                echo $inputAux->get_display_field();
                    
                $ref = "ano";
                $inputAux = new Uzzye_TextField($ref,$ref,get_lang("_ANO") . " (" . get_lang("_HELPANO") . ")");
                $inputAux->li_class = "w33p";

                echo $inputAux->get_display_field();
                
                ?>
            </ul>    
            <div class="inserir">       
                <input type="submit" id='botao_submit' class="btnPrimario corBase" value="<?php echo get_lang("_PROCESSAR");?>"/>
            </div>
        </form> 
    	<?php
    }

    function processa()
    {
        ?>
        <script type="text/javascript">

            function doPrint()
            {
                $(".noprint").css("visibility","hidden");
                window.print();
                $(".noprint").css("visibility","visible");
            }

        </script>
        <?php
        ?><div class="relatorio" id="relatorio"><?php
        $relatorio = $_REQUEST["relatorio"];
        
        if($relatorio == "relatorio")
        {
            if(intval($_REQUEST["mes"]) == 0){
                ?><h4>Selecione um Mês!</h4><?php
            } else {
                $sWhere = "";
                            
                if(intval($_REQUEST["mes"]) == 0) 
                {
                    $_REQUEST["mes"] = Date("m");
                }

                if(intval($_REQUEST["ano"]) == 0) 
                {
                    $_REQUEST["ano"] = Date("Y");
                }
                
                if(trim($sWhere) <> ""){
                    $sWhere .= " AND ";
                }
                $sWhere .= " MONTH(data) = " . intval($_REQUEST["mes"]) . " ";

                if(trim($sWhere) <> ""){
                    $sWhere .= " AND ";
                }
                $sWhere .= " YEAR(data) = " . intval($_REQUEST["ano"]) . " ";            

                ?>
                <h2>Nome do Relatório</h2>
                <hr/>
                
                <?php 
                $bFiltros = false;
                ?>

                <?php if(intval($_REQUEST["mes"]) > 0){?>
                <h3>Mês: <?php echo ($_REQUEST["mes"]); ?></h3>
                <?php $bFiltros = true; } ?></h3>

                <?php if(intval($_REQUEST["ano"]) > 0){?>
                <h3>Ano: <?php echo ($_REQUEST["ano"]); ?></h3>
                <?php $bFiltros = true; } ?></h3>

                <?php
                if($bFiltros){
                    ?><hr/><?php
                }

                /*$its = new lancamentos();
                $its = $its->get_array_ativos($sWhere,"YEAR(data) DESC, MONTH(data) DESC, data ASC, data_criacao ASC");*/

                if(is_array($its) && sizeof($its) > 0){
                    ?><table class="listagem">
                        <thead class="corBase tp20w" valign='top'>
                            <td class="">
                                Data
                            </td>
                        </thead>
                        <tbody>
                <?php } else {
                    ?><h4>Nenhum registro encontrado!</h4><?php
                }

                $iCount = 0;

                foreach ($its as $it)
                {
                    if($it != null)
                    {

                        //$valorTotal = 0;

                        $dataAux = stripslashes(get_output($it->get_var("data")));
                        $mesAux = substr($dataAux,5,2);
                        $anoAux = substr($dataAux,0,4);

                        ?>

                        <tr valign='top'>
                            <td>
                                <?php echo encode_data($dataAux, false); ?>
                            </td>
                        </tr>

                        <?php 

                        $iCount++;
                    }
                } 
                
                if(is_array($its) && sizeof($its) > 0){
                    ?>
                        </tbody>
                        <tfoot class="corBase tp20w" valign='top'>
                            <td class="cDir">
                            </td>
                        </tfoot>
                    </table>
                    <hr/>
                    <?php
                }
            }
        }
        ?>
        </div>
        <div class="imprimir noprint">
            <a href="javascript:doPrint();">Imprimir</a>
        </div><?php
    }
}
    
?>