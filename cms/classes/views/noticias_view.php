<?php    
include_once(ROOT_CMS . "classes/noticias_tags.php");
   
class noticias_view extends view
{     
        function __construct($owner = null)
        {
            global $modulo;

            $this->nome = "noticias";
            if(function_exists("get_lang")) {
              $this->nome_exibicao = get_lang("_NOTICIAS");
              $this->nome_exibicao_singular = get_lang("_NOTICIA");
            }
        
            parent::__construct($owner);          

            $this->custom_expr_masks["imagem"] = "return(\"<img src='\" . \$this->get_upload_folder(\"imagem\") . \$valor . \"' border='0' width='200' />\");";   
			
            $this->langs = array("pt","en");
        }   
    
        function monta_campos_form($id = "", $readonly = false, $clone = false, $resumido = false)
        {
            global $modulo;
        
            $b_editando = false;       
            if(trim($_REQUEST["id"]) <> "")
            {
                $id = $_REQUEST["id"];
                $this->controller->set_var("id",$id);
                $this->controller->carrega_dados();
                $b_editando = true;
            }
        
            $array_form_campos = array();
        
            $count_fields = 0;

            // Lang Tab Start
        
            $inputAux = new Uzzye_TabView("lang",get_lang("_CAMPOS_DE_IDIOMAS"),"fa fa-language");
            foreach($this->langs as $langAux) {
              $inputAux->addTab(strtoupper($langAux), "", ".lang-" . $langAux);
            }
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_fields++;
        
            foreach($this->langs as $langAux) {
              $classAux = "lang-item lang-" . $langAux . " tab-item"; if($langAux == $this->langs[0]) { $classAux .= " active";}
        
              $ref = "titulo_" . $langAux;
              $inputAux = new Uzzye_TextField($ref,$ref,get_lang("_TITULO_" . strtoupper($langAux)));
              $inputAux->set_value($this->controller->get_var($ref));
              $inputAux->li_class = "w50p " . $classAux;
              $inputAux->required = $this->controller->is_required($ref);
              $array_form_campos[sizeof($array_form_campos)] = $inputAux;
              $count_fields++;

              $ref = "descricao_" . $langAux;
              $inputAux = new Uzzye_CKEditorField($ref,$ref,get_lang("_DESCRICAO_" . strtoupper($langAux)));
              $inputAux->set_value($this->controller->get_var($ref));
              $inputAux->li_class = "w100p " . $classAux;
              //$inputAux->required = $this->controller->is_required($ref);
              $inputAux->required = false;
              $array_form_campos[sizeof($array_form_campos)] = $inputAux;
              $count_fields++;
            }
        
            // Lang Tab End
        
            // HR
            $inputAux = new Uzzye_HR();		
            $inputAux->label = get_lang("_OUTROS_CAMPOS");
            $inputAux->icon = "fa fa-align-left";
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_fields++;      
                
              $ref = "imagem";
          $inputAux = new Uzzye_ImageCrop("crop_" . $ref,"crop_" . $ref,get_lang("_IMAGEM"));
          $inputAux->li_class = "w50p";
            $inputAux->clone = $clone;
            $inputAux->label = get_lang("_IMAGEM") . " (JPG - " . get_lang("_TAMANHORECOMENDAVEL") . " 1080px x 720px - 80%)";
            $inputAux->crops = array(
                array("imagem",get_lang("_IMAGEM"),0,1080,720,80)  
            );
            $inputAux->set_value($b_editando);
            $inputAux->required = $this->controller->is_required($ref);
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_fields++;         
                
            $ref = "tags";
        $checked = $this->controller->get_var($ref);
          if(trim($_REQUEST[$ref]) <> "")
          {$checked = $_REQUEST[$ref];}
          $inputAux = new Uzzye_ListBox($ref,$ref,get_lang("_TAGS"));
          $inputAux->default_display = get_lang("_COMBOSELECIONE");
          $inputAux->default_value = 0;
          $inputAux->checked_value = $checked;
          $inputAux->size = 1;
          $inputAux->multiple = true;
          $inputAux->set_reference_item("noticias_tags");
              $inputAux->set_value($checked);
          $inputAux->li_class = "w50p";
          $inputAux->required = $this->controller->is_required($ref);
          $array_form_campos[sizeof($array_form_campos)] = $inputAux;
          $count_fields++;    
                
          
            $this->array_form_campos = $array_form_campos;

            parent::monta_campos_form($id, $readonly, $clone, $resumido);   
        }
}

?>