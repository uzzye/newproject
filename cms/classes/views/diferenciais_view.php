<?php    
   
class diferenciais_view extends view
{     
        function __construct($owner = null)
        {
            global $modulo;

            $this->nome = "diferenciais";
            if(function_exists("get_lang")) {
              $this->nome_exibicao = get_lang("_DIFERENCIAIS");
              $this->nome_exibicao_singular = get_lang("_DIFERENCIAL");
            }
        
            parent::__construct($owner);   

            $this->custom_expr_masks["icone"] = "return(\"<img src='\" . \$this->get_upload_folder(\"icone\") . \$valor . \"' border='0' width='96' />\");"; 
			
            $this->langs = array("pt","en");        
        }   
    
        function monta_campos_form($id = "", $readonly = false, $clone = false, $resumido = false)
        {
            global $modulo;
        
            $b_editando = false;       
            if(trim($_REQUEST["id"]) <> "")
            {
                $id = $_REQUEST["id"];
                $this->controller->set_var("id",$id);
                $this->controller->carrega_dados();
                $b_editando = true;
            }
        
            $array_form_campos = array();
        
            $count_fields = 0;

            // Lang Tab Start
        
            $inputAux = new Uzzye_TabView("lang",get_lang("_CAMPOS_DE_IDIOMAS"),"fa fa-language");
            foreach($this->langs as $langAux) {
              $inputAux->addTab(strtoupper($langAux), "", ".lang-" . $langAux);
            }
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_fields++;
        
            foreach($this->langs as $langAux) {
              $classAux = "lang-item lang-" . $langAux . " tab-item"; if($langAux == $this->langs[0]) { $classAux .= " active";}
        
              $ref = "titulo_" . $langAux;
              $inputAux = new Uzzye_TextField($ref,$ref,get_lang("_TITULO_" . strtoupper($langAux)));
              $inputAux->set_value($this->controller->get_var($ref));
              $inputAux->li_class = "w50p " . $classAux;
              $inputAux->required = $this->controller->is_required($ref);
              $array_form_campos[sizeof($array_form_campos)] = $inputAux;
              $count_fields++;

              $ref = "descricao_" . $langAux;
              $inputAux = new Uzzye_CKEditorField($ref,$ref,get_lang("_DESCRICAO_" . strtoupper($langAux)));
              $inputAux->set_value($this->controller->get_var($ref));
              $inputAux->li_class = "w100p " . $classAux;
              //$inputAux->required = $this->controller->is_required($ref);
              $inputAux->required = false;
              $array_form_campos[sizeof($array_form_campos)] = $inputAux;
              $count_fields++;
            }
        
            // Lang Tab End
        
            // HR
            $inputAux = new Uzzye_HR();		
            $inputAux->label = get_lang("_OUTROS_CAMPOS");
            $inputAux->icon = "fa fa-align-left";
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_fields++; 
                 
              $ref = "icone";
          $inputAux = new Uzzye_ImageCrop("crop_" . $ref,"crop_" . $ref,get_lang("_ICONE"));
          $inputAux->li_class = "w50p";
            $inputAux->clone = $clone;
            $inputAux->label = get_lang("_ICONE") . " (" . get_lang("_TAMANHORECOMENDAVEL") . " 96px x 96px)";
            $inputAux->crops = array(
                array("icone",get_lang("_ICONE"),0,96,96)  
            );
            $inputAux->set_value($b_editando);
            $inputAux->required = $this->controller->is_required($ref);
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_fields++;
                    
            $ref = "icone_classe";
            $inputAux = new Uzzye_TextField($ref,$ref,get_lang("_CLASSE_ICONE"));
            $inputAux->set_value($this->controller->get_var($ref));
            $inputAux->li_class = "w50p";
            $inputAux->required = $this->controller->is_required($ref);
              $array_form_campos[sizeof($array_form_campos)] = $inputAux;
              $count_fields++;
                
          
            $this->array_form_campos = $array_form_campos;

            parent::monta_campos_form($id, $readonly, $clone, $resumido);   
        }
}

?>