<?php

include_once(ROOT_CMS . "classes/arquivos_categorias.php");
include_once(ROOT_CMS . "classes/idiomas.php");


class arquivos_view extends view
{	  
    function __construct($owner = null)
	{
		$this->nome = "arquivos";
		if(function_exists("get_lang")) {
			$this->nome_exibicao = get_lang("_ARQUIVOS");
			$this->nome_exibicao_singular = get_lang("_ARQUIVO");
		}
		
		parent::__construct($owner);
			
		$this->langs = array("pt","en");
    }
	
	function monta_campos_form($id = "", $readonly = false, $clone = false, $resumido = false)
	{
		global $modulo;
		
		$b_editando = false;		
		if(trim($_REQUEST["id"]) <> "")
		{
			$id = $_REQUEST["id"];
			$this->controller->set_var("id",$id);
			$this->controller->carrega_dados();
			$b_editando = true;
		}
		
		$array_form_campos = array();

		// Lang Tab Start

		$inputAux = new Uzzye_TabView("lang",get_lang("_CAMPOS_DE_IDIOMAS"),"fa fa-language");
		foreach($this->langs as $langAux) {
			$inputAux->addTab(strtoupper($langAux), "", ".lang-" . $langAux);
		}
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_fields++;

		foreach($this->langs as $langAux) {
			$classAux = "lang-item lang-" . $langAux . " tab-item"; if($langAux == $this->langs[0]) { $classAux .= " active";}

			$ref = "titulo_" . $langAux;
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_TITULO_" . strtoupper($langAux)));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p " . $classAux;
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;
		}

		// Lang Tab End

		// HR
		$inputAux = new Uzzye_HR();		
		$inputAux->label = get_lang("_OUTROS_CAMPOS");
		$inputAux->icon = "fa fa-align-left";
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_fields++;

	// LISTBOX DE OUTRO MODULO
		$ref = "id_categoria";
		$checked = $this->controller->get_var($ref);
		if(trim($_REQUEST[$ref]) <> "") {$checked = $_REQUEST[$ref];}
		$inputAux = new Uzzye_ListBox($ref,$ref,get_lang("_CATEGORIA"),0);
		$inputAux->default_display = get_lang("_NENHUMA");
		$inputAux->default_value = 0;
		$inputAux->set_value($checked);
		$inputAux->multiple = false;
		$inputAux->required = false;
		$inputAux->set_reference_item($this->controller->get_reference_model($ref));
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_fields++;

	// CAMPO ARQUIVO
		$ref = "arquivo";
		$inputAux = new Uzzye_FileField();		
		$inputAux->label = get_lang("_ARQUIVO");
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->set_value($this->controller->get_var($ref));
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_textarea++;

	// IDIOMA
		$ref = "id_idioma";
		if(!$b_editando) {$checked = 99;}
		else {$checked = $this->controller->get_var($ref);}
		if(trim($_REQUEST[$ref]) <> "") {$checked = $_REQUEST[$ref];}
		$inputAux = new Uzzye_ListBox($ref,$ref,get_lang("_IDIOMA"),0);
		$inputAux->default_display = get_lang("_COMBOSELECIONE");
		$inputAux->default_value = 0;
		$inputAux->set_value($checked);
		$inputAux->multiple = false;
		$inputAux->required = false;
		$inputAux->set_reference_item($this->controller->get_reference_model($ref));
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_fields++;

	// Fim exemplo
		
		$this->array_form_campos = $array_form_campos;	

		parent::monta_campos_form($id, $readonly, $clone, $resumido);
	}
}