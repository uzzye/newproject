<?php    
	
class cms_log_entidades_view extends view
{	  
		function __construct($owner = null)
		{
			$this->nome = "log_entidades";
			if(function_exists("get_lang")){
				$this->nome_exibicao = get_lang("_LOG_DE_ENTIDADES");
				$this->nome_exibicao_singular = get_lang("_LOG_DE_ENTIDADES");
			}
		
			parent::__construct($owner);		
		}	
	
		function monta_campos_form($id = "", $readonly = false, $clone = false, $resumido = false)
		{
			global $modulo;
		
			$b_editando = false;		
			if(trim($_REQUEST["id"]) <> "")
			{
				$id = $_REQUEST["id"];
				$this->controller->set_var("id",$id);
				$this->controller->carrega_dados();
				$b_editando = true;
			}
		
			$array_form_campos = array();
		
			$count_fields = 0;
					
		
			$this->array_form_campos = $array_form_campos;	
		}
}

?>