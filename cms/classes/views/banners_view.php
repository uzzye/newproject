<?php    
//include_once(ROOT_CMS . "classes/teste.php");
	
class banners_view extends view
{	  
		function __construct($owner = null)
		{
			$this->nome = "banners";
			if(function_exists("get_lang")) {
				$this->nome_exibicao = get_lang("_BANNERS");
				$this->nome_exibicao_singular = get_lang("_BANNER");
			}
		
			parent::__construct($owner);
			
			$this->langs = array("pt","en");
		}	
	
		function monta_campos_form($id = "", $readonly = false, $clone = false, $resumido = false)
		{
			global $modulo;
		
			$b_editando = false;		
			if(trim($_REQUEST["id"]) <> "")
			{
				$id = $_REQUEST["id"];
				$this->controller->set_var("id",$id);
				$this->controller->carrega_dados();
				$b_editando = true;
			}
		
			$array_form_campos = array();
		
			$count_fields = 0;

			// Lang Tab Start

			$inputAux = new Uzzye_TabView("lang",get_lang("_CAMPOS_DE_IDIOMAS"),"fa fa-language");
			foreach($this->langs as $langAux) {
				$inputAux->addTab(strtoupper($langAux), "", ".lang-" . $langAux);
			}
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;

			foreach($this->langs as $langAux) {
				$classAux = "lang-item lang-" . $langAux . " tab-item"; if($langAux == $this->langs[0]) { $classAux .= " active";}

				$ref = "titulo_" . $langAux;
				$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_TITULO_" . strtoupper($langAux)));
				$inputAux->set_value($this->controller->get_var($ref));
				$inputAux->li_class = "w50p " . $classAux;
				$inputAux->required = $this->controller->is_required($ref);
				$array_form_campos[sizeof($array_form_campos)] = $inputAux;
				$count_fields++;
					
					$ref = "descricao_" . $langAux;
				$inputAux = new Uzzye_CKEditorField($ref,$ref,get_lang("_DESCRICAO_" . strtoupper($langAux)));
				$inputAux->set_value($this->controller->get_var($ref));
				$inputAux->li_class = "w100p " . $classAux;
				//$inputAux->required = $this->controller->is_required($ref);
				$inputAux->required = false;
				$array_form_campos[sizeof($array_form_campos)] = $inputAux;
				$count_fields++;
				
				$ref = "imagem_bg_" . $langAux;
				$inputAux = new Uzzye_ImageCrop("crop_" . $ref,"crop_" . $ref,get_lang("_IMAGEM_P_BG_" . strtoupper($langAux)));
				$inputAux->li_class = "w50p " . $classAux;
				$inputAux->label = get_lang("_IMAGEM_P_BG_" . strtoupper($langAux)) . " (JPG - " . get_lang("_TAMANHORECOMENDAVEL") . " 1920px x 1280px - 100% / 1024px x 768px - 80%)";
				$inputAux->array_crop_fields = array(
													array("imagem_bg_" . $langAux,get_lang("_IMAGEM_P_BG_" . strtoupper($langAux)),$this->controller->get_var("imagem_bg_" . $langAux),1920,1280,100),
													array("imagem_bg_mobile_" . $langAux,get_lang("_IMAGEM_P_BG_MOBILE_" . strtoupper($langAux)),$this->controller->get_var("imagem_bg_mobile_" . $langAux),1024,768,80)	
												);
				$inputAux->set_value($b_editando);
				$inputAux->required = $this->controller->is_required($ref);
				$array_form_campos[sizeof($array_form_campos)] = $inputAux;
				$count_fields++;
					
				$ref = "imagem_conteudo_" . $langAux;
				$inputAux = new Uzzye_ImageCrop("crop_" . $ref,"crop_" . $ref,get_lang("_IMAGEM"));
				$inputAux->li_class = "w50p " . $classAux;
				$inputAux->label = get_lang("_IMAGEM_CONTEUDO_" . strtoupper($langAux)) . " (PNG - " . get_lang("_TAMANHORECOMENDAVEL") . " 800px x 300px)";
				$inputAux->array_crop_fields = array(
													array("imagem_conteudo_" . $langAux,get_lang("_IMAGEM_CONTEUDO_" . strtoupper($langAux)),$this->controller->get_var("imagem_conteudo_" . $langAux),800,300)
												);
				$inputAux->set_value($b_editando);
				$inputAux->required = $this->controller->is_required($ref);
				$array_form_campos[sizeof($array_form_campos)] = $inputAux;
				$count_fields++;

				$ref = "texto_botao_" . $langAux;
				$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_TEXTO_P_BOTAO_" . strtoupper($langAux)));
				$inputAux->set_value($this->controller->get_var($ref));
				$inputAux->li_class = "w100p " . $classAux;
				$inputAux->required = $this->controller->is_required($ref);
				$array_form_campos[sizeof($array_form_campos)] = $inputAux;
				$count_fields++;
			}

			// Lang Tab End

			// HR
			$inputAux = new Uzzye_HR();		
			$inputAux->label = get_lang("_OUTROS_CAMPOS");
			$inputAux->icon = "fa fa-align-left";
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;
			
			/* $ref = "id_referencia";
			$checked = $this->controller->get_var($ref);
			if(trim($_REQUEST[$ref]) <> "")
			{$checked = $_REQUEST[$ref];}
			$inputAux = new Uzzye_ListBox($ref,$ref,get_lang("_TESTE"));
			$inputAux->default_display = get_lang("_NENHUM");
            $inputAux->default_value = 0;
			$inputAux->checked_value = $checked;
			$inputAux->size = 1;
            $inputAux->multiple = false;
            $classAux = "teste";
			$inputAux->reference_model = $classAux;
			$objAux = new $classAux();
			$inputAux->set_options($objAux->get_array_options("id","titulo","","titulo ASC"));
			$inputAux->refresh_options = array("id","titulo","","titulo ASC");
						$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w100p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++; */	
				
				/* $ref = "cinemagraph";
			$inputAux = new Uzzye_FileField($ref,$ref,get_lang("_CINEMAGRAPH"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;	
				
				$ref = "cinemagraph_mobile";
			$inputAux = new Uzzye_FileField($ref,$ref,get_lang("_CINEMAGRAPH_P_MOBILE"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;

		$ref = "video_url";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_VIDEO_URL"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;	
				
				$ref = "video_embed";
			$inputAux = new Uzzye_TextArea($ref,$ref,get_lang("_VIDEO_INCORPORADO"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;	

			$ref = "video_autoplay";
			if(!$b_editando){
				$checked = 0;
			} else {
				$checked = $this->controller->get_var($ref);
			}
			if(trim($_REQUEST[$ref]) <> "")
			{$checked = $_REQUEST[$ref];}

			$inputAux = new Uzzye_CheckBox($ref,$ref,get_lang("_VIDEO_AUTOPLAY"));
			$inputAux->default_value = 0;
			$inputAux->checked_value = $checked;		
			$inputAux->set_value(1);
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;	
				
				$ref = "cor_overlay";
			$inputAux = new Uzzye_ColorField($ref,$ref,get_lang("_COR_OVERLAY"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p color";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++; */
				
				$ref = "url";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_URL"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;	
				
				$ref = "target";
			$checked = $this->controller->get_var($ref);
			if(trim($_REQUEST[$ref]) <> "")
			{$checked = $_REQUEST[$ref];}
			$inputAux = new Uzzye_ComboBox($ref,$ref,get_lang("_TARGET"));
			$inputAux->default_display = get_lang("_COMBOSELECIONE");
			$inputAux->default_value = "";
			$inputAux->checked_value = $checked;
			$inputAux->size = 1;
            $inputAux->multiple = false;
			$inputAux->set_options(array(array(output_decode(""),output_decode("")),array(output_decode("_blank"),output_decode("_blank"))));
						$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;	
				
				$ref = "tempo_duracao";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_TEMPO_DURACAO_MS"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;	
				
				$ref = "tempo_transicao";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_TEMPO_TRANSICAO_MS"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;		
			
			$this->array_form_campos = $array_form_campos;	

			return parent::monta_campos_form($id, $readonly, $clone, $resumido);
		}
}

?>