<?php

class view 
{	
	public $nome_exibicao;
	public $nome_exibicao_singular;
	
	public $owner;
	public $controller;	
    
    // Arrays de ações devem ter mesma quantidade de posições, cada um respectivo ao outro
    public $array_acoes_cms; // array de ações <a> html para a lista de registros
    public $array_expr_acoes_cms; // array de posições com expressões eval para troca de classe - usar em combinação com data-function que chamará função JS no clique
    public $array_perms_acoes_cms; // array de permissões a validar para exibir a ação ou não
    
    // Arrays de botões acima da listagem devem ter mesma quantidade de posições, cada um respectivo ao outro
    public $array_botoes_cms; // array de botões <a> html
    public $array_perms_botoes_cms; // array de permissões para exibir os botões ou não
	
	public $array_form_campos;
	public $array_form_refs;
	
	public $help_text;

	// v3.0
	public $titulo_cadastro;

	// Array de campos permitidos no formulário, em branco para todos, passado chave = nome do campo / valor = editable. Ex.: array("nome" => false)
	public $campos_permitidos;

	public $array_form_campos_padrao;
	public $lang_table_vars;
	public $langs = array("pt");

	public $default_value_field;
	public $default_view_field;
	public $default_view_expr;
	public $default_value_expr;
	public $default_sql_order;
	public $default_sql_where;

	public $custom_expr_masks;
	/* $this->custom_expr_masks = array(
		"valor" => "expressão php para eval()"
	);*/

	public $number_masks;
	/* $this->number_masks = array(
		"valor" => array(2,",",".","RS "," %");
		campo 	=> array(decimal, decimal_char, thousand_char, prefix, sufix)
	);*/

	public $date_masks = array(
		"data_criacao" => true,
		"data_atualizacao" => true,
	);

	public $color_masks;
	/* $this->color_masks = array(
		"cor" => "hexa"
		campo 	=> tipo_cor
	);*/

	// v3.1
	public $qtd_itens_pp = 10;
	
	function __construct($owner = null){ 
    	global $modulo;
    	
		$this->owner = $owner;
    	$this->controller = $this->owner;    

		$this->array_perms_acoes_cms = array(
			"Editar",
			"AtivarDesativar",
			"Visualizar",
			"Clonar",
		   	"Excluir"
		);

		$this->array_perms_botoes_cms = array(
			"Incluir",
			"Exportar"
		);
    	
    	if(function_exists("get_lang")) {
    		
    		$this->array_acoes_cms = array(
    			"<a rel=\"address\" href=\"#/" . $modulo . "/editar/#PARAM#\" class=\"mr-5\" data-toggle=\"tooltip\" data-original-title=\"" . (get_lang("_EDITAR")) . "\"> <i class=\"fa fa-pencil text-inverse\"></i> </a>",
    			"<a href=\"#\" data-id=\"#PARAM#\" data-url=\"" . ROOT_SERVER . ROOT . "ajax/" . $modulo . "/ativar_desativar/#PARAM#\" class=\"mr-5 acao-toggle-ativo\" data-toggle=\"tooltip\" data-original-title=\"" . (get_lang("_ATIVAR_DESATIVAR")) . "\"> <i class=\"fa fa-#TOGGLE_ATIVO# text-inverse\"></i> </a>", //
    			"<a rel=\"address\" href=\"#/" . $modulo . "/visualizar/#PARAM#\" class=\"mr-5\" data-toggle=\"tooltip\" data-original-title=\"" . (get_lang("_VISUALIZAR")) . "\"> <i class=\"fa fa-eye text-inverse\"></i> </a>",
    			"<a rel=\"address\" href=\"#/" . $modulo . "/clonar/#PARAM#\" class=\"mr-5\" data-toggle=\"tooltip\" data-original-title=\"" . (get_lang("_CLONAR")) . "\"> <i class=\"fa fa-clone text-inverse\"></i> </a>",
    			"<a href=\"#\" data-id=\"#PARAM#\" data-url=\"" . ROOT_SERVER . ROOT . "ajax/" . $modulo . "/excluir/#PARAM#\" class=\"mr-5 acao-excluir\" data-toggle=\"tooltip\" data-original-title=\"" . (get_lang("_REMOVER")) . "\"> <i class=\"fa fa-trash text-danger\"></i> </a>"
    		);
									  
	    	$exib = get_lang("_ADICIONARNOVO");
			//if(strlen($this->nome_exibicao_singular) <= 12)
			{
				$exib .= " " . $this->nome_exibicao_singular; 
			}

			$this->array_botoes_cms = array(
				"<a rel=\"address\" href=\"#/" . $modulo . "/incluir\"><button type=\"button\" class=\"btn btn-success btn-anim\"><i class=\"icon-plus\"></i><span class=\"btn-text\">" . $exib . "</span></button></a>",
				"<a rel=\"address\" href=\"#/" . $modulo . "/exportar_csv\"><button type=\"button\" class=\"btn btn-default btn-anim\"><i class=\"icon-cloud-download\"></i><span class=\"btn-text\">" . get_lang("_EXPORTAR_CSV") . "</span></button></a>"
			);
		} else {
			$this->array_acoes_cms = array();
			$this->array_botoes_cms = array();
		}

		// v3.0 default value(id) and exib fields/masks

		$this->default_value_field = "id";
		$this->default_view_field = "id";

		//carrega_classe($this->controller->nome . "_model"); // Slow performance and not necessary
		if(property_exists($this->controller->nome . "_model","nome")) {
			$this->default_view_field = "nome";
		} else if(property_exists($this->controller->nome . "_model","titulo")) {
			$this->default_view_field = "titulo";
		} else if(property_exists($this->controller->nome . "_model","titulo_pt")) {
			$this->default_view_field = "titulo_pt";
		} else if(property_exists($this->controller->nome . "_model","label")) {
			$this->default_view_field = "label";
		}

		$this->default_view_expr = "";
		$this->default_value_expr = "";
		$this->default_sql_order = "";
		$this->default_sql_where = "";
   	}

   	function monta_campos_form($id = "", $readonly = false, $clone = false, $resumido = false) {
		global $modulo;

		$b_editando = false;		
		if(trim($_REQUEST["id"]) <> "")
		{
			$id = $_REQUEST["id"];
			$b_editando = true;
		}

   		// Interno na view derivada

   		// Adiciona ID como primeiro campo

		if($this->controller->mostra_id_registro) {
			$ref = "id_view";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_ID"));
			$inputAux->readonly = true;
			$inputAux->set_value($this->controller->get_var("id"));

			array_unshift($this->array_form_campos, $inputAux);
		}

   		// Campos padrão

   		$array_aux = array();

   		if($this->controller->mostra_ativo_registro) {

			$ref = "ativo";
			if(!$b_editando){
				$checked = 1;
			} else {
				$checked = $this->controller->get_var($ref);
			}
			if(trim($_REQUEST[$ref]) <> "")
			{$checked = $_REQUEST[$ref];}

			$inputAux = new Uzzye_CheckBox($ref,$ref,get_lang("_ATIVO"));
			$inputAux->default_value = 0;
			//$inputAux->li_class = "w25p"; // caso habilitar o campo de edição do permalink
			$inputAux->li_class = "w50p";
			$inputAux->checked_value = $checked;		
			$inputAux->set_value(1);
			$inputAux->required = $this->controller->is_required($ref);
			$array_aux[sizeof($array_aux)] = $inputAux;
   		}

   		if($this->controller->mostra_ranking_registro) {

			$ref = "ranking";
			if($b_editando) {
				$varAux = $this->controller->get_var($ref);
			} else {
				$varAux = $this->controller->get_last_var($ref)+1;
			}
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_RANKING"));
			$inputAux->set_value($varAux);
			//$inputAux->li_class = "w25p"; // caso habilitar o campo de edição do permalink
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_aux[sizeof($array_aux)] = $inputAux;
   		}

   		/*if($this->controller->mostra_permalink_registro) {

			$ref = "permalink";
			$varAux = $this->controller->get_var($ref);
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_CUSTOM_PERMALINK"));
			$inputAux->set_value($varAux);
			$inputAux->li_class = "w50p";
			$inputAux->required = false;
			$array_aux[sizeof($array_aux)] = $inputAux;
   		}*/

   		if($this->controller->mostra_data_registro) {

			$ref = "data_view";
			if(trim($this->controller->get_var("data_atualizacao")) != "" && trim($this->controller->get_var("data_atualizacao")) != "0000-00-00 00:00:00") {
				$varAux = $this->controller->get_var_format("data_atualizacao");
			} else if(trim($this->controller->get_var("data_criacao")) != "" && trim($this->controller->get_var("data_criacao")) != "0000-00-00 00:00:00") {
				$varAux = $this->controller->get_var_format("data_criacao");
			} else {
				$varAux = "";
			}

			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_DATA_ULTIMA"));
			$inputAux->set_value($varAux);
			$inputAux->readonly = true;
			$array_aux[sizeof($array_aux)] = $inputAux;
   		}

   		if($this->controller->mostra_autor_registro) {

			$ref = "usuario_view";
			if(intval($this->controller->get_var("usuario_atualizacao")) > 0) {
				$usuAux = $this->controller->get_objeto_referencia("usuario_atualizacao");
				$varAux = $usuAux->get_var("email");
			} else if(intval($this->controller->get_var("usuario_criacao")) > 0) {
				$usuAux = $this->controller->get_objeto_referencia("usuario_criacao");
				$varAux = $usuAux->get_var("email");
			} else {
				$varAux = "";
			}

			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_ULTIMO_USUARIO"));
			$inputAux->set_value($varAux);
			$inputAux->readonly = true;
			$array_aux[sizeof($array_aux)] = $inputAux;
   		}

   		// Tabela de lang

   		if($this->controller->mostra_lang_table) {

   			// Só exibe se tiver mais que Todos e Português (BR)

   			$idis = new idiomas();
   			if($idis->count_reg_ativos() > 2)
   			{

			// HR
				$inputAux = new Uzzye_HR();		
				$inputAux->label = get_lang("_IDIOMAS");
				$inputAux->icon = "fa fa-language";
				$array_aux[sizeof($array_aux)] = $inputAux;

			// LANG
				$ref = "lang";
		    	$inputAux = new Uzzye_LangTable();
		    	$inputAux->clone = $clone;
				//$inputAux->label = get_lang("_IDIOMAS");
				$inputAux->name = $ref;
				$inputAux->li_class = 'w100p';
				$inputAux->id = $ref;
				$inputAux->lang_vars = $this->lang_table_vars;
				$inputAux->modulo_registro = ($modulo);
				$inputAux->id_registro = intval($id);
				$inputAux->b_editando = $b_editando;
				$array_aux[sizeof($array_aux)] = $inputAux;
			}
		}

   		$this->array_form_campos_padrao = $array_aux;

   		// Testa campos permitidos

   		if(is_array($this->campos_permitidos) && sizeof($this->campos_permitidos) > 0) {
   			$keys = array_keys($this->campos_permitidos);

	   		for($w=0; $w<sizeof($this->array_form_campos); $w++) {
	   			$nomeAux = $this->array_form_campos[$w]->name;
	   			if(in_array($nomeAux, $keys)) {
	   				// OK
	   				$this->array_form_campos[$w]->readonly = !$this->campos_permitidos[$nomeAux];
	   			} else {
	   				array_splice($this->array_form_campos, $w, 1);
	   				$w--;
	   			}
	   		}

	   		for($w=0; $w<sizeof($this->array_form_refs); $w++) {
	   			$nomeAux = $this->array_form_refs[$w]->name;
	   			if(in_array($nomeAux, $keys)) {
	   				// OK
	   				$this->array_form_refs[$w]->readonly = !$this->campos_permitidos[$nomeAux];
	   			} else {
	   				array_splice($this->array_form_refs, $w, 1);
	   				$w--;
	   			}
	   		}

	   		for($w=0; $w<sizeof($this->array_form_campos_padrao); $w++) {
	   			$nomeAux = $this->array_form_campos_padrao[$w]->name;
	   			if(in_array($nomeAux, $keys)) {
	   				// OK
	   				//$this->array_form_campos_padrao[$w]->readonly = !$this->campos_permitidos[$nomeAux];
	   			} else {
	   				array_splice($this->array_form_campos_padrao, $w, 1);
	   				$w--;
	   			}
	   		}
	   	}
   	}
    
	function set_owner($owner){ 
    	$this->owner = $owner;
    	$this->controller = $this->owner;
    }
	
	function lista($array_campos,$array_nomes,$array_expressoes = "",$sqlWhere = "",$sqlOrderBy = "",$array_foreign_keys = "",$array_where_filtros = "",$array_condicoes_acoes = null, $array_expressoes_sql = null)
	{	
		global $modulo, $projectName;

		$permite_acesso = $this->controller->valida_permissao($modulo,"Listar",false);
		
		if($permite_acesso) {
			$this->heading();
			
			if(trim($_REQUEST["tipo_msg"]) != "") {
				$this->tipo_msg = $_REQUEST["tipo_msg"];
				$this->status = $_REQUEST["status_operacao"];
				$this->id = $_REQUEST["id"];
				$this->gera_mensagens();
			}

			if($array_condicoes_acoes == null) {
				$array_condicoes_acoes = $this->array_condicoes_acoes;
			}
			if($array_expressoes_sql == null) {
				$array_expressoes_sql = $this->array_expressoes_sql;
			}

			// Registra em sessão

			$sessVar = "_" . $projectName . "_" . $modulo;
			$_SESSION[$sessVar]["array_campos"] = $array_campos;
			$_SESSION[$sessVar]["array_nomes"] = $array_nomes;
			$_SESSION[$sessVar]["array_expressoes"] = $array_expressoes;
			$_SESSION[$sessVar]["sqlWhere"] = $sqlWhere;
			$_SESSION[$sessVar]["sqlOrderBy"] = $sqlOrderBy;
			$_SESSION[$sessVar]["array_foreign_keys"] = $array_foreign_keys;
			$_SESSION[$sessVar]["array_where_filtros"] = $array_where_filtros;
			$_SESSION[$sessVar]["array_condicoes_acoes"] = $array_condicoes_acoes;
			$_SESSION[$sessVar]["array_expressoes_sql"] = $array_expressoes_sql;

			$this->gera_grade_registros($array_campos,$array_nomes,$array_expressoes,$sqlWhere,$sqlOrderBy,$array_foreign_keys,$array_where_filtros,$array_condicoes_acoes,$array_expressoes_sql);	
		}
	}	
	
	function gera_mensagens($dismiss = true)
	{
		global $modulo;
		// DEBUG
		//$this->tipo_msg = "i";
		//$this->status = 1;

		if ( $this->tipo_msg != "" ) {
			if ( $this->status == 0 ) { // erro
				if($this->tipo_msg=="e")
				{
					$msg = "<i class=\"ti-na pr-15\"></i>";
					$msgText = get_lang("_MSG_ERRO_ALTERACAO");
				}
				else if($this->tipo_msg=="i")
				{ 
					$msg = "<i class=\"ti-na pr-15\"></i>";
					$msgText = get_lang("_MSG_ERRO_INCLUSAO");
				}
				else if($this->tipo_msg=="r")
				{ 
					$msg = "<i class=\"ti-na pr-15\"></i>";
					$msgText = get_lang("_MSG_ERRO_EXCLUSAO");
				}
				else if($this->tipo_msg=="c")
				{ 
					$msg = "<i class=\"ti-na pr-15\"></i>";
					$msgText = get_lang("_MSG_ERRO_CLONAGEM");
				}
				$classe_erro = "danger";
			} else { // sucesso
				if($this->tipo_msg=="e")
				{
					$msg = "<i class=\"ti-check pr-15\"></i>";
					$msgText = get_lang("_MSG_CONFIRMACAO_ALTERACAO");
				}
				else if($this->tipo_msg=="i")
				{ 
					$msg = "<i class=\"ti-check pr-15\"></i>";
					$msgText = get_lang("_MSG_CONFIRMACAO_INCLUSAO");
				}
				else if($this->tipo_msg=="r")
				{ 
					$msg = "<i class=\"ti-check pr-15\"></i>";
					$msgText = get_lang("_MSG_CONFIRMACAO_EXCLUSAO");
				}
				else if($this->tipo_msg=="c")
				{ 
					$msg = "<i class=\"ti-check pr-15\"></i>";
					$msgText = get_lang("_MSG_CONFIRMACAO_CLONAGEM");
				}
				$classe_erro = "success";
			} 
			if(trim($_REQUEST["msg"]) <> "")
			{
				//$msg .= "<br/>" . base64_decode($_REQUEST["msg"]);
				$msgText = base64_decode($_REQUEST["msg"]);
			}
			if(trim($this->descr_msg) != "")
			{
				//$msg .= "<br/><em>" . $this->descr_msg . "</em>";
				$msgText = "<em>" . $this->descr_msg . "</em>";
			}

			$msg .= $msgText;

			?>
			<div class="row alert-display">
				<div class="col-sm-12">
					<div class="alert alert-<?php echo $classe_erro; ?> <?php if($dismiss) {?>alert-dismissable<?php } ?>">
						<?php if($dismiss) {?><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><?php } ?>
						<?php echo ($msg); ?>
					</div>
				</div>
			</div>
			<?php
        }
	}
	
	function gera_grade_registros($array_campos,$array_nomes,$array_expressoes = "",$sqlWhere = "",$sqlOrderBy = "",$array_foreign_keys = "",$array_where_filtros = "",$array_condicoes_acoes = "",$array_expressoes_sql = "")
	{	
		global $modulo, $mySQL_prefix, $projectName;
		
		$sessVar = "_" . $projectName . "_" . $modulo;
	    		
		?>
		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view">
					<?php
					// SE QUISER MOSTRAR TÍTULO E REGISTROS POR PHP
					/*<div class="panel-heading">
						<div class="pull-left">
							<h6 class="panel-title txt-dark"><?php echo ($this->nome_exibicao)?> <strong id='<?php echo $modulo; ?>_i_count_reg'></strong></h6>
						</div>
						<div class="clearfix"></div>
					</div>*/ 
					// SENÃO, ABAIXO ?>
					<div class="panel-heading">
						<div class="pull-left">
							<h6 class="panel-title txt-dark"><?php echo get_lang("_LISTA_REGISTROS"); ?></h6>
						</div>
						
						<div class="clearfix"></div>
					</div>

					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="row mb-20">
								<div class="col-sm-12">
								<?php

								// BOTÕES

								$i_count = 0; 
								foreach($this->array_botoes_cms as $botao){
									if($this->controller->valida_permissao($modulo,$this->array_perms_botoes_cms[$i_count],false)){
										if($i_count>0)
										{echo " ";}
										echo $botao;
									}
									$i_count++;
								}

								if($this->controller->valida_permissao($modulo,"Excluir")) {
									if($i_count>0)
									{echo " ";}									
									echo "<button data-url=\"" . ROOT_SERVER . ROOT . "ajax/" . $modulo . "/do_excluir_massa\" type=\"button\" class=\"btn btn-danger btn-anim hidden btn-excluir-massa\"><i class=\"icon-trash\"></i><span class=\"btn-text\">" . get_lang("_EXCLUIR_REG_PAGINA") . "</span></button></a>";
								}

								?>
								</div>
							</div>
							<!-- Data table -->
							<div class="table-wrap mb-10">
								<div class="">
									<table id="table-<?php echo $modulo; ?>" class="table table-striped table-hover display pb-30" >
										<?php

										// MONTA RESULTADOS

										$str_fields = "";
										$json_columns = "";

										$json_columns .= "{\"name\": \"null.selecao_registro\", \"createdCell\": function(cell) { $(cell).addClass('col-selecao'); }}";
													
										if($this->controller->mostra_selecao_registro)
						                {
											$str_fields .= "<th class=\"no-sort\">
												<div class=\"checkbox checkbox-custom\">
													<input type=\"checkbox\" value=\"all\" class=\"select-reg select-all-reg\" id=\"checkbox-registro-all\" />
													<label for=\"checkbox-registro-all\"></label>
												</div>
											</th>";
						                } else {
						                	$str_fields .= "<th class=\"no-sort\">
						                		&nbsp;
											</th>";
						                }
													
										if($this->controller->mostra_id_registro)
						                {
						                	$json_columns .= ",";
		            						$json_columns .= "{\"name\": \"principal.id\", \"createdCell\": function(cell) { $(cell).addClass('col-id'); }}";

											$str_fields .= "<th title=\"principal.id\">" . (get_lang("ID")) . "</th>";
						                }
									
										$w=0;
										while($w<sizeof($array_nomes))
										{     
											if($array_campos[$w] != "ativo" &&
											   $array_campos[$w] != "permalink" &&
											   $array_campos[$w] != "ranking" &&
											   $array_campos[$w] != "views" &&
											   $array_campos[$w] != "usuario_criacao" &&
											   $array_campos[$w] != "usuario_atualizacao" &&
											   $array_campos[$w] != "data_criacao" &&
											   $array_campos[$w] != "data_atualizacao")
											{
											    if(trim($array_foreign_keys[$w]) <> "")
												{
													$i_index = $this->controller->get_foreign_key_index($array_campos[$w]);		
												}

												$json_columns .= ",";
												$json_columns .= "{\"name\": \"";
												$preField = "";
												if(stristr($array_campos[$w], ".") === false) {
													$json_columns .= "principal.";
													$preField = "principal.";
												}
												$json_columns .= $array_campos[$w] . "\", \"createdCell\": function(cell) { $(cell).addClass('col-" . $array_campos[$w] . "'); }}";
										
												$str_fields .= "<th title=\"" . $preField.$array_campos[$w] . "\">" . ($array_nomes[$w]) . "</th>";

												unset($preField);
											}

											$w++;
										}
										
										$i_index = $this->controller->get_foreign_key_index("usuario_criacao");
										
										if($this->controller->mostra_ranking_registro)
										{
											$json_columns .= ",";
		            						$json_columns .= "{\"name\": \"principal.ranking\", \"createdCell\": function(cell) { $(cell).addClass('col-ranking'); }}";

											$str_fields .= "<th title=\"principal.ranking\">" . (get_lang("_RANKING")) . "</th>";
										}

										if($this->controller->mostra_permalink_registro)
										{
											$json_columns .= ",";
											$json_columns .= "{\"name\": \"principal.permalink\", \"createdCell\": function(cell) { $(cell).addClass('col-permalink'); }}";

											$str_fields .= "<th title=\"principal.permalink\">" . (get_lang("_PERMALINK")) . "</th>";
										}

										if($this->controller->mostra_views_registro)
										{											
						                	$json_columns .= ",";
						                	$json_columns .= "{\"name\": \"principal.views\", \"createdCell\": function(cell) { $(cell).addClass('col-views'); }}";

											$str_fields .= "<th title=\"principal.views\">" . (get_lang("_VIEWS")) . "</th>";
										}

										if($this->controller->mostra_autor_registro)
										{
											$json_columns .= ",";
						            		//$json_columns .= "{\"name\": \"principal.usuario_criacao\", \"createdCell\": function(cell) { $(cell).addClass('col-usuario_criacao'); }}";
						            		$json_columns .= "{\"name\": \"aux0.email\", \"createdCell\": function(cell) { $(cell).addClass('col-usuario_criacao'); }}";

											$str_fields .= "<th title=\"aux0.email\">" . (get_lang("_AUTORREGISTRO")) . "</th>";
										}
										
										if($this->controller->mostra_data_registro)
										{
											$json_columns .= ",";
	                						$json_columns .= "{\"name\": \"principal.data_criacao\", \"createdCell\": function(cell) { $(cell).addClass('col-data_criacao'); }}";

											$str_fields .= "<th title=\"principal.data_criacao\">" . (get_lang("_DATAREGISTRO")) . "</th>";
										}
										
										if($this->controller->mostra_ativo_registro)
										{
											$json_columns .= ",";
											$json_columns .= "{\"name\": \"principal.ativo\", \"createdCell\": function(cell) { $(cell).addClass('col-ativo'); }}";

											$str_fields .= "<th title=\"principal.ativo\">" . (get_lang("_ATIVOINATIVO")) . "</th>";
										}

					                	$json_columns .= ",";
					                	$json_columns .= "{\"name\": \"null\", \"createdCell\": function(cell) { $(cell).addClass('col-acoes').addClass('text-nowrap'); }}";

										$str_fields .= "<th class=\"no-sort text-nowrap\" data-priority=\"1\">" . (get_lang("_ACOES")) . "</th>";

										?>
										<thead>
											<tr>
												<?php echo $str_fields; ?>
								            </tr>
								        </thead>
								        <tbody> 
								        	<!-- AJAX Content -->
								        	<?php

								        	//$this->get_registros_html($array_campos,$array_nomes,$array_expressoes,$sqlWhere,$sqlOrderBy,$array_foreign_keys,$array_where_filtros,$array_condicoes_acoes,$array_expressoes_sql);

								        	?>
								        	<tr>
												<td colspan="999999" align="center">
												<?php echo get_lang("_CARREGANDO"); ?>...
												</td>
								            </tr>
										</tbody>
										<tfoot>
											<?php echo $str_fields; ?>
										</tfoot>
									</table>
								</div>
							</div>

							<div class="row mb-20">
								<div class="col-sm-12">
								<?php

								// BOTÕES

								$i_count = 0; 
								foreach($this->array_botoes_cms as $botao){
									if($this->controller->valida_permissao($modulo,$this->array_perms_botoes_cms[$i_count],false)){
										if($i_count>0)
										{echo " ";}
										echo $botao;
									}
									$i_count++;
								}

								if($this->controller->valida_permissao($modulo,"Excluir")) {
									if($i_count>0)
									{echo " ";}									
									echo "<button data-url=\"" . ROOT_SERVER . ROOT . "ajax/" . $modulo . "/do_excluir_massa\" type=\"button\" class=\"btn btn-danger btn-anim hidden btn-excluir-massa\"><i class=\"icon-trash\"></i><span class=\"btn-text\">" . get_lang("_EXCLUIR_REG_PAGINA") . "</span></button></a>";
								}

								?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<script src="<?php echo ROOT; ?>_js/plugins/jquery.dataTables.min.js"></script>
		<script src="<?php echo ROOT; ?>_js/plugins/dataTables.buttons.min.js"></script>
		<script src="<?php echo ROOT; ?>_js/plugins/dataTables.responsive.min.js"></script>

		<?php
		
		ob_flush();
		?>
		<script language='javascript'>
			/*Responsive Datatable Init*/

			"use strict"; 

			$(document).ready(function(e) {
				dataTables["table-<?php echo $modulo; ?>"] = $('#table-<?php echo $modulo; ?>').DataTable( {
					responsive: true,
					pageLength: <?php echo intval($this->qtd_itens_pp); ?>,
  					lengthMenu: [ 10, 25, 50, 75, 100 ],
					language: {
						aria: {
							sortAscending: ": <?php echo get_lang("_ATIVARPARAORDENARASC"); ?>",
							sortDescending: ": <?php echo get_lang("_ATIVARPARAORDENARDESC"); ?>"
						},
						paginate: {
							first: "<?php echo get_lang("_PRIMEIRA"); ?>",
							last: "<?php echo get_lang("_ULTIMA"); ?>",
							next: "<?php echo get_lang("_PROXIMA"); ?>",
							previous: "<?php echo get_lang("_ANTERIOR"); ?>"
						},
						emptyTable: "<?php echo get_lang("_NENHUMREGISTRO"); ?>",
						info: "<?php echo get_lang("_MONSTRANDO"); ?> _START_ <?php echo get_lang("_TO"); ?> _END_ <?php echo get_lang("_OF"); ?> _TOTAL_ <?php echo strtolower(get_lang("_REGISTROS")); ?>",
						infoEmpty: "<?php echo get_lang("_MONSTRANDO"); ?> 0 <?php echo get_lang("_TO"); ?> 0 <?php echo get_lang("_OF"); ?> 0 <?php echo strtolower(get_lang("_REGISTROS")); ?>",
						infoFiltered: "(<?php echo get_lang("_FILTRADODE"); ?> _MAX_ <?php echo get_lang("_REGISTROS_TOTAIS"); ?>)",
						infoPostFix: "",
						decimal: "",
						thousands: ",",
						lengthMenu: "<?php echo get_lang("_MOSTRAR"); ?> _MENU_ <?php echo strtolower(get_lang("_REGISTROS")); ?>",
						loadingRecords: "<?php echo get_lang("_CARREGANDO"); ?>...",
						processing: "<?php echo get_lang("_PROCESSANDO"); ?>...",
						search: "<?php echo get_lang("_BUSCAR"); ?>:",
						searchPlaceholder: "",
						url: "",
						zeroRecords: "<?php echo get_lang("_NENHUMREGISTROCORRESPONDE"); ?>"
					},
					<?php
					// Restaura paginação em sessão
					if(@trim($_SESSION[$sessVar]["pag_start"]) != "") {
						?>"displayStart": <?php echo intval($_SESSION[$sessVar]["pag_start"]); ?>,<?php
					}
					if(@trim($_SESSION[$sessVar]["pag_length"]) != "") {
						?>"pageLength": <?php echo intval($_SESSION[$sessVar]["pag_length"]); ?>,<?php
					}
					$order_num = 1;
					$order_dir = "desc";
					if(@trim($_SESSION[$sessVar]["order_num"]) != "") {$order_num = $_SESSION[$sessVar]["order_num"];}
					if(@trim($_SESSION[$sessVar]["order_dir"]) != "") {$order_dir = $_SESSION[$sessVar]["order_dir"];}
					?>
					"order": [[ <?php echo intval($order_num); ?>, "<?php echo $order_dir; ?>" ]],
					"preDrawCallback": function( settings ) {
				        if($("#loader-wrapper").css("display") == "none") {
				        	showLoading(iTimeAuxFade);
				        }
				    },
					"drawCallback": function( settings ) {
				        refreshTabelaRegistros();
				        if($("#loader-wrapper").css("display") != "none") {
				        	hideLoading(iTimeAuxFade);
				        }
				    }
				    ,
			        "ajax": {
				        "url" : ROOT_SERVER + ROOT + "ajax/<?php echo $modulo; ?>/get_registros/",
				        "type": "POST"
				        // DEBUG
				        /*,"dataSrc": function ( json ) {
			                console.log(json.data);
			            }*/
					},
					"columns": [
						<?php echo $json_columns; ?>
					]
					,
					"deferRender": true,
					"processing": false,
        			"serverSide": true
				} );
			} );

			dataTables["table-<?php echo $modulo; ?>"].on( 'responsive-resize', function ( e, datatable, columns ) {
		        //refreshTabelaRegistros();
		    });
		    dataTables["table-<?php echo $modulo; ?>"].on( 'responsive-display', function ( e, datatable, row, showHide, update ) {
		    	//refreshTabelaRegistros();
		    });

			<?php
			// Restaura busca em sessão
			if(@trim($_SESSION[$sessVar]["search"]) != "") {
			?>
			dataTables["table-<?php echo $modulo; ?>"].search("<?php echo stripslashes($_SESSION[$sessVar]["search"]); ?>").draw(false);
			<?php
			}
			?>
		</script>
		<?php

		// SE QUISER MOSTRAR REGISTROS POR PHP
		/*document.getElementById('<?php echo $modulo; ?>_i_count_reg').innerHTML = "(<?php echo $this->controller->count_reg($sqlWhere) . " " . ($this->controller->count_reg($sqlWhere) > 1 ? "registros" : "registro") . ")"; ?>";*/
	}

	function get_registros($array_campos = "",$array_nomes = "",$array_expressoes = "",$sqlWhere = "",$sqlOrderBy = "",$array_foreign_keys = "",$array_where_filtros = "",$array_condicoes_acoes = "",$array_expressoes_sql = "") 
	{
		global $modulo, $mySQL_prefix, $projectName, $db;

		// Pega da sessão, ou seja, para testar o SQL nesta função, tem que primeiro recarregar a página mãe, que possui a listagem

		$sessVar = "_" . $projectName . "_" . $modulo;
		if(trim($array_campos) == "") {$array_campos = $_SESSION[$sessVar]["array_campos"];}
		if(trim($array_nomes) == "") {$array_nomes = $_SESSION[$sessVar]["array_nomes"];}
		if(trim($array_expressoes) == "") {$array_expressoes = $_SESSION[$sessVar]["array_expressoes"];}
		if(trim($sqlWhere) == "") {$sqlWhere = $_SESSION[$sessVar]["sqlWhere"];}
		if(trim($sqlOrderBy) == "") {$sqlOrderBy = $_SESSION[$sessVar]["sqlOrderBy"];}
		if(trim($array_foreign_keys) == "") {$array_foreign_keys = $_SESSION[$sessVar]["array_foreign_keys"];}
		if(trim($array_where_filtros) == "") {$array_where_filtros = $_SESSION[$sessVar]["array_where_filtros"];}
		if(trim($array_condicoes_acoes) == "") {$array_condicoes_acoes = $_SESSION[$sessVar]["array_condicoes_acoes"];}
		if(trim($array_expressoes_sql) == "") {$array_expressoes_sql = $_SESSION[$sessVar]["array_expressoes_sql"];}

		ob_clean();

		header("Content-Type: application/json; charset=UTF-8");

		$iTotalRegs = 0;
		$iFilterRegs = 0;

		$return = "{\"data\": [";

		//$return .= json_encode($_REQUEST); // DEBUG
		//if(false && is_array($array_campos) && sizeof($array_campos) > 0) {
		if(is_array($array_campos) && sizeof($array_campos) > 0) {

			// Monta request de ordenação
			$sOrderAux = "";
			if(is_array($_REQUEST["order"]) && sizeof($_REQUEST["order"]) > 0) {
				for($w=0; $w<sizeof($_REQUEST["order"]); $w++) {
					$iAux = $_REQUEST["order"][$w]["column"];
					$colAux = $_REQUEST["columns"][$iAux]["name"];

					// Prev null fields
					if(stristr($colAux, "null.") === false) {
						// Prev injection
						if(stristr($colAux, "SELECT ") === false &&
						   stristr($colAux, "DROP ") === false &&
						   stristr($colAux, "ALTER ") === false &&
						   stristr($colAux, "UPDATE ") === false &&
						   stristr($colAux, "DELETE ") === false &&
						   stristr($colAux, "GRANT ") === false &&
						   stristr($colAux, "CHANGE ") === false)
						{
							if(trim($sOrderAux) != "") {
								$sOrderAux .= ", ";
							}
							// Prev injection
							$ascDesc = "";
							if(strtoupper(trim($_REQUEST["order"][$w]["dir"])) == "ASC") {
								$ascDesc = "ASC";
							} else if(strtoupper(trim($_REQUEST["order"][$w]["dir"])) == "DESC") {
								$ascDesc = "DESC";
							}
							$sOrderAux .= $colAux . " " . $ascDesc;
		
							$_SESSION[$sessVar]["order_num"] = $_REQUEST["order"][$w]["column"];
							$_SESSION[$sessVar]["order_dir"] = strtolower($ascDesc);
						}
					}
				}
			}	

			if(trim($sOrderAux) != "") {
				if(trim($sqlOrderBy) != "") {
					$sqlOrderBy = $sOrderAux .= ", " . $sqlOrderBy;
				} else {
					$sqlOrderBy = $sOrderAux;
				}
			}
			unset($sOrderAux);

			// Monta request de paginação
			$sqlLimit = "";
			if(isset($_REQUEST["start"]) || isset($_REQUEST["length"])) {
				if(trim($_REQUEST["start"]) != "") {
					$sqlLimit .= $_REQUEST["start"];
					// Salva paginação em sessão
					$_SESSION[$sessVar]["pag_start"] = intval($_REQUEST["start"]);
				} else {
					$sqlLimit .= "0";
				}
				if(trim($sqlLimit) != "") {$sqlLimit .= ",";}
				if(trim($_REQUEST["length"]) != "") {
					$sqlLimit .= $_REQUEST["length"];
					// Salva paginação em sessão
					$_SESSION[$sessVar]["pag_length"] = intval($_REQUEST["length"]);
				} else {
					$sqlLimit .= "10";
				}
			}

			// Monta request de search
			//$_REQUEST["search"]["value"] = "ket"; // DEBUG

			if(trim($_REQUEST["search"]["value"]) != "") {
				// Prev injection
				if(stristr($_REQUEST["search"]["value"], "SELECT ") === false &&
				   stristr($_REQUEST["search"]["value"], "DROP ") === false &&
				   stristr($_REQUEST["search"]["value"], "ALTER ") === false &&
				   stristr($_REQUEST["search"]["value"], "UPDATE ") === false &&
				   stristr($_REQUEST["search"]["value"], "DELETE ") === false &&
				   stristr($_REQUEST["search"]["value"], "GRANT ") === false &&
				   stristr($_REQUEST["search"]["value"], "CHANGE ") === false)
				{} else {
					$_REQUEST["search"]["value"] = "";
				}
			}

			// Salva busca em sessão
			$_SESSION[$sessVar]["search"] = $_REQUEST["search"]["value"];

			// Processa busca
			if(trim($_REQUEST["search"]["value"]) != "") {
				$bQuebra = true;

				// Verifica se é para pegar valor inteiro
				if(strpos($_REQUEST["search"]["value"], "[") !== false && strpos($_REQUEST["search"]["value"], "]") !== false) {
					$bQuebra = false;
					$_REQUEST["search"]["value"] = str_replace("[","",str_replace("]","",$_REQUEST["search"]["value"]));
				}

				// Quebra search pelos espaços, para comparar cada valor
				if($bQuebra) {
					$arrySearch = explode(" ", $_REQUEST["search"]["value"]);
				} else {
					$arrySearch = array($_REQUEST["search"]["value"]);
				}
				foreach($arrySearch as $searchVal) {
					$sAux = "";
					if(trim($sqlWhere) != "") {$sAux .= " AND ";}
					$sAux .= "(";
					$iCountAux = 0;
					
					// Verifica se tem comparador
					$arrayComp = explode("=", $searchVal);
					if(is_array($arrayComp) && sizeof($arrayComp) > 1) {
						$sAux .= $arrayComp[0] . " LIKE \"%" . output_decode($db->escape_string($arrayComp[1])) . "%\" OR " . $arrayComp[0] . " LIKE \"%" . $db->escape_string(htmlentities($arrayComp[1])) . "%\" ";
						$iCountAux++;
					} else {
						// Percorre todos campos
						for($w=0; $w<sizeof($array_campos); $w++) {
							$campoAux = $array_campos[$w];
							if(strtoupper(trim($campoAux)) != "NULL" &&
							   trim($campoAux) != "") {
								if(trim($array_foreign_keys[$w]) != "") {
									$campoAux = "aux" . intval($this->controller->get_foreign_key_index($campoAux)) . "." . $array_foreign_keys[$w];
								} else if(trim($array_where_filtros[$w]) != "") {
									$campoAux = $array_where_filtros[$w];
								} else if(trim($array_expressoes_sql[$w]) != "") {
									$campoAux = $array_expressoes_sql[$w];
								} else {
									if(stristr($array_campos[$w], ".") === false) {
										$campoAux = "principal." . $campoAux;
									}
								}

								if($w>0) {$sAux .= " OR ";}
								$sAux .= $campoAux . " LIKE \"%" . output_decode($db->escape_string($searchVal)) . "%\" OR " . $campoAux . " LIKE \"%" . $db->escape_string(htmlentities($searchVal)) . "%\" ";
								$iCountAux++;
							}
						}

						//Percorre campos específicos
						if($this->controller->mostra_id_registro) {
							if($iCountAux>0) {$sAux .= " OR ";}
							$sAux .= "principal.id LIKE \"%" . output_decode($db->escape_string($searchVal)) . "%\" OR principal.id LIKE \"%" . output_decode($db->escape_string($searchVal)) . "%\" ";
						}
						if($this->controller->mostra_ativo_registro) {
							if($iCountAux>0) {$sAux .= " OR ";}
							$sAux .= "principal.ativo LIKE \"%" . output_decode($db->escape_string($searchVal)) . "%\" OR IF(principal.ativo=1,\"Ativo\",\"Inativo\") LIKE \"%" . output_decode($db->escape_string($searchVal)) . "%\" ";
						}
						if($this->controller->mostra_permalink_registro) {
							if($iCountAux>0) {$sAux .= " OR ";}
							$sAux .= "principal.permalink LIKE \"%" . output_decode($db->escape_string($searchVal)) . "%\" OR principal.permalink LIKE \"%" . $db->escape_string(htmlentities($searchVal)) . "%\" ";
						}
						if($this->controller->mostra_ranking_registro) {
							if($iCountAux>0) {$sAux .= " OR ";}
							$sAux .= "principal.ranking LIKE \"%" . output_decode($db->escape_string($searchVal)) . "%\" ";
						}
						if($this->controller->mostra_views_registro) {
							if($iCountAux>0) {$sAux .= " OR ";}
							$sAux .= "principal.views LIKE \"%" . output_decode($db->escape_string($searchVal)) . "%\" ";
						}
						if($this->controller->mostra_autor_registro) {
							if($iCountAux>0) {$sAux .= " OR ";}
							$sAux .= "aux0.email LIKE \"%" . output_decode($db->escape_string($searchVal)) . "%\" OR aux0.email LIKE \"%" . output_decode($db->escape_string($searchVal)) . "%\" ";
						}
						if($this->controller->mostra_data_registro) {
							if($iCountAux>0) {$sAux .= " OR ";}
							$sAux .= "principal.data_criacao LIKE \"%" . output_decode($db->escape_string($searchVal)) . "%\" OR principal.data_criacao = \"" . output_decode($db->escape_string($searchVal)) . "\" OR principal.data_criacao = \"" . decode_data($db->escape_string($searchVal), true) . "\" OR principal.data_criacao = \"" . decode_data($db->escape_string($searchVal)) . "\" OR DATE_FORMAT(principal.data_criacao, \"%d/%m/%Y %H:%i:%s\") LIKE \"%" . output_decode($db->escape_string($searchVal)) . "%\"";
						}
					}

					$sAux .= ")";
					$sqlWhere .= $sAux;

					unset($campoAux);
				}
			}
			unset($arrSearch, $searchVal);

			//echo $sqlWhere; die(); //DEBUG

			//$iTotalRegs = $this->controller->count_reg("",true,true);
			//$iTotalRegs = $this->controller->count_reg($sqlWhere,true,true);
			$iTotalRegs = $this->controller->count_reg($sqlWhere,true,false);

			$array_reg = $this->controller->get_array($sqlWhere,$sqlOrderBy,$sqlLimit,true,false,"",$array_expressoes_sql,true);
			$sizeof = sizeof($array_reg);

			//$iFilterRegs = $sizeof;
			$iFilterRegs = $iTotalRegs;

			// 19sec test
			//for($t=0;$t<100;$t++)
			{
			$r=0;	
			$iCountLinhas = 0;
			while($r<$sizeof)
			{	
				$contrAux = $array_reg[$r];
				$moduloAux = $contrAux->model;

				$obj = $moduloAux;
				$objAux = $moduloAux;

				$param = $obj->get_var("id");

				$iCountLinhas++;

				if($r > 0) {
					$return .= ",";
				}
				$return .= "[";

				if($this->controller->mostra_selecao_registro)
	            {
					$return .= json_encode("<div class=\"checkbox checkbox-custom\">
	            			<input id=\"checkbox-registro-" . intval($moduloAux->get_var("id")) . "\" class=\"select-reg\" type=\"checkbox\" value=\"" . intval($moduloAux->get_var("id")) . "\" />
	            			<label for=\"checkbox-registro-" . intval($moduloAux->get_var("id")) . "\"></label>
	            		</div>"); // . $sqlOrderBy // DEBUG
	            } else {
	            	$return .= json_encode("&nbsp;");
	            }

	            if($this->controller->mostra_id_registro)
	            {
	            	$return .= ",";
					$return .= json_encode(intval($moduloAux->get_var("id")));
	            }	
		
				$w=0;
				while($w<sizeof($array_campos))
				{     
					if($array_campos[$w] != "ativo" &&
					   $array_campos[$w] != "permalink" &&
					   $array_campos[$w] != "ranking" &&
					   $array_campos[$w] != "views" &&
					   $array_campos[$w] != "usuario_criacao" &&
					   $array_campos[$w] != "usuario_atualizacao" &&
					   $array_campos[$w] != "data_criacao" &&
					   $array_campos[$w] != "data_atualizacao")
					{
						//$valor = $moduloAux->$array_campos[$w];
						$valor = $contrAux->get_var_format($array_campos[$w]);

						// CAMPOS DE REFERENCIAS
						if(is_array($moduloAux->reference_models) && sizeof($moduloAux->reference_models) > 0) {
							foreach($moduloAux->reference_models as $refAux) {
								if($refAux[0] == $array_campos[$w]) {
									$arrAux = explode(";",$valor);
									$strAux = "";
									foreach($arrAux as $idAux) {
										carrega_classe($refAux[1]);
										$objAuxRef = new $refAux[1]();
										$objAuxRef->inicia_dados();
										$objAuxRef->set_var("id",intval($idAux));
										$objAuxRef->carrega_dados();

										$valAux = $objAuxRef->get_var($objAuxRef->view->default_view_field);
										if(trim($objAuxRef->view->default_view_expr) != "") {
											$valAux = eval($objAuxRef->view->default_view_expr);
										}

										if(trim($strAux) != "") {
											$strAux .= " "; //", ";
										}
										//$strAux .= $valAux;
										if(trim($valAux) == "") {
											$strAux .= "<button class=\"btn btn-default btn-xs mb-1 mt-1\">-</button></a>";
										} else {
											$strAux .= "<a rel=\"address\" href=\"#/" . $refAux[1] . "/visualizar/" . intval($idAux) . "\"><button class=\"btn btn-default btn-xs mb-1 mt-1\">" . $valAux . "</button></a>";
										}
									}
									$valor = $strAux;
								}
							}
						}
						
						// CAMPOS ESTRANGEIROS
						if(trim($array_foreign_keys[$w]) <> "")
						{
							$objAuxRef = $moduloAux->get_objeto_referencia($array_campos[$w]);
							if($objAuxRef != null) {
								$valor = $objAuxRef->get_var($array_foreign_keys[$w]);
							}
						}	

						// CAMPOS DE EXPRESSÕES SQL
						if(is_array($array_expressoes_sql) && trim($array_expressoes_sql[$w]) <> "")
						{
							$varAux = "sqlExprField".$w;
							$valor = $moduloAux->get_var($varAux);
						}	
						
						$valor_exib = get_output($valor);
						
						// APLICA EXPRESSOES PHP
						if(trim($array_expressoes[$w]) <> "")
						{
							if(!strstr($array_expressoes[$w],"return"))
							{
								$array_expressoes[$w] = "return " . $array_expressoes[$w];							
							}
							if(substr(trim($array_expressoes[$w]),-1 <> ";")){
								$array_expressoes[$w] = $array_expressoes[$w] . ";";}
							$valor_exib = get_output(eval($array_expressoes[$w]));
						}

						$return .= ",";
						$return .= json_encode(stripslashes($valor_exib));
	                }
					$w++;
				}
				
				if($this->controller->mostra_ranking_registro)
	            {
	            	$return .= ",";
					$return .= json_encode(get_output($moduloAux->get_var_format("ranking")));
	            }	
				
				if($this->controller->mostra_permalink_registro)
	            {
					$valAux = get_output($moduloAux->get_var_format("permalink"));
					$urlAux = ROOT_SERVER . ROOT . ROOT_SITE . $valAux;

					$return .= ",";
					$return .= json_encode("<a href=\"" . $urlAux. "\">" . $valAux . "</a>");

					unset($urlAux);
	            }												
				
				if($this->controller->mostra_views_registro)
	            {
	            	$return .= ",";
					$return .= json_encode(get_output($moduloAux->get_var_format("views")));
	            }

				if($this->controller->mostra_autor_registro)
	            {
	            	$return .= ",";
					if(intval($moduloAux->get_var("usuario_criacao")) > 0) {
						$objAuxRef = $moduloAux->get_objeto_referencia("usuario_criacao");
						$return .= json_encode(get_output($objAuxRef->get_var("email")));
					} else {
						$return .= json_encode("suporte@uzzye.com");
					}
	            }
	             
	            if($this->controller->mostra_data_registro)
	            {
	            	$dataAux = $moduloAux->get_var("data_criacao");
	            	/*if(trim($moduloAux->get_var("data_atualizacao")) != "") {
	                	if(trim(str_replace(":","",str_replace("-","",$moduloAux->get_var("data_criacao")))) >
	                	   trim(str_replace(":","",str_replace("-","",$moduloAux->get_var("data_atualizacao"))))) {
	                		$dataAux = $moduloAux->get_var("data_atualizacao");
	                	}
	                }*/

	                $return .= ",";
	            	$return .= json_encode(get_output(encode_data($dataAux,true)));

	            	unset($dataAux, $strAux);
	            }												
				
				if($this->controller->mostra_ativo_registro)
	            {
	            	$return .= ",";
					if(intval($moduloAux->get_var("ativo")) == 1) {
						$return .= json_encode(get_lang("_ATIVO"));
					} else {
						$return .= json_encode(get_lang("_INATIVO"));
					}
	            }

	        // ACOES

	            $sizeof_ac = sizeof($this->array_acoes_cms);

	            $str_acoes = "";
	             
	            for($a=0;$a<$sizeof_ac;$a++)
	            {   
	            	$acao_aux = str_replace("#PARAM#",$param,$this->array_acoes_cms[$a]) . " ";
	            	if(intval($moduloAux->get_var("ativo")) == 1) {
	            		$acao_aux = str_replace("#TOGGLE_ATIVO#","toggle-on text-success",$acao_aux);
	            	} else {
	                	$acao_aux = str_replace("#TOGGLE_ATIVO#","toggle-off text-danger",$acao_aux);
	                }

	                if(trim($this->array_expr_acoes_cms[$a]) != "") {
	            		$acao_aux = str_replace("#TOGGLE_CLASS#",@eval($this->array_expr_acoes_cms[$a]),$acao_aux);
	            	}
	            	
	            	if($this->controller->valida_permissao($modulo,$this->view->array_perms_acoes_cms[$a],false)){
						//if(trim($this->array_perms_acoes_cms[$a]) != "Visualizar" || !$this->controller->valida_permissao($modulo,"Editar",false))
						if(trim($this->array_perms_acoes_cms[$a]) != "Visualizar" || !in_array("Editar", $this->array_perms_acoes_cms))
						{
	            		
	                		$b_exibe_acao = true;   
		            		if(trim($array_condicoes_acoes[$a])<>"")
	    	        		{
	        	    			if(!strstr($array_condicoes_acoes[$a],"return"))
	            				{
	            					$array_condicoes_acoes[$a] = "return " . $array_condicoes_acoes[$a];
	            				}
	            				if(substr(trim($array_condicoes_acoes[$a]),-1 <> ";")){
									$array_condicoes_acoes[$a] = $array_condicoes_acoes[$a] . ";";}
	            				$b_exibe_acao = eval($array_condicoes_acoes[$a]);
	            			}	
	            	
	            			if($b_exibe_acao)
	            			{
	            				$str_acoes .= $acao_aux;
	            			}
						}
	            	}                 	                
	            }

	            if(trim($str_acoes) != "" && $b_exibe_acao) {
	            	$return .= ",";
					//$return .= "\"null.acoes\":";
					$return .= json_encode($str_acoes);
	            }
				
				$return .= "]";

	            unset($obj,$objAux,$objAuxRef);

				$r++;
			}}
		}

		$return .= "], \"draw\": " . intval($_REQUEST["draw"]) . ", \"recordsTotal\": " . intval($iTotalRegs) . ", \"recordsFiltered\": " . intval($iFilterRegs) . " ";
		//$return .= ", \"search\": " . json_encode($_REQUEST["search"]["value"]) . " "; // DEBUG
		//$return .= ", \"where\": " . json_encode($sqlWhere) . " "; // DEBUG
	    $return .= "}";

		echo $return;
	}

	function get_registros_html($array_campos,$array_nomes,$array_expressoes = "",$sqlWhere = "",$sqlOrderBy = "",$array_foreign_keys = "",$array_where_filtros = "",$array_condicoes_acoes = "",$array_expressoes_sql = "") {
		global $modulo, $mySQL_prefix;
										
		$array_reg = $this->controller->get_array($sqlWhere,$sqlOrderBy,"",true,false,"",$array_expressoes_sql,true);
		$sizeof = sizeof($array_reg);

		// 19sec test
		//for($t=0;$t<100;$t++)
		{
		$r=0;	
		$iCountLinhas = 0;
		while($r<$sizeof)
		{	
			$contrAux = $array_reg[$r];
			$moduloAux = $contrAux->model;

			$obj = $moduloAux;
			$objAux = $moduloAux;

			$param = $obj->get_var("id");

			$iCountLinhas++;

			?>
            <tr>
                <?php
				
				if($this->controller->mostra_selecao_registro)
                {
					?>
                	<td class="col-selecao">
						<div class="checkbox checkbox-custom">
                			<input id="checkbox-registro-<?php echo intval($moduloAux->get_var("id")); ?>" class="select-reg" type="checkbox" value="<?php echo intval($moduloAux->get_var("id")); ?>" />
                			<label for="checkbox-registro-<?php echo intval($moduloAux->get_var("id")); ?>"></label>
                		</div>
                	</td>
                	<?php
                } else {
                	?>
                	<td class="col-selecao">
                		&nbsp;
                	</td>
                	<?php
                }

                if($this->controller->mostra_id_registro)
                {
					?>
                	<td class="col-id">
					<?php echo intval($moduloAux->get_var("id")); ?>
                	</td>
                	<?php
                }	
		
				$w=0;
				while($w<sizeof($array_campos))
				{     
					if($array_campos[$w] != "ativo" &&
					   $array_campos[$w] != "permalink" &&
					   $array_campos[$w] != "ranking" &&
					   $array_campos[$w] != "views" &&
					   $array_campos[$w] != "usuario_criacao" &&
					   $array_campos[$w] != "usuario_atualizacao" &&
					   $array_campos[$w] != "data_criacao" &&
					   $array_campos[$w] != "data_atualizacao")
					{
						//$valor = $moduloAux->$array_campos[$w];
						$valor = $contrAux->get_var_format($array_campos[$w]);

						// CAMPOS DE REFERENCIAS
						if(is_array($moduloAux->reference_models) && sizeof($moduloAux->reference_models) > 0) {
							foreach($moduloAux->reference_models as $refAux) {
								if($refAux[0] == $array_campos[$w]) {
									$arrAux = explode(";",$valor);
									$strAux = "";
									foreach($arrAux as $idAux) {
										$objAuxRef = new $refAux[1]();
										$objAuxRef->inicia_dados();
										$objAuxRef->set_var("id",intval($idAux));
										$objAuxRef->carrega_dados();

										$valAux = $objAuxRef->get_var($objAuxRef->view->default_view_field);
										if(trim($objAuxRef->view->default_view_expr) != "") {
											$valAux = eval($objAuxRef->view->default_view_expr);
										}

										if(trim($strAux) != "") {
											$strAux .= " "; //", ";
										}
										//$strAux .= $valAux;
										if(trim($valAux) == "") {
											$strAux .= "<button class=\"btn btn-default btn-xs mb-1 mt-1\">-</button></a>";
										} else {
											$strAux .= "<a rel=\"address\" href=\"#/" . $refAux[1] . "/visualizar/" . intval($idAux) . "\"><button class=\"btn btn-default btn-xs mb-1 mt-1\">" . $valAux . "</button></a>";
										}
									}
									$valor = $strAux;
								}
							}
						}
						
						// CAMPOS ESTRANGEIROS
						if(trim($array_foreign_keys[$w]) <> "")
						{
							$objAuxRef = $moduloAux->get_objeto_referencia($array_campos[$w]);
							$valor = $objAuxRef->$array_foreign_keys[$w];
						}	

						// CAMPOS DE EXPRESSÕES SQL
						if(is_array($array_expressoes_sql) && trim($array_expressoes_sql[$w]) <> "")
						{
							$varAux = "sqlExprField".$w;
							$valor = $moduloAux->get_var($varAux);
						}	
						
						$valor_exib = get_output($valor);
						
						// APLICA EXPRESSOES PHP
						if(trim($array_expressoes[$w]) <> "")
						{
							if(!strstr($array_expressoes[$w],"return"))
							{
								$array_expressoes[$w] = "return " . $array_expressoes[$w];							
							}
							if(substr(trim($array_expressoes[$w]),-1 <> ";")){
								$array_expressoes[$w] = $array_expressoes[$w] . ";";}
							$valor_exib = get_output(eval($array_expressoes[$w]));
						}
						
						?>
	                	<td class="col-<?php echo $array_campos[$w]; ?>"><?php echo stripslashes($valor_exib)?></td>
	                    <?php
	                }
					$w++;
				}
				
				if($this->controller->mostra_ranking_registro)
                {
					?>
                	<td class="col-ranking">
					<?php echo get_output($moduloAux->get_var_format("ranking")); ?>
                	</td>
                	<?php
                }	
				
				if($this->controller->mostra_permalink_registro)
                {
					?>
                	<td class="col-permalink">
					<?php

					$valAux = get_output($moduloAux->get_var_format("permalink"));
					$urlAux = ROOT_SERVER . ROOT . ROOT_SITE . $valAux;

					echo "<a href=\"" . $urlAux. "\">" . $valAux . "</a>";

					unset($urlAux);

					?>
                	</td>
                	<?php
                }												
				
				if($this->controller->mostra_views_registro)
                {
					?>
                	<td class="col-views">
					<?php echo get_output($moduloAux->get_var_format("views")); ?>
                	</td>
                	<?php
                }

				if($this->controller->mostra_autor_registro)
                {
					?>
                	<td class="col-usuario_criacao">
					<?php 
					if(intval($moduloAux->get_var("usuario_criacao")) > 0) {
						$objAuxRef = $moduloAux->get_objeto_referencia("usuario_criacao");
						echo get_output($objAuxRef->get_var("email"));
					} else {
						echo "suporte@uzzye.com";
					}
					?>
                	</td>
                	<?php
                }
                 
                if($this->controller->mostra_data_registro)
                {
                	$dataAux = $moduloAux->get_var("data_criacao");
                	if(trim($moduloAux->get_var("data_atualizacao")) != "") {
	                	if(trim(str_replace(":","",str_replace("-","",$moduloAux->get_var("data_criacao")))) >
	                	   trim(str_replace(":","",str_replace("-","",$moduloAux->get_var("data_atualizacao"))))) {
	                		$dataAux = $moduloAux->get_var("data_atualizacao");
	                	}
	                }

                	?>
                	<td class="col-data_criacao"><?php echo get_output(encode_data($dataAux,true))?></td>
                	<?php 

                	unset($dataAux);
                }												
				
				if($this->controller->mostra_ativo_registro)
                {
					?>
                	<td class="col-ativo">
					<?php
					if(intval($moduloAux->get_var("ativo")) == 1) {
						echo get_lang("_ATIVO");
					} else {
						echo get_lang("_INATIVO");
					}
					?>
                	</td>
                	<?php
                }

                ?>
                <td class="text-nowrap">
                <?php
                
                $sizeof_ac = sizeof($this->array_acoes_cms);
                 
                for($a=0;$a<$sizeof_ac;$a++)
                {   
                	$acao_aux = str_replace("#PARAM#",$param,$this->array_acoes_cms[$a]) . " ";
                	if(intval($moduloAux->get_var("ativo")) == 1) {
                		$acao_aux = str_replace("#TOGGLE_ATIVO#","toggle-on text-success",$acao_aux);
                	} else {
	                	$acao_aux = str_replace("#TOGGLE_ATIVO#","toggle-off text-danger",$acao_aux);
	                }

	                if(trim($this->array_expr_acoes_cms[$a]) != "") {
                		$acao_aux = str_replace("#TOGGLE_CLASS#",@eval($this->array_expr_acoes_cms[$a]),$acao_aux);
                	}
                	
                	if($this->controller->valida_permissao($modulo,$this->array_perms_acoes_cms[$a],false)){
						//if(trim($this->array_perms_acoes_cms[$a]) != "Visualizar" || !$this->controller->valida_permissao($modulo,"Editar",false))
						if(trim($this->array_perms_acoes_cms[$a]) != "Visualizar" || !in_array("Editar", $this->array_perms_acoes_cms))
						{
                		
	                		$b_exibe_acao = true;   
    	            		if(trim($array_condicoes_acoes[$a])<>"")
        	        		{
            	    			if(!strstr($array_condicoes_acoes[$a],"return"))
                				{
                					$array_condicoes_acoes[$a] = "return " . $array_condicoes_acoes[$a];
                				}
                				if(substr(trim($array_condicoes_acoes[$a]),-1 <> ";")){
									$array_condicoes_acoes[$a] = $array_condicoes_acoes[$a] . ";";}
                				$b_exibe_acao = eval($array_condicoes_acoes[$a]);
                			}	
                	
                			if($b_exibe_acao)
                			{
                				echo $acao_aux;
                			}
						}
                	}                 	                
                }
                ?>
                </td>
                <?php

                ?>
            </tr>   
            <?php

            unset($obj,$objAux,$objAuxRef);

			$r++;
		}}
	}
	
	// Se quiser fazer ações diferentes de formulário, ou formulários exclusivos, copiar esta função para uma nova e apenas alterar 
	function monta_formulario($id = "",$readonly = false,$clone = false,$resumido = false)
	{
		global $modulo;
				
		$b_editando = false;

		if(!$resumido) {
			$this->heading();
			?>
		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view">
				<?php
		}
		
		$titulo = get_lang("_INCLUIR");
		if(trim($id) <> "")
		{
			$titulo = get_lang("_EDITAR");
		}
		
		if($readonly){
			$titulo = get_lang("_VISUALIZAR");
		}

		if(trim($this->titulo_cadastro == "")) {
			$this->titulo_cadastro = ($titulo) . " " . ($this->nome_exibicao_singular) . "";
		}

		if($clone){
			if(trim($id) <> "")
			{
				$this->controller->set_var("id",$id);
				$this->controller->carrega_dados();
			}
			
			$this->topo_cadastro("", $readonly, $resumido);		
			$this->monta_campos_form($id, $readonly, $clone, $resumido);
			$this->formulario_cadastro($this->array_form_campos, $this->array_form_refs, $readonly, $resumido, $this->array_form_campos_padrao);		
			$this->rodape_cadastro("", $readonly, $resumido);
		} else {
			if(trim($id) <> "")
			{
				$this->controller->set_var("id",$id);
				$this->controller->carrega_dados();
				$b_editando = true;
			}
			
			$this->topo_cadastro($id,$readonly, $resumido);
			$this->monta_campos_form($id, $readonly, $clone, $resumido);
			$this->formulario_cadastro($this->array_form_campos, $this->array_form_refs, $readonly, $resumido, $this->array_form_campos_padrao);		
			$this->rodape_cadastro($id, $readonly, $resumido);
		}
		if(!$resumido) {
		?>
				</div>
			</div>
		</div>
		<?php
		}
	}
	
	function topo_cadastro($id = "", $readonly = false, $resumido = false, $postURL = null, $returnURL = null, $acaoValue = null, $ajax = true)
	{
		global $modulo, $acao;

		if(trim($acaoValue) == "" || $acaoValue == null) {
			$acaoValue = "do_" . $acao;
		}
		if($returnURL == null) {
			$returnURL = ROOT_SERVER . ROOT . "ajax/" . $modulo;
		}
		if($postURL == null) {
			$postURL = ROOT_SERVER . ROOT . "ajax/" . $modulo . "/" . $acaoValue;
		}
		
		$hidden_id = "";
		if(trim($id) <> "")
		{
			$hidden_id = "<input type='hidden' name='id' id='id' value='" . $id . "'>";
		}
		
		if($readonly){
			$hidden_id = "";
		}
		
		if(!$resumido) {
		?>  
		<div class="panel-heading">
			<div class="pull-left">
				<h6 class="panel-title txt-dark"><?php echo $this->titulo_cadastro; ?></h6>
			</div>
			
			<div class="clearfix"></div>
		</div>

		<div class="panel-wrapper collapse in">
			<div class="panel-body">
				<?php /*<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae ultrices accumsan. Aliquam ornare lacus adipiscing, posuere lectus et, fringilla augue.</p>*/ ?>
				<div class="row mb-20">
		<?php 
		} 
		?>
					<div class="form-wrap">
        <?php

        ?>
		<script type="text/javascript">
			$(document).ready(function(e){
				if($("#form_<?php echo $modulo; ?><?php if($_REQUEST["resumido"] == "true" || $resumido){echo "_resumido_ajax";}?>").find("[autofocus=\"true\"]").length > 0){
   					$("#form_<?php echo $modulo; ?><?php if($_REQUEST["resumido"] == "true" || $resumido){echo "_resumido_ajax";}?>").find("[autofocus=\"true\"]").eq(0).focus();
   				}
   				if($("#botao_submit_padrao_form_mais").length > 0){
   					$("#botao_submit_padrao_form_mais").unbind("click").click(function(e){
   						$("#form_<?php echo $modulo; ?><?php if($_REQUEST["resumido"] == "true" || $resumido){echo "_resumido_ajax";}?>").append("<input type=\"hidden\" name=\"add_mais\" id=\"add_mais\" value=\"1\" />").submit();
   					});
   				}
   			});
   		</script>

        <form
        	name="form_cadastro_<?php echo $modulo; ?><?php if($_REQUEST["resumido"] == "true" || $resumido){echo "_resumido_ajax";}?>"
        	id="form_<?php echo $modulo; ?><?php if($_REQUEST["resumido"] == "true" || $resumido){echo "_resumido_ajax";}?>"
        	method="POST"
        	action="<?php if(!$ajax) {echo $postURL;} else {echo "javascript:void(0);";} ?>"
        	class="form-cadastro-padrao<?php if(!$ajax) {echo " no-ajax";} ?>"
        	enctype="multipart/form-data"
        	data-url-post="<?php echo $postURL; ?>"
        	data-url-return="<?php echo $returnURL; ?>"
        >
        	<div class="form-body">
	        	<input type="hidden" name='acao' id='acao' value='do_<?php echo $acao; ?>' />
	        	<input type="hidden" name='randcod' class="randcod" id='randcod' value='<?php echo gera_senha(); ?>' />
		        <?php
		        if($_REQUEST["resumido"] == "true" || $resumido){
		        	?><input type="hidden" name='resumido' id='resumido' value='true' /><?php 
		        } 
				echo $hidden_id;
	}
	
    function formulario_cadastro($array_form_campos,$array_form_refs, $readonly = false, $resumido = false, $array_form_campos_padrao = null)
	{
		global $modulo;

		$open = false;

	// Array de campos do formulário
		$w = 0;
		$type_prev = "";
		$label_prev = "";
		while($w<sizeof($array_form_campos))
		{
			$campo = $array_form_campos[$w];
			if($campo != "id" &&
			   $campo != "ativo" &&
			   $campo != "permalink" &&
			   $campo != "ranking" &&
			   $campo != "views" &&
			   $campo != "usuario_criacao" &&
			   $campo != "usuario_atualizacao" &&
			   $campo != "data_criacao" &&
			   $campo != "data_atualizacao")
			{
				if($readonly){
					$campo->readonly = true;
				}
				$type = $campo->type;
				$label = $campo->label;
				
				if($type_prev <> $type || ($type == "checkbox" && $campo->fixed_pos))
				{
					if(trim($type_prev) <> "" && ($type <> "hidden"))
					{
						if($open) {
							/*?></div>
							</div><?php*/
							$open = false;
						}
	                }				
					
					$style_class = "formulario";
					$sub_display = "";

					if($type <> "hidden")
					{
						/*?><div class="row">
							<div class="col-sm-12 <?php echo $style_class; ?>"><?php*/
						$open = true;
					}
					
					if($label_prev <> $label && ($type <> "hidden") && trim($campo->sub_label <> ""))
					{
						echo ($sub_display);
					}
				}

				if(isset($_REQUEST["status_operacao"]) && $_REQUEST["status_operacao"] == 0){
					$campo->set_value($_SESSION["_POST_".$modulo][$campo->name]);
				}
				
				echo $campo->get_display_field();
				
				$type_prev = $type;
				$label_prev = $label;
			}
			
			$w++;
		}
		if($open) {
			/*?></div>
			</div><?php*/
			$open = false;
		}
		
	// Array de campos de referência
		$w = 0;
		$type_prev = "";
		$label_prev = "";
		while($w<sizeof($array_form_refs))
		{
			$campo = $array_form_refs[$w];
			if($campo != "id" &&
			   $campo != "ativo" &&
			   $campo != "permalink" &&
			   $campo != "ranking" &&
			   $campo != "views" &&
			   $campo != "usuario_criacao" &&
			   $campo != "usuario_atualizacao" &&
			   $campo != "data_criacao" &&
			   $campo != "data_atualizacao")
			{
				$type = $campo->type;
				$label = $campo->label;
				
				if($type_prev <> $type || ($type == "checkbox" && $campo->fixed_pos))
				{
					if(trim($type_prev) <> "" && ($type <> "hidden"))
					{
						if($open) {
							/*?></div>
							</div><?php*/
							$open = false;
						}
	                }				
					
					$style_class = "formulario";
					$sub_display = "";
					
					if($type <> "hidden")
					{
						/*?><div class="row">
							<div class="col-sm-12 <?php echo $style_class; ?>"><?php*/
						$open = true;
					}
					
					if($label_prev <> $label && ($type <> "hidden") && trim($campo->sub_label <> ""))
					{
						echo ($sub_display);
					}
				}
						
				echo $campo->get_display_field();
				
				$type_prev = $type;
				$label_prev = $label;
			}
			
			$w++;
		}
		if($open) {
			/*?></div>
			</div><?php*/
			$open = false;
		}

	// Array de campos padrão

		if(sizeof($array_form_campos_padrao) > 0) {
			?>
			<div class="col-sm-12 mt-20">
				<h6 class="txt-dark capitalize-font"><i class="icon-pin mr-10"></i><?php echo get_lang("_CAMPOS_PADRAO"); ?></h6>
				<hr/>
			</div>
			<?php

			$w = 0;
			$type_prev = "";
			$label_prev = "";
			while($w<sizeof($array_form_campos_padrao))
			{
				$campo = $array_form_campos_padrao[$w];
				
				if($readonly){
					$campo->readonly = true;
				}
				$type = $campo->type;
				$label = $campo->label;
				
				if($type_prev <> $type || ($type == "checkbox" && $campo->fixed_pos))
				{
					if(trim($type_prev) <> "" && ($type <> "hidden"))
					{
						if($open) {
							/*?></div>
							</div><?php*/
							$open = false;
						}
	                }				
					
					$style_class = "formulario";
					$sub_display = "";
					
					if($type <> "hidden")
					{
						/*?><div class="row">
							<div class="col-sm-12 <?php echo $style_class; ?>"><?php*/ 
						$open = true;
					}
					
					if($label_prev <> $label && ($type <> "hidden") && trim($campo->sub_label <> ""))
					{
						echo ($sub_display);
					}
				}

				if(isset($_REQUEST["status_operacao"]) && $_REQUEST["status_operacao"] == 0){
					$campo->set_value($_SESSION["_POST_".$modulo][$campo->name]);
				}
				
				echo $campo->get_display_field();
				
				$type_prev = $type;
				$label_prev = $label;
				
				$w++;
			}
			if($open) {
				/*?></div>
				</div><?php*/
				$open = false;
			}
		}
	}
	
	function rodape_cadastro($id = "",$readonly = false, $resumido = false, $titulo = "")
	{
		global $modulo;	

		if(trim($titulo) == "") {
			$titulo = get_lang("_SALVAR");
		}

		?>
		<div class="col-sm-12 col-xs-12 form-btns">
		<?php

    	/*if($titulo == get_lang("_INCLUIR") && $_REQUEST["resumido"] <> "true"){?>
    		<input type="submit" id='botao_submit_padrao_form_mais' class="btnPrimario corBase" value="<?php echo ($titulo);?> +"/>
    		<?php
    	}*/

    	if(!$readonly && trim($_REQUEST["resumido"]) <> "true" && !$resumido){?>
    		<button type="submit" id="botao_submit_<?php echo $modulo; ?>"" class="btn btn-success btn-submit btn-anim"><i class="icon-rocket"></i> <span class="btn-text" data-label="<?php echo ($titulo);?>"><?php echo ($titulo);?></span></button>
    		<?php
    	}

    	/*if(trim($_REQUEST["resumido"]) <> "true"){
    		?><input type="button" class="btnPrimario corBase" value="<?php echo get_lang("_VOLTAR"); ?>" onclick="window.location='<?php echo ROOT_CMS; ?>index.php?modulo=<?php echo $modulo;?>';" /><?php 
    	}*/

        ?>
        </div>
    						</div>
						</form>
					</div>
		<?php if(!$resumido) { ?>
				</div>
			</div>
		</div>
        <?php
       	}
	}

	function heading() {
		global $modulo, $projectColors;

        $modAux = new cms_modulos();
        $modAux = $modAux->localiza(array("link"),array($modulo));
        $iconAux = "";
        if(is_array($modAux) && sizeof($modAux) > 0) {
        	$modAux = $modAux[0];
        	$iconAux = "<i class=\"" . $modAux->get_var("icon_class") . "\"></i> ";
        }
		?>
		<!-- Title -->
        <div class="row heading-bg bg-<?php echo $projectColors["main"]; ?>" style="<?php
		if(trim($modAux->get_var("cor")) != "") {
			echo " background-color: " . trim($modAux->get_var("cor")) . "!important; ";
		}
        ?>">
            <?php

            $strNavegacao = $this->controller->get_bread_crumbs();

            ?>
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-light">
                	<?php echo $iconAux; ?><span class="btn-text"><?php echo ($this->nome_exibicao); ?></span>
                </h5>
            </div>

            <!-- Breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                	<?php echo $strNavegacao; ?>
                </ol>
            </div>
            <!-- /Breadcrumb -->
        
        </div>
        <!-- /Title -->
        <?php
	}

	function get_var($var = "") {
		return $this->controller->get_var($var);
	}

	function set_var($var = "", $value = "") {
		return $this->controller->set_var($var,$value);
	}

	function get_var_format($var = "") {		
		$valor = $this->controller->get_var($var);

		// Fazer para reference_items?

		// CAMPOS DE MÁSCARAS DE NÚMEROS
		if(is_array($this->number_masks) && sizeof($this->number_masks) > 0) {
			$mask = $this->number_masks[$var];
			if(is_array($mask) && sizeof($mask) > 0) {
				$valor = $mask[3] . number_format($valor,$mask[0],$mask[1],$mask[2]) . $mask[4];
			}
		}

		// CAMPOS DE MÁSCARAS DE DATA
		if(is_array($this->date_masks) && sizeof($this->date_masks) > 0) {
			if(array_key_exists($var, $this->date_masks)) {
				$mostra_hora = $this->date_masks[$var];
				$valor = encode_data($valor,$mostra_hora);
			}
		}

		// CAMPOS DE MÁSCARAS DE COR
		if(is_array($this->color_masks) && sizeof($this->color_masks) > 0) {
			if(array_key_exists($var, $this->color_masks)) {
				$tipo_cor = $this->color_masks[$var];
				if($tipo_cor == "hexa" || $tipo_cor == "") {
					$valor = "<span class=\"btn-text\" style=\"color: " . $valor . "; text-shadow: 1px 0px 0px #aaa;\">" . $valor . "</span>";
				}
			}
		}

		// CAMPOS CUSTOMIZADOS
		if(is_array($this->custom_expr_masks) && sizeof($this->custom_expr_masks) > 0) {
			if(array_key_exists($var, $this->custom_expr_masks)) {
				$expr = $this->custom_expr_masks[$var];
				$valor = eval($expr);
			}
		}

		return $valor;
	}

	function get_nome_tabela()
	{
		return $this->controller->get_nome_tabela();
	}

	function get_upload_folder($var = "")
	{
		return $this->controller->get_upload_folder($var);
	}

	function monta_exportacao($id = null, $tipo = "") {
		global $modulo;

		if($id == null) {
			$id = $_REQUEST["id"];
		}

		if(trim($tipo) == "") {
			$tipo = "csv";
		}
			
		$this->heading();

		?>
		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view">
				<?php

				$b_editando = false;

				$this->titulo_cadastro = "<i class=\"icon-cloud-download mr-10\"></i>" . get_lang("_EXPORTAR") . " " . get_lang(strtoupper($tipo));

				$this->topo_cadastro($id, false, false, null, null, "do_exportar", false);
				
				$ref = "tipo";
		        $inputAux = new Uzzye_HiddenField();
				$inputAux->label = get_lang("_TIPO");
				$inputAux->name = $ref;
				$inputAux->id = $ref;
				$inputAux->set_value($tipo);
				$inputAux->required = false;
				echo $inputAux->get_display_field();

				$ref = "data_inicio";
		        $inputAux = new Uzzye_DateField();
				$inputAux->label = get_lang("_DATA_INICIO");
				$inputAux->name = $ref;
				$inputAux->id = $ref;
				$inputAux->li_class = 'w50p';
				$inputAux->required = false;
				echo $inputAux->get_display_field();

				$ref = "data_fim";
		        $inputAux = new Uzzye_DateField();
				$inputAux->label = get_lang("_DATA_FIM");
				$inputAux->name = $ref;
				$inputAux->id = $ref;
				$inputAux->li_class = 'w50p';
				$inputAux->required = false;
				echo $inputAux->get_display_field();

				$this->rodape_cadastro($id, false, false, get_lang("_EXPORTAR"));

				?>
				</div>
			</div>
		</div>
		<?php
	}

	function get_view_field_value() {
		$val = "";
		if(trim($this->default_view_expr) != ""){
			$val = @eval($this->default_view_expr);
		}
		else if(trim($this->default_view_field) != ""){
			$val = $this->get_var_format($this->default_view_field);
		}
		return $val;
	}

	function get_id_field_value() {
		$val = "";
		if(trim($this->default_value_expr) != ""){
			$val = @eval($this->default_value_expr);
		}
		else if(trim($this->default_value_field) != ""){
			$val = $this->get_var_format($this->default_value_field);
		}
		return $val;
	}
}