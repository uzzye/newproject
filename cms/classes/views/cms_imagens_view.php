<?php

class cms_imagens_view extends view
{	  
    function __construct($owner = null)
	{
		$this->nome = "cms_imagens";
		if(function_exists("get_lang")) {
			$this->nome_exibicao = get_lang("_IMAGENS");
			$this->nome_exibicao_singular = get_lang("_IMAGEM");
		}
		
		parent::__construct($owner);	

		$this->custom_expr_masks["arquivo"] = "return(\"<img src='\" . \$this->get_upload_folder(\"arquivo\") . \$valor . \"' border='0' width='100' />\");";
    }	
	
	function monta_campos_form($id = "", $readonly = false, $clone = false, $resumido = false)
	{
		global $modulo;
		
		$b_editando = false;
		$id="";
		if(trim($_REQUEST["id"]) <> "")
		{
			$id = $_REQUEST["id"];
			$this->controller->set_var("id",$id);
			$this->controller->carrega_dados();
			$b_editando = true;
		}
		
		$array_form_campos = array();
		$array_form_refs = array();
		
		$count_radio = 0;
		$count_text = 0;
		$count_check = 0;
		$count_refs = 0;
		$count_combo = 0;
		
	// CAMPO NOME
		$ref = "titulo";
		$inputAux = new Uzzye_TextField();
		$inputAux->label = get_lang("_TITULO");
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->li_class = 'w100p';
		$inputAux->set_value($this->controller->get_var($ref));
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_text++;
		
		$ref = "modulo_registro";
		$inputAux = new Uzzye_TextField();
		$inputAux->label = get_lang("_MODULO_REGISTRO");
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->li_class = 'w66p';
		$inputAux->set_value($this->controller->get_var($ref));	
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_text++;
		
		$ref = "id_registro";
		$inputAux = new Uzzye_TextField();
		$inputAux->label = get_lang("_ID_REGISTRO");
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->li_class = 'w33p';
		$inputAux->set_value($this->controller->get_var($ref));
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;		
		$count_text++;

		$ref = "campo_registro";
		$inputAux = new Uzzye_HiddenField();
		$inputAux->label = get_lang("_CAMPO_REGISTRO");
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->li_class = 'w33p';
		$inputAux->set_value($this->controller->get_var($ref));
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;		
		$count_text++;
	
	// CAMPO IMAGEM 		
		$ref = "crop_arquivo";
		$inputAux = new Uzzye_ImageCrop();		
		$inputAux->label = get_lang("_ARQUIVO");
		$inputAux->name = $ref;
		$inputAux->id = $ref;
		$inputAux->li_class = 'w100p';
		/*$inputAux->crops = array(
			array("arquivo",get_lang("_CROP")),
		);*/
		$inputAux->array_crop_fields = array(
			array("arquivo",get_lang("_CROP"),$this->controller->get_var("arquivo"))	
		);
		$array_form_campos[sizeof($array_form_campos)] = $inputAux;
		$count_text++;
		
	// FINALIZACAO
		$this->array_form_campos = $array_form_campos;	
		$this->array_form_refs = $array_form_refs;
	}
}