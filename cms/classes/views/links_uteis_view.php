<?php    
include_once(ROOT_CMS . "classes/views/idiomas_view.php");
	
class links_uteis_view extends view
{	  
		function __construct($owner = null)
		{
			$this->nome = "links_uteis";
			if(function_exists("get_lang")) {
				$this->nome_exibicao = get_lang("_LINKS_UTEIS");
				$this->nome_exibicao_singular = get_lang("_LINK");
			}
		
			parent::__construct($owner);		
		}	
	
		function monta_campos_form($id = "", $readonly = false, $clone = false, $resumido = false)
		{
			global $modulo;
		
			$b_editando = false;		
			if(trim($_REQUEST["id"]) <> "")
			{
				$id = $_REQUEST["id"];
				$this->controller->set_var("id",$id);
				$this->controller->carrega_dados();
				$b_editando = true;
			}
		
			$array_form_campos = array();
		
			$count_fields = 0;
					
			$ref = "referencia";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_REFERENCIA"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;	

			$ref = "titulo";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_TITULO"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;
				
				$ref = "url";
			$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_URL"));
			$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->max_length = null;
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;	
				
				$ref = "target";
			$checked = $this->controller->get_var($ref);
			if(trim($_REQUEST[$ref]) <> "")
			{$checked = $_REQUEST[$ref];}
			$inputAux = new Uzzye_ComboBox($ref,$ref,get_lang("_TARGET"));
			$inputAux->default_display = get_lang("_COMBOSELECIONE");
			$inputAux->default_value = "";
			$inputAux->checked_value = $checked;
			$inputAux->size = 1;
			$inputAux->set_options(array(array(output_decode(""),output_decode("")),array(output_decode("_blank"),output_decode("_blank"))));
						$inputAux->set_value($this->controller->get_var($ref));
			$inputAux->li_class = "w50p";
			$inputAux->required = $this->controller->is_required($ref);
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;	

			$ref = "id_idioma";
			$checked = $this->controller->get_var($ref);
			if(trim($_REQUEST[$ref]) <> "") {
				$checked = $_REQUEST[$ref];
			} else if(intval($checked) == 0) {
				$checked = 99;
			}
			$inputAux = new Uzzye_ListBox($ref,$ref,get_lang("_IDIOMA"),0);
			$inputAux->default_display = get_lang("_COMBOSELECIONE");
			$inputAux->default_value = 0;
			$inputAux->checked_value = $checked;
			$inputAux->size = 1;
			$classAux = "idiomas_controller";
			$inputAux->reference_model = "idiomas";
			$objAux = new $classAux();
			$inputAux->set_options($objAux->get_array_options("id","titulo","","titulo ASC"));
			$inputAux->refresh_options(array("id","titulo","","titulo ASC"));
			$inputAux->set_value($checked);
			$inputAux->li_class = "w50p";
			$inputAux->required = true;
			$inputAux->multiple = false;
			$array_form_campos[sizeof($array_form_campos)] = $inputAux;
			$count_fields++;
				
			
			$this->array_form_campos = $array_form_campos;	
		}
}

?>