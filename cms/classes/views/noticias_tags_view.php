<?php    
   
class noticias_tags_view extends view
{     
        function __construct($owner = null)
        {
            global $modulo;

            $this->nome = "noticias_tags";
            if(function_exists("get_lang")) {
                $this->nome_exibicao = get_lang("_TAGS");
                $this->nome_exibicao_singular = get_lang("_TAG");
            }
        
            parent::__construct($owner);         
			
            $this->langs = array("pt","en"); 
        }   
    
        function monta_campos_form($id = "", $readonly = false, $clone = false, $resumido = false)
        {
            global $modulo;
        
            $b_editando = false;       
            if(trim($_REQUEST["id"]) <> "")
            {
                $id = $_REQUEST["id"];
                $this->controller->set_var("id",$id);
                $this->controller->carrega_dados();
                $b_editando = true;
            }
        
            $array_form_campos = array();
        
            $count_fields = 0;

            // Lang Tab Start
        
            $inputAux = new Uzzye_TabView("lang",get_lang("_CAMPOS_DE_IDIOMAS"),"fa fa-language");
            foreach($this->langs as $langAux) {
              $inputAux->addTab(strtoupper($langAux), "", ".lang-" . $langAux);
            }
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_fields++;
        
            foreach($this->langs as $langAux) {
              $classAux = "lang-item lang-" . $langAux . " tab-item"; if($langAux == $this->langs[0]) { $classAux .= " active";}
        
              $ref = "titulo_" . $langAux;
              $inputAux = new Uzzye_TextField($ref,$ref,get_lang("_TITULO_" . strtoupper($langAux)));
              $inputAux->set_value($this->controller->get_var($ref));
              $inputAux->li_class = "w50p " . $classAux;
              $inputAux->required = $this->controller->is_required($ref);
              $array_form_campos[sizeof($array_form_campos)] = $inputAux;
              $count_fields++;
            }
        
            // Lang Tab End
        
            // HR
            /* $inputAux = new Uzzye_HR();		
            $inputAux->label = get_lang("_OUTROS_CAMPOS");
            $inputAux->icon = "fa fa-align-left";
            $array_form_campos[sizeof($array_form_campos)] = $inputAux;
            $count_fields++; */
                
          
            $this->array_form_campos = $array_form_campos;

            parent::monta_campos_form($id, $readonly, $clone, $resumido);   
        }
}

?>