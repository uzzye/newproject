<?php

class Uzzye_LangTable extends Uzzye_Field
{	
	public $clone;
	public $id_registro;
	public $modulo_registro;
	public $lang_vars;
	public $i_count_field;
	public $b_editando;
	
    function __construct($name = "", $id = "", $label = "", $default_value = "", $readonly = "", $style_class = "sltNormal", $li_class = "", $sub_label = "")
	{
		$this->type = "langTable";

		$this->i_count_field = 1;
		$this->b_editando = false;
		$this->clone = false;

		parent::__construct($name, $id, $label, $default_value, $readonly, $style_class, $li_class, $sub_label);
	}
	
	function get_display_field()
	{
		global $mysql, $cryptDec;

		?>
		<script type="text/javascript">

		$(document).ready(function(e){
		
			'use strict';

			var iCount = 1;
			var master = $("#table-<?php echo $this->name; ?>");

			resetLangEvents(e);

			master.find(".btnAdd").unbind("click").click(function(e){
				var lista = master.find("table");

				var iQtd = 1;
				if(master.find(".qtdAdd").val().trim() != ""){
					iQtd = parseInt(master.find(".qtdAdd").val());
				}

				for(var i=0;i<iQtd;i++){
					var elAux = lista.find(".itLista").eq(0).clone();

					// Reset select2
					elAux.find(".select2").not("select").remove();
					elAux.find("select").removeClass("select2-hidden-accessible");

					// Fazer reset nos demais kenny?

					//var elAuxId = parseInt(lista.find(".itLista").length)+1;
					var elAuxId = parseInt(lista.find(".itLista:last").prop("id").split("-")[2])+1;
					elAux.prop("id","itLista-<?php echo $this->name; ?>-<?php echo $this->i_count_field; ?>-" + elAuxId.toString());
					
					elAux.find(".id_aux_<?php echo $this->name; ?>").val("");
					elAux.find(".id_aux_<?php echo $this->name; ?>").prop("id","id_aux-" + elAuxId.toString());

					if(elAux.find(".clonable").length > 0) {
						elAux.find(".clonable").each(function(e){
							var elClone = $(this);
							var sIDAux = elClone.prop("id").split("-")[0];
							elClone.prop("id",sIDAux + "-" + elAuxId.toString());

							if(!elClone.hasClass("keep")) {
								if(elClone.attr("type") == "button" ||
								   elClone.attr("type") == "hidden" ||
								   elClone.attr("type") == "submit")
								{
									// NADA
								}
								else if(elClone.attr("type") == "checkbox" ||
								   elClone.attr("type") == "radio")
								{
									//elClone.iCheck('uncheck');
								} else {
									elClone.val("");
								}
								
								elClone.find("option").eq(0).attr('selected','selected');
								elClone.trigger('update.fs');
							}

							if(elClone.hasClass("idField")) {
								elClone.val("");
							}
						});
					}
					
					elAux.find("label").each(function(e){
						$(this).prop("for",$(this).parent().children().eq(1).prop("id"));
					});

					elAux.insertAfter(lista.find(".itLista:last"));
				
					kenny(elAux);

					resetLangEvents(e);
				}

				resetLangTableIDs(e);
			});

			$(".form-cadastro-padrao").on("submit", function(e){
				resetLangTableIDs(e);
			});

			function resetLangEvents(e){
				master.find("table").find(".btnRem").unbind("click").click(function(e){
					if(master.find("table").find(".itLista").length > 1) {
						$(this).closest(".itLista").remove();
					} else {
						master.find(".btnAdd").trigger("click");
						
						$(this).closest(".itLista").remove();
					}

					resetLangTableIDs(e);
				});

				resetLangTableIDs(e);
			}

			function resetLangTableIDs(e) {
				var iCountAux = 1;
				master.find(".itLista").each(function(e){
					$(this).find(".id_aux_<?php echo $this->name; ?>").val(iCountAux);

					iCountAux++;
				});

				master.find("table").rowSorter({
					handler: "td.sorter",
					tbody: true
				});

			}
		});
		</script>
		<?php
		
		$result = "";
		
		$result .= $this->ini_field_set();

		if(trim($this->label) != "") {
			$result .= $this->get_display_label();
		}

		$iCount = 0;
		$iCountAux = 0;
		$iCountCampo = 0;

		$values = null;

		$result .= "<div class=\"row\" id=\"table-" . $this->name . "\">";

			$result .= "<table class=\"grid " . $this->name . "\" id=\"lista-" . $this->name . "-" . $this->i_count_field . "\">
				<tbody>";

				if(intval($this->id_registro) > 0 && trim($this->modulo_registro) != ""){
					$sqlCmd = "SELECT lang.* FROM (" . DBTABLE_LANG . " lang)
					WHERE lang.id_registro = " . intval($this->id_registro) . "
					  AND lang.modulo_registro = \"" . trim($this->modulo_registro) . "\"
					ORDER BY lang.ranking ASC ";
					$resCmd = $mysql->exec_query($sqlCmd);

					while($iCount<$mysql->num_rows($resCmd))
					{
						$values = array();
						$iCountAux++;
						$values["id_aux_" . $this->name] = $iCountAux;
						if($this->clone) {
							$values["id_" . $this->name] = 0;
						} else {
							$values["id_" . $this->name] = $mysql->result_field($resCmd,$iCount,"id");
						}
						
						$values["id_registro_" . $this->name] = $mysql->result_field($resCmd,$iCount,"id_registro");
						$values["modulo_registro_" . $this->name] = $mysql->result_field($resCmd,$iCount,"modulo_registro");
						$values["id_idioma_" . $this->name] = $mysql->result_field($resCmd,$iCount,"id_idioma");
						$values["var_" . $this->name] = stripslashes($mysql->result_field($resCmd,$iCount,"variavel"));				
						$values["valor_" . $this->name] = stripslashes($mysql->result_field($resCmd,$iCount,"valor"));
						
						if(is_array($values) && sizeof($values) > 0) {
							$result .= $this->get_campos($iCount,$values);
						}
						
						$iCount++;
					}
				}

				if($iCount == 0){
					$result .= $this->get_campos($iCount);

					$iCount++;
				}

				$result .= "</tbody>
				<tfoot>
					<tr valign=\"top\">";

						$result .= "<td colspan=\"100\">";
							$valAux = "1";
							$ref = "qtdAdd-" . $this->name . "-" . $this->i_count_field;
							$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_ADICIONAR"));
							$inputAux->set_value($valAux);
							$inputAux->name = $ref;
							$inputAux->id = $ref;
							$inputAux->style_class = $ref . " qtdAdd";
							$inputAux->li_class = "w10p";
							$count_fields++;
							$result .= $inputAux->get_display_field();

							$ref = "btnAdd-" . $this->name . "-" . $this->i_count_field;
							$inputAux = new Uzzye_Button($ref,$ref," ");
							$inputAux->icon = "icon-plus";
							$inputAux->name = $ref;
							$inputAux->id = $ref;
							$inputAux->style_class = $ref . " btnAdd btn-square btn-icon btn-icon-anim btn-success btn-primary";
							$inputAux->li_class = "w5p";
							$count_fields++;
							$result .= $inputAux->get_display_field();
						$result .= "</td>";

					$result .= "</tr>
				</tfoot>";

			$result .= "</table>";

		$result .= "</div>";
		
		$result .= $this->end_field_set();
		
		return $result;
	}
	
	function get_db_value()
	{
		return output_decode($this->value);
	}	
	
	function set_value($valor)
	{
		$this->value = get_output($valor);
	}

	function get_campos($iCount,$values = array()){
		$result = "";

		$result .= "<tr class=\"itLista itLista-" . $this->name . "\" id=\"itLista-" . $this->name . "-" . $this->i_count_field . "-" . ($iCount+1). "\" valign=\"top\">";

			$result .= "<td class=\"sorter\" width=\"1\">";

				$inputAux = new Uzzye_Label();
				$inputAux->icon = "fa fa-bars";
				$inputAux->li_class = "w100p sorter mt-30";
				$result .= $inputAux->get_display_field();

				$ref = "id_aux_" . $this->name;
				$valAux = 0;
				if(sizeof($values) > 0){
					$valAux = $values[$ref];
				}
				$inputAux = new Uzzye_HiddenField($ref,$ref,get_lang("_ID"));
				$inputAux->name = $ref . "[]";
				$inputAux->id = $ref . "-" . ($iCount+1);
				$inputAux->set_value($valAux);
				$inputAux->style_class = $ref;
				$count_fields++;
				$result .= $inputAux->get_display_field();

				$ref = "id_" . $this->name;
				$valAux = 0;
				if(sizeof($values) > 0){
					$valAux = $values[$ref];
				}
				$inputAux = new Uzzye_HiddenField($ref,$ref,get_lang("_ID"));
				$inputAux->name = $ref . "[]";
				$inputAux->id = $ref . "-" . ($iCount+1);
				$inputAux->set_value($valAux);
				$inputAux->style_class = $ref . " clonable idField ";
				$count_fields++;
				$result .= $inputAux->get_display_field();

				$ref = "id_registro_" . $this->name;
				$valAux = $this->id_registro;
				if(sizeof($values) > 0){
					$valAux = $values[$ref];
				}
				$inputAux = new Uzzye_HiddenField($ref,$ref,get_lang("_ID_REGISTRO"));
				$inputAux->name = $ref . "[]";
				$inputAux->id = $ref . "-" . ($iCount+1);
				$inputAux->set_value($valAux);
				$inputAux->style_class = $ref . " clonable keep";
				$count_fields++;
				$result .= $inputAux->get_display_field();

				$ref = "modulo_registro_" . $this->name;
				$valAux = $this->modulo_registro;
				if(sizeof($values) > 0){
					$valAux = $values[$ref];
				}
				$inputAux = new Uzzye_HiddenField($ref,$ref,get_lang("_MODULO_REGISTRO"));
				$inputAux->name = $ref . "[]";
				$inputAux->id = $ref . "-" . ($iCount+1);
				$inputAux->set_value($valAux);
				$inputAux->style_class = $ref . " clonable keep";
				$count_fields++;
				$result .= $inputAux->get_display_field();

			$result .= "</td>";

			$result .= "<td>";

				if(is_array($this->lang_vars) && sizeof($this->lang_vars) > 0) {
					$arrOpt = array();
					foreach($this->lang_vars as $lang) {
						array_push($arrOpt, array(output_decode($lang), output_decode($lang)));
					}
				
					$ref = "var_" . $this->name;
					$valAux = "";
					if(sizeof($values) > 0){
						$valAux = $values[$ref];
					}
					$inputAux = new Uzzye_ComboBox($ref,$ref,get_lang("_VARIAVEL"));
					$inputAux->default_display = get_lang("_COMBOSELECIONE");
					$inputAux->name = $ref . "[]";
					$inputAux->checked_value = $valAux;
					$inputAux->default_value = "";
					$inputAux->size = 1;
					$inputAux->set_options($arrOpt);
					$inputAux->set_value($valAux);
					$inputAux->li_class = "w25p";
					$count_fields++;
					$result .= $inputAux->get_display_field();
				} else {
					$ref = "var_" . $this->name;
					$valAux = "";
					if(sizeof($values) > 0){
						$valAux = $values[$ref];
					}
					$inputAux = new Uzzye_TextField($ref,$ref,get_lang("_VARIAVEL"));
					$inputAux->name = $ref . "[]";
					$inputAux->id = $ref . "-" . ($iCount+1);
					$inputAux->style_class = $ref . " form-Text clonable";
					$inputAux->set_value($valAux);
					$inputAux->li_class = "w25p";
					$count_fields++;
					$result .= $inputAux->get_display_field();
				}

				$ref = "id_idioma_" . $this->name;
				$valAux = "";
				if(sizeof($values) > 0){
					$valAux = $values[$ref];
				}
				$inputAux = new Uzzye_ListBox($ref,$ref,get_lang("_IDIOMA"),0);
				$inputAux->set_value($valAux);
				$inputAux->name = $ref . "[]";
				$inputAux->multiple = false;
				$inputAux->li_class = "w25p";
				$inputAux->hide_help = true;
				$inputAux->show_button = false;
				$inputAux->sql_where = "(principal.id != 1 AND principal.id != 99)";
				$inputAux->style_class = $ref . " clonable";
				$inputAux->set_reference_item("idiomas");
				$count_fields++;
				$result .= $inputAux->get_display_field();

				$ref = "valor_" . $this->name;
				$valAux = "";
				if(sizeof($values) > 0){
					$valAux = $values[$ref];
				}
				$inputAux = new Uzzye_TextArea($ref,$ref,get_lang("_VALOR"));
				$inputAux->name = $ref . "[]";
				$inputAux->id = $ref . "-" . ($iCount+1);
				$inputAux->style_class = $ref . " form-Text clonable";
				$inputAux->set_value($valAux);
				$inputAux->li_class = "w33p";
				$count_fields++;
				$result .= $inputAux->get_display_field();

			// CAMPO REMOVER

				$ref = "btnRem-" . $this->name;
				$inputAux = new Uzzye_Button($ref,$ref);
				$inputAux->icon = "icon-trash";
				$inputAux->name = $ref . "[]";
				$inputAux->id = $ref . "-" . ($iCount+1);
				$inputAux->style_class = $ref . " btnRem btn-square btn-icon btn-icon-anim btn-danger btn-primary";
				$inputAux->li_class = "w5p";
				$count_fields++;
				$result .= $inputAux->get_display_field();

		// FIM CAMPOS

			$result .= "</td>";
		$result .= "</tr>";

		return $result;
	}
}

?>