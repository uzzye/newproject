<?php

class Uzzye_HR extends Uzzye_Field
{	
	public $icon;

	function __construct($name = "", $id = "", $label = "", $default_value = "", $readonly = "", $style_class = "form-Text", $li_class = "", $sub_label = "")
	{	
		$this->type = "hr";
		
		parent::__construct($name, $id, $label, $default_value, $readonly, $style_class, $li_class, $sub_label, $max_length);		
		
		$this->li_class = "w100p";
	}
	
	function get_display_field()
	{
		$result = "";
		$result .= $this->ini_field_set();
		//$result .= $this->get_display_label();		
		$result .= "<h6 class=\"txt-dark capitalize-font\"><i class=\"" . $this->icon . " mr-10\"></i>" . $this->label . "</h6>
		<hr/>";
		$result .= $this->end_field_set();
		
		return $result;
	}
	
	function get_db_value()
	{
		global $db;
		return output_decode($db->escape_string($this->value));
	}	
	
	function set_value($valor)
	{
		$this->value = htmlspecialchars(stripslashes(get_output($valor)));
	}
}

?>