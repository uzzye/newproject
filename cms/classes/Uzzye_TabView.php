<?php

class Uzzye_TabView extends Uzzye_Field
{	
	public $icon;
	public $tabs;
	public $fields;

	function __construct($name = "", $label = "", $icon = "")
	{	
		$this->type = "tab";
		$this->id = $name;
		
		parent::__construct($name, $id, $label);
		
		$this->li_class = "w100p";
		$this->tabs = array();
		$this->fields = array();
		$this->icon = $icon;
	}
	
	function get_display_field()
	{
		$result = "";

		// HR
		$inputAux = new Uzzye_HR();		
		$inputAux->label = $this->label;
		$inputAux->icon = $this->icon;
		$result .= $inputAux->get_display_field();

		$result .= $this->ini_field_set();
		//$result .= $this->get_display_label();

		$result .= "<div class=\"tab-struct\">
			<ul role=\"tablist\" class=\"nav nav-tabs\" id=\"tabNav_" . $this->name . "\">
				" . implode("", $this->tabs) . "
			</ul>";
		$result .= "</div>";

		$result .= $this->end_field_set();

		return $result;
	}

	function addTab($label, $icon, $toggle) {
		$classAux = ""; if(sizeof($this->tabs) == 0) {$classAux = "active in";}

		array_push($this->tabs,"<li class=\"" . $classAux . "\" role=\"presentation\"><a aria-expanded=\"true\" role=\"tab\" rel=\"." . $this->name . "-item\" id=\"tab_" . $this->name . "\" data-toggle=\"" . $toggle . "\" href=\"#\" onclick=\"$('#tabNav_" . $this->name . "').find('li').removeClass('active in'); $(this).addClass('active in');\">
			<h6 class=\"txt-dark capitalize-font\"><i class=\"" . $icon . " mr-10\"></i>" . $label . "</h6>
		</a></li>");
	}
	
	function get_db_value()
	{
		global $db;
		return output_decode($db->escape_string($this->value));
	}	
	
	function set_value($valor)
	{
		$this->value = htmlspecialchars(stripslashes(get_output($valor)));
	}
}

?>