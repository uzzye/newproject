<?php

class Uzzye_PermsTable extends Uzzye_Field
{	
	public $id_usuario;
	public $id_grupo;
	
    function __construct($name = "", $id = "", $label = "", $default_value = "", $readonly = "", $style_class = "sltNormal", $li_class = "", $sub_label = "")
	{
		$this->type = "permsTable";

		parent::__construct($name, $id, $label, $default_value, $readonly, $style_class, $li_class, $sub_label);
	}
	
	function get_display_field()
	{
		global $db, $_CMS_CONFIG;

		$varAux = "nome";
		if(trim($_SESSION["_config"]["ref_lang"]) != "pt") {
			$varAux = "nome_" . $_SESSION["_config"]["ref_lang"];
		}
		
		$result = "";
		$result .= $this->ini_field_set();
		$result .= $this->get_display_label();

		$sqlCmd = "";
		if(trim($this->id_usuario) != "" || trim($this->id_grupo) != ""){
			$sqlCmd = "SELECT mol.*, pai." . $varAux . " pai
			FROM " . DBTABLE_CMS_MODULOS . " mol
			LEFT JOIN " . DBTABLE_CMS_MODULOS . " pai ON pai.id = mol.modulo_pai
			WHERE mol.ativo = 1 
			ORDER BY pai." . $varAux . " ASC, mol." . $varAux . " ASC, mol.nome ASC";
			
			$its = @$db->exec_query($sqlCmd);
			$i_count_reg = @$db->num_rows($its);  

			if ( $i_count_reg > 0 ) {
				$iCount = 0;
				$result .= "<input type=\"hidden\" name=\"perms[]\" value=\"" . $this->id . "#acoes\"/>
				<div class=\"row\">";
				while ( $it = @mysqli_fetch_object($its) )
				{		
					$moduloAux = $it->link;
					$idModuloAux = $it->id;
					if(trim($moduloAux) != ""){
						
						if(trim($this->id_usuario) != ""){
							$sqlCmdAcoes = "SELECT acoes 
							FROM " . DBTABLE_CMS_MODULOS_USUARIOS . "
							WHERE id_usuario = " . intval($this->id_usuario) .  "
						  	 AND id_modulo = " . intval($idModuloAux) . " ";
							$itsAcoes = $db->exec_query($sqlCmdAcoes);
						}
						else if(trim($this->id_grupo) != ""){
							$sqlCmdAcoes = "SELECT acoes 
							FROM " . DBTABLE_CMS_MODULOS_GRUPOS_USUARIOS . "
							WHERE id_grupo = " . intval($this->id_grupo) .  "
						  	 AND id_modulo = " . intval($idModuloAux) . " ";
							$itsAcoes = $db->exec_query($sqlCmdAcoes);
						}
						
						$arrayAcoes = array();
						
						if(@$db->num_rows($itsAcoes) > 0){
							$arrayAcoes = explode(";",$db->result_field($itsAcoes,0,"acoes"));
						}
						
						$lang = get_lang();
						
						require_once(ROOT_CMS . "classes/" . $moduloAux . ".php");
						$objAux = new $moduloAux();
						
						$acaoAux = "Todas";
						$acoes = explode(";",$objAux->acoes);
						$strCheck = "";
						if(in_array($acaoAux,$arrayAcoes,true)){
							$strCheck = " checked";
						}
						
						$iCountAcao = 0;

						$modTit = stripslashes(get_output($it->$varAux));
						if(trim($modTit) == "") {
							$modTit = stripslashes(get_output($it->nome));
						}

						if(trim($it->pai) != "") {
							$modTit = stripslashes(get_output($it->pai)) . " -> " . $modTit;
						}

						$result .= "<div class=\"col-sm-3 mt-10\">" . $modTit . "
							<ul class=\"list acoesPerms\">
								<li>
									<div class=\"checkbox checkbox-custom\">
										<input" . $strCheck . " type=\"checkbox\" id=\"" . $this->id . "-acoes-" . intval($iCount) . "-" . intval($iCountAcao) . "\" name=\"" . $this->id . "#acoes[]\" value=\"" . $idModuloAux . "#" . $acaoAux . "\" />
										<label for=\"" . $this->id . "-acoes-" . intval($iCount) . "-" . intval($iCountAcao) . "\">&nbsp;" . $lang["_ACAO_" . strtoupper($acaoAux)] . "</label>
									</div>
								</li>";
						
						$iCountAcao = 1;
						foreach($acoes as $acaoAux){
							if(trim($acaoAux) != ""){
								$strCheck = "";
								if(in_array($acaoAux,$arrayAcoes,true)){
									$strCheck = " checked";
								}
								$result .= "<li>
									<div class=\"checkbox checkbox-custom\">
										<input" . $strCheck . " type=\"checkbox\" id=\"" . $this->id . "-acoes-" . intval($iCount) . "-" . intval($iCountAcao) . "\" name=\"" . $this->id . "#acoes[]\" value=\"" . $idModuloAux . "#" . $acaoAux . "\"/>
										<label for=\"" . $this->id . "-acoes-" . intval($iCount) . "-" . intval($iCountAcao) . "\">&nbsp;" . $lang["_ACAO_" . strtoupper($acaoAux)] . "</label>
									</div>
								</li>";
								$iCountAcao++;
							}
						}
						
						$result .= "</ul>
								</div>";
						
						$iCount++;
					}
				}
				$result .= "</div>";
			}
		}
		
		$result .= $this->end_field_set();
		
		return $result;
	}
	
	function get_db_value()
	{
		return output_decode($this->value);
	}	
	
	function set_value($valor)
	{
		$this->value = get_output($valor);
	}
}

?>