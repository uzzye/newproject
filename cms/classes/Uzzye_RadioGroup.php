<?php

class Uzzye_RadioGroup extends Uzzye_Field
{	
	public $checked_value;
	public $fixed_pos;
	public $array_options;

    function __construct($name = "", $id = "", $label = "", $default_value = "", $readonly = "", $style_class = "form-Radio", $li_class = "", $sub_label = "")
	{
		$this->type = "checkbox";
		$this->fixed_pos = false;
		
		parent::__construct($name, $id, $label, $default_value, $readonly, $style_class, $li_class, $sub_label);
	}
	
	function get_display_field()
	{
		$result = "";
		$result .= $this->ini_field_set();
		$result .= $this->get_display_label();		
		
		$result .= "<div class=\"radio-list\">";

		$iCount = 0;
		foreach($this->array_options as $option) {		
			$result .= "<div class=\"radio-inline pl-0\">
				<span class=\"radio radio-info\">
				<input type=\"radio\" class=\"" . $this->style_class. "\" id=\"" . ($this->id) . "_" . $iCount . "\" name=\"" . ($this->name) . "\" value=\"" . ($option[0]) . "\" ";
			if($this->checked_value == $option[0])
			{
				$result .= " checked ";
			}
			if($this->readonly)
			{
				$result .= " readonly onclick=\"this.checked = !this.checked; return false;\" ";
			}
			if($this->required)
			{
				$result .= " required='required' ";
			}
			$result .= " />
					<label for=\"" . ($this->id) . "_" . $iCount . "\">" . $option[1] . "</label>
				</span>
			</div>";
			$iCount++;
		}

		$result .= "</div>";
		
		$result .= $this->end_field_set();
		
		return $result;
	}
	
	function get_db_value()
	{
		return $this->value;
	}	
	
	function set_value($valor)
	{
		$this->value = $valor;
	}

	function set_options($array) {
		$this->array_options = $array;
	}
}

?>