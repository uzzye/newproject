<?php

class Uzzye_PasswordField extends Uzzye_Field
{	
	function __construct($name = "", $id = "", $label = "", $default_value = "", $readonly = "", $style_class = "form-Text", $li_class = "", $sub_label = "")
	{
		$this->type = "password";
		
		parent::__construct($name, $id, $label, $default_value, $readonly, $style_class, $li_class, $sub_label);
	}
	
	function get_display_field()
	{
		$result = "";
		$result .= $this->ini_field_set();
		$result .= $this->get_display_label();		
		
		$result .= "<input autocomplete=\"new-password_" . $_SESSION["GLOBAL_RANDOM_KEY"] . "\" type=\"password\" class=\"form-control " . $this->style_class. "\" id=\"" . ($this->id) . "\" name=\"" . ($this->name) . "\" value=\"" . ($this->value) . "\" placeholder=\"Deixe em branco para não alterar\" ";
		if($this->required)
		{
			$result .= " required='required' ";
		}
		$result .= "/>";
		
		$result .= $this->end_field_set();
		
		return $result;
	}
	
	function get_db_value()
	{
		return md5($this->value);
	}	
	
	function set_value($valor)
	{
		$this->value = get_output($valor);
	}
}

?>