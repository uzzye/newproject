<?php

class Uzzye_TextField extends Uzzye_Field
{	
	public $max_length;
	public $mask;
	
    function __construct($name = "", $id = "", $label = "", $default_value = "", $readonly = "", $style_class = "form-Text", $li_class = "", $sub_label = "", $max_length = 255)
	{	
		$this->type = "text";
		
		parent::__construct($name, $id, $label, $default_value, $readonly, $style_class, $li_class, $sub_label, $max_length);		
	}
	
	function get_display_field()
	{
		$result = "";
		$result .= $this->ini_field_set();
		$result .= $this->get_display_label();		
			
		$result .= "<input autocomplete=\"off\" type=\"text\" class=\"form-control " . $this->style_class. "\" id=\"" . ($this->id) . "\" name=\"" . ($this->name) . "\" value=\"" . ($this->value) . "\" placeholder=\"" . $this->default_value . "\"";
		if(trim($this->max_length) <> "" && $this->max_length > 0)
		{
			$result .= " maxlength=\"" . ($this->max_length) . "\"";
		}	
		if($this->readonly)
		{
			$result .= " readonly";
		}	
		if($this->required)
		{
			$result .= " required='required' ";
		}
		if($this->focus)
		{
			$result .= " autofocus='true' ";
		}
		$result .= "/>";

		if(trim($this->help_text) != "") {
			$result .= "<span class=\"help-block\"> " . $this->help_text . " </span>";
		}
		
		$result .= $this->end_field_set();
		
		if(trim($this->mask) <> ""){
			// TODO
			/*?>
			<script type="text/javascript">
			$(document).ready(function(e){
   				$("#<?php echo ($this->id);?>").mask("<?php echo ($this->mask);?>");
   			});
   			</script>
			<?php*/
		}
		
		return $result;
	}
	
	function get_db_value()
	{
		global $db;
		return output_decode($db->escape_string($this->value));
	}	
	
	function set_value($valor)
	{
		$this->value = htmlspecialchars(stripslashes(get_output($valor)));
	}
}

?>