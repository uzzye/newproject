<?php

include_once("config.php");

$db->exec_query("RENAME TABLE " . $mySQL_prefix . "_grupos_usuarios TO " . $mySQL_prefix . "_cms_grupos_usuarios");

$db->exec_query("RENAME TABLE " . $mySQL_prefix . "_usuarios TO " . $mySQL_prefix . "_cms_usuarios");

$db->exec_query("RENAME TABLE " . $mySQL_prefix . "_modulos TO " . $mySQL_prefix . "_cms_modulos");

$db->exec_query("RENAME TABLE " . $mySQL_prefix . "_modulos_grupos_usuarios TO " . $mySQL_prefix . "_cms_modulos_grupos_usuarios");

$db->exec_query("RENAME TABLE " . $mySQL_prefix . "_modulos_usuarios TO " . $mySQL_prefix . "_cms_modulos_usuarios");

$db->exec_query("RENAME TABLE " . $mySQL_prefix . "_log_entidades TO " . $mySQL_prefix . "_cms_log_entidades");

$db->exec_query("ALTER TABLE " . $mySQL_prefix . "_configuracoes
ADD views int(11) DEFAULT 0,
ADD permalink tinytext DEFAULT null,
ADD ranking int(11) DEFAULT 1,
ADD ga_user varchar(255) DEFAULT NULL,
ADD ga_props text DEFAULT NULL,
ADD valor_conversao_contato double(12,2) DEFAULT NULL");

$db->exec_query("UPDATE " . $mySQL_prefix . "_configuracoes SET 
ga_user = \"agenciauzzye@kinetic-valor-128616.iam.gserviceaccount.com\",
valor_conversao_contato = 1");

$db->exec_query("ALTER TABLE " . $mySQL_prefix . "_contatos
ADD views int(11) DEFAULT 0,
ADD permalink tinytext DEFAULT null,
ADD ranking int(11) DEFAULT 1,
ADD conversao int(1) DEFAULT NULL,
ADD data_conversao datetime DEFAULT NULL,
ADD valor_conversao double(12,2) DEFAULT NULL");

$db->exec_query("ALTER TABLE " . $mySQL_prefix . "_conteudos
ADD views int(11) DEFAULT 0,
ADD permalink tinytext DEFAULT null,
ADD ranking int(11) DEFAULT 1");

$db->exec_query("ALTER TABLE " . $mySQL_prefix . "_cms_grupos_usuarios
ADD views int(11) DEFAULT 0,
ADD permalink tinytext DEFAULT null,
ADD ranking int(11) DEFAULT 1");

$db->exec_query("ALTER TABLE " . $mySQL_prefix . "_idiomas
ADD views int(11) DEFAULT 0,
ADD permalink tinytext DEFAULT null,
ADD ranking int(11) DEFAULT 1");

$db->exec_query("ALTER TABLE " . $mySQL_prefix . "_cms_log_entidades
ADD views int(11) DEFAULT 0,
ADD permalink tinytext DEFAULT null,
ADD ranking int(11) DEFAULT 1");

$db->exec_query("ALTER TABLE " . $mySQL_prefix . "_cms_modulos
ADD views int(11) DEFAULT 0,
ADD permalink tinytext DEFAULT null,
ADD ranking int(11) DEFAULT 1");

$db->exec_query("ALTER TABLE " . $mySQL_prefix . "_cms_modulos_grupos_usuarios
ADD views int(11) DEFAULT 0,
ADD permalink tinytext DEFAULT null,
ADD ranking int(11) DEFAULT 1");

$db->exec_query("ALTER TABLE " . $mySQL_prefix . "_cms_modulos_usuarios
ADD views int(11) DEFAULT 0,
ADD permalink tinytext DEFAULT null,
ADD ranking int(11) DEFAULT 1");

$db->exec_query("ALTER TABLE " . $mySQL_prefix . "_cms_usuarios
ADD views int(11) DEFAULT 0,
ADD permalink tinytext DEFAULT null,
ADD ranking int(11) DEFAULT 1");

$db->exec_query("ALTER TABLE " . $mySQL_prefix . "_links_uteis
ADD views int(11) DEFAULT 0,
ADD permalink tinytext DEFAULT null,
ADD ranking int(11) DEFAULT 1");

$db->exec_query("ALTER TABLE " . $mySQL_prefix . "_cms_modulos
ADD icon_class varchar(255) DEFAULT NULL,
ADD cor varchar(255) DEFAULT NULL");

$db->exec_query("CREATE TABLE IF NOT EXISTS `" . $mySQL_prefix . "_cms_imagens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_criacao` datetime DEFAULT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `usuario_criacao` int(11) DEFAULT NULL,
  `usuario_atualizacao` int(11) DEFAULT NULL,
  `ativo` int(1) DEFAULT '1',
  `views` int(11) DEFAULT '0',
  `permalink` tinytext,
  `ranking` int(11) DEFAULT '1',
  `titulo` varchar(255) DEFAULT NULL,
  `arquivo` tinytext,
  `modulo_registro` varchar(255) DEFAULT NULL,
  `id_registro` int(11) DEFAULT NULL,
  `campo_registro` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_" . $mySQL_prefix . "_cms_imagens_usuario_criacao` (`usuario_criacao`),
  KEY `fk_" . $mySQL_prefix . "_cms_imagens_usuario_atualizacao` (`usuario_atualizacao`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1");

$db->exec_query("CREATE TABLE IF NOT EXISTS `" . $mySQL_prefix . "_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_criacao` datetime DEFAULT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `usuario_criacao` int(11) DEFAULT NULL,
  `usuario_atualizacao` int(11) DEFAULT NULL,
  `ativo` int(1) DEFAULT '1',
  `views` int(11) DEFAULT '0',
  `permalink` tinytext,
  `ranking` int(11) DEFAULT '1',
  `id_idioma` int(11) DEFAULT NULL,
  `id_registro` int(11) DEFAULT NULL,
  `modulo_registro` varchar(255) DEFAULT NULL,
  `variavel` varchar(255) DEFAULT NULL,
  `valor` longtext,
  PRIMARY KEY (`id`),
  KEY `fk_" . $mySQL_prefix . "_language_usuario_criacao` (`usuario_criacao`),
  KEY `fk_" . $mySQL_prefix . "_language_usuario_atualizacao` (`usuario_atualizacao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1");

$db->exec_query("CREATE TABLE IF NOT EXISTS `" . $mySQL_prefix . "_log_views` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_criacao` datetime DEFAULT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `usuario_criacao` int(11) DEFAULT NULL,
  `usuario_atualizacao` int(11) DEFAULT NULL,
  `ativo` int(1) DEFAULT '1',
  `views` int(11) DEFAULT '0',
  `permalink` tinytext,
  `ranking` int(11) DEFAULT '1',
  `modulo_registro` varchar(255) DEFAULT NULL,
  `id_registro` int(11) DEFAULT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `idade` int(3) DEFAULT NULL,
  `sexo` varchar(255) DEFAULT NULL,
  `relacionamento` varchar(255) DEFAULT NULL,
  `geolocalizacao` tinytext,
  `cidade` varchar(255) DEFAULT NULL,
  `estado` varchar(255) DEFAULT NULL,
  `pais` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `id_facebook` tinytext,
  `email` tinytext,
  `tags` text,
  PRIMARY KEY (`id`),
  KEY `fk_" . $mySQL_prefix . "_log_views_usuario_criacao` (`usuario_criacao`),
  KEY `fk_" . $mySQL_prefix . "_log_views_usuario_atualizacao` (`usuario_atualizacao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1");

$db->exec_query("ALTER TABLE `" . $mySQL_prefix . "_cms_imagens`
  ADD CONSTRAINT `fk_" . $mySQL_prefix . "_cms_imagens_usuario_atualizacao` FOREIGN KEY (`usuario_atualizacao`) REFERENCES `" . $mySQL_prefix . "_cms_usuarios` (`id`),
  ADD CONSTRAINT `fk_" . $mySQL_prefix . "_cms_imagens_usuario_criacao` FOREIGN KEY (`usuario_criacao`) REFERENCES `" . $mySQL_prefix . "_cms_usuarios` (`id`)");

$db->exec_query("ALTER TABLE `" . $mySQL_prefix . "_language`
  ADD CONSTRAINT `fk_" . $mySQL_prefix . "_language_usuario_atualizacao` FOREIGN KEY (`usuario_atualizacao`) REFERENCES `" . $mySQL_prefix . "_cms_usuarios` (`id`),
  ADD CONSTRAINT `fk_" . $mySQL_prefix . "_language_usuario_criacao` FOREIGN KEY (`usuario_criacao`) REFERENCES `" . $mySQL_prefix . "_cms_usuarios` (`id`)");

$db->exec_query("ALTER TABLE `" . $mySQL_prefix . "_log_views`
  ADD CONSTRAINT `fk_" . $mySQL_prefix . "_log_views_usuario_atualizacao` FOREIGN KEY (`usuario_atualizacao`) REFERENCES `" . $mySQL_prefix . "_cms_usuarios` (`id`),
  ADD CONSTRAINT `fk_" . $mySQL_prefix . "_log_views_usuario_criacao` FOREIGN KEY (`usuario_criacao`) REFERENCES `" . $mySQL_prefix . "_cms_usuarios` (`id`)");

$db->exec_query("CREATE TABLE IF NOT EXISTS `" . $mySQL_prefix . "_banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_criacao` datetime DEFAULT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `usuario_criacao` int(11) DEFAULT NULL,
  `usuario_atualizacao` int(11) DEFAULT NULL,
  `ativo` int(1) DEFAULT '1',
  `id_referencia` int(11) DEFAULT NULL,
  `titulo_pt` varchar(255) DEFAULT NULL,
  `descricao_pt` text,
  `imagem_bg` tinytext,
  `imagem_bg_mobile` tinytext,
  `cinemagraph` tinytext,
  `video_url` text,
  `video_embed` text,
  `url` text,
  `target` set('','_blank') DEFAULT NULL,
  `texto_botao_pt` varchar(255) DEFAULT NULL,
  `ranking` int(11) DEFAULT NULL,
  `tempo_duracao` int(11) DEFAULT NULL,
  `tempo_transicao` int(11) DEFAULT NULL,
  `imagem_conteudo_pt` tinytext,
  `titulo_en` varchar(255) DEFAULT NULL,
  `descricao_en` text,
  `texto_botao_en` varchar(255) DEFAULT NULL,
  `imagem_conteudo_en` tinytext,
  `video_autoplay` int(1) DEFAULT NULL,
  `cinemagraph_mobile` tinytext,
  `cor_overlay` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_" . $mySQL_prefix . "_banners_usuario_criacao` (`usuario_criacao`),
  KEY `fk_" . $mySQL_prefix . "_banners_usuario_atualizacao` (`usuario_atualizacao`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1");

ob_clean();
die();

?>