<?php

// Tutorial para migrar um CMS para a v3.0. Executar quando for subir o novo em cima do antigo.

// Fazer um BACKUP de toda a pasta antes de executar as modificações

// 1 - Copiar novas classes para o "/classes" e editar o "classes.php", adicionando:

include_once("classes/models/grupos_usuarios_model.php");
include_once("classes/views/grupos_usuarios_view.php");
include_once("classes/controllers/grupos_usuarios_controller.php");
include_once("classes/grupos_usuarios.php");
include_once("classes/models/usuarios_model.php");
include_once("classes/views/usuarios_view.php");
include_once("classes/controllers/usuarios_controller.php");
include_once("classes/usuarios.php");
include_once("classes/models/modulos_model.php");
include_once("classes/views/modulos_view.php");
include_once("classes/controllers/modulos_controller.php");
include_once("classes/modulos.php");
include_once("classes/models/log_entidades_model.php");
include_once("classes/views/log_entidades_view.php");
include_once("classes/controllers/log_entidades_controller.php");
include_once("classes/log_entidades.php");

// 2 - Copiar o conteúdo específico dos atuais "/lang/*.php" antes de "// TESTE" nos novos, converter de "define()" (modo antigo) para array (modo novo) e adicionar no final de cada arquivo:

$lang = get_lang();
$keys = array_keys($lang);
foreach($keys as $k) {
	define($k,$lang[$k]);
}

// 3 - Substituir "novoprojeto" pelo identificador do projeto em "DB_update.php" e executar

// 4 - Copiar o conteúdo específico do antigo "/_common/tabelas.php" para o novo

// 5 - Verificar módulos, em especial os views com configuração específica, como "contatos" ou "cadastros" (botão Visualizar)