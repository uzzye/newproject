<?php

class Uzzye_DateField extends Uzzye_Field
{	
	public $id_inicio;
	public $id_fim;
	public $time;
	
    function __construct($name = "", $id = "", $label = "", $default_value = "", $readonly = "", $style_class = "form-Text", $li_class = "", $sub_label = "")
	{
		$this->type = "date";	
		
		parent::__construct($name, $id, $label, $default_value, $readonly, $style_class, $li_class, $sub_label);
		
		$this->time = false;
	}
	
	function get_display_field()
	{
		$result = "";
		$result .= $this->ini_field_set();
		$result .= $this->get_display_label();	

		$randcod = gera_senha();	
		
		$result .= "<div class=\"input-group date\" id=\"" . ($this->id) . "_" . $randcod . "\">";
		$result .= "<input type=\"text\" class=\"form-control form-date " . $this->style_class. "\" id=\"" . ($this->id) . "_field\" name=\"" . ($this->name) . "\" value=\"" . ($this->value) . "\" placeholder=\"" . $this->default_value . "\"";
		if($this->readonly)
		{
			$result .= " readonly";
		}	
		if(trim($this->id_inicio) != "")
		{
			$result .= " data-id-inicio=\"" . trim($this->id_inicio) . "\"";
		}	
		if(trim($this->id_fim) != "")
		{
			$result .= " data-id-fim=\"" . trim($this->id_fim) . "\"";
		}	
		if($this->required)
		{
			$result .= " required='required' ";
		}
		if(trim($this->onchange) != "")
		{
			$result .= " onchange=\"" . $this->onchange . "\" ";
		}

		$mask = "99/99/9999";
		$format = "DD/MM/YYYY";
		if($this->time) {
			$mask .= " 99:99:99";
			$format .= " HH:mm:ss";
		}
		if(trim($mask) != "")
		{
			$result .= " data-mask=\"" . $mask . "\" ";
		}
		if(trim($format) != "")
		{
			$result .= " data-format=\"" . $format . "\" ";
		}

		$result .= "/>";
		$result .= "<span class=\"input-group-addon\">
				<span class=\"fa fa-calendar\"></span>
			</span>
		</div>";
		
		$result .= $this->end_field_set();
		return $result;
	}
	
	function get_db_value()
	{
		if($this->time) {
			return output_decode(decode_data($this->value,true));
		} else {
			return output_decode(decode_data($this->value));
		}
	}	
	
	function set_value($valor)
	{	
		if($this->time) {
			$this->value = get_output(encode_data($valor,true));
		} else {
			$this->value = get_output(encode_data($valor));
		}
	}
}

?>