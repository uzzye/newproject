<?php

require_once(ROOT_CMS . "classes/models/cms_grupos_usuarios_model.php");

class cms_usuarios_model extends model{

	public $nome;
	public $setor;
	public $email;
	public $senha;
	public $id_grupo;
	public $disponivel;
	public $avatar;
	public $data_ultimoacesso;
	public $id_idioma;
    
    function __construct(){
		// Instancia o Objeto
		$this->nome_tabela = DBTABLE_CMS_USUARIOS;
		
		$this->reference_items[sizeof($this->reference_items)] = array("id","cms_modulos_usuarios","id_usuario");
		$this->foreign_keys[sizeof($this->foreign_keys)] = array("id_grupo","cms_grupos_usuarios","id");
		$this->foreign_keys[sizeof($this->foreign_keys)] = array("id_idioma","idiomas","id");
		
		parent::__construct();
		
		$this->array_crop_fields = array("avatar");
    }
	
	function do_perms($id)
	{	
		global $db, $modulo;
		
		$db->exec_query("DELETE FROM " . DBTABLE_CMS_MODULOS_USUARIOS . " WHERE id_usuario = " . intval($id) .  " ");
		
		$idModuloAux = "";
		$idModuloAnterior = "";
		$strAcoes = "";
		
		if(@sizeof($_REQUEST["perms"]) > 0){
			foreach($_REQUEST["perms"] as $campo){
				if(@sizeof($_REQUEST[$campo]) > 0){
					foreach($_REQUEST[$campo] as $value){
						$values = explode("#",$value);
					
						$idModuloAux = $values[0];
						$acaoAux = $values[1];
					
						if(trim($idModuloAnterior) != "" && $idModuloAux != $idModuloAnterior){
							if(strstr($strAcoes,"Todas")){
								$strAcoes = "Todas;";
							}
							
							$db->exec_query("INSERT INTO " . DBTABLE_CMS_MODULOS_USUARIOS . " 
							SET id_usuario = " . intval($id) .  ", 
								id_modulo = " . intval($idModuloAnterior). ",
								acoes = \"" . substr($strAcoes,0,-1) . "\", 
								usuario_criacao = " . intval($_SESSION["idLogin"]) . ", 
								data_criacao = NOW() ");		
							$strAcoes = "";
						}
					
						$strAcoes .= $acaoAux . ";";
					
						$idModuloAnterior = $idModuloAux;
					}
					
					if(trim($strAcoes) != ""){
						if(strstr($strAcoes,"Todas")){
							$strAcoes = "Todas;";
						}
						$db->exec_query("INSERT INTO " . DBTABLE_CMS_MODULOS_USUARIOS . " 
							SET id_usuario = " . intval($id) .  ", 
							id_modulo = " . intval($idModuloAnterior). ",
							acoes = \"" . substr($strAcoes,0,-1) . "\", 
							usuario_criacao = " . intval($_SESSION["idLogin"]) . ", 
							data_criacao = NOW() ");		
						$strAcoes = "";
					}
				}
			}
		}
	}
	
	function before_excluir()
	{
		global $db, $modulo;
		
		$db->exec_query("DELETE FROM " . DBTABLE_CMS_MODULOS_USUARIOS . " WHERE id_usuario = " . intval($_REQUEST["id"]) .  " ");
	}
	
	function do_excluir()
	{
		global $db, $modulo;

		$this->before_excluir();
		
		return parent::do_excluir();
	}
	
	function do_excluir_massa()
	{
		global $db, $modulo;
		
		$this->before_excluir();
		
		return parent::do_excluir_massa();
	}
	
	function do_after_editar($id)
	{
		$this->do_perms($id);
		return parent::do_after_editar($id);
	}
	
	function do_after_incluir($id)
	{
		$this->do_perms($id);
		return parent::do_after_incluir($id);
	}
}

?>