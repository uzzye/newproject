<?php

//include_once(ROOT_CMS . "classes/models/idiomas_model.php");

class teste_model extends model{

	public $cms_modulos; // text DEFAULT NULL,
    public $imagem; // tinytext DEFAULT NULL,
    public $imagem_thumb; // tinytext DEFAULT NULL,
    public $arquivo; // tinytext DEFAULT NULL,
    public $descricao; // text DEFAULT NULL,
    public $descricao_html; // text DEFAULT NULL,
    public $sexo; // int(1) DEFAULT NULL,
    public $valor; // double(12,2) DEFAULT NULL,
    public $data; // datetime DEFAULT NULL,
    public $cor; // varchar(255) DEFAULT NULL,
    public $geolocalizacao; // text DEFAULT NULL,
    public $titulo; // varchar(255) DEFAULT NULL,
    
    function __construct(){
		// Instancia o Objeto
		$this->nome_tabela = DBTABLE_TESTE;
		
		parent::__construct();
        
        $this->reference_models[sizeof($this->reference_models)] = array("cms_modulos","cms_modulos","id");

        $this->reference_data_tables[sizeof($this->reference_data_tables)] = array("itens_teste","id_teste");
        //$this->reference_img_gallery[sizeof($this->reference_img_gallery)] = array("itens_teste", "id_teste");
        //$this->reference_file_gallery[sizeof($this->reference_file_gallery)] = array("itens_teste", "id_teste");
		
		$this->array_crop_fields = array("imagem", "imagem_thumb");
		$this->array_file_fields = array("arquivo");
    }
}

?>