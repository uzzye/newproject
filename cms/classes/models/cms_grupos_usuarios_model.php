<?php
    
class cms_grupos_usuarios_model extends model{

		public $titulo;

		function __construct(){
			// Instancia o Objeto
			$this->nome_tabela = DBTABLE_CMS_GRUPOS_USUARIOS;
			
			$this->reference_items[sizeof($this->reference_items)] = array("id","cms_modulos_grupos_usuarios","id_grupo");
		
			parent::__construct();
		}
		
function do_after_incluir($id)
	{	
		global $db, $modulo;
		
		$db->exec_query("DELETE FROM " . DBTABLE_CMS_MODULOS_GRUPOS_USUARIOS . " WHERE id_grupo = " . intval($id) .  " ");
		
		$idModuloAux = "";
		$idModuloAnterior = "";
		$strAcoes = "";
		
		foreach($_REQUEST["perms"] as $campo){
			foreach($_REQUEST[$campo] as $value){
				$values = explode("#",$value);
			
				$idModuloAux = $values[0];
				$acaoAux = $values[1];
			
				if(trim($idModuloAnterior) != "" && $idModuloAux != $idModuloAnterior){
					if(strstr($strAcoes,"Todas")){
						$strAcoes = "Todas;";
					}
					
					$db->exec_query("INSERT INTO " . DBTABLE_CMS_MODULOS_GRUPOS_USUARIOS . " 
					SET id_grupo = " . intval($id) .  ", 
						id_modulo = " . intval($idModuloAnterior). ",
						acoes = \"" . substr($strAcoes,0,-1) . "\", 
						usuario_criacao = " . intval($_SESSION["idLogin"]) . ", 
						data_criacao = NOW() ");		
					$strAcoes = "";
				}
			
				$strAcoes .= $acaoAux . ";";
			
				$idModuloAnterior = $idModuloAux;
			}
			
			if(trim($strAcoes) != ""){
				if(strstr($strAcoes,"Todas")){
					$strAcoes = "Todas;";
				}
				$db->exec_query("INSERT INTO " . DBTABLE_CMS_MODULOS_GRUPOS_USUARIOS . " 
					SET id_grupo = " . intval($id) .  ", 
					id_modulo = " . intval($idModuloAnterior). ",
					acoes = \"" . substr($strAcoes,0,-1) . "\", 
					usuario_criacao = " . intval($_SESSION["idLogin"]) . ", 
					data_criacao = NOW() ");		
				$strAcoes = "";
			}
		}
		echo "<!--reload-->";
	}
	
	function do_after_excluir($id)
	{
		global $db, $modulo;
		
		$db->exec_query("DELETE FROM " . DBTABLE_CMS_MODULOS_GRUPOS_USUARIOS . " WHERE id_grupo = " . intval($id) .  " ");
	}
	
	function do_after_editar($id)
	{
		$this->do_after_incluir($id);
	}
}
    
?>