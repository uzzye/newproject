<?php
include_once(ROOT_CMS . "classes/models/produtos_categorias_model.php");
include_once(ROOT_CMS . "classes/models/produtos_imagens_model.php");

class produtos_model extends model{

        public $categorias;
        public $titulo;
        public $imagem;
        public $descricao_pt;
        public $descricao_en;
        public $descricao_es;
        public $caracteristicas_pt;
        public $caracteristicas_en;
        public $caracteristicas_es;
        public $chamada_pt;
        public $chamada_en;
        public $chamada_es;
        public $destaque;
        public $mostrar_menu;
        public $produtos_relacionados;

        public $url_loja;
        public $target_loja;
        public $manual;

      function __construct(){
            // Instancia o Objeto
            $this->nome_tabela = DBTABLE_PRODUTOS;
            $this->reference_models[sizeof($this->reference_models)] = array("categorias","produtos_categorias","id");
            $this->reference_models[sizeof($this->reference_models)] = array("produtos_relacionados","produtos","id");
           $this->array_crop_fields = array("imagem");
           $this->array_file_fields = array("manual");

            $this->reference_img_gallery[sizeof($this->reference_img_gallery)] = array("produtos_imagens", "id_produto");
            
            parent::__construct();
      }
}
    
?>