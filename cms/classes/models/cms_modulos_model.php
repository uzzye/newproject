<?php

class cms_modulos_model extends model{

	public $nome;
	public $link;
	public $modulo_pai;
	public $cor;
	public $icon_class;
	public $nome_en;
    
    function __construct(){
		// Instancia o Objeto
		$this->nome_tabela = DBTABLE_CMS_MODULOS;	
		$this->foreign_keys[sizeof($this->foreign_keys)] = array("modulo_pai","cms_modulos","id");
		
		parent::__construct();
    }	
	
	function before_excluir()
	{
		global $db, $modulo;

		$db->exec_query("DELETE FROM " . DBTABLE_CMS_MODULOS_USUARIOS . " WHERE id_modulo = " . intval($_REQUEST["id"]) .  " ");

		$db->exec_query("DELETE FROM " . DBTABLE_CMS_MODULOS_GRUPOS_USUARIOS . " WHERE id_modulo = " . intval($_REQUEST["id"]) .  " ");
	}
	
	function do_excluir()
	{
		global $db, $modulo;

		$this->before_excluir();
		
		return parent::do_excluir();
	}
	
	function do_excluir_massa()
	{
		global $db, $modulo;
		
		$this->before_excluir();
		
		return parent::do_excluir_massa();
	}
}

?>