<?php

include_once(ROOT_CMS . "classes/models/teste_model.php");

class itens_teste_model extends model{

	public $id_teste; // int(11) DEFAULT NULL,
    public $label; // varchar(255) DEFAULT NULL,;
    public $imagem; // varchar(255) DEFAULT NULL,;
    public $imagem_thumb; // varchar(255) DEFAULT NULL,;
    
    function __construct(){
		// Instancia o Objeto
		$this->nome_tabela = DBTABLE_ITENS_TESTE;
		
		parent::__construct();
			
		$this->reference_models[sizeof($this->reference_models)] = array("id_teste","teste","id");

		//$this->array_crop_fields = array("imagem","imagem_thumb");
		$this->array_file_fields = array("imagem","imagem_thumb");
    }
}

?>