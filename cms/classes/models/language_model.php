<?php

require_once(ROOT_CMS . "classes/models/idiomas_model.php");

class language_model extends model{

	public $id_registro;
	public $modulo_registro;
	public $id_idioma;
	public $variavel;
	public $valor;
	
    function __construct(){
		// Instancia o Objeto
		$this->nome_tabela = DBTABLE_LANG;
		
		$this->foreign_keys[sizeof($this->foreign_keys)] = array("id_idioma","idiomas","id");
		
		parent::__construct();
    }
}

?>