<?php
include_once(ROOT_CMS . "classes/models/noticias_tags_model.php");

class noticias_model extends model{

        public $titulo_pt;
        public $titulo_en;
        public $titulo_es;
        public $descricao_pt;
        public $descricao_en;
        public $descricao_es;
        public $imagem;
        public $tags;

      function __construct(){
            // Instancia o Objeto
            $this->nome_tabela = DBTABLE_NOTICIAS;
            $this->array_crop_fields = array("imagem");
            $this->reference_models[sizeof($this->reference_models)] = array("tags","noticias_tags"," id");
            $this->foreign_keys[sizeof($this->foreign_keys)] = array("tags","noticias_tags"," id");
            
            parent::__construct();
      }
}
    
?>