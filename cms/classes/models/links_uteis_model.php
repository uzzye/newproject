<?php
include_once(ROOT_CMS . "classes/models/idiomas_model.php");

class links_uteis_model extends model{

		public $referencia;
		public $url;
		public $titulo;
		public $id_idioma;
		public $target;

		function __construct(){
			// Instancia o Objeto
			$this->nome_tabela = DBTABLE_LINKS_UTEIS;
			$this->array_required_fields = array("referencia","titulo","url","id_idioma");

			$this->foreign_keys[sizeof($this->foreign_keys)] = array("id_idioma","idiomas","id");
			
			parent::__construct();
		}
}
    
?>