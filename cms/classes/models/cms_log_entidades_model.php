<?php

class cms_log_entidades_model extends model{

		public $registro_serializado;
		public $nome_entidade;
		public $id_registro;
		public $acao;

		function __construct(){
			// Instancia o Objeto
			$this->nome_tabela = DBTABLE_CMS_LOG_ENTIDADES;
			$this->array_required_fields = array("registro_serializado","nome_entidade","id_registro","acao");
			
			parent::__construct();
		}
}
    
?>