<?php

//include_once(ROOT_CMS . "classes/models/idiomas_model.php");

class conteudos_model extends model{

	public $texto;
	public $texto_capa;
	public $titulo;
	public $subtitulo;
	public $nome_arquivo;
	public $banner_topo;
	public $meta_descricao;
	public $meta_palavras_chave;
	public $id_idioma;
    
    function __construct(){
		// Instancia o Objeto
		$this->nome_tabela = DBTABLE_CONTEUDOS;
		
		$this->foreign_keys[sizeof($this->foreign_keys)] = array("id_idioma","idiomas","id");
		
		parent::__construct();
		
		$this->array_crop_fields = array("banner_topo");
    }
}

?>