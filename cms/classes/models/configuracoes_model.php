<?php
include_once(ROOT_CMS . "classes/models/idiomas_model.php");

class configuracoes_model extends model{

		public $idioma_padrao;
		public $email_padrao_contato;
		public $email_default_from;
		public $titulo_padrao;
		public $descricao_padrao;
		public $palavras_chave_padrao;
		public $url_amigaveis;
		public $charset_padrao;
		public $qtd_itens_pp;
		public $qtd_group_pag;
		public $id_idioma;
		public $codigo_analytics;
		public $codigo_css;
		public $codigo_javascript;
		public $geolocalizacao;
		public $mapa_embed;
		public $nome_default_from;
		public $servidor_smtp;
		public $porta_smtp;
		public $secure_smtp;
		public $servidor_pop;
		public $porta_pop;
		public $secure_pop;
		public $usuario_email;
		public $senha_email;

		// v 3.0
		public $ga_user;
		public $ga_props;
		public $valor_conversao_contato;

		function __construct(){
			// Instancia o Objeto
			$this->nome_tabela = DBTABLE_CONFIGURACOES;
			$this->foreign_keys[sizeof($this->foreign_keys)] = array("id_idioma","idiomas","id");
$this->array_required_fields = array("idioma_padrao","email_padrao_contato","email_default_from","titulo_padrao","descricao_padrao","palavras_chave_padrao","url_amigaveis","charset_padrao","qtd_itens_pp","qtd_group_pag","id_idioma");
			
			parent::__construct();
		}
}
    
?>