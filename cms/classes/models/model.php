<?php

/*
 * a classe model é diretamente referenciada às tabelas no banco de dados 
 * 
 * */

class model {

    /* 
    * - as tabelas no banco de dados devem conter OBRIGATORIAMENTE as propriedades id, data_criacao, data_atualizacao, usuario_criacao, usuario_atualizacao
 	* - nome_tabela -> se refere ao nome da tabela no banco de dados. é declarada nas classes derivadas
 	*/
 
 	//v1.0
    public $id;
    public $data_criacao;
	public $data_atualizacao;
	public $usuario_criacao;
	public $usuario_atualizacao;
	//v2.0
	public $ativo;
	//v3.0
	public $permalink;
	public $views;
	public $ranking;
	
	public $owner;
	public $controller;
	
	public $SQL_JOIN_FIELDS;
	public $SQL_JOIN_TABLES; 

    // vari�veis internas da classe
    public $nome_tabela;
	public $primary_keys = array("id");  /* array de chaves prim�rias. serve para referenciar as informa��es de get do objeto no BD. */
	
	public $foreign_keys = array(array("usuario_criacao","cms_usuarios","id"),array("usuario_atualizacao","cms_usuarios","id"));      	
	/* array de chaves estrangeiras. serve para referenciar objetos filhos nas inst�ncias e manuten��es.   
	exemplo de utiliza��o em objetos filhos: 
	$this->foreign_keys[sizeof($this->foreign_keys)] = array("campoDaClasseETabelaAtual","nomeDaClasseReferencia","nomeDoCampoDaClasseReferencia");*/  
	
	public $reference_items = array();
	/*array de entidades de refer�ncia, funciona como o array de chaves estrangeiras:
	$this->reference_items[sizeof($this->reference_items)] = array("campoDaClasseETabelaAtual","nomeDaClasseReferencia","campoDaClasseReferencia");*/

	// v3.0
	public $reference_models = array();
	/*array de models de entidades de referência, funciona como o array de chaves estrangeiras:
	$this->reference_models[sizeof($this->reference_models)] = array("campoDaClasseETabelaAtual","nomeDaClasseReferencia","campoDaClasseReferencia");*/

	public $reference_data_tables = array();
	/*array de tabelas de refer�ncia para dataTables, funciona como o array de chaves estrangeiras, porém usa sempre o ID da classe atual
	$this->reference_data_tables[sizeof($this->reference_data_tables)] = array("nomeDaClasseReferencia","campoDaClasseReferencia");*/

	public $reference_img_gallery = array();
	/*array de tabelas de refer�ncia para galerias de imagens, funciona como o array de chaves estrangeiras, porém usa sempre o ID da classe atual
	$this->reference_img_gallery[sizeof($this->reference_img_gallery)] = array("nomeDaClasseReferencia","campoDaClasseReferencia");*/

	public $reference_file_gallery = array();
	/*array de tabelas de refer�ncia para galerias de arquivos, funciona como o array de chaves estrangeiras, porém usa sempre o ID da classe atual
	$this->reference_file_gallery[sizeof($this->reference_file_gallery)] = array("nomeDaClasseReferencia","campoDaClasseReferencia");*/
	
	public $array_file_fields = array();
	//array de campos de arquivo para upload;
	
	public $array_crop_fields = array();
	//array de campos de arquivo para upload;
	
	public $array_exibition_fields = array();
	//array de campos de arquivo para tratamento de exibição;	
	
	public $array_required_fields = array();
	//array de campos obrigatorios no formulario

	public $upload_folder = UPLOAD_FOLDER;
	public $upload_folders = array();

	public $crop_original = true;
    
    function __construct($owner = null)
	{
		$this->owner = $owner;
    	$this->controller = $this->owner;
	}
	
	function set_owner($owner){ 
    	$this->owner = $owner;
    	$this->controller = $this->owner;
    }
	
	// PHP 7 REMOVE
	/*public function __get($name) { 
		if(property_exists(get_class($this), $name)) {
       		return $this->$name;
		} else {
       		//echo "Erro ao obter valor de propriedade: " . $name . " @ " . $this->nome_tabela . "<br/><br/>";  
       		return null;
       	}
    }  
    
    public function __set($name, $value) {    
        $this->$name = $value;  
    }
    
    public function __isset($name) {    
        return isset($this->$name);  
    } 
    
    public function __unset($name) {    
        unset($this->$name);  
        return true;
    }*/
	
	function count_reg_ativos($sqlWhere = "",$join = true,$deepJoin = false)
    {
    	return $this->count_reg($sqlWhere,$join,true,$deepJoin);
    }
	
	function count_reg($sqlWhere = "",$join = true,$apenasAtivos = false,$deepJoin = false)
	{
		global $db;
			
		$sqlCmd = "SELECT COUNT(*) count_reg FROM " . $this->nome_tabela . " principal ";
		
		$sqlLeftJoin = "";
		if($join)
		{
		  	for($w=0;$w<sizeof($this->foreign_keys);$w++)
		  	{
		  	  	$joins = $this->get_joins($this->foreign_keys[$w],$w,"principal",$deepJoin);
				$sqlLeftJoin .= $joins[0];
	      	}
  			
  			$sqlCmd .= " " . $sqlLeftJoin . " ";	
		}
		
		if(trim($sqlWhere) <> "")
		{$sqlCmd .= " WHERE (true) AND " . $sqlWhere . " ";}
		
		if($apenasAtivos)
		{$sqlCmd .= " AND principal.ativo = 1 ";}

		$resCmd = $db->exec_query($sqlCmd);				
		
		return intval($db->result_field($resCmd,0,"count_reg"));
	}
	
	function get_result_dados($sqlWhere = "",$join = true)
	{
		global $db;		
		
		$sqlCmd = "SELECT principal.* ";
		if(sizeof($this->array_exibition_fields) > 0)
		{
			$sizeof = sizeof($this->array_exibition_fields);
			for($w=0;$w<$sizeof;$w++)
			{
				$array_aux = $this->array_exibition_fields[$w];				
				$sqlCmd .= ", " . $array_aux[1] . " AS " . $array_aux[0];				
			}			
		}		
		if(trim($this->SQL_JOIN_FIELDS) <> "" && $join)
		{
			$sqlCmd .= ", " . $this->SQL_JOIN_FIELDS;
		}

		$sqlLeftJoin = "";
		
		if($join) {
			for($w=0;$w<sizeof($this->foreign_keys);$w++)
			{
				$joins = $this->get_joins($this->foreign_keys[$w],$w,"principal",$deepJoin);
				$sqlLeftJoin .= $joins[0];
				$sqlCmd .= $joins[1];
			}
		}

		$sqlCmd .= " FROM " . $this->nome_tabela . " principal ";
		
		if($join) {
			if(trim($this->SQL_JOIN_TABLES) <> "")
		 	{
				$sqlCmd .= " " . $this->SQL_JOIN_TABLES;
			} 
			
	  		$sqlCmd .= " " . $sqlLeftJoin . " ";
	  	}

	  	$sqlCmd .= " WHERE (true) ";

  		if(trim($sqlWhere) != "") {
  			$sqlCmd .= " AND " . $sqlWhere . " ";
  		}

	    for($w=0;$w<sizeof($this->primary_keys);$w++)
		{
			$pk = $this->primary_keys[$w];
			$sqlCmd .= " AND principal." . $this->primary_keys[$w] . " = \"" . $this->get_var($pk) . "\" ";
		}			
		$sqlCmd .= " GROUP BY principal.id ";

		//var_dump($sqlCmd);

		$resCmd = $db->exec_query($sqlCmd);

		return $resCmd;
	}
	
	function get_result($sqlWhere = "",$sqlOrderBy = "",$sqlLimit= "",$join = false, $deepJoin = false, $groupBy = "",$array_expressoes_sql = "")
	{
		global $db;
			
		$sqlCmd = "SELECT principal.* ";		
		if(sizeof($this->array_exibition_fields) > 0)
		{
			$sizeof = sizeof($this->array_exibition_fields);
			for($w=0;$w<$sizeof;$w++)
			{
				$array_aux = $this->array_exibition_fields[$w];
				$sqlCmd .= ", " . $array_aux[1] . " AS " . $array_aux[0];				
			}			
		}		

		// PEGA EXPRESSOES SQL E ADICIONA COMO CAMPOS
		if(is_array($array_expressoes_sql) && sizeof($array_expressoes_sql) > 0)
		{
			$sizeof = sizeof($array_expressoes_sql);
			for($w=0;$w<$sizeof;$w++)
			{
				if(trim($array_expressoes_sql[$w]) != "") {
					$sqlAux = $array_expressoes_sql[$w];
					$sqlCmd .= ", (" . $sqlAux . ") AS sqlExprField" . $w . "";
				}
			}
		}

		$sqlLeftJoin = "";

		if($join) {		
			if(trim($this->SQL_JOIN_FIELDS) <> "")
			{
				$sqlCmd .= ", " . $this->SQL_JOIN_FIELDS;
			}

		  	for($w=0;$w<sizeof($this->foreign_keys);$w++)
		  	{
		  	  	$joins = $this->get_joins($this->foreign_keys[$w],$w,"principal",$deepJoin);
				$sqlLeftJoin .= $joins[0];
				$sqlCmd .= $joins[1];
	      	}
		}

		$sqlCmd .= " FROM " . $this->nome_tabela . " principal ";
		
		if($join)
		{
			$sqlCmd .= " " . $sqlLeftJoin . " ";

			if(trim($this->SQL_JOIN_TABLES) <> "")
			{
			$sqlCmd .= " " . $this->SQL_JOIN_TABLES;
			} 
		}
		
		if(trim($sqlWhere) <> "")
		{$sqlCmd .= " WHERE (true) AND " . $sqlWhere . " ";}

		if(trim($groupBy) != "") {
			$sqlCmd .= " GROUP BY " . $groupBy . " ";
		} else {
			$sqlCmd .= " GROUP BY principal.id ";
		}
		if(trim($sqlOrderBy) <> "")
		{$sqlCmd .= " ORDER BY " . $sqlOrderBy . " ";}
		if(trim($sqlLimit) <> "")
		{$sqlCmd .= " LIMIT " . $sqlLimit . " ";}
		
		//echo $sqlCmd . "<br/><br/>"; //die();
		
		$resCmd = $db->exec_query($sqlCmd);
				
		return $resCmd;
	}
	
	function get_result_sql($sqlCmd)
	{
		global $db;
			
		$resCmd = $db->exec_query($sqlCmd);		
		
		return $resCmd;
	}
	
	function get_result_field($res,$row,$field)
	{
		global $db;
			
		$valor = $db->result_field($res,$row,$field);		
		
		return $valor;
	}
	
	function get_fields_name($result = null)
	{
		global $db;
		
		if($result == null)
		{
			$array_aux = array();
			$sqlCmd = "DESCRIBE " . $this->nome_tabela;
			$resCmd = $db->exec_query($sqlCmd);
			for($w=0;$w<$db->num_rows($resCmd);$w++)
			{
				$array_aux[sizeof($array_aux)] = $db->result_field($resCmd,$w,"Field");				
			}
			//var_dump($array_aux); echo '<br/><br/>';
			return $array_aux;
		}
		else
		{
			//var_dump($db->fields_array($result)); echo '<br/><br/>';
			return $db->fields_array($result);
		}
	}
	
	function get_fields_type()
	{
		global $db;
	
		$array_aux = array();
		$sqlCmd = "DESCRIBE " . $this->nome_tabela;
		$resCmd = $db->exec_query($sqlCmd);
		for($w=0;$w<$db->num_rows($resCmd);$w++)
		{
			$array_aux[sizeof($array_aux)] = $db->result_field($resCmd,$w,"Type");				
		}
		return $array_aux;
	}
	
	function result_num_rows($resCmd)
	{
		global $db;
		return $db->num_rows($resCmd);
	}
	
	function inicia_dados()
	{
		$this->limpa_dados();
		
		$this->set_var("usuario_criacao","");
		$this->set_var("data_criacao","");
		$this->set_var("id","");
		$this->set_var("ativo",1);

		/*$class = get_class($this);		       
	    $keys = array_keys(get_class_vars($class));
			
    	$w=1;		
	 	while($w<sizeof($keys))
		{
			$keys[$w] = "";
            $w++;
		}*/
	}
	
	function carrega_dados()
	{
		global $db;

		$resCmd = $this->get_result_dados();

		//var_dump($resCmd); echo "<br/><br/>";
		
		if($this->result_num_rows($resCmd))
		{
    		$class = get_class($this);		
         	$keys = array_keys(get_class_vars($class));
			$fields_tabela = $this->get_fields_name($resCmd);
			
    		$w=0;		
	 	    while($w<sizeof($keys))
		    { 		
		      //if($this->get_var($keys[$w]) == null) // v3.0
		      {
				  if(in_array($keys[$w],$fields_tabela,true))
			      {
  		            $this->set_var($keys[$w],$this->get_result_field($resCmd,0,$keys[$w]));
			      }
		      }
		      $w++;
			}

			if(sizeof($this->array_exibition_fields) > 0)
			{
				$sizeof = sizeof($this->array_exibition_fields);
				for($w=0;$w<$sizeof;$w++)
				{	
					$array_aux = $this->array_exibition_fields[$w];
					$this->set_var($array_aux[0],$this->get_result_field($resCmd,0,$this->array_exibition_fields[$w][0]));
				}			
			}
		}
		else
		{
			$this->limpa_dados();
		}
	}
	
	function limpa_dados()
	{
		$class = get_class($this);		       
	    $keys = array_keys(get_class_vars($class));
			
    	$w=1;		
	 	while($w<sizeof($keys))
		{
			$keys[$w] = "";
            $w++;
		}
	}
	
	function get_objeto_referencia($campo)
	{				    
	    $objAux = null;		
		$w=0;	
	    while($w<sizeof($this->foreign_keys))
	    {    		
	      if($this->foreign_keys[$w] != "")			
		  {			  
			  $arrayAux = $this->foreign_keys[$w];			  
			  if($arrayAux[0] == $campo)
			  {
				$classe = $arrayAux[1] . "_model";
				$objAux = new $classe();				
				$objAux->set_var($arrayAux[2],$this->get_var($arrayAux[0]));
				$objAux->carrega_dados();
				break;
			  }
		  }
		  $w++;		  
	    }	
		
		return $objAux;
	}	
	
	function inclui($cms = true,$last_id = false)
	{
		global $modulo, $db;
		
		$class = get_class($this);
		//$vars = array_keys(get_class_vars($class));
		$vars = $this->get_fields_name();
		
		$array_campos_ja_utilizados = array();
				
		$w=0;
		$i_count = 0;
		while($w<sizeof($vars))
		{			
			$nomeCampo = $vars[$w];			
			
			//if($nomeCampo <> "id" && $nomeCampo <> "data_criacao" && $nomeCampo <> "usuario_criacao" && $nomeCampo <> "data_atualizacao" && $nomeCampo <> "usuario_atualizacao" && $nomeCampo <> "ativo")
			if(($nomeCampo <> "id" && 
				$nomeCampo <> "data_criacao" && 
				$nomeCampo <> "usuario_criacao" && 
				$nomeCampo <> "data_atualizacao" && 
				$nomeCampo <> "usuario_atualizacao" && 
				$nomeCampo <> "ativo") 
				|| trim($this->get_var($nomeCampo)) != ""
			)
			{					
				if(!in_array($nomeCampo,$array_campos_ja_utilizados,true) && trim($this->get_var($nomeCampo)) <> "")
				{
					if($i_count>0)
					{
						$str_campos .= ",";
						$str_valores .= ",";
					}
				
					$str_campos .= $nomeCampo;
					$auxVal = "\"" . $db->escape_string($this->get_var($nomeCampo)) . "\"";
								
					foreach($this->foreign_keys as $fks)
					{
						if($fks[0] == $nomeCampo && intval($this->get_var($nomeCampo)) == 0)
						{			
							$auxVal = "NULL";
							break;									
						}
					}
					$str_valores .= $auxVal;	
										
					$array_campos_ja_utilizados[sizeof($array_campos_ja_utilizados)] = $nomeCampo;
					
					$i_count++;
				}
			}
			
			$w++;
		}
		
		if($cms)
		{
			$id_usu_aux = $_SESSION["idLogin"]; 
		}
		else
		{
			$id_usu_aux = $_SESSION["idLogin_site"];
		}
		
		if(intval($this->usuario_criacao) > 0)
		{
			$id_usu_aux	= intval($this->usuario_criacao);
		}
		
		if(intval($id_usu_aux) == 0)
		{
			$id_usu_aux = 99; // INCLUI COMO VISITANTE
		}
		
		//$sqlCmd = "INSERT INTO " . $this->nome_tabela . " (" . $str_campos . ",usuario_criacao,data_criacao) VALUES (" . $str_valores . "," . intval($id_usu_aux). ",NOW())";

		$sqlCmd = "INSERT INTO " . $this->nome_tabela . " (" . $str_campos . "";
		$sqlValuesAux = "";
		$nomeCampo = "usuario_criacao";
		if(!in_array($nomeCampo,$array_campos_ja_utilizados,true) && in_array($nomeCampo,$vars,true))
		{
			$sqlCmd .= "," . $nomeCampo;
			$sqlValuesAux .= "," . intval($id_usu_aux) . "";
		}
		$nomeCampo = "data_criacao";
		if(!in_array($nomeCampo,$array_campos_ja_utilizados,true) && in_array($nomeCampo,$vars,true))
		{
			$sqlCmd .= "," . $nomeCampo;
			$sqlValuesAux .= ",NOW()";
		}
		/*$nomeCampo = "ativo";
		if(!in_array($nomeCampo,$array_campos_ja_utilizados,true) && in_array($nomeCampo,$vars,true))
		{
			$sqlCmd .= "," . $nomeCampo;
			$sqlValuesAux .= ",1";
		}*/
		$sqlCmd .= ") VALUES (" . $str_valores . "" . $sqlValuesAux . ")";

		//echo $sqlCmd ."<br/><br/>";
		
		$array_result = $db->exec_query($sqlCmd, true);		
		$result = $array_result[0];		
		$insert_id = $array_result[1];
		
		if($last_id){return $insert_id;}
		else{return $result;}
	}
	
	function exclui($cms = true)
	{
		global $modulo, $db;

		foreach($this->array_crop_fields as $field) {

			$endereco_aux = $this->get_upload_folder($field);

			@unlink($endereco_aux . $this->get_var($field));
		}
		foreach($this->array_file_fields as $field) {

			$endereco_aux = $this->get_upload_folder($field);

			@unlink($endereco_aux . $this->get_var($field));
		}
		
		$sqlCmd = "DELETE FROM " . $this->nome_tabela . " WHERE id = " . intval($this->id) . " ";	
		$resCmd = $db->exec_query($sqlCmd);
		
		return $resCmd;
	}
	
	function edita($cms = true, $last_id = false)
	{
		global $modulo, $db;
		
		$class = get_class($this);
		//$vars = array_keys(get_class_vars($class));
		$vars = $this->get_fields_name();
		
		$array_campos_ja_utilizados = array();		
				
		$w=0;
		$i_count = 0;
		while($w<sizeof($vars))
		{			
			$nomeCampo = $vars[$w];
			
			//if($nomeCampo <> "id" && $nomeCampo <> "data_criacao" && $nomeCampo <> "usuario_criacao" && $nomeCampo <> "data_atualizacao" && $nomeCampo <> "usuario_atualizacao")
			{
				if(!in_array($nomeCampo,$array_campos_ja_utilizados,true))
				{
					if($i_count>0)
					{
						$str_campos .= ",";
					}
					
					$auxVal = $nomeCampo . " = \"" . $db->escape_string($this->get_var($nomeCampo)) . "\"";
								
					foreach($this->foreign_keys as $fks)
					{
						if($fks[0] == $nomeCampo && intval($this->get_var($nomeCampo)) == 0)
						{			
							$auxVal = $nomeCampo . " = NULL";	
							break;								
						}
					}
					
					$str_campos .= $auxVal;	
					
					if(($nomeCampo <> "id" && 
						//$nomeCampo <> "data_criacao" && 
						//$nomeCampo <> "usuario_criacao" && 
						//$nomeCampo <> "data_atualizacao" && 
						//$nomeCampo <> "usuario_atualizacao" && 
						$nomeCampo <> "ativo") 
						|| trim($this->get_var($nomeCampo)) != ""
					){
						$array_campos_ja_utilizados[sizeof($array_campos_ja_utilizados)] = $nomeCampo;
					}
					
					$i_count++;
				}
			}
				
			$w++;
		}
		
		if($cms)
		{
			$id_usu_aux = $_SESSION["idLogin"]; 
		}
		else
		{
			$id_usu_aux = $_SESSION["idLogin_site"];
		}
		
		if(intval($this->usuario_criacao) > 0)
		{
			$id_usu_aux	= intval($this->usuario_criacao);
		}
		
		if(intval($id_usu_aux) == 0)
		{
			$id_usu_aux = 99; // INCLUI COMO VISITANTE
		}
		
		//$sqlCmd = "UPDATE " . $this->nome_tabela . " SET " . $str_campos . ",usuario_atualizacao = " . intval($id_usu_aux). ",data_atualizacao = NOW() WHERE id = " . intval($this->id) . " ";

		$sqlCmd = "UPDATE " . $this->nome_tabela . " SET " . $str_campos . "";
		$nomeCampo = "usuario_atualizacao";
		if(!in_array($nomeCampo,$array_campos_ja_utilizados,true) && in_array($nomeCampo,$vars,true))
		{
			$sqlCmd .= "," . $nomeCampo . " = " . intval($id_usu_aux). "";
		}
		$nomeCampo = "data_atualizacao";
		if(!in_array($nomeCampo,$array_campos_ja_utilizados,true) && in_array($nomeCampo,$vars,true))
		{
			$sqlCmd .= "," . $nomeCampo . " = NOW()";
		}
		/*$nomeCampo = "ativo";
		if(!in_array($nomeCampo,$array_campos_ja_utilizados,true) && in_array($nomeCampo,$vars,true))
		{
			$sqlCmd .= "," . $nomeCampo . " = 1";
		}*/
		$sqlCmd .= " WHERE id = " . intval($this->id) . " ";
		
		$array_result = $db->exec_query($sqlCmd, true);
		$result = $array_result[0];
		//$insert_id = $array_result[1];
		$insert_id = $this->id;
		
		if($last_id){return $insert_id;}
		else{return $result;}
	}

	function do_incluir($array_form_campos,$array_form_refs,$array_form_campos_padrao)
	{
		global $modulo, $db;
		
		$str_campos = "";
		$str_valores = "";	

		$class = get_class($this);
		$keys = array_keys(get_class_vars($class));
		
		$array_campos_ja_utilizados = array();
				
		$i_count = 0;

		$w=0;
		while($w<sizeof($array_form_campos))
		{			
			$field = $array_form_campos[$w];
			$nomeCampo = $field->name;
			
			if(!in_array($nomeCampo,$array_campos_ja_utilizados,true) && in_array($nomeCampo,$keys,true))
			{
				if($i_count>0)
				{
					$str_campos .= ",";
					$str_valores .= ",";
				}
			
				$str_campos .= $nomeCampo;
				$auxVal = "\"" . $this->get_var($nomeCampo) . "\"";
				//$auxVal = "\"" . $db->escape_string($this->get_var($nomeCampo)) . "\"";
								
				foreach($this->foreign_keys as $fks)
				{
					if($fks[0] == $nomeCampo && intval($this->get_var($nomeCampo)) == 0)
					{			
						$auxVal = "NULL";	
						break;								
					}
				}
				$str_valores .= $auxVal;					
				
				$array_campos_ja_utilizados[sizeof($array_campos_ja_utilizados)] = $nomeCampo;
				
				$i_count++;
			}
			
			$w++;
		}

		$w=0;
		while($w<sizeof($array_form_campos_padrao))
		{			
			$field = $array_form_campos_padrao[$w];
			$nomeCampo = $field->name;
			
			if(!in_array($nomeCampo,$array_campos_ja_utilizados,true) && in_array($nomeCampo,$keys,true))
			{
				if($i_count>0)
				{
					$str_campos .= ",";
					$str_valores .= ",";
				}
			
				$str_campos .= $nomeCampo;
				$auxVal = "\"" . $this->get_var($nomeCampo) . "\"";
				//$auxVal = "\"" . $db->escape_string($this->get_var($nomeCampo)) . "\"";
								
				foreach($this->foreign_keys as $fks)
				{
					if($fks[0] == $nomeCampo && intval($this->get_var($nomeCampo)) == 0)
					{			
						$auxVal = "NULL";	
						break;								
					}
				}
				$str_valores .= $auxVal;					
				
				$array_campos_ja_utilizados[sizeof($array_campos_ja_utilizados)] = $nomeCampo;
				
				$i_count++;
			}
			
			$w++;
		}
		
		/*if(trim($str_campos) <> "")
		{$str_campos .= ",";}
		if(trim($str_valores) <> "")
		{$str_valores .= ",";}*/

		$bCampos = false;
		if(trim($str_campos) != "") {
			$bCampos = true;
		}
		
		$sqlCmd = "INSERT INTO " . $this->nome_tabela . " (" . $str_campos . "";
		$sqlValuesAux = "";
		$nomeCampo = "usuario_criacao";
		if(!in_array($nomeCampo,$array_campos_ja_utilizados,true) && in_array($nomeCampo,$keys,true))
		{
			if($bCampos) { $sqlCmd .= ","; }
			$sqlCmd .= $nomeCampo;

			if($bCampos) { $sqlValuesAux .= ","; }
			$sqlValuesAux .= intval($_SESSION["idLogin"]) . "";

			$bCampos = true;
		}
		$nomeCampo = "data_criacao";
		if(!in_array($nomeCampo,$array_campos_ja_utilizados,true) && in_array($nomeCampo,$keys,true))
		{
			if($bCampos) { $sqlCmd .= ","; }
			$sqlCmd .= $nomeCampo;

			if($bCampos) { $sqlValuesAux .= ","; }
			$sqlValuesAux .= "NOW()";

			$bCampos = true;
		}
		$nomeCampo = "ativo";
		if(!in_array($nomeCampo,$array_campos_ja_utilizados,true) && in_array($nomeCampo,$keys,true))
		{
			if($bCampos) { $sqlCmd .= ","; }
			$sqlCmd .= $nomeCampo;

			if($bCampos) { $sqlValuesAux .= ","; }
			$sqlValuesAux .= "1";

			$bCampos = true;
		}
		$sqlCmd .= ") VALUES (" . $str_valores . "" . $sqlValuesAux . ")";

		$array_result = $db->exec_query($sqlCmd, true);
		$result = $array_result[0];
		$insert_id = $array_result[1];
		
		if(method_exists($this,"do_after_incluir"))
		{
			$this->do_after_incluir($insert_id);
		}		

		return $insert_id;
	}

	function do_editar($array_form_campos,$array_form_refs,$array_form_campos_padrao)
	{
		global $modulo, $db;
		
		$str_campos = "";
		
		$class = get_class($this);
		$keys = array_keys(get_class_vars($class));
		
		$array_campos_ja_utilizados = array();
				
		$i_count = 0;

		$w=0;
		while($w<sizeof($array_form_campos))
		{			
			$field = $array_form_campos[$w];
			$nomeCampo = $field->name;
			
			if((trim($field->value) != "") || ($field->type != "password" && $field->type != "file"))
			{
				if(!in_array($nomeCampo,$array_campos_ja_utilizados,true) && !in_array($nomeCampo,$this->array_file_fields,true) && !in_array($nomeCampo,$this->array_crop_fields,true) && in_array($nomeCampo,$keys,true))
				{
					if($i_count>0)
					{
						$str_campos .= ",";
					}
					
					$auxVal = $nomeCampo . " = \"" . $this->get_var($nomeCampo) . "\"";
					//$auxVal = $nomeCampo . " = \"" . $db->escape_string($this->get_var($nomeCampo)) . "\"";
								
					foreach($this->foreign_keys as $fks)
					{
						if($fks[0] == $nomeCampo && intval($this->get_var($nomeCampo)) == 0)
						{			
							$auxVal = $nomeCampo . " = NULL";		
							break;							
						}
					}		
			
					$str_campos .= $auxVal;				
				
					$array_campos_ja_utilizados[sizeof($array_campos_ja_utilizados)] = $nomeCampo;
					
					$i_count++;
				}
			}
			
			$w++;
		}
		
		$w=0;
		while($w<sizeof($array_form_campos_padrao))
		{			
			$field = $array_form_campos_padrao[$w];
			$nomeCampo = $field->name;
			
			if((trim($field->value) != "") || ($field->type != "password" && $field->type != "file"))
			{
				if(!in_array($nomeCampo,$array_campos_ja_utilizados,true) && !in_array($nomeCampo,$this->array_file_fields,true) && !in_array($nomeCampo,$this->array_crop_fields,true) && in_array($nomeCampo,$keys,true))
				{
					if($i_count>0)
					{
						$str_campos .= ",";
					}
					
					$auxVal = $nomeCampo . " = \"" . $this->get_var($nomeCampo) . "\"";
					//$auxVal = $nomeCampo . " = \"" . $db->escape_string($this->get_var($nomeCampo)) . "\"";
								
					foreach($this->foreign_keys as $fks)
					{
						if($fks[0] == $nomeCampo && intval($this->get_var($nomeCampo)) == 0)
						{			
							$auxVal = $nomeCampo . " = NULL";		
							break;							
						}
					}		
			
					$str_campos .= $auxVal;				
				
					$array_campos_ja_utilizados[sizeof($array_campos_ja_utilizados)] = $nomeCampo;
					
					$i_count++;
				}
			}
			
			$w++;
		}
		
		/*if(trim($str_campos) <> "")
		{$str_campos .= ",";}*/

		$bCampos = false;
		if(trim($str_campos) != "") {
			$bCampos = true;
		}
		
		//$sqlCmd = "UPDATE " . $this->nome_tabela . " SET " . $str_campos . "usuario_atualizacao = " . intval($_SESSION["idLogin"]). ",data_atualizacao = NOW() WHERE id = " . intval($this->id) . " ";

		$sqlCmd = "UPDATE " . $this->nome_tabela . " SET " . $str_campos . "";
		$nomeCampo = "usuario_atualizacao";
		if(!in_array($nomeCampo,$array_campos_ja_utilizados,true) && in_array($nomeCampo,$keys,true))
		{
			if($bCampos) { $sqlCmd .= ","; }
			$sqlCmd .= $nomeCampo . " = " . intval($_SESSION["idLogin"]). "";
			$bCampos = true;
		}
		$nomeCampo = "data_atualizacao";
		if(!in_array($nomeCampo,$array_campos_ja_utilizados,true) && in_array($nomeCampo,$keys,true))
		{
			if($bCampos) { $sqlCmd .= ","; }
			$sqlCmd .= $nomeCampo . " = NOW()";
			$bCampos = true;
		}
		$nomeCampo = "ativo";
		if(!in_array($nomeCampo,$array_campos_ja_utilizados,true) && in_array($nomeCampo,$keys,true))
		{
			if($bCampos) { $sqlCmd .= ","; }
			$sqlCmd .= $nomeCampo . " = 1";
			$bCampos = true;
		}
		$sqlCmd .= " WHERE id = " . intval($this->id) . " ";

		//echo $sqlCmd; die();
		
		$resCmd = $db->exec_query($sqlCmd);
		
		if(method_exists($this,"do_after_editar"))
		{
			$this->do_after_editar($this->id);
		}

		return $resCmd;
		
	}

	function do_excluir()
	{
		global $modulo, $db, $mySQL_prefix;	

		// Remove langs

		$sqlCmd = "DELETE FROM " . DBTABLE_LANG . " WHERE id_registro = " . intval($_REQUEST["id"]) . " AND modulo_registro = \"" . output_decode($modulo) . "\" ";
		$db->exec_query($sqlCmd, true);

		// Remove arquivos de galeria de Imagens

		if(is_array($this->reference_img_gallery) && sizeof($this->reference_img_gallery) > 0) {
			foreach($this->reference_img_gallery as $refAux) {
				$classAux = $refAux[0];
				carrega_classe($classAux);
				
				// Se é módulo
				if(class_exists($classAux)) {
					$objsAux = new $classAux();
					$nomeTabela = $objsAux->get_nome_tabela();

					$objsAux = $objsAux->localiza(array($refAux[1]), array(intval($_REQUEST["id"])));
					if(is_array($objsAux) && sizeof($objsAux) > 0) {
						foreach($objsAux as $objAux) {
							foreach($objAux->array_crop_fields as $field_name) {
								@unlink($objAux->get_upload_folder($field_name) . $objAux->get_var($field_name));
								$objAux->exclui();
							}
						}
					}
				}
			}
		}

		// Remove arquivos de galeria de Files

		if(is_array($this->reference_file_gallery) && sizeof($this->reference_file_gallery) > 0) {
			foreach($this->reference_file_gallery as $refAux) {
				$classAux = $refAux[0];
				carrega_classe($classAux);
				
				// Se é módulo
				if(class_exists($classAux)) {
					$objsAux = new $classAux();
					$nomeTabela = $objsAux->get_nome_tabela();

					$objsAux = $objsAux->localiza(array($refAux[1]), array(intval($_REQUEST["id"])));
					if(is_array($objsAux) && sizeof($objsAux) > 0) {
						foreach($objsAux as $objAux) {
							foreach($objAux->array_crop_fields as $field_name) {
								@unlink($objAux->get_upload_folder($field_name) . $objAux->get_var($field_name));
								$objAux->exclui();
							}
						}
					}
				}
			}
		}

		// Remove dataTables	

		if(is_array($this->reference_data_tables) && sizeof($this->reference_data_tables) > 0) {
			foreach($this->reference_data_tables as $refAux) {
				$classAux = $refAux[0];
				carrega_classe($classAux);
				
				// Se é módulo
				if(class_exists($classAux)) {
					$objAux = new $classAux();
					$nomeTabela = $objAux->get_nome_tabela();
				
					// Apaga antigos
					$sqlCmd = "DELETE FROM " . $nomeTabela . " WHERE " . $refAux[1] . " = " . intval($_REQUEST["id"]) . " ";
					$db->exec_query($sqlCmd);
				}
			}
		}

		// Remove referencias

		if(is_array($this->reference_items) && sizeof($this->reference_items) > 0) {
			foreach($this->reference_items as $refAux) {

				// Se v3.0
				if(sizeof($refAux) > 3) {
					$classTable = $refAux[1];
					carrega_classe($classTable);
					$classAux = "";
					$nomeTabela = "";
					// Se é módulo
					if(class_exists($classTable)) {
						$classAux = $classTable;
						//$nomeTabela = $mySQL_prefix."_".$classTable;
						$objAux = new $classAux();
						$nomeTabela = $objAux->model->nome_tabela;
					}
					// Se é tabela
					else {
						$nomeTabela = $classTable;
						$classAux = str_replace($mySQL_prefix."_","",$classTable);
					}

					if(trim($nomeTabela) != "" && trim($classAux) != "") {
						// Apaga antigos
						$sqlCmd = "DELETE FROM " . $nomeTabela . " WHERE " . $refAux[2] . " = " . intval($this->get_var($refAux[0])) . " ";
						$db->exec_query($sqlCmd);
					}
				}
			}
		}

		// Remove arquivos

		$w=0;
		$sizeof= sizeof($this->array_file_fields);
		while($w<$sizeof)
		{
			$field_name = $this->array_file_fields[$w];	
			$this->remove_file($field_name);
			
			$w++;
		}
		
		$w=0;
		$sizeof= sizeof($this->array_crop_fields);
		while($w<$sizeof)
		{
			$field_name = $this->array_crop_fields[$w];		
			$this->remove_file($field_name);	
			
			$w++;
		}
		
		/*$log = new log_entidades();
		$log->inicia_dados();
		$log->set_var("registro_serializado",serialize($this));
		$log->set_var("nome_entidade",str_replace("_model","",get_class($this)));
		$log->set_var("id_registro",$id);
		$log->set_var("acao","e");
		$log->inclui();*/

		// Apaga registro
		
		$sqlCmd = "DELETE FROM " . $this->nome_tabela . " WHERE id = " . intval($this->id) . " ";	

		$resCmd = $db->exec_query($sqlCmd);

		if(method_exists($this,"do_after_excluir"))
		{
			$this->do_after_excluir($this->id);
		}
		
		return $resCmd;
	}

	function do_excluir_massa()
	{
		global $modulo, $db, $mySQL_prefix;

		if(trim($_REQUEST["regids"]) != "") {	

			// Remove langs

			$sqlCmd = "DELETE FROM " . DBTABLE_LANG . " WHERE id_registro IN (" . trim($_REQUEST["regids"]) . ") AND modulo_registro = \"" . output_decode($modulo) . "\" ";
			$db->exec_query($sqlCmd, true);

			// Remove dataTables

			if(is_array($this->reference_data_tables) && sizeof($this->reference_data_tables) > 0) {
				foreach($this->reference_data_tables as $refAux) {
					$classAux = $refAux[0];
					carrega_classe($classAux);
					
					// Se é módulo
					if(class_exists($classAux)) {
						$objAux = new $classAux();
						$nomeTabela = $objAux->get_nome_tabela();
					
						// Apaga antigos
						$sqlCmd = "DELETE FROM " . $nomeTabela . " WHERE " . $refAux[1] . " IN (" . trim($_REQUEST["regids"]) . ") ";
						$db->exec_query($sqlCmd);
					}
				}
			}

			// Remove arquivos de galeria de Images

			if(is_array($this->reference_img_gallery) && sizeof($this->reference_img_gallery) > 0) {
				foreach($this->reference_img_gallery as $refAux) {
					$classAux = $refAux[0];
					carrega_classe($classAux);
					
					// Se é módulo
					if(class_exists($classAux)) {
						$objsAux = new $classAux();
						$nomeTabela = $objsAux->get_nome_tabela();

						$objsAux = $objsAux->localiza(array($refAux[1]), array("(" . trim($_REQUEST["regids"]) . ")"), array(" IN "));
						if(is_array($objsAux) && sizeof($objsAux) > 0) {
							foreach($objsAux as $objAux) {
								foreach($objAux->array_crop_fields as $field_name) {
									@unlink($objAux->get_upload_folder($field_name) . $objAux->get_var($field_name));
									$objAux->exclui();
								}
							}
						}
					}
				}
			}

			// Remove arquivos de galeria de Files

			if(is_array($this->reference_file_gallery) && sizeof($this->reference_file_gallery) > 0) {
				foreach($this->reference_file_gallery as $refAux) {
					$classAux = $refAux[0];
					carrega_classe($classAux);
					
					// Se é módulo
					if(class_exists($classAux)) {
						$objsAux = new $classAux();
						$nomeTabela = $objsAux->get_nome_tabela();

						$objsAux = $objsAux->localiza(array($refAux[1]), array("(" . trim($_REQUEST["regids"]) . ")"), array(" IN "));
						if(is_array($objsAux) && sizeof($objsAux) > 0) {
							foreach($objsAux as $objAux) {
								foreach($objAux->array_crop_fields as $field_name) {
									@unlink($objAux->get_upload_folder($field_name) . $objAux->get_var($field_name));
									$objAux->exclui();
								}
							}
						}
					}
				}
			}

			// Remove referencias

			if(is_array($this->reference_items) && sizeof($this->reference_items) > 0) {
				foreach($this->reference_items as $refAux) {
					// Se v3.0
					if(sizeof($refAux) > 3) { 
						$classTable = $refAux[1];
						carrega_classe($classTable);
						$classAux = "";
						$nomeTabela = "";
						// Se é módulo
						if(class_exists($classTable)) {
							$classAux = $classTable;
							//$nomeTabela = $mySQL_prefix."_".$classTable;
							$objAux = new $classAux();
							$nomeTabela = $objAux->model->nome_tabela;
						}
						// Se é tabela
						else {
							$nomeTabela = $classTable;
							$classAux = str_replace($mySQL_prefix."_","",$classTable);
						}

						if(trim($nomeTabela) != "" && trim($classAux) != "") {
							// Apaga antigos
							$sqlCmd = "DELETE FROM " . $nomeTabela . " WHERE " . $refAux[2] . " IN (" . trim($_REQUEST["regids"]) . ") ";
							$db->exec_query($sqlCmd);
						}
					}
				}
			}		

			// Apaga registros
		
			$sqlCmd = "DELETE FROM " . $this->nome_tabela . " WHERE id IN (" . trim($_REQUEST["regids"]) . ") ";
			$resCmd = $db->exec_query($sqlCmd);
			
			if(method_exists($this,"do_after_excluir_massa"))
			{
				$this->do_after_excluir_massa();
			}
			
			return $resCmd;
		} else {
			return false;
		}
	}

	function do_ativar_desativar()
	{
		global $modulo, $db;		
		
		$sqlCmd = "UPDATE " . $this->nome_tabela . " SET ativo = IF(ativo = 1,0,1) WHERE id = " . intval($this->id) . " ";	
		$resCmd = $db->exec_query($sqlCmd);
		
		if(method_exists($this,"do_after_ativar_desativar"))
		{
			$this->do_after_ativar_desativar($this->id);
		}
		
		return $resCmd;
	}
	
	function localiza($array_vars,$array_values,$array_compare = null, $return_controllers = false)
	{		
		global $db;
		
		$array_obj = array();
		
		$sqlCmd = "SELECT principal.* ";
		if(sizeof($this->array_exibition_fields) > 0)
		{
			$sizeof = sizeof($this->array_exibition_fields);
			for($w=0;$w<$sizeof;$w++)
			{
				$array_aux = $this->array_exibition_fields[$w];
				$sqlCmd .= ", " . $array_aux[1] . " AS " . $array_aux[0];				
			}			
		}		
		$sqlCmd .= " FROM " . $this->nome_tabela . " principal 
  				   WHERE (true) ";	
	    for($w=0;$w<sizeof($array_vars);$w++)
		{
			$var = $array_vars[$w];
			$value = $array_values[$w];
			$compare = "=";
			if(is_array($array_compare)) {
				$compare = $array_compare[$w];
			}
			$sqlCmd .= " AND " . $var . " " . $compare . " \"" . $value . "\" ";
		}			
		$sqlCmd .= "";
		$resCmd = $db->exec_query($sqlCmd);
		
		$class = get_class($this);		       
	    $keys = array_keys(get_class_vars($class));

		if($return_controllers) {
			$class = str_replace("_model","",$class);
			carrega_classe($class);
		}
		
		$w=0;
		while($w<$db->num_rows($resCmd))
		{			
			$obj = new $class();
			for($k=0;$k<sizeof($this->primary_keys);$k++)
			{
				$pk = $this->primary_keys[$k];
				$obj->$pk = $db->result_field($resCmd,$w,$pk);
			}			

			$obj->carrega_dados();			
			$array_obj[sizeof($array_obj)] = $obj;
			$w++;
		}
				
		return $array_obj;
	}
	
	function get_reference_item($modulo)
	{		
		$result = array();
		$w=0;
		while($w<sizeof($this->reference_items))
		{
			if($this->reference_items[$w][1] == $modulo)
			{
				$result = $this->reference_items[$w];
			}
			
			$w++;			
		}
		return $result;
	}
	
	function get_item_data_table($modulo)
	{		
		$result = array();
		$w=0;
		while($w<sizeof($this->reference_data_tables))
		{
			if($this->reference_data_tables[$w][0] == $modulo)
			{
				$result = $this->reference_data_tables[$w];
			}
			
			$w++;			
		}
		return $result;
	}

	function get_item_img_gallery($modulo)
	{		
		$result = array();
		$w=0;
		while($w<sizeof($this->reference_img_gallery))
		{
			if($this->reference_img_gallery[$w][0] == $modulo)
			{
				$result = $this->reference_img_gallery[$w];
			}
			
			$w++;			
		}
		return $result;
	}

	function get_item_file_gallery($modulo)
	{		
		$result = array();
		$w=0;
		while($w<sizeof($this->reference_file_gallery))
		{
			if($this->reference_file_gallery[$w][0] == $modulo)
			{
				$result = $this->reference_file_gallery[$w];
			}
			
			$w++;			
		}
		return $result;
	}

	function get_reference_model($field) {
		$result = "";
		$w=0;
		while($w<sizeof($this->reference_models))
		{
			if($this->reference_models[$w][0] == $field)
			{
				$result = $this->reference_models[$w][1];
			}
			
			$w++;			
		}
		return $result;
	}

	function get_foreign_key_model($field)
	{
		$result = "";
		$w=0;
		while($w<sizeof($this->foreign_keys))
		{
			if($this->foreign_keys[$w][0] == $field)
			{
				$result = $this->foreign_keys[$w][1];
			}
			
			$w++;			
		}
		return $result;
	}

	function get_var_referencia($moduloRef) {
		global $db, $modulo, $mySQL_prefix;

		$retids = "";

		if(is_array($this->reference_items) && sizeof($this->reference_items) > 0) {
			foreach($this->reference_items as $refAux) {
				// Se v3.0
				if(sizeof($refAux) > 3) { 
					$classTable = $refAux[1];
					carrega_classe($classTable);
					$classAux = "";
					$nomeTabela = "";
					// Se é módulo
					if(class_exists($classTable)) {
						$classAux = $classTable;
						//$nomeTabela = $mySQL_prefix."_".$classTable;
						$objAux = new $classAux();
						$nomeTabela = $objAux->model->nome_tabela;
					}
					// Se é tabela
					else {
						$nomeTabela = $classTable;
						$classAux = str_replace($mySQL_prefix."_","",$classTable);
					}

					if($classAux == $moduloRef) {

						if(trim($nomeTabela) != "" && trim($classAux) != "") {

							// Seleciona registros
							$sqlCmd = "SELECT " . $refAux[3] . " FROM " . $nomeTabela . " WHERE " . $refAux[2] . " = " . intval($this->get_var("id")) . " ";
							$resCmd = $db->exec_query($sqlCmd);

							$w=0;
							while($w<$db->num_rows($resCmd))
							{
								if(trim($retids) != "") {
									$retids .= ";";
								}
								$retids .= $db->result_field($resCmd,$w,$refAux[3]);
								
								$w++;
							}
						}

						break;
					}
				}
			}
		}

		return $retids;
	}
	
	function get_array_id_ref($nomeTabela,$sqlWhere,$campo_retorno)
	{
		global $db, $modulo;
		
		$sqlCmd = "SELECT principal.* FROM " . $nomeTabela . " principal WHERE (true) ";
		if(trim($sqlWhere) <> "")
		{
			$sqlCmd .= " AND " . $sqlWhere . " ";
		}
		$resCmd = $db->exec_query($sqlCmd);
		
		$array_res = array();
		$w=0;
		while($w<$db->num_rows($resCmd))
		{
			$array_res[sizeof($array_res)] = $db->result_field($resCmd,$w,$campo_retorno);
			
			$w++;
		}
		return $array_res;
	}
	
	function upload_file($field_name,$id)
	{
		global $modulo, $db;		

		switch ($_FILES[$field_name]['error'])
 		{
 			case 1:
            print '<p> O arquivo é maior do que permite o servidor PHP</p>';
            die();
    		
            case 2:
            print '<p> O arquivo é maior do que permite o formulário HTML</p>';
            die();
    	
            case 3:
            print '<p> Apenas parte do arquivo foi enviada</p>';
            die();
    
            //case 4:
            //print '<p> No file was uploaded</p>';
            //die();
 		}
 		
 		$mime = explode("/",$_FILES[$field_name]['type']);

 		$classe = str_replace("_model","",get_class($this)); 
 		carrega_classe($classe);
 		if(class_exists($classe)) {
			$this->controller = new $classe();
		} 		
		
		if(trim($_FILES[$field_name]["name"]) != "" && is_uploaded_file($_FILES[$field_name]["tmp_name"]))
		{
			$randcod = gera_senha();

			$arq = trim($_FILES[$field_name]["name"]);
			$array_aux = explode(".",$arq);
			$arqname = "";
			for($w=0; $w<sizeof($array_aux)-1; $w++) {
				if($w>0) {$arqname .= ".";}
				$arqname .= $array_aux[$w];
			}
			$ext = $array_aux[sizeof($array_aux)-1];
			
			if(@trim($this->upload_folders[$field_name]) != "") {
				$endereco_copia = $this->upload_folders[$field_name];
			} else {
				$endereco_copia = $this->upload_folder;
			}

			// Salva o nome conforme o display padrão
			$new_filename = "";
			if(trim($this->controller->view->default_view_expr) != "") {
				$objAux = $this;
				$new_filename = get_output(eval($this->controller->view->default_view_expr));
			} else if(trim($this->controller->view->default_view_field) != "" &&
				      trim($this->controller->view->default_view_field) != "id") {
				$field = $this->controller->view->default_view_field;
				$new_filename = get_output($this->get_var($field));
			} else {
				//$new_filename = $modulo . "_" . $id . "_" . $field_name;
				$new_filename = $arqname;
			}
			$new_filename = gera_titulo_amigavel($new_filename,true,"-",1,1,false);
			
			$nome_arquivo = $new_filename . "." . $ext;
			
			// Renomeia
			if(trim($nome_arquivo) != "" && file_exists($endereco_copia . $nome_arquivo))
			{
				$nome_arquivo = $new_filename . "_" . $randcod . "." . $ext;
				//@unlink($endereco_copia . $nome_arquivo);
			}			

			// Envia
			move_uploaded_file($_FILES[$field_name]["tmp_name"],$endereco_copia . $nome_arquivo);
			chmod($endereco_copia . $nome_arquivo, 0777);
			
			// Atualiza BD
			$sqlCmd = "UPDATE " . $this->nome_tabela . " SET " . $field_name . " = \"" . $nome_arquivo . "\" WHERE id = " . intval($id) . " ";
			$db->exec_query($sqlCmd);
		}
	}

	function upload_gallery_file($gal_item, $id) {
		global $modulo, $db;

		$classe = $gal_item[0];
 		$id_field = $gal_item[1];
 		carrega_classe($classe);

 		$objRef = new $classe();
 		$last_ranking = $objRef->get_last_var("ranking")+1;

		if(trim($_REQUEST["files_list_" . $gal_item[0]]) != "") {

			$arqlist = explode("#;#",$_REQUEST["files_list_" . $gal_item[0]]);
			foreach($arqlist as $url_arquivo) {

				if(trim($url_arquivo) != "") {
					$arrAux = explode("/",$url_arquivo);	
					$arq = $arrAux[sizeof($arrAux)-1];

					$randcod = gera_senha();

					$array_aux = explode(".",$arq);
					$arqname = "";
					for($w=0; $w<sizeof($array_aux)-1; $w++) {
						if($w>0) {$arqname .= ".";}
						$arqname .= $array_aux[$w];
					}
					$ext = $array_aux[sizeof($array_aux)-1];

					$sqlInserts = "";

					// Salva o nome conforme o display padrão
					$new_filename = "";
					if(trim($this->controller->view->default_view_expr) != "") {
						$objAux = $this;
						$new_filename = get_output(eval($this->controller->view->default_view_expr));
					} else if(trim($this->controller->view->default_view_field) != "" &&
						      trim($this->controller->view->default_view_field) != "id") {
						$field = $this->controller->view->default_view_field;
						$new_filename = get_output($this->get_var($field));
					} else {
						//$new_filename = $modulo . "_" . $id . "_" . $field_name;
						$new_filename = $arqname;
					}
					$new_filename = gera_titulo_amigavel($new_filename,true,"-",1,1,false);
					
					$nome_arquivo = $new_filename . "." . $ext;
					$nome_arquivo_original = $new_filename . "_original." . $ext;	

					foreach($objRef->model->array_file_fields as $field_name) {
					
						if(@trim($objRef->model->upload_folders[$field_name]) != "") {
							$endereco_copia = $objRef->model->upload_folders[$field_name];
						} else {
							$endereco_copia = $objRef->model->upload_folder;
						}

						// TODO fazer pegar nome do campo de display/label do item da galeria (quando puder inserir ele no upload)	
					
						// Renomeia
						if(trim($nome_arquivo) != "" && file_exists($endereco_copia . $nome_arquivo))
						{
							$nome_arquivo = $new_filename . "_" . $randcod . "." . $ext;
							//@unlink($endereco_copia . $nome_arquivo);
						}

						$sqlInserts .= " " . $field_name . " = \"" . $db->escape_string(output_decode($nome_arquivo)) . "\", ";

						// Envia
						copy($url_arquivo,$endereco_copia . $nome_arquivo);
						chmod($endereco_copia . $nome_arquivo, 0777);
					}

					// Seta título padrão
					if(trim($objRef->view->default_view_field) != "") {
						$sqlInserts .= " " . $objRef->view->default_view_field . " = \"" . $db->escape_string(output_decode(str_replace(".".$ext,"",$nome_arquivo))) . "\", ";
					}
					
					// Atualiza BD
					$sqlCmd = "INSERT INTO " . $objRef->model->nome_tabela . " SET
						ativo = 1,
						" . $id_field . " = " . $id . ",
						" . $sqlInserts . "
						ranking = " . intval($last_ranking) . ",
						usuario_criacao = " . intval($_SESSION["idLogin"]) . ", 
						data_criacao = NOW()
					";
					$idAux = $db->exec_query($sqlCmd, true);

					$last_ranking++;

					@unlink($url_arquivo);
				}
			}
		}
	}

	function upload_gallery_crop_file($gal_item, $id) {
		global $modulo, $db;

		$bUpdateOriginal = false;

 		$classe = $gal_item[0];
 		$id_field = $gal_item[1];
 		carrega_classe($classe);

 		$objRef = new $classe();
 		$last_ranking = $objRef->get_last_var("ranking")+1;

		if(trim($_REQUEST["imgs_list_" . $gal_item[0]]) != "") {

			$arqlist = explode("#;#",$_REQUEST["imgs_list_" . $gal_item[0]]);
			foreach($arqlist as $url_arquivo) {

				if(trim($url_arquivo) != "") {
					$arrAux = explode("/",$url_arquivo);	
					$arq = $arrAux[sizeof($arrAux)-1];

					$randcod = gera_senha();

					$array_aux = explode(".",$arq);
					$arqname = "";
					for($w=0; $w<sizeof($array_aux)-1; $w++) {
						if($w>0) {$arqname .= ".";}
						$arqname .= $array_aux[$w];
					}
					$ext = $array_aux[sizeof($array_aux)-1];

					$sqlInserts = "";

					// Salva o nome conforme o display padrão
					$new_filename = "";
					if(trim($this->controller->view->default_view_expr) != "") {
						$objAux = $this;
						$new_filename = get_output(eval($this->controller->view->default_view_expr));
					} else if(trim($this->controller->view->default_view_field) != "" &&
						      trim($this->controller->view->default_view_field) != "id") {
						$field = $this->controller->view->default_view_field;
						$new_filename = get_output($this->get_var($field));
					} else {
						//$new_filename = $modulo . "_" . $id . "_" . $field_name;
						$new_filename = $arqname;
					}
					$new_filename = gera_titulo_amigavel($new_filename,true,"-",1,1,false);
					
					$nome_arquivo = $new_filename . "." . $ext;
					$nome_arquivo_original = $new_filename . "_original." . $ext;	

					foreach($objRef->model->array_crop_fields as $field_name) {
					
						if(@trim($objRef->model->upload_folders[$field_name]) != "") {
							$endereco_copia = $objRef->model->upload_folders[$field_name];
						} else {
							$endereco_copia = $objRef->model->upload_folder;
						}

						// TODO fazer pegar nome do campo de display/label do item da galeria (quando puder inserir ele no upload)	
					
						// Renomeia
						if(trim($nome_arquivo) != "" && file_exists($endereco_copia . $nome_arquivo))
						{
							$nome_arquivo = $new_filename . "_" . $randcod . "." . $ext;
							//@unlink($endereco_copia . $nome_arquivo);
						}

						$sqlInserts .= " " . $field_name . " = \"" . $db->escape_string(output_decode($nome_arquivo)) . "\", ";

						// Envia
						copy($url_arquivo,$endereco_copia . $nome_arquivo);
						chmod($endereco_copia . $nome_arquivo, 0777);
					}
					
					// Envia original
					if($objRef->model->crop_original && stristr($url_arquivo, "/original/") === false) {
						$endereco_copia_original = UPLOAD_FOLDER . "original/";

						if(trim($nome_arquivo_original) != "" && file_exists($endereco_copia_original . $nome_arquivo_original))
						{
							$nome_arquivo_original = $new_filename . "_" . $randcod . "_original." . $ext;
							//@unlink($endereco_copia_original . $nome_arquivo_original);
						}	

						copy($url_arquivo,$endereco_copia_original . $nome_arquivo_original);
						chmod($endereco_copia_original . $nome_arquivo_original, 0777);
						$bUpdateOriginal = true;
					}

					// Seta título padrão
					if(trim($objRef->view->default_view_field) != "") {
						$sqlInserts .= " " . $objRef->view->default_view_field . " = \"" . $db->escape_string(output_decode(str_replace(".".$ext,"",$nome_arquivo))) . "\", ";
					}
					
					// Atualiza BD
					$sqlCmd = "INSERT INTO " . $objRef->model->nome_tabela . " SET
						ativo = 1,
						" . $id_field . " = " . $id . ",
						" . $sqlInserts . "
						ranking = " . intval($last_ranking) . ",
						usuario_criacao = " . intval($_SESSION["idLogin"]) . ", 
						data_criacao = NOW()
					";
					$idAux = $db->exec_query($sqlCmd, true);

					if($bUpdateOriginal) {
						$sqlCmd = "INSERT INTO " . DBTABLE_CMS_IMAGENS . " SET modulo_registro = \"" . $classe . "\", id_registro = " . intval($idAux) . ", titulo =\"" . $db->escape_string(output_decode($nome_arquivo_original)) . "\", arquivo =\"" . $db->escape_string(output_decode($nome_arquivo_original)) . "\" ";
						$db->exec_query($sqlCmd);
					}

					$last_ranking++;

					@unlink($url_arquivo);
				}
			}
		}
	}
	
	function upload_crop_file($field_name, $id, $registra_no_banco = true)
	{
		global $modulo, $db;

		$bUpdateOriginal = false;
			
		$url_arquivo = trim($_REQUEST[$field_name]);
		$url_arquivo_original = trim($_REQUEST[$field_name . "_original"]);
		$titulo_arquivo = "";
		$nome_arquivo = "";
		$nome_arquivo_original = "";

		if(trim($url_arquivo) <> "" && !is_dir($url_arquivo)) {
			if(file_exists($url_arquivo)) { 
				$arrAux = explode("/",$url_arquivo);	
				$arq = $arrAux[sizeof($arrAux)-1];

				$randcod = gera_senha();

				$array_aux = explode(".",$arq);
				$arqname = "";
				for($w=0; $w<sizeof($array_aux)-1; $w++) {
					if($w>0) {$arqname .= ".";}
					$arqname .= $array_aux[$w];
				}
				$ext = $array_aux[sizeof($array_aux)-1];
				
				if(@trim($this->upload_folders[$field_name]) != "") {
					$endereco_copia = $this->upload_folders[$field_name];
				} else {
					$endereco_copia = $this->upload_folder;
				}
				$endereco_copia_original = UPLOAD_FOLDER . "original/";

				// Salva o nome conforme o display padrão
				$new_filename = "";
				if(trim($this->controller->view->default_view_expr) != "") {
					$objAux = $this;
					$new_filename = get_output(eval($this->controller->view->default_view_expr));
				} else if(trim($this->controller->view->default_view_field) != "" &&
					      trim($this->controller->view->default_view_field) != "id") {
					$field = $this->controller->view->default_view_field;
					$new_filename = get_output($this->get_var($field));
				} else {
					//$new_filename = $modulo . "_" . $id . "_" . $field_name;
					$new_filename = $arqname;
				}
				$new_filename = gera_titulo_amigavel($new_filename,true,"-",1,1,false);

				$titulo_arquivo = $new_filename;
				
				$nome_arquivo = $titulo_arquivo . "." . $ext;
				$nome_arquivo_original = $titulo_arquivo . "_original." . $ext;
				
				// Renomeia
				if(trim($nome_arquivo) != "" && file_exists($endereco_copia . $nome_arquivo))
				{
					$titulo_arquivo = $new_filename . "_" . $randcod;
					$nome_arquivo = $titulo_arquivo . "." . $ext;
					//@unlink($endereco_copia . $nome_arquivo);
				}
				if(trim($nome_arquivo_original) != "" && file_exists($endereco_copia_original . $nome_arquivo_original))
				{
					$titulo_arquivo = $new_filename . "_" . $randcod;
					$nome_arquivo_original = $titulo_arquivo . "_original." . $ext;
					//@unlink($endereco_copia_original . $nome_arquivo_original);
				}			

				// Envia
				copy($url_arquivo,$endereco_copia . $nome_arquivo);
				chmod($endereco_copia . $nome_arquivo, 0777);
				
				// Envia original
				if($this->crop_original && $registra_no_banco) {
					if(trim($url_arquivo_original) != "" && file_exists($url_arquivo_original) && stristr($url_arquivo_original, "/original/") === false) {
						copy($url_arquivo_original,$endereco_copia_original . $nome_arquivo_original);
						chmod($endereco_copia_original . $nome_arquivo_original, 0777);
						$bUpdateOriginal = true;
					}
				}
			}
			$sqlCmd = "UPDATE " . $this->nome_tabela . " SET " . $field_name . " = \"" . $nome_arquivo . "\" WHERE id = " . intval($id) . " ";
			$db->exec_query($sqlCmd);

			if($bUpdateOriginal) {
				$sqlCmd = "DELETE FROM " . DBTABLE_CMS_IMAGENS . " WHERE modulo_registro = \"" . $modulo . "\" AND id_registro = " . intval($id) . " AND campo_registro = \"" . $field_name . "\" ";
				$db->exec_query($sqlCmd);

				$sqlCmd = "INSERT INTO " . DBTABLE_CMS_IMAGENS . " SET modulo_registro = \"" . $modulo . "\", id_registro = " . intval($id) . ", campo_registro = \"" . $field_name . "\", titulo =\"" . $titulo_arquivo . "\", arquivo =\"" . $nome_arquivo_original . "\" ";
				$db->exec_query($sqlCmd);
			}
		}
	}
		
	function remove_file($field_name)
	{
		global $modulo, $db;
		if(@trim($this->upload_folders[$field_name]) != "") {
			@unlink($this->upload_folders[$field_name] . $this->get_var($field_name));
		} else {
			@unlink($this->upload_folder . $this->get_var($field_name));
		}
	}
	
	function do_after_incluir($id)
	{
		global $modulo, $db, $mySQL_prefix;

		// Percorre arquivos normais
		$w=0;
		$sizeof= sizeof($this->array_file_fields);
		while($w<$sizeof)
		{
			$field_name = $this->array_file_fields[$w];		
			$this->upload_file($field_name,$id);	
			
			$w++;
		}
		
		// Percorre arquivos de crop
		//$arrayUpload = array();

		$w=0;
		$sizeof= sizeof($this->array_crop_fields);
		while($w<$sizeof)
		{
			$field_name = $this->array_crop_fields[$w];		

			if(trim($_REQUEST[$field_name]) != "") {
			
				if(@trim($this->upload_folders[$field_name]) != "") {
					$endereco_copia = $this->upload_folders[$field_name];
				} else {
					$endereco_copia = $this->upload_folder;
				}

				$nome_arquivo = $this->get_var($field_name);
				
				// Remove o arquivo atual
				if(trim($nome_arquivo) != "" && file_exists($endereco_copia . $nome_arquivo))
				{
					if((stristr($endereco_copia . $nome_arquivo, "/original/") === false || $modulo == "cms_imagens") &&
					   (trim($endereco_copia . $nome_arquivo) != $_REQUEST[$field_name])
					) {
						@unlink($endereco_copia . $nome_arquivo);
						//array_push($arrayUpload, $field_name);
					}
				} else {
					//array_push($arrayUpload, $field_name);
				}
			}	
			
			$w++;
		}
		$w=0;
		$array_in_cluster = array();
		while($w<$sizeof)
		{
			$field_name = $this->array_crop_fields[$w];

			//if(in_array($field_name, $arrayUpload)) // No incluir, sempre faz
			{

				$registra_no_banco = true;
				if(in_array($_REQUEST[$field_name . "_cluster"], $array_in_cluster)){
					$registra_no_banco = false;
				}
				$this->upload_crop_file($field_name, $id, $registra_no_banco);	
				array_push($array_in_cluster, $_REQUEST[$field_name . "_cluster"]);
			}
			
			$w++;
		}
		$w=0;
		while($w<$sizeof)
		{
			$field_name = $this->array_crop_fields[$w];

			//if(in_array($field_name, $arrayUpload)) // No incluir, sempre faz
			{
			
				if(stristr($_REQUEST[$field_name], "/original/") === false || $modulo == "cms_imagens") {
					@unlink(trim($_REQUEST[$field_name]));
				}
				if(stristr(trim($_REQUEST[$field_name . "_original"]), "temp/")) {
					@unlink(trim($_REQUEST[$field_name . "_original"]));
				}
			}			
			
			$w++;
		}

	// Percorre arquivos de galeria de Images

		$w=0;
		$sizeof= sizeof($this->reference_img_gallery);
		while($w<$sizeof)
		{
			$this->upload_gallery_crop_file($this->reference_img_gallery[$w], $id);
			
			$w++;
		}

	// Percorre arquivos de galeria de Files

		$w=0;
		$sizeof= sizeof($this->reference_file_gallery);
		while($w<$sizeof)
		{
			$this->upload_gallery_file($this->reference_file_gallery[$w], $id);
			
			$w++;
		}

	// atualiza rankings

		$class = get_class($this);
		$keys = array_keys(get_class_vars($class));

		if(in_array("ranking", $keys)){
			$sqlCmd = "SELECT principal.* FROM " . $this->nome_tabela . " principal WHERE ranking = " . intval($this->ranking) . " AND id != " . intval($id) . " ";
			$array_result = $db->exec_query($sqlCmd, true);		
			$result = $array_result[0];
			if($db->num_rows($result) > 0){
				$sqlCmd = "UPDATE " . $this->nome_tabela . " SET ranking = ranking+1 WHERE ranking >= " . intval($this->ranking) . " AND id != " . intval($id) . " ";
			
				//echo $sqlCmd; die();
			
				$array_result = $db->exec_query($sqlCmd, true);
				$result = $array_result[0];		
			}
		}

	// Insere logs
		
		/*$log = new log_entidades();
		$log->inicia_dados();
		$log->set_var("registro_serializado",serialize($this));
		$log->set_var("nome_entidade",str_replace("_model","",get_class($this)));
		$log->set_var("id_registro",$id);
		$log->set_var("acao","i");
		$log->inclui();*/

	// Insere languages

		$strIds = "";
		$strIdsCampos = "";
		
		$iCount = 0;
		for($w=0; $w<sizeof($_REQUEST["id_lang"]); $w++) {
			$idAux = $_REQUEST["id_aux_lang"][$w];
			$id_lang = $_REQUEST["id_lang"][$w];
			$id_registro = $_REQUEST["id_registro_lang"][$w];
			$modulo_registro = $_REQUEST["modulo_registro_lang"][$w];
			$variavel = $_REQUEST["var_lang"][$w];
			$id_idioma = $_REQUEST["id_idioma_lang"][$w];
			$valor = $_REQUEST["valor_lang"][$w];
			
			if(trim($valor) != "") {
				if(intval($id_lang) > 0) {
					$sqlCmd = "UPDATE " . DBTABLE_LANG . " SET
							id_registro = " . intval($id) .  ", 
							id_idioma = " . intval($id_idioma) .  ", 
							modulo_registro = \"" . output_decode($modulo) . "\",
							variavel = \"" . output_decode($db->escape_string($variavel)) . "\",
							valor = \"" . output_decode($db->escape_string($valor)) . "\",
							ranking = \"" . ($iCount+1) . "\",
							usuario_atualizacao = " . intval($_SESSION["idLogin"]) . ",
							data_atualizacao = NOW()
					WHERE id = " . intval($id_lang);

					$db->exec_query($sqlCmd);
					$iCount++;

					//echo $sqlCmd . "<br/>"; echo intval($id_lang) . "<br/>"; echo "<br/>";

					if(trim($strIds) != "") {
						$strIds .= ",";
					}
					$strIds .= intval($id_lang);

				} else {
					$sqlCmd = "INSERT INTO " . DBTABLE_LANG . " SET
							id_registro = " . intval($id) .  ", 
							id_idioma = " . intval($id_idioma) .  ", 
							modulo_registro = \"" . output_decode($modulo) . "\",
							variavel = \"" . output_decode($db->escape_string($variavel)) . "\",
							valor = \"" . output_decode($db->escape_string($valor)) . "\",
							ranking = \"" . ($iCount+1) . "\",
							usuario_criacao = " . intval($_SESSION["idLogin"]) . ", 
							ativo = " . (1) . //intval($_REQUEST["s_checked"][$w]) . 
							", data_criacao = NOW() ";

					$idAux = $db->exec_query($sqlCmd, true);
					$iCount++;

					//echo $sqlCmd . "<br/>"; echo intval($idAux[1]) . "<br/>"; echo "<br/>";

					if(trim($strIds) != "") {
						$strIds .= ",";
					}
					$strIds .= intval($idAux[1]);
				}
			}
		}
		
		$sqlWhereAux = "";
		if(trim($strIds) != "") {
			$sqlWhereAux = " AND id NOT IN (" . ($strIds) .  ") ";
		}
		//echo "DELETE FROM " . DBTABLE_LANG . " WHERE id_registro = " . intval($id) .  " AND modulo_registro = \"" . output_decode($modulo) . "\" " . $sqlWhereAux; die();
		$db->exec_query("DELETE FROM " . DBTABLE_LANG . " WHERE id_registro = " . intval($id) .  " AND modulo_registro = \"" . output_decode($modulo) . "\" " . $sqlWhereAux);

	// Insere dataTables

		if(is_array($this->reference_data_tables) && sizeof($this->reference_data_tables) > 0) {
			foreach($this->reference_data_tables as $refAux) {
				$strIds = "";
				$strIdsCampos = "";

				$classAux = $refAux[0];
				$campoID = $refAux[1];

				carrega_classe($classAux);
				if(class_exists($classAux)) {
					$objAux = new $classAux();

					$objAux->view->monta_campos_form($id);

					$tableAux = $objAux->get_nome_tabela();
					
					$iCount = 0;
					for($w=0; $w<sizeof($_REQUEST["id_" . $classAux]); $w++) {
						$idAux = $_REQUEST["id_aux_" . $classAux][$w];
						$id_item = $_REQUEST["id_" . $classAux][$w];
						$id_registro = $_REQUEST["id_registro_" . $classAux][$w];

						// Monta SQL campos
						$sqlFields = "";
						if(is_array($objAux->view->array_form_campos) && sizeof($objAux->view->array_form_campos) > 0) {
							foreach($objAux->view->array_form_campos as $inputAux) {
								if($inputAux->show_in_data_table) {
									if(isset($_REQUEST[$inputAux->name . "_" . $classAux][$w])) {
										$inputAux->value = $_REQUEST[$inputAux->name . "_" . $classAux][$w];
										//$sqlFields .= " " . $inputAux->name . " = \"" . output_decode($db->escape_string($_REQUEST[$inputAux->name . "_" . $classAux][$w])) . "\", ";
										//$sqlFields .= " " . $inputAux->name . " = \"" . $inputAux->get_db_value() . "\", ";
										$sqlFields .= " " . $inputAux->name . " = " . (trim($inputAux->get_db_value())==""?"NULL":"\"" . $inputAux->get_db_value() . "\"") . ", ";
									}
								}
							}
						}

						if(trim($sqlFields) != "") {
							if(intval($id_item) > 0) {
								$sqlCmd = "UPDATE " . $tableAux . " SET
										" . $campoID . " = " . intval($id) .  ", 
										" . $sqlFields . "
										ranking = \"" . ($iCount+1) . "\",
										usuario_atualizacao = " . intval($_SESSION["idLogin"]) . ",
										data_atualizacao = NOW()
								WHERE id = " . intval($id_item);

								$db->exec_query($sqlCmd);
								$iCount++;

								//echo $sqlCmd . "<br/>"; echo intval($id_lang) . "<br/>"; echo "<br/>";

								if(trim($strIds) != "") {
									$strIds .= ",";
								}
								$strIds .= intval($id_item);

								// Do after
								$objRef = new $classAux();
								$objRef->inicia_dados();
								$objRef->set_var("id",$id_item);
								$objRef->carrega_dados();
								$objRef->model->do_after_editar($id_item);

							} else {
								$sqlCmd = "INSERT INTO " . $tableAux . " SET
										" . $campoID . " = " . intval($id) .  ", 
										" . $sqlFields . "
										ranking = \"" . ($iCount+1) . "\",
										usuario_criacao = " . intval($_SESSION["idLogin"]) . ", 
										ativo = " . (1) . //intval($_REQUEST["s_checked"][$w]) . 
										", data_criacao = NOW() ";

								$idAux = $db->exec_query($sqlCmd, true);
								$iCount++;

								//echo $sqlCmd . "<br/>"; echo intval($idAux[1]) . "<br/>"; echo "<br/>";

								if(trim($strIds) != "") {
									$strIds .= ",";
								}
								$strIds .= intval($idAux[1]);

								// Do after
								$objRef = new $classAux();
								$objRef->inicia_dados();
								$objRef->set_var("id",$idAux[1]);
								$objRef->carrega_dados();
								$objRef->model->do_after_incluir($idAux[1]);
							}
						}
					}
					
					$sqlWhereAux = "";
					if(trim($strIds) != "") {
						$sqlWhereAux = " AND id NOT IN (" . ($strIds) .  ") ";
					}
					//echo "DELETE FROM " . DBTABLE_LANG . " WHERE " . $campoID . " = " . intval($id) .  " " . $sqlWhereAux; die();
					$db->exec_query("DELETE FROM " . $tableAux . " WHERE " . $campoID . " = " . intval($id) .  " " . $sqlWhereAux);
				}
			}
		}

	// Insere referencias

		if(is_array($this->reference_items) && sizeof($this->reference_items) > 0) {
			foreach($this->reference_items as $refAux) {
				// Se v3.0
				if(sizeof($refAux) > 3) { 
					$classTable = $refAux[1];
					carrega_classe($classTable);
					$classAux = "";
					$nomeTabela = "";
					// Se é módulo
					if(class_exists($classTable)) {
						$classAux = $classTable;
						//$nomeTabela = $mySQL_prefix."_".$classTable;
						$objAux = new $classAux();
						$nomeTabela = $objAux->model->nome_tabela;
					}
					// Se é tabela
					else {
						$nomeTabela = $classTable;
						$classAux = str_replace($mySQL_prefix."_","",$classTable);
					}

					if(trim($nomeTabela) != "" && trim($classAux) != "") {
						// Apaga antigos
						$sqlCmd = "DELETE FROM " . $nomeTabela . " WHERE " . $refAux[2] . " = " . intval($id) . " ";
						$db->exec_query($sqlCmd);

						// Insere novos
						foreach($_REQUEST[$classAux] as $item) {
							$sqlCmd = "INSERT INTO " . $nomeTabela . " SET
								" . $refAux[2] . " = " . intval($id) . ",
								" . $refAux[3] . " = " . intval($item) . " ";
							$db->exec_query($sqlCmd);
						}
					}
				}
			}
		}

		$classe = str_replace("_model","",get_class($this)); 
 		carrega_classe($classe);
 		if(class_exists($classe)) {
			$this->controller = new $classe();
		} 

		// Gera permalink
		if($this->controller != null && trim($_REQUEST["permalink"]) == "") {
			$this->controller->set_var("id",$id);
			$this->controller->carrega_dados();
			$this->controller->gera_permalink(true);
		}

		return true;
	}
	
	function do_after_editar($id)
	{
		global $modulo, $db, $mySQL_prefix;
		
		// Percorre arquivos normais
		$w=0;
		$sizeof= sizeof($this->array_file_fields);
		while($w<$sizeof)
		{
			$field_name = $this->array_file_fields[$w];		
			$this->upload_file($field_name,$id);	
			
			$w++;
		}

		// Percorre arquivos de crop
		$arrayUpload = array();

		$w=0;
		$sizeof= sizeof($this->array_crop_fields);
		while($w<$sizeof)
		{
			$field_name = $this->array_crop_fields[$w];

			if(trim($_REQUEST[$field_name]) != "") {
			
				if(@trim($this->upload_folders[$field_name]) != "") {
					$endereco_copia = $this->upload_folders[$field_name];
				} else {
					$endereco_copia = $this->upload_folder;
				}

				$nome_arquivo = $this->get_var($field_name);
				
				// Remove o arquivo atual
				if(trim($nome_arquivo) != "" && file_exists($endereco_copia . $nome_arquivo))
				{
					if((stristr($endereco_copia . $nome_arquivo, "/original/") === false || $modulo == "cms_imagens") &&
					   (trim($endereco_copia . $nome_arquivo) != $_REQUEST[$field_name])
					) {
						@unlink($endereco_copia . $nome_arquivo);
						array_push($arrayUpload, $field_name);
					}
				} else {
					array_push($arrayUpload, $field_name);
				}
			}	
			
			$w++;
		}
		$w=0;
		$array_in_cluster = array();
		while($w<$sizeof)
		{
			$field_name = $this->array_crop_fields[$w];		

			if(in_array($field_name, $arrayUpload)) {

				$registra_no_banco = true;
				if(in_array($_REQUEST[$field_name . "_cluster"], $array_in_cluster)){
					$registra_no_banco = false;
				}
				$this->upload_crop_file($field_name, $id, $registra_no_banco);	
				array_push($array_in_cluster, $_REQUEST[$field_name . "_cluster"]);
			}
				
			$w++;
		}
		$w=0;
		while($w<$sizeof)
		{
			$field_name = $this->array_crop_fields[$w];

			if(in_array($field_name, $arrayUpload)) {
			
				if(stristr($_REQUEST[$field_name], "/original/") === false || $modulo == "cms_imagens") {
					@unlink(trim($_REQUEST[$field_name]));
				}
				if(stristr(trim($_REQUEST[$field_name . "_original"]), "temp/")) {
					@unlink(trim($_REQUEST[$field_name . "_original"]));
				}
			}
			
			$w++;
		}

	// Percorre arquivos de galeria de Images

		$w=0;
		$sizeof= sizeof($this->reference_img_gallery);
		while($w<$sizeof)
		{
			$this->upload_gallery_crop_file($this->reference_img_gallery[$w], $id);
			
			$w++;
		}

	// Percorre arquivos de galeria de Files

		$w=0;
		$sizeof= sizeof($this->reference_file_gallery);
		while($w<$sizeof)
		{
			$this->upload_gallery_file($this->reference_file_gallery[$w], $id);
			
			$w++;
		}

	// Atualiza rankings

		$class = get_class($this);
		$keys = array_keys(get_class_vars($class));

		if(in_array("ranking", $keys)){
			$sqlCmd = "SELECT principal.* FROM " . $this->nome_tabela . " principal WHERE ranking = " . intval($this->ranking) . " AND id != " . intval($id) . " ";
			$array_result = $db->exec_query($sqlCmd, true);		
			$result = $array_result[0];
			if($db->num_rows($result) > 0){
				$sqlCmd = "UPDATE " . $this->nome_tabela . " SET ranking = ranking+1 WHERE ranking >= " . intval($this->ranking) . " AND id != " . intval($id) . " ";
				
				//echo $sqlCmd; die();
			
				$array_result = $db->exec_query($sqlCmd, true);
				$result = $array_result[0];		
			}
		}

	// Atualiza log
		
		/*$log = new log_entidades();
		$log->inicia_dados();
		$log->set_var("registro_serializado",serialize($this));
		$log->set_var("nome_entidade",str_replace("_model","",get_class($this)));
		$log->set_var("id_registro",$id);
		$log->set_var("acao","a");
		$log->inclui();*/

	// Insere languages

		$strIds = "";
		$strIdsCampos = "";
		
		$iCount = 0;
		for($w=0; $w<sizeof($_REQUEST["id_lang"]); $w++) {
			$idAux = $_REQUEST["id_aux_lang"][$w];
			$id_lang = $_REQUEST["id_lang"][$w];
			$id_registro = $_REQUEST["id_registro_lang"][$w];
			$modulo_registro = $_REQUEST["modulo_registro_lang"][$w];
			$variavel = $_REQUEST["var_lang"][$w];
			$id_idioma = $_REQUEST["id_idioma_lang"][$w];
			$valor = $_REQUEST["valor_lang"][$w];
			
			if(trim($valor) != "") {
				if(intval($id_lang) > 0) {
					$sqlCmd = "UPDATE " . DBTABLE_LANG . " SET
							id_registro = " . intval($id) .  ", 
							id_idioma = " . intval($id_idioma) .  ", 
							modulo_registro = \"" . output_decode($modulo) . "\",
							variavel = \"" . output_decode($db->escape_string($variavel)) . "\",
							valor = \"" . output_decode($db->escape_string($valor)) . "\",
							ranking = \"" . ($iCount+1) . "\",
							usuario_atualizacao = " . intval($_SESSION["idLogin"]) . ",
							data_atualizacao = NOW()
					WHERE id = " . intval($id_lang);

					$db->exec_query($sqlCmd);
					$iCount++;

					//echo $sqlCmd . "<br/>"; echo intval($id_lang) . "<br/>"; echo "<br/>";

					if(trim($strIds) != "") {
						$strIds .= ",";
					}
					$strIds .= intval($id_lang);

				} else {
					$sqlCmd = "INSERT INTO " . DBTABLE_LANG . " SET
							id_registro = " . intval($id) .  ", 
							id_idioma = " . intval($id_idioma) .  ", 
							modulo_registro = \"" . output_decode($modulo) . "\",
							variavel = \"" . output_decode($db->escape_string($variavel)) . "\",
							valor = \"" . output_decode($db->escape_string($valor)) . "\",
							ranking = \"" . ($iCount+1) . "\",
							usuario_criacao = " . intval($_SESSION["idLogin"]) . ", 
							ativo = " . (1) . //intval($_REQUEST["s_checked"][$w]) . 
							", data_criacao = NOW() ";

					$idAux = $db->exec_query($sqlCmd, true);
					$iCount++;

					//echo $sqlCmd . "<br/>"; echo intval($idAux[1]) . "<br/>"; echo "<br/>";

					if(trim($strIds) != "") {
						$strIds .= ",";
					}
					$strIds .= intval($idAux[1]);
				}
			}
		}
		
		$sqlWhereAux = "";
		if(trim($strIds) != "") {
			$sqlWhereAux = " AND id NOT IN (" . ($strIds) .  ") ";
		}
		//echo "DELETE FROM " . DBTABLE_LANG . " WHERE id_registro = " . intval($id) .  " AND modulo_registro = \"" . output_decode($modulo) . "\" " . $sqlWhereAux; die();
		$db->exec_query("DELETE FROM " . DBTABLE_LANG . " WHERE id_registro = " . intval($id) .  " AND modulo_registro = \"" . output_decode($modulo) . "\" " . $sqlWhereAux);

	// Insere dataTables

		if(is_array($this->reference_data_tables) && sizeof($this->reference_data_tables) > 0) {
			foreach($this->reference_data_tables as $refAux) {

				$strIds = "";
				$strIdsCampos = "";

				$classAux = $refAux[0];
				$campoID = $refAux[1];

				carrega_classe($classAux);
				if(class_exists($classAux)) {
					$objAux = new $classAux();

					$objAux->view->monta_campos_form($id);

					$tableAux = $objAux->get_nome_tabela();
					
					$iCount = 0;
					for($w=0; $w<sizeof($_REQUEST["id_" . $classAux]); $w++) {
						$idAux = $_REQUEST["id_aux_" . $classAux][$w];
						$id_item = $_REQUEST["id_" . $classAux][$w];
						$id_registro = $_REQUEST["id_registro_" . $classAux][$w];

						// Monta SQL campos
						$sqlFields = "";
						if(is_array($objAux->view->array_form_campos) && sizeof($objAux->view->array_form_campos) > 0) {
							foreach($objAux->view->array_form_campos as $inputAux) {
								if($inputAux->show_in_data_table) {
									if(isset($_REQUEST[$inputAux->name . "_" . $classAux][$w])) {
										$inputAux->value = $_REQUEST[$inputAux->name . "_" . $classAux][$w];
										//$sqlFields .= " " . $inputAux->name . " = \"" . output_decode($db->escape_string($_REQUEST[$inputAux->name . "_" . $classAux][$w])) . "\", ";
										//$sqlFields .= " " . $inputAux->name . " = \"" . $inputAux->get_db_value() . "\", ";
										$sqlFields .= " " . $inputAux->name . " = " . (trim($inputAux->get_db_value())==""?"NULL":"\"" . $inputAux->get_db_value() . "\"") . ", ";
									}
								}
							}
						}

						if(trim($sqlFields) != "") {
						
							if(intval($id_item) > 0) {
								$sqlCmd = "UPDATE " . $tableAux . " SET
										" . $campoID . " = " . intval($id) .  ", 
										" . $sqlFields . "
										ranking = \"" . ($iCount+1) . "\",
										usuario_atualizacao = " . intval($_SESSION["idLogin"]) . ",
										data_atualizacao = NOW()
								WHERE id = " . intval($id_item);

								$db->exec_query($sqlCmd);
								$iCount++;

								//echo $sqlCmd . "<br/>"; echo intval($id_lang) . "<br/>"; echo "<br/>";

								if(trim($strIds) != "") {
									$strIds .= ",";
								}
								$strIds .= intval($id_item);

								// Do after
								$objRef = new $classAux();
								$objRef->inicia_dados();
								$objRef->set_var("id",$id_item);
								$objRef->carrega_dados();
								$objRef->model->do_after_editar($id_item);

							} else {
								$sqlCmd = "INSERT INTO " . $tableAux . " SET
										" . $campoID . " = " . intval($id) .  ", 
										" . $sqlFields . "
										ranking = \"" . ($iCount+1) . "\",
										usuario_criacao = " . intval($_SESSION["idLogin"]) . ", 
										ativo = " . (1) . //intval($_REQUEST["s_checked"][$w]) . 
										", data_criacao = NOW() ";

								$idAux = $db->exec_query($sqlCmd, true);
								$iCount++;

								//ob_clean(); echo $sqlCmd . "<br/>"; echo intval($idAux[1]) . "<br/>"; echo "<br/>"; die();

								if(trim($strIds) != "") {
									$strIds .= ",";
								}
								$strIds .= intval($idAux[1]);

								// Do after
								$objRef = new $classAux();
								$objRef->inicia_dados();
								$objRef->set_var("id",$idAux[1]);
								$objRef->carrega_dados();
								$objRef->model->do_after_incluir($idAux[1]);
							}	
						}
					}
					
					$sqlWhereAux = "";
					if(trim($strIds) != "") {
						$sqlWhereAux = " AND id NOT IN (" . ($strIds) .  ") ";
					}
					//echo "DELETE FROM " . DBTABLE_LANG . " WHERE " . $campoID . " = " . intval($id) .  " " . $sqlWhereAux; die();
					$db->exec_query("DELETE FROM " . $tableAux . " WHERE " . $campoID . " = " . intval($id) .  " " . $sqlWhereAux);
				}
			}
		}

	// Insere referencias

		if(is_array($this->reference_items) && sizeof($this->reference_items) > 0) {
			foreach($this->reference_items as $refAux) {
				// Se v3.0
				if(sizeof($refAux) > 3) { 
					$classTable = $refAux[1];

					carrega_classe($classTable);
					$classAux = "";
					$nomeTabela = "";
					// Se é módulo
					if(class_exists($classTable)) {
						$classAux = $classTable;
						//$nomeTabela = $mySQL_prefix."_".$classTable;
						$objAux = new $classAux();
						$nomeTabela = $objAux->model->nome_tabela;
					}
					// Se é tabela
					else {
						$nomeTabela = $classTable;
						$classAux = str_replace($mySQL_prefix."_","",$classTable);
					}

					if(trim($nomeTabela) != "" && trim($classAux) != "") {
						// Apaga antigos
						$sqlCmd = "DELETE FROM " . $nomeTabela . " WHERE " . $refAux[2] . " = " . intval($id) . " ";
						$db->exec_query($sqlCmd);

						// Insere novos
						foreach($_REQUEST[$classAux] as $item) {
							$sqlCmd = "INSERT INTO " . $nomeTabela . " SET
								" . $refAux[2] . " = " . intval($id) . ",
								" . $refAux[3] . " = " . intval($item) . " ";
							$db->exec_query($sqlCmd);
						}
					}
				}
			}
		}

		$classe = str_replace("_model","",get_class($this)); 
 		carrega_classe($classe);
 		if(class_exists($classe)) {
			$this->controller = new $classe();
		} 		

		// Gera permalink
		if($this->controller != null && trim($_REQUEST["permalink"]) == "") {
			$this->controller->set_var("id",$id);
			$this->controller->carrega_dados();
			$this->controller->gera_permalink(true);
		}
	}
	
	function do_after_excluir($id)
	{		
		//NO
		return true;
	}

	function do_after_excluir_massa()
	{
		// NO
		return true;
	}

	function do_after_ativar_desativar($id)
	{
		//$this->id = $id;
		//$this->carrega_dados();
	}
	
	function get_foreign_key_index($campo)
	{
		$retorno = false;
		$a=0;
		while($a<sizeof($this->foreign_keys))
		{
			$array_aux = $this->foreign_keys[$a];
			if($array_aux[0] == $campo)
			{		
				$retorno = $a;
				break;
			}
			$a++;
		}
		return $retorno;
	}
	
	function set_var($var,$value)
	{
		//return $this->__set($var,$value);
		$this->$var = $value;
	}
	
	function get_var($var)
	{
		//return $this->__get($var);
		return $this->$var;
	}

	function get_var_format($var)
	{
		if($this->controller != null) {
			return $this->controller->get_var_format($var);
		} else {
			return $this->get_var($var);
		}
	}
	
	function get_array_sql_modulo($sqlCmd)	
	{	
		$resCmd = $this->get_result_sql($sqlCmd);
		
		$array_aux = array();		
		
		$num_rows = $this->result_num_rows($resCmd);
		
		for($i=0;$i<$num_rows;$i++)
		{	  
		  $class = $this->get_result_field($resCmd,$i,'nome_modulo') . "_model";		  

		  $keys = array_keys(get_class_vars($class));
		  $fields_tabela = $this->get_fields_name($resCmd);
		  
		  $claAux = new $class();		  
		  for($w=0;$w<sizeof($claAux->primary_keys);$w++)
		  {		  
			$pk = $claAux->primary_keys[$w];
			$claAux->set_var($pk,$this->get_result_field($resCmd,$i,$pk)); 
		  }	
		  $claAux->carrega_dados();
		  
		  $array_aux[sizeof($array_aux)] = $claAux;		  
		}

		return $array_aux;
	}

	function get_joins($arrayAux, $w, $alias, $deepJoin){
		$classAux = $arrayAux[1] . "_model";
		$objAux = new $classAux();
		$sqlLeftJoin = " LEFT JOIN " . $objAux->nome_tabela . " aux" . $w . " ON ". $alias . "." . $arrayAux[0] . " = aux" . $w . "." . $arrayAux[2] . " ";

		$sqlFieldsJoin = "";
		if($alias != "principal") {
			$fields_tabela = $objAux->get_fields_name();
			foreach($fields_tabela as $field) {
				$sqlFieldsJoin .= ", " . $alias . "." . $field . " AS " . $arrayAux[1] . "_" . $field;
			}
		}

		if($deepJoin)
		{
			for($y=0;$y<sizeof($objAux->foreign_keys);$y++)
			{
				if($objAux->foreign_keys[$y][0] <> "usuario_criacao" && 
				   $objAux->foreign_keys[$y][0] <> "usuario_atualizacao" &&
				   $objAux->foreign_keys[$y][1] <> "cms_grupos_usuarios"){
					$joins = $this->get_joins($objAux->foreign_keys[$y],$w . "_" . $y,"aux" . $w,$deepJoin);
					$sqlLeftJoin .= $joins[0];
					$sqlFieldsJoin .= $joins[1];
				}
	    	}
		}

		return array($sqlLeftJoin,$sqlFieldsJoin);
	}

	function get_nome_tabela()
	{
		return $this->nome_tabela;
	}

	function get_upload_folder($var = "")
	{
		$folder = UPLOAD_FOLDER;
		if(trim($var) != "" && trim($this->upload_folders[$var]) != "") {
			$folder = trim($this->upload_folders[$var]);
		}
		else if(trim($this->upload_folder) != "") {
			$folder = trim($this->upload_folder);
		}
		return $folder;
	}

	function log_view($arrayInfos = null) {
		$this->views++;
		$this->edita(false);

		carrega_classe("log_views");

		$log = new log_views();
		$log->inicia_dados();
		$log->set_var("modulo_registro",str_replace("_model","",get_class($this)));
		$log->set_var("id_registro",$this->get_var("id"));
		$log->set_var("ip",get_ip());

		$keys = array_keys($arrayInfos);

		$vars = array("nome", "idade", "sexo", "relacionamento", "geolocalizacao", "cidade", "estado", "pais", "id_facebook", "email", "tags");

		foreach($vars as $var) {
			$value = "";

			if(in_array($var, $keys)) { 
				$value = $arrayInfos[$var];
			}
			else {
				// Pega dos cookies

				// TODO
			}

			if(trim($value) != "") {
				$log->set_var($var,output_decode($value));
			}
		}
		
		$idAux = $log->inclui(false, true);

		return $this->views;
	}

	function salva_permalink($id, $permalink = "") {
		global $db;

		$sqlCmd = "UPDATE " . $this->nome_tabela . " SET permalink = \"" . $db->escape_string(output_decode($permalink)) . "\" WHERE id = " . intval($id) . " ";
		$resCmd = $db->exec_query($sqlCmd);
		
		return $resCmd;
	}

	function get_view_field_value() {
		if($this->controller != null && $this->controller->view != null) {
			return $this->controller->view->get_view_field_value();
		} else {
			return $this->get_var("id");
		}
	}

	function get_id_field_value() {
		if($this->controller != null && $this->controller->view != null) {
			return $this->controller->view->get_id_field_value();
		} else {
			return $this->get_var("id");
		}
	}
}

?>