<?php

class cms_imagens_model extends model{

	public $titulo;
	public $arquivo;
	public $modulo_registro;
	public $id_registro;
	public $campo_registro;
    
    function __construct(){
		// Instancia o Objeto
		$this->nome_tabela = DBTABLE_CMS_IMAGENS;
		
		parent::__construct();

		$this->crop_original = false;
	
		$this->upload_folder = UPLOAD_FOLDER . "original/";
		
		$this->array_crop_fields = array("arquivo");
    }
}

?>