<?php

class log_views_model extends model{

		public $modulo_registro;
		public $id_registro;
		public $nome;
		public $idade;
		public $sexo;
		public $relacionamento;
		public $geolocalizacao;
		public $cidade;
		public $estado;
		public $pais;
		public $ip;
		public $id_facebook;
		public $email;
		public $tags;

		function __construct(){
			// Instancia o Objeto
			$this->nome_tabela = DBTABLE_LOG_VIEWS;
			$this->array_file_fields = array("email");
			
			parent::__construct();
		}
}
    
?>