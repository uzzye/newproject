<?php

class idiomas_model extends model{

		public $identificador;
		public $titulo;

		function __construct(){
			// Instancia o Objeto
			$this->nome_tabela = DBTABLE_IDIOMAS;
			$this->array_required_fields = array("identificador","titulo");
			
			parent::__construct();
		}
}
    
?>