<?php
    
class contatos_model extends model{

	public $nome;
	public $mensagem;
	public $email;
	public $ip;
	public $telefone;
	public $assunto;
	public $id_referencia;
	public $referencia;
	public $recebe_novidades;
	public $cidade;
	public $estado;
	public $arquivo;

	// v3.0
	public $conversao;
	public $valor_conversao;
	public $data_conversao;

	function __construct(){
		global $_CMS_CONFIG;
		
		// Instancia o Objeto
		$this->nome_tabela = DBTABLE_CONTATOS;
		
		$this->array_required_fields = array("nome","mensagem","email","ip","telefone","assunto","recebe_novidades");
		$this->array_file_fields = array("arquivo");
		$this->upload_folders["arquivo"] = UPLOAD_FOLDER . "contact/";

		parent::__construct();
	}

	function do_converter($valor_conversao = "")
	{
		global $modulo, $db;		

		$idAux = $this->id;
		
		$sqlCmd = "UPDATE " . $this->nome_tabela . " SET
			valor_conversao = IF(conversao = 0,\"" . $valor_conversao . "\",NULL),
			data_conversao = IF(conversao = 0,NOW(),NULL),
			conversao = IF(conversao = 1,0,1)
		WHERE id = " . intval($idAux) . " ";	
		$resCmd = $db->exec_query($sqlCmd);
		
		if(method_exists($this,"do_after_converter"))
		{
			$this->do_after_converter($idAux);
		}

		$this->carrega_dados();
		
		return $resCmd;
	}

	function get_conversoes($dataIni = "", $dataFim = "", $conversao = 1) {
		global $db;

		$varAux = "data_criacao";
		/*if( intval($conversao) == 1) {
			$varAux = "data_conversao";
		}*/

		$sqlWhere = "";
		if(trim($dataIni) != "") {
			$sqlWhere .= " AND DATE(convs." . $varAux . ") >= \"" . $dataIni . "\" ";
		}
		if(trim($dataFim) != "") {
			$sqlWhere .= " AND DATE(convs." . $varAux . ") <= \"" . $dataFim . "\" ";
		}

		$sqlAux = "(SELECT COUNT(*) FROM " . $this->nome_tabela . " WHERE MONTH(data_criacao) = MONTH(convs.data_criacao))";

		$sqlCmd = "SELECT COUNT(*) AS count_conv_mes,
		SUM(convs.valor_conversao) AS valor_conv_mes,
		MONTH(convs." . $varAux . ") AS mes,
		(COUNT(*)/" . $sqlAux . ") AS taxa_conv_mes,
		" . $sqlAux. " AS count_total_mes
		FROM " . $this->nome_tabela . " convs
		WHERE convs.conversao = " . intval($conversao) . "
		" . $sqlWhere . "
		GROUP BY MONTH(convs." . $varAux . ")
		ORDER BY mes";

		$resAux = $db->exec_query($sqlCmd);

		return $resAux;
	}
}
    
?>