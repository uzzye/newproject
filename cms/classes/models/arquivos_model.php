<?php

include_once(ROOT_CMS . "classes/models/idiomas_model.php");
include_once(ROOT_CMS . "classes/models/arquivos_categorias_model.php");

class arquivos_model extends model{

  public $id_categoria; // int(11) DEFAULT NULL,
  public $arquivo; // tinytext DEFAULT NULL,
  public $titulo_pt; // varchar(255) DEFAULT NULL,
  public $titulo_en; // varchar(255) DEFAULT NULL,
  public $id_idioma; // int(11) DEFAULT NULL,
  
  function __construct(){
    // Instancia o Objeto
    $this->nome_tabela = DBTABLE_ARQUIVOS;
    
    parent::__construct();
      
    $this->reference_models[sizeof($this->reference_models)] = array("id_categoria","arquivos_categorias","id");
    $this->foreign_keys[sizeof($this->foreign_keys)] = array("id_categoria","arquivos_categorias","id");
    $this->reference_models[sizeof($this->reference_models)] = array("id_idioma","idiomas","id");
    $this->foreign_keys[sizeof($this->foreign_keys)] = array("id_idioma","idiomas","id");
    $this->array_file_fields = array("arquivo");
    $this->upload_folders["arquivo"] = UPLOAD_FOLDER . "files/";
  }
}

?>