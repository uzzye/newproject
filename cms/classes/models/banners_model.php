<?php
//include_once(ROOT_CMS . "classes/models/teste_model.php");

class banners_model extends model{

		public $id_referencia;
		public $titulo_pt;
		public $titulo_en;
		public $titulo_es;
		public $imagem_conteudo_pt;
		public $imagem_conteudo_en;
		public $imagem_conteudo_es;
		public $descricao_pt;
		public $descricao_en;
		public $descricao_es;
		public $imagem_bg_pt;
		public $imagem_bg_mobile_pt;
		public $imagem_bg_en;
		public $imagem_bg_mobile_en;
		public $imagem_bg_es;
		public $imagem_bg_mobile_es;
		public $video_url;
		public $video_embed;
		public $url;
		public $target;
		public $texto_botao_pt;
		public $texto_botao_en;
		public $texto_botao_es;
		public $ranking;
		public $tempo_duracao;
		public $tempo_transicao;
		public $video_autoplay;
		public $cinemagraph;
		public $cinemagraph_mobile;
		public $cor_overlay;

		function __construct(){
			// Instancia o Objeto
			$this->nome_tabela = DBTABLE_BANNERS;
			//$this->foreign_keys[sizeof($this->foreign_keys)] = array("id_referencia","teste","id");
			$this->array_crop_fields = array("imagem_bg_pt","imagem_bg_mobile_pt","imagem_bg_en","imagem_bg_mobile_en","imagem_bg_es","imagem_bg_mobile_es","imagem_conteudo_pt","imagem_conteudo_en","imagem_conteudo_es");
			$this->array_file_fields = array("cinemagraph","cinemagraph_mobile");

			$this->array_required_fields = array("titulo_pt");
			
			parent::__construct();
		}
}
    
?>