<?php

class Uzzye_Field
{
	public $label;
	public $sub_label;
	public $name;
	public $id;
	public $value;
	public $type;	
	public $readonly;
	public $required;		
	public $style_class;	
	public $default_value;
	public $li_class;
	public $hint;
	public $focus;
	public $onchange;
	public $help_text;

	// v3.0
	public $show_in_data_table;
	
	function __construct($name = "", $id = "", $label = "", $default_value = "", $readonly = "", $style_class = "", $li_class = "", $sub_label = "", $max_length = "")
	{		 
		$this->name = $name; 
		$this->id = $id; 
		$this->label = $label;
		$this->hint = ""; 
		$this->default_value = $default_value; 
		$this->readonly = $readonly; 
		$this->style_class = $style_class; 
		$this->li_class = $li_class; 
		$this->sub_label = $sub_label;
		$this->help_text = "";
		$this->show_in_data_table = false;
		if(trim($li_class) == "")
		{
			$this->li_class = "w50p";
		}
		else
		{
			$this->li_class = $li_class;
		}

		if(trim($max_length) <> "")
		{
			$this->max_length = $max_length;
		}
		
		$this->required = false;
	}
	
	function get_display_label()
	{
		global $modulo; 
		
		$label = $this->label;
		if(trim($this->sub_label) <> "" && $this->type != "radio" && $this->type != "checkbox")
		{
			$label .= "<br/><small class=\"pl-0\">" . $this->sub_label . "</small>";
		}
		
		//TODO
		$arq = ROOT_CMS . "_estilo/images/hints/" . $modulo . "_" . $this->name . ".jpg";
		if(file_exists($arq)){
			return "<label class=\"control-label mb-0\" for=\"" . ($this->id) . "\">" . ($label) . " <a class=\"hint-a\" rel=\"$arq\" title=\"" . $this->hint . "\"><img src=\"" . ROOT_CMS . "_estilo/images/geral/info.png\" border=\"0\" /></a></label>";
		} else {
			return "<label class=\"control-label mb-0\" for=\"" . ($this->id) . "\">" . ($label) . "</label>";							
		}
	}
		
	function ini_field_set()
	{
		global $modulo; 

		// v3.0		
		if(stristr($this->li_class,"w5p")) {
			$this->li_class .= " col-sm-1 ";		
		}	
		else if(stristr($this->li_class,"w10p")) {
			$this->li_class .= " col-sm-2 ";		
		}	
		else if(stristr($this->li_class,"w25p")) {
			$this->li_class .= " col-sm-3 ";		
		}
		else if(stristr($this->li_class,"w33p")) {
			$this->li_class .= " col-sm-4 ";		
		}
		else if(stristr($this->li_class,"w50p")) {
			$this->li_class .= " col-sm-6 ";		
		}		
		else if(stristr($this->li_class,"w66p")) {
			$this->li_class .= " col-sm-8 ";		
		}		
		else if(stristr($this->li_class,"w100p")) {
			$this->li_class .= " col-sm-12 ";		
		} else {
			$this->li_class .= " col-sm-12 ";
		}
		
		return "<div class=\"" . $this->li_class . "\">
			<div class=\"form-group\">";
	}
	
	function end_field_set()
	{		
		return "</div>
		</div>";
	}

	function setFocus($bFocus){
		$this->focus = $bFocus;
	}
}

?>