<?php
class Uzzye_HiddenField extends Uzzye_Field
{	
	
    function __construct($name = "", $id = "", $label = "", $default_value = "", $readonly = "", $style_class = "", $li_class = "", $sub_label = "")
	{
		$this->type = "hidden";
		
		parent::__construct($name, $id, $label, $default_value, $readonly, $style_class, $li_class, $sub_label);
		
		$this->li_class = "";
	}
	
	function get_display_field()
	{
		$result = "";
		$result .= $this->ini_field_set();
		$result .= $this->get_display_label();		
		
		$result .= "<input autocomplete=\"off\" type=\"hidden\" id=\"" . ($this->id) . "\" name=\"" . ($this->name) . "\" value=\"" . ($this->value) . "\" class=\"" . ($this->style_class) . "\" ";
		if($this->required)
		{
			$result .= " required='required' ";
		}
		$result .= "/>";
		$result .= $this->end_field_set();
		return $result;
	}
	
	function get_display_label()
	{
	}
	
	function ini_field_set()
	{		
	}
	
	function end_field_set()
	{			
	}
	
	function get_db_value()
	{
		return output_decode($this->value);
	}	
	
	function set_value($valor)
	{
		$this->value = get_output($valor);
	}
}
?>