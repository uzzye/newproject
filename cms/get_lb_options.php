<?php

include_once("config.php");

include_once("verificaLogado.php");

ob_clean();

$root = rawurldecode($_REQUEST["root"]);
$rootVal = rawurldecode($_REQUEST["rootVal"]);
$modulo = rawurldecode($_REQUEST["modulo"]);
$value_field = rawurldecode($_REQUEST["value_field"]);
$html_field = rawurldecode($_REQUEST["html_field"]);
$sqlOrderBy = rawurldecode($_REQUEST["sqlOrderBy"]);
$sqlWhere = rawurldecode($_REQUEST["sqlWhere"]);

$exprValor =  $cryptDec->phpDecrypt(rawurldecode($_REQUEST["exprValor"]));
$exprDisplay = $cryptDec->phpDecrypt(rawurldecode($_REQUEST["exprDisplay"]));

$arraySel = explode(",",rawurldecode($_REQUEST["sel"]));

//var_dump($arraySel); die();

$sqlTableJoin = rawurldecode($_REQUEST["sqlTableJoin"]);
$sqlFieldJoinFrom = rawurldecode($_REQUEST["sqlFieldJoinFrom"]);
$sqlFieldJoinTo = rawurldecode($_REQUEST["sqlFieldJoinTo"]);

include_once(ROOT_CMS . "classes/models/" . $modulo . "_model.php");
include_once(ROOT_CMS . "classes/controllers/" . $modulo . "_controller.php");
include_once(ROOT_CMS . "classes/views/" . $modulo . "_view.php");

$result = "";

if(trim($modulo) <> "" &&
	trim($value_field) <> "" &&
	trim($html_field))
{
	if(trim($sqlTableJoin) == ""){
		$classAux = $modulo . "_controller";
		$objAux = new $classAux();
		$options = $objAux->get_array_options($value_field,$html_field,$sqlWhere,$sqlOrderBy,$exprValor,$exprDisplay,"",$arraySel);

		if(trim($root) <> "")
		{
			$result .= "<option value=\"$rootVal\">$root</option>";			
		}
		
		foreach($options as $option)
		{
			$val_id = $option[0];
			$val_display = $option[1];
			
			$result .= "<option value=\"" . get_output($val_id) . "\" ";	
			if(in_array(get_output($val_id),$arraySel))
			{
				$result .= "selected=\"true\"";
			}	
			$result .= ">" . get_output($val_display) . "</option>";
		}	
	} else {

		if(trim($root) <> "")
		{
			$result .= "<option value=\"$rootVal\">$root</option>";			
		}

		//$sqlCmd = "SELECT * FROM " . $sqlTableJoin . " WHERE " . $sqlFieldJoinFrom . " IN(\"" . $sqlWhere . "\") ";
		$sqlCmd = "SELECT * FROM " . $sqlTableJoin . " WHERE " . $sqlWhere . " ";

		$array_result = $db->exec_query($sqlCmd, true);		
		$resSql = $array_result[0];

		$arrayOpt = array();

		if($db->num_rows($resSql) > 0){
			for($w=0;$w<$db->num_rows($resSql);$w++){

							// id
				$sqlWhere = $value_field . " = " . $db->result_field($resSql,$w,$sqlFieldJoinTo);
				//var_dump($sqlWhere); die();

				$classAux = $modulo . "_controller";
				$objAux = new $classAux();
				$options = $objAux->get_array_options($value_field,$html_field,$sqlWhere,$sqlOrderBy,$exprValor,$exprDisplay);
				
				foreach($options as $option)
				{
					$val_id = $option[0];
					$val_display = $option[1];

					if(!in_array($val_id, $arrayOpt)){
						$arrayOpt[sizeof($arrayOpt)] = $val_id;

						$result .= "<option value=\"" . get_output($val_id) . "\" ";	
						if(in_array(get_output($val_id),$arraySel))
						{
							$result .= "selected=\"true\"";
						}	
						$result .= ">" . get_output($val_display) . "</option>";
					}
				}
			}
		}	
	}
}
echo $result;

?>