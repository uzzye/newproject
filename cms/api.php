<?php

@set_time_limit(0);
@ignore_user_abort(1);
@ini_set('max_execution_time', 0);

$DB_persistent = true;
$minify = true;

include_once("config.php");

@ini_set('display_errors', 1);
@ini_set('error_reporting', E_ALL ^ E_NOTICE ^ E_DEPRECATED ^ E_WARNING ^ E_STRICT);
@setlocale(LC_ALL,"ptb");
@setlocale(LC_ALL,"pt_BR");
// Fix for float number with incorrect decimal separator.
@setlocale(LC_NUMERIC, "us");
@setlocale(LC_NUMERIC, "en_US");

$API_VER = "API Uzzye v3.0";

$SAFE_MODE = false;

if(!isset($_REQUEST["env"]) || strtoupper($_REQUEST["env"]) == "PROD") {
	$API_URL = "http://uzzye.com/";
} else if (strtoupper($_REQUEST["env"]) == "DEV") {
	$API_URL = "http://localhost/";
}

$API_HEADER = array();
array_push($API_HEADER,'Content-Type: application/json');
array_push($API_HEADER,'Accept: application/json');
if(!isset($_REQUEST["isTotem"]) || intval($_REQUEST["isTotem"]) == 0) {
	array_push($API_HEADER,'SC-IsTotem: 0');
} else if (intval($_REQUEST["isTotem"]) == 1) {
	array_push($API_HEADER,'SC-IsTotem: 1');
}

// CONFIG DO FACEBOOK
// SDK 4
/*$FB_config = array(
  'app_id' => FACEBOOK_ID,
  'app_secret' => FACEBOOK_SECRET,
  'default_graph_version' => 'v2.5'
);*/

// OLD SDK
$FB_config = array(
	'appId' => FACEBOOK_ID,
	'secret' => FACEBOOK_SECRET,
	'fileUpload' => false, // optional
	'allowSignedRequest' => false, // optional, but should be set to false for non-canvas apps
);

// PARAMETROS PASSADOS POR $_REQUEST (DEV)
$token = rawurldecode($_REQUEST["token"]);
$classAux = rawurldecode($_REQUEST["mod"]);
$methodAux = rawurldecode($_REQUEST["meth"]);

$sWhereAux = rawurldecode($_REQUEST["where"]);
//$sWhereAux = base64_decode($sWhereAux);
$limitAux = rawurldecode($_REQUEST["limit"]);
$orderAux = rawurldecode($_REQUEST["order"]);
$groupAux = rawurldecode($_REQUEST["group"]);
$selAux = rawurldecode($_REQUEST["sel"]);
$llAux = rawurldecode($_REQUEST["ll"]);
$buscaAux = rawurldecode($_REQUEST["busca"]);
$dtAux = rawurldecode($_REQUEST["dt"]);
$notAux = rawurldecode($_REQUEST["no"]);
if(trim($notAux) == "") {
	$notAux = rawurldecode($_REQUEST["not"]);
}
$textsAux = rawurldecode($_REQUEST["texts"]);

$joinAux = false;
$base64 = false;
$howfar = false;
$princiAux = "";

$globalCompKey = null;

$data = array();
if($_REQUEST != null) {
	$data = array_merge($data,$_REQUEST);
}
$jsonData = json_decode(file_get_contents('php://input'), true);
if($jsonData != null) {
	$data = array_merge($data,$jsonData);
}

if(isset($_REQUEST["get"]) || intval($_REQUEST["get"]) == 1) {
	$data = $_REQUEST;
} else {
	if(trim($data["where"]) != ""){
		$sWhereAux = $data["where"];
	}
	if(trim($data["limit"]) != ""){
		$limitAux = $data["limit"];
	}
	if(trim($data["order"]) != ""){
		$orderAux = $data["order"];
	}
	if(trim($data["group"]) != ""){
		$groupAux = $data["group"];
	}
	if(trim($data["ll"]) != ""){
		$llAux = $data["ll"];
	}
	if(trim($data["busca"]) != ""){
		$buscaAux = $data["busca"];
	}
	if(trim($data["dt"]) != ""){
		$dtAux = $data["dt"];
	}
	if(trim($data["sel"]) != ""){
		$selAux = $data["sel"];
	}
	if(trim($data["no"]) != ""){
		$notAux = $data["no"];
	} else {
		if(trim($data["not"]) != ""){
			$notAux = $data["not"];
		}	
	}
	if(trim($data["texts"]) != ""){
		$textsAux = $data["texts"];
	} 
}

// MONTA CAMPOS QUE NÃO MOSTRA
if(trim($notAux) != ""){
	$notAux = explode(",",$notAux);
}

// MONTA CAMPOS DE SELEÇÃO
if(trim($selAux) != ""){
	$selAux = explode(",",$selAux);
}

// MONTA CAMPOS DE TEXTO, QUE NÃO CONVERTEM QUEBRA DE LINHA
if(trim($textsAux) != ""){
	$textsAux = explode(",",$textsAux);
}

// MONTA CAMPO JOIN
if(isset($_REQUEST["join"]) || $_REQUEST["join"] == "true" || isset($data["join"]) || $data["join"] == "true"){
	$joinAux = true;
	$princiAux = "principal.";
}
// MONTA CAMPO DISTANCIA
if(isset($_REQUEST["howfar"]) || $_REQUEST["howfar"] == "true" || isset($data["howfar"]) || $data["howfar"] == "true"){
	$howfar = true;
}
// MONTA CAMPO BASE64
if(isset($_REQUEST["base64"]) || $_REQUEST["base64"] == "true" || isset($data["base64"]) || $data["base64"] == "true"){
	$base64 = true;
}

// SE TEM GPS
$bGPS = false;
if(trim($llAux) != ""){
	$bGPS = true;
}

$sWhereAux = stripslashes($sWhereAux);
$orderAux = stripslashes($orderAux);
$limitAux = stripslashes($limitAux);
$groupAux = stripslashes($groupAux);

// MONTA FILTROS

$i=1;
$sWhereFiltros = "";

if(trim($sWhereAux) <> "") {
	if(trim($sWhereFiltros) <> "")
	{$sWhereFiltros .= " AND ";}

	//$sWhereAux = rawurldecode($sWhereAux);

	$sWhereFiltros .= $sWhereAux;
}

while(trim($_REQUEST["field".$i]) <> "")
{
	if(trim($sWhereFiltros) <> "")
	{$sWhereFiltros .= " AND ";}
	
	if(!isset($_REQUEST["match".$i]) || trim($_REQUEST["match".$i]) == "equal"){
		$sWhereFiltros .= " " . urldecode($_REQUEST["field".$i]) . " = \"" . urldecode($_REQUEST["value".$i]) . "\" ";
	}
	else if(trim($_REQUEST["match".$i]) == "gt"){
		$sWhereFiltros .= " " . urldecode($_REQUEST["field".$i]) . " > \"" . urldecode($_REQUEST["value".$i]) . "\" ";
	}
	else if(trim($_REQUEST["match".$i]) == "lt"){
		$sWhereFiltros .= " " . urldecode($_REQUEST["field".$i]) . " < \"" . urldecode($_REQUEST["value".$i]) . "\" ";
	}
	else if(trim($_REQUEST["match".$i]) == "gte"){
		$sWhereFiltros .= " " . urldecode($_REQUEST["field".$i]) . " >= \"" . urldecode($_REQUEST["value".$i]) . "\" ";
	}
	else if(trim($_REQUEST["match".$i]) == "lte"){
		$sWhereFiltros .= " " . urldecode($_REQUEST["field".$i]) . " <= \"" . urldecode($_REQUEST["value".$i]) . "\" ";
	}
	else if(trim($_REQUEST["match".$i]) == "dif"){
		$sWhereFiltros .= " " . urldecode($_REQUEST["field".$i]) . " != \"" . urldecode($_REQUEST["value".$i]) . "\" ";
	}
	else if(trim($_REQUEST["match".$i]) == "null"){
		$sWhereFiltros .= " ISNULL(" . urldecode($_REQUEST["field".$i]) . ") ";
	}
	else if(trim($_REQUEST["match".$i]) == "nnull"){
		$sWhereFiltros .= " !ISNULL(" . urldecode($_REQUEST["field".$i]) . ") ";
	}
	else if(trim($_REQUEST["match".$i]) == "like"){
		$sWhereFiltros .= " " . urldecode($_REQUEST["field".$i]) . " LIKE \"%" . urldecode($_REQUEST["value".$i]) . "%\" ";
	}

	$i++;
}

if(trim($sWhereFiltros) != "") {
	$sWhereFiltros = (output_decode($sWhereFiltros));
}

// INICIA RETORNO

ob_clean();

header("Content-Type: application/json; charset=UTF-8");
/*if(!isset($_REQUEST["isWeb"]) || intval($_REQUEST["isWeb"]) == 0) {
	header("Access-Control-Allow-Origin: http://localhost:3030");
} else*/ {
	header("Access-Control-Allow-Origin: *");
}
header("Access-Control-Allow-Headers: *");

// SE VALIDAR O TOKEN
if($token == "uzzyeabuse"){

	// SE FOR PARA PESQUISAR
	if(trim($methodAux) == "" || trim($methodAux) == "g" || trim($methodAux) == "get") {
		if(trim($classAux) <> ""){
			carrega_classe($classAux);

			if(class_exists($classAux)) {

				$objAux = new $classAux();
				
				$sWhere = "(true)";
				if($sWhereFiltros <> NULL && $sWhereFiltros <> "")
				{
					$sWhere .= " AND " . $sWhereFiltros;
				}
				
				$array_obj = $objAux->get_array_ativos($sWhere,$orderAux,$limitAux,$joinAux, false, $groupAux);

				getJSONFinal($array_obj);
			}
			else {
			?>{
	"result": "<?php echo $API_VER; ?>",
	"timestamp_serv": "<?php echo Date("Y-m-d H:i:s"); ?>"
}<?php
			}
		}
		else {
			?>{
	"result": "<?php echo $API_VER; ?>",
	"timestamp_serv": "<?php echo Date("Y-m-d H:i:s"); ?>"
}<?php
		}
	} 

	// SE FOR PARA PEGAR VERSÃO
	else if(trim($methodAux) == "versao") {
		if(trim($_CONFIG["versoes_app"]) != "") {
			$versoes = explode(";",$_CONFIG["versoes_app"]);

			if(is_array($versoes)) {
				$strRes = "";
				for($w=0;$w<sizeof($versoes);$w++){
					if(trim($versoes[$w]) != "") {
						$versao = explode("=",$versoes[$w]);

						if(is_array($versao)) {
							$so = $versao[0];
							$ver = str_replace(";","",$versao[1]);

							if($w>0) {
								$strRes .= ",";
							}

							$strRes .= "{
								\"" . $so . "\": \"" . $ver . "\"
							}";
						}
					}
				}

				?>{
	"result": [<?php echo $strRes; ?>]
}<?php
			}
		}
	}

	include_once("api_modules/api_getset_user.php");

	// SE FOR PARA PEGAR ITENS ESPECIFICOS
	if(trim($methodAux) == "nome_entidade") {
		$json = array();
		$json["termo"] = $data["termo"];
		$json["timestamp"] = $data["timestamp"];
		$json["ip"] = $data["ip"];// = "127.0.0.1";
		$json["token_app"] = $data["token_app"];// = "DuendeY2014";
		$json["id_so"] = $data["id_so"];// = "97";

		$erro = "";

		// PROCURA ITENS

		/*$sqlCmd = "SELECT
			item.id AS id,
			item.nome AS nome,
		FROM " . DBTABLE_ENTIDADE . " item
		WHERE item.ativo = 1 ";
		
		if(trim($json["termo"]) != "") {
			$sqlCmd .= " AND (";
		
			$termo = $json["termo"];

			$sqlCmd .= " item.nome LIKE \"%" . output_decode($termo) . "%\" ";
			$sqlCmd .= " OR item.nome LIKE \"%" . output_decode($termo) . "%\" ";

			$sqlCmd .= ") ";
		}
		
		$sqlCmd .= " GROUP BY item.id ";
		$sqlCmd .= " ORDER BY nome ASC";

		$resCmd = $mysql->exec_query($sqlCmd);

		$array_final = array();

		if($mysql->num_rows($resCmd) > 0) {

			$iCount = 0;
			while ($objAux = mysql_fetch_object($resCmd)){
				$array_final[sizeof($array_final)] = $objAux;
				$iCount++;

				if(trim($json["termo"]) != "" && $iCount >= 10) {
					break;
				} else if($iCount >= 50) {
					break;
				}
			}
		}*/

		if(trim($erro) != "") {				
			$obj = new stdClass();
			$obj->erro = output_decode($erro);
			$obj->apimsg = $obj->erro;
			$obj->apistatus = 0;
			$array_final = array(array($obj));
		}

		if(!is_array($selAux)) {
			$selAux = array();
		}
		
		$selAux[sizeof($selAux)] = "erro";

		$selAux[sizeof($selAux)] = "apimsg";
		$selAux[sizeof($selAux)] = "apistatus";

		getJSONFinal($array_final, true);
	}

	/*else if(trim($classAux) <> "array") {
		$array_final = array(
			array("id"=>1,"nome"=>"Mau Salamon","itens"=> array(["id"=>2],["id"=>3],["id"=>4])
			),
			array("id"=>2,"nome"=>"Mau Salamon 2","itens"=> array(["id"=>2],["id"=>3],["id"=>4])
			)
		);
		getJSONFinal($array_final, true, "array");

	}*/

	// SE FOR PARA EXECUTAR OUTROS MÉTODOS QUE NÃO PESQUISAR
	if(trim($classAux) <> "") {

		// ENTIDADE
		if($classAux == "nome_entidade") {

			// INCLUSÃO
			if(trim($methodAux) == "i" || trim($methodAux) == "inc" || trim($methodAux) == "set"){		
				$json = array();
				$json["mensagem"] = $data["mensagem"];// = "Teste de mensagem...";
				$json["timestamp"] = $data["timestamp"];// = "2014-05-16 00:35:00";
				$json["ip"] = $data["ip"];// = "127.0.0.1";
				$json["token_app"] = $data["token_app"];// = "DuendeY2014";
				$json["id_so"] = $data["id_so"];// = "97";

				$erro = "";

				carrega_classe("nome_entidade");

				$array_final = array();
				$resultName = "nome_entidade";
				$generic = true;

				// CRIA REGISTRO
				$itens = new nome_entidade();

				$itens->inicia_dados();
				
				$itens->set_var("mensagem",output_decode($json["mensagem"]));
				$itens->set_var("data_envio",output_decode($json["timestamp"]));
				$itens->set_var("usuario_criacao",output_decode($json["id_so"]));
				$itens->set_var("data_criacao",Date("Y-m-d H:i:s"));
				//$ip = get_ip();
				$ip = $json["ip"];
				$itens->set_var("ip",output_decode($ip));
				// FIM APENAS

				$id_inc = $itens->inclui(false,true);

				$sqlCmd = " SELECT it.*
				FROM " . DBTABLE_ENTIDADE . " it
				WHERE it.id = " . intval($id_inc) . "
				GROUP BY it.id";

				$resCmd = $mysql->exec_query($sqlCmd);

				if($mysql->num_rows($resCmd) > 0) {

					$iCount = 0;
					while ($objAux = mysql_fetch_object($resCmd)){
						
						$array_final[sizeof($array_final)] = $objAux;

						$iCount++;
					}
				}

				if(trim($erro) != "") {				
					$obj = new stdClass();
					$obj->erro = output_decode($erro);
					$obj->apimsg = $obj->erro;
					$obj->apistatus = 0;
					$array_final = array(array($obj));
				}

				if(!is_array($selAux)) {
					$selAux = array();
				}
				
				$selAux[sizeof($selAux)] = "erro";

				$selAux[sizeof($selAux)] = "apimsg";
				$selAux[sizeof($selAux)] = "apistatus";
				
				// MONTA JSON
				getJSONFinal($array_final, $generic, $resultName);
			}
		}
	}
	/* else {
		?>{
		"result": "<?php echo $API_VER; ?>"
	}<?php
	} */
}
else {
	?>{
	"result": "<?php echo $API_VER; ?>"
}<?php
}

function getJSONModel($objAux,$nivel = 1,$ignField = ""){
	global $notAux, $selAux, $llAux, $base64, $howfar;
	
	$bProcessaCampo = true;

	if(intval($objAux->id) > 0){
		if(method_exists($objAux,"get_fields_name")) {
			$array_fields = $objAux->get_fields_name();
		} else {
			//$array_fields = array_keys(get_object_vars($objAux));
			$array_fields = get_object_vars($objAux);
		}

		$iCountFields = 0;

		// ABRE OBJETO
		tab($nivel); ?>{<?php new_line();

		// PERCORRE CAMPOS
		foreach($array_fields as $fieldAux){
			if($fieldAux != "usuario_criacao" &&
				$fieldAux != "usuario_atualizacao" &&
				//$fieldAux != "ativo" &&
				$fieldAux != "senha" &&
				$fieldAux != $ignField)
			{
				if(method_exists($objAux,"get_foreign_key_index")) {
					// SE TEM OBJETO FILHO, INCLUI
					if($fieldAux != "usuario_criacao" &&
						$fieldAux != "usuario_atualizacao" &&
						$objAux->get_foreign_key_index($fieldAux))
					{
						$objRef = $objAux->get_objeto_referencia($fieldAux);

						$classAux = str_replace("_model","",get_class($objRef));

						$bShow = true;
						if(is_array($selAux) && !in_array($classAux, $selAux, true)) {
							$bShow = false;
						}

						if($bShow && (!is_array($notAux) || !in_array($classAux, $notAux, true))) {

							// ESCREVER VALOR DA PROPRIEDADE
							if($iCountFields > 0){
								?>,<?php new_line();
							}

							tab($nivel+1); ?>"<?php echo $classAux; ?>": [<?php new_line(); 

							getJSONModel($objRef,$nivel+1); new_line();

							tab($nivel+1); ?>]<?php

							$iCountFields++;
							
							//$bProcessaCampo = false;
						} else {
							//echo "aqui";
							$bProcessaCampo = true;
						}
					}
				} else {
					$bProcessaCampo = true;
				}

				// SE DEVE MOSTRAR O CAMPO
				if($bProcessaCampo) {
					$bShow = true;
					if(is_array($selAux) && !in_array($fieldAux, $selAux, true)) {
						$bShow = false;
					}

					if($bShow && (!is_array($notAux) || !in_array($fieldAux, $notAux, true))) {
						$valorAux = stripslashes(get_output($objAux->get_var($fieldAux)));
						$valorAux = str_replace("\"","''",$valorAux);
						if(!is_array($textsAux) || !in_array($fieldAux, $textsAux, true)) {
							$valorAux = str_replace("\r","\\r",$valorAux);
							$valorAux = str_replace("\n","\\n",$valorAux);
						}
						$valorAux = str_replace("[ROOT_SERVER]",ROOT_SERVER,$valorAux);
						$valorAux = str_replace("[ROOT_SITE]",ROOT_SITE,$valorAux);
						$valorAux = str_replace("[ROOT_CMS]",ROOT_CMS,$valorAux);
						$valorAux = str_replace("[ROOT]",ROOT,$valorAux);
						$valorAux = str_replace("[UPLOAD_FOLDER]",UPLOAD_FOLDER,$valorAux);
						$valorAuxOrigem = $valorAux;

						// TRATA LOGO
						/*if($fieldAux == "logo")
						{
							$folderAux = UPLOAD_FOLDER;
							if(@trim($objAux->upload_folder) != "" && @trim($objAux->upload_folder) != $folderAux) {
								$folderAux = UPLOAD_FOLDER . $objAux->upload_folder;
							}
							if(@trim($objAux->upload_folders[$fieldAux]) != "" && @trim($objAux->upload_folder) != $folderAux) {
								$folderAux = UPLOAD_FOLDER . $objAux->upload_folders[$fieldAux];
							}

							//$output = @file_get_contents(ROOT_SERVER . ROOT . $valorAuxOrigem . "&" . date("YmdHis"));
							$output = @file_get_contents(getcwd() . DIRECTORY_SEPARATOR . $folderAux . DIRECTORY_SEPARATOR . $valorAuxOrigem);// . "&" . date("YmdHis"));
							$valorAux = base64_encode($output);
						}
						// SE IMAGENS OU ARQUIVOS
						else*/ if(in_array($fieldAux, $objAux->array_crop_fields, true) ||
							    in_array($fieldAux, $objAux->array_file_fields, true)) {
							$folderAux = UPLOAD_FOLDER;
							if(@trim($objAux->upload_folder) != "" && @trim($objAux->upload_folder) != $folderAux) {
								$folderAux = UPLOAD_FOLDER . $objAux->upload_folder;
							}
							if(@trim($objAux->upload_folders[$fieldAux]) != "" && @trim($objAux->upload_folder) != $folderAux) {
								$folderAux = UPLOAD_FOLDER . $objAux->upload_folders[$fieldAux];
							}

							$output = null;
							if(trim($valorAuxOrigem) != "") {
								$output = ROOT_SERVER . ROOT . $folderAux . $valorAuxOrigem;
							}
							$valorAux = ($output);
						}

						if(trim($valorAux) == ""){
							$valorAux = "null";
						} else {
							$valorAux = json_encode($valorAux);
							if(!is_array($textsAux) || !in_array($fieldAux, $textsAux, true)) {
								$valorAux = str_replace("\\\\r\\\\n", "", $valorAux);
							}
						}

						// ESCREVER VALOR DA PROPRIEDADE
						if($iCountFields > 0){
							?>,<?php new_line();
						}
						echo tab($nivel+1); ?>"<?php echo $fieldAux; ?>": <?php echo $valorAux; ?><?php

						// BUSCA BASE64 EM CASO DE IMAGEM OU ARQUIVO
						if($base64) {
							if(in_array($fieldAux, $objAux->array_crop_fields, true) ||
								in_array($fieldAux, $objAux->array_file_fields, true) || 
								$fieldAux == "logo" ||
								$fieldAux == "arquivo") {
								$folderAux = UPLOAD_FOLDER;
								if(@trim($objAux->upload_folder) != "" && @trim($objAux->upload_folder) != $folderAux) {
									$folderAux = UPLOAD_FOLDER . $objAux->upload_folder;
								}
								if(@trim($objAux->upload_folders[$fieldAux]) != "" && @trim($objAux->upload_folder) != $folderAux) {
									$folderAux = UPLOAD_FOLDER . $objAux->upload_folders[$fieldAux];
								}
								
								$output = null;
								if(trim($valorAuxOrigem) != "") {
									$output = @file_get_contents(getcwd() . DIRECTORY_SEPARATOR . $folderAux . DIRECTORY_SEPARATOR . $valorAuxOrigem);// . "&" . date("YmdHis"));
									$valorAux = json_encode(base64_encode($output));
									if(!is_array($textsAux) || !in_array($fieldAux, $textsAux, true)) {
										$valorAux = str_replace("\\\\r\\\\n", "", $valorAux);
									}
								} else {
									$valorAux = ($output);
								}

								?>,<?php new_line();
								echo tab($nivel+1); ?>"<?php echo $fieldAux; ?>_base64": <?php echo $valorAux; ?><?php
							}
						}

						// BUSCA DISTANCIA EM CASO DE GEOLOCALIZACAO
						if($howfar) {
							if($fieldAux == "geolocalizacao") {
								$arrAux1 = explode(",",$llAux);
								$latAux1 = $arrAux1[0];
								$lngAux1 = $arrAux1[1];

								$arrAux2 = explode(",",$valorAux);
								$latAux2 = $arrAux2[0];
								$lngAux2 = $arrAux2[1];

								$distAux = distancia($latAux1,$lngAux1,$latAux2,$lngAux2);

								?>,<?php new_line();
								echo tab($nivel+1); ?>"distancia": "<?php echo $distAux; ?>"<?php
							}
						}

						$iCountFields++;
					}
				}
			}
		}

		// PROCESSA OBJETOS RELACIONADOS
		if($nivel <= 2) {
			if(sizeof($objAux->reference_items) > 0) {
				foreach($objAux->reference_items as $itRef) {
					$classAux = $itRef[1];

					$bShow = true;
					if(is_array($selAux) && !in_array($classAux, $selAux, true)) {
						$bShow = false;
					}

					if($bShow && (!is_array($notAux) || !in_array($classAux, $notAux, true))) {
						carrega_classe($classAux);
						if(class_exists($classAux)) {
							$itsAux = new $classAux();
							$itsAux = $itsAux->get_array_ativos($itRef[2] . " = " . intval($objAux->$itRef[0]),"");

							echo ",";
							new_line();
							tab($nivel+1); ?>"<?php echo $itRef[1]; ?>": [<?php new_line();

							$iCountAux = 0;
							if(is_array($itsAux) && sizeof($itsAux) > 0) {
								foreach($itsAux as $itAux) {
									if($iCountAux > 0) {
										echo ",";
									}
									getJSONModel($itAux,$nivel,$itRef[2]);
									$iCountAux++;
								}
							}

							tab($nivel+1); echo "]";
						}
					}
				}
			}
		}

		// FECHA OBJETO
		new_line(); tab($nivel); ?>}<?php
	} else {
		/*if(intval($iCountObjs) == 0){
			tab($nivel+1);?>"result": "Nenhum registro encontrado"<?php
		}*/
	}
}

function getJSONObject($objAux,$nivel = 1){
	global $notAux, $selAux, $llAux;
	
	$bProcessaCampo = true;
	$bArray = false;

	//if(intval($objAux->id) > 0)
	{
		if(is_array($objAux)) {
			$bArray = true;
			$array_fields = array_keys($objAux);
		} else {
			$array_fields = array_keys(get_object_vars($objAux));
		}

		$iCountFields = 0;

		// PERCORRE CAMPOS
		foreach($array_fields as $fieldAux){

			// SE É ARRAY E TEM ARRAY OU OBJETO FILHO, INCLUI
			if($bArray) {
				if(is_array($objAux[$fieldAux]) || is_object($objAux[$fieldAux])) {
					$objRef = $objAux[$fieldAux];
				
					$classAux = $fieldAux;
					
					$bShow = true;
					if(is_object($objAux->$fieldAux) && (is_array($selAux) && !in_array($classAux, $selAux, true))) {
						$bShow = false;
					}

					if($bShow && (!is_array($notAux) || !in_array($classAux, $notAux, true))) {

						// ESCREVER VALOR DA PROPRIEDADE
						if($iCountFields > 0){
							?>,<?php new_line();
						}

						// ABRE OBJETO
						tab($nivel); ?>{<?php new_line();

						if(is_array($objRef)) {
							tab($nivel+1); ?>"<?php echo $classAux; ?>": [<?php new_line();
						
							getJSONObject($objRef,$nivel+2); new_line();

							tab($nivel+1); ?>]<?php
						} else if(is_object($objRef)) {						
							getJSONObject($objRef,$nivel+2); new_line();
						}

						// FECHA OBJETO
						new_line(); tab($nivel); ?>}<?php

						$iCountFields++;
						
						$bProcessaCampo = false;
					} else {
						$bProcessaCampo = false;
					}
				} else {
					$bProcessaCampo = true;
				}
			}
			// SE NÃO É ARRAY E TEM ARRAY OU OBJETO FILHO, INCLUI
			else if(!$bArray) {
				if(is_array($objAux->$fieldAux) || is_object($objAux->$fieldAux)) {
					$objRef = $objAux->$fieldAux;

					$classAux = $fieldAux;
					
					$bShow = true;
					if(is_object($objAux->$fieldAux) || (is_array($selAux) && !in_array($classAux, $selAux, true))) {
						$bShow = false;
					}

					if($bShow && (!is_array($notAux) || !in_array($classAux, $notAux, true))) {

						// ESCREVER VALOR DA PROPRIEDADE
						if($iCountFields > 0){
							?>,<?php new_line();
						}
						
						// ABRE OBJETO
						/*tab($nivel); ?>{<?php new_line();*/

						if(is_array($objRef)) {
							tab($nivel+1); ?>"<?php echo $classAux; ?>": [<?php new_line();
						
							getJSONObject($objRef,$nivel+2); new_line();

							tab($nivel+1); ?>]<?php
						} else if(is_object($objRef)) {						
							getJSONObject($objRef,$nivel+2); new_line();
						} else {
						}

						// FECHA OBJETO
						/*new_line(); tab($nivel); ?>}<?php*/

						$iCountFields++;
						
						$bProcessaCampo = false;
					} else {
						$bProcessaCampo = false;
					}
				} else {
					$bProcessaCampo = true;
				}
			} else {
				$bProcessaCampo = true;
			}

			// SE DEVE MOSTRAR O CAMPO
			if($bProcessaCampo) {
				$bShow = true;
				if(is_array($selAux) && !in_array($fieldAux, $selAux, true)) {
					$bShow = false;
				}

				if($bShow && (!is_array($notAux) || !in_array($fieldAux, $notAux, true))) {
					if($bArray) {
						$valorAux = stripslashes(get_output($objAux[$fieldAux]));
					} else {
						$valorAux = stripslashes(get_output($objAux->$fieldAux));
					}
					$valorAux = str_replace("\"","''",$valorAux);
					if(!is_array($textsAux) || !in_array($fieldAux, $textsAux, true)) {
						$valorAux = str_replace("\r","\\r",$valorAux);
						$valorAux = str_replace("\n","\\n",$valorAux);
					}

					if(trim($valorAux) == ""){
						$valorAux = "null";
					} else {
						$valorAux = json_encode($valorAux);
						if(!is_array($textsAux) || !in_array($fieldAux, $textsAux, true)) {
							$valorAux = str_replace("\\\\r\\\\n", "", $valorAux);
						}
					}

					// ESCREVER VALOR DA PROPRIEDADE
					if($iCountFields > 0){
						?>,<?php new_line();
					}

					echo tab($nivel+1); ?>"<?php echo $fieldAux; ?>": <?php echo $valorAux; ?><?php

					$iCountFields++;
				}
			}
		}
	} /*else {
		//if(intval($iCountObjs) == 0){
			//tab($nivel+1);?>"result": "Nenhum registro encontrado"<?php
		//}
	}*/
}

function getJSONFinal($array_obj,$generic = false,$jsonName = ""){
	global $classAux;

	$resultName = "";
	if(trim($jsonName) != "") {
		$resultName = $jsonName;
	}

	// INICIA JSON
	/*?>var JSONResult = { <?php echo "\r\n";*/
	?>{ <?php new_line();
	$iCountObjs = 0;

	tab(); ?>"result": [<?php new_line();
	tab(); ?>{<?php new_line(); 
	if($generic) {
		tab(2); ?>"<?php echo $resultName; ?>": [<?php new_line(); 
	} else {
		if(trim($jsonName) != "") {
			tab(2); ?>"<?php echo $resultName; ?>": [<?php new_line(); 
		} else {
			tab(2); ?>"<?php echo $classAux; ?>": [<?php new_line(); 
		}
	}

	// PERCORRE OBJETOS
	if(is_array($array_obj)) {
		foreach($array_obj as $objAux)
		{
			// ESCREVE OBJETO
			if($iCountObjs > 0){
				?>,<?php new_line();
			}

			if($generic) {
				getJSONObject($objAux,2); //new_line();
			} else {
				getJSONModel($objAux,2); //new_line();
			}

			$iCountObjs++;
		}
	} else {
		if($generic) {
			getJSONObject($array_obj,2); //new_line();
		} else {
			getJSONModel($array_obj,2); //new_line();
		}

		$iCountObjs++;
	}

	new_line();
	tab(2);
	echo "],";
	new_line();
	tab(2);
	echo "\"timestamp_serv\": \"" . Date("Y-m-d H:i:s") . "\"";
	new_line();
	tab();
	echo "}";
	new_line();
	tab();
	echo "]";
	new_line();
	echo "}";
}

function tab($vezes=1, $return = false){
	global $minify;
	
	$strRet = "";
	if(!$minify) {
		for($w=0;$w<$vezes;$w++){
			$strRet .= "	";
		}
	}
	if(!$return) {
		echo $strRet;
	} else {
		return $strRet;
	}
}

function new_line($vezes=1, $return = false){
	global $minify;

	$strRet = "";
	if(!$minify) {
		for($w=0;$w<$vezes;$w++){
			$strRet .= "\r\n";
		}
	}
	if(!$return) {
		echo $strRet;
	} else {
		return $strRet;
	}
}

function file_get_contents_url($url, $timeout=0) {
    $ch = curl_init();
    curl_setopt ($ch, CURLOPT_URL, $url);
    curl_setopt ($ch, CURLOPT_HEADER, 1);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    $conteudo = curl_exec($ch);
    curl_close($ch);
    //$arquivo = explode("\n", $conteudo);

    return $conteudo;
}

function distancia($lat1 = 0, $lon1 = 0, $lat2 = 0, $lon2 = 0, $unit = "M") {
	if(trim($lat1) == ""){$lat1 = 0;}
	if(trim($lon1) == ""){$lon1 = 0;}
	if(trim($lat2) == ""){$lat2 = 0;}
	if(trim($lon2) == ""){$lon2 = 0;}

	$theta = $lon1 - $lon2;

	$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	$dist = acos($dist);
	$dist = rad2deg($dist);
	$miles = $dist * 60 * 1.1515;
	$unit = strtoupper($unit);

	if (strtoupper($unit) == "K") {
		return ($miles * 1.609344);
	} else if (strtoupper($unit) == "N") {
		return ($miles * 0.8684);
	} else if (strtoupper($unit) == "M") {
		return ($miles * 1.609344)*1000;
	} else {
		return $miles;
	}
}

function comparaArrays ($a, $b) {
	global $globalCompKey;

	return strcmp($a->$globalCompKey,$b->$globalCompKey);
}

function criaSenha()
{
   $tamanho = 6;
   $senhagerada = "";
   $varMin = 'abcdefghijklmnopqrstuvwxyz';
   $varMai = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
   $varNum = '0123456789';
   $caracteres = $varMin . $varMai . $varNum;

  for($i = 0; $i < intval($tamanho); $i++)
  {
     $varchar[$i] = substr($caracteres,rand(0,strlen($caracteres)-1),1);
  }
  for($i = 0; $i < intval($tamanho); $i++)
  {
     $senhagerada = $senhagerada . $varchar[$i];
  }
  return $senhagerada;
}

function get_dia($dia) {
	$result = "";
	switch($dia) {
		default:
			break;
		case "1":
			$result = "SEG";
			break;
		case "2":
			$result = "TER";
			break;
		case "3":
			$result = "QUA";
			break;
		case "4":
			$result = "QUI";
			break;
		case "5":
			$result = "SEX";
			break;
		case "6":
			$result = "SÁB";
			break;
		case "7":
			$result = "DOM";
			break;
	}
	return output_decode($result);
}

?>