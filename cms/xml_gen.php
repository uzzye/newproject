<?php

include_once("config.php");

$token = $_REQUEST["token"];
$classAux = $_REQUEST["module"];
$gera_ids = $_REQUEST["gera_ids"];
$not = $_REQUEST["not"];
$refs = $_REQUEST["refs"];
$interno = $_REQUEST["interno"];

if(trim($gera_ids) == "") {
	$gera_ids = false;
}
if(trim($interno) == "") {
	$interno = false;
}
if(trim($not) != "") {
	$not = split(",",$not);
}
if(trim($refs) == "") {
	$refs = true;
} else {
	$refs = false;
}

$i=1;
while(trim($_REQUEST["field".$i]) <> "")
{
	if(strtoupper($_REQUEST["compare".$i]) == "LIKE"){
		$filtros[$i-1] = " " . urldecode($_REQUEST["field".$i]) . " LIKE \"%" . urldecode($_REQUEST["value".$i]) . "%\" ";
	} 
	else if(strtoupper($_REQUEST["compare".$i]) == "EQUAL"){
		$filtros[$i-1] = " " . urldecode($_REQUEST["field".$i]) . " = \"" . urldecode($_REQUEST["value".$i]) . "\" ";
	}
	else if(strtoupper($_REQUEST["compare".$i]) == "DIFF"){
		$filtros[$i-1] = " " . urldecode($_REQUEST["field".$i]) . " != \"" . urldecode($_REQUEST["value".$i]) . "\" ";
	}
	$i++;
}

if(trim($classAux) <> "" && $token == $_SESSION["GLOBAL_RANDOM_KEY"])
{
	carrega_classe($classAux);

	$classAux .= "_controller";
	$objAux = new $classAux();

	$xmlGen = $objAux->gera_xml($interno,true,$gera_ids,"",$filtros,$refs,$not);

	ob_clean();

	header("Content-Type: text/xml; charset=UTF-8");

	echo $xmlGen;
}

?>