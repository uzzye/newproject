DROP TABLE IF EXISTS newproject_newentity;
CREATE TABLE newproject_newentity(
       id int(11) NOT NULL AUTO_INCREMENT,
       data_criacao datetime DEFAULT NULL,
       data_atualizacao datetime DEFAULT NULL,
       usuario_criacao int(11) DEFAULT NULL,
       usuario_atualizacao int(11) DEFAULT NULL,
       ativo int(1) DEFAULT '1',
       views int(11) DEFAULT 0,
       permalink tinytext DEFAULT null,
       ranking int(11) DEFAULT 1,
       PRIMARY KEY (`id`),
       CONSTRAINT fk_newproject_newentity_usuario_criacao FOREIGN KEY (`usuario_criacao`) REFERENCES newproject_cms_usuarios(id),
       CONSTRAINT fk_newproject_newentity_usuario_atualizacao FOREIGN KEY (`usuario_atualizacao`) REFERENCES newproject_cms_usuarios(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;