<?php

include_once("config.php");

include_once("verificaLogado.php");

ob_clean();

$daysPermit = 7; // Quantidade de dias permitidos para unlink

// Limpa arquivos temporários

$itens = array();

$diretorio = UPLOAD_FOLDER . "temp";
if(substr($diretorio,-1) <> "/")
{$diretorio .= "/";}
	
$ponteiro  = opendir($diretorio); // ponteiro que ira percorrer a pasta
while ($nome_itens = readdir($ponteiro)) { // monta o vetor com os itens da pasta
    $itens[] = $nome_itens;
}
sort($itens); // ordena o vetor de itens
foreach ($itens as $listar) {  //percorre o vetor para fazer a separacao entre arquivos e pastas
	if ($listar!="." && $listar!=".."){ // retira os itens "./" e "../" para que retorne apenas pastas e arquivos
	    if (!is_dir($listar)) { // checa se é uma pasta
	    	//if(substr($listar,0,strlen($_REQUEST["modulo"])) == $_REQUEST["modulo"] && strstr($listar,"_".$_SESSION["GLOBAL_RANDOM_KEY"]."."))
	    	$ext = explode(".",$listar);
	    	$nome_aux = substr($listar,0,-(strlen($ext[sizeof($ext)-1])+1));
	    	
	    	// v2.0 only
	    	if(substr($nome_aux,-6,1) == intval($_SESSION["idLogin"]))
	    	{	    		
	    		@unlink($diretorio . $listar);
	    	}

	    	// v3.0
	    	$dataAux = date("Y-m-d H:i:s",@filectime($diretorio . $listar));
	    	$dataNow = date("Y-m-d H:i:s");
	    	$diff = abs(strtotime($dataNow) - strtotime($dataAux));

			$days = floor(($diff) / (60*60*24));

			if($days > $daysPermit) {	
	    		@unlink($diretorio . $listar);
	    	}
		}
	}
}

// Limpa imagens originais não usadas

$arrayFiles = array();

$sqlAux = "SELECT GROUP_CONCAT(DISTINCT arquivo SEPARATOR \",\") AS files FROM " . DBTABLE_CMS_IMAGENS . " ";
$resAux = $db->exec_query($sqlAux);
if($db->num_rows($resAux) > 0) {
	$arrayFiles = explode(",",$db->result_field($resAux,0,"files"));
}

$itens = array();

$diretorio = UPLOAD_FOLDER . "original";
if(substr($diretorio,-1) <> "/")
{$diretorio .= "/";}
	
$ponteiro  = opendir($diretorio);
while ($nome_itens = readdir($ponteiro)) {
	if ($nome_itens!="." && $nome_itens!=".."){
    	//$itens[] = $nome_itens;
    	if(!in_array($nome_itens, $arrayFiles)) {
	    	$dataAux = date("Y-m-d H:i:s",@filectime($diretorio . $nome_itens));
	    	$dataNow = date("Y-m-d H:i:s");
	    	$diff = abs(strtotime($dataNow) - strtotime($dataAux));

			$days = floor(($diff) / (60*60*24));

			if($days > $daysPermit) {	
	    		@unlink($diretorio . $nome_itens);
	    	}
    	}
    }
}

?>