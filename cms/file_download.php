<?php

include_once("config.php");

$modulo = $_REQUEST["modulo"];
$id = $_REQUEST["id"];
$campo = $_REQUEST["campo"];
$file = rawurldecode($_REQUEST["file"]);
$base64 = $_REQUEST["base64"];
$titulo = $_REQUEST["titulo"];
$tituloDef = $_REQUEST["titulo_def"];

$folder = UPLOAD_FOLDER;
if(trim($_REQUEST["folder"]) != "" && trim($_REQUEST["folder"]) != $folder) {
      $folder = rawurldecode($_REQUEST["folder"]);
}

$filename = $file;

$addExt = false;

$dot = '.';
$hyphen = '-';

$trade = array(
      'Ã¡'=>'a',
      'Ã '=>'a',
      'Ã£'=>'a',
      'Ã¤'=>'a',
      'Ã¢'=>'a',
      'Ã '=>'A',
      'Ã€'=>'A',
      'Ãƒ'=>'A',
      'Ã„'=>'A',
      'Ã‚'=>'A',
      'Ã(c)'=>'e',
      'Ã¨'=>'e',
      'Ã«'=>'e',
      'Ãª'=>'e',
      'Ã‰'=>'E',
      'Ãˆ'=>'E',
      'Ã‹'=>'E',
      'ÃŠ'=>'E',
      'Ã­'=>'i',
      'Ã¬'=>'i',
      'Ã¯'=>'i',
      'Ã(r)'=>'i',
      'Ã '=>'I',
      'ÃŒ'=>'I',
      'Ã '=>'I',
      'ÃŽ'=>'I',
      'Ã³'=>'o',
      'Ã²'=>'o',
      'Ãµ'=>'o',
      'Ã¶'=>'o',
      'Ã´'=>'o',
      'Ã"'=>'O',
      'Ã\''=>'O',
      'Ã•'=>'O',
      'Ã–'=>'O',
      'Ã"'=>'O',
      'Ãº'=>'u',
      'Ã¹'=>'u',
      'Ã¼'=>'u',
      'Ã»'=>'u',
      'Ãš'=>'U',
      'Ã™'=>'U',
      'Ãœ'=>'U',
      'Ã›'=>'U',
      '$'=>'',
      '@'=>'',
      '!'=>'',
      '#'=>'',
      '%'=>'',
      '^'=>'',
      '&'=>'e',
      '*'=>'',
      '('=>'',
      ')'=>'',
      '-'=>$hyphen,
      '+'=>'',
      '='=>'',
      ':'=>'',
      ';'=>'',
      '\\'=>'',
      '|'=>'',
      '`'=>'',
      '` '=>'',
      '´'=>'',
      '´ '=>'',
      '~'=>'',
      '/'=>'',
      '\"'=>'',
      '\''=>'',
      '<'=>'',
      '>'=>'',
      '?'=>'',
      ','=>'',
      ':'=>'',
      'Ã§'=>'c',
      'Ã‡'=>'C',
      '®'=>'C',
      '©'=>'C',
      '™'=>'C',
      '²' => '2',
      '³' => '3',
      '' => '',
      '°' => '',
      '¼' => '14',
      '½' => '12',
      '¾' => '34',
      '.'=>$dot
);

if(isset($_REQUEST["base64"])) {
      $addExt = true;

      carrega_classe($modulo);
      $classAux = $modulo . "_controller";
      $objAux = new $classAux();
      $objAux->set_var("id",$id);
      $objAux->carrega_dados();     

      $file_content = base64_decode($objAux->get_var($campo));

      $f = finfo_open();
      $mime_type = finfo_buffer($f, $file_content, FILEINFO_MIME_TYPE);
      $split = explode( '/', $mime_type );
      $type = $split[1]; 

      $filename = 'Arquivo_Mensagem_' . Date("YmdHis") . '.' . $type;

      header('Content-Description: '.$filename.'');
      if(trim($mime_type) != "") {
            header('Content-Type: ' . $mime_type);
      } else {
            header('Content-Type: application/octet-stream');
      }
      header('Content-Disposition: attachment; filename='.$filename.'');
      header('Content-Transfer-Encoding: binary');
      header('Expires: 0');
      header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
      header('Pragma: public');
      header('Content-Length: '.strlen($file_content).'');
      ob_clean();
      flush();
      echo $file_content;

      exit;
} else {
      $bProcessa = false;

      if(trim($file) == "" || trim($file) == "undefined")
      {	
            if(trim($modulo) != "") {
                  $addExt = true;

            	carrega_classe($modulo);
            	$classAux = $modulo . "_controller";
            	$objAux = new $classAux();
            	$objAux->set_var("id",$id);
            	$objAux->carrega_dados();	

                  $folder = ROOT_SITE . $objAux->get_upload_folder($campo); // . UPLOAD_FOLDER;

                  /*$folder = ROOT_SITE . UPLOAD_FOLDER;
                  if(@trim($objAux->model->upload_folders[$campo]) != "" && @trim($objAux->model->upload_folders[$campo]) != UPLOAD_FOLDER) {
                        $folder .= $objAux->model->upload_folders[$campo];
                  } else if(trim($objAux->model->upload_folder) != "" && trim($objAux->model->upload_folder) != UPLOAD_FOLDER) {
                        $folder .= $objAux->model->upload_folder;
                  }*/

            	$file = $objAux->get_var($campo);

            	if(trim($tituloDef) != ""){
            		$filename = rawurldecode($tituloDef);
            	} else {
            		if(property_exists($modulo . "_model", $titulo)){
            			if(trim($objAux->get_var($titulo)) != "") {
            				$filename = stripslashes(get_output($objAux->get_var($titulo)));
            			} else {
            				//$filename = $titulo;
            			}
            		} else {
            			//$filename = $titulo;
            		}
            	}
                  $urlToDown = ($folder.$file);

                  $bProcessa = true;
            } else {
            }
      } else {
            $arrAux = explode("/",$filename);
            $filename = $arrAux[sizeof($arrAux)-1];
            $urlToDown = ($folder.$file); //output_decode

            $bProcessa = true;
      }

      if($bProcessa) {
            //echo $urlToDown; die();
            //echo intval(file_exists($urlToDown)); die();

            $filesize = filesize($urlToDown);
            $filetype = filetype($urlToDown);
            $ext = explode(".",$file);
            $ext = $ext[sizeof($ext)-1];

            if($addExt) {
                  $filename .= "." . $ext;
            }

            /*ob_clean();

            header("Content-Type: $filetype");
            header("Content-Length: $filesize");
            header("Content-Disposition: attachment; filename=\"$filename\"; size=$filesize");
            header("Pragma: no-cache");
            header("Expires: 0");

            readfile($folder . $file);*/

            $filename = (strtr($filename,$trade));
            $ctype = get_headers(ROOT_SERVER . ROOT . str_replace(ROOT_SERVER . ROOT, "", $urlToDown), 1)["Content-Type"];

            header('Content-Description: '.$filename.'');
            if(trim($ctype) != "") {
                  header('Content-Type: ' . $ctype);
            } else {
                  header('Content-Type: application/octet-stream');
            }
            header('Content-Disposition: attachment; filename='.$filename.'');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: '.$filesize.'');
            ob_clean();
            flush();
            readfile($urlToDown);

            exit;
      }
}

?>