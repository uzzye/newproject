-- phpMyAdmin SQL Dump
-- version 4.0.0-beta3
-- http://www.phpmyadmin.net
--
-- Máquina: 127.0.0.1
-- Data de Criação: 26-Set-2017 às 15:32
-- Versão do servidor: 5.6.28-log
-- versão do PHP: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de Dados: `newproject`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `newproject_banners`
--

CREATE TABLE IF NOT EXISTS `newproject_banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_criacao` datetime DEFAULT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `usuario_criacao` int(11) DEFAULT NULL,
  `usuario_atualizacao` int(11) DEFAULT NULL,
  `ativo` int(1) DEFAULT '1',
  `id_referencia` int(11) DEFAULT NULL,
  `titulo_pt` varchar(255) DEFAULT NULL,
  `descricao_pt` text,
  `imagem_bg_pt` tinytext DEFAULT NULL,
  `imagem_bg_mobile_pt` tinytext DEFAULT NULL,
  `imagem_bg_en` tinytext DEFAULT NULL,
  `imagem_bg_mobile_en` tinytext DEFAULT NULL,
  `imagem_bg_es` tinytext DEFAULT NULL,
  `imagem_bg_mobile_es` tinytext DEFAULT NULL,
  `cinemagraph` tinytext,
  `video_url` text,
  `video_embed` text,
  `url` text,
  `target` set('','_blank') DEFAULT NULL,
  `texto_botao_pt` varchar(255) DEFAULT NULL,
  `ranking` int(11) DEFAULT NULL,
  `tempo_duracao` int(11) DEFAULT NULL,
  `tempo_transicao` int(11) DEFAULT NULL,
  `imagem_conteudo_pt` tinytext,
  `titulo_en` varchar(255) DEFAULT NULL,
  `descricao_en` text,
  `texto_botao_en` varchar(255) DEFAULT NULL,
  `imagem_conteudo_en` tinytext,
  `video_autoplay` int(1) DEFAULT NULL,
  `cinemagraph_mobile` tinytext,
  `cor_overlay` varchar(255) DEFAULT NULL,
  `titulo_es` varchar(255) DEFAULT NULL,
  `descricao_es` text,
  `texto_botao_es` varchar(255) DEFAULT NULL,
  `imagem_conteudo_es` tinytext,
  PRIMARY KEY (`id`),
  KEY `fk_newproject_banners_usuario_criacao` (`usuario_criacao`),
  KEY `fk_newproject_banners_usuario_atualizacao` (`usuario_atualizacao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `newproject_cms_grupos_usuarios`
--

CREATE TABLE IF NOT EXISTS `newproject_cms_grupos_usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(70) NOT NULL,
  `data_criacao` datetime NOT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `usuario_criacao` int(11) NOT NULL,
  `usuario_atualizacao` int(11) DEFAULT NULL,
  `ativo` int(1) DEFAULT '1',
  `views` int(11) DEFAULT '0',
  `permalink` tinytext,
  `ranking` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_newproject_grupos_usuarios_usuario_criacao` (`usuario_criacao`),
  KEY `fk_newproject_grupos_usuarios_atualizacao` (`usuario_atualizacao`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Extraindo dados da tabela `newproject_cms_grupos_usuarios`
--

INSERT INTO `newproject_cms_grupos_usuarios` (`id`, `titulo`, `data_criacao`, `data_atualizacao`, `usuario_criacao`, `usuario_atualizacao`, `ativo`, `views`, `permalink`, `ranking`) VALUES
(29, 'Uzzye', '2011-12-07 16:25:24', '2017-08-17 11:54:40', 1, 1, 1, 0, NULL, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `newproject_cms_imagens`
--

CREATE TABLE IF NOT EXISTS `newproject_cms_imagens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_criacao` datetime DEFAULT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `usuario_criacao` int(11) DEFAULT NULL,
  `usuario_atualizacao` int(11) DEFAULT NULL,
  `ativo` int(1) DEFAULT '1',
  `views` int(11) DEFAULT '0',
  `permalink` tinytext,
  `ranking` int(11) DEFAULT '1',
  `titulo` varchar(255) DEFAULT NULL,
  `arquivo` tinytext,
  `modulo_registro` varchar(255) DEFAULT NULL,
  `id_registro` int(11) DEFAULT NULL,
  `campo_registro` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_newproject_cms_imagens_usuario_criacao` (`usuario_criacao`),
  KEY `fk_newproject_cms_imagens_usuario_atualizacao` (`usuario_atualizacao`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=98 ;

--
-- Extraindo dados da tabela `newproject_cms_imagens`
--

INSERT INTO `newproject_cms_imagens` (`id`, `data_criacao`, `data_atualizacao`, `usuario_criacao`, `usuario_atualizacao`, `ativo`, `views`, `permalink`, `ranking`, `titulo`, `arquivo`, `modulo_registro`, `id_registro`, `campo_registro`) VALUES
(96, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 'Barlambras_dFibhb', 'Barlambras_dFibhb_original.jpg', 'teste', 13, 'imagem'),
(97, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 'Video', 'Video_original.jpg', 'banners', 0, 'imagem_bg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `newproject_cms_log_entidades`
--

CREATE TABLE IF NOT EXISTS `newproject_cms_log_entidades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `registro_serializado` longtext NOT NULL,
  `data_criacao` datetime NOT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `usuario_criacao` int(11) NOT NULL,
  `usuario_atualizacao` int(11) DEFAULT NULL,
  `ativo` int(1) DEFAULT '1',
  `nome_entidade` tinytext,
  `id_registro` int(11) DEFAULT NULL,
  `acao` varchar(255) DEFAULT NULL,
  `views` int(11) DEFAULT '0',
  `permalink` tinytext,
  `ranking` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_newproject_logentidades_usuario_criacao` (`usuario_criacao`),
  KEY `fk_newproject_logentidades_usuario_atualizacao` (`usuario_atualizacao`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Extraindo dados da tabela `newproject_cms_log_entidades`
--

INSERT INTO `newproject_cms_log_entidades` (`id`, `registro_serializado`, `data_criacao`, `data_atualizacao`, `usuario_criacao`, `usuario_atualizacao`, `ativo`, `nome_entidade`, `id_registro`, `acao`, `views`, `permalink`, `ranking`) VALUES
(1, 'O:13:"idiomas_model":20:{s:13:"identificador";s:5:"pt-br";s:6:"titulo";s:18:"Português (Brasil)";s:2:"id";N;s:12:"data_criacao";N;s:16:"data_atualizacao";N;s:15:"usuario_criacao";N;s:19:"usuario_atualizacao";N;s:5:"ativo";N;s:5:"owner";N;s:10:"controller";N;s:15:"SQL_JOIN_FIELDS";N;s:15:"SQL_JOIN_TABLES";N;s:11:"nome_tabela";s:19:"newproject_idiomas";s:12:"primary_keys";a:1:{i:0;s:2:"id";}s:12:"foreign_keys";a:2:{i:0;a:3:{i:0;s:15:"usuario_criacao";i:1;s:8:"usuarios";i:2;s:2:"id";}i:1;a:3:{i:0;s:19:"usuario_atualizacao";i:1;s:8:"usuarios";i:2;s:2:"id";}}s:15:"reference_items";a:0:{}s:17:"array_file_fields";a:0:{}s:17:"array_crop_fields";a:0:{}s:22:"array_exibition_fields";a:0:{}s:21:"array_required_fields";a:2:{i:0;s:13:"identificador";i:1;s:6:"titulo";}}', '2012-04-18 16:06:15', NULL, 1, NULL, 1, 'idiomas', 1, 'i', 0, NULL, 1),
(2, 'O:19:"configuracoes_model":30:{s:9:"namespace";s:11:"newproject";s:13:"idioma_padrao";i:1;s:20:"email_padrao_contato";s:18:"mauricio@uzzye.com";s:18:"email_default_from";s:18:"mauricio@uzzye.com";s:13:"titulo_padrao";s:37:"Uzzye";s:16:"descricao_padrao";s:271:"Somos uma agência produtora que atua no universo digital, inovando e destacando marcas, produtos e serviços. Nosso dever é conhecer o potencial dos nossos clientes e fornecer estratégias que gerem resultados. Nascemos ligados no que é digital e somos loucos por inovação.";s:21:"palavras_chave_padrao";s:254:"uzzye, agencia, presenca, inovacao, tecnologia, digital, produtora, marca, midia, midias sociais, social, mobile, aplicativo, app, facebook, twitter, site, blog, portal, web, mausalamon, salamon, mauricio, caxias do sul, caxias, serra, gaucha, rs, brasil";s:13:"url_amigaveis";i:1;s:14:"charset_padrao";s:5:"UTF-8";s:12:"qtd_itens_pp";s:2:"20";s:13:"qtd_group_pag";s:1:"3";s:9:"id_idioma";s:1:"1";s:2:"id";N;s:12:"data_criacao";N;s:16:"data_atualizacao";N;s:15:"usuario_criacao";N;s:19:"usuario_atualizacao";N;s:5:"ativo";N;s:5:"owner";N;s:10:"controller";N;s:15:"SQL_JOIN_FIELDS";N;s:15:"SQL_JOIN_TABLES";N;s:11:"nome_tabela";s:25:"newproject_configuracoes";s:12:"primary_keys";a:1:{i:0;s:2:"id";}s:12:"foreign_keys";a:3:{i:0;a:3:{i:0;s:15:"usuario_criacao";i:1;s:8:"usuarios";i:2;s:2:"id";}i:1;a:3:{i:0;s:19:"usuario_atualizacao";i:1;s:8:"usuarios";i:2;s:2:"id";}i:2;a:3:{i:0;s:9:"id_idioma";i:1;s:7:"idiomas";i:2;s:2:"id";}}s:15:"reference_items";a:0:{}s:17:"array_file_fields";a:0:{}s:17:"array_crop_fields";a:0:{}s:22:"array_exibition_fields";a:0:{}s:21:"array_required_fields";a:12:{i:0;s:9:"namespace";i:1;s:13:"idioma_padrao";i:2;s:20:"email_padrao_contato";i:3;s:18:"email_default_from";i:4;s:13:"titulo_padrao";i:5;s:16:"descricao_padrao";i:6;s:21:"palavras_chave_padrao";i:7;s:13:"url_amigaveis";i:8;s:14:"charset_padrao";i:9;s:12:"qtd_itens_pp";i:10;s:13:"qtd_group_pag";i:11;s:9:"id_idioma";}}', '2012-04-18 17:14:42', NULL, 1, NULL, 1, 'configuracoes', 1, 'i', 0, NULL, 1),
(3, 'O:19:"configuracoes_model":30:{s:9:"namespace";s:11:"newproject";s:13:"idioma_padrao";i:0;s:20:"email_padrao_contato";s:18:"mauricio@uzzye.com";s:18:"email_default_from";s:18:"mauricio@uzzye.com";s:13:"titulo_padrao";s:37:"Uzzye";s:16:"descricao_padrao";s:255:"Somos uma agência produtora que atua no universo digital, inovando e destacando marcas, produtos e serviços. Nosso dever é conhecer o potencial dos nossos clientes e fornecer estratégias que gerem resultados. Nascemos ligados no que é digital e somos louc";s:21:"palavras_chave_padrao";s:254:"uzzye, agencia, presenca, inovacao, tecnologia, digital, produtora, marca, midia, midias sociais, social, mobile, aplicativo, app, facebook, twitter, site, blog, portal, web, mausalamon, salamon, mauricio, caxias do sul, caxias, serra, gaucha, rs, brasil";s:13:"url_amigaveis";i:1;s:14:"charset_padrao";s:5:"UTF-8";s:12:"qtd_itens_pp";s:2:"20";s:13:"qtd_group_pag";s:1:"3";s:9:"id_idioma";s:1:"1";s:2:"id";s:1:"1";s:12:"data_criacao";s:19:"2012-04-18 17:14:42";s:16:"data_atualizacao";N;s:15:"usuario_criacao";s:1:"1";s:19:"usuario_atualizacao";N;s:5:"ativo";s:1:"1";s:5:"owner";N;s:10:"controller";N;s:15:"SQL_JOIN_FIELDS";N;s:15:"SQL_JOIN_TABLES";N;s:11:"nome_tabela";s:25:"newproject_configuracoes";s:12:"primary_keys";a:1:{i:0;s:2:"id";}s:12:"foreign_keys";a:3:{i:0;a:3:{i:0;s:15:"usuario_criacao";i:1;s:8:"usuarios";i:2;s:2:"id";}i:1;a:3:{i:0;s:19:"usuario_atualizacao";i:1;s:8:"usuarios";i:2;s:2:"id";}i:2;a:3:{i:0;s:9:"id_idioma";i:1;s:7:"idiomas";i:2;s:2:"id";}}s:15:"reference_items";a:0:{}s:17:"array_file_fields";a:0:{}s:17:"array_crop_fields";a:0:{}s:22:"array_exibition_fields";a:0:{}s:21:"array_required_fields";a:12:{i:0;s:9:"namespace";i:1;s:13:"idioma_padrao";i:2;s:20:"email_padrao_contato";i:3;s:18:"email_default_from";i:4;s:13:"titulo_padrao";i:5;s:16:"descricao_padrao";i:6;s:21:"palavras_chave_padrao";i:7;s:13:"url_amigaveis";i:8;s:14:"charset_padrao";i:9;s:12:"qtd_itens_pp";i:10;s:13:"qtd_group_pag";i:11;s:9:"id_idioma";}}', '2012-04-18 17:14:50', NULL, 1, NULL, 1, 'configuracoes', 1, 'a', 0, NULL, 1),
(4, 'O:19:"configuracoes_model":30:{s:9:"namespace";s:11:"newproject";s:13:"idioma_padrao";i:1;s:20:"email_padrao_contato";s:18:"mauricio@uzzye.com";s:18:"email_default_from";s:18:"mauricio@uzzye.com";s:13:"titulo_padrao";s:37:"Uzzye";s:16:"descricao_padrao";s:255:"Somos uma agência produtora que atua no universo digital, inovando e destacando marcas, produtos e serviços. Nosso dever é conhecer o potencial dos nossos clientes e fornecer estratégias que gerem resultados. Nascemos ligados no que é digital e somos louc";s:21:"palavras_chave_padrao";s:254:"uzzye, agencia, presenca, inovacao, tecnologia, digital, produtora, marca, midia, midias sociais, social, mobile, aplicativo, app, facebook, twitter, site, blog, portal, web, mausalamon, salamon, mauricio, caxias do sul, caxias, serra, gaucha, rs, brasil";s:13:"url_amigaveis";i:1;s:14:"charset_padrao";s:5:"UTF-8";s:12:"qtd_itens_pp";s:2:"20";s:13:"qtd_group_pag";s:1:"3";s:9:"id_idioma";s:1:"1";s:2:"id";s:1:"1";s:12:"data_criacao";s:19:"2012-04-18 17:14:42";s:16:"data_atualizacao";s:19:"2012-04-18 17:14:50";s:15:"usuario_criacao";s:1:"1";s:19:"usuario_atualizacao";s:1:"1";s:5:"ativo";s:1:"1";s:5:"owner";N;s:10:"controller";N;s:15:"SQL_JOIN_FIELDS";N;s:15:"SQL_JOIN_TABLES";N;s:11:"nome_tabela";s:25:"newproject_configuracoes";s:12:"primary_keys";a:1:{i:0;s:2:"id";}s:12:"foreign_keys";a:3:{i:0;a:3:{i:0;s:15:"usuario_criacao";i:1;s:8:"usuarios";i:2;s:2:"id";}i:1;a:3:{i:0;s:19:"usuario_atualizacao";i:1;s:8:"usuarios";i:2;s:2:"id";}i:2;a:3:{i:0;s:9:"id_idioma";i:1;s:7:"idiomas";i:2;s:2:"id";}}s:15:"reference_items";a:0:{}s:17:"array_file_fields";a:0:{}s:17:"array_crop_fields";a:0:{}s:22:"array_exibition_fields";a:0:{}s:21:"array_required_fields";a:12:{i:0;s:9:"namespace";i:1;s:13:"idioma_padrao";i:2;s:20:"email_padrao_contato";i:3;s:18:"email_default_from";i:4;s:13:"titulo_padrao";i:5;s:16:"descricao_padrao";i:6;s:21:"palavras_chave_padrao";i:7;s:13:"url_amigaveis";i:8;s:14:"charset_padrao";i:9;s:12:"qtd_itens_pp";i:10;s:13:"qtd_group_pag";i:11;s:9:"id_idioma";}}', '2012-04-18 17:14:55', NULL, 1, NULL, 1, 'configuracoes', 1, 'a', 0, NULL, 1),
(5, 'O:19:"configuracoes_model":30:{s:9:"namespace";s:11:"newproject";s:13:"idioma_padrao";i:0;s:20:"email_padrao_contato";s:18:"mauricio@uzzye.com";s:18:"email_default_from";s:18:"mauricio@uzzye.com";s:13:"titulo_padrao";s:37:"Uzzye";s:16:"descricao_padrao";s:255:"Somos uma agência produtora que atua no universo digital, inovando e destacando marcas, produtos e serviços. Nosso dever é conhecer o potencial dos nossos clientes e fornecer estratégias que gerem resultados. Nascemos ligados no que é digital e somos louc";s:21:"palavras_chave_padrao";s:254:"uzzye, agencia, presenca, inovacao, tecnologia, digital, produtora, marca, midia, midias sociais, social, mobile, aplicativo, app, facebook, twitter, site, blog, portal, web, mausalamon, salamon, mauricio, caxias do sul, caxias, serra, gaucha, rs, brasil";s:13:"url_amigaveis";i:1;s:14:"charset_padrao";s:5:"UTF-8";s:12:"qtd_itens_pp";s:2:"20";s:13:"qtd_group_pag";s:1:"3";s:9:"id_idioma";s:1:"1";s:2:"id";s:1:"1";s:12:"data_criacao";s:19:"2012-04-18 17:14:42";s:16:"data_atualizacao";s:19:"2012-04-18 17:14:55";s:15:"usuario_criacao";s:1:"1";s:19:"usuario_atualizacao";s:1:"1";s:5:"ativo";s:1:"1";s:5:"owner";N;s:10:"controller";N;s:15:"SQL_JOIN_FIELDS";N;s:15:"SQL_JOIN_TABLES";N;s:11:"nome_tabela";s:25:"newproject_configuracoes";s:12:"primary_keys";a:1:{i:0;s:2:"id";}s:12:"foreign_keys";a:3:{i:0;a:3:{i:0;s:15:"usuario_criacao";i:1;s:8:"usuarios";i:2;s:2:"id";}i:1;a:3:{i:0;s:19:"usuario_atualizacao";i:1;s:8:"usuarios";i:2;s:2:"id";}i:2;a:3:{i:0;s:9:"id_idioma";i:1;s:7:"idiomas";i:2;s:2:"id";}}s:15:"reference_items";a:0:{}s:17:"array_file_fields";a:0:{}s:17:"array_crop_fields";a:0:{}s:22:"array_exibition_fields";a:0:{}s:21:"array_required_fields";a:12:{i:0;s:9:"namespace";i:1;s:13:"idioma_padrao";i:2;s:20:"email_padrao_contato";i:3;s:18:"email_default_from";i:4;s:13:"titulo_padrao";i:5;s:16:"descricao_padrao";i:6;s:21:"palavras_chave_padrao";i:7;s:13:"url_amigaveis";i:8;s:14:"charset_padrao";i:9;s:12:"qtd_itens_pp";i:10;s:13:"qtd_group_pag";i:11;s:9:"id_idioma";}}', '2012-04-18 17:17:17', NULL, 1, NULL, 1, 'configuracoes', 1, 'a', 0, NULL, 1),
(6, 'O:19:"configuracoes_model":30:{s:9:"namespace";s:11:"newproject";s:13:"idioma_padrao";i:1;s:20:"email_padrao_contato";s:18:"mauricio@uzzye.com";s:18:"email_default_from";s:18:"mauricio@uzzye.com";s:13:"titulo_padrao";s:37:"Uzzye";s:16:"descricao_padrao";s:255:"Somos uma agência produtora que atua no universo digital, inovando e destacando marcas, produtos e serviços. Nosso dever é conhecer o potencial dos nossos clientes e fornecer estratégias que gerem resultados. Nascemos ligados no que é digital e somos louc";s:21:"palavras_chave_padrao";s:254:"uzzye, agencia, presenca, inovacao, tecnologia, digital, produtora, marca, midia, midias sociais, social, mobile, aplicativo, app, facebook, twitter, site, blog, portal, web, mausalamon, salamon, mauricio, caxias do sul, caxias, serra, gaucha, rs, brasil";s:13:"url_amigaveis";i:1;s:14:"charset_padrao";s:5:"UTF-8";s:12:"qtd_itens_pp";s:2:"20";s:13:"qtd_group_pag";s:1:"3";s:9:"id_idioma";s:1:"1";s:2:"id";s:1:"1";s:12:"data_criacao";s:19:"2012-04-18 17:14:42";s:16:"data_atualizacao";s:19:"2012-04-18 17:17:17";s:15:"usuario_criacao";s:1:"1";s:19:"usuario_atualizacao";s:1:"1";s:5:"ativo";s:1:"1";s:5:"owner";N;s:10:"controller";N;s:15:"SQL_JOIN_FIELDS";N;s:15:"SQL_JOIN_TABLES";N;s:11:"nome_tabela";s:25:"newproject_configuracoes";s:12:"primary_keys";a:1:{i:0;s:2:"id";}s:12:"foreign_keys";a:3:{i:0;a:3:{i:0;s:15:"usuario_criacao";i:1;s:8:"usuarios";i:2;s:2:"id";}i:1;a:3:{i:0;s:19:"usuario_atualizacao";i:1;s:8:"usuarios";i:2;s:2:"id";}i:2;a:3:{i:0;s:9:"id_idioma";i:1;s:7:"idiomas";i:2;s:2:"id";}}s:15:"reference_items";a:0:{}s:17:"array_file_fields";a:0:{}s:17:"array_crop_fields";a:0:{}s:22:"array_exibition_fields";a:0:{}s:21:"array_required_fields";a:12:{i:0;s:9:"namespace";i:1;s:13:"idioma_padrao";i:2;s:20:"email_padrao_contato";i:3;s:18:"email_default_from";i:4;s:13:"titulo_padrao";i:5;s:16:"descricao_padrao";i:6;s:21:"palavras_chave_padrao";i:7;s:13:"url_amigaveis";i:8;s:14:"charset_padrao";i:9;s:12:"qtd_itens_pp";i:10;s:13:"qtd_group_pag";i:11;s:9:"id_idioma";}}', '2012-04-18 17:17:21', NULL, 1, NULL, 1, 'configuracoes', 1, 'a', 0, NULL, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `newproject_cms_modulos`
--

CREATE TABLE IF NOT EXISTS `newproject_cms_modulos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) DEFAULT NULL,
  `link` varchar(200) DEFAULT NULL,
  `modulo_pai` int(11) DEFAULT NULL,
  `data_criacao` datetime DEFAULT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `usuario_criacao` int(11) DEFAULT NULL,
  `usuario_atualizacao` int(11) DEFAULT NULL,
  `ativo` int(1) DEFAULT '1',
  `views` int(11) DEFAULT '0',
  `permalink` tinytext,
  `ranking` int(11) DEFAULT '1',
  `icon_class` varchar(255) DEFAULT NULL,
  `cor` varchar(255) DEFAULT NULL,
  `nome_en`	varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_newproject_modulos_usuario_criacao` (`usuario_criacao`),
  KEY `fk_newproject_modulos_usuario_atualizacao` (`usuario_atualizacao`),
  KEY `fk_newproject_modulos_modulo_pai` (`modulo_pai`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=211 ;

--
-- Extraindo dados da tabela `newproject_cms_modulos`
--


INSERT INTO `newproject_cms_modulos` (`id`, `nome`, `nome_en`, `link`, `modulo_pai`, `data_criacao`, `data_atualizacao`, `usuario_criacao`, `usuario_atualizacao`, `ativo`, `views`, `permalink`, `ranking`, `icon_class`, `cor`) VALUES
(1, 'Administração', 'Administração', '', NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 10, 'icon-lock', NULL),
(2, 'Usuários', 'Users', 'cms_usuarios', 1, NULL, NULL, NULL, NULL, 1, 0, NULL, 10, 'icon-user', NULL),
(3, 'Módulos', 'Modules', 'cms_modulos', 1, NULL, '2010-06-11 09:12:55', NULL, NULL, 1, 0, NULL, 10, 'icon-folder', NULL),
(162, 'Site', 'Site', '', NULL, '2010-07-26 10:48:23', NULL, NULL, NULL, 1, 0, NULL, 10, 'icon-globe', NULL),
(141, 'Conteúdos', 'Contents', 'conteudos', 162, '2010-06-30 14:56:46', '2010-07-26 10:48:28', NULL, NULL, 1, 0, NULL, 10, 'icon-docs', NULL),
(171, 'Contatos', 'Contacts', 'contatos', 162, NULL, NULL, NULL, NULL, 1, 0, NULL, 10, 'icon-envelope', NULL),
(172, 'Grupos de Usuários', 'Users Groups', 'cms_grupos_usuarios', 1, '2011-12-07 16:13:19', NULL, 1, NULL, 1, 0, NULL, 10, 'icon-people', NULL),
(173, 'Idiomas', 'Languages', 'idiomas', 1, NULL, '2017-07-25 09:53:34', NULL, 1, 1, 0, NULL, 8, 'icon-speech', ''),
(174, 'Configurações', 'Configurations', 'configuracoes', 1, NULL, '2017-07-25 09:53:47', NULL, 1, 1, 0, NULL, 9, 'icon-settings', ''),
(175, 'Links Úteis', 'Quick Links', 'links_uteis', 162, NULL, NULL, NULL, NULL, 1, 0, NULL, 10, 'icon-share', NULL),
(182, 'Teste', 'Test', 'teste', 162, '2017-07-11 10:30:32', NULL, 1, NULL, 0, 0, NULL, 10, NULL, NULL),
(186, 'Itens de Teste', 'Test Items', 'itens_teste', 162, '2017-07-11 10:41:04', NULL, 1, NULL, 0, 0, NULL, 10, NULL, NULL),
(187, 'Banco de Imagens', 'Images Database', 'cms_imagens', 1, '2017-07-18 17:29:29', '2017-07-18 17:43:37', 1, 1, 0, 0, NULL, 7, 'icon-picture', ''),
(188, 'Variáveis de Idiomas', 'Language Variables', 'language', 162, '2017-07-20 14:22:09', NULL, 1, NULL, 0, 0, NULL, 7, 'fa fa-language', ''),
(195, 'Log de Views', 'Views Log', 'log_views', 162, NULL, '2017-07-25 17:48:04', NULL, 1, 0, 0, NULL, 6, 'icon-notebook', ''),
(197, 'Downloads', 'Downloads', 'downloads', 162, '2017-08-17 11:53:21', NULL, 1, NULL, 1, 0, NULL, 6, 'icon-cloud-download', ''),
(198, 'Banners', 'Banners', 'banners', 162, '2017-08-17 11:54:03', NULL, 1, NULL, 1, 0, NULL, 6, 'fa fa-picture-o', ''),
(199, 'Cadastros', 'Registrations', 'cadastros', 162, '2019-02-08 16:23:57', NULL, 1, NULL, 0, 0, 'cms_modulos/cadastros/199', 6, 'fa fa-user-plus', ''),
(200, 'Imagens', 'Images', 'imagens', 162, '2019-02-08 16:24:15', NULL, 1, NULL, 0, 0, 'cms_modulos/imagens/200', 6, 'fa fa-image', ''),
(201, 'Estados', 'States', 'estados', 162, NULL, NULL, NULL, NULL, 0, 0, NULL, 6, 'fa fa-map', ''),
(202, 'Representantes', 'Representatives', 'representantes', 162, NULL, '2020-02-07 14:00:14', NULL, 1, 0, 0, 'cms_modulos/representantes/202', 4, 'fa fa-truck', ''),
(204, 'Diferenciais', 'Differentials', 'diferenciais', 162, NULL, '2020-02-07 14:00:08', NULL, 1, 0, 0, 'cms_modulos/diferenciais/204', 3, 'fa fa-medal', ''),
(205, 'Categorias de Produtos', 'Products Categories', 'produtos_categorias', 162, NULL, '2020-02-07 14:04:23', NULL, 1, 0, 0, 'cms_modulos/categorias-de-produtos/205', 5, 'fa fa-object-group', ''),
(206, 'Produtos', 'Products', 'produtos', 162, NULL, '2019-02-08 17:08:04', NULL, 1, 0, 0, 'cms_modulos/produtos/206', 1, 'fa fa-box-open', ''),
(207, 'Imagens de Produtos', 'Products Images', 'produtos_imagens', 162, NULL, '2020-02-07 14:00:01', NULL, 1, 0, 0, 'cms_modulos/imagens-de-produtos/207', 2, 'fa fa-picture-o', ''),
(208, 'Lojas Virtuais', 'Online Stores', 'lojas_virtuais', 162, NULL, NULL, NULL, NULL, 0, 0, NULL, 1, 'fa fa-shopping-cart', ''),
(209, 'Notícias', 'News', 'noticias', 162, NULL, NULL, NULL, NULL, 0, 0, NULL, 1, 'fa fa-newspaper', ''),
(210, 'Destaques', 'Highlighs', 'destaques', 162, NULL, NULL, NULL, NULL, 0, 0, NULL, 1, 'fa fa-highlighter', ''),
(211, 'Categorias de Arquivos', 'Files Categories', 'arquivos_categorias', 162, NULL, NULL, NULL, NULL, 0, 0, NULL, 1, 'fas fa-folder', ''),
(212, 'Arquivos', 'Files', 'arquivos', 162, NULL, NULL, NULL, NULL, 0, 0, NULL, 1, 'fas fa-file', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `newproject_cms_modulos_grupos_usuarios`
--

CREATE TABLE IF NOT EXISTS `newproject_cms_modulos_grupos_usuarios` (
  `id_modulo` int(11) DEFAULT NULL,
  `id_grupo` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_criacao` datetime DEFAULT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `usuario_criacao` int(11) DEFAULT NULL,
  `usuario_atualizacao` int(11) DEFAULT NULL,
  `ativo` int(1) DEFAULT '1',
  `acoes` text,
  `views` int(11) DEFAULT '0',
  `permalink` tinytext,
  `ranking` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_newproject_modulos_grupos_usuarios_usuario_criacao` (`usuario_criacao`),
  KEY `fk_newproject_modulos_grupos_usuarios_usuario_atualizacao` (`usuario_atualizacao`),
  KEY `fk_newproject_modulos_grupos_usuarios_id_modulo` (`id_modulo`),
  KEY `fk_newproject_modulos_grupos_usuarios_id_grupo` (`id_grupo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1100 ;

--
-- Extraindo dados da tabela `newproject_cms_modulos_grupos_usuarios`
--

INSERT INTO `newproject_cms_modulos_grupos_usuarios` (`id_modulo`, `id_grupo`, `id`, `data_criacao`, `data_atualizacao`, `usuario_criacao`, `usuario_atualizacao`, `ativo`, `acoes`, `views`, `permalink`, `ranking`) VALUES
(187, 29, 1073, '2017-08-17 11:54:40', NULL, 1, NULL, 1, 'Todas', 0, NULL, 1),
(198, 29, 1074, '2017-08-17 11:54:40', NULL, 1, NULL, 1, 'Todas', 0, NULL, 1),
(174, 29, 1075, '2017-08-17 11:54:40', NULL, 1, NULL, 1, 'Todas', 0, NULL, 1),
(171, 29, 1076, '2017-08-17 11:54:40', NULL, 1, NULL, 1, 'Todas', 0, NULL, 1),
(141, 29, 1077, '2017-08-17 11:54:40', NULL, 1, NULL, 1, 'Todas', 0, NULL, 1),
(197, 29, 1078, '2017-08-17 11:54:40', NULL, 1, NULL, 1, 'Todas', 0, NULL, 1),
(172, 29, 1079, '2017-08-17 11:54:40', NULL, 1, NULL, 1, 'Todas', 0, NULL, 1),
(173, 29, 1080, '2017-08-17 11:54:40', NULL, 1, NULL, 1, 'Todas', 0, NULL, 1),
(186, 29, 1081, '2017-08-17 11:54:40', NULL, 1, NULL, 1, 'Todas', 0, NULL, 1),
(175, 29, 1082, '2017-08-17 11:54:40', NULL, 1, NULL, 1, 'Todas', 0, NULL, 1),
(195, 29, 1083, '2017-08-17 11:54:40', NULL, 1, NULL, 1, 'Todas', 0, NULL, 1),
(3, 29, 1084, '2017-08-17 11:54:40', NULL, 1, NULL, 1, 'Todas', 0, NULL, 1),
(182, 29, 1085, '2017-08-17 11:54:40', NULL, 1, NULL, 1, 'Todas', 0, NULL, 1),
(2, 29, 1086, '2017-08-17 11:54:40', NULL, 1, NULL, 1, 'Todas', 0, NULL, 1),
(188, 29, 1087, '2017-08-17 11:54:40', NULL, 1, NULL, 1, 'Todas', 0, NULL, 1),
(199, 29, 1088, '2017-08-17 11:54:40', NULL, 1, NULL, 1, 'Todas', 0, NULL, 1),
(200, 29, 1089, '2017-08-17 11:54:40', NULL, 1, NULL, 1, 'Todas', 0, NULL, 1),
(201, 29, 1090, '2017-08-17 11:54:40', NULL, 1, NULL, 1, 'Todas', 0, NULL, 1),
(202, 29, 1091, '2017-08-17 11:54:40', NULL, 1, NULL, 1, 'Todas', 0, NULL, 1),
(204, 29, 1093, '2017-08-17 11:54:40', NULL, 1, NULL, 1, 'Todas', 0, NULL, 1),
(205, 29, 1094, '2017-08-17 11:54:40', NULL, 1, NULL, 1, 'Todas', 0, NULL, 1),
(206, 29, 1095, '2017-08-17 11:54:40', NULL, 1, NULL, 1, 'Todas', 0, NULL, 1),
(207, 29, 1096, '2017-08-17 11:54:40', NULL, 1, NULL, 1, 'Todas', 0, NULL, 1),
(208, 29, 1097, '2017-08-17 11:54:40', NULL, 1, NULL, 1, 'Todas', 0, NULL, 1),
(209, 29, 1098, '2017-08-17 11:54:40', NULL, 1, NULL, 1, 'Todas', 0, NULL, 1),
(210, 29, 1099, '2017-08-17 11:54:40', NULL, 1, NULL, 1, 'Todas', 0, NULL, 1),
(211, 29, 1100, '2017-08-17 11:54:40', NULL, 1, NULL, 1, 'Todas', 0, NULL, 1),
(212, 29, 1101, '2017-08-17 11:54:40', NULL, 1, NULL, 1, 'Todas', 0, NULL, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `newproject_cms_modulos_usuarios`
--

CREATE TABLE IF NOT EXISTS `newproject_cms_modulos_usuarios` (
  `id_modulo` int(11) DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_criacao` datetime DEFAULT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `usuario_criacao` int(11) DEFAULT NULL,
  `usuario_atualizacao` int(11) DEFAULT NULL,
  `ativo` int(1) DEFAULT '1',
  `acoes` text,
  `views` int(11) DEFAULT '0',
  `permalink` tinytext,
  `ranking` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_newproject_modulos_usuarios_usuario_criacao` (`usuario_criacao`),
  KEY `fk_newproject_modulos_usuarios_usuario_atualizacao` (`usuario_atualizacao`),
  KEY `fk_newproject_modulos_usuarios_id_modulo` (`id_modulo`),
  KEY `fk_newproject_modulos_usuarios_id_usuario` (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `newproject_cms_usuarios`
--

CREATE TABLE IF NOT EXISTS `newproject_cms_usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) DEFAULT NULL,
  `setor` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `senha` varchar(100) DEFAULT NULL,
  `disponivel` int(1) DEFAULT NULL,
  `data_criacao` datetime DEFAULT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `data_ultimoacesso` datetime DEFAULT NULL,
  `usuario_criacao` int(11) DEFAULT NULL,
  `usuario_atualizacao` int(11) DEFAULT NULL,
  `ativo` int(1) DEFAULT '1',
  `id_grupo` int(11) DEFAULT NULL,
  `avatar` tinytext,
  `views` int(11) DEFAULT '0',
  `permalink` tinytext,
  `ranking` int(11) DEFAULT '1',
  `id_idioma` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_newproject_usuarios_usuario_criacao` (`usuario_criacao`),
  KEY `fk_newproject_usuarios_usuario_atualizacao` (`usuario_atualizacao`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=100 ;

--
-- Extraindo dados da tabela `newproject_cms_usuarios`
--

INSERT INTO `newproject_cms_usuarios` (`id`, `nome`, `setor`, `email`, `senha`, `disponivel`, `data_criacao`, `data_atualizacao`, `data_ultimoacesso`, `usuario_criacao`, `usuario_atualizacao`, `ativo`, `id_grupo`, `avatar`, `views`, `permalink`, `ranking`, `id_idioma`) VALUES
(1, 'Maurício Salamon', 'Desenvolvimento', 'mauricio@uzzye.com', 'e7f00aa3f441f44c946f2c05b83c43b6', 1, '0000-00-00 00:00:00', '2017-07-24 14:41:54', '2017-09-26 15:30:33', NULL, 1, 1, 29, '2bdd69_avatar.png', 0, '', 1, 1),
(2, 'Administrador', 'Administração', 'falecom@uzzye.com', '15ac53fd2125f69d0d81aebd8c9eb9a4', 1, '2010-04-30 12:34:28', '2011-12-08 16:43:46', '2010-07-22 19:36:48', NULL, 1, 1, 29, 'usuarios_avatar_2.jpg', 0, NULL, 2, 1),
(99, 'Visitante', '', 'visitante@uzzye.com', 'e7f00aa3f441f44c946f2c05b83c43b6', 1, NULL, '2011-12-08 16:44:08', NULL, NULL, 1, 1, 29, NULL, 0, NULL, 2, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `newproject_configuracoes`
--

CREATE TABLE IF NOT EXISTS `newproject_configuracoes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idioma_padrao` int(1) DEFAULT NULL,
  `email_padrao_contato` tinytext NOT NULL,
  `email_default_from` tinytext NOT NULL,
  `nome_default_from` tinytext NOT NULL,
  `servidor_smtp` tinytext,
  `porta_smtp` tinytext,
  `secure_smtp` tinytext,
  `servidor_pop` tinytext,
  `porta_pop` tinytext,
  `secure_pop` tinytext,
  `usuario_email` tinytext,
  `senha_email` tinytext,
  `titulo_padrao` tinytext NOT NULL,
  `descricao_padrao` text NOT NULL,
  `palavras_chave_padrao` text NOT NULL,
  `url_amigaveis` int(1) NOT NULL DEFAULT '1',
  `charset_padrao` varchar(20) NOT NULL DEFAULT '',
  `qtd_itens_pp` int(3) NOT NULL DEFAULT '20',
  `qtd_group_pag` int(3) NOT NULL DEFAULT '3',
  `id_idioma` int(11) NOT NULL DEFAULT '99',
  `data_criacao` datetime NOT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `usuario_criacao` int(11) NOT NULL,
  `usuario_atualizacao` int(11) DEFAULT NULL,
  `ativo` int(1) DEFAULT '1',
  `codigo_analytics` text,
  `codigo_css` text,
  `codigo_javascript` text,
  `geolocalizacao` text,
  `mapa_embed` text,
  `views` int(11) DEFAULT '0',
  `permalink` tinytext,
  `ranking` int(11) DEFAULT '1',
  `ga_user` varchar(255) DEFAULT NULL,
  `valor_conversao_contato` double(12,2) DEFAULT NULL,
  `ga_props` text,
  PRIMARY KEY (`id`),
  KEY `fk_newproject_configuracoes_usuario_criacao` (`usuario_criacao`),
  KEY `fk_newproject_configuracoes_usuario_atualizacao` (`usuario_atualizacao`),
  KEY `fk_newproject_configuracoes_id_idioma` (`id_idioma`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `newproject_configuracoes`
--

INSERT INTO `newproject_configuracoes` (`id`, `idioma_padrao`, `email_padrao_contato`, `email_default_from`, `nome_default_from`, `servidor_smtp`, `porta_smtp`, `secure_smtp`, `servidor_pop`, `porta_pop`, `secure_pop`, `usuario_email`, `senha_email`, `titulo_padrao`, `descricao_padrao`, `palavras_chave_padrao`, `url_amigaveis`, `charset_padrao`, `qtd_itens_pp`, `qtd_group_pag`, `id_idioma`, `data_criacao`, `data_atualizacao`, `usuario_criacao`, `usuario_atualizacao`, `ativo`, `codigo_analytics`, `codigo_css`, `codigo_javascript`, `geolocalizacao`, `mapa_embed`, `views`, `permalink`, `ranking`, `ga_user`, `valor_conversao_contato`, `ga_props`) VALUES
(1, 1, 'mauricio@uzzye.com', 'mauricio@uzzye.com', 'Uzzye.', '', '', '', '', '', '', '', '', 'Uzzye.', 'Born for online marketing and digital business . Nascidos para o marketing online e negócios digitais', 'uzzye, agencia, presenca, inovacao, tecnologia, digital, produtora, marca, midia, midias sociais, social, mobile, aplicativo, app, facebook, google, lets connect, marketing, publicidade, desenvolvimento, online, twitter, site, blog, portal, web, mausalamon, salamon, mauricio, caxias do sul, caxias, serra, gaucha, rs, brasil', 1, 'UTF-8', 20, 3, 1, '2012-04-18 17:14:42', '2017-09-04 16:40:02', 1, 1, 1, NULL, '', '', '<meta content="Caxias do Sul - RS, Brazil" />\r\n<meta content="-29.1578096;-51.1846846" />\r\n<meta content="BR-RS" />\r\n<meta content="-29.1578096,-51.1846846" />', '', 0, NULL, 1, 'agenciauzzye@kinetic-valor-128616.iam.gserviceaccount.com', 1.00, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `newproject_contatos`
--

CREATE TABLE IF NOT EXISTS `newproject_contatos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL,
  `mensagem` text NOT NULL,
  `email` tinytext NOT NULL,
  `data_criacao` datetime NOT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `usuario_criacao` int(11) NOT NULL,
  `usuario_atualizacao` int(11) DEFAULT NULL,
  `ativo` int(1) DEFAULT '1',
  `ip` varchar(100) NOT NULL,
  `telefone` varchar(255) DEFAULT NULL,
  `assunto` varchar(100) DEFAULT NULL,
  `cidade` varchar(255) DEFAULT NULL,
  `estado` varchar(255) DEFAULT NULL,
  `arquivo` tinytext,
  `recebe_novidades` int(1) DEFAULT NULL,
  `id_referencia` int(11) DEFAULT NULL,
  `referencia` varchar(255) DEFAULT NULL,
  `views` int(11) DEFAULT '0',
  `permalink` tinytext,
  `ranking` int(11) DEFAULT '1',
  `conversao` int(1) DEFAULT NULL,
  `valor_conversao` double(12,2) DEFAULT NULL,
  `data_conversao` datetime DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_newproject_contatos_usuario_criacao` (`usuario_criacao`),
  KEY `fk_newproject_contatos_usuario_atualizacao` (`usuario_atualizacao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `newproject_conteudos`
--

CREATE TABLE IF NOT EXISTS `newproject_conteudos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` longtext NOT NULL,
  `texto_capa` text,
  `data_criacao` datetime NOT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `usuario_criacao` int(11) NOT NULL,
  `usuario_atualizacao` int(11) DEFAULT NULL,
  `ativo` int(1) DEFAULT '1',
  `titulo` varchar(200) NOT NULL,
  `subtitulo` varchar(200) DEFAULT NULL,
  `nome_arquivo` varchar(200) NOT NULL,
  `banner_topo` tinytext,
  `meta_descricao` text,
  `meta_palavras_chave` text,
  `id_idioma` int(11) NOT NULL DEFAULT '99',
  `views` int(11) DEFAULT '0',
  `permalink` tinytext,
  `ranking` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_newproject_conteudos_usuario_criacao` (`usuario_criacao`),
  KEY `fk_newproject_conteudos_usuario_atualizacao` (`usuario_atualizacao`),
  KEY `fk_newproject_conteudos_id_idioma` (`id_idioma`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `newproject_downloads`
--

CREATE TABLE IF NOT EXISTS `newproject_downloads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_criacao` datetime DEFAULT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `usuario_criacao` int(11) DEFAULT NULL,
  `usuario_atualizacao` int(11) DEFAULT NULL,
  `ativo` int(1) DEFAULT '1',
  `views` int(11) DEFAULT '0',
  `permalink` tinytext,
  `ranking` int(11) DEFAULT '1',
  `modulo` varchar(255) DEFAULT NULL,
  `id_registro` int(11) DEFAULT NULL,
  `arquivo` tinytext,
  `id_usuario` int(11) DEFAULT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `cidade` varchar(255) DEFAULT NULL,
  `estado` varchar(255) DEFAULT NULL,
  `id_referencia` int(11) DEFAULT NULL,
  `referencia` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `nome_arquivo` varchar(255) DEFAULT NULL,
  `tamanho` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_newproject_downloads_usuario_criacao` (`usuario_criacao`),
  KEY `fk_newproject_downloads_usuario_atualizacao` (`usuario_atualizacao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `newproject_idiomas`
--

CREATE TABLE IF NOT EXISTS `newproject_idiomas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identificador` varchar(100) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `data_criacao` datetime NOT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `usuario_criacao` int(11) NOT NULL,
  `usuario_atualizacao` int(11) DEFAULT NULL,
  `ativo` int(1) DEFAULT '1',
  `views` int(11) DEFAULT '0',
  `permalink` tinytext,
  `ranking` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_newproject_idiomas_usuario_criacao` (`usuario_criacao`),
  KEY `fk_newproject_idiomas_usuario_atualizacao` (`usuario_atualizacao`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=100 ;

--
-- Extraindo dados da tabela `newproject_idiomas`
--

INSERT INTO `newproject_idiomas` (`id`, `identificador`, `titulo`, `data_criacao`, `data_atualizacao`, `usuario_criacao`, `usuario_atualizacao`, `ativo`, `views`, `permalink`, `ranking`) VALUES
(1, 'pt-br', 'Português (Brasil)', '2012-04-18 16:06:15', NULL, 1, NULL, 1, 0, NULL, 2),
(99, 'todos', 'Todos', '2016-08-16 17:37:58', NULL, 1, NULL, 1, 0, NULL, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `newproject_itens_teste`
--

CREATE TABLE IF NOT EXISTS `newproject_itens_teste` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_criacao` datetime DEFAULT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `usuario_criacao` int(11) DEFAULT NULL,
  `usuario_atualizacao` int(11) DEFAULT NULL,
  `ativo` int(1) DEFAULT '1',
  `views` int(11) DEFAULT '0',
  `permalink` tinytext,
  `ranking` int(11) DEFAULT '1',
  `id_teste` int(11) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `imagem` tinytext,
  `imagem_thumb` tinytext,
  PRIMARY KEY (`id`),
  KEY `fk_newproject_itens_teste_usuario_criacao` (`usuario_criacao`),
  KEY `fk_newproject_itens_teste_usuario_atualizacao` (`usuario_atualizacao`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Extraindo dados da tabela `newproject_itens_teste`
--

INSERT INTO `newproject_itens_teste` (`id`, `data_criacao`, `data_atualizacao`, `usuario_criacao`, `usuario_atualizacao`, `ativo`, `views`, `permalink`, `ranking`, `id_teste`, `label`, `imagem`, `imagem_thumb`) VALUES
(9, '2017-07-28 17:00:31', NULL, 1, NULL, 1, 0, NULL, 1, 13, NULL, 'Barlambras.png', 'Barlambras_G39dG.png'),
(10, '2017-07-28 17:00:31', NULL, 1, NULL, 1, 0, NULL, 2, 13, NULL, 'Barlambras.jpg', 'Barlambras_5c7h1W.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `newproject_language`
--

CREATE TABLE IF NOT EXISTS `newproject_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_criacao` datetime DEFAULT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `usuario_criacao` int(11) DEFAULT NULL,
  `usuario_atualizacao` int(11) DEFAULT NULL,
  `ativo` int(1) DEFAULT '1',
  `views` int(11) DEFAULT '0',
  `permalink` tinytext,
  `ranking` int(11) DEFAULT '1',
  `id_idioma` int(11) DEFAULT NULL,
  `id_registro` int(11) DEFAULT NULL,
  `modulo_registro` varchar(255) DEFAULT NULL,
  `variavel` varchar(255) DEFAULT NULL,
  `valor` longtext,
  PRIMARY KEY (`id`),
  KEY `fk_newproject_language_usuario_criacao` (`usuario_criacao`),
  KEY `fk_newproject_language_usuario_atualizacao` (`usuario_atualizacao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `newproject_links_uteis`
--

CREATE TABLE IF NOT EXISTS `newproject_links_uteis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_criacao` datetime DEFAULT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `usuario_criacao` int(11) DEFAULT NULL,
  `usuario_atualizacao` int(11) DEFAULT NULL,
  `ativo` int(1) DEFAULT '1',
  `referencia` varchar(255) NOT NULL DEFAULT '',
  `titulo` tinytext NOT NULL,
  `url` tinytext NOT NULL,
  `target` set('','_blank') DEFAULT NULL,
  `id_idioma` int(11) NOT NULL DEFAULT '99',
  `views` int(11) DEFAULT '0',
  `permalink` tinytext,
  `ranking` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_newproject_links_uteis_usuario_criacao` (`usuario_criacao`),
  KEY `fk_newproject_links_uteis_usuario_atualizacao` (`usuario_atualizacao`),
  KEY `fk_newproject_links_uteis_id_idioma` (`id_idioma`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `newproject_log_views`
--

CREATE TABLE IF NOT EXISTS `newproject_log_views` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_criacao` datetime DEFAULT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `usuario_criacao` int(11) DEFAULT NULL,
  `usuario_atualizacao` int(11) DEFAULT NULL,
  `ativo` int(1) DEFAULT '1',
  `views` int(11) DEFAULT '0',
  `permalink` tinytext,
  `ranking` int(11) DEFAULT '1',
  `modulo_registro` varchar(255) DEFAULT NULL,
  `id_registro` int(11) DEFAULT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `idade` int(3) DEFAULT NULL,
  `sexo` varchar(255) DEFAULT NULL,
  `relacionamento` varchar(255) DEFAULT NULL,
  `geolocalizacao` tinytext,
  `cidade` varchar(255) DEFAULT NULL,
  `estado` varchar(255) DEFAULT NULL,
  `pais` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `id_facebook` tinytext,
  `email` tinytext,
  `tags` text,
  PRIMARY KEY (`id`),
  KEY `fk_newproject_log_views_usuario_criacao` (`usuario_criacao`),
  KEY `fk_newproject_log_views_usuario_atualizacao` (`usuario_atualizacao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `newproject_modulos_teste`
--

CREATE TABLE IF NOT EXISTS `newproject_modulos_teste` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_criacao` datetime DEFAULT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `usuario_criacao` int(11) DEFAULT NULL,
  `usuario_atualizacao` int(11) DEFAULT NULL,
  `ativo` int(1) DEFAULT '1',
  `views` int(11) DEFAULT '0',
  `permalink` tinytext,
  `ranking` int(11) DEFAULT '1',
  `id_modulo` int(11) DEFAULT NULL,
  `id_teste` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_newproject_modulos_teste_usuario_criacao` (`usuario_criacao`),
  KEY `fk_newproject_modulos_teste_usuario_atualizacao` (`usuario_atualizacao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------
--
-- Estrutura da tabela `newproject_teste`
--

CREATE TABLE IF NOT EXISTS `newproject_teste` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_criacao` datetime DEFAULT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `usuario_criacao` int(11) DEFAULT NULL,
  `usuario_atualizacao` int(11) DEFAULT NULL,
  `ativo` int(1) DEFAULT '1',
  `views` int(11) DEFAULT '0',
  `permalink` tinytext,
  `ranking` int(11) DEFAULT '1',
  `cms_modulos` text,
  `imagem` tinytext,
  `imagem_thumb` tinytext,
  `arquivo` tinytext,
  `descricao` text,
  `descricao_html` text,
  `sexo` int(1) DEFAULT NULL,
  `valor` double(12,2) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `cor` varchar(255) DEFAULT NULL,
  `geolocalizacao` text,
  `titulo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_newproject_teste_usuario_criacao` (`usuario_criacao`),
  KEY `fk_newproject_teste_usuario_atualizacao` (`usuario_atualizacao`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Extraindo dados da tabela `newproject_teste`
--

INSERT INTO `newproject_teste` (`id`, `data_criacao`, `data_atualizacao`, `usuario_criacao`, `usuario_atualizacao`, `ativo`, `views`, `permalink`, `ranking`, `cms_modulos`, `imagem`, `imagem_thumb`, `arquivo`, `descricao`, `descricao_html`, `sexo`, `valor`, `data`, `cor`, `geolocalizacao`, `titulo`) VALUES
(13, '2017-07-11 14:48:19', '2017-08-08 19:17:31', 1, 1, 1, 9, 'teste/barlambras/13', 1, '1;2;3', 'Barlambras.jpg', 'Barlambras.jpeg', '', '', '', 0, 500.56, '0000-00-00 00:00:00', 'rgba(191,191,191,0.51)', '-29.1548432,-51.186641399999985', 'Barlambras');

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `newproject_cms_grupos_usuarios`
--
ALTER TABLE `newproject_cms_grupos_usuarios`
  ADD CONSTRAINT `fk_newproject_grupos_usuarios_atualizacao` FOREIGN KEY (`usuario_atualizacao`) REFERENCES `newproject_cms_usuarios` (`id`),
  ADD CONSTRAINT `fk_newproject_grupos_usuarios_usuario_criacao` FOREIGN KEY (`usuario_criacao`) REFERENCES `newproject_cms_usuarios` (`id`);

--
-- Limitadores para a tabela `newproject_cms_imagens`
--
ALTER TABLE `newproject_cms_imagens`
  ADD CONSTRAINT `fk_newproject_cms_imagens_usuario_atualizacao` FOREIGN KEY (`usuario_atualizacao`) REFERENCES `newproject_cms_usuarios` (`id`),
  ADD CONSTRAINT `fk_newproject_cms_imagens_usuario_criacao` FOREIGN KEY (`usuario_criacao`) REFERENCES `newproject_cms_usuarios` (`id`);

--
-- Limitadores para a tabela `newproject_cms_log_entidades`
--
ALTER TABLE `newproject_cms_log_entidades`
  ADD CONSTRAINT `fk_newproject_logentidades_usuario_atualizacao` FOREIGN KEY (`usuario_atualizacao`) REFERENCES `newproject_cms_usuarios` (`id`),
  ADD CONSTRAINT `fk_newproject_logentidades_usuario_criacao` FOREIGN KEY (`usuario_criacao`) REFERENCES `newproject_cms_usuarios` (`id`);

--
-- Limitadores para a tabela `newproject_cms_modulos`
--
ALTER TABLE `newproject_cms_modulos`
  ADD CONSTRAINT `fk_newproject_modulos_modulo_pai` FOREIGN KEY (`modulo_pai`) REFERENCES `newproject_cms_modulos` (`id`),
  ADD CONSTRAINT `fk_newproject_modulos_usuario_atualizacao` FOREIGN KEY (`usuario_atualizacao`) REFERENCES `newproject_cms_usuarios` (`id`),
  ADD CONSTRAINT `fk_newproject_modulos_usuario_criacao` FOREIGN KEY (`usuario_criacao`) REFERENCES `newproject_cms_usuarios` (`id`);

--
-- Limitadores para a tabela `newproject_cms_modulos_grupos_usuarios`
--
ALTER TABLE `newproject_cms_modulos_grupos_usuarios`
  ADD CONSTRAINT `fk_newproject_modulos_grupos_usuarios_id_grupo` FOREIGN KEY (`id_grupo`) REFERENCES `newproject_cms_grupos_usuarios` (`id`),
  ADD CONSTRAINT `fk_newproject_modulos_grupos_usuarios_id_modulo` FOREIGN KEY (`id_modulo`) REFERENCES `newproject_cms_modulos` (`id`),
  ADD CONSTRAINT `fk_newproject_modulos_grupos_usuarios_usuario_atualizacao` FOREIGN KEY (`usuario_atualizacao`) REFERENCES `newproject_cms_usuarios` (`id`),
  ADD CONSTRAINT `fk_newproject_modulos_grupos_usuarios_usuario_criacao` FOREIGN KEY (`usuario_criacao`) REFERENCES `newproject_cms_usuarios` (`id`);

--
-- Limitadores para a tabela `newproject_cms_modulos_usuarios`
--
ALTER TABLE `newproject_cms_modulos_usuarios`
  ADD CONSTRAINT `fk_newproject_modulos_usuarios_id_modulo` FOREIGN KEY (`id_modulo`) REFERENCES `newproject_cms_modulos` (`id`),
  ADD CONSTRAINT `fk_newproject_modulos_usuarios_id_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `newproject_cms_usuarios` (`id`),
  ADD CONSTRAINT `fk_newproject_modulos_usuarios_usuario_atualizacao` FOREIGN KEY (`usuario_atualizacao`) REFERENCES `newproject_cms_usuarios` (`id`),
  ADD CONSTRAINT `fk_newproject_modulos_usuarios_usuario_criacao` FOREIGN KEY (`usuario_criacao`) REFERENCES `newproject_cms_usuarios` (`id`);

--
-- Limitadores para a tabela `newproject_cms_usuarios`
--
ALTER TABLE `newproject_cms_usuarios`
  ADD CONSTRAINT `fk_newproject_usuarios_usuario_atualizacao` FOREIGN KEY (`usuario_atualizacao`) REFERENCES `newproject_cms_usuarios` (`id`),
  ADD CONSTRAINT `fk_newproject_usuarios_usuario_criacao` FOREIGN KEY (`usuario_criacao`) REFERENCES `newproject_cms_usuarios` (`id`);

--
-- Limitadores para a tabela `newproject_configuracoes`
--
ALTER TABLE `newproject_configuracoes`
  ADD CONSTRAINT `fk_newproject_configuracoes_id_idioma` FOREIGN KEY (`id_idioma`) REFERENCES `newproject_idiomas` (`id`),
  ADD CONSTRAINT `fk_newproject_configuracoes_usuario_atualizacao` FOREIGN KEY (`usuario_atualizacao`) REFERENCES `newproject_cms_usuarios` (`id`),
  ADD CONSTRAINT `fk_newproject_configuracoes_usuario_criacao` FOREIGN KEY (`usuario_criacao`) REFERENCES `newproject_cms_usuarios` (`id`);

--
-- Limitadores para a tabela `newproject_contatos`
--
ALTER TABLE `newproject_contatos`
  ADD CONSTRAINT `fk_newproject_contatos_usuario_atualizacao` FOREIGN KEY (`usuario_atualizacao`) REFERENCES `newproject_cms_usuarios` (`id`),
  ADD CONSTRAINT `fk_newproject_contatos_usuario_criacao` FOREIGN KEY (`usuario_criacao`) REFERENCES `newproject_cms_usuarios` (`id`);

--
-- Limitadores para a tabela `newproject_conteudos`
--
ALTER TABLE `newproject_conteudos`
  ADD CONSTRAINT `fk_newproject_conteudos_id_idioma` FOREIGN KEY (`id_idioma`) REFERENCES `newproject_idiomas` (`id`),
  ADD CONSTRAINT `fk_newproject_conteudos_usuario_atualizacao` FOREIGN KEY (`usuario_atualizacao`) REFERENCES `newproject_cms_usuarios` (`id`),
  ADD CONSTRAINT `fk_newproject_conteudos_usuario_criacao` FOREIGN KEY (`usuario_criacao`) REFERENCES `newproject_cms_usuarios` (`id`);

--
-- Limitadores para a tabela `newproject_downloads`
--
ALTER TABLE `newproject_downloads`
  ADD CONSTRAINT `fk_newproject_downloads_usuario_atualizacao` FOREIGN KEY (`usuario_atualizacao`) REFERENCES `newproject_cms_usuarios` (`id`),
  ADD CONSTRAINT `fk_newproject_downloads_usuario_criacao` FOREIGN KEY (`usuario_criacao`) REFERENCES `newproject_cms_usuarios` (`id`);

--
-- Limitadores para a tabela `newproject_idiomas`
--
ALTER TABLE `newproject_idiomas`
  ADD CONSTRAINT `fk_newproject_idiomas_usuario_atualizacao` FOREIGN KEY (`usuario_atualizacao`) REFERENCES `newproject_cms_usuarios` (`id`),
  ADD CONSTRAINT `fk_newproject_idiomas_usuario_criacao` FOREIGN KEY (`usuario_criacao`) REFERENCES `newproject_cms_usuarios` (`id`);

--
-- Limitadores para a tabela `newproject_itens_teste`
--
ALTER TABLE `newproject_itens_teste`
  ADD CONSTRAINT `fk_newproject_itens_teste_usuario_atualizacao` FOREIGN KEY (`usuario_atualizacao`) REFERENCES `newproject_cms_usuarios` (`id`),
  ADD CONSTRAINT `fk_newproject_itens_teste_usuario_criacao` FOREIGN KEY (`usuario_criacao`) REFERENCES `newproject_cms_usuarios` (`id`);

--
-- Limitadores para a tabela `newproject_language`
--
ALTER TABLE `newproject_language`
  ADD CONSTRAINT `fk_newproject_language_usuario_atualizacao` FOREIGN KEY (`usuario_atualizacao`) REFERENCES `newproject_cms_usuarios` (`id`),
  ADD CONSTRAINT `fk_newproject_language_usuario_criacao` FOREIGN KEY (`usuario_criacao`) REFERENCES `newproject_cms_usuarios` (`id`);

--
-- Limitadores para a tabela `newproject_links_uteis`
--
ALTER TABLE `newproject_links_uteis`
  ADD CONSTRAINT `fk_newproject_links_uteis_id_idioma` FOREIGN KEY (`id_idioma`) REFERENCES `newproject_idiomas` (`id`),
  ADD CONSTRAINT `fk_newproject_links_uteis_usuario_atualizacao` FOREIGN KEY (`usuario_atualizacao`) REFERENCES `newproject_cms_usuarios` (`id`),
  ADD CONSTRAINT `fk_newproject_links_uteis_usuario_criacao` FOREIGN KEY (`usuario_criacao`) REFERENCES `newproject_cms_usuarios` (`id`);

--
-- Limitadores para a tabela `newproject_log_views`
--
ALTER TABLE `newproject_log_views`
  ADD CONSTRAINT `fk_newproject_log_views_usuario_atualizacao` FOREIGN KEY (`usuario_atualizacao`) REFERENCES `newproject_cms_usuarios` (`id`),
  ADD CONSTRAINT `fk_newproject_log_views_usuario_criacao` FOREIGN KEY (`usuario_criacao`) REFERENCES `newproject_cms_usuarios` (`id`);

--
-- Limitadores para a tabela `newproject_modulos_teste`
--
ALTER TABLE `newproject_modulos_teste`
  ADD CONSTRAINT `fk_newproject_modulos_teste_usuario_atualizacao` FOREIGN KEY (`usuario_atualizacao`) REFERENCES `newproject_cms_usuarios` (`id`),
  ADD CONSTRAINT `fk_newproject_modulos_teste_usuario_criacao` FOREIGN KEY (`usuario_criacao`) REFERENCES `newproject_cms_usuarios` (`id`);

-- Limitadores para a tabela `newproject_teste`
--
ALTER TABLE `newproject_teste`
  ADD CONSTRAINT `fk_newproject_teste_usuario_atualizacao` FOREIGN KEY (`usuario_atualizacao`) REFERENCES `newproject_cms_usuarios` (`id`),
  ADD CONSTRAINT `fk_newproject_teste_usuario_criacao` FOREIGN KEY (`usuario_criacao`) REFERENCES `newproject_cms_usuarios` (`id`);



-- Módulos Padrão



-- MODULOS

--
-- Estrutura da tabela `newproject_cadastros`
--

CREATE TABLE IF NOT EXISTS `newproject_cadastros` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_criacao` datetime DEFAULT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `usuario_criacao` int(11) DEFAULT NULL,
  `usuario_atualizacao` int(11) DEFAULT NULL,
  `ativo` int(1) DEFAULT '1',
  `views` int(11) DEFAULT '0',
  `permalink` tinytext,
  `ranking` int(11) DEFAULT '1',
  `nome` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `url` text,
  `ip` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_newproject_cadastros_usuario_criacao` (`usuario_criacao`),
  KEY `fk_newproject_cadastros_usuario_atualizacao` (`usuario_atualizacao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `newproject_destaques`
--

CREATE TABLE IF NOT EXISTS `newproject_destaques` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_criacao` datetime DEFAULT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `usuario_criacao` int(11) DEFAULT NULL,
  `usuario_atualizacao` int(11) DEFAULT NULL,
  `ativo` int(1) DEFAULT '1',
  `views` int(11) DEFAULT '0',
  `permalink` tinytext,
  `ranking` int(11) DEFAULT '1',
  `titulo_pt` varchar(255) DEFAULT NULL,
  `titulo_en` varchar(255) DEFAULT NULL,
  `titulo_es` varchar(255) DEFAULT NULL,
  `imagem` tinytext,
  PRIMARY KEY (`id`),
  KEY `fk_newproject_destaques_usuario_criacao` (`usuario_criacao`),
  KEY `fk_newproject_destaques_usuario_atualizacao` (`usuario_atualizacao`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `newproject_destaques`
--
-- --------------------------------------------------------

--
-- Estrutura da tabela `newproject_diferenciais`
--

CREATE TABLE IF NOT EXISTS `newproject_diferenciais` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_criacao` datetime DEFAULT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `usuario_criacao` int(11) DEFAULT NULL,
  `usuario_atualizacao` int(11) DEFAULT NULL,
  `ativo` int(1) DEFAULT '1',
  `views` int(11) DEFAULT '0',
  `permalink` tinytext,
  `ranking` int(11) DEFAULT '1',
  `titulo_pt` text,
  `titulo_en` text,
  `titulo_es` text,
  `icone` tinytext,
  PRIMARY KEY (`id`),
  KEY `fk_newproject_premios_usuario_criacao` (`usuario_criacao`),
  KEY `fk_newproject_premios_usuario_atualizacao` (`usuario_atualizacao`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `newproject_diferenciais`
--
-- --------------------------------------------------------

--
-- Estrutura da tabela `newproject_estados`
--

CREATE TABLE IF NOT EXISTS `newproject_estados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_criacao` datetime DEFAULT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `usuario_criacao` int(11) DEFAULT NULL,
  `usuario_atualizacao` int(11) DEFAULT NULL,
  `ativo` int(1) DEFAULT '1',
  `views` int(11) DEFAULT '0',
  `permalink` tinytext,
  `ranking` int(11) DEFAULT '1',
  `titulo` varchar(255) DEFAULT NULL,
  `uf` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_newproject_estados_usuario_criacao` (`usuario_criacao`),
  KEY `fk_newproject_estados_usuario_atualizacao` (`usuario_atualizacao`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `newproject_estados`
--
-- --------------------------------------------------------

--
-- Estrutura da tabela `newproject_imagens`
--

CREATE TABLE IF NOT EXISTS `newproject_imagens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_criacao` datetime DEFAULT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `usuario_criacao` int(11) DEFAULT NULL,
  `usuario_atualizacao` int(11) DEFAULT NULL,
  `ativo` int(1) DEFAULT '1',
  `views` int(11) DEFAULT '0',
  `permalink` tinytext,
  `ranking` int(11) DEFAULT '1',
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` tinytext,
  `referencia` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_newproject_imagens_sobre_usuario_criacao` (`usuario_criacao`),
  KEY `fk_newproject_imagens_sobre_usuario_atualizacao` (`usuario_atualizacao`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `newproject_imagens`
--
-- --------------------------------------------------------

--
-- Estrutura da tabela `newproject_lojas_virtuais`
--

CREATE TABLE IF NOT EXISTS `newproject_lojas_virtuais` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_criacao` datetime DEFAULT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `usuario_criacao` int(11) DEFAULT NULL,
  `usuario_atualizacao` int(11) DEFAULT NULL,
  `ativo` int(1) DEFAULT '1',
  `views` int(11) DEFAULT '0',
  `permalink` tinytext,
  `ranking` int(11) DEFAULT '1',
  `titulo` varchar(255) DEFAULT NULL,
  `logo` tinytext,
  `url` longtext,
  PRIMARY KEY (`id`),
  KEY `fk_newproject_lojas_virtuais_usuario_criacao` (`usuario_criacao`),
  KEY `fk_newproject_lojas_virtuais_usuario_atualizacao` (`usuario_atualizacao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `newproject_noticias_tags`
--

CREATE TABLE IF NOT EXISTS `newproject_noticias_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_criacao` datetime DEFAULT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `usuario_criacao` int(11) DEFAULT NULL,
  `usuario_atualizacao` int(11) DEFAULT NULL,
  `ativo` int(1) DEFAULT '1',
  `views` int(11) DEFAULT '0',
  `permalink` tinytext,
  `ranking` int(11) DEFAULT '1',
  `titulo_pt` varchar(255) DEFAULT NULL,
  `titulo_en` varchar(255) DEFAULT NULL,
  `titulo_es` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_newproject_noticias_tags_usuario_criacao` (`usuario_criacao`),
  KEY `fk_newproject_noticias_tags_usuario_atualizacao` (`usuario_atualizacao`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Estrutura da tabela `newproject_noticias`
--

CREATE TABLE IF NOT EXISTS `newproject_noticias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_criacao` datetime DEFAULT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `usuario_criacao` int(11) DEFAULT NULL,
  `usuario_atualizacao` int(11) DEFAULT NULL,
  `ativo` int(1) DEFAULT '1',
  `views` int(11) DEFAULT '0',
  `permalink` tinytext,
  `ranking` int(11) DEFAULT '1',
  `titulo_pt` varchar(255) DEFAULT NULL,
  `titulo_en` varchar(255) DEFAULT NULL,
  `titulo_es` varchar(255) DEFAULT NULL,
  `descricao_pt` longtext,
  `descricao_en` longtext,
  `descricao_es` longtext,
  `imagem` tinytext,
  `tags` text,
  PRIMARY KEY (`id`),
  KEY `fk_newproject_noticias_usuario_criacao` (`usuario_criacao`),
  KEY `fk_newproject_noticias_usuario_atualizacao` (`usuario_atualizacao`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `newproject_noticias`
--
-- --------------------------------------------------------

--
-- Estrutura da tabela `newproject_produtos`
--

CREATE TABLE IF NOT EXISTS `newproject_produtos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_criacao` datetime DEFAULT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `usuario_criacao` int(11) DEFAULT NULL,
  `usuario_atualizacao` int(11) DEFAULT NULL,
  `ativo` int(1) DEFAULT '1',
  `views` int(11) DEFAULT '0',
  `permalink` tinytext,
  `ranking` int(11) DEFAULT '1',
  `categorias` varchar(255) DEFAULT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` tinytext,
  `descricao_pt` longtext,
  `chamada_pt` text,
  `destaque` int(1) DEFAULT NULL,
  `url_loja` longtext,
  `target_loja` varchar(255) DEFAULT NULL,
  `mostrar_menu` int(1) DEFAULT NULL,
  `descricao_en` longtext,
  `descricao_es` longtext,
  `chamada_en` text,
  `chamada_es` text,
  `caracteristicas_en` longtext,
  `caracteristicas_es` longtext,
  `caracteristicas_pt` longtext,
  `produtos_relacionados` text,
  `manual` tinytext,
  PRIMARY KEY (`id`),
  KEY `fk_newproject_produtos_usuario_criacao` (`usuario_criacao`),
  KEY `fk_newproject_produtos_usuario_atualizacao` (`usuario_atualizacao`),
  KEY `fk_newproject_produtos_id_categoria` (`categorias`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `newproject_produtos`
--
-- --------------------------------------------------------

--
-- Estrutura da tabela `newproject_produtos_categorias`
--

CREATE TABLE IF NOT EXISTS `newproject_produtos_categorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_criacao` datetime DEFAULT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `usuario_criacao` int(11) DEFAULT NULL,
  `usuario_atualizacao` int(11) DEFAULT NULL,
  `ativo` int(1) DEFAULT '1',
  `views` int(11) DEFAULT '0',
  `permalink` tinytext,
  `ranking` int(11) DEFAULT '1',
  `titulo_pt` varchar(255) DEFAULT NULL,
  `titulo_en` varchar(255) DEFAULT NULL,
  `titulo_es` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_newproject_produtos_categorias_usuario_criacao` (`usuario_criacao`),
  KEY `fk_newproject_produtos_categorias_usuario_atualizacao` (`usuario_atualizacao`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `newproject_produtos_categorias`
--
-- --------------------------------------------------------

--
-- Estrutura da tabela `newproject_produtos_imagens`
--

CREATE TABLE IF NOT EXISTS `newproject_produtos_imagens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_criacao` datetime DEFAULT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `usuario_criacao` int(11) DEFAULT NULL,
  `usuario_atualizacao` int(11) DEFAULT NULL,
  `ativo` int(1) DEFAULT '1',
  `views` int(11) DEFAULT '0',
  `permalink` tinytext,
  `ranking` int(11) DEFAULT '1',
  `id_produto` int(11) DEFAULT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` longtext,
  PRIMARY KEY (`id`),
  KEY `fk_newproject_produtos_infos_usuario_criacao` (`usuario_criacao`),
  KEY `fk_newproject_produtos_infos_usuario_atualizacao` (`usuario_atualizacao`),
  KEY `fk_newproject_produtos_infos_id_produto` (`id_produto`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `newproject_representantes`
--

CREATE TABLE IF NOT EXISTS `newproject_representantes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_criacao` datetime DEFAULT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `usuario_criacao` int(11) DEFAULT NULL,
  `usuario_atualizacao` int(11) DEFAULT NULL,
  `ativo` int(1) DEFAULT '1',
  `views` int(11) DEFAULT '0',
  `permalink` tinytext,
  `ranking` int(11) DEFAULT '1',
  `estados` text DEFAULT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `endereco` text,
  `email` text,
  `telefone` text,
  `geolocalizacao` text,
  PRIMARY KEY (`id`),
  KEY `fk_newproject_distribuidores_usuario_criacao` (`usuario_criacao`),
  KEY `fk_newproject_distribuidores_usuario_atualizacao` (`usuario_atualizacao`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `newproject_representantes`
--

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `newproject_destaques`
--
ALTER TABLE `newproject_destaques`
  ADD CONSTRAINT `fk_newproject_destaques_usuario_atualizacao` FOREIGN KEY (`usuario_atualizacao`) REFERENCES `newproject_cms_usuarios` (`id`),
  ADD CONSTRAINT `fk_newproject_destaques_usuario_criacao` FOREIGN KEY (`usuario_criacao`) REFERENCES `newproject_cms_usuarios` (`id`);

--
-- Limitadores para a tabela `newproject_diferenciais`
--
ALTER TABLE `newproject_diferenciais`
  ADD CONSTRAINT `fk_newproject_premios_usuario_atualizacao` FOREIGN KEY (`usuario_atualizacao`) REFERENCES `newproject_cms_usuarios` (`id`),
  ADD CONSTRAINT `fk_newproject_premios_usuario_criacao` FOREIGN KEY (`usuario_criacao`) REFERENCES `newproject_cms_usuarios` (`id`);

--
-- Limitadores para a tabela `newproject_estados`
--
ALTER TABLE `newproject_estados`
  ADD CONSTRAINT `fk_newproject_estados_usuario_atualizacao` FOREIGN KEY (`usuario_atualizacao`) REFERENCES `newproject_cms_usuarios` (`id`),
  ADD CONSTRAINT `fk_newproject_estados_usuario_criacao` FOREIGN KEY (`usuario_criacao`) REFERENCES `newproject_cms_usuarios` (`id`);

--
-- Limitadores para a tabela `newproject_lojas_virtuais`
--
ALTER TABLE `newproject_lojas_virtuais`
  ADD CONSTRAINT `fk_newproject_lojas_virtuais_usuario_atualizacao` FOREIGN KEY (`usuario_atualizacao`) REFERENCES `newproject_cms_usuarios` (`id`),
  ADD CONSTRAINT `fk_newproject_lojas_virtuais_usuario_criacao` FOREIGN KEY (`usuario_criacao`) REFERENCES `newproject_cms_usuarios` (`id`);

--
-- Limitadores para a tabela `newproject_noticias`
--
ALTER TABLE `newproject_noticias`
  ADD CONSTRAINT `fk_newproject_noticias_usuario_atualizacao` FOREIGN KEY (`usuario_atualizacao`) REFERENCES `newproject_cms_usuarios` (`id`),
  ADD CONSTRAINT `fk_newproject_noticias_usuario_criacao` FOREIGN KEY (`usuario_criacao`) REFERENCES `newproject_cms_usuarios` (`id`);

--
-- Limitadores para a tabela `newproject_produtos`
--
ALTER TABLE `newproject_produtos`
  ADD CONSTRAINT `fk_newproject_produtos_usuario_atualizacao` FOREIGN KEY (`usuario_atualizacao`) REFERENCES `newproject_cms_usuarios` (`id`),
  ADD CONSTRAINT `fk_newproject_produtos_usuario_criacao` FOREIGN KEY (`usuario_criacao`) REFERENCES `newproject_cms_usuarios` (`id`);

--
-- Limitadores para a tabela `newproject_produtos_categorias`
--
ALTER TABLE `newproject_produtos_categorias`
  ADD CONSTRAINT `fk_newproject_produtos_categorias_usuario_atualizacao` FOREIGN KEY (`usuario_atualizacao`) REFERENCES `newproject_cms_usuarios` (`id`),
  ADD CONSTRAINT `fk_newproject_produtos_categorias_usuario_criacao` FOREIGN KEY (`usuario_criacao`) REFERENCES `newproject_cms_usuarios` (`id`);

--
-- Limitadores para a tabela `newproject_produtos_imagens`
--
ALTER TABLE `newproject_produtos_imagens`
  ADD CONSTRAINT `fk_newproject_produtos_infos_id_produto` FOREIGN KEY (`id_produto`) REFERENCES `newproject_produtos` (`id`),
  ADD CONSTRAINT `fk_newproject_produtos_infos_usuario_atualizacao` FOREIGN KEY (`usuario_atualizacao`) REFERENCES `newproject_cms_usuarios` (`id`),
  ADD CONSTRAINT `fk_newproject_produtos_infos_usuario_criacao` FOREIGN KEY (`usuario_criacao`) REFERENCES `newproject_cms_usuarios` (`id`);

--
-- Limitadores para a tabela `newproject_representantes`
--
ALTER TABLE `newproject_representantes`
  ADD CONSTRAINT `fk_newproject_distribuidores_usuario_atualizacao` FOREIGN KEY (`usuario_atualizacao`) REFERENCES `newproject_cms_usuarios` (`id`),
  ADD CONSTRAINT `fk_newproject_distribuidores_usuario_criacao` FOREIGN KEY (`usuario_criacao`) REFERENCES `newproject_cms_usuarios` (`id`);


  
INSERT INTO `newproject_estados` (`id`, `data_criacao`, `data_atualizacao`, `usuario_criacao`, `usuario_atualizacao`, `ativo`, `views`, `permalink`, `ranking`, `titulo`, `uf`) VALUES
(1, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 'Rondônia', 'RO'),
(2, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 'Acre', 'AC'),
(3, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 'Amazonas', 'AM'),
(4, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 'Roraima', 'RR'),
(5, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 'Amapá', 'AP'),
(6, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 'Tocantins', 'TO'),
(7, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 'Mato Grosso', 'MT'),
(8, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 'Goiás', 'GO'),
(9, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 'Mato Grosso do Sul', 'MS'),
(10, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 'Minas Gerais', 'MG'),
(11, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 'Paraná', 'PR'),
(12, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 'Rio Grande do Sul', 'RS'),
(13, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 'Bahia', 'BA'),
(14, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 'Piauí', 'PI'),
(15, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 'Ceará', 'CE'),
(16, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 'Rio Grande do Norte', 'RN'),
(17, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 'Alagoas', 'AL'),
(18, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 'Sergipe', 'SE'),
(19, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 'Distrito Federal', 'DF'),
(20, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 'Pernambuco', 'PE'),
(21, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 'Maranhão', 'MA'),
(22, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 'Pará', 'PA'),
(23, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 'São Paulo', 'SP'),
(24, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 'Rio de Janeiro', 'RJ'),
(25, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 'Espírito Santo', 'ES'),
(26, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 'Santa Catarina', 'SC'),
(27, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 'Paraíba', 'PB');

DROP TABLE IF EXISTS newproject_arquivos_categorias;
CREATE TABLE newproject_arquivos_categorias(
       id int(11) NOT NULL AUTO_INCREMENT,
       data_criacao datetime DEFAULT NULL,
       data_atualizacao datetime DEFAULT NULL,
       usuario_criacao int(11) DEFAULT NULL,
       usuario_atualizacao int(11) DEFAULT NULL,
       ativo int(1) DEFAULT '1',
       views int(11) DEFAULT 0,
       permalink tinytext DEFAULT null,
       ranking int(11) DEFAULT 1,
       titulo_pt varchar(255) DEFAULT NULL,
       titulo_en varchar(255) DEFAULT NULL,
       PRIMARY KEY (`id`),
       CONSTRAINT fk_newproject_arquivos_categorias_usuario_criacao FOREIGN KEY (`usuario_criacao`) REFERENCES newproject_cms_usuarios(id),
       CONSTRAINT fk_newproject_arquivos_categorias_usuario_atualizacao FOREIGN KEY (`usuario_atualizacao`) REFERENCES newproject_cms_usuarios(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

DROP TABLE IF EXISTS newproject_arquivos;
CREATE TABLE newproject_arquivos(
       id int(11) NOT NULL AUTO_INCREMENT,
       data_criacao datetime DEFAULT NULL,
       data_atualizacao datetime DEFAULT NULL,
       usuario_criacao int(11) DEFAULT NULL,
       usuario_atualizacao int(11) DEFAULT NULL,
       ativo int(1) DEFAULT '1',
       views int(11) DEFAULT 0,
       permalink tinytext DEFAULT null,
       ranking int(11) DEFAULT 1,
       id_categoria int(11) DEFAULT NULL,
       titulo_pt varchar(255) DEFAULT NULL,
       titulo_en varchar(255) DEFAULT NULL,
       arquivo text DEFAULT NULL,
       id_idioma int(11) DEFAULT NULL,
       PRIMARY KEY (`id`),
       CONSTRAINT fk_newproject_arquivos_usuario_criacao FOREIGN KEY (`usuario_criacao`) REFERENCES newproject_cms_usuarios(id),
       CONSTRAINT fk_newproject_arquivos_usuario_atualizacao FOREIGN KEY (`usuario_atualizacao`) REFERENCES newproject_cms_usuarios(id),
       CONSTRAINT fk_newproject_arquivos_id_categoria FOREIGN KEY (`id_categoria`) REFERENCES newproject_arquivos_categorias(id),
       CONSTRAINT fk_newproject_arquivos_id_idioma FOREIGN KEY (`id_idioma`) REFERENCES newproject_idiomas(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

ALTER TABLE `newproject_diferenciais` ADD `icone_classe` VARCHAR( 255 ) NULL ,
ADD `descricao_pt` LONGTEXT NULL ,
ADD `descricao_en` LONGTEXT NULL ,
ADD `descricao_es` LONGTEXT NULL ;