<?php

include_once("config.php");

include_once("verificaLogado.php");

//var_dump($_REQUEST); die();

$field_name = "file";

switch ($_FILES[$field_name]['error'])
{
      case 1:
      print '<p> The file is bigger than this PHP installation allows</p>';
      die();

      case 2:
      print '<p> The file is bigger than this form allows</p>';
      die();

      case 3:
      print '<p> Only part of the file was uploaded</p>';
      die();

      //case 4:
      //print '<p> No file was uploaded</p>';
      //die();
}

$mime = explode("/",$_FILES[$field_name]['type']);
$endereco_copia = $nome_arquivo = "";

// Se upload normal
if (!empty($_FILES) && 
    (
      (trim($_FILES[$field_name]["name"]) != "" && is_uploaded_file($_FILES[$field_name]["tmp_name"])) ||
      (trim($_FILES[$field_name]["name"][0]) != "" && is_uploaded_file($_FILES[$field_name]["tmp_name"][0]))
    )
   )
{
      @set_time_limit(0);

      $randcod = $_REQUEST["randcod"];
      $field_id = $_REQUEST["field_id"];

      if($_REQUEST["is_dropzone"]) {
            $_REQUEST_name = $_FILES[$field_name]["name"][0];
            $_REQUEST_tmp_name = $_FILES[$field_name]["tmp_name"][0];
      } else {
            $_REQUEST_name = $_FILES[$field_name]["name"];
            $_REQUEST_tmp_name = $_FILES[$field_name]["tmp_name"];
      }

      $arq = $_REQUEST_name;
      $array_aux = explode(".",$arq);
      $arqname = "";
      for($w=0; $w<sizeof($array_aux)-1; $w++) {
            if($w>0) {$arqname .= ".";}
            $arqname .= $array_aux[$w];
      }
      $ext = $array_aux[sizeof($array_aux)-1];
      
      $endereco_copia = UPLOAD_FOLDER . "temp/";

      if(!$_REQUEST["selfname"]) {
            $nome_arquivo = $randcod . "_" . $field_id . "." . $ext;
      } else {
            $nome_arquivo = $_REQUEST_name;
      }

      $nome_arquivo = gera_titulo_amigavel($nome_arquivo,true,"-",1,1,false);
      
      // Remove já existente
      if(file_exists($endereco_copia . $nome_arquivo))
      {
            @unlink($endereco_copia . $nome_arquivo);
      }                 

      // Envia
      move_uploaded_file($_REQUEST_tmp_name,$endereco_copia . $nome_arquivo);
      chmod($endereco_copia . $nome_arquivo, 0777);
}

// Para image crops
if(isset($_REQUEST["blob"])) {
      $ch = curl_init();

      // set URL and other appropriate options
      curl_setopt($ch, CURLOPT_URL, ROOT_SERVER . ROOT . $endereco_copia . $nome_arquivo);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
      curl_setopt($ch, CURLOPT_HEADER, 0);

      // grab URL and pass it to the browser
      $img = curl_exec($ch);

      // close cURL resource, and free up system resources
      curl_close($ch);

      ob_clean();
      echo trim($img);
} else {
      ob_clean();
      echo trim($endereco_copia . $nome_arquivo);
}
die();

?>