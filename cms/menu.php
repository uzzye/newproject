	<!-- Top Menu Items -->
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<?php if(verifica_logado_cms()) { ?>
		<a id="toggle_nav_btn" class="toggle-left-nav-btn inline-block mr-20 pull-left" href="javascript:void(0);"><i class="fa fa-bars"></i></a>
		<?php } ?>
		<a href="<?php echo ROOT_SERVER; ?>"><img class="brand-img pull-left" src="<?php echo ROOT; ?>_estilo/images/logo_cms.png" alt="brand"/></a>

		<?php if(verifica_logado_cms()) { ?>
		<ul class="nav navbar-right top-nav pull-right">
			<?php
			// BOTÃO PESQUISAR
			/*<li>
				<a href="javascript:void(0);" data-toggle="collapse" data-target="#site_navbar_search">
				<i class="fa fa-search top-nav-icon"></i>
				</a>
			</li>*/ ?>
			<?php
			// CONFIGURAÇÕES
			/*<li>
				<a id="open_right_sidebar" href="javascript:void(0);"><i class="fa fa-cog top-nav-icon"></i></a>
			</li>*/ ?>
			<?php 
			// APPS
			/*<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-th top-nav-icon"></i></a>
				<ul class="dropdown-menu app-dropdown" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
					<li>
						<ul class="app-icon-wrap">
							<li>
								<a href="#" class="connection-item">
								<i class="pe-7s-umbrella txt-info"></i>
								<span class="block">weather</span>
								</a>
							</li>
							<li>
								<a href="#" class="connection-item">
								<i class="pe-7s-mail-open-file txt-success"></i>
								<span class="block">e-mail</span>
								</a>
							</li>
							<li>
								<a href="#" class="connection-item">
								<i class="pe-7s-date txt-primary"></i>
								<span class="block">calendar</span>
								</a>
							</li>
							<li>
								<a href="#" class="connection-item">
								<i class="pe-7s-map txt-danger"></i>
								<span class="block">map</span>
								</a>
							</li>
							<li>
								<a href="#" class="connection-item">
								<i class="pe-7s-comment txt-warning"></i>
								<span class="block">chat</span>
								</a>
							</li>
							<li>
								<a href="#" class="connection-item">
								<i class="pe-7s-notebook"></i>
								<span class="block">contact</span>
								</a>
							</li>
						</ul>
					</li>
					<li class="divider"></li>
					<li class="text-center"><a href="#">More</a></li>
				</ul>
			</li>*/ ?>
			<?php
			// NOTIFY
			/*<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell top-nav-icon"></i><span class="top-nav-icon-badge">5</span></a>
				<ul  class="dropdown-menu alert-dropdown" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
					<li>
						<div class="streamline message-box message-nicescroll-bar">
							<div class="sl-item">
								<div class="sl-avatar avatar avatar-sm avatar-circle">
									<img class="img-responsive img-circle" src="<?php
									if(trim($_SESSION["avatarLogin"]) != "") {
										echo ROOT . UPLOAD_FOLDER . $_SESSION["avatarLogin"];
									} else {
										echo ROOT . "_estilo/images/user.png";
									}
									?>" alt="avatar"/>
								</div>
								<div class="sl-content">
									<a href="javascript:void(0)" class="inline-block capitalize-font  pull-left">Sandy Doe</a>
									<span class="inline-block font-12  pull-right">12/10/16</span>
									<div class="clearfix"></div>
									<p>Neque porro quisquam est!</p>
								</div>
							</div>
							<hr/>
							<div class="sl-item">
								<div class="icon">
									<i class="fa fa-spotify"></i>
								</div>
								<div class="sl-content">
									<a href="javascript:void(0)" class="inline-block capitalize-font  pull-left">
									2 voice mails</a>
									<span class="inline-block font-12  pull-right">2pm</span>
									<div class="clearfix"></div>
									<p>Neque porro quisquam est</p>
								</div>
							</div>
							<hr/>
							<div class="sl-item">
								<div class="icon">
									<i class="fa fa-whatsapp"></i>
								</div>
								<div class="sl-content">
									<a href="javascript:void(0)" class="inline-block capitalize-font  pull-left">8 voice messanger</a>
									<span class="inline-block font-12 pull-right">1pm</span>
									<div class="clearfix"></div>
									<p>8 texts</p>
								</div>
							</div>
							<hr/>
							<div class="sl-item">
								<div class="icon">
									<i class="fa fa-envelope"></i>
								</div>
								<div class="sl-content">
									<a href="javascript:void(0)" class="inline-block capitalize-font  pull-left">2 new messages</a>
									<span class="inline-block font-12  pull-right">1pm</span>
									<div class="clearfix"></div>
									<p>ashjs@gmail.com</p>
								</div>
							</div>
							<hr/>
							<div class="sl-item">
								<div class="sl-avatar avatar avatar-sm avatar-circle">
									<img class="img-responsive img-circle" src="<?php
									if(trim($_SESSION["avatarLogin"]) != "") {
										echo ROOT . UPLOAD_FOLDER . $_SESSION["avatarLogin"];
									} else {
										echo ROOT . "_estilo/images/user4.png";
									}
									?>" alt="avatar"/>
								</div>
								<div class="sl-content">
									<a href="javascript:void(0)" class="inline-block capitalize-font  pull-left">Sandy Doe</a>
									<span class="inline-block font-12  pull-right">1pm</span>
									<div class="clearfix"></div>
									<p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</p>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</li>*/ ?>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle pr-0" data-toggle="dropdown"><img src="<?php
				/*if(trim($_SESSION["avatarLogin"]) != "") {
					echo ROOT . UPLOAD_FOLDER . $_SESSION["avatarLogin"];
				}*/
				$objAux = new cms_usuarios();
				$objAux->inicia_dados();
				$objAux->set_var("id",intval($_SESSION["idLogin"]));
				$objAux->carrega_dados();
				if(@trim($objAux->model->upload_folders["avatar"]) != "") {
					$uploadFolder = $objAux->model->upload_folders["avatar"];
				} else {
					$uploadFolder =$objAux->model->upload_folder;
				}
				if(@trim($objAux->get_var("avatar")) != "") {
					echo $uploadFolder . $objAux->get_var("avatar");
				} else {
					echo ROOT . "_estilo/images/user1.png";
				}
				?>" alt="user_auth" class="user-auth-img img-circle"/><span class="user-online-status"></span></a>
				<ul class="dropdown-menu user-auth-dropdown" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
					<li>
						<a rel="address" href="#/cms_usuarios/perfil"><i class="fa fa-fw fa-user"></i> <?php echo get_lang("_PROFILE_EDIT"); ?></a>
					</li>
					<?php /*<li>
						<a href="#"><i class="fa fa-fw fa-credit-card-alt"></i> my balance</a>
					</li>*/ ?>
					<?php /*<li>
						<a href="#"><i class="fa fa-fw fa-envelope"></i> Mensagens</a>
					</li>*/ ?>
					<li>
						<a rel="address" href="#/cms_usuarios/senha"><i class="fa fa-fw fa-gear"></i> <?php echo get_lang("_PASSWORD_CHANGE"); ?></a>
					</li>
					<li class="divider"></li>
					<li>
						<a href="<?php echo ROOT; ?>cms_usuarios/logout"><i class="fa fa-fw fa-power-off"></i> <?php echo get_lang("_LOGOUT"); ?></a>
					</li>
				</ul>
			</li>
		</ul>
		<?php
		// PESQUISAR
		/*<div class="collapse navbar-search-overlap" id="site_navbar_search">
			<form role="search">
				<div class="form-group mb-0">
					<div class="input-search">
						<div class="input-group">	
							<input type="text" id="overlay_search" name="overlay-search" class="form-control pl-30" placeholder="Pesquisar">
							<span class="input-group-addon pr-30">
							<a  href="javascript:void(0)" class="close-input-overlay" data-target="#site_navbar_search" data-toggle="collapse" aria-label="Close" aria-expanded="true"><i class="fa fa-times"></i></a>
							</span> 
						</div>
					</div>
				</div>
			</form>
		</div>*/ ?>
		<?php } ?>
	</nav>
	<!-- /Top Menu Items -->

	<!-- Left Sidebar Menu -->
	<?php if(verifica_logado_cms()) { ?>
	<div class="fixed-sidebar-left">
		<ul class="nav navbar-nav side-nav nicescroll-bar">
			<li>
    			<a href="<?php echo ROOT_SERVER . ROOT; ?>">
    				<i class="icon-home mr-10"></i> <?php echo get_lang("_INICIO"); ?>
    			</a>
    		</li>
			<?php

			$sqlCmd = "SELECT * FROM (" . DBTABLE_CMS_MODULOS_USUARIOS . " mou)
				WHERE mou.id_usuario = \"" . $_SESSION["idLogin"] . "\"";
			$resCmd = $db->exec_query($sqlCmd);

			$refLangAux = "";
			if(trim($_SESSION["_config"]["ref_lang"]) != "pt") {
				$refLangAux = "_" . $_SESSION["_config"]["ref_lang"];
			}

			if($db->num_rows($resCmd) > 0){
				$sqlCmd = "SELECT mol.*, pai.icon_class icone_pai, pai.nome" . $refLangAux . " pai FROM (" . DBTABLE_CMS_MODULOS . " mol, " . DBTABLE_CMS_MODULOS_USUARIOS . " mou)
				LEFT JOIN " . DBTABLE_CMS_MODULOS . " pai ON pai.id = mol.modulo_pai
				WHERE mol.id = mou.id_modulo
				  AND mou.id_usuario = \"" . $_SESSION["idLogin"] . "\" 
				  AND mol.modulo_pai != 0
				  AND mol.ativo = 1
				GROUP BY mol.id 
				ORDER BY pai.id, pai.nome" . $refLangAux . ", mol.nome";
			}else{
				$sqlCmd = "SELECT mol.*, pai.icon_class icone_pai, pai.nome" . $refLangAux . " pai FROM (" . DBTABLE_CMS_MODULOS . " mol, " . DBTABLE_CMS_MODULOS_GRUPOS_USUARIOS . " mou)
				LEFT JOIN " . DBTABLE_CMS_MODULOS . " pai ON pai.id = mol.modulo_pai
				WHERE mol.id = mou.id_modulo
				  AND mou.id_grupo = \"" . $_SESSION["idGrupo"] . "\" 
				  AND mol.modulo_pai != 0
				  AND mol.ativo = 1
				GROUP BY mol.id 
				ORDER BY pai.id, pai.nome" . $refLangAux . ", mol.nome";
			}
			$resCmd = $db->exec_query($sqlCmd);		 
			
			$i=0;
			$count_menu = 0;
			$pai = $db->result_field($resCmd,0,"pai");
			$pai_anterior = "";
			$str_menu = "";

			while($i<$db->num_rows($resCmd))				
			{
				$pai = $db->result_field($resCmd,$i,"pai");
				$nome = $db->result_field($resCmd,$i,"nome");
				if(trim($_SESSION["_config"]["ref_lang"]) != "pt") {
					$nome = $db->result_field($resCmd,$i,"nome_" . $_SESSION["_config"]["ref_lang"]);
				}
				$id = $db->result_field($resCmd,$i,"id");
				$link = $db->result_field($resCmd,$i,"link");
				$icon = $db->result_field($resCmd,$i,"icon_class");
				$icone_pai = $db->result_field($resCmd,$i,"icone_pai");
				
				if($pai != $pai_anterior)
				{	
					$str_menu = str_replace("{{ativo}}","",$str_menu);
					$str_menu = str_replace("{{count}}",$count_menu,$str_menu);

					$count_menu = 0;
					
					if($i>0)
					{
						$str_menu .= "</ul>
							</li>";
					}

					if(trim($icone_pai) == "") {
						$icone_pai = "icon-arrow-right-circle";
					}
			
	        		$str_menu .= "<li>
						<a href=\"javascript:void(0);\" data-toggle=\"collapse\" data-target=\"#menu_" . $id . "\">
							<i class=\"" . $icone_pai . " mr-10\"></i>" . get_output($pai) . " <span class=\"pull-right\"><span class=\"label label-success mr-10\">{{count}}</span><i class=\"fa fa-fw fa-angle-down\"></i></span>
						</a>

	        			<ul id=\"menu_" . $id . "\" class=\"collapse collapse-level-1 {{ativo}}\">";                		
				}
				
				if($modulo == $link)
				{
					$str_menu = str_replace("{{ativo}}"," ativo",$str_menu);
				}
			                                
	            $str_menu .= "<li";
	            if($link == $modulo)
	            {
	            	$str_menu .= " class=\"ativo\"";
	            }

				if(trim($icon) == "") {
					$icon = "icon-arrow-right-circle";
				}

	            $str_menu .= ">
					<a rel=\"address\" href=\"#/" . gera_titulo_amigavel(get_output($link)) . "\">
						<i class=\"" . $icon . " mr-10\"></i>" . stripslashes(get_output($nome)) . "
					</a>
				</li>";
	        
			    $pai_anterior = $pai;
				$i++;
				$count_menu++;
			}
					
			$str_menu = str_replace("{{count}}",$count_menu,$str_menu);
			
	        $str_menu .= "</ul>
	        	</li>";
	        
	        if(strstr($_SESSION["emailLogin"],"@uzzye.com"))
	        { 
	        	$str_menu .= "<li>
					<a href=\"javascript:void(0);\" data-toggle=\"collapse\" data-target=\"#menu_tools\">
						<i class=\"icon-wrench mr-10\"></i>" . get_lang("_TOOLS") . " <span class=\"pull-right\"><span class=\"label label-success mr-10\">1</span><i class=\"fa fa-fw fa-angle-down\"></i></span>
					</a>

					<ul id=\"menu_tools\" class=\"collapse collapse-level-1\">
		        		<li>
		        			<a rel=\"address\" href='" . ROOT_SERVER . ROOT . "#/ferramentas/mvc'>
		        				<i class=\"icon-layers mr-10\"></i> " . get_lang("_MVC_MANAGER") . "
		        			</a>
		        		</li>
		        	</ul>
				</li>";
	        }

	        $str_menu = str_replace("{{ativo}}","",$str_menu);
	        echo $str_menu;

	        ?>
			<?php
			// MÚLTIPLOS MENUS
			/*<li>
				<a href="javascript:void(0);" data-toggle="collapse" data-target="#app_dr"><i class="icon-grid mr-10"></i>Apps <span class="pull-right"><span class="label label-info mr-10">9</span><i class="fa fa-fw fa-angle-down"></i></span></a>
				<ul id="app_dr" class="collapse collapse-level-1">
					<li>
						<a href="chats.html">chats</a>
					</li>
					<li>
						<a href="calendar.html">calendar</a>
					</li>
					<li>
						<a href="weather.html">weather</a>
					</li>
					<li>
						<a href="javascript:void(0);" data-toggle="collapse" data-target="#email_dr">Email<span class="pull-right"><i class="fa fa-fw fa-angle-down"></i></span></a>
						<ul id="email_dr" class="collapse">
							<li>
								<a href="inbox.html">inbox</a>
							</li>
							<li>
								<a href="inbox-detail.html">detail email</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="javascript:void(0);" data-toggle="collapse" data-target="#contact_dr">Contacts<span class="pull-right"><i class="fa fa-fw fa-angle-down"></i></span></a>
						<ul id="contact_dr" class="collapse">
							<li>
								<a href="contact-list.html">list</a>
							</li>
							<li>
								<a href="contact-card.html">cards</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="file-manager.html">File Manager</a>
					</li>
					<li>
						<a href="todo-tasklist.html">To Do/Tasklist</a>
					</li>
				</ul>
			</li>*/ ?>
			<?php
			// SIMPLES MENU
			/*<li>
				<a href="widgets.html"><i class="icon-drawar mr-10"></i>widgets</a>
			</li>*/ ?>
		</ul>
	</div>
	<!-- /Left Sidebar Menu -->
	<?php } ?>
	
	<?php
	// MENU DIREITO
	/*<!-- Right Sidebar Menu -->
	<div class="fixed-sidebar-right">
		<ul class="right-sidebar nicescroll-bar">
			<li>
				<div  class="tab-struct custom-tab-1">
					<ul role="tablist" class="nav nav-tabs" id="right_sidebar_tab">
						<li class="active" role="presentation"><a aria-expanded="true"  data-toggle="tab" role="tab" id="chat_tab_btn" href="#chat_tab">chat</a></li>
						<li role="presentation" class=""><a  data-toggle="tab" id="messages_tab_btn" role="tab" href="#messages_tab" aria-expanded="false">messages</a></li>
						<li role="presentation" class=""><a  data-toggle="tab" id="todo_tab_btn" role="tab" href="#todo_tab" aria-expanded="false">todo</a></li>
					</ul>
					<div class="tab-content" id="right_sidebar_content">
						<div  id="chat_tab" class="tab-pane fade active in" role="tabpanel">
							<div class="chat-cmplt-wrap">
								<div class="chat-box-wrap">
									<form role="search">
										<div class="input-group mb-15">
											<input type="text" id="example-input1-group2" name="example-input1-group2" class="form-control" placeholder="Search">
											<span class="input-group-btn">
											<button type="button" class="btn  btn-default"><i class="fa fa-search"></i></button>
											</span>
										</div>
									</form>
									<ul class="chat-list-wrap">
										<li class="chat-list">
											<div class="chat-body">
												<a  href="javascript:void(0)">
													<div class="chat-data">
														<img class="user-img img-circle"  src="dist/img/user.png" alt="user"/>
														<div class="user-data">
															<span class="name block capitalize-font">ryan gosling</span>
															<span class="time block txt-grey">2pm</span>
														</div>
														<div class="status away"></div>
														<div class="clearfix"></div>
													</div>
												</a>
												<a  href="javascript:void(0)">
													<div class="chat-data">
														<img class="user-img img-circle"  src="dist/img/user1.png" alt="user"/>
														<div class="user-data">
															<span class="name block capitalize-font">ryan gosling</span>
															<span class="time block txt-grey">1pm</span>
														</div>
														<div class="status offline"></div>
														<div class="clearfix"></div>
													</div>
												</a>
												<a  href="javascript:void(0)">
													<div class="chat-data">
														<img class="user-img img-circle"  src="dist/img/user2.png" alt="user"/>
														<div class="user-data">
															<span class="name block capitalize-font">ryan gosling</span>
															<span class="time block txt-grey">2pm</span>
														</div>
														<div class="status online"></div>
														<div class="clearfix"></div>
													</div>
												</a>
												<a  href="javascript:void(0)">
													<div class="chat-data">
														<img class="user-img img-circle"  src="dist/img/user3.png" alt="user"/>
														<div class="user-data">
															<span class="name block capitalize-font">ryan gosling</span>
															<span class="time block txt-grey">2pm</span>
														</div>
														<div class="status online"></div>
														<div class="clearfix"></div>
													</div>
												</a>
												<a  href="javascript:void(0)">
													<div class="chat-data">
														<img class="user-img img-circle"  src="dist/img/user4.png" alt="user"/>
														<div class="user-data">
															<span class="name block capitalize-font">ryan gosling</span>
															<span class="time block txt-grey">2pm</span>
														</div>
														<div class="status online"></div>
														<div class="clearfix"></div>
													</div>
												</a>
											</div>
										</li>
									</ul>
								</div>
								<div class="recent-chat-box-wrap">
									<div class="panel panel-success card-view">
										<div class="panel-heading mb-20">
											<a class="goto-chat-list txt-light" href="javascript:void(0);"><i class="ti-angle-left"></i></a>
											<div class="text-center">
												<h6 class="panel-title txt-light">Alan Gilchrist</h6>
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="panel-wrapper collapse in">
											<div class="panel-body">
												<div class="chat-content">
													<ul>
														<li class="friend">
															<div class="friend-msg-wrap">
																<img class="user-img img-circle block pull-left"  src="dist/img/user.png" alt="user"/>
																<div class="msg pull-left">A forest is a large area of land covered with trees
																	<div class="msg-per-detail mt-5">
																		<span class="msg-per-name pr-5 txt-success">ryan</span>
																		<span class="msg-time txt-grey">2:30pm</span>
																	</div>
																</div>
																<div class="clearfix"></div>
															</div>	
														</li>
														<li class="self">
															<div class="self-msg-wrap">
																<div class="msg block pull-right"> Provide ecosystem...
																	<div class="msg-per-detail mt-5">
																		<span class="msg-time txt-grey">2:30pm</span>
																	</div>
																</div>
																<div class="clearfix"></div>
															</div>	
														</li>
														<li class="friend">
															<div class="friend-msg-wrap">
																<img class="user-img img-circle block pull-left"  src="dist/img/user.png" alt="user"/>
																<div class="msg pull-left"> Account for 75% of the gross primary productivity of them Earth's biosphere
																	<div class="msg-per-detail mt-5">
																		<span class="msg-per-name pr-5 txt-success">ryan</span>
																		<span class="msg-time txt-grey">2:30pm</span>
																	</div>
																</div>
																<div class="clearfix"></div>
															</div>	
														</li>
													</ul>
												</div>
												<div class="input-group">
													<div class="input-group-btn">
														<div class="dropup">
															<button type="button" class="btn  btn-default  dropdown-toggle" data-toggle="dropdown" ><i class="fa fa-smile-o"></i></button>
															<ul class="dropdown-menu dropdown-menu-right">
																<li><a href="javascript:void(0)">Action</a></li>
																<li><a href="javascript:void(0)">Another action</a></li>
																<li class="divider"></li>
																<li><a href="javascript:void(0)">Separated link</a></li>
															</ul>
														</div>
													</div>
													
													<input type="text" id="input_msg_send" name="send-msg" class="form-control" placeholder="Type something">
													<div class="input-group-btn">
														<div class="fileupload btn  btn-default"><i class="fa fa-paperclip"></i>
															<input type="file" class="upload">
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div id="messages_tab" class="tab-pane fade" role="tabpanel">
							<div class="message-box-wrap">
								<div class="streamline message-box">
									<div class="sl-item">
										<div class="sl-avatar avatar avatar-sm avatar-circle">
											<img class="img-responsive img-circle" src="dist/img/user.png" alt="avatar"/>
										</div>
										<div class="sl-content">
											<a href="javascript:void(0)" class="inline-block capitalize-font  mb-5 pull-left">Sandy Doe</a>
											<span class="inline-block font-12 mb-5 pull-right">12/10/16</span>
											<div class="clearfix"></div>
											<p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</p>
										</div>
									</div>
									<hr/>
									<div class="sl-item">
										<div class="sl-avatar avatar avatar-sm avatar-circle">
											<img class="img-responsive img-circle" src="dist/img/user1.png" alt="avatar"/>
										</div>
										<div class="sl-content">
											<a href="javascript:void(0)" class="inline-block capitalize-font  mb-5 pull-left">Sandy Doe</a>
											<span class="inline-block font-12 mb-5 pull-right">2pm</span>
											<div class="clearfix"></div>
											<p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</p>
										</div>
									</div>
									<hr/>
									<div class="sl-item">
										<div class="sl-avatar avatar avatar-sm avatar-circle">
											<img class="img-responsive img-circle" src="dist/img/user2.png" alt="avatar"/>
										</div>
										<div class="sl-content">
											<a href="javascript:void(0)" class="inline-block capitalize-font  mb-5 pull-left">Sandy Doe</a>
											<span class="inline-block font-12 mb-5 pull-right">1pm</span>
											<div class="clearfix"></div>
											<p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</p>
										</div>
									</div>
									<hr/>
									<div class="sl-item">
										<div class="sl-avatar avatar avatar-sm avatar-circle">
											<img class="img-responsive img-circle" src="dist/img/user3.png" alt="avatar"/>
										</div>
										<div class="sl-content">
											<a href="javascript:void(0)" class="inline-block capitalize-font  mb-5 pull-left">Sandy Doe</a>
											<span class="inline-block font-12 mb-5 pull-right">1pm</span>
											<div class="clearfix"></div>
											<p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</p>
										</div>
									</div>
									<hr/>
									<div class="sl-item">
										<div class="sl-avatar avatar avatar-sm avatar-circle">
											<img class="img-responsive img-circle" src="dist/img/user4.png" alt="avatar"/>
										</div>
										<div class="sl-content">
											<a href="javascript:void(0)" class="inline-block capitalize-font  mb-5 pull-left">Sandy Doe</a>
											<span class="inline-block font-12 mb-5 pull-right">1pm</span>
											<div class="clearfix"></div>
											<p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div  id="todo_tab" class="tab-pane fade" role="tabpanel">
							<div class="todo-box-wrap">
								<!-- Todo-List -->
								<ul class="todo-list">
									<li class="todo-item">
										<div class="checkbox checkbox-default">
											<input type="checkbox" id="checkbox01"/>
											<label for="checkbox01">Record The First Episode Of HTML Tutorial</label>
										</div>
									</li>
									<li class="todo-item">
										<div class="checkbox checkbox-pink">
											<input type="checkbox" id="checkbox02"/>
											<label for="checkbox02">Prepare The Conference Schedule</label>
										</div>
									</li>
									<li class="todo-item">
										<div class="checkbox checkbox-warning">
											<input type="checkbox" id="checkbox03" checked/>
											<label for="checkbox03">Decide The Live Discussion Time</label>
										</div>
									</li>
									<li class="todo-item">
										<div class="checkbox checkbox-success">
											<input type="checkbox" id="checkbox04" checked/>
											<label for="checkbox04">Prepare For The Next Project</label>
										</div>
									</li>
									<li class="todo-item">
										<div class="checkbox checkbox-danger">
											<input type="checkbox" id="checkbox05" checked/>
											<label for="checkbox05">Finish Up AngularJs Tutorial</label>
										</div>
									</li>
									<li class="todo-item">
										<div class="checkbox checkbox-purple">
											<input type="checkbox" id="checkbox06" checked/>
											<label for="checkbox06">Finish Infinity Project</label>
										</div>
									</li>
								</ul>
								<!-- /Todo-List -->
							</div>
						</div>
					</div>
				</div>
			</li>
		</ul>
	</div>
	<!-- /Right Sidebar Menu -->*/ ?>

<?php

?>