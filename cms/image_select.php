<?php

include_once("config.php");

include_once("verificaLogado.php");

carrega_classe("cms_imagens");
$imgs = new cms_imagens();

$upload_folder = UPLOAD_FOLDER;
if(@trim($imgs->model->upload_folders["arquivo"]) != "") {
  $upload_folder = $imgs->model->upload_folders["arquivo"];
} else if(@trim($imgs->model->upload_folder) != "") {
  $upload_folder = $imgs->model->upload_folder;
}

$imgs = $imgs->get_array_ativos_controllers();

?>
<div class="col-sm-12">
<?php

if(is_array($imgs) && sizeof($imgs) > 0) {
  ?>
  <div class="row imagens-select">
  <?php
  //for($w=0;$w<100;$w++)
  {foreach($imgs as $img) {
    $imgTag = "";


    if(trim($img->get_var("arquivo")) != "") {
      list($width, $height, $type, $attr) = @getimagesize($upload_folder . get_output($img->get_var("arquivo")));
      $classAux = "";
      if($width >= $height) {
        $classAux = "wide";
      } else if($width < $height) {
        $classAux = "tall";
      }
      $imgTag = str_replace("<img", "<img class=\"center middle " . $classAux . "\"", get_output($img->get_var_format("arquivo")));
    }

    if(trim($imgTag) != "") {
      ?>
      <div class="col-sm-3 col-xs-6 mb-10">
        <a class="imagem-select square" data-dismiss="modal">
          <?php echo $imgTag; ?>
        </a>
      </div>
      <?php
    }
  }}
  ?>
  </div>
  <?php
} else {
  ?>
  <div class="row">
      <div class="col-sm-12">
        Nenhum item encontrado no Banco de Imagens.
      </div>
  </div>
  <?php
}

?>
</div>