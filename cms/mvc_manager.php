<?php

include_once("config.php");

include_once("verificaLogado.php");

$class_aux = "model";
$model_keys = get_class_vars($class_aux);
$model_keys = array_keys($model_keys);

?>
<script type="text/javascript" >    
    $(document).ready(function(e){
        $(".btn-submit").unbind("click").click(function(e){
            e.preventDefault();
            e.stopPropagation();
            swal({   
                title: "Você tem certeza?",   
                text: "Confirmar o processamento da tabela implicará na substituição de arquivos previamente criados para ela.",
                type: "warning",  
                showCancelButton: true,   
                confirmButtonColor: "#fcb03b",   
                confirmButtonText: "Sim, processar!",   
                cancelButtonText: "Não, cancelar!",   
                closeOnConfirm: true,   
                closeOnCancel: true 
            }, function(isConfirm){   
                if (isConfirm) {  
                    $("#form2").trigger("submit");
                    return true;
                } else {
                   //swal("Cancelado", "Nenhum arquivo foi removido.", "error");   
                   return false;
                } 
            });
        });

        $(".btn-back").unbind("click").click(function(e){
            e.preventDefault();
            e.stopPropagation();
            swal({   
                title: "Você tem certeza?",   
                text: "Iremos ignorar as alterações não salvas e voltar ao início do CMS.",
                type: "warning",  
                showCancelButton: true,   
                confirmButtonColor: "#fcb03b",   
                confirmButtonText: "Sim, ignorar!",   
                cancelButtonText: "Não, ficar!",   
                closeOnConfirm: false,   
                closeOnCancel: true 
            }, function(isConfirm){   
                if (isConfirm) {  
                    showLoading(iTimeAuxFade);
                    window.location = ROOT_SERVER + ROOT;
                    return true;
                } else {
                   //swal("Cancelado", "Nenhum arquivo foi removido.", "error");   
                   return false;
                } 
            });
        });

        $(".formAjax").unbind("submit").submit(function(e){
            showLoading(iTimeAuxFade);

            var formAux = $(this);
            $.ajax({
               url : formAux.data("url-post"),
               type : 'POST',
               data : formAux.serialize(),
               success : function(data) {
                    $("#cp-main").html(data);

                    if(AJAXCallback){
                        AJAXCallback(e);
                    } 

                    if(atualizaConteudoInterna){
                        atualizaConteudoInterna(e);
                    }

                    document.body.scrollTop = document.documentElement.scrollTop = 0;
                   
                    hideLoading(iTimeAuxFade);
               }
            });
        });

        $("#select-table").on("change", function(e){
            e.stopPropagation();
            $("#formMVCMan1").trigger("submit");
        });

        $(".mvc-table").rowSorter({
            handler: "td.sorter",
            tbody: true
        });
    });

    function setFieldType(type,field)
    {
        if(document.getElementById('img_group_' + field))
        {
            document.getElementById('img_group_' + field).selectedIndex = 0;
            document.getElementById('img_group_' + field).style.display = 'none';
            if(type == "ImageCrop")
            {
                document.getElementById('img_group_' + field).style.display = '';
            }
        }

        if(document.getElementById('ref_models_' + field))
        {
            document.getElementById('ref_models_' + field).value = "";
            document.getElementById('ref_models_' + field).style.display = 'none';
            if(type == "ListBox")
            {
                document.getElementById('ref_models_' + field).style.display = '';
            }
        }

        if(document.getElementById('data_tables_' + field))
        {
            document.getElementById('data_tables_' + field).value = "";
            document.getElementById('data_tables_' + field).style.display = 'none';
            if(type == "DataTable")
            {
                document.getElementById('data_tables_' + field).style.display = '';
            }
        }
    }
</script>

<!-- Title -->
<div class="row heading-bg bg-<?php echo $projectColors["main"]; ?>">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h5 class="txt-light">
            <i class="icon-layers mr-10"></i><span class="btn-text">Gerenciador MVC</span>
        </h5>
    </div>

    <!-- Breadcrumb -->
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
        </ol>
    </div>
    <!-- /Breadcrumb -->
</div>
<!-- /Title -->

<!-- Row -->
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default card-view">

<?php

if($_REQUEST["acao"] == "procTable")
{
    $sqlCmd = "DESCRIBE " . $_REQUEST["table"];
    $resCmd = $db->exec_query($sqlCmd);
    $array_aux = $db->result_object_array($resCmd);
    
    $name = substr(str_replace($mySQL_prefix,"",$_REQUEST["table"]),1);
    $nome_tabela_aux = "DBTABLE_" . strtoupper($name);
    
    $array_lang = array();

// PROCESSA PHP DO MODEL
    $str_fields = "";
    
    $str_fk_includes_model = "";
    $str_fk_includes_controller = "";
    $str_fk_includes_view = "";
    
    $str_fields_view = "";
    $str_labels_view = "";
    $str_fk_view = "";
    $str_exp_view = "";   
    $str_where_filtros = "";
    $str_exp_sql = "";
    
    $array_str_images_to_crop = array();
    $array_count_images_to_crop = array();
    
    $str_req = "";    
    $str_pks = "";
    $str_fks = "";   
    $str_ref_models = "";
    $str_data_tables = ""; 

    $str_file = "<?php
#FK_INCLUDES#
class " . $name . "_model extends model{\r\n\r\n";
        
    foreach($array_aux as $obj)
    {
        if(!in_array($obj->Field,$model_keys,true))
        {
            $label_aux = "_" . strtoupper(str_replace(" ","_",remove_acentos(remove_caracteres_especiais($_REQUEST["label_" . $obj->Field]))));
            if(trim($_REQUEST["label_" . $obj->Field]) == "")
            {
                $label_aux = "\"\"";
            }
            
            $const_obj = "_" . strtoupper(str_replace(" ","_",remove_acentos(remove_caracteres_especiais($_REQUEST["label_" . $obj->Field]))));
            $array_lang[sizeof($array_lang)] = array($const_obj,$_REQUEST["label_" . $obj->Field]);  
            
            // ADICIONA CAMPOS
            $str_fields .= "        public \$" . $obj->Field . ";\r\n";
            if(intval($_REQUEST["list_" . $obj->Field]) == 1)
            {
                $str_fields_view .= "\"" . $obj->Field . "\",";
                if(trim($_REQUEST["label_" . $obj->Field]) <> "")
                {                           
                    $str_labels_view .= "get_lang(\"_" . strtoupper(str_replace(" ","_",remove_acentos(remove_caracteres_especiais($_REQUEST["label_" . $obj->Field])))) . "\"),";
                }
                else
                {
                    $str_labels_view .= "\"\",";
                }
            }
            
            // ADICIONA CAMPOS OBRIGATORIOS
            if($_REQUEST["null_" . $obj->Field] == "NO")
            {
                if(trim($str_req) <> "")
                {
                    $str_req .= ",";
                }
                $str_req .= "\"" . $obj->Field . "\"";
            }
            
            // ADICIONA CAMPOS DE ARQUIVO OU IMAGEM
            $class_aux = "TextField";
            if(trim($_REQUEST["type_" . $obj->Field]) <> "")
            {
                $class_aux = $_REQUEST["type_" . $obj->Field];
            }
            if(trim($class_aux) == "FileField")
            {
                $str_file_fields .= "\"" . $obj->Field . "\",";
            }
            else if(trim($class_aux) == "ImageCrop")
            {
                $str_image_fields .= "\"" . $obj->Field . "\",";
                $field_aux = $obj->Field;
                if(trim($_REQUEST["img_group_" . $obj->Field]) <> "")
                {
                    $field_aux = $_REQUEST["img_group_" . $obj->Field]; 
                }               
                if(trim($array_str_images_to_crop[$field_aux]) <> "")
                {$array_str_images_to_crop[$field_aux] .= ",";}
                //$array_str_images_to_crop[$field_aux] .= "array(\"" . $obj->Field . "\",get_lang(\"$label_aux\"),\$this->controller->get_var(\"" . $obj->Field . "\"),0,0)";              
                $array_str_images_to_crop[$field_aux] .= "array(\"" . $obj->Field . "\",get_lang(\"$label_aux\"),0,0,0)";
                $array_count_images_to_crop[$field_aux]++;
            }

            // ADICIONA CHAVE ESTRANGEIRA
            if(trim($_REQUEST["fk_table_" . $obj->Field]) <> "")
            {
                $modulo_aux = substr(str_replace($mySQL_prefix,"",$_REQUEST["fk_table_" . $obj->Field]),1);
                $str_fks .= "\$this->foreign_keys[sizeof(\$this->foreign_keys)] = array(\"" . $obj->Field . "\",\"" . $modulo_aux . "\",\"" . $_REQUEST["fk_field_" . $obj->Field] . "\");\r\n";                
                $str_fk_includes_model .= "include_once(ROOT_CMS . \"classes/models/" . $modulo_aux . "_model.php\");\r\n";
                $str_fk_includes_controller .= "include_once(ROOT_CMS . \"classes/controllers/" . $modulo_aux . "_controller.php\");\r\n";
                $str_fk_includes_view .= "include_once(ROOT_CMS . \"classes/views/" . $modulo_aux . "_view.php\");\r\n";
                $str_fk_view .= "\"" . $_REQUEST["fk_exib_field_" . $obj->Field] . "\",";
            }   
            else
            {
                $str_fk_view .= "\"\",";
            } 

            // ADICIONA REFERENCE MODEL
            if(trim($_REQUEST["ref_models_" . $obj->Field]) <> "")
            {
                $RMAux = "";
                $arrAux = explode(",",$_REQUEST["ref_models_" . $obj->Field]);
                foreach($arrAux as $it) {
                    $RMAux .= ",\"" . $it . "\"";
                }
                $modulo_aux = $arrAux[0];
                unset($it);
                $str_ref_models .= "\$this->reference_models[sizeof(\$this->reference_models)] = array(\"" . $obj->Field . "\"" . $RMAux . ");\r\n"; 
                $str_ref_models .= "\$this->foreign_keys[sizeof(\$this->foreign_keys)] = array(\"" . $obj->Field . "\"" . $RMAux . ");\r\n";               
                $str_fk_includes_model .= "include_once(ROOT_CMS . \"classes/models/" . $modulo_aux . "_model.php\");\r\n";
                $str_fk_includes_controller .= "include_once(ROOT_CMS . \"classes/" . $modulo_aux . ".php\");\r\n";
                $str_fk_includes_view .= "include_once(ROOT_CMS . \"classes/" . $modulo_aux . ".php\");\r\n";
            }   

            // ADICIONA DATA TABLES MODEL
            if(trim($_REQUEST["data_tables_" . $obj->Field]) <> "")
            {
                $RMAux = "";
                $arrAux = explode(",",$_REQUEST["data_tables_" . $obj->Field]);
                $iCount = 0;
                foreach($arrAux as $it) {
                    if($iCount > 0) {$RMAux .= ",";}
                    $RMAux .= "\"" . $it . "\"";
                    $iCount++;
                }
                $modulo_aux = $arrAux[0];
                unset($it);
                $str_data_tables .= "\$this->data_tables[sizeof(\$this->data_tables)] = array(" . $RMAux . ");\r\n";                
                $str_fk_includes_model .= "include_once(ROOT_CMS . \"classes/models/" . $modulo_aux . "_model.php\");\r\n";
                $str_fk_includes_controller .= "include_once(ROOT_CMS . \"classes/" . $modulo_aux . ".php\");\r\n";
                $str_fk_includes_view .= "include_once(ROOT_CMS . \"classes/" . $modulo_aux . ".php\");\r\n";
            }  
            
            $str_exp_view .= "\"\",";
        }                           
    }
    
    // TIRA A "," NO FINAL DAS STRINGS QUE CONTEM ARRAYS E ESCREVE-OS
    $str_labels_view = substr($str_labels_view,0,-1);
    $str_fields_view = substr($str_fields_view,0,-1);
    $str_exp_view = substr($str_exp_view,0,-1);
    $str_fk_view = substr($str_fk_view,0,-1);
    $str_file_fields = substr($str_file_fields,0,-1);
    $str_image_fields = substr($str_image_fields,0,-1);
    //$str_where_filtros = substr($str_exp_view,0,-1);
    //$str_exp_sql = substr($str_exp_view,0,-1);
    
    if(trim($str_req) <> "")
    {
        $str_req = "\$this->array_required_fields = array(" . $str_req . ");\r\n";
    }
    if(trim($str_file_fields) <> "")
    {
        $str_file_fields = "\$this->array_file_fields = array(" . $str_file_fields . ");\r\n";
    }
    if(trim($str_image_fields) <> "")
    {
        $str_image_fields = "           \$this->array_crop_fields = array(" . $str_image_fields . ");\r\n";
    }
    
    // ADICIONA STRINGS
    $str_file .= $str_fields;
    $str_file .= "\r\n      function __construct(){
            // Instancia o Objeto
            \$this->nome_tabela = " . $nome_tabela_aux. ";
            " . $str_fks . "" . $str_ref_models . "" . $str_data_tables . "" . $str_req . "" . $str_file_fields . "" . $str_image_fields . "            
            parent::__construct();\r\n";
    $str_file .= "      }\r\n";
    
    // KEYS
    
    $str_file .= "}
    
?>";
    
    $str_file = str_replace("#FK_INCLUDES#",$str_fk_includes_model,$str_file);
    
    $filepath = "classes/models/" . $name . "_model.php";    
    ////if(file_exists($filepath))
    {
        //@unlink($filepath);
    }    
    /*$fp = fopen($filepath, "w");
    fwrite($fp, $str_file);
    fclose($fp);*/
    file_put_contents($filepath, $str_file);
    
// PROCESSA PHP DO CONTROLLER
    $str_fields = "";
    $str_pks = "";
    $str_fks = "";    
    $str_ref_models = "";
    $str_data_tables = "";
    $str_file = "<?php
#FK_INCLUDES#
class " . $name . "_controller extends controller{
  
    function __construct(){ 
        \$this->nome = \"" . $name . "\";
        
        parent::__construct();
    }
    
    function acaoPadrao()
    {
        // escrever aqui a acao padrao para o front-end, caso ela não seja definida, senão executa a acaoPadrao do parent
        parent::acaoPadrao();
    }
    
    function lista_registros()
    {
        parent::lista_registros();
        
        \$array_campos = array(";    
        $str_file .= $str_fields_view;          
        $str_file .= ");
        \$array_nomes = array(";
        $str_file .= $str_labels_view;
        $str_file .= ");            
        \$array_foreign_keys = array(";
        $str_file .= $str_fk_view;
        $str_file .= ");
        \$array_expressoes = array(";
        $str_file .= $str_exp_view;
        $str_file .= ");
        \$array_where_filtros = array(";
        $str_file .= $str_where_filtros;
        $str_file .= ");
        \$array_expressoes_sql = array(";
        $str_file .= $str_exp_sql;
        $str_file .= ");
        
        \$this->view->lista(\$array_campos,\$array_nomes,\$array_expressoes,\"\",\"\",\$array_foreign_keys,\$array_where_filtros,\$array_condicoes_acoes,\$array_expressoes_sql);
    }   
}
?>";
    
    $str_file = str_replace("#FK_INCLUDES#",$str_fk_includes_controller,$str_file);
    
    $filepath = "classes/controllers/" . $name . "_controller.php";    
    ////if(file_exists($filepath))
    {
        //@unlink($filepath);
    }    
    /*$fp = fopen($filepath, "w");
    fwrite($fp, $str_file);
    fclose($fp);*/
    file_put_contents($filepath, $str_file);
    
    $constante_nomePlural = "_" . strtoupper(str_replace(" ","_",remove_acentos(remove_caracteres_especiais($_REQUEST["nomePlural"]))));
    $constante_nomeSingular = "_" . strtoupper(str_replace(" ","_",remove_acentos(remove_caracteres_especiais($_REQUEST["nomeSingular"]))));
    $constante_tituloMenu = "_" . strtoupper(str_replace(" ","_",remove_acentos(remove_caracteres_especiais($_REQUEST["tituloMenu"]))));
    
// PROCESSA PHP DA CLASSE GENÉRICA
    $str_fields = "";
    $str_pks = "";
    $str_fks = "";    
    $str_ref_models = "";
    $str_data_tables = "";
    $str_file = "<?php
    
include_once(ROOT_CMS . \"classes/controllers/" . $name . "_controller.php\");

class " . $name . " extends " . $name . "_controller{
  
    function __construct(){
        parent::__construct();
    }
    
    function acaoPadrao()
    {
        parent::acaoPadrao();
    }
    
    function lista_registros()
    {
        parent::lista_registros();      
    }   
}
?>";
    
    $filepath = "classes/" . $name . ".php";    
    ////if(file_exists($filepath))
    {
        //@unlink($filepath);
    }    
    /*$fp = fopen($filepath, "w");
    fwrite($fp, $str_file);
    fclose($fp);*/
    file_put_contents($filepath, $str_file);
        
// PROCESSA PHP DO VIEW
    $str_fields = "";
    $str_pks = "";
    $str_fks = "";
    $str_ref_models = "";
    $str_data_tables = "";
    $str_file = "";
    $str_file .= "<?php    
#FK_INCLUDES#   
class " . $name . "_view extends view
{     
        function __construct(\$owner = null)
        {
            global \$modulo;

            \$this->nome = \"" . $name . "\";
            if(function_exists(\"get_lang\")) {
                \$this->nome_exibicao = get_lang(\"" . $constante_nomePlural . "\");
                \$this->nome_exibicao_singular = get_lang(\"" . $constante_nomeSingular . "\");
            }
        
            parent::__construct(\$owner);       
        }   
    
        function monta_campos_form(\$id = \"\", \$readonly = false, \$clone = false, \$resumido = false)
        {
            global \$modulo;
        
            \$b_editando = false;       
            if(trim(\$_REQUEST[\"id\"]) <> \"\")
            {
                \$id = \$_REQUEST[\"id\"];
                \$this->controller->set_var(\"id\",\$id);
                \$this->controller->carrega_dados();
                \$b_editando = true;
            }
        
            \$array_form_campos = array();
        
            \$count_fields = 0;
                    
    ";

    foreach($array_aux as $obj)
    {
        if(trim($_REQUEST["type_" . $obj->Field]) <> ""){
        
        if(!in_array($obj->Field,$model_keys,true))
        {
            $str_file .= "      \$ref = \"" . $obj->Field . "\";\r\n";
            
            //ADICIONA LABELS
            //$label_aux = $_REQUEST["label_" . $obj->Field];
            $label_aux = "_" . strtoupper(str_replace(" ","_",remove_acentos(remove_caracteres_especiais($_REQUEST["label_" . $obj->Field]))));
            if(trim($_REQUEST["label_" . $obj->Field]) == "")
            {
                $label_aux = "\"\"";
            }
                
            // ADICIONA INPUTS
            $class_aux = "TextField";
            if(trim($_REQUEST["type_" . $obj->Field]) <> "")
            {
                $class_aux = $_REQUEST["type_" . $obj->Field];
            }
            
            // ADICIONA READONLY
            $readonly_aux = "";
            if(intval($_REQUEST["readonly_" . $obj->Field]) == 1)
            {
                $readonly_aux = "\r\n           \$inputAux->readonly = true;\r\n";  
            }
            
            // SE FOI COMBO
            if(trim($class_aux) == "ComboBox")
            {
                $modulo_aux = substr(str_replace($mySQL_prefix,"",$_REQUEST["fk_table_" . $obj->Field]),1);
                
                $str_file .= "          \$checked = \$this->controller->get_var(\$ref);
            if(trim(\$_REQUEST[\$ref]) <> \"\")
            {\$checked = \$_REQUEST[\$ref];}
            \$inputAux = new Uzzye_" . $class_aux . "(\$ref,\$ref,get_lang(\"" . $label_aux . "\"));
            \$inputAux->default_display = get_lang(\"_COMBOSELECIONE\");
            \$inputAux->default_value = 0;
            \$inputAux->checked_value = \$checked;" . $readonly_aux . "
            \$inputAux->size = 1;
            \$inputAux->multiple = false;\r\n";
//TODO              
                // SE POSSUI CHAVE ESTANGEIRA MONTA ARRAY DO MODEL
                if(trim($_REQUEST["fk_field_" . $obj->Field]) <> "")
                {
                    $str_file .= "          \$classAux = \"" . $modulo_aux . "_controller\";
            \$objAux = new \$classAux();
            \$inputAux->set_options(\$objAux->get_array_options(\"" . $_REQUEST["fk_field_" . $obj->Field] . "\",\"" . $_REQUEST["fk_exib_field_" . $obj->Field] . "\",\"\",\"" . $_REQUEST["fk_exib_field_" . $obj->Field] . " ASC\"));
            ";
                }
                // SE FOR CAMPO SET, MONTA OPCOES COM VALOR E INNERHTML IGUAIS
                else if(substr($obj->Type,0,3) == "set")
                {
                    $array_options = array();
                    $array_options = explode(",",substr($obj->Type,4,-1));
                    $str_file .= "          \$inputAux->set_options(array(";
                    for ($w=0;$w<sizeof($array_options);$w++)
                    {
                        //$array_options[$w] = substr($array_options[$w],1,-1);
                        $array_options[$w] = str_replace("'","",$array_options[$w]);
                        
                        if($w>0){
                            $str_file .= ",";
                        }
                        $str_file .= "array(output_decode(\"" . output_encode($array_options[$w]) . "\"),output_decode(\"" . output_encode($array_options[$w]) . "\"))";
                    }
                    $str_file .= "));
            ";
                }
                $str_file .= "          \$inputAux->set_value(\$this->controller->get_var(\$ref));
            \$inputAux->li_class = \"w50p\";
            \$inputAux->required = \$this->controller->is_required(\$ref);
            \$array_form_campos[sizeof(\$array_form_campos)] = \$inputAux;
            \$count_fields++;   
                
        ";              
            }
            // SE FOR LISTBOX
        else if(trim($class_aux) == "ListBox")
            {
                $modulo_aux = substr(str_replace($mySQL_prefix,"",$_REQUEST["fk_table_" . $obj->Field]),1);
                
                $str_file .= "          \$checked = \$this->controller->get_var(\$ref);
            if(trim(\$_REQUEST[\$ref]) <> \"\")
            {\$checked = \$_REQUEST[\$ref];}
            \$inputAux = new Uzzye_" . $class_aux . "(\$ref,\$ref,get_lang(\"" . $label_aux . "\"));
            \$inputAux->default_display = get_lang(\"_COMBOSELECIONE\");
            \$inputAux->default_value = 0;
            \$inputAux->checked_value = \$checked;" . $readonly_aux . "
            \$inputAux->size = 1;
            \$inputAux->multiple = false;\r\n";
                
                // SE POSSUI CHAVE ESTANGEIRA MONTA ARRAY DO MODEL
                if(trim($_REQUEST["fk_field_" . $obj->Field]) <> "")
                {
                    /*$str_file .= "            \$classAux = \"" . $modulo_aux . "_controller\";
            \$inputAux->reference_model = \"" . $modulo_aux . "\";
            \$objAux = new \$classAux();
            \$inputAux->set_options(\$objAux->get_array_options(\"" . $_REQUEST["fk_field_" . $obj->Field] . "\",\"" . $_REQUEST["fk_exib_field_" . $obj->Field] . "\",\"\",\"" . $_REQUEST["fk_exib_field_" . $obj->Field] . " ASC\"));
            \$inputAux->refresh_options = array(\"" . $_REQUEST["fk_field_" . $obj->Field] . "\",\"" . $_REQUEST["fk_exib_field_" . $obj->Field] . "\",\"\",\"" . $_REQUEST["fk_exib_field_" . $obj->Field] . " ASC\");
            ";*/
                    $str_file .= "              \$inputAux->set_reference_item(\$this->controller->get_reference_model(\$ref));";
                }
                // SE FOR CAMPO SET, MONTA OPCOES COM VALOR E INNERHTML IGUAIS
                else if(substr($obj->Type,0,3) == "set")
                {
                    $array_options = array();
                    $array_options = explode(",",substr($obj->Type,4,-1));
                    $str_file .= "          \$inputAux->set_options(array(";
                    for ($w=0;$w<sizeof($array_options);$w++)
                    {
                        //$array_options[$w] = substr($array_options[$w],1,-1);
                        $array_options[$w] = str_replace("'","",$array_options[$w]);
                        
                        if($w>0){
                            $str_file .= ",";
                        }
                        $str_file .= "array(\"" . $array_options[$w] . "\",\"" . $array_options[$w] . "\")";
                    }
                    $str_file .= "));
            ";
                    
                }
                $str_file .= "
                \$inputAux->set_value(\$checked);
            \$inputAux->li_class = \"w50p\";
            \$inputAux->required = \$this->controller->is_required(\$ref);
            \$array_form_campos[sizeof(\$array_form_campos)] = \$inputAux;
            \$count_fields++;   
                
        ";              
            }           
            // SE FOR IMAGEM
            else if(trim($class_aux) == "ImageCrop")
            {
                if(trim($array_str_images_to_crop[$obj->Field]) <> "")
                {
                $str_file .= "          \$inputAux = new Uzzye_ImageCrop(\"crop_\" . \$ref,\"crop_\" . \$ref,get_lang(\"" . $label_aux . "\"));\r\n";
                //$str_file .= "            \$inputAux->set_value(\$this->controller->get_var(\$ref));
                $str_file .= "          \$inputAux->li_class = \"w50p\";
            \$inputAux->clone = \$clone;
            \$inputAux->label = get_lang(\"_IMAGENS\") . \" (\" . get_lang(\"_TAMANHORECOMENDAVEL\") . \" ";
            for($w=0;$w<$array_count_images_to_crop[$obj->Field];$w++){
                if($w>0){
                    $str_file .= " / ";
                }
                $str_file .= "100% x 100%";
            }
            $str_file .= ")\";
            \$inputAux->crops = array(
                " . $array_str_images_to_crop[$obj->Field] . "  
            );
            \$inputAux->set_value(\$b_editando);
            \$inputAux->required = \$this->controller->is_required(\$ref);" . $readonly_aux . "
            \$array_form_campos[sizeof(\$array_form_campos)] = \$inputAux;
            \$count_fields++;   
                
        ";
                }
            }
            //SE FOR ARQUIVO
            else if(trim($class_aux) == "FileField")
            {
                $str_file .= "          \$inputAux = new Uzzye_" . $class_aux . "(\$ref,\$ref,get_lang(\"" . $label_aux . "\"));\r\n";
                $str_file .= "          \$inputAux->set_value(\$this->controller->get_var(\$ref));
            \$inputAux->li_class = \"w50p\";
            \$inputAux->required = \$this->controller->is_required(\$ref);" . $readonly_aux . "
            \$array_form_campos[sizeof(\$array_form_campos)] = \$inputAux;
            \$count_fields++;   
                
        ";  
            }
            //SE FOR CHECKBOX
            else if(trim($class_aux) == "CheckBox")
            {
                $str_file .= "          \$checked = \$this->controller->get_var(\$ref);
            if(trim(\$_REQUEST[\$ref]) <> \"\")
            {\$checked = \$_REQUEST[\$ref];}\r\n";
                $str_file .= "          \$inputAux = new Uzzye_" . $class_aux . "(\$ref,\$ref,get_lang(\"" . $label_aux . "\"));\r\n";
                $str_file .= "          \$inputAux->set_value(1);
            \$inputAux->default_value = 0;
            \$inputAux->checked_value = \$checked;
            \$inputAux->li_class = \"w50p\";
            \$inputAux->required = \$this->controller->is_required(\$ref);" . $readonly_aux . "
            \$array_form_campos[sizeof(\$array_form_campos)] = \$inputAux;
            \$count_fields++;   
                
        ";  
            }
            //SE FOR CKEDITOR
            else if(trim($class_aux) == "CKEditorField" || trim($class_aux) == "WYSIHTML5")
            {
                $str_file .= "          \$inputAux = new Uzzye_" . $class_aux . "(\$ref,\$ref,get_lang(\"" . $label_aux . "\"));\r\n";
                $str_file .= "          \$inputAux->set_value(\$this->controller->get_var(\$ref));
            \$inputAux->li_class = \"w100p\";
            \$inputAux->required = \$this->controller->is_required(\$ref);" . $readonly_aux . "
            \$array_form_campos[sizeof(\$array_form_campos)] = \$inputAux;
            \$count_textarea++; 
                
        ";  //\$inputAux->required = false;
            }
            //SE FOR COLOR
            else if(trim($class_aux) == "ColorField")
            {
                $str_file .= "          \$inputAux = new Uzzye_" . $class_aux . "(\$ref,\$ref,get_lang(\"" . $label_aux . "\"));\r\n";
                $str_file .= "          \$inputAux->set_value(\$this->controller->get_var(\$ref));
            \$inputAux->li_class = \"w50p\";
            \$inputAux->required = \$this->controller->is_required(\$ref);" . $readonly_aux . "
            \$array_form_campos[sizeof(\$array_form_campos)] = \$inputAux;
            \$count_color++; 
                
        ";  
            }
            //SE FOR GEOLOCATION
            else if(trim($class_aux) == "GeolocationField")
            {
                $str_file .= "          \$inputAux = new Uzzye_" . $class_aux . "(\$ref,\$ref,get_lang(\"" . $label_aux . "\"));\r\n";
                $str_file .= "          \$inputAux->set_value(\$this->controller->get_var(\$ref));
            \$inputAux->li_class = \"w100p\";
            \$inputAux->required = \$this->controller->is_required(\$ref);" . $readonly_aux . "
            \$array_form_campos[sizeof(\$array_form_campos)] = \$inputAux;
            \$count_geolocation++; 
                
        ";  
            }
            //SE FOR DATEFIELD
            else if(trim($class_aux) == "DateField")
            {
                $str_file .= "          \$inputAux = new Uzzye_" . $class_aux . "(\$ref,\$ref,get_lang(\"" . $label_aux . "\"));\r\n";
                $str_file .= "          \$inputAux->li_class = \"w50p\";
            \$inputAux->time = \$this->date_masks[\$ref];
            \$inputAux->required = \$this->controller->is_required(\$ref);" . $readonly_aux . "
            \$inputAux->set_value(\$this->controller->get_var(\$ref));
            \$array_form_campos[sizeof(\$array_form_campos)] = \$inputAux;
            \$count_datefield++; 
                
        ";  
            }
            //SE FOR NUMBERFIELD
            else if(trim($class_aux) == "NumberField")
            {
                $str_file .= "          \$inputAux = new Uzzye_" . $class_aux . "(\$ref,\$ref,get_lang(\"" . $label_aux . "\"));\r\n
            \$inputAux->set_mask(\$this->number_masks[\$ref]);\r\n";
                $str_file .= "          \$inputAux->set_value(\$this->controller->get_var(\$ref));
            \$inputAux->li_class = \"w50p\";
            \$inputAux->required = \$this->controller->is_required(\$ref);" . $readonly_aux . "
            \$array_form_campos[sizeof(\$array_form_campos)] = \$inputAux;
            \$count_numberfield++; 
                
        ";  
            }
            //SE FOR HIDDENFIELD
            else if(trim($class_aux) == "HiddenField")
            {
                $str_file .= "          \$inputAux = new Uzzye_" . $class_aux . "(\$ref,\$ref,get_lang(\"" . $label_aux . "\"));\r\n";
                $str_file .= "          \$inputAux->set_value(\$this->controller->get_var(\$ref));
            " . $readonly_aux . "
            \$array_form_campos[sizeof(\$array_form_campos)] = \$inputAux;
            \$count_hiddenfield++; 
                
        ";  
            }
            //SE FOR DATATABLE
            else if(trim($class_aux) == "DataTable")
            {
                $str_file .= "          \$inputAux = new Uzzye_" . $class_aux . "();\r\n
            \$inputAux->clone = \$clone;
            \$inputAux->label = get_lang(\"" . $label_aux . "\");
            \$inputAux->name = \$ref;
            \$inputAux->li_class = 'w100p';
            \$inputAux->id = \$ref;
            \$inputAux->id_registro = intval(\$id);
            \$inputAux->sql_where = \"\";
            \$inputAux->b_editando = \$b_editando;
            \$inputAux->set_reference_item(\$this->controller->get_item_data_table(\$ref));\r\n";
                $str_file .= "          " . $readonly_aux . "
            \$array_form_campos[sizeof(\$array_form_campos)] = \$inputAux;
            \$count_datatables++; 
                
        ";  
            }
            // SENÃO, É TEXTFIELD
            else if(trim($class_aux) == "PasswordField")
            {           
                $str_file .= "          \$inputAux = new Uzzye_" . $class_aux . "(\$ref,\$ref,get_lang(\"" . $label_aux . "\"));\r\n";
                $str_file .= "          \$inputAux->li_class = \"w50p\";\r\n";
                $str_file .= "          \$inputAux->required = (\$b_editando?false:\$this->controller->is_required(\$ref));" . $readonly_aux . "
            \$array_form_campos[sizeof(\$array_form_campos)] = \$inputAux;
            \$count_fields++;   
                
        ";
            }
            // SENÃO, É TEXTFIELD
            else
            {           
                $str_file .= "          \$inputAux = new Uzzye_" . $class_aux . "(\$ref,\$ref,get_lang(\"" . $label_aux . "\"));\r\n";
                $str_file .= "          \$inputAux->set_value(\$this->controller->get_var(\$ref));\r\n";
                if(strtoupper(substr($obj->Type,0,3)) == "INT"){
                    $str_file .= "          \$inputAux->li_class = \"w25p\";\r\n";}
                else{
                    $str_file .= "          \$inputAux->li_class = \"w50p\";\r\n";}
            $str_file .= "          \$inputAux->required = \$this->controller->is_required(\$ref);" . $readonly_aux . "
            \$array_form_campos[sizeof(\$array_form_campos)] = \$inputAux;
            \$count_fields++;   
                
        ";
            }
        }                           
        }
    }
        
    $str_file .= "  
            \$this->array_form_campos = \$array_form_campos;

            parent::monta_campos_form(\$id, \$readonly, \$clone, \$resumido);   
        }
}

?>";    
    
    $str_file = str_replace("#FK_INCLUDES#",$str_fk_includes_view,$str_file);
        
    $filepath = "classes/views/" . $name . "_view.php";    
    ////if(file_exists($filepath))
    {
        //@unlink($filepath);
    }    
    /*$fp = fopen($filepath, "w");    
    fwrite($fp, $str_file);
    fclose($fp);*/
    file_put_contents($filepath, $str_file);
    
// PROCESSA ARQUIVO DE IDIOMAS
    $filepath = "lang/" . $_CONFIG["lang"] . ".php";
    
    $array_lang[sizeof($array_lang)] = array($constante_nomePlural,$_REQUEST["nomePlural"]);
    $array_lang[sizeof($array_lang)] = array($constante_nomeSingular,$_REQUEST["nomeSingular"]);
    $array_lang[sizeof($array_lang)] = array($constante_tituloMenu,$_REQUEST["tituloMenu"]);
        
    foreach($array_lang as $item)
    {
        //$fp = fopen($filepath, "r");  
        //$str_file = fread($fp, filesize($filepath));
        $str_file = file_get_contents($filepath);
    
        $const_aux = $item[0];
        $val_aux = $item[1];        
            
        // Encontra variável já definida
        $pos = strpos($str_file,"\"" .$const_aux . "\" => ");
        if($pos === false)
        {           
        }   
        else
        {
            $str_ant = substr($str_file,0,$pos);        
            $pos_2 = $pos + strpos(substr($str_file,$pos),",\r\n"); // O que ocorre quando houver vírgula no texto?         
            $str_pos = substr($str_file,$pos_2);
            $str_file = $str_ant . "\"" . $const_aux . "\" => \"" . output_encode(html_text($val_aux)) . "\"" . $str_pos;   
        }
        
        /*$fp = fopen($filepath, "w");
        fwrite($fp, $str_file);
        fclose($fp);*/
        file_put_contents($filepath, $str_file);
    }

    /*$fp = fopen($filepath, "r");  
    $str_file = fread($fp, filesize($filepath));
    fclose($fp);*/
    $str_file = file_get_contents($filepath);
        
    $str_end_file = substr($str_file,strrpos($str_file,"\"\" => \"\""));
    $str_file = substr($str_file,0,strrpos($str_file,"\"\" => \"\""));
    if(is_array($array_lang) && sizeof($array_lang) > 0) {
        $str_end_file .= "\r\n        ";
        foreach($array_lang as $item)
        {
            $const_aux = $item[0];
            $val_aux = $item[1];    
                
            $pos = strpos($str_file,"\"" .$const_aux . "\" => ");
            if($pos === false)
            {       
                $str_file .= "\"" . $const_aux . "\" => \"" . output_encode(html_text($val_aux)) . "\",\r\n        ";
            }       
        }
    }
    $str_file .= $str_end_file;
    
    /*$fp = fopen($filepath, "w");
    fwrite($fp, $str_file);
    fclose($fp);*/
    file_put_contents($filepath, $str_file);
    
// PROCESSA ARQUIVO DE TABELAS
    $filepath = "_common/tabelas.php";
    /*$fp = fopen($filepath, "r");  
    $str_file = fread($fp, filesize($filepath));
    fclose($fp);*/
    $str_file = file_get_contents($filepath);
    
    //$fp = fopen($filepath, "w");
    $pos = strpos($str_file,$nome_tabela_aux);  
    if($pos === false)
    {
        $str_file = substr($str_file,0,strrpos($str_file,";")+1);
        $str_file .= "\r\n define(\"" . $nome_tabela_aux . "\",\$mySQL_prefix.\"_" . substr(str_replace($mySQL_prefix,"",$name),0) . "\");\r\n\r\n?>";
    }   
    else
    {
        $str_ant = substr($str_file,0,$pos);        
        $pos_2 = $pos + strpos(substr($str_file,$pos),")");     
        $str_pos = substr($str_file,$pos_2);
        $str_file = $str_ant . $nome_tabela_aux . "\",\$mySQL_prefix.\"_" . substr(str_replace($mySQL_prefix,"",$name),0) . "\"" . $str_pos;        
    }
    /*fwrite($fp, $str_file);
    fclose($fp);*/
    file_put_contents($filepath, $str_file);
    
    $sqlCmd = "SELECT * FROM " . DBTABLE_CMS_MODULOS . " WHERE link = \"" . $db->escape_string($name) . "\"";
    $resCmd = $db->exec_query($sqlCmd);
    $id_modulo = intval(@$db->result_field($resCmd,0,"id"));
    if(intval($id_modulo) == 0)
    {        
        $sqlCmd = "INSERT INTO " . DBTABLE_CMS_MODULOS . " SET 
                        nome = \"" . output_decode($db->escape_string($_REQUEST["tituloMenu"])) . "\", 
                        icon_class = \"" . output_decode($db->escape_string($_REQUEST["classeIcone"])) . "\", 
                        cor = \"" . output_decode($db->escape_string($_REQUEST["corModulo"])) . "\", 
                        link = \"" . output_decode($db->escape_string($name)) . "\",
                        modulo_pai = \"" . output_decode($db->escape_string($_REQUEST["moduloPai"])) . "\"";
        $resCmd = $db->exec_query($sqlCmd);
    //}
    
        if(intval($_REQUEST["incluirMenu"]) == 1){          
            $sqlCmd = "INSERT INTO " . DBTABLE_CMS_MODULOS_GRUPOS_USUARIOS . " (id_modulo,id_grupo,acoes)
                    SELECT mol.id, gru.id, \"Todas\" FROM " . DBTABLE_CMS_MODULOS . " mol, " . DBTABLE_CMS_GRUPOS_USUARIOS . " gru
                    WHERE mol.link = \"" . $db->escape_string($name) . "\"
                    AND LEFT(gru.titulo,5) = 'Uzzye'";
            $resCmd = $db->exec_query($sqlCmd);
        }
    }

    ?>
    <div class="row alert-display">
        <div class="col-sm-12">
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <i class="ti-check pr-15"></i>Processamento efetuado com sucesso!
            </div>
        </div>
    </div>
    <?php
}

?>
<div class="panel-wrapper collapse in">
    <div class="panel-body">
        <div class="row mb-20">
            <div class="form-wrap">
<?php
//else
{
    $options_sel_tabl = "";
?>
    <form action="javascript:void(0);" method="post" name="formMVCMan1" id="formMVCMan1" class="formAjax"
        enctype="multipart/form-data"
        data-url-post="<?php echo ROOT_SERVER . ROOT; ?>ajax/tools/mvc">
        <div class="form-body">
            <div class="col-sm-12">
                <input type="hidden" name="acao" value="selTable">
                <h6 class="mb-10">Selecione a Tabela</h6>
                <select name="table" id="select-table" class="form-control select2">
                    <optgroup>
                        <option value="">Selecione...</option>
                        <?php

                        $sqlCmd = "SHOW TABLES";
                        $resCmd = $db->exec_query($sqlCmd);
                        $array_aux = $db->result_object_array($resCmd);
                        
                        foreach($array_aux as $obj)
                        {
                            $field_name = "Tables_in_" . $db->dbase;
                            ?><option value="<?php echo $obj->$field_name; ?>"<?php
                            if($obj->$field_name == $_REQUEST["table"]){echo " selected";} 
                            ?>><?php echo $obj->$field_name; ?></option><?php

                            $options_sel_tabl .= "<option value=\"" . $obj->$field_name . "\">" . $obj->$field_name . "</option>";
                        }                       
                        ?>
                    </optgroup>
                </select>
                <button class="btn btn-default btn-back mt-20">Início</button>
            </div>
        </div>
    </form>
<?php 
    if(($_REQUEST["acao"] == "selTable" || $_REQUEST["acao"] == "procTable") && trim($_REQUEST["table"]) <> "")
    {   
        ?>
        <form action="javascript:void(0);" method="post" name="form2" id="form2" class="formAjax"
            enctype="multipart/form-data"
            data-url-post="<?php echo ROOT_SERVER . ROOT; ?>ajax/tools/mvc">
            <div class="form-body">
                <div class="col-sm-12">
                    <input type="hidden" name="acao" value="procTable">
                    <input type="hidden" name="table" value="<?php echo $_REQUEST["table"]; ?>">
                    <?php
                    $sqlCmd = "DESCRIBE " . $_REQUEST["table"];
                    $resCmd = $db->exec_query($sqlCmd);
                    $array_aux = $db->result_object_array($resCmd);
                    
                    /*object(stdClass)#7 (6) 
                    { 
                        ["Field"]=>  string(2) "id" 
                        ["Type"]=>  string(7) "int(11)" 
                        ["Null"]=>  string(2) "NO" 
                        ["Key"]=>  string(3) "PRI" 
                        ["Default"]=>  NULL 
                        ["Extra"]=>  string(14) "auto_increment" 
                    }*/
                    
                    ?>
                    <br />

                    <hr />
                    <h6 class="mb-10">Campos</h6>
                    <table width='100%' border='0' cellpadding='0' cellspacing='0' class="mvc-table pad-table">
                        <thead>
                            <tr>
                                <td width="1"></td>
                                <td>Campo</td>              
                                <td>Tipo</td>
                                <td>Classe</td>
                                <td>Etiqueta (PT-BR)</td>
                                <td>Listar</td>
                                <td>Leitura</td>
                                <td>Nulo</td>
                                <td>Chave</td>
                                <td>Padrão</td>
                                <td>Opções</td>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach($array_aux as $obj)
                        {
                            //if(!in_array($obj->Field,$model_keys,true))
                            {
                                echo "<tr valign='top'>
                                    <td class=\"sorter\">
                                        <i class=\"fa fa-bars\"></i><span class=\"btn-text\"></span>
                                    </td>
                                    <td>
                                        <b>" . get_output($obj->Field) . "</b>
                                    </td>
                                    <td>
                                        " . get_output($obj->Type) . "
                                    </td>";
                                if(!in_array($obj->Field,$model_keys,true))
                                {
                                    ?>
                                    <td>                    
                                        <select class="form-control select2" name="type_<?php echo  $obj->Field; ?>" onchange="setFieldType(this.value,'<?php echo $obj->Field ?>')";>
                                            <optgroup>
                                                <option value="">Não incluir</option>
                                                <?php
                                                $sel = get_str_cb_class_types($obj->Field,$obj->Type);
                                                ?>
                                            </optgroup>
                                        </select>
                                        <select class="form-control select2" name="img_group_<?php echo $obj->Field ?>" id="img_group_<?php echo $obj->Field ?>" style='display:<?php if($sel <> "ImageCrop"){echo "none";}?>'>
                                            <optgroup>
                                                <option value="">Nenhum agrupador</option>
                                                <?php
                                                
                                                $sqlCmd_2 = "DESCRIBE " . $_REQUEST["table"];
                                                $resCmd_2 = $db->exec_query($sqlCmd_2);
                                                $array_aux_2 = $db->result_object_array($resCmd_2);
                                    
                                                foreach($array_aux_2 as $obj_2)
                                                {
                                                    echo ("<option value=\"" . $obj_2->Field . "\">" . get_output($obj_2->Field) . "</option>");
                                                }
                                                 
                                                ?>
                                            </optgroup>
                                        </select>
                                        <input type="text" class="form-control reference-models-input" name="ref_models_<?php echo $obj->Field ?>" placeholder='entidade,campo_id_entidade' id="ref_models_<?php echo $obj->Field ?>" style='display:<?php if($sel <> "ListBox"){echo "none";}?>' />
                                        <input type="text" class="form-control data-tables-input" name="data_tables_<?php echo $obj->Field ?>" placeholder='entidade,campo_id_referente' id="data_tables_<?php echo $obj->Field ?>" style='display:<?php if($sel <> "DataTable"){echo "none";}?>' />
                                    </td>
                                    <?php
                                                 
                                    if(substr($obj->Field,0,9) == "subtitulo") {
                                        $const_aux = str_ireplace("_"," ",str_ireplace("subtitulo","Subtítulo",strtoupper($obj->Field)));
                                    } else if(substr($obj->Field,0,6) == "titulo") {
                                        $const_aux = str_ireplace("_"," ",str_ireplace("titulo","Título",strtoupper($obj->Field)));
                                    } else if(substr($obj->Field,0,9) == "descricao") {
                                        $const_aux = str_ireplace("_"," ",str_ireplace("descricao","Descrição",strtoupper($obj->Field)));
                                    } else if(substr($obj->Field,0,7) == "chamada") {
                                        $const_aux = str_ireplace("_"," ",str_ireplace("chamada","Chamada",strtoupper($obj->Field)));
                                    } else if(substr($obj->Field,0,6) == "imagem") {
                                        $const_aux = str_ireplace("_"," ",str_ireplace("imagem","Imagem",strtoupper($obj->Field)));
                                    } else if(substr($obj->Field,0,7) == "arquivo") {
                                        $const_aux = str_ireplace("_"," ",str_ireplace("arquivo","Arquivo",strtoupper($obj->Field)));
                                    } else {
                                        $const_aux = ucfirst(str_replace("_"," ",$obj->Field));
                                        //$const_aux = $obj->Field;
                                    }
                                    echo "<td>
                                        <input type='text' class='form-control form-Text' name='label_" . $obj->Field . "' value='" . $const_aux . "'>
                                    </td>";
                                    echo "<td>
                                        <div class=\"checkbox checkbox-custom\">
                                            <input type='checkbox' name='list_" . $obj->Field . "' id='list_" . $obj->Field . "' value='1'>
                                            <label for=\"list_" . $obj->Field . "\"></label>
                                        </div>
                                    </td>";
                                    echo "<td>
                                        <div class=\"checkbox checkbox-custom\">
                                            <input type='checkbox' name='readonly_" . $obj->Field . "' id='readonly_" . $obj->Field . "' value='1'>
                                            <label for=\"readonly_" . $obj->Field . "\"></label>
                                        </div>
                                    </td>";
                                }
                                else
                                {
                                    echo "<td></td>";
                                    echo "<td></td>";
                                    echo "<td></td>";
                                    echo "<td></td>";
                                }
                            
                                echo "<td>"; ?>
                                    <input type="hidden" name="null_<?php echo  $obj->Field; ?>" value="<?php echo $obj->Null; ?>">
                                    <?php echo $obj->Null . "
                                </td>
                                <td>
                                    " . $obj->Key . "
                                </td>
                                <td>
                                    " . $obj->Default . "
                                </td>
                                <td>
                                    " . $obj->Extra . "
                                </td>
                                </tr>";
                            }                       
                        }   
                        
                        $sqlCmd = "SHOW INDEX FROM " . $_REQUEST["table"];
                        $resCmd = $db->exec_query($sqlCmd);
                        $array_aux = $db->result_object_array($resCmd);
                        
                        /*Table             Non_unique  Key_name        Seq_in_index    Column_name         Collation   Cardinality Sub_part    Packed  Null    Index_type  Comment
                        novoprojeto_tags    0           PRIMARY         1               id                  A           0           NULL        NULL            BTREE    
                        novoprojeto_tags    1           fk_usuario_inc  1               usuario_criacao     A           NULL        NULL        NULL            BTREE    
                        novoprojeto_tags    1           fk_usuario_alt  1               usuario_atualizacao A           NULL        NULL        NULL    YES     BTREE               */
                        ?>
                        <tbody>
                    </table>
                    <br />

                    <hr />
                    <h6 class="mb-10">Chaves</h6>
                    Selecione a tabela que a chave referencia, o campo da referência e o campo da tabela referenciada a aparecer nas visualizações.
                    <br/><br/>
                    <table width='100%' border='0' cellpadding='0' cellspacing='0' class='pad-table'>
                        <tbody>
                            <tr>
                                <td>Coluna</td>
                                <td>Chave</td>
                                <td>Ref. Tabela</td>
                                <td>Ref. Campo</td>
                                <td>Ref. Campo Visual</td>
                            </tr>
                            <?php
                            foreach($array_aux as $obj)
                            {
                                //if(!in_array($obj->Field,$model_keys,true))
                                {
                                    if(!in_array($obj->Column_name,$model_keys,true))
                                    {
                                        if(strtoupper($obj->Key_name) == "PRIMARY")
                                        {
                                            echo "<tr valign='top'>
                                                <td>
                                                    <b>" . $obj->Column_name . "</b>" . "
                                                </td>
                                                <td colspan='100'>
                                                    " . $obj->Key_name . "
                                                </td>
                                            </tr>";             
                                        }               
                                        else if(strtoupper(substr($obj->Key_name,0,2)) == "FK")
                                        {
                                            echo "<tr valign='top'>
                                                <td>
                                                    <b>" . $obj->Column_name . "</b>" . "
                                                </td>
                                                <td>
                                                    " . $obj->Key_name . "
                                                </td>
                                                <td>
                                                    <select name='fk_table_" . $obj->Column_name . "' class=\"form-control select2\">
                                                        <optgroup>
                                                            <option value=\"\">Selecione...</option>
                                                            " . $options_sel_tabl . "
                                                        </optgroup>
                                                    </select>
                                                </td>
                                                <td>
                                                    <input class='form-control form-Text' type='text' name='fk_field_" . $obj->Column_name . "' value=\"id\">
                                                </td>
                                                <td>
                                                    <input class='form-control form-Text' type='text' name='fk_exib_field_" . $obj->Column_name . "' value=\"id\">
                                                </td>
                                            </tr>";         
                                        }
                                        else
                                        {
                                            echo "<tr valign='top'>
                                                <td>
                                                    <b>" . $obj->Column_name . "</b>" . "
                                                </td>
                                                <td colspan='100'>
                                                    " . $obj->Key_name . "
                                                </td>
                                            </tr>";             
                                        }
                                    }
                                    else
                                    {
                                        echo "<tr valign='top'>
                                            <td>
                                                <b>" . $obj->Column_name . "</b>" . "
                                            </td>
                                            <td colspan='100'>
                                                " . $obj->Key_name . "
                                            </td>
                                        </tr>";                 
                                    }
                                }                               
                            }   
                            ?>
                        </tbody>
                    </table>
                    </br>
                    
                    <hr />
                    <h6 class="mb-10">Títulos (PT-BR)</h6>
                    Defina as etiquetas de descrição singular e plural do módulo, a descrição para o menu, marque se o módulo estará visível no menu e selecione qual o menu principal o qual é pertencente.
                    <br/><br/>
                    <table width='100%' border="0" cellpadding="0" cellspacing="0" class="pad-table">
                        <tbody>
                            <tr>
                                <td width="20%">Etiqueta p/ singular:</td>
                                <td width="80%">
                                    <input type='text' class='form-control form-Text' name='nomeSingular'>
                                </td>
                            </tr>
                            <tr>
                                <td>Etiqueta p/ plural:</td>
                                <td>
                                    <input type='text' class='form-control form-Text' name='nomePlural' id='nomePlural' onchange="document.getElementById('tituloMenu').value = this.value;">
                                </td>
                            </tr>
                            <tr>
                                <td>Etiqueta p/ menu:</td>
                                <td>    
                                    <input type='text' class='form-control form-Text' name='tituloMenu' id='tituloMenu' />
                                    <div class="checkbox checkbox-custom">
                                       <input type='checkbox' name='incluirMenu' value="1" checked id="incluirMenu"/>
                                       <label for="incluirMenu">Incluir no menu</label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>Classe do Ícone</td>
                                <td>    
                                    <input type='text' class='form-control form-Text' name='classeIcone' id='classeIcone' />
                                </td>
                            </tr>
                            <tr>
                                <td>Cor do Módulo</td>
                                <td>
                                    <input type="text" class="form-control form-color form-Text" id="corModulo" name="corModulo" value="" />
                                </td>
                            </tr>
                            <tr>
                                <td>Módulo pai:</td>
                                <td>    
                                    <select name="moduloPai" class="form-control select2">
                                        <optgroup>
                                            <option value="">Selecione...</option>
                                            <?php
                                            
                                            $sqlCmd = "SELECT * FROM " . DBTABLE_CMS_MODULOS . " WHERE (modulo_pai = 0 OR ISNULL(modulo_pai))";
                                            $resCmd = $db->exec_query($sqlCmd);
                                            $array_aux = $db->result_object_array($resCmd);
                                
                                            foreach($array_aux as $obj)
                                            {
                                                echo ("<option value=\"" . $obj->id . "\">" . get_output($obj->nome) . "</option>");
                                            }
                                             
                                            ?>
                                        </optgroup>
                                    </select>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <br />          
                    
                    <button type="button" id="botao_submit_<?php echo $modulo; ?>"" class="btn btn-success btn-submit btn-anim"><i class="icon-rocket"></i> <span class="btn-text">Processar</span></button>
                </div>
            </div>
        </form>
        <?php
    }
}
?>
            </div>
        </div>
    </div>
</div>
<?php

function get_str_cb_class_types($name,$type)
{   
    $sel = "";
    
    $def_item = "TextField";

    // PRÉ-SELECIONA POR FILTROS
    if(substr($type,0,3) == "int")
    {
        $def_item = "TextField";
    }
    else if(substr($type,0,6) == "double")
    {
        $def_item = "TextField";
    }
    else if(substr($type,0,7) == "varchar" || substr($type,0,4) == "char")
    {
        $def_item = "TextField";
    }   
    else if(substr($type,0,4) == "date" || substr($type,0,4) == "datetime")
    {
        $def_item = "DateField";
    }
    else if(strstr($type,"tinytext"))
    {
        if(strstr($name,"image") || strstr($name,"foto") || strstr($name,"mini") || strstr($name,"thumb"))
        {
            $def_item = "ImageCrop";
        }
        else
        {
            $def_item = "FileField";
        }
    }       
    else if(strstr($type,"text"))
    {
        $def_item = "CKEditorField";
    }   
    else if(substr($type,0,3) == "set")
    {
        $def_item = "ComboBox";
    }
    
    // PRÉ-SELECIONA POR NOMES
    if(strstr($name,"cor"))
    {
        $def_item = "ColorField";
    }
    
    if(strstr($name,"geolocalizacao"))
    {
        $def_item = "GeolocationField";
    }
    
    $str_cb_class_types = "";
    $diretorio = "classes";
    if(substr($diretorio,-1) <> "/")
    {$diretorio .= "/";}
    $ponteiro  = opendir($diretorio); // ponteiro que ira percorrer a pasta
    while ($nome_itens = readdir($ponteiro)) { // monta o vetor com os itens da pasta
        $itens[] = $nome_itens;
    }
    sort($itens); // ordena o vetor de itens
    foreach ($itens as $listar) {  //percorre o vetor para fazer a separacao entre arquivos e pastas
        if ($listar!="." && $listar!=".."){ // retira os itens "./" e "../" para que retorne apenas pastas e arquivos
            if (!is_dir($listar)) { // checa se é uma pasta
                if(substr($listar,0,5) == "Uzzye")
                {
                    $item = substr($listar,6,-4);
                    
                    //if($item == "ComboBox" || $item == "TextField")
                    {
                        $str_cb_class_types .= ("<option value=\"" . $item . "\"");
                        if($item == $def_item)
                        {
                            $str_cb_class_types .= " selected";
                            $sel = $item;
                        }
                        $str_cb_class_types .= (">" . $item . "</option>");
                    }
                }
            }
        }
    }
    echo $str_cb_class_types;
    return $sel;
}
?>
        </div>
    </div>
</div>