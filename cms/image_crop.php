<?php

include_once("config.php");

include_once("verificaLogado.php");

//var_dump($_REQUEST);

class CropImage {
  private $src;
  private $data;
  private $ratio;
  private $dst;
  private $folder;
  private $type;
  private $mime;
  private $extension;
  private $msg;
  private $quality;
 
  function __construct($src, $data, $quality) {
    $this -> setSrc($src);
    $this -> setData($data);
    //$this -> setFile($file);
    $this -> setQuality($quality);
    $this -> crop($this -> src, $this -> dst, $this -> data);
  }
 
  private function setSrc($src) {
    if (!empty($src)) {
      $info   = getimagesize($src);
      $mime   = $info['mime']; // mime-type as string for ex. "image/jpeg" etc.
      $width  = $info[0];      // width as integer for ex. 512
      $height = $info[1];      // height as integer for ex. 384
      $type   = $info[2];      // same as exif_imagetype
 
      //if ($type) 
        $this -> src = $src;
        $this -> type = $type;
        $this -> mime = $mime;
        $this -> extension = image_type_to_extension($type);
        $this -> setDst();
      //}
    }
  }
 
  private function setData($data) {
    if (!empty($data)) {
      $this -> data = json_decode(stripslashes($data));
    }
  }
 
  private function setQuality($quality) {
    if (!empty($quality)) {
      $this -> quality = $quality;
    }
  }
 
  /*private function setFile($file) {
    $errorCode = $file['error'];
 
    if ($errorCode === UPLOAD_ERR_OK) {
      $type = exif_imagetype($file['tmp_name']);
 
      if ($type) {
        $extension = image_type_to_extension($type);
        $src = 'img/' . date('YmdHis') . '.original' . $extension;
 
        if ($type == IMAGETYPE_GIF || $type == IMAGETYPE_JPEG || $type == IMAGETYPE_PNG) {
 
          if (file_exists($src)) {
            unlink($src);
          }
 
          $result = move_uploaded_file($file['tmp_name'], $src);
 
          if ($result) {
            $this -> src = $src;
            $this -> type = $type;
            $this -> extension = $extension;
            $this -> setDst();
          } else {
             $this -> msg = 'Falha ao salvar o arquivo na pasta';
          }
        } else {
          $this -> msg = 'Por favor, envie a imagem em um dos seguintes formatos: JPG, PNG, GIF';
        }
      } else {
        $this -> msg = 'Por favor, selecione uma imagem';
      }
    } else {
      $this -> msg = $this -> codeToMessage($errorCode);
    }
  }*/
 
  private function setDst() {
    //$this -> dst = 'img/' . date('YmdHis') . '.png';
    if(trim($_REQUEST["image_src"]) != "") {
      $arrAux = explode("/", $_REQUEST["image_src"]);
      $folder = "";
      for ($w = 0; $w < sizeof($arrAux)-1; $w++) {
        $folder .= $arrAux[$w] . "/";
      }
      $this -> folder = $folder;

      if(trim($_REQUEST["modulo"]) != "" &&
         trim($_REQUEST["field"]) != "" &&
         trim($_REQUEST["randcod"]) != "") {
        $this -> dst = $this -> folder . $_REQUEST["randcod"] . "_" . $_REQUEST["field"] . $this -> extension;
      } else {
        $this -> dst = $this -> src;
      }
    } else {
      $this -> dst = $this -> src;
    }
  }
 
  private function crop($src, $dst, $data) {
    if (!empty($src) && !empty($dst) && !empty($data)) {
      switch ($this -> type) {
        case IMAGETYPE_GIF:
          $src_img = imagecreatefromgif($src);
          break;
 
        case IMAGETYPE_JPEG:
          $src_img = imagecreatefromjpeg($src);
          break;
 
        case IMAGETYPE_PNG:
          $src_img = imagecreatefrompng($src);
          break;
      }
 
      if (!$src_img) {
        $this -> msg = "Falha ao ler o arquivo de imagem na pasta";
        return;
      }
 
      $size = getimagesize($src);
      $size_w = $size[0]; // natural width
      $size_h = $size[1]; // natural height
 
      $src_img_w = $size_w;
      $src_img_h = $size_h;
 
      $degrees = $data -> rotate;
      $scaleX = $data -> scaleX;
      $scaleY = $data -> scaleY;
      $zoom = $data -> zoom;

      // Rotate the source image
      if (is_numeric($degrees) && $degrees != 0) {
        // PHP's degrees is opposite to CSS's degrees
        $new_img = imagerotate( $src_img, -$degrees, imagecolorallocatealpha($src_img, 0, 0, 0, 127) );
 
        imagedestroy($src_img);
        $src_img = $new_img;
 
        $deg = abs($degrees) % 180;
        $arc = ($deg > 90 ? (180 - $deg) : $deg) * M_PI / 180;
 
        $src_img_w = $size_w * cos($arc) + $size_h * sin($arc);
        $src_img_h = $size_w * sin($arc) + $size_h * cos($arc);
 
        // Fix rotated image miss 1px issue when degrees < 0
        $src_img_w -= 1;
        $src_img_h -= 1;
      }

      // Flip it vertically and horizontally
      if(is_numeric($scaleY) && $scaleY == -1) {
        imageflip($src_img, IMG_FLIP_VERTICAL);
      }
      if(is_numeric($scaleX) && $scaleX == -1) {
        imageflip($src_img, IMG_FLIP_HORIZONTAL);
      }
 
      $tmp_img_w = $data -> width;
      $tmp_img_h = $data -> height;

      $dst_img_w = $_REQUEST["image_width"];
      $dst_img_h = $_REQUEST["image_height"];

      // Se não foi passado tamanho, usa o do próprio crop
      if(floatval($dst_img_w) == 0 && floatval($dst_img_h) == 0) {
        $dst_img_w = $data->width;
        $dst_img_h = $data->height;
      }
      
      // Se não tem largura, utiliza proporcional ao crop
      if(floatval($dst_img_w) == 0) {
        $dst_img_w = ($dst_img_h/$tmp_img_h)*$tmp_img_w;
      }

      // Se não tem altura, utiliza proporcional ao crop
      if(floatval($dst_img_h) == 0) {
        $dst_img_h = ($dst_img_w/$tmp_img_w)*$tmp_img_h;
      }

      // Ajusta crop para, no máximo, o tamanho da imagem
      if(floatval($dst_img_w) > floatval($src_img_w)) {
        $dst_img_h = $dst_img_h*($src_img_w/$dst_img_w);
        $dst_img_w = $src_img_w;
      }
      if(floatval($dst_img_h) > floatval($src_img_h)) {
        $dst_img_w = $dst_img_w*($src_img_h/$dst_img_h);
        $dst_img_h = $src_img_h;
      }

      $src_x = $data -> dataX;
      $src_y = $data -> dataY;
 
      if ($src_x <= -$tmp_img_w || $src_x > $src_img_w) {
        $src_x = $src_w = $dst_x = $dst_w = 0;
      } else if ($src_x <= 0) {
        $dst_x = -$src_x;
        $src_x = 0;
        $src_w = $dst_w = min($src_img_w, $tmp_img_w + $src_x);
      } else if ($src_x <= $src_img_w) {
        $dst_x = 0;
        $src_w = $dst_w = min($tmp_img_w, $src_img_w - $src_x);
      }
 
      if ($src_w <= 0 || $src_y <= -$tmp_img_h || $src_y > $src_img_h) {
        $src_y = $src_h = $dst_y = $dst_h = 0;
      } else if ($src_y <= 0) {
        $dst_y = -$src_y;
        $src_y = 0;
        $src_h = $dst_h = min($src_img_h, $tmp_img_h + $src_y);
      } else if ($src_y <= $src_img_h) {
        $dst_y = 0;
        $src_h = $dst_h = min($tmp_img_h, $src_img_h - $src_y);
      }
 
      // Scale to destination position and size
      $ratio = $tmp_img_w / $dst_img_w;
      $dst_x /= $ratio;
      $dst_y /= $ratio;
      $dst_w /= $ratio;
      $dst_h /= $ratio;

      /*
      $dst_img_w = $data -> width * $data -> zoom;
      $dst_img_h = $data -> height * $data -> zoom;

      $src_x = $data -> dataX;
      $dst_x = $src_x * $zoom;
      $src_y = $data -> dataY;
      $dst_y = $src_y * $zoom;

      $dst_w = $dst_img_w;
      $dst_h = $dst_img_h;
      */

      //echo $src_x . "," . $src_y . " TO " . $dst_x . "," . $dst_y; die(); // DEBUG
 
      $dst_img = imagecreatetruecolor($dst_img_w, $dst_img_h);
 
      // Add transparent background to destination image
      imagefill($dst_img, 0, 0, imagecolorallocatealpha($dst_img, 0, 0, 0, 127));
      imagesavealpha($dst_img, true);
 
      $result = imagecopyresampled($dst_img, $src_img, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);
 
      if ($result) {
        $imgres = false;

        switch ($this -> type) {
          default:
          case IMAGETYPE_PNG:
            $imgres = imagepng($dst_img,$dst);  
            break;
          case IMAGETYPE_GIF:
            $imgres = imagegif($dst_img,$dst); 
            break;
          case IMAGETYPE_JPEG:
            $imgres = imagejpeg($dst_img,$dst, $this -> quality); 
            break;
          case IMAGETYPE_BMP:
            $imgres = imagewbmp($dst_img,$dst); 
            break;
        } 

        if (!$imgres) {
          $this -> msg = "Falha ao salvar o crop da imagem na pasta";
        }
      } else {
        $this -> msg = "Falha ao realizar o crop da imagem na pasta";
      }
 
      imagedestroy($src_img);
      imagedestroy($dst_img);
    }
  }
 
  private function codeToMessage($code) {
    $errors = array(
      UPLOAD_ERR_INI_SIZE =>'O tamanho da imagem enviada excede o parâmetro upload_max_filesize especificado no php.ini',
      UPLOAD_ERR_FORM_SIZE =>'O tamanho da imagem enviada excede o parâmetro MAX_FILE_SIZE especificado no formulário HTML',
      UPLOAD_ERR_PARTIAL =>'O arquivo de imagem foi enviado parcialmente',
      UPLOAD_ERR_NO_FILE =>'Nenhum arquivo foi enviado',
      UPLOAD_ERR_NO_TMP_DIR =>'A pasta de arquivos temporários não foi definida',
      UPLOAD_ERR_CANT_WRITE =>'Falha ao escrever arquivo no disco',
      UPLOAD_ERR_EXTENSION =>'O upload de arquivos foi cancelado por uma extensão',
    );
 
    if (array_key_exists($code, $errors)) {
      return $errors[$code];
    }
 
    return 'Unknown upload error';
  }
 
  public function getResult() {
    return !empty($this -> data) ? $this -> dst : $this -> src;
  }
 
  public function getMsg() {
    return $this -> msg;
  }
}

$image_data = json_encode($_REQUEST);
 
$crop = new CropImage(
  isset($_REQUEST['image_src']) ? $_REQUEST['image_src'] : null,
  isset($image_data) ? $image_data : null,
  isset($_REQUEST['image_quality']) ? $_REQUEST['image_quality'] : null
);
 
$response = array(
  'state'  => 200,
  'message' => $crop -> getMsg(),
  'result' => $crop -> getResult()
);
 
ob_clean();
header('Content-Type: application/json');
echo json_encode($response);

die();

?>