<?php

$projectName = "newproject";
$projectColors = array(
	"main" => "dark",
	"theme" => "#000"
);

//$root = dirname( $_SERVER["PHP_SELF"] ) == DIRECTORY_SEPARATOR ? "" : dirname( $_SERVER["PHP_SELF"] );
$root = str_replace("/" . basename(__FILE__),"",str_replace($_SERVER["DOCUMENT_ROOT"],'', str_replace('\\','/',__FILE__ ) ));
if(substr($root, 0, 1) != "/" && substr($root, 0, 1) != "\\" && trim($root) != "") {
	$root = "/" . $root;
}
$_CONFIG = $_CMS_CONFIG = $_CONFIG_FORMAT = array();

// compression
@ini_set('zlib.output_compression','On');
@ini_set('zlib.output_compression_level','1');

// sessão
@ini_set('session.gc_maxlifetime',24*60*60); // 1 dia
@ini_set('session.cookie_httponly',true);

//@session_save_path(getcwd() . "/_sessions");
session_name($projectName . "_cms");
session_start();
ob_start();

@set_time_limit(0);

// constantes
if(!defined("ROOT")){
	define("ROOT",$root . "/"); 	// UTILIZADO PARA INCLUDES/REFERENCIAS CLIENT-SIDE (LINKS, IMAGENS, JS E DEMAIS ARQUIVOS "EXTERNOS")
}

if(!defined("ROOT_CMS")){
	define("ROOT_CMS","../cms/");	// UTILIZADO PARA INCLUDES/REFERÊNCIAS SERVER-SIDE (PHP) DO CMS
}
if(!defined("ROOT_SITE")){
	define("ROOT_SITE","../");		// UTILIZADO PARA INCLUDES/REFERÊNCIAS SERVER-SIDE (PHP) DO SITE EM ESPECÍFICO
}
if(!defined("ROOT_SERVER")){
	//$rootServer = "http://" . $_SERVER["SERVER_NAME"];		// UTILIZADO PARA COMPARTILHAMENTO DE CONTEÚDO
	$rootServer = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]"; //:$_SERVER[SERVER_PORT]
	/*if(substr($rootServer,-1) != "/") {
		$rootServer .= "/";
	}*/
	define("ROOT_SERVER",$rootServer);
}

// pasta de uploads
$_CMS_CONFIG["pasta_upload"] = "upload/";
define("UPLOAD_FOLDER",ROOT_SITE . $_CMS_CONFIG["pasta_upload"]);

// includes do cms
include_once(ROOT_CMS . "utils.php");

/*$lang = "pt-br";
include_once(ROOT_CMS . "lang/" . $lang . ".php");*/

require_once(ROOT_CMS . "classes/CryptDecrypt.php");
$chaveCriptografia = "U#z)z@y.e%2&01(1";
$cryptDec = new CryptDecrypt();
$cryptDec->hash = $chaveCriptografia;

// includes do bd
//$_CMS_CONFIG["db_padrao"] = "DB_MySQL";
$_CMS_CONFIG["db_padrao"] = "DB_MySQLi";
include_once(ROOT_CMS . "classes/" . $_CMS_CONFIG["db_padrao"] . ".php");
$db = $mysql = new $_CMS_CONFIG["db_padrao"]();

$mySQL_prefix = $projectName;

$DB_persistent = true;
include(ROOT_CMS . "_common/conexao.php");
include(ROOT_CMS . "_common/tabelas.php");

include_once(ROOT_CMS . "classes.php");

// armazena configs
if($_REQUEST["lang"] == "" && (!is_array($_SESSION["_config"]) || trim($_SESSION["_config"]["lang"]) == "")) {
	$_REQUEST["lang"] = get_country_lang();
}
configura($_REQUEST["lang"]);

$lang = $_CONFIG["lang"];

$lang_site = array();

if(file_exists(ROOT_CMS . "lang/" . $lang . ".php"))
{
	include_once(ROOT_CMS . "lang/" . $lang . ".php");
} else {
	include_once(ROOT_CMS . "lang/pt-br.php");
}

// configura endereços de e-mails
define("EMAIL_PADRAO_CONTATO",$_CONFIG["email_padrao_contato"]);
define("EMAIL_DEFAULT_FROM",$_CONFIG["email_default_from"]);

if(!isset($_SESSION["GLOBAL_RANDOM_KEY"]) || trim($_SESSION["GLOBAL_RANDOM_KEY"]) == "" || $_SESSION["ROOT_SESSION"] <> ROOT)
{
	$_SESSION["GLOBAL_RANDOM_KEY"] = intval($_SESSION["idLogin"]) . str_pad(rand(0,99999),5,"0",STR_PAD_LEFT);
	if(trim($_SESSION["ROOT_SESSION"]) == "")
	{
		$_SESSION["ROOT_SESSION"] = ROOT;
	}	
}

// configurações gerais
$_CMS_CONFIG["lang"] = $lang;
$_CMS_CONFIG["titulo"] = "Uzzye";
$_CMS_CONFIG["modulo_padrao"] = "cms_home";
$_CMS_CONFIG["acao_padrao"] = "lista_registros";
$_CMS_CONFIG["qtd_reg_pp"] = 20;
define("CONFIG_QTD_REG_PP",$_CMS_CONFIG["qtd_reg_pp"]);
//define("CONFIG_QTD_PAG_PAGINACAO",2);  -- versao 2

$_CMS_CONFIG["url_amigaveis"] = false;
$_CMS_CONFIG["charset_padrao"] = "UTF-8";
header("Content-Type: text/html; charset=" . $_CMS_CONFIG["charset_padrao"]);
header('X-XSS-Protection:0');

header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: *");
header("Access-Control-Allow-Headers: *");
//header("Access-Control-Allow-Headers: api-key,content-type");

?>