// Script "gulp" para compactar arquivos. Verificar mais informações comentadas no "gulpfile.js" da pasta raíz do projeto. Este é a compactação base, utilizada no CMS e também no front (pasta raíz)

'use strict';

 //gulpfile.js
var gulp = require('gulp');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
//var resolveDependencies = require('gulp-resolve-dependencies');
//var less = require('gulp-less');
var minifyCSS = require('gulp-csso');

//script paths
var jsFiles = '_js/vendor/**/*.js',
    jsDest = '_js/dist';
gulp.task('prejs', function(){
    return gulp.src(['_js/core/**/*.js'])
      .pipe(concat('uz-init.js'))
      .pipe(gulp.dest(jsDest))
      .pipe(rename('uz-init.min.js'))
      .pipe(uglify())
      .pipe(gulp.dest(jsDest));
});
gulp.task('js', gulp.series( 'prejs', function() {
    return gulp.src(['_js/plugins/jquery-1.9.1.min.js', '_js/plugins/jquery-migrate-1.2.1.min.js', '_js/plugins/moment-with-locales.min.js', jsFiles, '!_js/vendor/_off/*.js', '_js/dist/uz-init.min.js'])
      /*pipe(resolveDependencies({
        pattern: /\* @require [\s-]*(.*?\.js)/g,
          log: true
      }))*/
      .pipe(concat('uz.js'))
        .pipe(gulp.dest(jsDest))
        .pipe(rename('uz.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(jsDest));
}));

//css paths
var cssFiles = '_estilo/*.css',
    cssFa = '_estilo/font-awesome/*.css',
    cssDest = '_estilo';
gulp.task('facss', function(){
    return gulp.src([cssFa])
      .pipe(concat('font-awesome.min.css'))
      //.pipe(less())
      .pipe(minifyCSS())
      .pipe(gulp.dest(cssDest))
});
gulp.task('precss', gulp.series( 'facss', function(){
    return gulp.src([cssFiles, '!_estilo/uz.css', '!_estilo/uz.min.css', '!_estilo/_off/*.css', '!_estilo/login.css', '!_estilo/geral.css'])
      .pipe(concat('uz.css'))
      .pipe(gulp.dest(cssDest))
}));
gulp.task('css', gulp.series( 'precss', function(){
    return gulp.src([cssDest+'/uz.css', cssDest+'/geral.css'])
      .pipe(concat('uz.css'))
      .pipe(gulp.dest(cssDest))
      .pipe(rename('uz.min.css'))
      //.pipe(less())
      .pipe(minifyCSS())
      .pipe(gulp.dest(cssDest))
}));

/*gulp.task('default', function(){
  // Default task code
  console.log('GULP GULP GULP')
});*/
gulp.task('default', gulp.series( 'js', 'css' ));