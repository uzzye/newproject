<?php

// API access key from Google API's Console
	// Maps
	define( 'MAPS_API_KEY', 'AIzaSyB2wrv-mJ3aSB2O7WcaJ385ZztX5XAWeCY');

	// Production
		/* define( 'PUSH_API_ACCESS_KEY', 'AAAAVOAOXEI:APA91bHHPDygtYxaDA2RWUvEAdfjbRwrr0HCniPXEvd-qnhZOdsSnv6Y-ishN8WWYfP3QnWS6mGqzxGH0RIOwHSoNXFQSoNuA0CgvpWlkWzu4SKOKStknbyDzK4pZxMPjldQEqcjppxd' );
		define( 'PUSH_SERVER_KEY', 'AIzaSyDgmodWoWbgODpSPxK8IrRSzm624VinUt4' ); // AIzaSyCUbusoOyiuDhd4168jPd3zGbNiaRSage0
		define( 'SENDER_ID', '364536290370' );*/
	// Development
		define( 'PUSH_API_ACCESS_KEY', '' );
		define( 'PUSH_SERVER_KEY', '' );
		define( 'SENDER_ID', '' );

// Payment API Keys
	//define( '_PAYMENT_TOKEN', '' ); // Production
	define( '_PAYMENT_TOKEN', '' ); // Development
	//define( '_PAYMENT_GATEWAY_URL', 'https://api.branvopay.com/' ); // Production
	define( '_PAYMENT_GATEWAY_URL', 'https://sandbox-api.branvopay.com/' ); // Development
	
// Framework Defaults

@set_time_limit(0);
@ini_set('memory_limit', '-1');

@ini_set('display_errors', 0);
@ini_set('error_reporting', E_ALL ^ E_NOTICE ^ E_DEPRECATED ^ E_WARNING ^ E_STRICT);
@setlocale(LC_ALL,"ptb");
@setlocale(LC_ALL,"pt_BR");
// Fix for float number with incorrect decimal separator.
@setlocale(LC_NUMERIC, "us");
@setlocale(LC_NUMERIC, "en_US");

/*if (!ini_get("register_globals"))
{
    import_request_variables('GPC');
}*/

// habilita charset
$_CONFIG["charset_encoding"] = true;
$charsetEncoding = true;

// $_POST = array com todos os parametros que a pagina recebeu por postagem
foreach ($_POST as $secvalue)
{
   if(is_array($secvalue)){$secvalue = "";}
   /*	
   if ((preg_match("/<[^>]*script*\"?[^>]*>/i", $secvalue)) ||
       (preg_match("/<[^>]*object*\"?[^>]*>/i", $secvalue)) ||
       (preg_match("/<[^>]*iframe*\"?[^>]*>/i", $secvalue)) ||
       (preg_match("/<[^>]*applet*\"?[^>]*>/i", $secvalue)) ||
       (preg_match("/<[^>]*meta*\"?[^>]*>/i", $secvalue)) ||
       (preg_match("/<[^>]*cmd*\"?[^>]*>/i", $secvalue))
       ) //|| (eregi("\"", $secvalue)))
   {
      die ("<h3>HTTP Request Error</h3>");
   }
   */
}

// $_GET = array com todos os parametros que a pagina recebeu por url
foreach ($_GET as $secvalue)
{
   if(is_array($secvalue)){$secvalue = "";}
   if ((preg_match("/<[^>]*script*\"?[^>]*>/i", $secvalue)) ||
       (preg_match("/<[^>]*object*\"?[^>]*>/i", $secvalue)) ||
       (preg_match("/<[^>]*iframe*\"?[^>]*>/i", $secvalue)) ||
       (preg_match("/<[^>]*applet*\"?[^>]*>/i", $secvalue)) ||
       (preg_match("/<[^>]*meta*\"?[^>]*>/i", $secvalue)) ||
       (preg_match("/<[^>]*style*\"?[^>]*>/i", $secvalue)) ||
       (preg_match("/<[^>]*form*\"?[^>]*>/i", $secvalue)) ||
       (preg_match("/<[^>]*img*\"?[^>]*>/i", $secvalue)) ||
       (preg_match("/<[^>]*cmd*\"?[^>]*>/i", $secvalue))
       ) //|| (eregi("\"", $secvalue)))
   {
      die ("<h3>HTTP Request Error</h3>");
   }
}

function get_texto_acao($ac)
{	
	switch($ac)	
	{
		default:
			$ret = $ac;		
			break;
			
		case "acaoPadrao":
			$ret = get_lang("_ACAO_APRESENTACAO");
			break;
			
		case "lista_registros":
			$ret = get_lang("_ACAO_APRESENTACAO");
			break;
			
		case "lista":
			$ret = get_lang("_ACAO_APRESENTACAO");
			break;			
			
		case "incluir":
			$ret = get_lang("_ACAO_INCLUSAO");
			break;
			
		case "editar":
			$ret = get_lang("_ACAO_ALTERACAO");
			break;
			
		case "ativar_desativar":
			$ret = get_lang("_ACAO_ATIVACAO_DESATIVACAO");
			break;
			
		case "excluir":
			$ret = get_lang("_ACAO_EXCLUSAO");
			break;			
			
		case "do_incluir":
			$ret = get_lang("_ACAO_INCLUSAO");
			break;
			
		case "do_editar":
			$ret = get_lang("_ACAO_ALTERACAO");
			break;
			
		case "do_excluir":
			$ret = get_lang("_ACAO_EXCLUSAO");
			break;	
			
		case "visualizar":
			$ret = get_lang("_ACAO_VISUALIZAR");
			break;	
			
		case "exportar":
			$ret = get_lang("_ACAO_EXPORTACAO");	
	}
	
	return $ret;
}

function get_output($var)
{
	global $_CONFIG;
	
	if($_CONFIG["charset_encoding"]){
		return @utf8_encode($var);}
	else{
		return $var;}	
}

function output_decode($var)
{
	global $_CONFIG;
	
	if($_CONFIG["charset_encoding"]){
		return @utf8_decode($var);}
	else{
		return $var;}	
}

function output_encode($var)
{
	global $_CONFIG;
	
	if($_CONFIG["charset_encoding"]){
		return get_output($var);}
	else{
		return $var;}	
}

function get_url()
{
	global $modulo;
	
	$url = ROOT_CMS . "index.php";
	if(trim($modulo->id) <> "")
	{
		$url .= "?modulo=" . $modulo->id . "&";
	}	
	else
	{
		$url .= "?";
	}
	return $url;
}

function decode_data($data,$hora = false){
		$retorno_funcao = "";
		if(trim($data) <> "")
		{
   			$retorno_funcao = substr($data,6,4)."-".substr($data,3,2)."-".substr($data,0,2);
			if($hora)
			{
				$retorno_funcao .= " " . substr($data,11,2).":".substr($data,14,2).":".substr($data,17,2);
			}
		}
		return $retorno_funcao;
}
	
function encode_data($data,$hora = false){
		$retorno_funcao = "";
		if(trim($data) <> "")
		{
			$retorno_funcao = substr($data,8,2)."/".substr($data,5,2)."/".substr($data,0,4); 
			if($hora)
			{
				$retorno_funcao .= " " . substr($data,11,2).":".substr($data,14,2).":".substr($data,17,2);
			}
		}
		return $retorno_funcao;
}
	
function encode_hora($data){
    	$retorno_funcao = substr($data,11,8); 
		return $retorno_funcao;
}
	
function acerta_fone($str){
		if ($str != NULL){
			$ddd = substr($str,0,2);
			$fone = trim(substr($str,2,strlen($str)));
			if(strlen($fone)<8){
				$fone = "3".$fone;
			}
			$fone1 = substr($fone, 0,4);
			$fone2 = substr($fone,4,4);
			$fone_novo = "(".$ddd.")".$fone1."-".$fone2;
			return $fone_novo;
		}
}
	
function acerta_cep($str){
		if($str != NULL && strlen($str)<9){
			$cep1 = substr($str,0,5);
			$cep2 = substr($str,5,strlen($str));
			$cep = $cep1."-".$cep2;
			return $cep;
		}
}

function carrega_classe($str_classe)
{
	$str_classe = str_replace("_model", "", $str_classe);
	$str_classe = str_replace("_view", "", $str_classe);
	$str_classe = str_replace("_controller", "", $str_classe);

	if(strstr($str_classe, "_model")) {
		include_once(ROOT_CMS . "classes/models/" . $str_classe . ".php");
	} else {
		include_once(ROOT_CMS . "classes/models/" . $str_classe . "_model.php");	
	}

	if(strstr($str_classe, "_view")) {
		include_once(ROOT_CMS . "classes/views/" . $str_classe . ".php");
	} else {
		include_once(ROOT_CMS . "classes/views/" . $str_classe . "_view.php");
	}

	if(strstr($str_classe, "_controller")) {
		include_once(ROOT_CMS . "classes/controllers/" . $str_classe . ".php");
	} else {
		include_once(ROOT_CMS . "classes/controllers/" . $str_classe . "_controller.php");
	}

	if(file_exists(ROOT_CMS . "classes/" . $str_classe . ".php")){
		include_once(ROOT_CMS . "classes/" . $str_classe . ".php");
	}		
}

/*function selfURL(){ if(!isset($_SERVER['REQUEST_URI'])){ $serverrequri = $_SERVER['PHP_SELF']; }else{ $serverrequri = $_SERVER['REQUEST_URI']; } $s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : ""; $protocol = strleft(strtolower($_SERVER["SERVER_PROTOCOL"]), "/").$s; $port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]); return $protocol."://".$_SERVER['SERVER_NAME'].$port.$serverrequri; }
function strleft($s1, $s2) { return substr($s1, 0, strpos($s1, $s2)); }*/

function selfURL(){ 
	$_SERVER['FULL_URL'] = 'http';
	if($_SERVER['HTTPS']=='on'){$_SERVER['FULL_URL'] .=  's';}
	$_SERVER['FULL_URL'] .=  '://';
	if($_SERVER['SERVER_PORT']!='80') $_SERVER['FULL_URL'] .=  $_SERVER['HTTP_HOST'].':'.$_SERVER['SERVER_PORT'].$_SERVER['SCRIPT_NAME'];
	else
	$_SERVER['FULL_URL'] .=  $_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
	if($_SERVER['QUERY_STRING']>' '){$_SERVER['FULL_URL'] .=  '?'.$_SERVER['QUERY_STRING'];}
	return $_SERVER['FULL_URL'];
}

function curPageURL() {
 $pageURL = 'http';
 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
 $pageURL .= "://";
 if ($_SERVER["SERVER_PORT"] != "80") {
  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
 } else {
  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
 } 
 return $pageURL;
}

function abrev_texto($string,$i_limite_caracteres = 40,$ap = "p", $strContinue = " ...", $remove_obj = true)
{
	$stringOriginal = $string;
	if($remove_obj){
		while(strpos($string,"<iframe") !== false || strpos($string,"<object") !== false || strpos($string,"<embed") !== false)
		{
			$iPosAnt = strpos($string,"<iframe");		
			$iPosPos = strpos($string,"</iframe>");
			if($iPosAnt !== false && $iPosPos !== false){
				$string = substr($string,0,$iPosAnt) . substr($string,$iPosPos + strlen("</iframe>"));
			}

			$iPosAnt = strpos($string,"<object");		
			$iPosPos = strpos($string,"</object>");
			if($iPosAnt !== false && $iPosPos !== false){
				$string = substr($string,0,$iPosAnt) . substr($string,$iPosPos + strlen("</object>"));
			}
		
			$iPosAnt = strpos($string,"<embed");		
			$iPosPos = strpos($string,"</embed>");
			if($iPosAnt !== false && $iPosPos !== false){
				$string = substr($string,0,$iPosAnt) . substr($string,$iPosPos + strlen("</embed>"));
			}
		}
	}	
	$retorno = $string;
	if(strlen(@trim($string)) > $i_limite_caracteres)
    {
    	if(substr($string,$i_limite_caracteres-1,1) != " ")
		{
			if(($ap == "p") && (strpos(substr($string,$i_limite_caracteres-1)," ")))
			{				
     		  $i_limite_caracteres += strpos(substr($string,$i_limite_caracteres-1)," ")-1;				
			}
			else
			{
			  $i_limite_caracteres = strrpos(substr($string,0,$i_limite_caracteres)," ");
			}
		}
		
		$retorno = trim(substr($string,0,$i_limite_caracteres)) . $strContinue;
		
		if(!$remove_obj){
			while(substr_count($retorno,"<iframe") != substr_count($retorno,"</iframe>"))
			{					
				$strAux = substr($string,$i_limite_caracteres);	
				$iPosPos = strpos($strAux,"</iframe>");
				if($iPosPos !== false && ($iPosPos + strlen("</iframe>") + $i_limite_caracteres > $i_limite_caracteres)){
					$retorno = substr($string,0,$iPosPos + strlen("</iframe>") + $i_limite_caracteres);
					if(strlen(@trim($retorno)) < strlen(@trim($string)))
					{
						$retorno .= $strContinue;
					}
				}
			}
			while(substr_count($retorno,"<object") != substr_count($retorno,"</object>"))
			{					
				$strAux = substr($string,$i_limite_caracteres);	
				$iPosPos = strpos($strAux,"</object>");
				if($iPosPos !== false && ($iPosPos + strlen("</object>") + $i_limite_caracteres > $i_limite_caracteres)){
					$retorno = substr($string,0,$iPosPos + strlen("</object>") + $i_limite_caracteres);
					if(strlen(@trim($retorno)) < strlen(@trim($string)))
					{
						$retorno .= $strContinue;
					}
				}
			}
			while(substr_count($string,"<embed") != substr_count($retorno,"</embed>"))
			{					
				$strAux = substr($string,$i_limite_caracteres);	
				$iPosPos = strpos($strAux,"</embed>");
				if($iPosPos !== false && ($iPosPos + strlen("</embed>") + $i_limite_caracteres > $i_limite_caracteres)){
					$retorno = substr($string,0,$iPosPos + strlen("</embed>") + $i_limite_caracteres);
					if(strlen(@trim($retorno)) < strlen(@trim($string)))
					{
						$retorno .= $strContinue;
					}
				}
			}
		}
	}
	else
	{
		$retorno = $string;
	}
		
	return $retorno;
}

function gera_titulo_amigavel($titulo,$remove_acentos = true,$carac_espaco = "-",$savehyphen = 1,$savedots = 0,$lowercase = true)
{
	if($remove_acentos)
	{
		//$return = str_replace(" ",$carac_espaco,remove_caracteres_especiais(remove_acentos($titulo)));
		$return = (str_replace(" ",$carac_espaco,remove_caracteres_especiais(remove_acentos($titulo),$savehyphen,$savedots)));
	}
	else
	{
		$return = (str_replace(" ",$carac_espaco,remove_caracteres_especiais(($titulo),$savehyphen,$savedots)));
	}
	if($lowercase) {
		$return = strtolower($return);
	}
	return $return;
}	

function remove_acentos($str, $enc = "UTF-8")
{	 
	$acentos = array(
	'A' => '/&Agrave;|&Aacute;|&Acirc;|&Atilde;|&Auml;|&Aring;/',
	'a' => '/&agrave;|&aacute;|&acirc;|&atilde;|&auml;|&aring;/',
	'C' => '/&Ccedil;/',
	'c' => '/&ccedil;/',
	'E' => '/&Egrave;|&Eacute;|&Ecirc;|&Euml;/',
	'e' => '/&egrave;|&eacute;|&ecirc;|&euml;/',
	'I' => '/&Igrave;|&Iacute;|&Icirc;|&Iuml;/',
	'i' => '/&igrave;|&iacute;|&icirc;|&iuml;/',
	'N' => '/&Ntilde;/',
	'n' => '/&ntilde;/',
	'O' => '/&Ograve;|&Oacute;|&Ocirc;|&Otilde;|&Ouml;/',
	'o' => '/&ograve;|&oacute;|&ocirc;|&otilde;|&ouml;/',
	'U' => '/&Ugrave;|&Uacute;|&Ucirc;|&Uuml;/',
	'u' => '/&ugrave;|&uacute;|&ucirc;|&uuml;/',
	'Y' => '/&Yacute;/',
	'y' => '/&yacute;|&yuml;/',
	'a.' => '/&ordf;/',
	'o.' => '/&ordm;/');
 
    $str = preg_replace($acentos,
                       array_keys($acentos),
                       htmlentities($str,ENT_NOQUOTES, $enc));
	return $str;
}

function remove_caracteres_especiais($str,$savehyphen = 1,$savedots = 0)
{
	$str = html_entity_decode($str);
	
	/*$str = preg_replace("/[^a-zA-Z0-9-_]+[{áàãäâ}]+[{éèëê}]+[{óòõôö}]+[{íìîï}]+[{úùüû}]+[{ç}]+[{ÁÀÃÄÂ}]+[{ÉÈËÊ}]+[{ÓÒÕÔÖ}]+[{ÍÌÎÏ}]+[{ÚÙÜÛ}]+[{Ç}]/","", $str);
	//$str = preg_replace("/[^a-zA-Z0-9-_]/","", $str);*/
	
	$str = str_replace("?","",$str);
	$str = str_replace("/","",$str);
	$str = str_replace("`","",$str);
	$str = str_replace("´","",$str);
	$str = str_replace("\"","",$str);
	$str = str_replace("\'","",$str);
	$str = str_replace("'","",$str);
	$str = str_replace("²","2",$str);
	$str = str_replace("³","3",$str);
	
	if(function_exists("mb_detect_encoding")) {
		if(mb_detect_encoding($str) <> "UTF-8"){$str = get_output($str);}
	}
		
	if($savedots) $dot = '.'; else $dot = '';
	if($savehyphen) $hyphen = '-'; else $hyphen = '';
    $trade = array(
                'Ã¡'=>'a',
                'Ã '=>'a',
                'Ã£'=>'a',
                'Ã¤'=>'a',
                'Ã¢'=>'a',
                'Ã '=>'A',
                'Ã€'=>'A',
                'Ãƒ'=>'A',
                'Ã„'=>'A',
                'Ã‚'=>'A',
                'Ã(c)'=>'e',
                'Ã¨'=>'e',
                'Ã«'=>'e',
                'Ãª'=>'e',
                'Ã‰'=>'E',
                'Ãˆ'=>'E',
                'Ã‹'=>'E',
                'ÃŠ'=>'E',
                'Ã­'=>'i',
                'Ã¬'=>'i',
                'Ã¯'=>'i',
                'Ã(r)'=>'i',
                'Ã '=>'I',
                'ÃŒ'=>'I',
                'Ã '=>'I',
                'ÃŽ'=>'I',
                'Ã³'=>'o',
                'Ã²'=>'o',
                'Ãµ'=>'o',
                'Ã¶'=>'o',
                'Ã´'=>'o',
                'Ã"'=>'O',
                'Ã\''=>'O',
                'Ã•'=>'O',
                'Ã–'=>'O',
                'Ã"'=>'O',
                'Ãº'=>'u',
                'Ã¹'=>'u',
                'Ã¼'=>'u',
                'Ã»'=>'u',
                'Ãš'=>'U',
                'Ã™'=>'U',
                'Ãœ'=>'U',
                'Ã›'=>'U',
                '$'=>'',
                '@'=>'',
                '!'=>'',
                '#'=>'',
                '%'=>'',
                '^'=>'',
                '&'=>'e',
                '*'=>'',
                '('=>'',
                ')'=>'',
                '['=>'',
                ']'=>'',
                '{'=>'',
                '}'=>'',
                '-'=>$hyphen,
                '+'=>'',
                '='=>'',
    			':'=>'',
    			';'=>'',
                '\\'=>'',
                '|'=>'',
                '`'=>'',
    			'` '=>'',
    			'´'=>'',
    			'´ '=>'',
                '~'=>'',
                '/'=>'',
                '\"'=>'',
                '\''=>'',
                '<'=>'',
                '>'=>'',
                '?'=>'',
                ','=>'',
			    ':'=>'',
                'Ã§'=>'c',
                'Ã‡'=>'C',
                '®'=>'C',
                '©'=>'C',
                '™'=>'C',
                '²' => '2',
                '³' => '3',
                '' => '',
                '¼' => '14',
                '½' => '12',
                '¾' => '34',
                '°' => '',
                '.'=>$dot
                );
    return(strtr($str,$trade)); 
}

function debug($var,$iteracao_parar = -1,$iteracao_atual = -1)
{
	if($iteracao_parar >= 0)
	{
		if($iteracao_parar == $iteracao_atual)
		{
			ob_clean();
			var_dump($var);
			die();
		}
	}
	else
	{
		ob_clean();
		var_dump($var);
		die();
	}
}

function getBitLy($url) {
	// http://james.cridland.net/code
	// v0.2 24 May 08: added a URLdecode function, to correctly cope with some charactersets
	//                 thanks to Nick at www.japansoc.com

	$bitly_login="uzzye";
	$bitly_apikey="R_15c1e006799d5c8cb96d2479fdca06e9";

	$api_call = file_get_contents("http://api.bit.ly/shorten?version=2.0.1&longUrl=".$url."&login=".$bitly_login."&apiKey=".$bitly_apikey);

	$bitlyinfo=json_decode(get_output($api_call),true);

	if ($bitlyinfo['errorCode']==0) {
		return $bitlyinfo['results'][urldecode($url)]['shortUrl'];
	} else {
		return false;
	}
}

function getTinyUrl($url) {   
    $ch = curl_init();  
	$timeout = 5;  
	curl_setopt($ch,CURLOPT_URL,'http://tinyurl.com/api-create.php?url='.urlencode($url));  
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);  
	curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);  
	$data = curl_exec($ch);  
	curl_close($ch);  
	return $data;
}

function getTweetUrl($url, $title, $via = "")
{
	//$url = str_replace(ROOT,"",$url);
	$url = ROOT_SERVER . $url;
	
	$maxTitleLength = 140 - (strlen($url)+1);
	if (strlen($title) > $maxTitleLength) {
		$title = substr($title, 0, ($maxTitleLength-3)).'...';
	}	

	$output = get_output(gera_titulo_amigavel($title,false,"+")) . ":+" . getTinyUrl($url);
	if(trim($via) <> "")
	{
		$output .= "+$via";
	}

	//return 'http://twitter.com/home?status='.urlencode($output);
	return 'http://twitter.com/home?status='.($output);
}

function encode_xml($sXML)
{
	return "<![CDATA[" . $sXML . "]]>";
}

function get_ip()
{
	/* if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
      $ip=$_SERVER['REMOTE_ADDR'];
	} */
	if( array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER) && !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
        if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',')>0) {
            $addr = explode(",",$_SERVER['HTTP_X_FORWARDED_FOR']);
            return trim($addr[0]);
        } else {
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
    }
    else {
        return $_SERVER['REMOTE_ADDR'];
    }
    return $ip;	
}

function valida_email($email)
{	
	if(filter_var($email, FILTER_VALIDATE_EMAIL))
 		return true;
	else
		return false;
}

function sendAttachedMail($De,$Para,$Assunto,$Conteudo,$Arquivo = "",$Notificacao="N")
{
   $headers = "MIME-Version: 1.0\n";
   $headers .= "Content-type: text/html; charset=UTF-8\n";
   $headers .= "From: $De\n";
   $headers .= "Reply-To: $De\n";
   //$headers .= "X-Mailer: PHP's mail() Function\n";
   if ($Notificacao == "S")
   {
	 $headers .= "Disposition-Notification-To: $De\n";
   }
   $headers .= $Cabecalho;

   $Para = str_replace("Header:","NAH",str_replace("To:","NAH",$Para));
   $Conteudo = str_replace("Header:","NAH",str_replace("To:","NAH",$Conteudo));
   $Assunto = str_replace("Header:","NAH",str_replace("To:","NAH",$Assunto));
   
   $retorno = true;
   
   $array_para = explode(";",$Para);
   foreach($array_para as $Para)
   {
		//if(mail(trim($Para), $Assunto, $Conteudo , $headers))
   		if(mail(trim($Para), $Assunto, $Conteudo , $headers ,"-r".$De)){ // Se for Postfix
			$retorno = true;
		}
		else
		{
			$headers .= "Return-Path: $De\n"; // Se "não for Postfix"
    		if(mail(trim($Para), $Assunto, $Conteudo , $headers)){
    			return true;
    		}
			else{
				return false;
			}
		}
   }
   return $retorno;
}

/*function sendMail($De,$Para,$Assunto,$Conteudo,$Notificacao="N",$Cabecalho = "")
{
   $headers = "MIME-Version: 1.0\r\n";
   $headers .= "Content-type: text/html; charset=UTF-8\r\n";
   $headers .= "From: $De\r\n";
   $headers .= "Reply-To: $De\r\n";
   //$headers .= "X-Mailer: PHP's mail() Function\n";
   if ($Notificacao == "S")
   {
	 $headers .= "Disposition-Notification-To: $De\r\n";
   }
   $headers .= $Cabecalho;

   $Para = str_replace("Header:","NAH",str_replace("To:","NAH",$Para));
   $Conteudo = str_replace("Header:","NAH",str_replace("To:","NAH",$Conteudo));
   $Assunto = str_replace("Header:","NAH",str_replace("To:","NAH",$Assunto));
   
   $retorno = true;
   
   $array_para = explode(";",$Para);
   foreach($array_para as $Para)
   {
	   if(mail(trim($Para), $Assunto, $Conteudo , $headers))
   	   {
         $retorno = true;
   	   }
   	   else
   	   {
     	 return false;
   	   }
   }
   return $retorno;
}*/

function sendMail($De,$Para,$Assunto,$Conteudo,$Notificacao="N")
{
	$headers = "MIME-Version: 1.0\r\n";
	$headers .= "Content-type: text/html; charset=UTF-8\r\n";
	$headers .= "From: $De\r\n";
	$headers .= "Reply-To: $De\r\n";
	//$headers .= "X-Mailer: PHP's mail() Function\r\n";
	if ($Notificacao == "S")
	{
		$headers .= "Disposition-Notification-To: $De\r\n";
	}

	$Para = str_replace("Header:","NAH",str_replace("To:","NAH",$Para));
	$Conteudo = str_replace("Header:","NAH",str_replace("To:","NAH",$Conteudo));
	$Assunto = str_replace("Header:","NAH",str_replace("To:","NAH",$Assunto));

	$retorno = true;

	$array_para = explode(";",$Para);
	foreach($array_para as $Para)
	{
		if(!mail(trim($Para), $Assunto, $Conteudo , $headers, "-r".$De)) // Se "for Postfix"
		{
			$headers .= "Return-Path: " . $De . "\r\n"; // Se "não for Postfix"
			if(mail(trim($Para), $Assunto, $Conteudo, $headers )){
				$retorno = true;
			}
			else {
				$retorno = false;
			}
		}
		else
		{
			$retorno = true;
		}
	}
	return $retorno;
}

function get_array_estados()
{             							
	$array_aux = array(array("AC","Acre"),
					   array("AL","Alagoas"),
					   array("AP","Amap&aacute;"),
					   array("AM","Amazonas"),
					   array("BA","Bahia"),
					   array("CE","Cear&aacute;"),
					   array("DF","Distrito Federal"),
					   array("ES","Espirito Santo"),
					   array("GO","Goi&aacute;s"),
					   array("MA","Maranh&atilde;o"),
					   array("MT","Mato Grosso"),
					   array("MS","Mato Grosso do Sul"),
					   array("MG","Minas Gerais"),
					   array("PA","Par&aacute;"),
					   array("PB","Para&iacute;ba"),
					   array("PR","Paran&aacute;"),
					   array("PE","Pernambuco"),
					   array("PI","Piau&iacute;"),
					   array("RJ","Rio de Janeiro"),
					   array("RN","Rio Grande do Norte"),
					   array("RS","Rio Grande do Sul"),
					   array("RO","Rod&ocirc;nia"),
					   array("RR","Roraima"),
					   array("SC","Santa Catarina"),
					   array("SP","S&atilde;o Paulo"),
					   array("SE","Sergipe"),
					   array("TO","Tocantins"));
	return $array_aux;
}

function _bot_detected() {
	return (
	  isset($_SERVER['HTTP_USER_AGENT'])
	  && preg_match('/bot|crawl|slurp|spider|mediapartners/i', $_SERVER['HTTP_USER_AGENT'])
	);
}

//howto use: $crawler = crawlerDetect($_SERVER['HTTP_USER_AGENT']);
function crawlerDetect($USER_AGENT)
{
	/*$crawlers = array(
    'Google'=>'Google',
    'MSN' => 'msnbot',
    'Rambler'=>'Rambler',
    'Yahoo'=> 'Yahoo',
    'AbachoBOT'=> 'AbachoBOT',
    'accoona'=> 'Accoona',
    'AcoiRobot'=> 'AcoiRobot',
    'ASPSeek'=> 'ASPSeek',
    'CrocCrawler'=> 'CrocCrawler',
    'Dumbot'=> 'Dumbot',
    'FAST-WebCrawler'=> 'FAST-WebCrawler',
    'GeonaBot'=> 'GeonaBot',
    'Gigabot'=> 'Gigabot',
    'Lycos spider'=> 'Lycos',
    'MSRBOT'=> 'MSRBOT',
    'Altavista robot'=> 'Scooter',
    'AltaVista robot'=> 'Altavista',
    'ID-Search Bot'=> 'IDBot',
    'eStyle Bot'=> 'eStyle',
    'Scrubby robot'=> 'Scrubby',
    );*/
    
    // to get crawlers string used in function uncomment it
    // it is better to save it in string than use implode every time
    // global $crawlers
    // $crawlers_agents = implode('|',$crawlers);
    $crawlers_agents = 'Google|msnbot|Rambler|Yahoo|AbachoBOT|accoona|AcioRobot|ASPSeek|CocoCrawler|Dumbot|FAST-WebCrawler|GeonaBot|Gigabot|Lycos|MSRBOT|Scooter|AltaVista|IDBot|eStyle|Scrubby';

    if ( strpos($crawlers_agents , $USER_AGENT) === false )
       return false;
    // crawler detected
    // you can use it to return its name
    
    else {
       //return array_search($USER_AGENT, $crawlers);
       return true;
    }    
}

function valida_cpf($cpf)
{	// Verifiva se o número digitado contém todos os digitos
    $cpf = str_pad(preg_replace('[^0-9]', '', $cpf), 11, '0', STR_PAD_LEFT);
    $cpf = str_replace("-","",str_replace("/","",str_replace(".","",$cpf)));
    
    if(trim($cpf) == "")
    {return true;}
    else{
	
	// Verifica se nenhuma das sequências abaixo foi digitada, caso seja, retorna falso
    if (strlen($cpf) != 11 || $cpf == '00000000000' || $cpf == '11111111111' || $cpf == '22222222222' || $cpf == '33333333333' || $cpf == '44444444444' || $cpf == '55555555555' || $cpf == '66666666666' || $cpf == '77777777777' || $cpf == '88888888888' || $cpf == '99999999999')
	{
	return false;
    }
	else
	{   // Calcula os números para verificar se o CPF é verdadeiro
        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf[$c] * (($t + 1) - $c);
            }

            $d = ((10 * $d) % 11) % 10;

            if ($cpf[$c] != $d) {
                return false;
            }
        }

        return true;
    }}
}

function now($bd = false)
{
	$str = "";
	$str = date("Y-m-d H:i:s");
	return $str;
}

function secure_insert($str)
{ 
	$str = str_replace("<","",$str);
	$str = str_replace(">","",$str);
	$str = str_replace("(","",$str);
	$str = str_replace(")","",$str);
	$str = str_replace("%","",$str);
	$str = str_replace("$","",$str);
	$str = str_replace("´","",$str);
	$str = str_replace("`","",$str);
	//$str = $db->escape_string($str);
	return $str;
}

function gera_senha()
{
	$var = 'abcdhFGHFghfdWei123456789';

   for($i = 0; $i < 6; $i++)
   {
      $varchar[$i] = substr($var,rand(0,strlen($var)),1);
   }
   for($i = 0; $i < 6; $i++)
   {
      $senhagerada = $senhagerada . $varchar[$i];
   }
   return $senhagerada;
}

function ckeditor_isnull($str)
{
	$str = trim($str);
	$str = str_replace("\r\n","",$str);
	if($str == "<p>	<br />	&nbsp;</p>" || $str == "<br />"){return true;}
	else{return false;}
}

function clearBrowserCache() {
    header("Pragma: no-cache");
    header("Cache: no-cache");
    header("Cache-Control: no-cache, must-revalidate");
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
}

function normaliza_ckeditor( $embed, $largura, $altura )
{
	$resultado = preg_replace('/width="[0-9][0-9][0-9]"/','width="'.$largura.'"',$embed);
	$resultado = preg_replace('/height="[0-9][0-9][0-9]"/','height="'.$altura.'"',$resultado);
	$resultado = preg_replace('/width: [0-9][0-9][0-9]/','width: '.$largura.'',$resultado);
	$resultado = preg_replace('/height: [0-9][0-9][0-9]/','height: '.$altura.'',$resultado);
	$resultado = preg_replace('/width:[0-9][0-9][0-9]/','width:'.$largura.'',$resultado);
	$resultado = preg_replace('/height:[0-9][0-9][0-9]/','height:'.$altura.'',$resultado);

	$resultado = preg_replace('/<embed/','<param wmode="transparent"></param><embed',$resultado);
	$resultado = preg_replace('/allowfullscreen="/','wmode="transparent" allowfullscreen="',$resultado);
	return $resultado;
}

function diff_dates($data1, $data2)
{
	$databd=$data1; // coloque a data vinda do banco de dados
	$databd= explode("-",$databd); 
	$data = mktime(0,0,0,$databd[1],$databd[2],$databd[0]);
	
	$data_atual=$data2; // coloque a data vinda do banco de dados
	$data_atual= explode("-",$data_atual);
	$data_atual = mktime(0,0,0,$data_atual[1],$data_atual[2],$data_atual[0]);
	
	$dias = ($data - $data_atual)/86400;
	$dias = ceil($dias);
	
	return $dias;
}

function diff_time($firstTime,$lastTime) {
    $firstTime=strtotime($firstTime);
    $lastTime=strtotime($lastTime);
    $timeDiff=$lastTime-$firstTime;
    return $timeDiff; 
}

function html_text($string, $show = false){
	if($show){
		$string = nl2br(str_replace("&", "&amp;", htmlentities($string)));
	} else {
		$string = htmlentities($string);
	}
	return $string;
}

function configura($refIdioma){
	global $_CONFIG, $_noSQL, $_CONFIG_FORMAT, $charsetEncoding;

	$bConfig = false;

    if(!isset($_CONFIG["noSQL"]) || $_CONFIG["noSQL"] === false) {

		carrega_classe("idiomas");
		carrega_classe("configuracoes");
		
		$sqlWhere = "idioma_padrao = 1";
		
		$bProcessaLang = true;
		
		$itensCfg = array();

		// SE IDIOMA FOI PASSADO, PROCURA CONFIG COM ELE E CONFIGURA
		if(trim($refIdioma) != ""){
			$idiomas = new idiomas();
			$idiomas = $idiomas->localiza(array("identificador"),array(trim($refIdioma)));
			
			if(is_array($idiomas) && sizeof($idiomas) > 0){
				$sqlWhere = "id_idioma = " . intval($idiomas[0]->get_var("id"));
				
				$itensCfg = new configuracoes();
				$itensCfg = $itensCfg->get_array_ativos_controllers($sqlWhere,"data_atualizacao DESC",1);
				
				// processa idioma
				$lang = trim(stripslashes(get_output($idiomas[0]->get_var("identificador"))));
				
				$_SESSION["_config"]["lang"] = trim($lang);
				$_SESSION["_config"]["id_lang"] = intval($idiomas[0]->get_var("id"));
				$refLang = explode("-",$lang);
				$_SESSION["_config"]["ref_lang"] = $refLang[0];
				
				$bProcessaLang = false;
			}
		} else {
		// SE NÃO FOI PASSADO IDIOMA, PROCURA CONFIG COM IDIOMA ATUAL
			if(is_array($_SESSION["_config"]) && trim($_SESSION["_config"]["lang"]) != ""){
				$sqlWhere = "id_idioma = " . intval($_SESSION["_config"]["id_lang"]);
				
				$itensCfg = new configuracoes();
				$itensCfg = $itensCfg->get_array_ativos_controllers($sqlWhere,"data_atualizacao DESC",1);
				
				// processa idioma			
				$idiomas = new idiomas();
				$idiomas->inicia_dados();
				$idiomas->set_var("id",intval($_SESSION["_config"]["id_lang"]));
				$idiomas->carrega_dados();
			
				$lang = trim(stripslashes(get_output($idiomas->get_var("identificador"))));
				
				$_SESSION["_config"]["lang"] = trim($lang);
				$_SESSION["_config"]["id_lang"] = intval($_SESSION["_config"]["id_lang"]);
				$refLang = explode("-",$lang);
				$_SESSION["_config"]["ref_lang"] = $refLang[0];
				
				$bProcessaLang = false;
			} 
		}
		
		if(!is_array($itensCfg) || sizeof($itensCfg) == 0){
		// SE NÃO TEM IDIOMA ATUAL (1A ENTRADA) PROCURA CONFIG PADRÃO
			$sqlWhere = "idioma_padrao = 1";
				
			$itensCfg = new configuracoes();
			$itensCfg = $itensCfg->get_array_ativos_controllers($sqlWhere,"data_atualizacao DESC",1);
		}
		
		// SE ACHOU CONFIG, SETA PARÂMETROS
		if(is_array($itensCfg) && sizeof($itensCfg) > 0){
			$itCfg = $itensCfg[0];

			$idiomas = new idiomas();
			$idiomas->inicia_dados();
			$idiomas->set_var("id",intval($itCfg->get_var("id_idioma")));
			$idiomas->carrega_dados();
			
			$lang = trim(stripslashes(get_output($idiomas->get_var("identificador"))));

			$fields = $itCfg->model->get_fields_name();

			//var_dump($fields); echo '<br/><br/>'; die();

			foreach($fields as $campo){
				$_SESSION["_config"][$campo] = trim(stripslashes(get_output($itCfg->get_var($campo))));
				$_CONFIG_FORMAT[$campo] = trim(stripslashes(get_output($itCfg->get_var_format($campo))));
			}

			// emails
			$_SESSION["_config"]["email_padrao_contato"] = trim(stripslashes(get_output($itCfg->get_var("email_padrao_contato"))));
			$_SESSION["_config"]["email_default_from"] = trim(stripslashes(get_output($itCfg->get_var("email_default_from"))));
			
			// processa idioma
			if($bProcessaLang && intval($_SESSION["_config"]["id_lang"]) == 0){
				$_SESSION["_config"]["lang"] = trim($lang);
				$_SESSION["_config"]["id_lang"] = intval($itCfg->get_var("id_idioma"));
				$refLang = explode("-",$lang);
				$_SESSION["_config"]["ref_lang"] = $refLang[0];
			}

			// configurações gerais
			$_SESSION["_config"]["titulo"] = trim(stripslashes(get_output($itCfg->get_var("titulo_padrao"))));
			$_SESSION["_config"]["descricao"] = trim(stripslashes(get_output($itCfg->get_var("descricao_padrao"))));
			$_SESSION["_config"]["palavras-chave"] = trim(stripslashes(get_output($itCfg->get_var("palavras_chave_padrao"))));
			$_SESSION["_config"]["url_amigaveis"] = (bool)($itCfg->get_var("url_amigaveis"));
			$_SESSION["_config"]["charset_encoding"] = $charsetEncoding;
			$_SESSION["_config"]["charset_padrao"] = trim(stripslashes(get_output($itCfg->get_var("charset_padrao"))));
			$_SESSION["_config"]["qtd_itens_pp"] = intval($itCfg->get_var("qtd_itens_pp"));
			$_SESSION["_config"]["qtd_group_pag"] = intval($itCfg->get_var("qtd_group_pag"));
			$_SESSION["_config"]["codigo_analytics"] = trim(stripslashes(get_output($itCfg->get_var("codigo_analytics"))));
			$_SESSION["_config"]["codigo_css"] = trim(stripslashes(get_output($itCfg->get_var("codigo_css"))));
			$_SESSION["_config"]["codigo_javascript"] = trim(stripslashes(get_output($itCfg->get_var("codigo_javascript"))));
			$_SESSION["_config"]["geolocalizacao"] = trim(stripslashes(get_output($itCfg->get_var("geolocalizacao"))));
			$_SESSION["_config"]["mapa_embed"] = trim(stripslashes(get_output($itCfg->get_var("mapa_embed"))));
			
			$_CONFIG = $_SESSION["_config"];

			$bConfig = true;
		}
	}

	if(!$bConfig) {
	// SE NÃO ACHOU CONFIG, SETA PARÂMETROS PADRÃO UZZYE
		$itCfg = new configuracoes_model();
		$fields = $itCfg->get_fields_name();
		foreach($fields as $campo){
			$_CONFIG[$campo] = "";
			$_CONFIG_FORMAT[$campo] = "";
		}

		$_CONFIG["email_padrao_contato"] = "mauricio@uzzye.com";
		$_CONFIG["email_default_from"] = "mauricio@uzzye.com";
		
		$_CONFIG["lang"] = "pt-br";
		$_CONFIG["id_lang"] = 1;
		$_CONFIG["ref_lang"] = "pt";
		$_CONFIG["titulo"] = "Uzzye.";
		$_CONFIG["descricao"] = "Born for online marketing and digital business . Nascidos para o marketing online e negócios digitais";
		$_CONFIG["palavras-chave"] = "conexao, digital, marca, consumidor, work, negocios, empresa, presença, produção, conteúdo, site, mídias, sociais, engajamento, relacionamento, marketing, solução, valor, estratégia, caxias do sul, caxias, serra, gaucha, rs, brasil";
		$_CONFIG["url_amigaveis"] = true;
		$_CONFIG["charset_encoding"] = $charsetEncoding;
		$_CONFIG["charset_padrao"] = "UTF-8";
		$_CONFIG["qtd_itens_pp"] = 20;
		$_CONFIG["qtd_group_pag"] = 3;
		$_CONFIG["codigo_analytics"] = "";
		$_CONFIG["codigo_css"] = "";
		$_CONFIG["codigo_javascript"] = "";
		$_CONFIG["geolocalizacao"] = "";
		$_CONFIG["mapa_embed"] = "";
		
		$_SESSION["_config"] = $_CONFIG;
	}

	$_CONFIG["noSQL"] = $_noSQL;
	$_SESSION["_config"]["noSQL"] = $_CONFIG["noSQL"];
}

function getParams(){
	global $_CONFIG;
	return $_CONFIG["params"];
}

function is_ie(){
    $u_agent = $_SERVER['HTTP_USER_AGENT']; 
    $ub = False; 
    if(preg_match('/MSIE/i',$u_agent)) 
    { 
        $ub = True; 
    } 
    
    return $ub; 
} 

function getBrowser(){
    $u_agent = $_SERVER['HTTP_USER_AGENT']; 
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version= "";
    $mobile = false;

    if ( preg_match ( "/phone|iphone|itouch|ipod|symbian|android|htc_|htc-|palmos|blackberry|opera mini|iemobile|windows ce|nokia|fennec|hiptop|kindle|mot |mot-|webos\/|samsung|sonyericsson|^sie-|nintendo/i", $u_agent ) ) {
        // these are the most common
        $mobile = true;
    } else if ( preg_match ( "/mobile|pda;|avantgo|eudoraweb|minimo|netfront|brew|teleca|lg;|lge |wap;| wap /i", $u_agent ) ) {
        // these are less common, and might not be worth checking
        $mobile = true;
    }

    //First get the platform?
    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
    }
    elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
    }
    elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'windows';
    }
    
    // Next get the name of the useragent yes seperately and for good reason
    if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) 
    { 
        $bname = 'Internet Explorer'; 
        $ub = "MSIE"; 
    } 
    elseif(preg_match('/Firefox/i',$u_agent)) 
    { 
        $bname = 'Mozilla Firefox'; 
        $ub = "Firefox"; 
    } 
    elseif(preg_match('/Chrome/i',$u_agent)) 
    { 
        $bname = 'Google Chrome'; 
        $ub = "Chrome"; 
    } 
    elseif(preg_match('/Safari/i',$u_agent)) 
    { 
        $bname = 'Apple Safari'; 
        $ub = "Safari"; 
    } 
    elseif(preg_match('/Opera/i',$u_agent)) 
    { 
        $bname = 'Opera'; 
        $ub = "Opera"; 
    } 
    elseif(preg_match('/Netscape/i',$u_agent)) 
    { 
        $bname = 'Netscape'; 
        $ub = "Netscape"; 
    } 
    
    // finally get the correct version number
    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) .
    ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
        // we have no matching number just continue
    }
    
    // see how many we have
    $i = count($matches['browser']);
    if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
            $version= $matches['version'][0];
        }
        else {
            $version= $matches['version'][1];
        }
    }
    else {
        $version= $matches['version'][0];
    }
    
    // check if we have a number
    if ($version==null || $version=="") {$version="?";}
    
    return array(
        'userAgent' => $u_agent,
        'name'      => $bname,
        'version'   => $version,
        'platform'  => $platform,
        'pattern'    => $pattern,
        'mobile'	=> $mobile
    );
}

function getArrayConteudos($sqlWhereIdioma){
	$arrayAux = array();

	$itens = new conteudos();
	$itens = $itens->get_array_ativos($sqlWhereIdioma);

	if(is_array($itens) && sizeof($itens) > 0){
		foreach($itens as $it){
			if($it != null){

				$ref = stripslashes(get_output($it->get_var("nome_arquivo")));
				$titulo = stripslashes(get_output($it->get_var("titulo")));
				$subTitulo = stripslashes(get_output($it->get_var("subtitulo")));
				$imagemTitulo = stripslashes(get_output($it->get_var("banner_topo")));
				$descricao = stripslashes(get_output($it->get_var("texto")));
				$descricao_capa = stripslashes(get_output($it->get_var("texto_capa")));
				$meta_descricao = stripslashes(get_output($it->get_var("meta_descricao")));
				$meta_palavras_chave = stripslashes(get_output($it->get_var("meta_palavras_chave")));
				
				$descricao = str_replace("[ROOT_SERVER]",ROOT_SERVER,$descricao);
                $descricao = str_replace("[ROOT_CMS]",ROOT_CMS,$descricao);
                $descricao = str_replace("[ROOT_SITE]",ROOT_SITE,$descricao);
                $descricao = str_replace("[ROOT]",ROOT,$descricao);
                $descricao = str_replace("[UPLOAD_FOLDER]",UPLOAD_FOLDER,$descricao);
				
				$descricao_capa = str_replace("[ROOT_SERVER]",ROOT_SERVER,$descricao_capa);
                $descricao_capa = str_replace("[ROOT_CMS]",ROOT_CMS,$descricao_capa);
                $descricao_capa = str_replace("[ROOT_SITE]",ROOT_SITE,$descricao_capa);
                $descricao_capa = str_replace("[ROOT]",ROOT,$descricao_capa);
                $descricao_capa = str_replace("[UPLOAD_FOLDER]",UPLOAD_FOLDER,$descricao_capa);

				$arrayAux[$ref]["ref"] = $ref;
				$arrayAux[$ref]["titulo"] = $titulo;
				$arrayAux[$ref]["subTitulo"] = $subTitulo;
				$arrayAux[$ref]["imagemTitulo"] = $imagemTitulo;
				$arrayAux[$ref]["descricao_capa"] = $descricao_capa;
				$arrayAux[$ref]["descricao"] = $descricao;
				$arrayAux[$ref]["meta_descricao"] = $meta_descricao;
				$arrayAux[$ref]["meta_palavras_chave"] = $meta_palavras_chave;

				// MANTENDO O ANTIGO PARA NAO PREJUDICAR O JA DESENVOLVIDO E ADICIONADO TODOS OS CAMPOS ABAIXO

				$keys = array_keys(get_class_vars("conteudos_model"));

				foreach($keys as $k){
					$arrayAux[$ref][$k] = @stripslashes(get_output($it->get_var($k)));
				}
			}
		}
	}

	return $arrayAux;
}

function getArrayLinks($sqlWhereIdioma){
	$arrayAux = array();

	$itens = new links_uteis();
	$itens = $itens->get_array_ativos($sqlWhereIdioma);

	if(is_array($itens) && sizeof($itens) > 0){
		foreach($itens as $it){
			if($it != null){

				$ref = stripslashes(get_output($it->get_var("referencia")));
				$titulo = stripslashes(get_output($it->get_var("titulo")));
				$url = stripslashes(get_output($it->get_var("url")));
				$target = stripslashes(get_output($it->get_var("target")));
				
                $url = str_replace("[ROOT_SERVER]",ROOT_SERVER,$url);
                $url = str_replace("[ROOT_CMS]",ROOT_CMS,$url);
                $url = str_replace("[ROOT_SITE]",ROOT_SITE,$url);
                $url = str_replace("[ROOT]",ROOT,$url);
                $url = str_replace("[UPLOAD_FOLDER]",UPLOAD_FOLDER,$url);

				$arrayAux[$ref]["ref"] = $ref;
				$arrayAux[$ref]["titulo"] = $titulo;
				$arrayAux[$ref]["url"] = $url;
				$arrayAux[$ref]["target"] = $target;
			}
		}
	}

	return $arrayAux;
}

function toDouble($value,$decimal_char = ",",$towsand_char = "."){
	return str_replace($decimal_char,".",str_replace($towsand_char,"",$value));
}

function retorna_nome_mes($mes, $tipo = 1)
{
	// tipo: 1 = abreviado - 2 = completo
	switch ( $mes ) {
		case 1:		$mes = "Janeiro";		break;
		case 2:		$mes = "Fevereiro";		break;
		case 3:		$mes = "Março";			break;
		case 4:		$mes = "Abril";			break;
		case 5:		$mes = "Maio";			break;
		case 6:		$mes = "Junho";			break;
		case 7:		$mes = "Julho";			break;
		case 8:		$mes = "Agosto";		break;
		case 9:		$mes = "Setembro";		break;
		case 10:	$mes = "Outubro";		break;
		case 11:	$mes = "Novembro";		break;
		case 12:	$mes = "Dezembro";		break;
	}
	if ( $tipo == 1 ) {
		$mes = substr($mes,0,3);
	}
	return $mes;
}

function verifica_logado_cms()
{
	if(!isset($_SESSION["idLogin"]) || $_SESSION["idLogin"] == false || intval($_SESSION["idLogin"]) == 0 || $_SESSION["idLogin"] == null){
		$_SESSION["idLogin"] = null;
		return false;	
	}
	else{
		return true;
	}
}

function verifica_logado_site()
{
	if(!isset($_SESSION["btLogin_site"]) || $_SESSION["btLogin_site"] == false){
		$_SESSION["btLogin_site"] = false;
		return false;	
	}
	else{
		return true;
	}
}

function retorna_extensao($arquivo)
{
	$tam = strlen($arquivo);

	if( $arquivo[($tam)-4] == '.' )
	{
		$extensao = substr($arquivo,-3);
	}
	elseif( $arquivo[($tam)-5] == '.' )
	{
		$extensao = substr($arquivo,-4);
	}
	elseif( $arquivo[($tam)-3] == '.' )
	{
		$extensao = substr($arquivo,-2);
	}
	else
	{
		$extensao = NULL;
	}
	return $extensao;
}

function retorna_tamanho($filename)
{
	$bytes = filesize($filename);
    $bytes = floatval($bytes);
    $arBytes = array(
        0 => array(
            "UNIT" => "TB",
            "VALUE" => pow(1024, 4)
        ),
        1 => array(
            "UNIT" => "GB",
            "VALUE" => pow(1024, 3)
        ),
        2 => array(
            "UNIT" => "MB",
            "VALUE" => pow(1024, 2)
        ),
        3 => array(
            "UNIT" => "KB",
            "VALUE" => 1024
        ),
        4 => array(
            "UNIT" => "B",
            "VALUE" => 1
        ),
    );

    foreach($arBytes as $arItem)
    {
        if($bytes >= $arItem["VALUE"])
        {
            $result = $bytes / $arItem["VALUE"];
            $result = str_replace(".", "," , strval(round($result, 2)))." ".$arItem["UNIT"];
            break;
        }
    }
    return $result;
}

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
//use PHPMailer\PHPMailer\Exception;

function sendMailSMTP($De,$Para,$Assunto,$Conteudo,$ReplyTo = "",$ReplyToName = "",$Notificacao="N")
{
	global $_CONFIG;

	//require_once (ROOT_CMS . "classes/vendor/PHPMailerAutoload.php");

	// Load Composer's autoloader
	require ROOT_CMS . 'vendor/autoload.php';

	$mail = new PHPMailer(false); // Without Exceptions

	//$mail->SMTPDebug = 2;

	if(intval($_CONFIG["smtp_debug"]) > 0) {
		$mail->SMTPDebug = $_CONFIG["smtp_debug"];
	} else {
		$mail->SMTPDebug = false;
	}

	$mail->CharSet = 'UTF-8';
	$mail->isSMTP();                                      // Set mailer to use SMTP
	$mail->Host = $_CONFIG["servidor_smtp"];  // Specify main and backup server
	$mail->Port = $_CONFIG["porta_smtp"];
	$mail->SMTPAutoTLS = false;

	if(trim($_CONFIG["usuario_email"]) != "") {
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = $_CONFIG["usuario_email"];                            // SMTP username
		$mail->Password = $_CONFIG["senha_email"];                           // SMTP password
		if(isset($_CONFIG["secure_smtp"])) {
			$mail->SMTPSecure = $_CONFIG["secure_smtp"];
		} else {
			//$mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted
		}
	}

	$mail->From = $De; //'maybee@maybee.com.br';
	$mail->Sender = $De; //'maybee@maybee.com.br';
	$mail->FromName = $_CONFIG["nome_default_from"];

	/*$array_para = explode(";",$Para);
	if(is_array($array_para)) {
		foreach($array_para as $Para)
		{
			$mail->addBCC($Para);
		}
	} else {
		$mail->addAddress($Para); //'ellen@example.com');               // Name is optional
	}*/
	//$mail->addAddress('josh@example.net', 'Josh Adams');  // Add a recipient
	//$mail->addReplyTo('info@example.com', 'Information');
	//$mail->addCC('cc@example.com');
	//$mail->addBCC('bcc@example.com');

	$mail->WordWrap = 50;                                 // Set word wrap to 50 characters
	//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
	//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
	$mail->isHTML(true);                                  // Set email format to HTML

	$mail->Subject = $Assunto; //'Here is the subject';
	$mail->Body    = $Conteudo; //'This is the HTML message body <b>in bold!</b>';
	//$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

	$mailSents = array();
	$mailErrors = array();

	$array_para = explode(";",$Para);
	if(is_array($array_para)) {
		foreach($array_para as $Para)
		{
			$mail->ClearAllRecipients();
			$mail->addAddress($Para);
			if(trim($ReplyTo) != "") {
				if(trim($ReplyToName) != "") {
					$mail->addReplyTo($ReplyTo, $ReplyToName);
				} else {
					$mail->addReplyTo($ReplyTo);
				}
			}
			$mailSents[sizeof($mailSents)] = $mail->send();
			$mailErrors[sizeof($mailErrors)] = $mail->ErrorInfo;
		}
	} else {
		$mail->ClearAllRecipients();
		$mail->addAddress($Para); //'ellen@example.com');               // Name is optional
		if(trim($ReplyTo) != "") {
			if(trim($ReplyToName) != "") {
				$mail->addReplyTo($ReplyTo, $ReplyToName);
			} else {
				$mail->addReplyTo($ReplyTo);
			}
		}
		$mailSents = $mail->send();
		$mailErrors = $mail->ErrorInfo;
	}
	//$mailSent = $mail->send();
	
	$mail->ClearAllRecipients();
	$mail->ClearAttachments();

	if(is_array($mailSents)){
		if(!in_array(false, $mailSents)) {
			return true;
		} else {
		   	//echo 'Message could not be sent.<br/>';
			//echo 'Mailer Error: ' . $mail->ErrorInfo;

			return false;
		}
	} else {
		if($mailSents) {
			return true;
		} else {
		   	//echo 'Message could not be sent.<br/>';
			//echo 'Mailer Error: ' . $mail->ErrorInfo;

			return false;
		}
	}
}

function isMobile(){
	$browser = getBrowser();

	return $browser["mobile"];
}

function normaliza_nome($nameAux) {
	$finalName = $nameAux;
	$arrAux = explode(" ",$nameAux);
	if(is_array($arrAux) && sizeof($arrAux) > 0) {
		$finalName = "";
		foreach($arrAux as $name) {
			if(trim($finalName) != "") { $finalName .= " "; }
			if(trim($name) == "de" ||
			   trim($name) == "da" ||
			   trim($name) == "do" ||
			   trim($name) == "das" ||
			   trim($name) == "dos") {
				$finalName .= $name;
			} else {
				$finalName .= ucfirst($name);
			}
		}
	}
	return $finalName;
}

function getNomeExtArquivo($nome_arquivo) {
    $array_aux = explode(".",$nome_arquivo);
    $ext = $array_aux[sizeof($array_aux)-1];
    $name = "";
    for($w=0;$w<sizeof($array_aux)-1;$w++) {
        if(trim($name) != "") {
            $name .= ".";
        }
        $name .= $array_aux[$w];
    }

    return array("name" => $name,
                 "ext" => $ext);
} 

/** 
* PNG ALPHA CHANNEL SUPPORT for imagecopymerge(); 
* by Sina Salek 
* 
* Bugfix by Ralph Voigt (bug which causes it 
* to work only for $src_x = $src_y = 0. 
* Also, inverting opacity is not necessary.) 
* 08-JAN-2011 
* 
**/ 
function imagecopymerge_alpha($dst_im, $src_im, $dst_x, $dst_y, $src_x, $src_y, $src_w, $src_h, $pct){ 
    // creating a cut resource 
    $cut = imagecreatetruecolor($src_w, $src_h); 

    // copying relevant section from background to the cut resource 
    imagecopy($cut, $dst_im, 0, 0, $dst_x, $dst_y, $src_w, $src_h); 
    
    // copying relevant section from watermark to the cut resource 
    imagecopy($cut, $src_im, 0, 0, $src_x, $src_y, $src_w, $src_h); 
    
    // insert cut resource to destination image 
    imagecopymerge($dst_im, $cut, $dst_x, $dst_y, 0, 0, $src_w, $src_h, $pct); 
} 

function get_attr_from_tag($tag, $param) {
	preg_match('/' . $param . '="([^"]+)"/', $tag, $match);
	return $match[1];
}

function get_youtube_id_from_url($url) {
    preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url, $match);
    return $match[1];
}

function get_file_extension_image($file) {
	$folderAux = ROOT . "_estilo/images/file-extensions/";
	$strAux = $folderAux . "icon_generic.png";
	if(file_exists($file)) {
		$arrAux = explode(".",$file);
		$extAux = $arrAux[sizeof($arrAux)-1];
		switch(trim(strtoupper($extAux))) {
			default:
				$strAux = $folderAux . "icon_generic.png";
				break;
			//case "JPG":
			//case "JPEG":
			//case "PNG":
			case "BMP":
			case "GIF":
			case "TIFF":
				$strAux = $file;
				break;
			case "JPG":
			case "JPEG":
				$strAux = $folderAux . "icon_jpg.png";
				break;
			case "PNG":
				$strAux = $folderAux . "icon_png.png";
				break;
			case "AI":
				$strAux = $folderAux . "icon_ai.png";
				break;
			case "CSS":
				$strAux = $folderAux . "icon_css.png";
				break;
			case "DOC":
				$strAux = $folderAux . "icon_doc.png";
				break;
			case "HTML":
				$strAux = $folderAux . "icon_html.png";
				break;
			case "ID":
				$strAux = $folderAux . "icon_id.png";
				break;
			case "MP3":
				$strAux = $folderAux . "icon_mp3.png";
				break;
			case "PDF":
				$strAux = $folderAux . "icon_pdf.png";
				break;
			case "PPT":
				$strAux = $folderAux . "icon_ppt.png";
				break;
			case "PSD":
				$strAux = $folderAux . "icon_psd.png";
				break;
			case "XLS":
				$strAux = $folderAux . "icon_xls.png";
				break;
			case "TXT":
				$strAux = $folderAux . "icon_txt.png";
				break;
		}
	}
	return $strAux;
}

function push_notify($notify_tipo = "", $notify_rids = "", $notify_titulo = "", $notify_descricao = "", $notify_data = array()) {
	$msg = array
	(
		'title'				=> $notify_titulo,
		'body'				=> html_entity_decode(htmlspecialchars_decode(strip_tags($notify_descricao))),
		'detail'			=> html_entity_decode(htmlspecialchars_decode(strip_tags($notify_descricao))),
		'sound'				=> 'default',
		'click_action'		=> 'FCM_PLUGIN_ACTIVITY',  //Must be present for Android
		'icon'				=> 'fcm_push_icon',
		'content-available' => 1
	);

	$notify_data["tipo"] = $notify_tipo;
	$notify_data["visibility"] = 1;
	/*$data = array (
		"tipo"				=> $notify_tipo
	);*/

	$data = $notify_data;

	if(is_array($notify_rids)) {
		$array_ids = $notify_rids;
	} else {
		$array_ids = explode(",",$notify_rids);
	}
	$condition = "";
	foreach($array_ids as $idAux) {
		if(trim($condition) != "") { $condition .= " || ";}
		$condition .= "'" . $idAux . "' in topics";
	}
	
	$fields = array(
		//'registration_ids'  => explode(";", $notify_rids),
		'condition'  		=> $condition,
		'notification'		=> $msg,
		'data'				=> $data,
		'priority'			=> "high",
		'content_available'	=> true
	);
				
	$headers = array(
		'Authorization: key=' . PUSH_SERVER_KEY,
		'Content-Type: application/json; charset=UTF-8'
	);

	//var_dump($headers); die();
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://fcm.googleapis.com/fcm/send");
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

	$result = curl_exec($ch);
	if ($result === FALSE) {
		die('Problem occurred: ' . curl_error($ch));
	} else {
		//var_dump($result); die();
	}

// DEBUG DO PUSH
	/*// // "a" representa que o arquivo é aberto para ser escrito
	$fp = fopen("push_data.txt", "a");
	 
	// // Escreve "exemplo de escrita" no bloco1.txt
	//$escreve = fwrite($fp, file_get_contents('php://input'));
	$escreve = fwrite($fp, print_r($result, true));
	 
	// // Fecha o arquivo
	fclose($fp);*/
// FIM DEBUG

	curl_close($ch);
}

function get_country_lang() {
	$xml = @simplexml_load_file("http://www.geoplugin.net/xml.gp?ip=".get_ip());
	$lang = "";
	try 
	{
		if(trim($xml->geoplugin_countryCode) != "") {
			switch($xml->geoplugin_countryCode)
			{
				case "BR":
				case "PT":
				case "AO":
				case "MZ":
				case "CV":
				case "TL":
				case "MO":
					$lang = "pt-br";
					break;
				default:
					$lang = "en";
					break;
			}
		}
	}
	catch(Exception $e) {
		
	}

	if(_bot_detected()) {$lang = "";}
	
	return $lang;
}

function normaliza_url_vars($strAux) {				
	$strAux = str_replace("[ROOT_SERVER]",ROOT_SERVER,$strAux);
	$strAux = str_replace("[ROOT_CMS]",ROOT_CMS,$strAux);
	$strAux = str_replace("[ROOT_SITE]",ROOT_SITE,$strAux);
	$strAux = str_replace("[ROOT]",ROOT,$strAux);
	$strAux = str_replace("[UPLOAD_FOLDER]",UPLOAD_FOLDER,$strAux);
	return $strAux;
}

function phone_number_format($number) {
	$return = $number;
	
	// Allow only Digits, remove all other characters.
	$number = preg_replace("/[^\d]/","",$number);

	if(substr($number, 0, 1) == "0") {
		$number = substr($number,1);
	}	 

	$tam = strlen(preg_replace("/[^0-9]/", "", $number));
	if ($tam == 13) { // COM CÓDIGO DE ÁREA NACIONAL E DO PAIS e 9 dígitos
		$return = "+".substr($number,0,$tam-11)."(".substr($number,$tam-11,2).")".substr($number,$tam-9,5)."-".substr($number,-4);
	}
	if ($tam == 12) { // COM CÓDIGO DE ÁREA NACIONAL E DO PAIS
		$return = "+".substr($number,0,$tam-10)."(".substr($number,$tam-10,2).")".substr($number,$tam-8,4)."-".substr($number,-4);
	}
	if ($tam == 11) { // COM CÓDIGO DE ÁREA NACIONAL e 9 dígitos
		$return = "(".substr($number,0,2).")".substr($number,2,5)."-".substr($number,7,11);
	}
	if ($tam == 10) { // COM CÓDIGO DE ÁREA NACIONAL
		$return = "(".substr($number,0,2).")".substr($number,2,4)."-".substr($number,6,10);
	}
	if ($tam <= 9) { // SEM CÓDIGO DE ÁREA
		$return = substr($number,0,$tam-4)."-".substr($number,-4);
	}
	
	return $return; 
}

function state_format($state = "") {
	$return = "-";
	$states = get_array_estados();
	$state = trim(strtolower(remove_acentos(remove_caracteres_especiais($state))));
	foreach($states as $st) {
		if(trim(strtolower(remove_acentos(remove_caracteres_especiais($st[1])))) == $state || trim(strtolower(remove_acentos(remove_caracteres_especiais($st[0])))) == $state) {
			$return = $st[0];
			break;
		}
	}
	return $return;
}

function card_flag($cc, $extra_check = false){
    $cards = array(
        "visa" => "(4\d{12}(?:\d{3})?)",
        "amex" => "(3[47]\d{13})",
        "jcb" => "(35[2-8][89]\d\d\d{10})",
        "maestro" => "((?:5020|5038|6304|6579|6761)\d{12}(?:\d\d)?)",
        "solo" => "((?:6334|6767)\d{12}(?:\d\d)?\d?)",
        "mastercard" => "(5[1-5]\d{14})",
		"switch" => "(?:(?:(?:4903|4905|4911|4936|6333|6759)\d{12})|(?:(?:564182|633110)\d{10})(\d\d)?\d?)",		
		"diners" => "/^3(0[0-5]|[68]\d)\d{11}$/",
		"discover" => "/^6(?:011|5[0-9]{2})[0-9]{12}$/",
		"elo" => "^(40117[8-9]|431274|438935|451416|457393|45763[1-2]|506(699|7[0-6][0-9]|77[0-8])|509\d{3}|504175|627780|636297|636368|65003[1-3]|6500(3[5-9]|4[0-9]|5[0-1])|6504(0[5-9]|[1-3][0-9])|650(4[8-9][0-9]|5[0-2][0-9]|53[0-8])|6505(4[1-9]|[5-8][0-9]|9[0-8])|6507(0[0-9]|1[0-8])|65072[0-7]|6509(0[1-9]|1[0-9]|20)|6516(5[2-9]|[6-7][0-9])|6550([0-1][0-9]|2[1-9]|[3-4][0-9]|5[0-8]))",
		"aura" => "/^(5078\d{2})(\d{2})(\d{11})$/",
		"hipercard" => "/^(606282\d{10}(\d{3})?)|(3841\d{15})$/"
    );
    $names = array("Visa", "American Express", "JCB", "Maestro", "Solo", "Mastercard", "Switch", "Diners", "Discover", "Elo", "Aura", "Hipercard");
    $matches = array();
    $pattern = "#^(?:".implode("|", $cards).")$#";
    $result = preg_match($pattern, str_replace(" ", "", $cc), $matches);
    if($extra_check && $result > 0){
        $result = (validatecard($cc))?1:0;
    }
    return ($result>0)?$names[sizeof($matches)-2]:false;
}

?>