<?php

include_once("config.php");

include_once("verificaLogado.php");

define("_UZCMS_URL_FACEBOOK","http://www.facebook.com/agenciauzzye");
define("_UZCMS_URL_TWITTER","http://www.twitter.com/agenciauzzye");
define("_UZCMS_URL_SITE","http://uzzye.com/");
define("_UZCMS_EMAIL","support@uzzye.com");
define("_UZCMS_PHONE","+55 54 98116 8709");

$modulo = $_CMS_CONFIG["modulo_padrao"];
if(trim($_REQUEST["modulo"]) <> "")
{   
    $modulo = trim($_REQUEST["modulo"]);    
}

$acao = $_CMS_CONFIG["acao_padrao"];
if(trim($_REQUEST["acao"]) <> "")
{   
    $acao = trim($_REQUEST["acao"]);    
}

$status_operacao = "";
if(trim($_REQUEST["status_operacao"]) <> "")
{   
    $status_operacao = trim($_REQUEST["status_operacao"]);  
}

$id = null;
if(trim($_REQUEST["id"]) <> "")
{   
    $id = trim($_REQUEST["id"]);  
}

if(file_exists(ROOT_CMS . "classes/models/" . $modulo . "_model.php")){
include_once(ROOT_CMS . "classes/models/" . $modulo . "_model.php");}
if(file_exists(ROOT_CMS . "classes/views/" . $modulo . "_view.php")){
include_once(ROOT_CMS . "classes/views/" . $modulo . "_view.php");}
if(file_exists(ROOT_CMS . "classes/controllers/" . $modulo . "_controller.php")){
include_once(ROOT_CMS . "classes/controllers/" . $modulo . "_controller.php");}

ob_clean();

?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7" xmlns="http://www.w3.org/1999/xhtml" lang="<?php echo $_CONFIG["lang"]; ?>"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie10 lt-ie9 lt-ie8" xmlns="http://www.w3.org/1999/xhtml" lang="<?php echo $_CONFIG["lang"]; ?>"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie10 lt-ie9" xmlns="http://www.w3.org/1999/xhtml" lang="<?php echo $_CONFIG["lang"]; ?>"><![endif]-->
<!--[if IE 9]><html class="no-js lt-ie10" xmlns="http://www.w3.org/1999/xhtml" lang="<?php echo $_CONFIG["lang"]; ?>"><![endif]-->
<!--[if gt IE 9]><!--><html class="no-js ie-10" xmlns="http://www.w3.org/1999/xhtml" lang="<?php echo $_CONFIG["lang"]; ?>" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml"><!--<![endif]-->
<head prefix="og: http://ogp.me/ns#">
    <title>UzCMS 3 | <?php echo $_CONFIG["titulo"]; ?></title>
    <link rel="apple-touch-icon" href="<?php echo ROOT_SERVER . ROOT; ?>_estilo/images/touchIcon.png" />
    <!-- MetaTags -->
    <meta name="theme-color" content="<?php echo $projectColors["theme"]; ?>" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Cache-Control" content="max-age=0, public" />
    <meta name="author" content="Uzzye" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1.5" />
    <meta name="apple-mobile-web-app-capable" content="yes" /> 
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="resource-types" content="document" /> 
    <meta name="classification" content="Internet" /> 
    <meta name="abstract" content="UzCMS. Gerenciador de conteúdo online. Uzzye Com. Dig. Ltda." /> 
    <meta name="description" content="UzCMS. Gerenciador de conteúdo online. Uzzye Com. Dig. Ltda." /> 
    <meta name="keywords" content="uzcms, cms, gerenciador, conteudo, content, inbound, uzzye, caxias do sul, caxias, serra, gaucha, rs, rio grande do sul, brasil, mauricio, salamon, mausalamon"/> 
    <meta name="robots" content="NONE" /> 
    <meta name="distribution" content="Local" /> 
    <meta name="rating" content="General" /> 
    <meta name="author" content="Uzzye" /> 
    <link rel="shortcut icon" href="<?php echo ROOT_SERVER . ROOT; ?>favicon.ico?<?php echo Date("YmdHis"); ?>" type="image/x-icon" />
    <link rel="icon" href="<?php echo ROOT_SERVER . ROOT; ?>favicon.png" />
    <?php /*<link rel="image_src" href="<?php echo ROOT_SERVER . ROOT; ?>_estilo/images/logo_cms.png" />
    <meta property="og:image" content="<?php echo ROOT_SERVER . ROOT; ?>_estilo/images/logo_cms.png" />
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="470">
    <meta property="og:image:height" content="246">*/ ?>
    <!-- Geolocalização -->
    <link rel="alternate" hreflang="pt" href="<?php echo ROOT_SERVER . ROOT; ?>" />
    <?php if(trim($_CONFIG["geolocalizacao"]) <> ""){?>
    <?php echo $_CONFIG["geolocalizacao"]; ?>
    <?php } ?>
    <!-- JavaScript -->
    <script language="javascript">  
        var ROOT_CMS = "<?php echo ROOT_CMS; ?>";
        var ROOT_SITE = "<?php echo ROOT_SITE; ?>";
        var ROOT_SERVER = "<?php echo ROOT_SERVER; ?>";
        var ROOT = "<?php echo ROOT; ?>";   
        var UPLOAD_FOLDER = "<?php echo UPLOAD_FOLDER; ?>";
        var CMS_MODULO = "<?php echo $modulo; ?>";
        var CMS_ACAO = "<?php echo $acao; ?>";
    </script>
    <?php if(file_exists(ROOT_CMS . "lang/" . $_CONFIG["lang"] . ".js.php")) {
		include_once(ROOT_CMS . "lang/" . $_CONFIG["lang"] . ".js.php");
	} ?>
        
    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT; ?>_estilo/uz.min.css" media="all" />
    <!--[if IE 7]>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_SERVER . ROOT; ?>_estilo/iehacks/ie7hacks.css" />
    <![endif]-->
    <!--[if IE]>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_SERVER . ROOT; ?>_estilo/iehacks/iehacks.css" />
    <style type="text/css">
        body { behavior:url(<?php echo ROOT_SERVER . ROOT; ?>_estilo/iehacks/csshover3.htc); }
    </style>
    <![endif]-->
</head>
<body>
    <!--Preloader-->
    <!-- <div class="preloader-it">
        <div class="la-anim-1"></div>
    </div> -->
    <!--/Preloader-->

    <div id="loader-wrapper">
        <div id="loader"></div>
     
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
     
    </div>

    <div class="pageSize"></div>
    <div class="page wrapper">
        <?php

        /*$strNavegacao = "";

        $strHome = "<a href=\"" . ROOT_CMS . "index.php\">" . (_HOME) . "</a> > ";

        $modAux = new cms_modulos_controller();             
        $strNavegacao = $modAux->get_bread_crumbs($modulo);

        echo $strHome;

        echo $strNavegacao;

        ?>
        <span class="local">
            <?php echo (get_texto_acao($acao))?>
        </span>
        
        <?php*/

        include_once(ROOT_CMS . "menu.php");

        ?>
        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid" id="cp-main">
                <?php
                            
                // Load de módulo interno executado no ajax.php, aqui somente logout

                if($acao == "logout") {
                    $error = "";
                    if(file_exists(ROOT_CMS . "classes/" . $modulo . ".php"))
                    {
                        $classe = $modulo."_controller";                
                        $controller = new $classe();
                        if(trim($acao) == "")
                        {$acao = "acaoPadrao";}

                        if(!method_exists($classe,"valida_permissao") || $controller->valida_permissao($modulo))
                        {   
                            if(method_exists($classe,$acao))
                            {           
                                $controller->$acao();
                            }       
                            else if(method_exists($classe,"acaoPadrao"))
                            {           
                                $controller->acaoPadrao();
                            }
                        } else {
                            $error = "401";
                        }
                    } else {
                        $error = "401";
                    } 

                    if(trim($error) != "") {
                        ?>
                        <iframe class="error-frame fullScreen" src="<?php echo ROOT_SERVER . ROOT; ?>401.html"></iframe>
                        <?php
                    }
                }

                ?>
            </div>
            
            <!-- Footer -->
            <footer class="footer container-fluid pl-30 pr-30">
                <div class="row">
                    <div class="col-sm-5">
                        <a href="<?php echo _UZCMS_URL_SITE; ?>" class="brand mr-30"><img src="<?php echo ROOT_SERVER . ROOT; ?>_estilo/images/logo-uzzye-sm.png" alt="logo"/></a>
                        <ul class="footer-link nav navbar-nav">
                            <?php /*<li class="logo-footer"><a href="<?php echo _UZCMS_URL_SITE; ?>/contato">Site</a></li>*/ ?>
                            <?php
                            /*<li class="logo-footer"><a href="<?php echo _UZCMS_URL_SITE; ?>/termos">Termos</a></li>*/
                            /*<li class="logo-footer"><a href="<?php echo _UZCMS_URL_SITE; ?>/privacidade">Privacidade</a></li>*/
                            ?>
                            <li class="logo-footer"><a class="pl-5 pr-5" href="<?php echo _UZCMS_URL_FACEBOOK; ?>">Facebook</a></li>
                            <li class="logo-footer"><a class="pl-5 pr-5" href="mailto:<?php echo _UZCMS_EMAIL; ?>">Email</a></li>
                            <li class="logo-footer"><a class="pl-5 pr-5" href="phone:<?php echo str_replace(" ","",_UZCMS_PHONE); ?>">Phone</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-7 text-right">
                        <p>UzCMS 3 2019 &copy; Uzzye. All rights reserved.</p>
                        <!-- Customizado de Kenny - Pampered by Hencework -->
                    </div>
                </div>
            </footer>
            <!-- /Footer -->
        
        </div>
        <!-- /Main Content -->
    
    </div>
    <!-- /#wrapper -->

    <!-- Cropping modal -->
    <div class="container" id="crop-cropper">
        <div class="modal fade modal-crop" id="cropper-modal" aria-hidden="true" aria-labelledby="cropper-modal-label" role="dialog" tabindex="-1">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="cropper-body">
                <form
                  name="form_cropper"
                  id="form_cropper"
                  method="POST"
                  action="javascript:void(0);"
                  data-url="<?php echo ROOT_SERVER . ROOT; ?>tools/crop"
                  class="cropper-form"
                  enctype="multipart/form-data">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="cropper-modal-label"><?php echo get_lang("_TITULOCROP"); ?> <small id="crop-label"></small></h4>
                  </div>
                  <div class="modal-body" style="max-width: 1920px; margin: auto;">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-md-8">
                                <!-- <h3 class="page-header">Demo:</h3> -->
                                <div class="img-container" id="cropper-img-container">
                                    <img src="<?php echo ROOT_SERVER . ROOT; ?>_estilo/images/blank.gif" alt="Picture">
                                </div>

                                <!-- Row -->
                                <div id="crop-actions">
                                    <div class="docs-buttons">
                                        <!-- <h3 class="page-header">Toolbar:</h3> -->
                                        <?php /*<div class="btn-group">
                                            <button type="button" class="btn btn-primary" data-method="setDragMode" data-option="move" title="Move">
                                            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.setDragMode(&quot;move&quot;)">
                                            <span class="fa fa-arrows"></span>
                                            </span>
                                            </button>
                                            <button type="button" class="btn btn-primary" data-method="setDragMode" data-option="crop" title="Crop">
                                            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.setDragMode(&quot;crop&quot;)">
                                            <span class="fa fa-crop"></span>
                                            </span>
                                            </button>
                                        </div>*/ ?>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-primary" data-method="zoom" data-option="0.1" title="Zoom In">
                                            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.zoom(0.1)">
                                            <span class="fa fa-search-plus"></span>
                                            </span>
                                            </button>
                                            <button type="button" class="btn btn-primary" data-method="zoom" data-option="-0.1" title="Zoom Out">
                                            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.zoom(-0.1)">
                                            <span class="fa fa-search-minus"></span>
                                            </span>
                                            </button>
                                        </div>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-primary" data-method="move" data-option="-10" data-second-option="0" title="Move Left">
                                            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.move(-10, 0)">
                                            <span class="fa fa-arrow-left"></span>
                                            </span>
                                            </button>
                                            <button type="button" class="btn btn-primary" data-method="move" data-option="10" data-second-option="0" title="Move Right">
                                            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.move(10, 0)">
                                            <span class="fa fa-arrow-right"></span>
                                            </span>
                                            </button>
                                            <button type="button" class="btn btn-primary" data-method="move" data-option="0" data-second-option="-10" title="Move Up">
                                            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.move(0, -10)">
                                            <span class="fa fa-arrow-up"></span>
                                            </span>
                                            </button>
                                            <button type="button" class="btn btn-primary" data-method="move" data-option="0" data-second-option="10" title="Move Down">
                                            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.move(0, 10)">
                                            <span class="fa fa-arrow-down"></span>
                                            </span>
                                            </button>
                                        </div>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-primary" data-method="rotate" data-option="-45" title="Rotate Left">
                                            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.rotate(-45)">
                                            <span class="fa fa-rotate-left"></span>
                                            </span>
                                            </button>
                                            <button type="button" class="btn btn-primary" data-method="rotate" data-option="45" title="Rotate Right">
                                            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.rotate(45)">
                                            <span class="fa fa-rotate-right"></span>
                                            </span>
                                            </button>
                                        </div>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-primary" data-method="scaleX" data-option="-1" title="Flip Horizontal">
                                            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.scaleX(-1)">
                                            <span class="fa fa-arrows-h"></span>
                                            </span>
                                            </button>
                                            <button type="button" class="btn btn-primary" data-method="scaleY" data-option="-1" title="Flip Vertical">
                                            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.scaleY(-1)">
                                            <span class="fa fa-arrows-v"></span>
                                            </span>
                                            </button>
                                        </div>
                                        <?php /*<div class="btn-group">
                                            <button type="button" class="btn btn-primary" data-method="crop" title="Crop">
                                            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.crop()">
                                            <span class="fa fa-check"></span>
                                            </span>
                                            </button>
                                            <button type="button" class="btn btn-primary" data-method="clear" title="Clear">
                                            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.clear()">
                                            <span class="fa fa-remove"></span>
                                            </span>
                                            </button>
                                        </div>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-primary" data-method="disable" title="Disable">
                                            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.disable()">
                                            <span class="fa fa-lock"></span>
                                            </span>
                                            </button>
                                            <button type="button" class="btn btn-primary" data-method="enable" title="Enable">
                                            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.enable()">
                                            <span class="fa fa-unlock"></span>
                                            </span>
                                            </button>
                                        </div>*/ ?>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-primary" data-method="reset" title="Reset">
                                            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.reset()">
                                            <span class="fa fa-refresh"></span>
                                            </span>
                                            </button>
                                            <?php /*<label class="btn btn-primary btn-upload" for="inputImage" title="Upload image file">
                                            <input type="file" class="sr-only" id="inputImage" name="file" accept="image/*">
                                            <span class="docs-tooltip" data-toggle="tooltip" title="Import image with Blob URLs">
                                            <span class="fa fa-upload"></span>
                                            </span>
                                            </label>
                                            <button type="button" class="btn btn-primary" data-method="destroy" title="Destroy">
                                            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.destroy()">
                                            <span class="fa fa-power-off"></span>
                                            </span>
                                            </button>*/ ?>
                                        </div>
                                        <?php /*<div class="btn-group btn-group-crop">
                                            <button type="button" class="btn btn-primary" data-method="getCroppedCanvas">
                                            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.getCroppedCanvas()">
                                            Get Cropped Canvas
                                            </span>
                                            </button>
                                            <button type="button" class="btn btn-primary" data-method="getCroppedCanvas" data-option="{ &quot;width&quot;: 160, &quot;height&quot;: 90 }">
                                            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.getCroppedCanvas({ width: 160, height: 90 })">
                                            160&times;90
                                            </span>
                                            </button>
                                            <button type="button" class="btn btn-primary" data-method="getCroppedCanvas" data-option="{ &quot;width&quot;: 320, &quot;height&quot;: 180 }">
                                            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.getCroppedCanvas({ width: 320, height: 180 })">
                                            320&times;180
                                            </span>
                                            </button>
                                        </div>*/ ?>
                                        <?phph /*<!-- Show the cropped image in modal -->
                                        <div class="modal fade docs-cropped" id="getCroppedCanvasModal" role="dialog" aria-hidden="true" aria-labelledby="getCroppedCanvasTitle" tabindex="-1">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <h4 class="modal-title" id="getCroppedCanvasTitle">Cropped</h4>
                                                    </div>
                                                    <div class="modal-body"></div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        <a class="btn btn-primary" id="crop-download" href="javascript:void(0);" download="cropped.html">Download</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.modal -->
                                        <button type="button" class="btn btn-primary" data-method="getData" data-option data-target="#putData">
                                        <span class="docs-tooltip" data-toggle="tooltip" title="cropper.getData()">
                                        Get Data
                                        </span>
                                        </button>
                                        <button type="button" class="btn btn-primary" data-method="setData" data-target="#putData">
                                        <span class="docs-tooltip" data-toggle="tooltip" title="cropper.setData(data)">
                                        Set Data
                                        </span>
                                        </button>
                                        <button type="button" class="btn btn-primary" data-method="getContainerData" data-option data-target="#putData">
                                        <span class="docs-tooltip" data-toggle="tooltip" title="cropper.getContainerData()">
                                        Get Container Data
                                        </span>
                                        </button>
                                        <button type="button" class="btn btn-primary" data-method="getImageData" data-option data-target="#putData">
                                        <span class="docs-tooltip" data-toggle="tooltip" title="cropper.getImageData()">
                                        Get Image Data
                                        </span>
                                        </button>
                                        <button type="button" class="btn btn-primary" data-method="getCanvasData" data-option data-target="#putData">
                                        <span class="docs-tooltip" data-toggle="tooltip" title="cropper.getCanvasData()">
                                        Get Canvas Data
                                        </span>
                                        </button>
                                        <button type="button" class="btn btn-primary" data-method="setCanvasData" data-target="#putData">
                                        <span class="docs-tooltip" data-toggle="tooltip" title="cropper.setCanvasData(data)">
                                        Set Canvas Data
                                        </span>
                                        </button>
                                        <button type="button" class="btn btn-primary" data-method="getCropBoxData" data-option data-target="#putData">
                                        <span class="docs-tooltip" data-toggle="tooltip" title="cropper.getCropBoxData()">
                                        Get Crop Box Data
                                        </span>
                                        </button>
                                        <button type="button" class="btn btn-primary" data-method="setCropBoxData" data-target="#putData">
                                        <span class="docs-tooltip" data-toggle="tooltip" title="cropper.setCropBoxData(data)">
                                        Set Crop Box Data
                                        </span>
                                        </button>
                                        <button type="button" class="btn btn-primary" data-method="moveTo" data-option="0">
                                        <span class="docs-tooltip" data-toggle="tooltip" title="cropper.moveTo(0)">
                                        0,0
                                        </span>
                                        </button>
                                        <button type="button" class="btn btn-primary" data-method="zoomTo" data-option="1">
                                        <span class="docs-tooltip" data-toggle="tooltip" title="cropper.zoomTo(1)">
                                        100%
                                        </span>
                                        </button>
                                        <button type="button" class="btn btn-primary" data-method="rotateTo" data-option="180">
                                        <span class="docs-tooltip" data-toggle="tooltip" title="cropper.rotateTo(180)">
                                        180°
                                        </span>
                                        </button>
                                        <input type="text" class="form-control" id="putData" placeholder="Get data to here or set data with this value">*/ ?>
                                    </div>
                                    <!-- /.docs-buttons -->
                                    <?php /*<div class="col-md-4 docs-toggles">
                                        <!-- <h3 class="page-header">Toggles:</h3> -->
                                        <div class="btn-group docs-aspect-ratios" data-toggle="buttons">
                                            <label class="btn btn-primary active">
                                            <input type="radio" class="sr-only" id="aspectRatio1" name="aspectRatio" value="1.7777777777777777">
                                            <span class="docs-tooltip" data-toggle="tooltip" title="aspectRatio: 16 / 9">
                                            16:9
                                            </span>
                                            </label>
                                            <label class="btn btn-primary">
                                            <input type="radio" class="sr-only" id="aspectRatio2" name="aspectRatio" value="1.3333333333333333">
                                            <span class="docs-tooltip" data-toggle="tooltip" title="aspectRatio: 4 / 3">
                                            4:3
                                            </span>
                                            </label>
                                            <label class="btn btn-primary">
                                            <input type="radio" class="sr-only" id="aspectRatio3" name="aspectRatio" value="1">
                                            <span class="docs-tooltip" data-toggle="tooltip" title="aspectRatio: 1 / 1">
                                            1:1
                                            </span>
                                            </label>
                                            <label class="btn btn-primary">
                                            <input type="radio" class="sr-only" id="aspectRatio4" name="aspectRatio" value="0.6666666666666666">
                                            <span class="docs-tooltip" data-toggle="tooltip" title="aspectRatio: 2 / 3">
                                            2:3
                                            </span>
                                            </label>
                                            <label class="btn btn-primary">
                                            <input type="radio" class="sr-only" id="aspectRatio5" name="aspectRatio" value="NaN">
                                            <span class="docs-tooltip" data-toggle="tooltip" title="aspectRatio: NaN">
                                            Free
                                            </span>
                                            </label>
                                        </div>*/ ?>
                                        <?php /*<div class="btn-group docs-view-modes" data-toggle="buttons">
                                            <label class="btn btn-primary active">
                                            <input type="radio" class="sr-only" id="viewMode0" name="viewMode" value="0" checked>
                                            <span class="docs-tooltip" data-toggle="tooltip" title="View Mode 0">
                                            VM0
                                            </span>
                                            </label>
                                            <label class="btn btn-primary">
                                            <input type="radio" class="sr-only" id="viewMode1" name="viewMode" value="1">
                                            <span class="docs-tooltip" data-toggle="tooltip" title="View Mode 1">
                                            VM1
                                            </span>
                                            </label>
                                            <label class="btn btn-primary">
                                            <input type="radio" class="sr-only" id="viewMode2" name="viewMode" value="2">
                                            <span class="docs-tooltip" data-toggle="tooltip" title="View Mode 2">
                                            VM2
                                            </span>
                                            </label>
                                            <label class="btn btn-primary">
                                            <input type="radio" class="sr-only" id="viewMode3" name="viewMode" value="3">
                                            <span class="docs-tooltip" data-toggle="tooltip" title="View Mode 3">
                                            VM3
                                            </span>
                                            </label>
                                        </div>*/ ?>
                                        <?php /*<div class="dropdown dropup docs-options">
                                            <button type="button" class="btn btn-primary btn-block dropdown-toggle" id="toggleOptions" data-toggle="dropdown"  data-placement="top" aria-expanded="true">
                                            Toggle Options
                                            <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu" aria-labelledby="toggleOptions">
                                                <li role="presentation">
                                                    <label class="checkbox-inline">
                                                    <input type="checkbox" name="responsive" checked>
                                                    responsive
                                                    </label>
                                                </li>
                                                <li role="presentation">
                                                    <label class="checkbox-inline">
                                                    <input type="checkbox" name="restore" checked>
                                                    restore
                                                    </label>
                                                </li>
                                                <li role="presentation">
                                                    <label class="checkbox-inline">
                                                    <input type="checkbox" name="checkCrossOrigin" checked>
                                                    checkCrossOrigin
                                                    </label>
                                                </li>
                                                <li role="presentation">
                                                    <label class="checkbox-inline">
                                                    <input type="checkbox" name="checkOrientation" checked>
                                                    checkOrientation
                                                    </label>
                                                </li>
                                                <li role="presentation">
                                                    <label class="checkbox-inline">
                                                    <input type="checkbox" name="modal" checked>
                                                    modal
                                                    </label>
                                                </li>
                                                <li role="presentation">
                                                    <label class="checkbox-inline">
                                                    <input type="checkbox" name="guides" checked>
                                                    guides
                                                    </label>
                                                </li>
                                                <li role="presentation">
                                                    <label class="checkbox-inline">
                                                    <input type="checkbox" name="center" checked>
                                                    center
                                                    </label>
                                                </li>
                                                <li role="presentation">
                                                    <label class="checkbox-inline">
                                                    <input type="checkbox" name="highlight" checked>
                                                    highlight
                                                    </label>
                                                </li>
                                                <li role="presentation">
                                                    <label class="checkbox-inline">
                                                    <input type="checkbox" name="background" checked>
                                                    background
                                                    </label>
                                                </li>
                                                <li role="presentation">
                                                    <label class="checkbox-inline">
                                                    <input type="checkbox" name="autoCrop" checked>
                                                    autoCrop
                                                    </label>
                                                </li>
                                                <li role="presentation">
                                                    <label class="checkbox-inline">
                                                    <input type="checkbox" name="movable" checked>
                                                    movable
                                                    </label>
                                                </li>
                                                <li role="presentation">
                                                    <label class="checkbox-inline">
                                                    <input type="checkbox" name="rotatable" checked>
                                                    rotatable
                                                    </label>
                                                </li>
                                                <li role="presentation">
                                                    <label class="checkbox-inline">
                                                    <input type="checkbox" name="scalable" checked>
                                                    scalable
                                                    </label>
                                                </li>
                                                <li role="presentation">
                                                    <label class="checkbox-inline">
                                                    <input type="checkbox" name="zoomable" checked>
                                                    zoomable
                                                    </label>
                                                </li>
                                                <li role="presentation">
                                                    <label class="checkbox-inline">
                                                    <input type="checkbox" name="zoomOnTouch" checked>
                                                    zoomOnTouch
                                                    </label>
                                                </li>
                                                <li role="presentation">
                                                    <label class="checkbox-inline">
                                                    <input type="checkbox" name="zoomOnWheel" checked>
                                                    zoomOnWheel
                                                    </label>
                                                </li>
                                                <li role="presentation">
                                                    <label class="checkbox-inline">
                                                    <input type="checkbox" name="cropBoxMovable" checked>
                                                    cropBoxMovable
                                                    </label>
                                                </li>
                                                <li role="presentation">
                                                    <label class="checkbox-inline">
                                                    <input type="checkbox" name="cropBoxResizable" checked>
                                                    cropBoxResizable
                                                    </label>
                                                </li>
                                                <li role="presentation">
                                                    <label class="checkbox-inline">
                                                    <input type="checkbox" name="toggleDragModeOnDblclick" checked>
                                                    toggleDragModeOnDblclick
                                                    </label>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- /.dropdown -->
                                    </div>*/ ?>
                                    <!-- /.docs-toggles -->
                                </div>
                                <!-- /Row -->
                            </div>
                            <div class="col-md-4">
                                <!-- <h3 class="page-header">Preview:</h3> -->
                                <div class="docs-preview clearfix">
                                    <div class="img-preview preview-lg"></div>
                                    <div class="img-preview preview-md"></div>
                                    <div class="img-preview preview-sm"></div>
                                    <div class="img-preview preview-xs"></div>
                                </div>
                                <!-- <h3 class="page-header">Data:</h3> -->
                                <div class="docs-data">
                                    <input type="hidden" name="image_src" class="image-src" id="cropper-image-src" />
                                    <input type="hidden" name="modulo" class="modal-modulo" id="cropper-modulo" />
                                    <input type="hidden" name="field" class="modal-field" id="cropper-field" />
                                    <input type="hidden" name="randcod" class="modal-randcod" id="cropper-randcod" />
                                    <input type="hidden" name="image_width" class="image-width" id="cropper-image-width" />
                                    <input type="hidden" name="image_height" class="image-height" id="cropper-image-height" />
                                    <input type="hidden" name="image_quality" class="image-quality" id="cropper-image-quality" />
                                    <div class="input-group ">
                                        <label class="input-group-addon" for="crop-dataX">X</label>
                                        <input type="text" class="form-control" id="crop-dataX" placeholder="x" readonly name="dataX">
                                        <span class="input-group-addon">px</span>
                                    </div>
                                    <div class="input-group ">
                                        <label class="input-group-addon" for="crop-dataY">Y</label>
                                        <input type="text" class="form-control" id="crop-dataY" placeholder="y" readonly name="dataY">
                                        <span class="input-group-addon">px</span>
                                    </div>
                                    <div class="input-group ">
                                        <label class="input-group-addon" for="crop-dataWidth">Width</label>
                                        <input type="text" class="form-control" id="crop-dataWidth" placeholder="width" readonly name="width">
                                        <span class="input-group-addon">px</span>
                                    </div>
                                    <div class="input-group ">
                                        <label class="input-group-addon" for="crop-dataHeight">Height</label>
                                        <input type="text" class="form-control" id="crop-dataHeight" placeholder="height" readonly name="height">
                                        <span class="input-group-addon">px</span>
                                    </div>
                                    <div class="input-group ">
                                        <label class="input-group-addon" for="crop-dataRotate">Rotate</label>
                                        <input type="text" class="form-control" id="crop-dataRotate" placeholder="rotate" readonly name="rotate">
                                        <span class="input-group-addon">deg</span>
                                    </div>
                                    <div class="input-group ">
                                        <label class="input-group-addon" for="crop-dataScaleX">ScaleX</label>
                                        <input type="text" class="form-control" id="crop-dataScaleX" placeholder="scaleX" readonly name="scaleX">
                                    </div>
                                    <div class="input-group ">
                                        <label class="input-group-addon" for="crop-dataScaleY">ScaleY</label>
                                        <input type="text" class="form-control" id="crop-dataScaleY" placeholder="scaleY" readonly name="scaleY">
                                    </div>
                                    <div class="input-group ">
                                        <label class="input-group-addon" for="crop-dataZoom">Zoom</label>
                                        <input type="text" class="form-control" id="crop-dataZoom" placeholder="zoom" readonly name="zoom">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Row -->
                  </div>
                  <div class="modal-footer">
                    <div class="row cropper-btns">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo get_lang("_FECHAR"); ?></button>
                            <button type="submit" class="btn btn-primary cropper-save btn-submit btn-success btn-anim" data-dismiss="modal"><i   class="icon-rocket"></i> <span class="btn-text"><?php echo get_lang("_CORTAR"); ?></span></button>
                        </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
    </div>

    <!-- JS -->
    <script type="text/javascript" language="javascript" src="https://maps.google.com/maps/api/js?sensor=true&key=AIzaSyDLgXx-Xv5YfCPYrCACeZQ8qD8kT-ozqGw"></script>
    <!--[if lt IE 9]>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <script type="text/javascript" language="javascript" src="<?php echo ROOT_SERVER . ROOT; ?>_js/dist/uz.min.js"></script>

    <!-- jQueryUI JavaScript -->
    <?php /*<script type="text/javascript" language="javascript" src="<?php echo ROOT_SERVER . ROOT; ?>_js/plugins/jquery-ui.min.js"></script>*/ ?>
    <!-- CKEditor and TinyMCE JavaScript -->
    <script type="text/javascript" language="javascript" src="<?php echo ROOT_SERVER . ROOT; ?>_js/plugins/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo ROOT_SERVER . ROOT; ?>_js/plugins/tinymce.min.js"></script>

    <script type="text/javascript" language="javascript" src="<?php echo ROOT_SERVER . ROOT; ?>_js/dist/cms.min.js"></script>

    <script type="text/javascript">
        loaded();
    </script>

</body>
</html>