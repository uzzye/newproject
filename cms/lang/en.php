<?php

$_DICAS_UTEIS = array(
	"Use the Crop Tool with wisdom. Respect the file size limits imposed by your server and try to maintain the recommended dimensions so as not to misconfigure the layout of your site.",
	
	"Carefully read the field labels on the forms. Labels with units in parentheses mean that it's not necessary to place the unit in the field.",
	
	"The access permissions set for a user overrides and the access permissions set for the group to which this user belongs.",
	
	"Remember that the contents registered in UzCMS is of yours responsibility and it gives shape to your site. Therefore, be careful to include only true information, clearly and with good spelling.",
	
	"Your site is a document. Never enter fully lower case or CAPS content. This type of feature must be managed on the front end by the webdesigner, so enter the informations correctly.",
	
	"When adding or modifying a record, remember to use the \"Save\" command at the end of the form, including after you finish editing images.",
	
	"Always use high quality images, respecting the file size limit imposed by your server.",
	
	"Whenever there is any doubt when using any UzCMS feature, please feel free to contact our team. Links in the page footer."
);

function get_lang($index = "") {
	global $lang_site;

	$lang_cms = array(
		"_ACAO_APRESENTACAO" => "Default View",
		"_ACAO_INCLUSAO" => "Add",
		"_ACAO_ALTERACAO" => "Edit",
		"_ACAO_EXCLUSAO" => "Delete",
		"_ACAO_ATIVACAO_DESATIVACAO" => "Activate/Deactivate",
		"_ACAO_LISTAR" => "List",
		"_ACAO_VISUALIZAR" => "View",
		"_ACAO_INCLUIR" => "Add",
		"_ACAO_EDITAR" => "Edit",
		"_ACAO_EXCLUIR" => "Delete",
		"_ACAO_CLONAR" => "Clone",
		"_ACAO_EXPORTACAO" => "Export",
		"_ACAO_TODAS" => "All",
		"_DICAS_UTEIS" => "Quick Tips",
		"_DICA_UTIL" => "Quick Tip",
		"_ULTIMOS_ACESSOS_CMS" => "CMS Last Access",

		"_HOME_VIEW_ULTIMOS_MESES" => "Views in Last Months",
		"_HOME_TAXA_CONVERSAO" => "Leads Convertion Ratio",
		"_HOME_DICA_UTIL" => "Quick Tip",
		"_HOME_ULTIMOS_ACESSOS" => "Last CMS Access",
		"_HOME_USUARIOS_DIARIOS_MES_ANTERIOR" => "Daily Users in Previous Month (And Current Until Today)",
		"_HOME_PRINCIPAIS_CONTEUDOS" => "Most Valuable Contents (Previous Month)",
		"_HOME_PAGINA" => "Page",
		"_HOME_VISITAS" => "Visits",
		"_HOME_CONVERTIONS_OF" => "convertions of",
		"_HOME_LEADS_MES_ATUAL" => "leads in current month",

		"_HELPPERMISSOES" => "Select below the modules and their actions you want to grant access. If you do not select any, the permissions set by the selected User Group will remain.",
		"_TODAS" => "All",
		"_GRUPO_USUARIOS" => "Users Group",
		"_GRUPO_DE_USUARIOS" => "Users Group",
		"_GRUPOS_DE_USUARIOS" => "Users Group",
		"_LISTA_REGISTROS" => "Records List",

		"_VOCENAOTEMPERMISSAOACESSOMODULO" => "You don't have permission to access this module!",
		"_VOCENAOTEMPERMISSAOACAO" => "You don't have permission to execute this action!",
		"_AVATAR" => "Photo",

		"_MSG_CONFIRMACAO_INCLUSAO" => "Record successfully add.",
		"_MSG_CONFIRMACAO_EXCLUSAO" => "Record successfully deleted.",
		"_MSG_CONFIRMACAO_ALTERACAO" => "Record successfully edited.",
		"_MSG_CONFIRMACAO_CLONAGEM" => "Record successfully cloned.",
		"_MSG_ERRO_INCLUSAO" => "Cannot add this record.",
		"_MSG_ERRO_EXCLUSAO" => "Cannot delete this record.",
		"_MSG_ERRO_ALTERACAO" => "Cannot edit this record.",
		"_MSG_ERRO_CLONAGEM" => "Cannot clone this record.",

		"_ALTERACAO_SENHA" => "Password Change",
		"_MEU_PERFIL" => "My Profile",
		"_CUSTOM_PERMALINK" => "Custom Permalink (leave blank to auto generate it)",

		"_HOME" => "Home",
		"_SEUPRIMEIROACESSO" => "First access?",
		"_LEIAABAIXOECOMECEAGERENCIAR" => "Read the infos below to start managing!",

		"_PAGINAINICIAL" => "CMS Home",
		"_ADICIONARNOVO" => "Add",
		"_ADICIONARNOVOEMAIS" => "Add +",
		"_CLONAR" => "Clone",
		"_ADDNOVO" => "Add",
		"_SIM" => "Yes",
		"_NAO" => "No",
		"_BANCO_IMAGENS" => "Images Database",

		"_LANGUAGE" => "Language",
		"_VARIAVEL" => "Variable",
		"_AUTORREGISTRO" => "Record Author",
		"_DATAREGISTRO" => "Record Date",
		"_ULTIMO_USUARIO" => "Last User",
		"_DATA_ULTIMA" => "Last Update",
		"_DATA_INICIO" => "Start Date",
		"_DATA_FIM" => "End Date",
		"_AUTOR" => "Author",
		"_DATA" => "Date",
		"_DATAFIM" => "Closure Date",
		"_HORA" => "Hour",
		"_HORAFIM" => "Closure Hour",
		"_FILTRAR" => "Filter",
		"_LIMPAR" => "Reset",
		"_ACOES" => "Actions",
		"_EDITAR" => "Edit",
		"_ATIVAR_DESATIVAR" => "Activate/Deactivate",
		"_REMOVER" => "Remove",
		"_EXCLUIR" => "Delete",
		"_PRIMEIRA" => "Fist",
		"_ANTERIOR" => "Previous",
		"_PRÓXIMA" => "Next",
		"_PROXIMA" => "Next",
		"_ÚLTIMA " => "Last",
		"_ULTIMA " => "Last",
		"_INCLUIR" => "Add",

		// USUARIOS
		"_NOME" => "Name",
		"_NOME_EN" => "Name EN",
		"_EMAIL" => "Email",
		"_SETOR" => "Department",
		"_SENHA" => "Password",
		"_SENHA_ATUAL" => "Current Password",
		"_NOVA_SENHA" => "New Password",
		"_NOVA_SENHA_REPETIDA" => "Repeat the new password",

		"_DISPONIVEL" => "Available",
		"_USUARIO" => "User",
		"_USUARIOS" => "Users",

		// MODULOS
		"_MODULO" => "Module",
		"_LINK" => "Link",
		"_COMBOSELECIONE" => "Select",
		"_COMBOTODOS" => "All",
		"_COMBOTODAS" => "All",
		"_COMBONENHUM" => "None",
		"_MODULOS" => "Modules",
		"_MODULO" => "Module",
		"_PERMISSOES" => "Permissions",

		"_URLVIDEO" => "Video URL",
		"_URLAOVIVO" => "Live Stream URL",
		"_DESTAQUE" => "Highlight",
		"_DESTAQUECAPA" => "Show as cover highlight",
		"_MOSTRARNACAPA" => "Show on cover",

		"_COMENTARIO" => "Comment",
		"_COMENTARIOS" => "Comments",
		"_APROVAR" => "Approve",
		"_LOGADOCOMO" => "Logged in as",
		"_VOCEESTAEM" => "You are in",
		"_IMAGEM" => "Image",
		"_NOVO" => "New",

		"_IMAGEMDESTAQUE" => "Highlight Image",
		"_URLFLICKR" => "Flickr URL",
		"_URLYOUTUBE" => "Youtube URL",
		"_LINKSSERVICOS" => "External services links",
		"_TIPO" => "Type",
		"_NOMEMUSICA" => "Song Name",
		"_NOMEARTISTA" => "Artist Name",
		"_ARQUIVO" => "File",
		"_CONTEUDO" => "Content",
		"_CONTEUDOS" => "Contents",
		"_DESCRICAO" => "Description",

		"_URL" => "URL",
		"_IMAGEMMINI" => "Mini Image",
		"_IMAGEMMINILISTA" => "Mini/list Image",
		"_DESCRICAOTAMANHO" => "Description/Size",
		"_GERAR" => "Generate",
		"_SUBTITULOCHAMADA" => "Subtitle/Call",
		"_CHAMADA" => "Call",
		"_IMAGEMREDUZIDA" => "Reduced Image",
		"_IMAGEMMINITHUMB" => "Thumb/mini Image",
		"_SELECIONEUMAIMAGEM" => "No image selected.",
		"_TITULOCROP" => "Image Crop",
		"_LABEL" => "Label",
		"_CORTAR" => "Cut",
		"_CROP" => "Crop",
		"_CORTE" => "Cut",
		"_NORMAL" => "Normal",
		"_THUMB" => "Thumb",
		"_UPLOAD" => "Upload",
		"_URLEMBED" => "URL Embed",
		"_OBSURLEMBEDVIDEO" => "(Attention! We do recommend videos of maximum size to maintain the layout aspect.)",
		"_FICHATECNICA" => "Technical Sheet",
		"_DESCRICAOFICHATECNICA" => "Description/Technical Sheet",
		"_SUBTITULOCHAMADA" => "Subtitle/Call",
		"_IMAGEMDESTAQUETOPO" => "Top Highlight Image",
		"_NOMEARQUIVOPHP" => "Identifier (normally the script name (*.php) or module (*))",
		"_NOMEARQUIVO" => "File name",
		"_IMAGEMLISTAGEM" => "List Image",
		"_URLALBUMFLICKR" => "URL to Flickr Album",
		"_NOMEALBUMFLICKR" => "Name of Flickr Album",
		"_IMAGENSNACAPA" => "Publish images on cover",
		"_IMAGENSFLICKRNACAPA" => "Publish Flicker images on cover",
		"_CABECALHOS" => "Headers",
		"_CABECALHO" => "Header",
		"_ATIVO" => "Active",
		"_ATIVOS" => "Actives",
		"_INATIVO" => "Inactive",
		"_INATIVOS" => "Inactives",
		"_RANKING" => "Ranking",
		"_PERMALINK" => "Permalink",
		"_VIEWS" => "Views",
		"_ATIVOINATIVO" => "Active/Inactive",
		"_CONTATO" => "Contact",
		"_CONTATOS" => "Contacts",
		"_IP" => "IP",
		"_MENSAGEM" => "Mesage",
		"_IMAGEMMINI" => "Mini Image",
		"_FINALIZAR" => "Finish",
		"_IMAGENS" => "Images",
		"_MOSTRARESCONDERIMAGENS" => "Show/Hide Images",
		"_MOSTRARESCONDER" => "Show/Hide",
		"_TAMANHORECOMENDAVEL" => "Maximum recommended size(s)",
		"_LAP_L" => "W",
		"_LAP_A" => "H",
		"_LAP_P" => "D",
		"_OBSIMAGECROP" => "(press CTRL+Q to select a image proportionally)",
		"_OBSSEPARADOSPONTOVIRGULA" => "separate by \";\"",
		"_EXEMPLOSALBUMFLICKR" => "",
		"_REDIMENSIONAR" => "Resize",
		"_IMAGEMCAPA" => "Cover Image",
		"_VISUALIZAR" => "View",
		"_TITULO" => "Title",
		"_TAG" => "Tag",
		"_IMAGEM_MINI" => "Mini Image",
		"_TEXTO" => "Text",
		"_TAGS" => "Tags",
		"_GRUPOS_DE_USUARIOS" => "Users Groups",
		"_GRUPO_DE_USUARIO" => "User Group",
		"_MODULOPAI" => "Parent Module",
		"_SUBTITULO" => "Subtitle",
		"_GRUPO" => "Group",
		"_CPF" => "SSN",
		"_ASSUNTO" => "Subject",
		"_RECEBE_NOVIDADES" => "Receive News",
		"_ASSOCIACOES" => "Associations",
		"_CONFIRMA_ATIVACAO" => "Confirm record activation?",
		"_POR_FAVOR_AGUARDE" => "Please wait...",
		"_AGUARDE" => "Wait...",
		"_RELATORIO" => "Report",
		"_RELATORIOS" => "Reports",
		"_CAMPOS_PADRAO" => "Default Fields",
		"_UF" => "Initials",

		"_JANEIRO" => "January",
		"_FEVEREIRO" => "February",
		"_MARCO" => "March",
		"_ABRIL" => "April",
		"_MAIO" => "May",
		"_JUNHO" => "June",
		"_JULHO" => "July",
		"_AGOSTO" => "August",
		"_SETEMBRO" => "September",
		"_OUTUBRO" => "October",
		"_NOVEMBRO" => "November",
		"_DEZEMBRO" => "December",
		"_ADICIONAR" => "Add",

		"_DE" => "From",
		"_ATE" => "To",
		"_DESEJASAIR" => "Do you really want to exit?",
		"_ULTIMA" => "Last",
		"_ÚLTIMA" => "Last",
		"_CLIQUE" => "Click",

		"_GERANDO" => "Generating",
		"_IMAGEMTEMP" => "Temp",
		"_IMAGENSGERADASFINALIZAR" => "Images generated! Click in finish to end.",
		"_OBS" => "Note:",
		"_EHNECESSARIOREFRESHMOSTRARIMGS" => "It's necessary to finish this operation to update images, just \"REMOVE\" is real time.",
		"_HELPCROP" => "Hi!<br><br>To use Uzzye Image Generator you have to upload a file. If the file is large, verify if the server is configured to deny it. In this case, contact the server admin.
		<br><br>- After select the image, you can crop or resize it;
		<br>- Please note this screen title for the recommended dimensions to preserve the layout;
		<br>- You can forward all steps as you need for the images count;
		<br>- You can interrupt a image generation anytime, just click the button \"FINISH\";
		<br>- If the image is a \"PNG\", the crop box could show a background, but after finish it will be generated with original/transparent background;
		<br>- After finishing the image generation, please remember to finish the form edition in previous screen;
		<br>- Please remember that image quality is very important for your website presentation;",

		"_RODAPE_TEXTO1" => "Caxias do Sul - RS - Brasil | <a href=\"mailto:falecom@uzzye.com\">falecom@uzzye.com</a> | +55 54 3538 8333 | +55 54 98116 8709",
		"_RODAPE_TEXTO2" => "&copy; <b>Uzzye</b> 2019 - All rights reserved.",
		"_RODAPE_VISITE" => "Visit us",
		"_RODAPE_TWITTER" => "Twitter",
		"_RODAPE_FACEBOOK" => "Facebook",
		"_RODAPE_ORKUT" => "Orkut",
		"_CMSHOME" => "CMS Home",
		"_FILTROS" => "Filter",
		"_VOLTAR" => "Back",
		"_BEMVINDO" => "Welcome",
		"_OLA" => "Hi",
		"_SAIR" => "Exit",
		"_HOMETITULO" => "Welcome!",
		"_ULTMOS_ACESSOS" => "Recently online",
		"_ULTMOS_REGISTROS" => "Last records",
		"_REGISTRO_SERIALIZADO" => "Serialized Record",
		"_NOME_ENTIDADE" => "Entity Name",
		"_ID_REGISTRO" => "Record ID",
		"_MODULO_REGISTRO" => "Record Module",
		"_CAMPO_REGISTRO" => "Record Field",
		"_ACAO" => "Action",
		"_LOG_DE_ENTIDADES" => "Entity Log",
		"_ID" => "ID",
		"_POR" => "By",
		"_EM" => "@",
		"_ALTERACAO" => "Edition",
		"_INCLUSAO" => "Inclusion",
		"_EXCLUSAO" => "Deletion",
		"_RECOMENDADO" => "Recommended",
		"_CKEDITORHELP" => "Tip: for a new paragraph line use SHIFT + ENTER. For a new simple line use ENTER.",
		"_LISTBOXHELP" => "Tip: to select more than one item use CTRL + Click. If no item is selected, the first one is used.",
		"_LISTBOXHELPSINGLE" => "Tip: If no item is selected, the first one is used.",
		"_ENDERECO" => "Address",
		"_LATITUDE" => "Latitude",
		"_LONGITUDE" => "Longitude",
		"_NENHUM" => "None",
		"_NENHUMA" => "None",
		"_TODOS" => "All",
		"_SELECIONE" => "Select",
		"_TEXTO_CAPA" => "Resume",
		"_RESUMO" => "Resume",
		"_META_DESCRICAO" => "META Description (Only for level 1 pages)",
		"_META_PALAVRAS" => "META Keywords (Only for level 1 pages)",
		"_REFERENCIA" => "Reference",
		"_REFERENCIAS" => "References",
		"_ID_REFERENCIA" => "ID Reference",
		"_TELEFONE" => "Phone",
		"_DICASUTEIS" => "Quick Tips",
		"_NENHUMARQUIVOENVIADO" => "No file sent.",

		"_IDENTIFICADOR" => "Identifier",
		"_IDIOMAS" => "Languages",
		"_IDIOMA" => "Language",
		"_NAMESPACE" => "Namespace",
		"_IDIOMA_PADRAO" => "Default language",
		"_EMAIL_PADRAO_P_CONTATO" => "Default receiver email",
		"_EMAIL_REMETENTE_PADRAO" => "Default sender email",
		"_TITULO_PADRAO" => "Default title",
		"_DESCRICAO_PADRAO" => "Default description",
		"_PALAVRASCHAVE_PADRAO" => "Default keywords",
		"_URL_AMIGAVEL" => "Friendly URL",
		"_CHARSET_PADRAO" => "Default charset",
		"_QUANTIDADE_DE_ITENS_POR_PAGINA" => "Items per page",
		"_QUANTIDADE_AGRUPADA_NA_PAGINACAO" => "Grouped Items in pagination",
		"_CONFIGURACOES" => "Configs",
		"_CONFIGURACAO" => "Config",
		"_CODIGO_ANALYTICS" => "Analytics Code",
		"_CODIGO_CSS" => "Raw CSS",
		"_CODIGO_JAVASCRIPT" => "Raw Javascript",
		"_GEOLOCALIZACAO" => "Geolocation",
		"_GEOLOCALIZACAO_METAS" => "Geolocation / Meta Tags",
		"_MAPA_EMBED" => "Map (Embed)",
		"_TARGET" => "Target",
		"_DESTINO" => "Target",
		"_LINKS_UTEIS" => "Quick Links",

		"_EXPORTAR" => "Export",
		"_CSV" => "CSV",
		"_EXPORTAR_CSV" => "Export in CSV",

		"_DEFAULTMAPSEARCH" => "Use this field to search for a location and press TAB...",

		"_SITE" => "Site",
		"_NOME_REMETENTE_PADRAO" => "Default sender name",
		"_SERVIDOR_SMTP" => "SMTP Server",
		"_PORTA_SMTP" => "SMTP Port",
		"_SECURE_SMTP" => "SMTP Security",
		"_SERVIDOR_POP" => "POP Server",
		"_PORTA_POP" => "POP Port",
		"_SECURE_POP" => "POP Security",
		"_USUARIO_EMAIL" => "Email user",
		"_SENHA_EMAIL" => "Email password",
		"_OUTRAS_CONFIGURACOES" => "Other configs",
		"_CIDADE" => "City",
		"_ESTADO" => "State",
		
		"_FECHAR" => "Close",
		"_SALVAR" => "Save",
		"_ID" => "ID",
		"_COR" => "Color",
		"_VALOR" => "Value",
		"_ICON_CLASS" => "Icon Class",
		"_EXCLUIR_REG_PAGINA" => "Delete selected records in this page",

		"_UID_GA" => "Google Analytics UID",
		"_GA_USER" => "Google Analytics User",
		"_GA_PROPS" => "Google Analytics Properties (separate by \",\")",
		"_CONVERSAO" => "Conversion",
		"_VALOR_CONVERSAO" => "Conversion value (blank for default)",
		"_VALOR_CONVERSAO_CONTATO" => "Default conversion value for contacts",
        "_MODULO_DO_REGISTRO" => "Record Module",
        "_ID_DO_REGISTRO" => "Record ID",
        "_IDADE" => "Age",
        "_SEXO" => "Gender",
        "_RELACIONAMENTO" => "Relationship",
        "_PAIS" => "Parents",
        "_ID_DO_FACEBOOK" => "Facebook ID",
        "_LOG_DE_VIEWS" => "Views Log",
        "_ITEM_DE_LOG" => "Log Item",
        "_CONVERTER" => "Convert",
        "_CONVERTIDO" => "Converted",

        "_BANNERS" => "Banners",
		"_BANNER" => "Banner",
		"_TEMPO_DURACAO" => "Duration Time",
		"_TEMPO_DURACAO_MS" => "Duration Time (miliseconds)",
		"_TEMPO_TRANSICAO" => "Transition Time",
		"_TEMPO_TRANSICAO_MS" => "Transition Time (miliseconds)",
		"_CINEMAGRAPH" => "Cinemagraph",
		"_TEXTO_P_BOTAO" => "Button Text",
		"_COR_OVERLAY" => "Overlay Color",
		"_CINEMAGRAPH_P_MOBILE" => "Cinemagraph (Mobile)",
		"_IMAGEM_CONTEUDO" => "Content Image",
		"_IMAGEM_CONTEUDO_PT" => "Content Image PT",
		"_IMAGEM_CONTEUDO_EN" => "Content Image EN",
		"_IMAGEM_CONTEUDO_MOBILE" => "Content Image (Mobile)",
		"_IMAGEM_P_BG" => "BG Image",
		"_IMAGEM_P_BG_MOBILE" => "BG Imagem (Mobile)",
		"_VIDEO_URL" => "Video URL",
		"_VIDEO_INCORPORADO" => "Embed Video",
		"_TEXTO_P_BOTAO_PT" => "Button Text PT",
		"_TEXTO_P_BOTAO_EN" => "Button Text EN",
		"_VIDEO_AUTOPLAY" => "Autoplay video",
		"_TITULO_RESUMIDO_PT" => "Resume Title PT",
		"_TITULO_RESUMIDO_EN" => "Resume Title EN",
		"_SUBTITULO_PT" => "Subtitle PT",
		"_SUBTITULO_EN" => "Subtitle EN",
		"_IMAGEM_DE_THUMB" => "Thumb Image",
		"_IMAGEM_BG" => "BG Image",
		"_DATA_ULTIMO_ACESSO" => "Last Access Date",
		"_UPLOADPECASTITULO" => "Pieces Upload",
		"_UPLOADHELP" => "Select a collection and files to send a batch of pieces.",
		"_REMOVERANTIGASCOLECAO" => "Delete existing pieces in this collection for selected genre and type?",

        "_ID_DO_USUARIO" => "User ID",
        "_ID_DA_REFERENCIA" => "Reference ID",
        "_DOWNLOADS" => "Downloads",
        "_DOWNLOAD" => "Download",
        "_NOME_ARQUIVO" => "File Name",
		"_TAMANHO" => "Size",
        
        "_ESTADOS" => "States",
        "_DISTRIBUIDORES" => "Distributors",
        "_DISTRIBUIDOR" => "Distributor",
        "_ICONE" => "Icon",
        "_HARMONIZACOES" => "Harmonizations",
        "_HARMONIZACAO" => "Harmonization",
        "_PREMIOS" => "Prizes",
        "_PREMIO" => "Prize",
        "_CATEGORIAS" => "Categories",
        "_CATEGORIA" => "Category",
        "_PRODUTOS" => "Products",
        "_PRODUTO" => "Product",
        "_INFORMACOES" => "Informations",
        "_INFORMACAO" => "Information",
        "_CADASTRO" => "Registration",
        "_CADASTROS" => "Registrations",
        "_PAGINA" => "Page",
        "_MOSTRAR_MENU" => "Show in Menu",
        "_LOGO" => "Logo",
        "_LOJAS_VIRTUAIS" => "Online Stores",
        "_LOJA_VIRTUAL" => "Online Store",
        "_URL_LOJA" => "Store URL",
        "_TARGET_LOJA" => "Store Target",
		"_TITULO_PT" => "Title PT",
		"_TITULO_EN" => "Title EN",
		"_TITULO_ES" => "Title ES",
		"_DESCRICAO_PT" => "Description PT",
		"_DESCRICAO_EN" => "Description EN",
		"_DESCRICAO_ES" => "Description ES",
		"_TEXTO_P_BOTAO_ES" => "Button Text ES",
		"_DIFERENCIAL" => "Differential",
		"_DIFERENCIAIS" => "Differentials",
		"_IMAGENS_DE_PRODUTOS" => "Products Images",
		"_IMAGEM_DE_PRODUTO" => "Product Image",
		"_CHAMADA_PT" => "Call PT",
		"_CHAMADA_EN" => "Call EN",
		"_CHAMADA_ES" => "Call ES",
		"_CARACTERISTICAS_PT" => "Characteristics PT",
		"_CARACTERISTICAS_EN" => "Characteristics EN",
		"_CARACTERISTICAS_ES" => "Characteristics ES",
		"_PRODUTOS_RELACIONADOS" => "Related Products",
		"_GALERIA" => "Gallery",
		"_INTERNACIONAL" => "None/International",
		"_MANUAL" => "Guide",
		"_REPRESENTANTE" => "Representative",
		"_REPRESENTANTES" => "Representatives",

        "_NOTICIAS" => "News",
        "_NOTICIA" => "News",
		"_DESTAQUES" => "Highlights",
		"_CAMPOS_DE_IDIOMAS" => "Language Fields",
		"_OUTROS_CAMPOS" => "General Fields",
		"_TAMANHO_ARQUIVO_RECOMENDADO" => "Maximum of 1MB file recommended.",
		"_AUTENTICACAO" => "Authentication",
		"_LEMBRAR_ACESSO" => "Remember Access",
		"_ENTRAR" => "Sign In",
		"_INICIO" => "Home",
		"_RESULTADO" => "Result",
        "_PROFILE_EDIT" => "Edit profile",
        "_PASSWORD_CHANGE" => "Change password",
        "_LOGOUT" => "Logout",
        "_MVC_MANAGER" => "MVC Manager",
        "_TOOLS" => "Tools",
        
        "_COR_CLARA" => "Bright Color",
        "_COR_ESCURA" => "Dark Color",
        "_UNIDADES_DE_NEGOCIO" => "Business Units",
        "_UNIDADES_NEGOCIO" => "Business Units",
        "_UNIDADE_DE_NEGOCIO" => "Business Unit",
        "_UNIDADE_NEGOCIO" => "Business Unit",
        "_USUARIO_DO_SITE" => "App User",
        "_UNIDADES_DE_NEGOCIO_POR_USUARIOS" => "Users Business Units",
        "_UNIDADE_DE_NEGOCIO_POR_USUARIO" => "User Businss Unit",
        "_SELECAO_UNIDADE_NEGOCIO" => "Business Unit Selection",
        "_INICIO" => "Home",
        "_NENHUMA_UNIDADE_NEGOCIO_SEL" => "No Business Unit selected.",
        "_TROCAR" => "Change",

        "_BUSCAR" => "Search",
        "_PROCESSANDO" => "Processing...",
        "_CARREGANDO" => "Loading...",
        "_MONSTRANDO" => "Showing",
        "_TO" => "to",
        "_OF" => "of",
        "_FILTRADODE" => "filtered of",
        "_REGISTROS_TOTAIS" => "total records",
        "_REGISTROS" => "Records",
        "_MOSTRAR" => "Show",
        "_NENHUMREGISTRO" => "No records found",
        "_NENHUMREGISTROCORRESPONDE" => "No corresponding records found",
        "_ATIVARPARAORDENARASC" => "activate to order column ascending",
        "_ATIVARPARAORDENARDESC" => "activate to order column descending",
        "_CONFIGURACOES_SITE" => "Website Configs",
        "_CONFIGURACOES_EMAIL" => "Email Configs",
        "_OUTRAS_CONFIGS" => "Other Configs",
        "_DOWNLOAD_ORIGINAL_IMAGE" => "Download Original Image",
        "_SELECT_IMAGE" => "Select from Image Database",
        "_TIMES_TABLE" => "Times Table",
        "_SERVICES_TABLE" => "Services Table",
        "_CIFRA" => "USD",
        "_VALUE_COMMA_1" => ",",
        "_VALUE_COMMA_2" => ".",
        "_SEG" => "MON",
        "_TER" => "TUE",
        "_QUA" => "WED",
        "_QUI" => "THU",
        "_SEX" => "FRI",
        "_SAB" => "SAT",
        "_DOM" => "SUN",
        "_SEMANAL" => "Weekly",
        "_QUINZENAL" => "Biweekly",
        "_MENSAL" => "Monthly",
        "_BIMESTRAL" => "Bimonthly",
        "_TRIMESTRAL" => "Quarterly",
        "_SEMESTRAL" => "Semestral",
		"_ANUAL" => "Yearly",
		"_DROPZONE_DEFAULTMESSAGE" => "<i class='icon-cloud-upload'></i><br/>Drag a new file or click to add",
        "_DROPZONE_FALLBACKMESSAGE" => "Your browser doesn't support file selection from another window.",
        "_DROPZONE_FALLBACKTEXT" => "Please,use the form below to send files in a old school way.",
        "_DROPZONE_FILETOOBIG" => "The file is too large ({{filesize}}MiB). Maximum: {{maxFilesize}}MiB.",
        "_DROPZONE_INVALIDFILETYPE" => "You cannot send this kind of file.",
        "_DROPZONE_RESPONSEERROR" => "The server answered with code {{statusCode}}.",
        "_DROPZONE_CANCELUPLOAD" => "Cancel",
        "_DROPZONE_CANCELUPLOADCONFIRMATION" => "Are you sure you want to cancel the upload?",
        "_DROPZONE_REMOVEFILE" => "Remove",
		"_DROPZONE_MAXFILESEXCEEDED" => "You cannot send more files.",
		"_VOCETEMCERTEZA" => "Are you sure?",
		"_CONFIRMAPROCESSAMENTO" => "Do you confirm these file proccess?",
		"_NAOSERAPOSSIVELRECUPERAR" => "It won't be possible to recover these data after removed!",
		"_SIMREMOVER" => "Yes, remove!",
		"_SIMPROCESSAR" => "Yes, proccess!",
		"_NAOCANCELAR" => "No, cancel!",
		"_CATEGORIAS_DE_ARQUIVOS" => "Files Categories",
		"_CATEGORIA_DE_ARQUIVO" => "File Category",
		"_ARQUIVOS" => "Files",
		"_CLASSE_ICONE" => "Icon Class",
		"_IMAGEM_BG_PT" => "BG Image PT",
		"_IMAGEM_BG_EN" => "BG Image EN",
		"_IMAGEM_BG_ES" => "BG Image ES",
		"_IMAGEM_P_BG_PT" => "BG Image PT",
		"_IMAGEM_P_BG_EN" => "BG Image EN",
		"_IMAGEM_P_BG_ES" => "BG Image ES",
		"_IMAGEM_P_BG_MOBILE_PT" => "BG Image (Mobile) PT",
		"_IMAGEM_P_BG_MOBILE_EN" => "BG Image (Mobile) EN",
		"_IMAGEM_P_BG_MOBILE_ES" => "BG Image (Mobile) ES",
	
		"_SWAL_CONFIRMA_ALTERACAO" => "Confirm below the register update.",
		"_SWAL_VOCE_TEM_CERTEZA" => "Are you sure?",
		"_SWAL_SIM_CONFIRMAR" => "Yes, confirm!",
		"_SWAL_NAO_CANCELAR" => "No, cancel!",
		"_SWAL_SIM_REMOVER" => "Yes, remove!",
		"_SWAL_NAO_SERA_POSSIVEL_RECUPERAR" => "You cannot recover this register and its files after removed!",
		"_SWAL_NAO_SERA_POSSIVEL_RECUPERAR_PLURAL" => "You cannot recover these registers and their files after removed!",
		"_SWAL_REMOVIDOS" => "Removed!",
		"_SWAL_REMOVIDO" => "Removed!",
		"_SWAL_OPS" => "Ops!",
		"_SWAL_AVISO" => "Alert!",
		"_SWAL_SUCESSO" => "Success!",
		"_SWAL_CROP_IMAGEM" => "The image crop was done.",
		"_SWAL_SELECIONAR_IMAGEM" => "You have to select an image.",
		"_SWAL_REGISTRO_REMOVIDO" => "This register and its files was removed.",
		"_SWAL_REGISTROS_REMOVIDOS" => "The selected registers and their files were removed.",
		"_SWAL_ALGO_ACONTECEU" => "Something happened in processing the request.",

		"_MSG_NEW_PASS_DOESNT_MATCH" => "Password doesn't match.",
		"_MSG_PASS_CHANGED" => "Password successfully changed.",
		"_MSG_CURRENT_PASS_INVALID" => "Current password is invalid.",
		"_MSG_USER_PASS_INVALID" => "Invalid user or password!",
		"_MSG_AUTHENTICATING" => "Authenticating...",

		// TESTE
		"_TESTE" => "Teste",
		"_ITEM_TESTE" => "Item de Teste",
		"_ITENS_TESTE" => "Itens de Teste",
		// FIM TESTE
        
        "" => "" // REQUIRED FOR MVC MANAGER - DO NOT REMOVE THIS LINE
	);

	$lang_cms = array_merge($lang_cms, $lang_site);

	if(!array_key_exists($index, $lang_cms)){
		return $index;
	} else {
		if(trim($index) == "") {
			return $lang_cms;
		} else {
			return $lang_cms[$index];
		}
	}
}

?>