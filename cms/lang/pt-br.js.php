<script language="javascript">
	var dropify_messages = {
        'default': 'Arraste um arquivo ou clique para enviar',
        'replace': 'Arraste um novo arquivo ou clique para substituir',
        'remove':  'Remover',
        'error':   'O arquivo é inválido ou o tamanho é superior ao limite do servidor.'
    };
	var dropify_error = {
        'fileSize': 'O tamanho do arquivo é muito grande ({{ value }} máx).',
        'minWidth': 'A largura da imagem é muito pequena ({{ value }}}px mín).',
        'maxWidth': 'A largura da imagem é muito grande ({{ value }}}px máx).',
        'minHeight': 'A altura da imagem é muito pequena ({{ value }}}px mín).',
        'maxHeight': 'A altura da imagem é muito grande ({{ value }}px máx).',
        'imageFormat': 'O formato da imagem não é suportado ({{ value }} apenas).'
    };
    var date_tooltips = {
        today: 'Ir para hoje',
        clear: 'Limpar seleção',
        close: 'Fechar',
        selectMonth: 'Selecionar Mês',
        prevMonth: 'Mês Anterior',
        nextMonth: 'Próximo Mês',
        selectYear: 'Selecionar Ano',
        prevYear: 'Ano Anterior',
        nextYear: 'Próximo Ano',
        selectDecade: 'Selecionar Década',
        prevDecade: 'Década Anterior',
        nextDecade: 'Próxima Década',
        prevCentury: 'Século Anterior',
        nextCentury: 'Próximo Século',
        incrementHour: 'Aumentar Hora',
        pickHour: 'Selecionar Hora',
        decrementHour:'Diminuir Hora',
        incrementMinute: 'Aumentar Minuto',
        pickMinute: 'Selecionar Minuto',
        decrementMinute:'Diminuir Minuto',
        incrementSecond: 'Aumentar Segundo',
        pickSecond: 'Selecionar Segundo',
        decrementSecond:'Diminuir Segundo'
    };
    var _lang = [];
    _lang["_AGUARDE"] = "<?php echo get_lang("_AGUARDE"); ?>";
    _lang["_SALVAR"] = "<?php echo get_lang("_SALVAR"); ?>";
    _lang["_SWAL_CONFIRMA_ALTERACAO"] = "<?php echo get_lang("_SWAL_CONFIRMA_ALTERACAO"); ?>";
    _lang["_SWAL_CONFIRMA_ALTERACAO"] = "<?php echo get_lang("_SWAL_CONFIRMA_ALTERACAO"); ?>";
    _lang["_SWAL_VOCE_TEM_CERTEZA"] = "<?php echo get_lang("_SWAL_VOCE_TEM_CERTEZA"); ?>";
    _lang["_SWAL_SIM_CONFIRMAR"] = "<?php echo get_lang("_SWAL_SIM_CONFIRMAR"); ?>";
    _lang["_SWAL_NAO_CANCELAR"] = "<?php echo get_lang("_SWAL_NAO_CANCELAR"); ?>";
    _lang["_SWAL_SIM_REMOVER"] = "<?php echo get_lang("_SWAL_SIM_REMOVER"); ?>";
    _lang["_SWAL_NAO_SERA_POSSIVEL_RECUPERAR"] = "<?php echo get_lang("_SWAL_NAO_SERA_POSSIVEL_RECUPERAR"); ?>";
    _lang["_SWAL_NAO_SERA_POSSIVEL_RECUPERAR_PLURAL"] = "<?php echo get_lang("_SWAL_NAO_SERA_POSSIVEL_RECUPERAR_PLURAL"); ?>";
    _lang["_SWAL_REMOVIDOS"] = "<?php echo get_lang("_SWAL_REMOVIDOS"); ?>";
    _lang["_SWAL_REMOVIDO"] = "<?php echo get_lang("_SWAL_REMOVIDO"); ?>";
    _lang["_SWAL_OPS"] = "<?php echo get_lang("_SWAL_OPS"); ?>";
    _lang["_SWAL_AVISO"] = "<?php echo get_lang("_SWAL_AVISO"); ?>";
    _lang["_SWAL_SUCESSO"] = "<?php echo get_lang("_SWAL_SUCESSO"); ?>";
    _lang["_SWAL_CROP_IMAGEM"] = "<?php echo get_lang("_SWAL_CROP_IMAGEM"); ?>";
    _lang["_SWAL_SELECIONAR_IMAGEM"] = "<?php echo get_lang("_SWAL_SELECIONAR_IMAGEM"); ?>";
    _lang["_SWAL_REGISTRO_REMOVIDO"] = "<?php echo get_lang("_SWAL_REGISTRO_REMOVIDO"); ?>";
    _lang["_SWAL_REGISTROS_REMOVIDOS"] = "<?php echo get_lang("_SWAL_REGISTROS_REMOVIDOS"); ?>";
    _lang["_SWAL_ALGO_ACONTECEU"] = "<?php echo get_lang("_SWAL_ALGO_ACONTECEU"); ?>";
</script>