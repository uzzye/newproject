<script language="javascript">
	var dropify_messages = {
        'default': 'Drag a file or click to upload',
        'replace': 'Drag a new file or click to replace',
        'remove':  'Remove',
        'error':   'This file is invalid or its size is over the server limit.'
    };
	var dropify_error = {
        'fileSize': 'The file size is too big ({{ value }} max).',
        'minWidth': 'The image width is too small ({{ value }}}px min).',
        'maxWidth': 'The image width is too big ({{ value }}}px max).',
        'minHeight': 'The image height is too small ({{ value }}}px min).',
        'maxHeight': 'The image height is too big ({{ value }}px max).',
        'imageFormat': 'The image format is not allowed ({{ value }} only).'
    };
    var date_tooltips = {
        today: 'Go to today',
        clear: 'Clear selection',
        close: 'Close',
        selectMonth: 'Select Month',
        prevMonth: 'Previous Month',
        nextMonth: 'Next Month',
        selectYear: 'Select Year',
        prevYear: 'Previous Year',
        nextYear: 'Next Year',
        selectDecade: 'Select Decade',
        prevDecade: 'Previous Decade',
        nextDecade: 'Next Decade',
        prevCentury: 'Previous Century',
        nextCentury: 'Next Century',
        incrementHour: 'Raise Hour',
        pickHour: 'Select Hour',
        decrementHour:'Lower Hour',
        incrementMinute: 'Raise Minute',
        pickMinute: 'Select Minute',
        decrementMinute:'Lower Minute',
        incrementSecond: 'Raise Second',
        pickSecond: 'Select Second',
        decrementSecond:'Lower Second'
    };
    var _lang = [];
    _lang["_AGUARDE"] = "<?php echo get_lang("_AGUARDE"); ?>";
    _lang["_SALVAR"] = "<?php echo get_lang("_SALVAR"); ?>";
    _lang["_SWAL_CONFIRMA_ALTERACAO"] = "<?php echo get_lang("_SWAL_CONFIRMA_ALTERACAO"); ?>";
    _lang["_SWAL_CONFIRMA_ALTERACAO"] = "<?php echo get_lang("_SWAL_CONFIRMA_ALTERACAO"); ?>";
    _lang["_SWAL_VOCE_TEM_CERTEZA"] = "<?php echo get_lang("_SWAL_VOCE_TEM_CERTEZA"); ?>";
    _lang["_SWAL_SIM_CONFIRMAR"] = "<?php echo get_lang("_SWAL_SIM_CONFIRMAR"); ?>";
    _lang["_SWAL_NAO_CANCELAR"] = "<?php echo get_lang("_SWAL_NAO_CANCELAR"); ?>";
    _lang["_SWAL_SIM_REMOVER"] = "<?php echo get_lang("_SWAL_SIM_REMOVER"); ?>";
    _lang["_SWAL_NAO_SERA_POSSIVEL_RECUPERAR"] = "<?php echo get_lang("_SWAL_NAO_SERA_POSSIVEL_RECUPERAR"); ?>";
    _lang["_SWAL_NAO_SERA_POSSIVEL_RECUPERAR_PLURAL"] = "<?php echo get_lang("_SWAL_NAO_SERA_POSSIVEL_RECUPERAR_PLURAL"); ?>";
    _lang["_SWAL_REMOVIDOS"] = "<?php echo get_lang("_SWAL_REMOVIDOS"); ?>";
    _lang["_SWAL_REMOVIDO"] = "<?php echo get_lang("_SWAL_REMOVIDO"); ?>";
    _lang["_SWAL_OPS"] = "<?php echo get_lang("_SWAL_OPS"); ?>";
    _lang["_SWAL_AVISO"] = "<?php echo get_lang("_SWAL_AVISO"); ?>";
    _lang["_SWAL_SUCESSO"] = "<?php echo get_lang("_SWAL_SUCESSO"); ?>";
    _lang["_SWAL_CROP_IMAGEM"] = "<?php echo get_lang("_SWAL_CROP_IMAGEM"); ?>";
    _lang["_SWAL_SELECIONAR_IMAGEM"] = "<?php echo get_lang("_SWAL_SELECIONAR_IMAGEM"); ?>";
    _lang["_SWAL_REGISTRO_REMOVIDO"] = "<?php echo get_lang("_SWAL_REGISTRO_REMOVIDO"); ?>";
    _lang["_SWAL_REGISTROS_REMOVIDOS"] = "<?php echo get_lang("_SWAL_REGISTROS_REMOVIDOS"); ?>";
    _lang["_SWAL_ALGO_ACONTECEU"] = "<?php echo get_lang("_SWAL_ALGO_ACONTECEU"); ?>";
</script>