<?php

include_once("config.php");

include_once("verificaLogado.php");

define("_UZCMS_URL_FACEBOOK","http://www.facebook.com/agenciauzzye");
define("_UZCMS_URL_TWITTER","http://www.twitter.com/agenciauzzye");
define("_UZCMS_URL_SITE","http://uzzye.com");
define("_UZCMS_EMAIL","suporte@uzzye.com");

$modulo = $_CMS_CONFIG["modulo_padrao"];
if(trim($_REQUEST["modulo"]) <> "")
{	
	$modulo = trim($_REQUEST["modulo"]);	
}

$acao = $_CMS_CONFIG["acao_padrao"];
if(trim($_GET["acao"]) <> "")
{	
	$acao = trim($_GET["acao"]);	
} else if(trim($_POST["acao"]) <> "")
{	
	$acao = trim($_POST["acao"]);	
}

$status_operacao = "";
if(trim($_REQUEST["status_operacao"]) <> "")
{	
	$status_operacao = trim($_REQUEST["status_operacao"]);	
}

$id = null;
if(trim($_REQUEST["id"]) <> "")
{   
    $id = trim($_REQUEST["id"]);  
}

if(file_exists(ROOT_CMS . "classes/models/" . $modulo . "_model.php")){
include_once(ROOT_CMS . "classes/models/" . $modulo . "_model.php");}
if(file_exists(ROOT_CMS . "classes/views/" . $modulo . "_view.php")){
include_once(ROOT_CMS . "classes/views/" . $modulo . "_view.php");}
if(file_exists(ROOT_CMS . "classes/controllers/" . $modulo . "_controller.php")){
include_once(ROOT_CMS . "classes/controllers/" . $modulo . "_controller.php");}

$error = "";

?>
<script language="javascript">
	CMS_MODULO = "<?php echo $modulo; ?>";
	CMS_ACAO = "<?php echo $acao; ?>";
</script>
<?php

if(file_exists(ROOT_CMS . "classes/" . $modulo . ".php"))
{
	$classe = $modulo."_controller";				
	$controller = new $classe();
	if(trim($acao) == "")
	{$acao = "acaoPadrao";}

	if($acao == "login" || !method_exists($classe,"valida_permissao") || $controller->valida_permissao($modulo))
	{	
		if(method_exists($classe,$acao))
		{			
			if($acao == "get_registros") {
				$controller->$acao(null, null);
			} else {
				$controller->$acao();
			}
		}		
		else if(method_exists($classe,"acaoPadrao"))
		{			
			$controller->acaoPadrao();
		}
	} else {
		$error = "401";
	}
} else {
	$error = "404";
} 

if(trim($error) != "") {
	ob_clean();
	?>
	<iframe class="error-frame fullScreen" src="<?php echo ROOT_SERVER . ROOT . $error; ?>.html"></iframe>
	<?php
    die();
}

?>