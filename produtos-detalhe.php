<?php

include_once("config.php"); 

$_ref_page = "produtos";

$pages = new conteudos();
$pages = $pages->localiza(array("nome_arquivo","id_idioma"),array($_ref_page,$_CONFIG["id_lang"]));
if(!is_array($pages) || sizeof($pages) == 0) {
    $pages = new conteudos();
    $pages = $pages->localiza(array("nome_arquivo","id_idioma"),array($_ref_page,1));
}
if(!is_array($pages) || sizeof($pages) == 0) {
    $pages = new conteudos();
    $pages = $pages->localiza(array("nome_arquivo","id_idioma"),array($_ref_page,99));
}

if(is_array($pages))
{
	$page = $pages[0];
}

if($page <> null)
{
	$descr = $page->get_var("texto");
	$titulo = stripslashes(get_output($page->get_var("titulo")));
	$descrRes = stripslashes(get_output($page->get_var("meta_descricao")));
	$keywords = stripslashes(get_output($page->get_var("meta_palavras_chave")));

	$_TITULO_PAGINA = strip_tags($titulo);
	$_DESCRICAO_PAGINA = strip_tags($descrRes);
	$_PALAVRAS_CHAVE_PAGINA = strip_tags($keywords);
}

if(intval($_REQUEST["id"]) > 0) {
  carrega_classe("produtos");
  carrega_classe("produtos_categorias");

  $item = new produtos();
  $item->inicia_dados();
  $item->set_var("id",intval($_REQUEST["id"]));
  $item->carrega_dados();

  if($item != null) {
      //$item_tit = stripslashes(get_output($item->get_var("titulo_" . $_CONFIG["ref_lang"])));
      //if(trim($item_tit) == "") {$item_tit = stripslashes(get_output($item->get_var("titulo_pt")));}
      //$item_titurl = gera_titulo_amigavel(get_output($item->get_var("titulo_pt")));
      $item_tit = stripslashes(get_output($item->get_var("titulo")));
      $item_titurl = gera_titulo_amigavel(get_output($item->get_var("titulo")));
      $item_id = intval($item->get_var("id"));
      $item_cham = stripslashes(get_output($item->get_var("chamada_" . $_CONFIG["ref_lang"])));
      if(trim($item_cham) == "") {$item_cham = stripslashes(get_output($item->get_var("chamada_pt")));}
      $item_descr = stripslashes(get_output($item->get_var("descricao_" . $_CONFIG["ref_lang"])));
      if(trim($item_descr) == "") {$item_descr = stripslashes(get_output($item->get_var("descricao_pt")));}
      $item_bg = stripslashes(get_output($item->get_var("imagem")));
      
      $item_img = $item_bg;
      $item_folder = ROOT_SERVER . ROOT . $item->get_upload_folder("imagem");
      $cats = stripslashes(get_output($item->get_var("produtos_categorias")));          
      
      $item_categorias = "";
      $cats = explode(";",$cats);
      foreach($cats as $cid) {
        $cat = new produtos_categorias();
        $cat->inicia_dados();
        $cat->set_var("id",intval($cid));
        $cat->carrega_dados();

        if($cat != null) {
          $cat_id = intval($cat->get_var("id"));
          $cat_tit = stripslashes(get_output($cat->get_var("titulo_" . $_CONFIG["ref_lang"])));
          $cat_titurl = gera_titulo_amigavel(get_output($cat->get_var("titulo_pt")));
          if(trim($item_categorias) != "") {
            $item_categorias .= " <span class=\"mx-2\">&bullet;</span> ";  
          }
          $item_categorias .= " <a href=\"" . ROOT_SERVER . ROOT . "produtos/categoria/" . $cat_titurl . "/" . $cat_id . "\">" . $cat_tit . "</a>";
        }
      }

      $shareURL_FB = ROOT_SERVER . ROOT . $_ref_page . "/" . $_REQUEST["titulo"] . "/" . $_REQUEST["id"];
      
      $_TITULO_PAGINA = $item_tit;
      $_DESCRICAO_PAGINA = strip_tags($item_descr);
      //$_PALAVRAS_CHAVE_PAGINA = $item_tags;
      if(trim($item_img) != "") {
          $_IMG_SHARE = ROOT_SERVER . ROOT . UPLOAD_FOLDER . $item_img;
      }
  }
}

include_once("header.php"); 

?>

<div class="site-blocks-cover blog-cover overlay lazy" data-src="<?php echo $item_folder . $item_img; ?>" data-aos="fade">
  <div class="overlay-shadow"></div>
  <div class="container">
    <div class="row align-items-center justify-content-center">

          <div class="col-md-6 mt-lg-5 text-center">
            <h1><?php echo $item_tit; ?></h1>
            <p class="post-meta"><?php /*echo converte_data($item_data,3); ?> <span class="mx-2">&bullet;</span> <?php*/ if(trim($item_cham) != "") { echo $item_cham; } if(trim(strip_tags($item_categorias)) != "") { ?> <span class="mx-2">&bullet;</span> <?php } ?> <?php echo $item_categorias; ?></p>
            
          </div>
        
    </div>
  </div>
</div>  



<section class="site-section">
  <div class="container">
    <div class="row">
      <div class="col-12 blog-content">
        <div class="pt-5">
          <?php echo $item_descr; ?>
        </div>

      </div>
    </div>
  </div>
</section>

<?php

include_once("section-contato.php");

include_once("footer.php");

?>