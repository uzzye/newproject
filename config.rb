require 'compass/import-once/activate'
require 'ceaser-easing'

base_path = '.'
sass_dir = 'sass'
css_dir =  'css'
fonts_dir = 'fonts'
http_fonts_dir = 'newproject/fonts'
images_dir = 'img'
http_images_dir= 'newproject/img'
javascripts_dir = 'js'

#add_import_path "."
#add_import_path "angularapp/components"
#add_import_path "angularapp/pages"