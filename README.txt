++++++++++++++++++++++
+                    +
+     UzCMS v3.0     +
+                    +
++++++++++++++++++++++

TO USE THIS CMS PLEASE FOLLOW THE STEPS BELOW

I 	- Clone the "newproject" folder and rename it to the project

II 	- Remove the .git folder

III	- Create the database from the file "./cms/sql_tables.sql", replacing "newproject" to the table prefix for this project

IV	- Create additional tables using the file "./cms/sql_template.sql" as a template, replacing "newproject" to the table prefix for this project

V	- Modify the files below, renaming "newproject" to the table prefix for this project
	"config.php"
	"./cms/config.php"
	"config.rb"
	
VI	- Change the database configs in "./cms/_common/conexao.php"

VII	- Uncheck folders and files from "Read only" in order to use the MVC Manager

VIII	- Set writing permissions to the folder "upload"

IX	- Install node and run to install gulp for JS compilation:
		npm install gulp
		npm install gulp-uglify
		npm install gulp-concat
		npm install gulp-rename
		npm install gulp-csso

To better undestand the code structure follow the includes in "index.php" and "config.php" in the root folder for the project and under "./cms" for the CMS. Also check the key file "./cms/utils.php" for encoding and other useful functions.

The structure under CMS is a MVC-like, with the controller class being called in an AJAX call and including model and view classes.

The core JS are in "./cms/_js/core" and compiled to "./cms/_js/dist".

"uz*.*" are framework JS files while "cms.js" and "main.js" are CMS and Project exclusives.