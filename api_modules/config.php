<?php

// inicializa
$root = dirname( $_SERVER["PHP_SELF"] ) == DIRECTORY_SEPARATOR ? "" : dirname( $_SERVER["PHP_SELF"] );
$_CONFIG = array();

// configura sessão
@ini_set('session.gc_maxlifetime',24*60*60); // 1 dia
@ini_set('session.cookie_httponly',true);
@set_time_limit(0);

// configura constantes
if(!defined("ROOT")){
	define("ROOT",$root . "/../"); 	// UTILIZADO PARA INCLUDES/REFERENCIAS CLIENT-SIDE (LINKS, IMAGENS, JS E DEMAIS ARQUIVOS "EXTERNOS")
}

if(!defined("ROOT_CMS")){
	define("ROOT_CMS","../cms/");	// UTILIZADO PARA INCLUDES/REFERÊNCIAS SERVER-SIDE (PHP) DO CMS
}
if(!defined("ROOT_SITE")){
	define("ROOT_SITE","../");		// UTILIZADO PARA INCLUDES/REFERÊNCIAS SERVER-SIDE (PHP) DO SITE EM ESPECÍFICO
}
if(!defined("ROOT_SERVER")){
	//$rootServer = "http://" . $_SERVER["SERVER_NAME"];		// UTILIZADO PARA COMPARTILHAMENTO DE CONTEÚDO
	$rootServer = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]"; //:$_SERVER[SERVER_PORT]
	/*if(substr($rootServer,-1) != "/") {
		$rootServer .= "/";
	}*/
	define("ROOT_SERVER",$rootServer);
}

include_once("../config.php");

?>