<?php

/*
&meth=
    novaSenha
    login
    getUser
    cadastro
*/

include_once("config.php");

// SE FOR PARA GERAR NOVA SENHA

if(trim($methodAux) == "novaSenha") {
    $json = array();
    $json["timestamp"] = $data["timestamp"];
    $json["ip"] = $data["ip"];// = "127.0.0.1";
    $json["token_app"] = $data["token_app"];// = "DuendeY2014";
    $json["id_so"] = $data["id_so"];// = "970";
    if(trim($json["id_so"]) == "") {$json["id_so"] = 99;}

    $json["login"] = $data["login"];

    $array_final = array();
    
    carrega_classe("usuarios_site");

    $resultName = "login";
    $generic = true;
            
    $erro = "";
    $api_msg = "";

    $itens = null;
    $bProcessa = false;

    if(trim($json["login"]) != "")
    {
        // PROCURA USUÁRIO
        $itens = new usuarios_site();
        $itens->inicia_dados();

        $itens = $itens->localiza(array("usuario"),array($json["login"]));

        // SE NÃO ENCONTROU
        if(!is_array($itens) || sizeof($itens) == 0) {
            // NÃO RETORNA NADA
            $array_final = array();
            $erro = "Usuário inválido!";
        } else {
            // ATUALIZA E RETORNA O LOCALIZADO
            $itens = $itens[0];
            if(intval($itens->get_var("ativo")) == 1) {
                $bProcessa = true;
            }
            // NÃO ESTÁ ATIVO, RETORNA VAZIO
            else {
                $array_final = array();
                $erro = "Este usuário está inativo!";
            }
        }
    } else {
        $erro = "Informe o usuário para solicitar nova senha!";
    }

    if($bProcessa) {
        $email = stripslashes(get_output($itens->get_var("email")));
        $nome = stripslashes(get_output($itens->get_var("nome")));
        $id_inc = $itens->get_var("id");

        $itens->enviaEmailSenha();

        $item = new stdClass();
        $item->id = $id_inc;
        $item->nome = $nome;
        $item->email = $email;
        $item->apimsg = output_decode("Você recebeu um email de confirmação! Siga os passos descritos nele para ativar a nova senha.");
        $item->apistatus = 1;

        // ADICIONA

        $arrAux[sizeof($arrAux)] = $item;
    
        $array_final = array($arrAux);
    }

    if(trim($erro) != "") {
        $obj = new stdClass();
        $obj->erro = output_decode($erro);
        $obj->apimsg = $obj->erro;
        $obj->apistatus = 0;
        $array_final = array(array($obj));
    }

    if(!is_array($selAux)) {
        $selAux = array();
    }

    $selAux[sizeof($selAux)] = "id";
    $selAux[sizeof($selAux)] = "usuario";
    $selAux[sizeof($selAux)] = "nome";
    $selAux[sizeof($selAux)] = "email";

    $selAux[sizeof($selAux)] = "erro";	
    
    $selAux[sizeof($selAux)] = "apimsg";
    $selAux[sizeof($selAux)] = "apistatus";

    getJSONFinal($array_final, $generic, $resultName);
    die();
}

// SE FOR PARA FAZER LOGIN

else if(trim($methodAux) == "login") {
    $json = array();
    $json["timestamp"] = $data["timestamp"];
    $json["ip"] = $data["ip"];// = "127.0.0.1";
    $json["token_app"] = $data["token_app"];// = "DuendeY2014";
    $json["id_so"] = $data["id_so"];// = "970";
    if(trim($json["id_so"]) == "") {$json["id_so"] = 99;}

    $json["login"] = $data["login"];
    $json["senha"] = $data["senha"];
    $json["registration_id"] = $data["registration_id"];
    $json["md5"] = $data["md5"];
    if(isset($_REQUEST["md5"]) && ($json["md5"] == "false" || $json["md5"] == "0")) {
        $json["senha"] = md5(output_decode($json["senha"]));
    }
    $json["site"] = $data["site"];

    // FACEBOOK
    $json["facebook"] = $data["facebook"];
    $json["id_facebook"] = $data["id_facebook"];
    //$json["id_facebook"] = "100000501768632";
    $json["token_facebook"] = $data["token_facebook"];
    //$json["token_facebook"] = "EAAGOjZAvctXUBADtbw4fNjoIZAyDZAahHyEY0hIXd9g6TgvqzoybQpjVt1dZA5WXViGuJRpdADwNZAYIEnyDlqElYcZBC9p77AvCqcZB99C5MNZBOIqUn9oJ2m2dTIWSkNbdFULZAOBwocrm8un5XSGBZCaUAJlC9NbZAWaGH5hgwuw0AZDZD";

    $array_final = array();
    
    carrega_classe("usuarios_site");
    carrega_classe("salas_usuarios");

    $resultName = "login";
    $generic = true;
            
    $erro = "";
    $api_msg = "";

    $itens = null;
    $bProcessa = false;

    // SE FOR PARA LOGIN COM FACEBOOK

    if(intval($json["facebook"]) == 1) {
        //if(trim($json["id_facebook"]) != "" && trim($json["token_facebook"]) != "")
        if(trim($json["id_facebook"]) != "")
        {
            // PROCURA USUÁRIO
            $itens = new usuarios_site();
            $itens->inicia_dados();

            $itens = $itens->localiza(array("id_facebook"),array($json["id_facebook"]));

            // SE NÃO ENCONTROU
            if(!is_array($itens) || sizeof($itens) == 0) {
                // NÃO RETORNA NADA
                $array_final = array();
                $erro = "Nenhum usuário associado a esta conta de Facebook encontrado!";
            } else {
                // ATUALIZA E RETORNA O LOCALIZADO
                $itens = $itens[0];

                if(intval($json["site"]) == 1 && intval($itens->get_var("tipo")) != 1 && intval($itens->get_var("tipo")) != 2) {						
                    $array_final = array();
                    $erro = "Seu usuário não tem permissão de acesso via web. Por favor, utilize o app!";
                } else {
                    if(intval($itens->get_var("ativo")) == 1) {
                        if(trim($json["token_facebook"]) != "") {
                            $itens->set_var("token_facebook",$json["token_facebook"]);
                        }
                        $bProcessa = true;
                    }
                    // NÃO ESTÁ ATIVO, RETORNA VAZIO
                    else {
                        $array_final = array();
                        $erro = "Usuário está inativo!";
                    }
                }
            }
        }
    }

    // SE FOR PARA LOGIN NORMAL
    else {
        if(trim($json["login"]) != "" && trim($json["senha"]) != "")
        {
            // PROCURA USUÁRIO
            $itens = new usuarios_site();
            $itens->inicia_dados();

            $itens = $itens->localiza(array("usuario","senha"),array($json["login"],$json["senha"]));

            // SE NÃO ENCONTROU
            if(!is_array($itens) || sizeof($itens) == 0) {
                // NÃO RETORNA NADA
                $array_final = array();
                $erro = "Usuário ou senha inválidos!";
            } else {
                // ATUALIZA E RETORNA O LOCALIZADO
                $itens = $itens[0];

                if(intval($json["site"]) == 1 && intval($itens->get_var("tipo")) != 1 && intval($itens->get_var("tipo")) != 2) {						
                    $array_final = array();
                    $erro = "Seu usuário não tem permissão de acesso via web. Por favor, utilize o app!";
                } else {
                    if(intval($itens->get_var("ativo")) == 1) {
                        $bProcessa = true;
                    }
                    // NÃO ESTÁ ATIVO, RETORNA VAZIO
                    else {
                        $array_final = array();
                        $erro = "Este usuário está inativo!";
                    }
                }
            }
        }
    }

    // SE ACHOU USUÁRIO, MOSTRA
    if($bProcessa) {

        // SE USER ESTÁ ATIVO, LOGA
        if(intval($json["site"]) == 1) {
            $itens->set_var("data_ultimoacesso",Date("Y-m-d H:i:s"));

            $_SESSION["idLogin_site"] = $itens->get_var("id");
            $_SESSION["btLogin_site"] = true;
            $_SESSION["strLogin_site"] = $itens->get_var("usuario");
            $_SESSION["emailLogin_site"] = $itens->get_var("email");
            $_SESSION["nomeLogin_site"] = $itens->get_var("nome");
            $_SESSION["data_ultimoacesso_site"] = date("Y/m/d H:i:s");
        } else {
            $itens->set_var("data_ultimo_acesso_app",Date("Y-m-d H:i:s"));
        }
        $reg_ids = get_output($itens->get_var("registration_ids"));
        if(!strstr(";" . $reg_ids . ";", ";" . $json["registration_id"] . ";")) {
            if(trim($reg_ids) != "") {
                $reg_ids .= ";";
            }
            $reg_ids .= $json["registration_id"];
        }
        $itens->set_var("registration_ids",output_decode($reg_ids));

        $id_inc = $itens->get_var("id");

        $itens->edita(false,true);

        $itens = new usuarios_site();
        $itens = $itens->get_array_ativos("id = " . intval($id_inc));

        $array_final = array();

        if(is_array($itens) && sizeof($itens) > 0){

            $folderAux = UPLOAD_FOLDER;
            if(@trim($objAux->upload_folder) != "" && @trim($objAux->upload_folder) != $folderAux) {
                $folderAux = UPLOAD_FOLDER . $objAux->upload_folder;
            }
            $folderImageAux = $folderAux;
            if(@trim($objAux->upload_folders["imagem"]) != "" && @trim($objAux->upload_folder) != $folderImageAux) {
                $folderImageAux = UPLOAD_FOLDER . $objAux->upload_folders["imagem"];
            }

            $arrAux = array();

            foreach($itens as $item) {
                //$item->prop = "";

                // TRATA IMAGEM

                // A IMAGEM JÁ É SALVA DO FACEBOOK EM BASE64, NÃO PRECISA MAIS
                /*if($base64) {
                    $arqAux = $item->get_var("imagem");
                    $output = @file_get_contents(getcwd() . DIRECTORY_SEPARATOR . $folderImageAux . $arqAux);// . "&" . date("YmdHis"));
                    $arqAux = base64_encode($output);
                    $item->imagem_base64 = $arqAux;
                }*/

                // MONTA EMPREENDIMENTOS

                /* $empsAux = array();
                $sqlWhereAux = " principal.id IN (SELECT eu.id_empreendimento FROM " . DBTABLE_EMPREENDIMENTOS_USUARIOS . " eu WHERE eu.id_usuario_site = " . intval($item->get_var("id")) . ") ";
                $itsAux = new empreendimentos();
                $itsAux = $itsAux->get_array_ativos($sqlWhereAux,"","",true);
                if(is_array($itsAux) && sizeof($itsAux) > 0){
                    foreach($itsAux as $emp) {
                        if(intval($emp->get_var("ativo")) == 1) {
                            $empsAux[sizeof($empsAux)] = $emp;
                        }
                    }
                }

                $item->empreendimentos_usuarios = $empsAux; */

                // REGISTRATION ID

                $item->registration_id_recebido = $json["registration_id"];

                $item->apimsg = output_decode("Autenticando...");
                $item->apistatus = 1;

                // ADICIONA

                $arrAux[sizeof($arrAux)] = $item;
            }

            $array_final = array($arrAux);
        }
    }

    if(trim($erro) != "") {
        $obj = new stdClass();
        $obj->erro = output_decode($erro);
        $obj->apimsg = $obj->erro;
        $obj->apistatus = 0;
        $array_final = array(array($obj));
    }

    if(!is_array($selAux)) {
        $selAux = array();
    }

    $selAux[sizeof($selAux)] = "id";
    $selAux[sizeof($selAux)] = "usuario";
    $selAux[sizeof($selAux)] = "nome";
    $selAux[sizeof($selAux)] = "email";
    $selAux[sizeof($selAux)] = "id_facebook";
    $selAux[sizeof($selAux)] = "token_facebook";
    $selAux[sizeof($selAux)] = "tamanho_avatar";
    $selAux[sizeof($selAux)] = "relacionamento";
    $selAux[sizeof($selAux)] = "data_nascimento";
    $selAux[sizeof($selAux)] = "sexo";
    $selAux[sizeof($selAux)] = "cidade";
    $selAux[sizeof($selAux)] = "estado";
    $selAux[sizeof($selAux)] = "pais";
    $selAux[sizeof($selAux)] = "cep";
    $selAux[sizeof($selAux)] = "endereco";
    $selAux[sizeof($selAux)] = "telefone";
    $selAux[sizeof($selAux)] = "celular";
    $selAux[sizeof($selAux)] = "imagem";
    $selAux[sizeof($selAux)] = "imagem_url";
    $selAux[sizeof($selAux)] = "numero";
    $selAux[sizeof($selAux)] = "tipo";	
    $selAux[sizeof($selAux)] = "cpf";	

    $selAux[sizeof($selAux)] = "erro";	
    
    $selAux[sizeof($selAux)] = "apimsg";
    $selAux[sizeof($selAux)] = "apistatus";

    $selAux[sizeof($selAux)] = "registration_id_recebido";
    $selAux[sizeof($selAux)] = "registration_ids";

    getJSONFinal($array_final, $generic, $resultName);
    die();
}

// SE FOR PARA BUSCAR USUARIO

else if(trim($methodAux) == "getUser") {
    $json = array();
    $json["timestamp"] = $data["timestamp"];
    $json["ip"] = $data["ip"];// = "127.0.0.1";
    $json["token_app"] = $data["token_app"];// = "DuendeY2014";
    $json["id_so"] = $data["id_so"];// = "970";
    if(trim($json["id_so"]) == "") {$json["id_so"] = 99;}
    
    $json["id_usuario"] = $data["id_usuario"];

    $array_final = array();
    
    carrega_classe("usuarios_site");
    carrega_classe("salas_usuarios");

    $resultName = "usuarios_site";
    $generic = true;
            
    $erro = "";
    $api_msg = "";

    $itens = null;
    $bProcessa = false;

    if(intval($json["id_usuario"]) > 0) {

        $itens = new usuarios_site();
        $itens = $itens->get_array_ativos("id = " . intval($json["id_usuario"]));

        $array_final = array();

        if(is_array($itens) && sizeof($itens) > 0){

            $folderAux = UPLOAD_FOLDER;
            if(@trim($objAux->upload_folder) != "" && @trim($objAux->upload_folder) != $folderAux) {
                $folderAux = UPLOAD_FOLDER . $objAux->upload_folder;
            }
            $folderImageAux = $folderAux;
            if(@trim($objAux->upload_folders["imagem"]) != "" && @trim($objAux->upload_folder) != $folderImageAux) {
                $folderImageAux = UPLOAD_FOLDER . $objAux->upload_folders["imagem"];
            }

            $arrAux = array();

            foreach($itens as $item) {
                //$item->prop = "";

                // TRATA IMAGEM

                // A IMAGEM JÁ É SALVA DO FACEBOOK EM BASE64, NÃO PRECISA MAIS
                /*if($base64) {
                    $arqAux = $item->get_var("imagem");
                    $output = @file_get_contents(getcwd() . DIRECTORY_SEPARATOR . $folderImageAux . $arqAux);// . "&" . date("YmdHis"));
                    $arqAux = base64_encode($output);
                    $item->imagem_base64 = $arqAux;
                }*/

                // MONTA EMPREENDIMENTOS

                /*$empsAux = array();
                $sqlWhereAux = " principal.id IN (SELECT eu.id_empreendimento FROM " . DBTABLE_EMPREENDIMENTOS_USUARIOS . " eu WHERE eu.id_usuario_site = " . intval($item->get_var("id")) . ") ";
                $itsAux = new empreendimentos();
                $itsAux = $itsAux->get_array_ativos($sqlWhereAux,"","",true);
                if(is_array($itsAux) && sizeof($itsAux) > 0){
                    foreach($itsAux as $emp) {
                        if(intval($emp->get_var("ativo")) == 1) {
                            $empsAux[sizeof($empsAux)] = $emp;
                        }
                    }
                }

                $item->empreendimentos_usuarios = $empsAux;*/

                // REGISTRATION ID

                $item->registration_id_recebido = $json["registration_id"];

                $item->apimsg = output_decode("Autenticando...");
                $item->apistatus = 1;

                // ADICIONA

                $arrAux[sizeof($arrAux)] = $item;
            }

            $array_final = array($arrAux);
        }
        else {
            $erro = "Usuário não informado!";
        }
    }
    else {
        $erro = "Usuário não informado!";
    }

    if(trim($erro) != "") {
        $obj = new stdClass();
        $obj->erro = output_decode($erro);
        $obj->apimsg = $obj->erro;
        $obj->apistatus = 0;
        $array_final = array(array($obj));
    }

    if(!is_array($selAux)) {
        $selAux = array();
    }

    $selAux[sizeof($selAux)] = "id";
    $selAux[sizeof($selAux)] = "usuario";
    $selAux[sizeof($selAux)] = "nome";
    $selAux[sizeof($selAux)] = "email";
    $selAux[sizeof($selAux)] = "id_facebook";
    $selAux[sizeof($selAux)] = "token_facebook";
    $selAux[sizeof($selAux)] = "tamanho_avatar";
    $selAux[sizeof($selAux)] = "relacionamento";
    $selAux[sizeof($selAux)] = "data_nascimento";
    $selAux[sizeof($selAux)] = "sexo";
    $selAux[sizeof($selAux)] = "cidade";
    $selAux[sizeof($selAux)] = "estado";
    $selAux[sizeof($selAux)] = "pais";
    $selAux[sizeof($selAux)] = "cep";
    $selAux[sizeof($selAux)] = "endereco";
    $selAux[sizeof($selAux)] = "telefone";
    $selAux[sizeof($selAux)] = "celular";
    $selAux[sizeof($selAux)] = "imagem";
    $selAux[sizeof($selAux)] = "imagem_url";
    $selAux[sizeof($selAux)] = "numero";
    $selAux[sizeof($selAux)] = "tipo";	
    $selAux[sizeof($selAux)] = "cpf";	

    $selAux[sizeof($selAux)] = "erro";	
    
    $selAux[sizeof($selAux)] = "apimsg";
    $selAux[sizeof($selAux)] = "apistatus";

    $selAux[sizeof($selAux)] = "registration_id_recebido";
    $selAux[sizeof($selAux)] = "registration_ids";

    getJSONFinal($array_final, $generic, $resultName);
    die();
}

// SE FOR PARA FAZER CADASTRO

else if(trim($methodAux) == "cadastro") {
    $json = array();
    $json["timestamp"] = $data["timestamp"];
    $json["ip"] = $data["ip"];// = "127.0.0.1";
    $json["token_app"] = $data["token_app"];// = "DuendeY2014";
    $json["id_so"] = $data["id_so"];// = "970";
    if(trim($json["id_so"]) == "") {$json["id_so"] = 99;}

    $json["id_usuario"] = $data["id_usuario"];
    $json["id_empreendimento"] = $data["id_empreendimento"];
    $json["id_sala"] = $data["id_sala"];

    $json["edita"] = $data["edita"];
    
    $json["senha_atual"] = $data["senha_atual"];
    $json["senha_nova"] = $data["senha_nova"];
    $json["nome"] = $data["nome"];
    $json["email"] = $data["email"];
    $json["telefone"] = $data["telefone"];
    $json["celular"] = $data["celular"];
    $json["cep"] = $data["cep"];
    $json["endereco"] = $data["endereco"];
    $json["cidade"] = $data["cidade"];
    $json["estado"] = $data["estado"];
    $json["permite_mensagem"] = $data["permite_mensagem"];

    // FACEBOOK
    $json["facebook"] = $data["facebook"];
    $json["id_facebook"] = $data["id_facebook"];
    //$json["id_facebook"] = "100000501768632";
    $json["token_facebook"] = $data["token_facebook"];
    //$json["token_facebook"] = "EAAGOjZAvctXUBADtbw4fNjoIZAyDZAahHyEY0hIXd9g6TgvqzoybQpjVt1dZA5WXViGuJRpdADwNZAYIEnyDlqElYcZBC9p77AvCqcZB99C5MNZBOIqUn9oJ2m2dTIWSkNbdFULZAOBwocrm8un5XSGBZCaUAJlC9NbZAWaGH5hgwuw0AZDZD";
    $json["nascimento"] = $data["nascimento"];
    $json["imagem"] = $data["imagem"];
    $json["relacionamento"] = $data["relacionamento"];
    $json["sexo"] = $data["sexo"];

    $json["cpf"] = $data["cpf"];
    if(trim($json["cpf"]) != "") {
        $json["cpf"] = str_replace(".","",str_replace("-","",str_replace("/","",str_replace(" ","",$json["cpf"]))));
    }

    //file_put_contents('api_log.txt', print_r($json, true));

    // TODO FAZER FACEBOOK

    $array_final = array();

    $resultName = "cadastro";
    $generic = true;

    $bCadastra = true;
            
    $erro = "";

    carrega_classe("usuarios_site");

    // SE FACEBOOK
    if(intval($json["facebook"]) == 1) {

        // SE FOR PARA VINCULAR
        if(trim($json["id_facebook"]) != "" && trim($json["id_facebook"]) != "0") {
            
            // PROCURA USUÁRIO
            $itens = new usuarios_site();
            $itens->inicia_dados();

            $itens = $itens->localiza(array("id_facebook"),array(output_decode($json["id_facebook"])));

            // SE NÃO ENCONTROU
            if(!$json["edita"] && (!is_array($itens) || sizeof($itens) == 0)) {
                // NÃO RETORNA NADA
                $bCadastra = true;
                $array_final = array();
            } else {
                if(!$json["edita"]) {
                    $bCadastra = false;
                    $erro = "Esta conta de Facebook já está associada a outro usuário.";
                } else {
                    foreach($itens as $item) {
                        if(intval($item->get_var("id")) != intval($json["id_usuario"])) {
                            $bCadastra = false;
                            $erro = "Esta conta de Facebook já está associada a outro usuário.";
                        }
                    }
                }
            }
        }

        // SE FOR PARA DESVINCULAR
        else {
            $bCadastra = false;

            $itens = new usuarios_site();

            $itens->inicia_dados();
            $itens->set_var("id",intval($json["id_usuario"]));
            $itens->carrega_dados();

            $itens->set_var("id_facebook","");
            $itens->set_var("token_facebook","");

            $id_inc = $itens->edita(false,true);
            
            $itens = new usuarios_site();
            $itens->inicia_dados();
            $itens = $itens->localiza(array("id"),array(intval($id_inc)));
            
            $itens[0]->apimsg = output_decode("Dados alterados com sucesso!");
            $itens[0]->apistatus = 1;

            $array_final = array($itens);
        }

        if($bCadastra) {

            $itens = new usuarios_site();

            $itens->inicia_dados();

            if($json["edita"]) {
                $itens->set_var("id",intval($json["id_usuario"]));
                $itens->carrega_dados();
            }

            // FACEBOOK

            // BUSCA FB DO USER
            
            //SDK ANTIGO
            //require_once(ROOT_CMS . "classes/vendor/facebook.php");
            require_once("classes/facebook.php");

            //var_dump($FB_config); die();

            $facebook = new Facebook($FB_config);
              $facebook->setAccessToken($json["token_facebook"]);

            $fb = null;

              // SDK 4
            /*require_once("classes/Facebook/autoload.php");
            //require_once(ROOT_CMS . "classes/vendor/Facebook/autoload.php");

            $facebook = new Facebook\Facebook($FB_config);
            $facebook->setDefaultAccessToken($json["token_facebook"]);

            $fb = null;*/

            try {
                // SDK ANTIGO
                $fb = $facebook->api("/me");
                  $fb_user_id = $fb["id"];

                  //$fb = $facebook->api("/". $fb_user_id . "?fields=id,name,location,relationship_status,gender,birthday&locale=pt_BR");
                
                // SDK 4
                /*$fb = $facebook->get("/me");
                $fb = $fb->getDecodedBody();
                  //$userNode = $fb->getGraphUser();*/

                //$fb_user_id = $fb["id"];
                
                // SDK ANTIGO
                $fb = $facebook->api("/". $fb_user_id . "?fields=id,name,location,relationship_status,gender,birthday&locale=pt_BR");

                // SDK 4
                  /*$fb = $facebook->get("/". $fb_user_id . "?fields=id,name,location,relationship_status,gender,birthday&locale=pt_BR");
                  $fb = $fb->getDecodedBody();*/

                  $relacionamento = output_decode($fb["relationship_status"]);
                  $sexo = output_decode($fb["gender"]);

                  // MONTA NASCIMENTO
                  $nascimento = $fb["birthday"];
                  if(trim($nascimento) != "") {
                      $nascimento = substr($nascimento,6,4) . "-" . substr($nascimento,0,2) . "-" . substr($nascimento,3,2);
                  }

                  // BUSCA FOTO
                $filesize = "";
                $output = "";
                    
                //$output_url = "https://graph.facebook.com/" . $fb_user_id . "/picture?type=large&" . date("YmdHis");
                $output_url = "https://graph.facebook.com/" . $fb_user_id . "/picture?type=square&width=500&height=500&" . date("YmdHis");

                @ini_set('user_agent', 'Ohtel Condomínios Inteligentes');
                //$image = json_decode(@file_get_contents($output_url . "&redirect=false&access_token=" . $access_token),true);
                $ch = @curl_init();
                @curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                @curl_setopt($ch, CURLOPT_HEADER, false);
                @curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                //@curl_setopt($ch, CURLOPT_URL, $image["data"]["url"]);
                //@curl_setopt($ch, CURLOPT_REFERER, $image["data"]["url"]);
                @curl_setopt($ch, CURLOPT_URL, $output_url . "&redirect=true&access_token=" . $access_token);
                @curl_setopt($ch, CURLOPT_REFERER, $output_url . "&redirect=true&access_token=" . $access_token);
                @curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                $output = @curl_exec($ch);
                @curl_close($ch);

                if(trim($output) != "") {
                    $filesize = strlen($output);
                    $output = base64_encode($output);
                }

                // PEGA LOCAL
                $cidade = "";
                  $estado = "";
                  $pais = "";

                $loc_id = $fb["location"]["id"];

                if(trim($loc_id) != "") {
                    // SDK ANTIGO
                    $fb_loc = $facebook->api("/". $loc_id . "?fields=location&locale=pt_BR");
                    // SDK 4
                      /*$fb_loc = $facebook->get("/". $loc_id . "?fields=location&locale=pt_BR");
                      $fb_loc = $fb_loc->getDecodedBody();*/

                      $cidade = $fb_loc["location"]["city"];
                      $estado = $fb_loc["location"]["state"];
                      $pais = $fb_loc["location"]["country"];
                }
            //} catch(Facebook\Exceptions\FacebookResponseException $e) {
                //// When Graph returns an error
                //echo 'Graph returned an error: ' . $e->getMessage();
                //exit;
            //} catch(Facebook\Exceptions\FacebookSDKException $e) {
                //// When validation fails or other local issues
                //echo 'Facebook SDK returned an error: ' . $e->getMessage();
                //exit;
            } catch(Exception $e) {
                //echo $e->getMessage();
                //die();
            }

            // REGISTRA

            $itens->set_var("id_facebook",output_decode($json["id_facebook"]));
            $itens->set_var("token_fb",output_decode($json["token_facebook"]));
            $itens->set_var("imagem", output_decode($output));
            $itens->set_var("imagem_url", output_decode($output_url));
            $itens->set_var("tamanho_avatar", output_decode($filesize));
            $itens->set_var("cidade",$cidade);
            $itens->set_var("estado",$estado);
            $itens->set_var("pais",$pais);
            $itens->set_var("relacionamento",$relacionamento);
            $itens->set_var("data_nascimento",output_decode($nascimento));
            $itens->set_var("sexo",output_decode(ucfirst($sexo)));
            //$itens->set_var("nome",output_decode($fb["name"]));
            //$itens->set_var("email",output_decode($fb["email"]));

            // APENAS NA INCLUSÃO
            if(!$json["edita"]) {
                $itens->set_var("geolocalizacao","0,0");
                $itens->set_var("ativo",0);
                $itens->set_var("cpf",output_decode($json["cpf"]));
                $itens->set_var("tipo",1);
            } else {
            }

            //$ip = get_ip();
            $ip = $json["ip"];
            $itens->set_var("ip",output_decode($ip));
            // FIM APENAS

            if(!$json["edita"]) {
                $id_inc = $itens->inclui(false,true);
            } else {
                $id_inc = $itens->edita(false,true);
            }

            $itens = new usuarios_site();
            $itens->inicia_dados();
            $itens = $itens->localiza(array("id"),array(intval($id_inc)));
            
            $itens[0]->apimsg = output_decode("Dados alterados com sucesso!");
            $itens[0]->apistatus = 1;

            $array_final = array($itens);
        } else {
            if(trim($erro) != "") {				
                $obj = new stdClass();
                $obj->erro = output_decode($erro);
                $obj->apimsg = $obj->erro;
                $obj->apistatus = 0;
                $array_final = array(array($obj));
            }
        }
    }

    // SE NORMAL
    else {
        // PROCURA USUÁRIO
        $itens = new usuarios_site();
        $itens->inicia_dados();

        $itens = $itens->localiza(array("email"),array(output_decode($json["email"])));

        // SE NÃO ENCONTROU
        if(!$json["edita"] && (!is_array($itens) || sizeof($itens) == 0)) {
            // NÃO RETORNA NADA
            $bCadastra = true;
            $array_final = array();
        } else {
            if(!$json["edita"]) {
                $bCadastra = false;
                $erro = "E-mail já está cadastrado.";
            } else {
                foreach($itens as $item) {
                    if(intval($item->get_var("id")) != intval($json["id_usuario"])) {
                        $bCadastra = false;
                        $erro = "E-mail já está cadastrado.";
                    }
                }
            }
        }

        // SE TROCOU A SENHA E A SENHA É INVÁLIDA
        if($json["edita"] && trim($json["senha_nova"]) != "") {
            /*if($json["senha_atual"] != $json["senha_nova"]) {
                $bCadastra = false;
                $erro = "As senhas não conferem.";
            }
            else*/ {
                // PROCURA USUÁRIO
                $itens = new usuarios_site();
                $itens->inicia_dados();

                $itens = $itens->localiza(array("id","senha"),array($json["id_usuario"],md5(output_decode($json["senha_atual"]))));

                if(!is_array($itens) || sizeof($itens) == 0) {
                    $bCadastra = false;
                    $erro = "A senha atual é inválida.";					
                }
            }
        }

        if($bCadastra) {

            $itens = new usuarios_site();

            $itens->inicia_dados();

            if($json["edita"]) {
                $itens->set_var("id",intval($json["id_usuario"]));
                $itens->carrega_dados();

                if($itens->get_var("email") == $itens->get_var("usuario")) {
                    $itens->set_var("usuario",output_decode($json["email"]));
                }
            }

            $itens->set_var("usuario_criacao",output_decode($json["id_so"]));
            $itens->set_var("data_criacao",Date("Y-m-d H:i:s"));
            $itens->set_var("data_ultimo_acesso_app",Date("Y-m-d H:i:s"));

            $itens->set_var("nome",output_decode($json["nome"]));
            $itens->set_var("email",output_decode($json["email"]));
            $itens->set_var("telefone",output_decode($json["telefone"]));
            $itens->set_var("celular",output_decode($json["celular"]));
            $itens->set_var("cep",output_decode($json["cep"]));
            $itens->set_var("endereco",output_decode($json["endereco"]));
            $itens->set_var("cidade",output_decode($json["cidade"]));
            $itens->set_var("estado",output_decode($json["estado"]));
            $itens->set_var("cpf",output_decode($json["cpf"]));
            //$itens->set_var("pais",output_decode($json["pais"]));
            $itens->set_var("permite_mensagem",output_decode($json["permite_mensagem"]));

            if(!$json["edita"] || trim($json["senha_nova"]) != "") {
                $itens->set_var("senha",md5(output_decode($json["senha_nova"])));
            }

            // APENAS NA INCLUSÃO
            if(!$json["edita"]) {
                $itens->set_var("geolocalizacao","0,0");
                $itens->set_var("ativo",0);
                $itens->set_var("tipo",1);
            } else {
            }

            //$ip = get_ip();
            $ip = $json["ip"];
            $itens->set_var("ip",output_decode($ip));
            // FIM APENAS

            if(!$json["edita"]) {
                $id_inc = $itens->inclui(false,true);
            } else {
                $id_inc = $itens->edita(false,true);
            }

            $itens = new usuarios_site();
            $itens->inicia_dados();
            $itens = $itens->localiza(array("id"),array(intval($id_inc)));

            if(is_array($itens) && sizeof($itens) > 0) {
                for($w=0; $w<sizeof($itens); $w++) {
                    $itens[0]->apimsg = output_decode("Dados alterados com sucesso!");
                    $itens[0]->apistatus = 1;
                }
            }

            $array_final = array($itens);
        } else {
            if(trim($erro) != "") {				
                $obj = new stdClass();
                $obj->erro = output_decode($erro);
                $obj->apimsg = $obj->erro;
                $obj->apistatus = 0;
                $array_final = array(array($obj));
            }
        }
    }

    // FINALIZA

    if(!is_array($selAux)) {
        $selAux = array();
    }

    $selAux[sizeof($selAux)] = "id";
    $selAux[sizeof($selAux)] = "usuario";
    $selAux[sizeof($selAux)] = "nome";
    $selAux[sizeof($selAux)] = "email";
    $selAux[sizeof($selAux)] = "id_facebook";
    $selAux[sizeof($selAux)] = "token_facebook";
    $selAux[sizeof($selAux)] = "tamanho_avatar";
    $selAux[sizeof($selAux)] = "relacionamento";
    $selAux[sizeof($selAux)] = "data_nascimento";
    $selAux[sizeof($selAux)] = "sexo";
    $selAux[sizeof($selAux)] = "cidade";
    $selAux[sizeof($selAux)] = "estado";
    $selAux[sizeof($selAux)] = "pais";
    $selAux[sizeof($selAux)] = "cep";
    $selAux[sizeof($selAux)] = "endereco";
    $selAux[sizeof($selAux)] = "telefone";
    $selAux[sizeof($selAux)] = "celular";
    $selAux[sizeof($selAux)] = "imagem";
    $selAux[sizeof($selAux)] = "imagem_url";
    $selAux[sizeof($selAux)] = "numero";
    $selAux[sizeof($selAux)] = "tipo";	
    $selAux[sizeof($selAux)] = "cpf";	
    
    $selAux[sizeof($selAux)] = "erro";

    $selAux[sizeof($selAux)] = "apimsg";
    $selAux[sizeof($selAux)] = "apistatus";

    //getJSONFinal($array_final, $generic, $resultName);
    getJSONFinal($array_final, $generic, $resultName);
    die();
}

?>