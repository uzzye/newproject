<?php	
    include_once("config.php");

	$_SESSION["btLogin_site"] = false;
	$strUsu = $_POST["usuario"];
	$strSenha = $_POST["senha"];	

	$sMsg = "";

	if(trim($_REQUEST["verifier"]) != "") {
		$sMsg = "<!--ERROR-->Preenchimento de formulário inválido!";
	}
	else if(trim($strUsu) == "" || trim($strSenha) == "") {
		$sMsg = "<!--ERROR-->Usuário e senha são obrigatórios!";
	}
	else {
		$sqlAux = "SELECT * FROM " . DBTABLE_USUARIOS . "
			WHERE TRIM(usuario) != \"\"
			  AND TRIM(usuario) = \"".$db->escape_string($strUsu)."\"
			  AND ativo = 1
			ORDER BY data_criacao DESC ";
			//AND ( ISNULL(validade) OR DATE(validade) = \"0000-00-00\" OR DATE(validade) >= CURRENT_DATE() )
		//echo $sqlAux; die();
		$resAux = $mysql->exec_query($sqlAux);// or die(@mysql_error());

		if ($mysql->num_rows($resAux) > 0) {
			$sqlAux = "SELECT * FROM " . DBTABLE_USUARIOS . "
			WHERE TRIM(usuario) != \"\"
			  AND TRIM(senha) != \"\"
			  AND TRIM(usuario) = \"".$db->escape_string($strUsu)."\"
			  AND TRIM(senha) = MD5(\"".$strSenha."\")
			  AND ativo = 1
			ORDER BY data_criacao DESC ";
			//AND ( ISNULL(validade) OR DATE(validade) = \"0000-00-00\" OR DATE(validade) >= CURRENT_DATE() )
			//echo $sqlAux; die();
			$resAux = $mysql->exec_query($sqlAux);// or die(@mysql_error());
		
			if ($mysql->num_rows($resAux) > 0) {
				$rsUsuario = $mysql->result_array($resAux);
				$_SESSION["idLogin_site"] = $rsUsuario["id"];
				$_SESSION["btLogin_site"] = true;
				$_SESSION["strLogin_site"] = $rsUsuario["email"];
				$_SESSION["emailLogin_site"] = $rsUsuario["email"];

      			$_SESSION["_site_login_id_usuario"] = $rsUsuario["id"];
      			$_SESSION["_site_login_nome"] = $rsUsuario["nome"];
      			$_SESSION["_site_login_email"] = $rsUsuario["email"];
      			//$_SESSION["_site_login_endereco"] = $rsUsuario["endereco"];

				$_SESSION["data_ultimoacesso_site"] = date("Y/m/d H:i:s");
				$_SESSION["GLOBAL_RANDOM_KEY"] = intval($_SESSION["idLogin_site"]) . str_pad(rand(0,99999),5,"0",STR_PAD_LEFT);			
				$comando = $mysql->exec_query("UPDATE " . DBTABLE_USUARIOS . " SET data_ultimo_acesso = NOW() WHERE id = '".$rsUsuario["id"]."'");
			
				$sMsg = "<!--SUCESSO-->Autenticando, aguarde...";
			}
			else {
				$_SESSION["btLogin_site"] = false;
				
				$sMsg = "<!--ERROR-->Usuário ou senha inválidos!";
			}
		}
		else {
			$_SESSION["btLogin_site"] = false;
			
			$sMsg = "<!--ERROR-->Usuário não encontrado ou desativado! Por favor, realize um novo cadastro.";
		}
	}

	if(trim($sMsg) != "") {
		ob_clean();
		echo $sMsg;
		die();
	}
?>
