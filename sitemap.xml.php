<?php

include_once("config.php");

carrega_classe("nomedaclasse");

/*<image:image>
  <image:loc>http://example.com/image.jpg</image:loc>
</image:image>
<video:video>
  <video:content_loc>http://www.example.com/video123.flv</video:content_loc>
  <video:player_loc allow_embed="yes" autoplay="ap=1">http://www.example.com/videoplayer.swf?video=123</video:player_loc>
  <video:thumbnail_loc>http://www.example.com/miniaturas/123.jpg</video:thumbnail_loc>
  <video:title>Grelhando carne no verão</video:title>
  <video:description>Deixe os bifes sempre no ponto</video:description>
</video:video>*/

ob_clean();

header("Content-Type: text/xml; charset=UTF-8");

?>
<urlset xmlns="http://www.google.com/schemas/sitemap/0.9"
      xmlns:image="http://www.google.com/schemas/sitemap-image/1.1"
      xmlns:video="http://www.google.com/schemas/sitemap-video/1.1">

<url>
  <loc><?php echo ROOT_SERVER . ROOT; ?></loc>
  <changefreq>monthly</changefreq>
  <priority>0.5</priority>
</url>

<?php /*
<url>
  <loc><?php echo ROOT_SERVER . ROOT; ?>nomedaclasse/</loc>
  <changefreq>monthly</changefreq>
  <priority>0.5</priority>
</url>

<?php

// NOMEDACLASSE

$its = new nomedaclasse();
$its = $its->get_array_ativos("","ranking ASC, data_atualizacao DESC, data_criacao DESC");

if(is_array($its) && sizeof($its) > 0) {
  $priorAux = 1.0;
  foreach ($its as $it) {
    $idAux = intval($it->get_var("id"));
    //$dataAux = encode_data($it->get_var("data_atualizacao"));
    //$dataAux = substr($it->get_var("data_atualizacao"),0,10);
    $dataAux = $it->get_var("data_atualizacao"); if(trim($dataAux) == "" || trim($dataAux) == "0000-00-00 00:00:00") {$dataAux = $it->get_var("data_criacao");} if(trim($dataAux) == "" || trim($dataAux) == "0000-00-00 00:00:00") {$dataAux = Date("Y-m-d H:i:s");}
    $timeStamp = strtotime($dataAux);
    $dataAux = Date('c', $timeStamp);
    $titAux = stripcslashes(get_output($it->get_var("titulo")));
    $imgAux = stripcslashes(get_output($it->get_var("imagem")));

    ?>
    <url>
      <loc><?php echo ROOT_SERVER . ROOT . "colecao/" . gera_titulo_amigavel($titAux) . "/" . $idAux; ?>/</loc>
      <changefreq>monthly</changefreq>
      <priority><?php echo number_format($priorAux,1,".",""); ?></priority>
      <lastmod><?php echo $dataAux; ?></lastmod>
      <?php if(trim($imgAux) != "") { ?>
      <image:image>
        <image:loc><?php echo ROOT_SERVER . ROOT . $it->get_upload_folder("imagem") . $imgAux; ?></image:loc>
      </image:image>
      <?php } ?>
    </url>
    <?php

    $priorAux -= 0.1;
    if($priorAux < 0.1) {
      $priorAux = 0.1;
    }
  }
}
*/

?>
</urlset>
<?php

//  Return the contents of the output buffer
$htmlStr = ob_get_contents();
// Clean (erase) the output buffer and turn off output buffering
//ob_end_clean(); 
// Write final string to file
file_put_contents("sitemap.xml", $htmlStr);

?>