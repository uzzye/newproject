<?php

include_once("config.php");

carrega_classe("banners");

$banners = new banners();
if(intval($_REQUEST["replica"]) == 1) {
    $banners = $banners->get_array_ativos("","ranking DESC",1);
} else {
    $banners = $banners->get_array_ativos("","ranking ASC");
}

if(is_array($banners) && sizeof($banners) > 0) {
    ?>
    <!-- Home Section -->
    <section class="fullScreen home" id="home">
        <div class="anchor home mt-1" data-tpad="0"></div>
        <div id="banner-home" class="carousel slide h-100" data-ride="carousel">
            <div class="carousel-inner h-100">
                <?php
                $strNav = "";
                $iCount = 0;
                foreach($banners as $banner) {

                    $it_tit = stripslashes(get_output($banner->get_var("titulo_" . $_CONFIG["ref_lang"])));
                    if(trim($it_tit) == "") {$it_tit = stripslashes(get_output($banner->get_var("titulo_pt")));}
                    $it_descr = stripslashes(get_output($banner->get_var("descricao_" . $_CONFIG["ref_lang"])));
                    if(trim($it_descr) == "") {$it_descr = stripslashes(get_output($banner->get_var("descricao_pt")));}
                    $it_cor = stripslashes(get_output($banner->get_var("cor_titulo")));
                    $it_cin = stripslashes(get_output($banner->get_var("cinemagraph")));
                    $it_vid = stripslashes(get_output($banner->get_var("video_url")));
                    $it_url = stripslashes(get_output($banner->get_var("url")));
                    $it_target = stripslashes(get_output($banner->get_var("target")));
                    $it_btn = stripslashes(get_output($banner->get_var("texto_botao_" . $_CONFIG["ref_lang"])));
                    if(trim($it_btn) == "") {$it_btn = stripslashes(get_output($banner->get_var("texto_botao_pt")));}

                    $bg_field = "imagem_bg_" . $_CONFIG["ref_lang"];
                    if(trim($banner->get_var("imagem_bg_" . $_CONFIG["ref_lang"])) == "") { $bg_field = "imagem_bg_pt";}
                    $it_img = stripslashes(get_output($banner->get_var($bg_field)));
                    $bg_field_mob = "imagem_bg_mobile_" . $_CONFIG["ref_lang"];
                    if(trim($banner->get_var("imagem_bg_mobile_" . $_CONFIG["ref_lang"])) == "") { $bg_field = "imagem_bg_mobile_pt";}
                    $it_img_mob = stripslashes(get_output($banner->get_var($bg_field_mob)));
                    if(isMobile() && trim($it_img_mob) != "") { $it_img = $it_img_mob; $bg_field = $bg_field_mob; }

                    if(trim($it_cin) != "") {
                        $it_img = $it_cin;
                        $it_folder = ROOT_SERVER . ROOT . $banner->get_upload_folder("cinemagraph");
                    } else {
                        $it_folder = ROOT_SERVER . ROOT . $banner->get_upload_folder($bg_field);
                    }

                    $it_url = normaliza_url_vars($it_url);

                    ?>
                    <div class="carousel-item h-100 lazy bgParallax <?php if($iCount == 0) {echo "active";} ?>" data-speed=".5" data-src="<?php echo $it_folder . $it_img; ?>" >
                        <div class="overlay-shadow"></div>
                        <div class="carousel-caption d-block"><?php // d-none d-md-block ?>
                            <h5><?php echo $it_tit; ?></h5>
                            <p class="lead"><?php echo $it_descr; ?></p>
                        </div>
                        <?php 
                        if(trim($it_url) != "") {
                            ?><a class="linkFullDiv" href="<?php echo $it_url; ?>" target="<?php echo $it_tgt; ?>" title="<?php echo $it_tit; ?>"></a><?php
                        }
                        ?>
                    </div>
                    <?php
                    $strNav .= "<li data-target=\"#banner-home\" data-slide-to=\"" . $iCount . "\" class=\"" . ($iCount==0?"active":"") . "\"></li>";
                    $iCount++;
                }
                ?>
    
                <div class="carousel-control">
                    <?php /*<ol class="carousel-indicators">
                        <?php echo $strNav; ?>
                    </ol>*/ ?>
                    <a class="carousel-control-prev" href="#banner-home" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only"><?php echo get_lang("_LIGHTBOX_ANT"); ?></span>
                    </a>
                    <a class="carousel-control-next" href="#banner-home" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only"><?php echo get_lang("_LIGHTBOX_PROX"); ?></span>
                    </a>

                    <div class="scrollable"></div>
                </div>
            </div>
        </div>
    </section>
    <?php
} 
?>