<?php

include_once("config.php"); 

$_ref_page = "produtos";

$pages = new conteudos();
$pages = $pages->localiza(array("nome_arquivo","id_idioma"),array($_ref_page,$_CONFIG["id_lang"]));
if(!is_array($pages) || sizeof($pages) == 0) {
    $pages = new conteudos();
    $pages = $pages->localiza(array("nome_arquivo","id_idioma"),array($_ref_page,1));
}
if(!is_array($pages) || sizeof($pages) == 0) {
    $pages = new conteudos();
    $pages = $pages->localiza(array("nome_arquivo","id_idioma"),array($_ref_page,99));
}

if(is_array($pages))
{
	$page = $pages[0];
}

if($page <> null)
{
	$descr = $page->get_var("texto");
	$titulo = stripslashes(get_output($page->get_var("titulo")));
	$descrRes = stripslashes(get_output($page->get_var("meta_descricao")));
	$keywords = stripslashes(get_output($page->get_var("meta_palavras_chave")));

	$_TITULO_PAGINA = strip_tags($titulo);
	$_DESCRICAO_PAGINA = strip_tags($descrRes);
	$_PALAVRAS_CHAVE_PAGINA = strip_tags($keywords);
}

include_once("header.php"); 

if(intval($_REQUEST["id"]) > 0) {
  carrega_classe("produtos");
  carrega_classe("produtos_categorias");

  $item = new produtos();
  $item->inicia_dados();
  $item->set_var("id",intval($_REQUEST["id"]));
  $item->carrega_dados();

  if($item != null) {
      //$it_tit = stripslashes(get_output($produto->get_var("titulo_" . $_CONFIG["ref_lang"])));
      //if(trim($it_tit) == "") {$it_tit = stripslashes(get_output($produto->get_var("titulo_pt")));}
      //$it_titurl = gera_titulo_amigavel(get_output($produto->get_var("titulo_pt")));
      $it_tit = stripslashes(get_output($produto->get_var("titulo")));
      $it_titurl = gera_titulo_amigavel(get_output($produto->get_var("titulo")));
      $it_id = intval($produto->get_var("id"));
      $it_descr = stripslashes(get_output($produto->get_var("chamada_" . $_CONFIG["ref_lang"])));
      if(trim($it_descr) == "") {$it_descr = stripslashes(get_output($produto->get_var("chamada_pt")));}
      $it_bg = stripslashes(get_output($produto->get_var("imagem")));
      
      $it_img = $it_bg;
      $it_folder = ROOT_SERVER . ROOT . $produto->get_upload_folder("imagem");
      $cats = stripslashes(get_output($item->get_var("produtos_categorias")));          
      
      $it_categorias = "";
      $cats = explode(";",$cats);
      foreach($cats as $cid) {
        $cat = new produtos_categorias();
        $cat->inicia_dados();
        $cat->set_var("id",intval($cid));
        $cat->carrega_dados();

        if($cat != null) {
          $cat_id = intval($cat->get_var("id"));
          $cat_tit = stripslashes(get_output($cat->get_var("titulo_" . $_CONFIG["ref_lang"])));
          $cat_titurl = gera_titulo_amigavel(get_output($cat->get_var("titulo_pt")));
          if(trim($it_categorias) != "") {
            $it_categorias .= " <span class=\"mx-2\">&bullet;</span> ";  
          }
          $it_categorias .= " <a href=\"" . ROOT_SERVER . ROOT . "produtos/categoria/" . $cat_titurl . "/" . $cat_id . "\">" . $cat_tit . "</a>";
        }
      }

      $shareURL_FB = ROOT_SERVER . ROOT . $idPaginaAux . "/" . $_REQUEST["titulo"] . "/" . $_REQUEST["id"];
      
      $_TITULO_PAGINA = $item_tit;
      $_DESCRICAO_PAGINA = strip_tags($item_descr);
      //$_PALAVRAS_CHAVE_PAGINA = $item_tags;
      if(trim($item_img) != "") {
          $_IMG_SHARE = ROOT_SERVER . ROOT . UPLOAD_FOLDER . $item_img;
      }
  }
}

?>

<div class="site-blocks-cover blog-cover overlay lazy" data-src="<?php echo $it_folder . $it_img; ?>" data-aos="fade">
  <div class="overlay-shadow"></div>
  <div class="container">
    <div class="row align-items-center justify-content-center">

      
          <div class="col-md-6 mt-lg-5 text-center">
            <h1><?php echo $it_tit; ?></h1>
            <p class="post-meta"><?php echo converte_data($it_data,3); ?> <span class="mx-2">&bullet;</span> <?php if(trim($it_aut) != "") { echo $it_aut; ?> <span class="mx-2">&bullet;</span> <?php } ?> <?php echo $it_categorias; ?></p>
            
          </div>
        
    </div>
  </div>
</div>  



<section class="site-section">
  <div class="container">
    <div class="row">
      <div class="col-md-8 blog-content">
        <div class="lead"><?php echo $it_res; ?></div>
        <div class="pt-5">
          <?php echo $it_descr; ?>
        </div>


        <?php
        if(intval($it_vis) == 1) { ?>
          <div class="pt-5 w-100">
            <div class="fb-comments" data-href="<?php echo $shareURL_FB; ?>" data-width="100%" data-numposts="10"></div>
          </div>
        <?php } ?>

      </div>
      <div class="col-md-4 sidebar">
        <div class="sidebar-box gray-box">
          <form action="<?php echo ROOT_SERVER . ROOT; ?>posts/" class="search-form" method="GET">
            <div class="form-group">
              <span class="icon fa fa-search"></span>
              <input type="text" name="busca" class="form-control" placeholder="<?php echo get_lang("_BUSCA"); ?>">
            </div>
          </form>
        </div>
        <?php if(trim($it_categorias) != "") { ?>
        <div class="sidebar-box">
          <div class="categories">
            <h3><?php echo get_lang("_CATEGORIAS"); ?></h3>
            <?php echo $it_categorias; ?>
          </div>
        </div>
        <?php } ?>
        <?php
        if(trim($it_autor) != "") {
          ?>
          <div class="sidebar-box">
            <h3><?php echo get_lang("_POSTPOR"); ?><?php echo $it_autor; ?></h3>
          </div>
          <?php
        }
        ?>

        <?php /*<div class="sidebar-box">
          <h3>Paragraph</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus itaque, autem necessitatibus voluptate quod mollitia delectus aut, sunt placeat nam vero culpa sapiente consectetur similique, inventore eos fugit cupiditate numquam!</p>
        </div>*/ ?>
      </div>
    </div>
  </div>
</section>

<?php

include_once("section-contato.php");

include_once("footer.php");

?>