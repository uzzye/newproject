<?php

$lang_site["_LIGHTBOX_ERRO"] = "No se puede cargar el contenido.<br/>Inténtelo de nuevo más tarde.";
$lang_site["_LIGHTBOX_FECHAR"] = "Cerrar";
$lang_site["_LIGHTBOX_PROX"] = "Siguiente";
$lang_site["_LIGHTBOX_ANT"] = "Anterior";

$lang_site["_SOBRE"] = "Acerca";
$lang_site["_CASES"] = "Cases";
$lang_site["_CONTATO"] = "Contacto";
$lang_site["_FALE_CONOSCO"] = "Fale Conosco";
$lang_site["_BLOG"] = "Blog";
$lang_site["_AREA_RESTRITA"] = "Área restringida";
$lang_site["_HOME"] = "Hogar";
$lang_site["_PRODUTOS"] = "Productos";
$lang_site["_POSTS"] = "Posts";
$lang_site["_NOTICIAS"] = "Noticias";
$lang_site["_REPRESENTANTES"] = "Representantes";
$lang_site["_BUSCA"] = "Buscar";
$lang_site["_POSTPOR"] = "Por";

$lang_site["_SELECIONE"] = "Seleccione";
$lang_site["_ENVIAR"] = "Enviar";
$lang_site["_AGUARDE"] = "Espere...";
$lang_site["_ENVIAR_MENSAGEM"] = "Enviar mensaje";
$lang_site["_ENVIAR_CADASTRO"] = "Enviar registro";
$lang_site["_NOME"] = "Nombre";
$lang_site["_CPFCNPJ"] = "SSN";
$lang_site["_ENDERECO"] = "Habla a";
$lang_site["_CIDADE"] = "Ciudad";
$lang_site["_ESTADO"] = "Estado";
$lang_site["_TELEFONE"] = "Teléfono";
$lang_site["_EMAIL"] = "Email";
$lang_site["_OBSERVACAO"] = "Notas";
$lang_site["_VER_TODOS"] = "Ver todo";
$lang_site["_VER_MAIS_NOTICIAS"] = "Ver más noticias";
$lang_site["_VER_MAIS"] = "Ver más";
$lang_site["_BUSCAR"] = "Buscar";
$lang_site["_IDIOMAS"] = "Idiomas";
$lang_site["_RECEBA_NOVIDADES"] = "Reciba nuestro boletín";
$lang_site["_CADASTRE_SE_RECEBA_NOVIDADES"] = "Regístrese para recibir nuestro Newsletter";
$lang_site["_CADASTRAR"] = "Registrarse";
$lang_site["_SOLICITAR_ORCAMENTO"] = "Obtenga una cotización";
$lang_site["_ORCAMENTO"] = "Cotización";
$lang_site["_FOLLOW_INSTA"] = "SÍGUENOS EN INSTAGRAM";
$lang_site["_FOLLOW_FACEBOOK"] = "DANOS LIKE EN FACEBOOK";
$lang_site["_LOCALIZAR_REPRESENTANTES"] = "Busque un representante";
$lang_site["_SELECIONE_ESTADO"] = "Selecciona un Estado";
$lang_site["_NENHUM_REGISTRO_ENCONTRADO"] = "No se encontraron registros.";
$lang_site["_OUTRAS_NOTICIAS"] = "Mas noticias";
$lang_site["_BUSCAR_PRODUTOS"] = "Buscar Productos...";
$lang_site["_INFORMACOES_TECNICAS"] = "Información técnica";
$lang_site["_BAIXAR_CATALOGO"] = "Descargar Catalogo";
$lang_site["_GALERIA_IMAGENS"] = "galería de imágenes";
$lang_site["_DESENHO_TECNICO"] = "Imagen técnica";
$lang_site["_CATALOGOS"] = "Catálogos";
$lang_site["_CATALOGO"] = "Catalogar";
$lang_site["_BAIXAR"] = "Descargar";
$lang_site["_OBRIGATORIO_PREENCHER"] = "Llenado obligatorio";
$lang_site["_LINKS_RAPIDOS"] = "Enlaces rápidos";
$lang_site["_INSCREVA_SE"] = "Registrarse";
$lang_site["_SIGA_NOS"] = "Síguenos";

$lang_site["_FORM_RETURN_ERRO_1"]  = "Relleno de formulario no válido";
$lang_site["_FORM_RETURN_ERRO_2"]  = "¡Todos los campos son obligatorios!";
$lang_site["_FORM_RETURN_ERRO_3"]  = "¡Los campos con * son obligatorios!";
$lang_site["_FORM_RETURN_ERRO_4"]  = "Dirección de correo electrónico inválida";
$lang_site["_FORM_RETURN_ERRO_5"]  = "¡Formato de archivo inválido!";
$lang_site["_FORM_RETURN_ERRO_6"]  = "¡El usuario y las contraseñas son obligatorios!";
$lang_site["_FORM_RETURN_ERRO_7"]  = "¡Usuario y contraseña inválidos!";
$lang_site["_FORM_RETURN_ERRO_8"]  = "¡Usuario no encontrado o desactivado! Por favor contáctenos.";
$lang_site["_FORM_RETURN_ERRO_9"]  = "¡Las contraseñas no coinciden!";
$lang_site["_FORM_RETURN_ERRO_10"] = "¡La contraseña actual no es válida!";
$lang_site["_FORM_RETURN_ERRO_11"] = "Este correo electrónico ya fue tomado, por favor use otro.";

$lang_site["_FORM_RETURN_OK_1"] = "¡Registro enviado con éxito!";
$lang_site["_FORM_RETURN_OK_2"] = "¡Mensaje enviado con éxito!";
$lang_site["_FORM_RETURN_OK_3"] = "Autenticando, espera.";


/*
// SE NAO TEM O IDIOMA ATUAL NO CMS

function get_lang($index = "") {
	global $lang_site, $lang_cms;

	if(!is_array($lang_cms)) {
		$lang_cms = array();
	}

	$lang_cms = array_merge($lang_cms, $lang_site);

	if(!array_key_exists($index, $lang_cms)){
		return $index;
	} else {
		if(trim($index) == "") {
			return $lang_cms;
		} else {
			return $lang_cms[$index];
		}
	}
}*/

?>