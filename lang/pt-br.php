<?php

$lang_site["_LIGHTBOX_ERRO"] = "Não foi possível carregar o conteúdo.<br/>Tente novamente mais tarde.";
$lang_site["_LIGHTBOX_FECHAR"] = "Fechar";
$lang_site["_LIGHTBOX_PROX"] = "Próxima";
$lang_site["_LIGHTBOX_ANT"] = "Anterior";

$lang_site["_SOBRE"] = "Sobre";
$lang_site["_CASES"] = "Cases";
$lang_site["_CONTATO"] = "Contato";
$lang_site["_FALE_CONOSCO"] = "Fale Conosco";
$lang_site["_BLOG"] = "Blog";
$lang_site["_AREA_RESTRITA"] = "Área Restrita";
$lang_site["_HOME"] = "Home";
$lang_site["_PRODUTOS"] = "Produtos";
$lang_site["_POSTS"] = "Posts";
$lang_site["_NOTICIAS"] = "Notícias";
$lang_site["_REPRESENTANTES"] = "Representantes";
$lang_site["_BUSCA"] = "Busca";
$lang_site["_POSTPOR"] = "Por";

$lang_site["_SELECIONE"] = "Selecione";
$lang_site["_ENVIAR"] = "Enviar";
$lang_site["_AGUARDE"] = "Aguarde...";
$lang_site["_ENVIAR_MENSAGEM"] = "Enviar Mensagem";
$lang_site["_ENVIAR_CADASTRO"] = "Enviar Cadastro";
$lang_site["_NOME"] = "Nome";
$lang_site["_CPFCNPJ"] = "CPF/CNPJ";
$lang_site["_ENDERECO"] = "Endereço";
$lang_site["_CIDADE"] = "Cidade";
$lang_site["_ESTADO"] = "Estado";
$lang_site["_TELEFONE"] = "Telefone";
$lang_site["_EMAIL"] = "Email";
$lang_site["_OBSERVACAO"] = "Observação";
$lang_site["_VER_TODOS"] = "Ver Todos";
$lang_site["_VER_MAIS_NOTICIAS"] = "Ver Mais Notícias";
$lang_site["_VER_MAIS"] = "Ver Mais";
$lang_site["_BUSCAR"] = "Buscar";
$lang_site["_IDIOMAS"] = "Idiomas";
$lang_site["_RECEBA_NOVIDADES"] = "Receba Nossas Novidades";
$lang_site["_CADASTRE_SE_RECEBA_NOVIDADES"] = "Cadastre-se para receber nossa Newsletter";
$lang_site["_CADASTRAR"] = "Cadastrar";
$lang_site["_SOLICITAR_ORCAMENTO"] = "Solicitar Orçamento";
$lang_site["_ORCAMENTO"] = "Orçamento";
$lang_site["_FOLLOW_INSTA"] = "SIGA-NOS NO INSTAGRAM";
$lang_site["_FOLLOW_FACEBOOK"] = "CURTA-NOS NO FACEBOOK";
$lang_site["_LOCALIZAR_REPRESENTANTES"] = "Encontre um Representante";
$lang_site["_SELECIONE_ESTADO"] = "Selecione um Estado";
$lang_site["_NENHUM_REGISTRO_ENCONTRADO"] = "Nenhum registro encontrado.";
$lang_site["_OUTRAS_NOTICIAS"] = "Mais Notícias";
$lang_site["_BUSCAR_PRODUTOS"] = "Buscar produtos...";
$lang_site["_INFORMACOES_TECNICAS"] = "Informações Técnicas";
$lang_site["_BAIXAR_CATALOGO"] = "Baixar Catálogo";
$lang_site["_GALERIA_IMAGENS"] = "Galeria de Imagens";
$lang_site["_DESENHO_TECNICO"] = "Desenho Técnico";
$lang_site["_CATALOGOS"] = "Catálogos";
$lang_site["_CATALOGO"] = "Catálogo";
$lang_site["_BAIXAR"] = "Baixar";
$lang_site["_OBRIGATORIO_PREENCHER"] = "Obrigatório preencher";
$lang_site["_LINKS_RAPIDOS"] = "Links Rápidos";
$lang_site["_INSCREVA_SE"] = "Increva-se";
$lang_site["_SIGA_NOS"] = "Siga-nos";

$lang_site["_FORM_RETURN_ERRO_1"]  = "Preenchimento de formulário inválido!";
$lang_site["_FORM_RETURN_ERRO_2"]  = "Todos os campos são obrigatórios!";
$lang_site["_FORM_RETURN_ERRO_3"]  = "Campos marcados com * são obrigatórios!";
$lang_site["_FORM_RETURN_ERRO_4"]  = "Endereço de e-mail inválido!";
$lang_site["_FORM_RETURN_ERRO_5"]  = "Formato de arquivo inválido!";
$lang_site["_FORM_RETURN_ERRO_6"]  = "Usuário e senha são obrigatórios!";
$lang_site["_FORM_RETURN_ERRO_7"]  = "Usuário e senha inválidos!";
$lang_site["_FORM_RETURN_ERRO_8"]  = "Usuário não encontrado ou desativado! Por favor, realize um novo cadastro.";
$lang_site["_FORM_RETURN_ERRO_9"]  = "As senhas digitadas não conferem!";
$lang_site["_FORM_RETURN_ERRO_10"] = "Sua senha atual é inválida!";
$lang_site["_FORM_RETURN_ERRO_11"] = "Este email já está em uso, por favor informe outro!";

$lang_site["_FORM_RETURN_OK_1"] = "Cadastro enviado com sucesso!";
$lang_site["_FORM_RETURN_OK_2"] = "Mensagem enviada com sucesso!";
$lang_site["_FORM_RETURN_OK_3"] = "Autenticando, aguarde...";


/*
// SE NAO TEM O IDIOMA ATUAL NO CMS

function get_lang($index = "") {
	global $lang_site, $lang_cms;

	if(!is_array($lang_cms)) {
		$lang_cms = array();
	}

	$lang_cms = array_merge($lang_cms, $lang_site);

	if(!array_key_exists($index, $lang_cms)){
		return $index;
	} else {
		if(trim($index) == "") {
			return $lang_cms;
		} else {
			return $lang_cms[$index];
		}
	}
}*/

?>