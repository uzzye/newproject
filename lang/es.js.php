<script language="javascript">
	var dropify_messages = {
        'default': 'Arrastre y suelte un archivo aquí o haga clic en',
        'replace': 'Arrastra y suelta o haz clic para reemplazar',
        'remove':  'Eliminar',
        'error':   'Vaya, sucedió algo mal.'
    };
    /* var dropify_messages_work = dropify_messages;
     dropify_messages_work['default'] = 'Drop here your resume or click';
     dropify_messages_work['replace'] = 'Drop here the resume to change it or click'; */
	var dropify_error = {
        'fileSize':    'El tamaño del archivo es demasiado grande ({{ value }} máx.).',
        'minWidth':    'El ancho de la imagen es demasiado pequeño ({{ value }}} px min).',
        'maxWidth':    'El ancho de la imagen es demasiado grande ({{ value }}} px como máximo).',
        'minHeight':   'La altura de la imagen es demasiado pequeña ({{ value }}} px min).',
        'maxHeight':   'La altura de la imagen es demasiado grande ({{ value }} px como máximo).',
        'imageFormat': 'El formato de imagen no está permitido (solo {{ value }}).'
    };
    var formError = [];
    formError[1] = "<?php echo $lang_site["_FORM_RETURN_ERRO_1"]; ?>";
    formError[2] = "<?php echo $lang_site["_FORM_RETURN_ERRO_2"]; ?>";
    formError[3] = "<?php echo $lang_site["_FORM_RETURN_ERRO_3"]; ?>";
    formError[4] = "<?php echo $lang_site["_FORM_RETURN_ERRO_4"]; ?>";
    formError[5] = "<?php echo $lang_site["_FORM_RETURN_ERRO_5"]; ?>";
    formError[6] = "<?php echo $lang_site["_FORM_RETURN_ERRO_6"]; ?>";
    formError[7] = "<?php echo $lang_site["_FORM_RETURN_ERRO_7"]; ?>";
    formError[8] = "<?php echo $lang_site["_FORM_RETURN_ERRO_8"]; ?>";
    formError[9] = "<?php echo $lang_site["_FORM_RETURN_ERRO_9"]; ?>";
    formError[10] = "<?php echo $lang_site["_FORM_RETURN_ERRO_10"]; ?>";
    formError[11] = "<?php echo $lang_site["_FORM_RETURN_ERRO_11"]; ?>";
</script>