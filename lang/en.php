<?php

$lang_site["_LIGHTBOX_ERRO"] = "Cannot load the content.<br/>Try again later.";
$lang_site["_LIGHTBOX_FECHAR"] = "Close";
$lang_site["_LIGHTBOX_PROX"] = "Next";
$lang_site["_LIGHTBOX_ANT"] = "Previous";

$lang_site["_SOBRE"] = "About";
$lang_site["_CASES"] = "Cases";
$lang_site["_CONTATO"] = "Contact";
$lang_site["_FALE_CONOSCO"] = "Talk to Us";
$lang_site["_BLOG"] = "Blog";
$lang_site["_AREA_RESTRITA"] = "Restricted Area";
$lang_site["_HOME"] = "Home";
$lang_site["_PRODUTOS"] = "Products";
$lang_site["_POSTS"] = "Posts";
$lang_site["_NOTICIAS"] = "News";
$lang_site["_REPRESENTANTES"] = "Representatives";
$lang_site["_BUSCA"] = "Search";
$lang_site["_POSTPOR"] = "By";

$lang_site["_SELECIONE"] = "Select";
$lang_site["_ENVIAR"] = "Send";
$lang_site["_AGUARDE"] = "Wait...";
$lang_site["_ENVIAR_MENSAGEM"] = "Send Message";
$lang_site["_ENVIAR_CADASTRO"] = "Send Registration";
$lang_site["_NOME"] = "Name";
$lang_site["_CPFCNPJ"] = "SSN";
$lang_site["_ENDERECO"] = "Address";
$lang_site["_CIDADE"] = "City";
$lang_site["_ESTADO"] = "State";
$lang_site["_TELEFONE"] = "Phone";
$lang_site["_EMAIL"] = "Email";
$lang_site["_OBSERVACAO"] = "Notes";
$lang_site["_VER_TODOS"] = "Seel All";
$lang_site["_VER_MAIS_NOTICIAS"] = "See More News";
$lang_site["_VER_MAIS"] = "See More";
$lang_site["_BUSCAR"] = "Search";
$lang_site["_IDIOMAS"] = "Languages";
$lang_site["_RECEBA_NOVIDADES"] = "Receive Our Newsletter";
$lang_site["_CADASTRE_SE_RECEBA_NOVIDADES"] = "Register to receive our Newsletter";
$lang_site["_CADASTRAR"] = "Register";
$lang_site["_SOLICITAR_ORCAMENTO"] = "Get a quotation";
$lang_site["_ORCAMENTO"] = "Quotation";
$lang_site["_FOLLOW_INSTA"] = "FOLLOW US ON INSTAGRAM";
$lang_site["_FOLLOW_FACEBOOK"] = "LIKE US ON FACEBOOK";
$lang_site["_LOCALIZAR_REPRESENTANTES"] = "Locate a Represenatative";
$lang_site["_SELECIONE_ESTADO"] = "Select a State";
$lang_site["_NENHUM_REGISTRO_ENCONTRADO"] = "No records found.";
$lang_site["_OUTRAS_NOTICIAS"] = "More News";
$lang_site["_BUSCAR_PRODUTOS"] = "Search products...";
$lang_site["_INFORMACOES_TECNICAS"] = "Technical Infos";
$lang_site["_BAIXAR_CATALOGO"] = "Download Catalog";
$lang_site["_GALERIA_IMAGENS"] = "Image Gallery";
$lang_site["_DESENHO_TECNICO"] = "Technical Image";
$lang_site["_CATALOGOS"] = "Catalogs";
$lang_site["_CATALOGO"] = "Catalog";
$lang_site["_BAIXAR"] = "Download";
$lang_site["_OBRIGATORIO_PREENCHER"] = "Mandatory filling";
$lang_site["_LINKS_RAPIDOS"] = "Quick Links";
$lang_site["_INSCREVA_SE"] = "Sign up";
$lang_site["_SIGA_NOS"] = "Follow us";

$lang_site["_FORM_RETURN_ERRO_1"]  = "Invalid form filling!";
$lang_site["_FORM_RETURN_ERRO_2"]  = "All fields are mandatory!";
$lang_site["_FORM_RETURN_ERRO_3"]  = "Fields with * are mandatory!";
$lang_site["_FORM_RETURN_ERRO_4"]  = "Invalid email address!";
$lang_site["_FORM_RETURN_ERRO_5"]  = "Invalid file format!";
$lang_site["_FORM_RETURN_ERRO_6"]  = "User and passwords are mandatory!";
$lang_site["_FORM_RETURN_ERRO_7"]  = "Invalid user and password!";
$lang_site["_FORM_RETURN_ERRO_8"]  = "User not found or deactivated! Please, contact us.";
$lang_site["_FORM_RETURN_ERRO_9"]  = "The passwords don't match!";
$lang_site["_FORM_RETURN_ERRO_10"] = "Current password is invalid!";
$lang_site["_FORM_RETURN_ERRO_11"] = "This email was already taken, please use another!";

$lang_site["_FORM_RETURN_OK_1"] = "Registration sent successfully!";
$lang_site["_FORM_RETURN_OK_2"] = "Message sent successfully!";
$lang_site["_FORM_RETURN_OK_3"] = "Authenticating, wait...";


/*
// SE NAO TEM O IDIOMA ATUAL NO CMS

function get_lang($index = "") {
	global $lang_site, $lang_cms;

	if(!is_array($lang_cms)) {
		$lang_cms = array();
	}

	$lang_cms = array_merge($lang_cms, $lang_site);

	if(!array_key_exists($index, $lang_cms)){
		return $index;
	} else {
		if(trim($index) == "") {
			return $lang_cms;
		} else {
			return $lang_cms[$index];
		}
	}
}*/

?>