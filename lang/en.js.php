<script language="javascript">
	var dropify_messages = {
        'default': 'Drag and drop a file here or click',
        'replace': 'Drag and drop or click to replace',
        'remove':  'Remove',
        'error':   'Ooops, something wrong happened.'
    };
    /* var dropify_messages_work = dropify_messages;
     dropify_messages_work['default'] = 'Drop here your resume or click';
     dropify_messages_work['replace'] = 'Drop here the resume to change it or click'; */
	var dropify_error = {
        'fileSize': 'The file size is too big ({{ value }} max).',
        'minWidth': 'The image width is too small ({{ value }}}px min).',
        'maxWidth': 'The image width is too big ({{ value }}}px max).',
        'minHeight': 'The image height is too small ({{ value }}}px min).',
        'maxHeight': 'The image height is too big ({{ value }}px max).',
        'imageFormat': 'The image format is not allowed ({{ value }} only).'
    };
    var formError = [];
    formError[1] = "<?php echo $lang_site["_FORM_RETURN_ERRO_1"]; ?>";
    formError[2] = "<?php echo $lang_site["_FORM_RETURN_ERRO_2"]; ?>";
    formError[3] = "<?php echo $lang_site["_FORM_RETURN_ERRO_3"]; ?>";
    formError[4] = "<?php echo $lang_site["_FORM_RETURN_ERRO_4"]; ?>";
    formError[5] = "<?php echo $lang_site["_FORM_RETURN_ERRO_5"]; ?>";
    formError[6] = "<?php echo $lang_site["_FORM_RETURN_ERRO_6"]; ?>";
    formError[7] = "<?php echo $lang_site["_FORM_RETURN_ERRO_7"]; ?>";
    formError[8] = "<?php echo $lang_site["_FORM_RETURN_ERRO_8"]; ?>";
    formError[9] = "<?php echo $lang_site["_FORM_RETURN_ERRO_9"]; ?>";
    formError[10] = "<?php echo $lang_site["_FORM_RETURN_ERRO_10"]; ?>";
    formError[11] = "<?php echo $lang_site["_FORM_RETURN_ERRO_11"]; ?>";
</script>