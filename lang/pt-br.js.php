<script language="javascript">
	var dropify_messages = {
        'default': 'Arraste aqui seu arquivo ou clique',
        'replace': 'Arraste aqui o arquivo para substituir ou clique',
        'remove':  'Remover',
        'error':   'Ooops, algo deu errado.'
    };
    /* var dropify_messages_work = dropify_messages;
     dropify_messages_work['default'] = 'Arraste aqui seu currículo ou clique';
     dropify_messages_work['replace'] = 'Arraste aqui o currículo para substituir ou clique'; */
	var dropify_error = {
        'fileSize': 'O tamanho do arquivo é muito grande ({{ value }} máx).',
        'minWidth': 'A largura da imagem é muito pequena ({{ value }}}px mín).',
        'maxWidth': 'A largura da imagem é muito grande ({{ value }}}px máx).',
        'minHeight': 'A altura da imagem é muito pequena ({{ value }}}px mín).',
        'maxHeight': 'A altura da imagem é muito grande ({{ value }}px máx).',
        'imageFormat': 'O formato da imagem não é suportado ({{ value }} apenas).'
    };
    var formError = [];
    formError[1] = "<?php echo $lang_site["_FORM_RETURN_ERRO_1"]; ?>";
    formError[2] = "<?php echo $lang_site["_FORM_RETURN_ERRO_2"]; ?>";
    formError[3] = "<?php echo $lang_site["_FORM_RETURN_ERRO_3"]; ?>";
    formError[4] = "<?php echo $lang_site["_FORM_RETURN_ERRO_4"]; ?>";
    formError[5] = "<?php echo $lang_site["_FORM_RETURN_ERRO_5"]; ?>";
    formError[6] = "<?php echo $lang_site["_FORM_RETURN_ERRO_6"]; ?>";
    formError[7] = "<?php echo $lang_site["_FORM_RETURN_ERRO_7"]; ?>";
    formError[8] = "<?php echo $lang_site["_FORM_RETURN_ERRO_8"]; ?>";
    formError[9] = "<?php echo $lang_site["_FORM_RETURN_ERRO_9"]; ?>";
    formError[10] = "<?php echo $lang_site["_FORM_RETURN_ERRO_10"]; ?>";
    formError[11] = "<?php echo $lang_site["_FORM_RETURN_ERRO_11"]; ?>";
</script>