			</main><!-- /.container -->
			<footer class="site-footer">
				<div class="container">
					<div class="row">
						<div class="col-12 col-md-8">
							<div class="row">
								<div class="col-md-5">
									<h2 class="footer-heading mb-4"><?php echo $arrayConteudos["sobre"]["titulo"]; ?></h2>
									<p><?php echo $arrayConteudos["sobre"]["descricao"]; ?></p>
									<div class="mt-4">
									<?php
									if($_CONFIG["lang"] != "en") {
										?><a href="<?php echo ROOT_SERVER . ROOT; ?>lang/en" title="English Version">English Version</a><br/><?php
									}
									if($_CONFIG["lang"] != "es") {
										?><a href="<?php echo ROOT_SERVER . ROOT; ?>lang/es" title="Versión en Español">Versión en Español</a><br/><?php
									} 
									if($_CONFIG["lang"] != "pt-br") {
										?><a href="<?php echo ROOT_SERVER . ROOT; ?>lang/pt-br" title="Versão em Português">Versão em Português</a><br/><?php
									}						
									?>
									</div>
								</div>
								<div class="col-md-5 ml-auto mt-4 mt-md-0">
									<h2 class="footer-heading mb-4"><?php echo get_lang("_LINKS_RAPIDOS"); ?></h2>
									<ul class="list-unstyled">
									<li><a href="<?php if($idPaginaAux != "home") { echo ROOT_SERVER . ROOT; } ?>#sobre"><?php echo get_lang("_SOBRE"); ?></a></li>
									<li><a href="<?php if($idPaginaAux != "home") { echo ROOT_SERVER . ROOT; } ?>#posts"><?php echo get_lang("_POSTS"); ?></a></li>
									<li><a href="<?php if($idPaginaAux != "home") { echo ROOT_SERVER . ROOT; } ?>#contato"><?php echo get_lang("_CONTATO"); ?></a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-12 col-md-4 mt-4 mt-md-0">
							<div class="mb-4">
								<h2 class="footer-heading mb-4"><?php echo get_lang("_INSCREVA_SE"); ?></h2>
								<form id="form-news" method="post" action="javascript:return false;" data-url="<?php echo ROOT_SERVER . ROOT; ?>ajax/news/enviar" class="footer-subscribe formPadrao">
									<input type="hidden" name="acao" id="acao_news" value="enviar" />
									<input type="hidden" name="verifier" id="verifier_news" value="" />

									<div class="input-group mb-3">
										<input type="text" class="form-control border-secondary text-white bg-transparent" placeholder="<?php echo get_lang("_EMAIL"); ?>" aria-label="<?php echo get_lang("_EMAIL"); ?>" aria-describedby="button-addon2" name="email">
										<div class="input-group-append">
										<button class="btn btn-primary text-black btnEnviar" type="submit" id="button-addon2" value="&nbsp;" data-label-send="<?php echo get_lang("_ENVIAR"); ?>" data-label-wait="<?php echo get_lang("_AGUARDE"); ?>"><span><?php echo get_lang("_ENVIAR"); ?></span></button>
										</div>
									</div>

									<div class="row form-group">
										<div class="col-md-12 retornoPost">
										</div>
									</div>
								</form>  
							</div>
							
							<div class="">
								<h2 class="footer-heading mb-4"><?php echo get_lang("_SIGA_NOS"); ?></h2>
								<a href="<?php echo $araryLinks["facebook"]["url"]; ?>" target="<?php echo $araryLinks["facebook"]["url"]; ?>" class="pl-0 pr-3"><span class="icon-facebook"></span></a>
								<a href="<?php echo $araryLinks["twitter"]["url"]; ?>" target="<?php echo $araryLinks["twitter"]["url"]; ?>" class="pl-3 pr-3"><span class="icon-twitter"></span></a>
								<a href="<?php echo $araryLinks["instagram"]["url"]; ?>" target="<?php echo $araryLinks["instagram"]["url"]; ?>" class="pl-3 pr-3"><span class="icon-instagram"></span></a>
								<a href="<?php echo $araryLinks["linkedin"]["url"]; ?>" target="<?php echo $araryLinks["linkedin"]["url"]; ?>" class="pl-3 pr-3"><span class="icon-linkedin"></span></a>
							</div>
						</div>
					</div>
					<div class="row pt-5 mt-5 text-center">
						<div class="col-md-12">
							<div class="border-top pt-5">
							<p><?php echo $arrayConteudos["copyright"]["descricao"]; ?></p>
							<div class="sign">
								<div class="uzzye"><a href="https://www.uzzye.com" target="_blank" class="linkFull" title="Uzzye Com. Dig. Ltda."></a></div>
							</div>
						</div>
					</div>

				</div>
				<!-- /.container -->
			</footer>

			<?php include_once("_lgpd_confirm.php"); ?>

		</div> <!-- .site-wrap -->

		<!-- Fonts -->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/v4-shims.css">
		<?php if(intval($_REQUEST["replica"]) == 1) { ?>
		<link href="https://fonts.googleapis.com/css?family=Oswald:300,400,900" rel="stylesheet">
		<?php } else { ?>
		<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,900" rel="stylesheet">
		<?php } ?>
		<link rel="stylesheet" href="<?php echo ROOT_SERVER . ROOT; ?>fonts/icomoon/style.css">
		<link rel="stylesheet" href="<?php echo ROOT_SERVER . ROOT; ?>fonts/flaticon/font/flaticon.css">

		<!-- Custom CSS -->
		<link rel="stylesheet" href="<?php echo ROOT_SERVER . ROOT; ?>css/jquery-ui.css">
		<link rel="stylesheet" href="<?php echo ROOT_SERVER . ROOT; ?>css/owl.carousel.min.css">
		<link rel="stylesheet" href="<?php echo ROOT_SERVER . ROOT; ?>css/owl.theme.default.min.css">
		<link rel="stylesheet" href="<?php echo ROOT_SERVER . ROOT; ?>css/owl.theme.default.min.css">
		<link rel="stylesheet" href="<?php echo ROOT_SERVER . ROOT; ?>css/jquery.fancybox.min.css">
		<link rel="stylesheet" href="<?php echo ROOT_SERVER . ROOT; ?>css/aos.css">
		<link rel="stylesheet" href="<?php echo ROOT_SERVER . ROOT; ?>css/lightgallery.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_SERVER . ROOT; ?>css/helpers.css" />

		<?php if(trim($_CONFIG["codigo_css"]) <> ""){?>
		<style type="text/css">
		<?php echo $_CONFIG["codigo_css"]; ?>
		</style>
		<?php } ?>
		<?php if(trim($_CONFIG["codigo_analytics"]) <> ""){
			echo $_CONFIG["codigo_analytics"];
		} ?>

		<?php if(trim($_CONFIG["codigo_javascript"]) <> ""){?>
		<script type="text/javascript" language="javascript">
		<?php echo $_CONFIG["codigo_javascript"]; ?>
		</script>
		<?php } ?>

		<script type="text/javascript" src="<?php echo ROOT_SERVER . ROOT; ?>js/frameworks/lazysizes.min.js"></script>

		<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

		<?php /*
		<!-- CSS -->
		<!--[if IE 7]>
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_SERVER . ROOT; ?>css/iehacks/ie7hacks.css" />
		<![endif]-->
		<!--[if IE]>
		<link rel="stylesheet" type="text/css" href="<?php echo ROOT_SERVER . ROOT; ?>css/iehacks/iehacks.css" />
		<style type="text/css">
			body { behavior:url(<?php echo ROOT_SERVER . ROOT; ?>css/iehacks/csshover3.htc); }
		</style>
		<![endif]-->
		<!--[if lt IE 9]><script async src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
		*/ ?>

		<script src="https://maps.google.com/maps/api/js?sensor=true&key=AIzaSyDLgXx-Xv5YfCPYrCACeZQ8qD8kT-ozqGw"></script>
		<?php /*<script type="text/javascript" src="<?php echo ROOT_SERVER . ROOT; ?>js/dist/defer.min.js"></script>*/ ?>

		<script type="text/javascript" src="<?php echo ROOT_SERVER . ROOT; ?>js/dist/uz.min.js"></script>
		<?php // Minify "main.js" for dist version and uncomment below - or - run "gulp" (dist version) to build it into uz.min.js ?>
		<?php /*<script type="text/javascript" src="<?php echo ROOT_SERVER . ROOT; ?>js/dist/main.min.js?<?php echo Date("Y-m-d"); ?>"></script>*/ ?>
		<script type="text/javascript" src="<?php echo ROOT_SERVER . ROOT; ?>js/dist/main.js?<?php echo Date("Y-m-d"); ?>"></script>
		
		<?php
		if(is_array($arrayScripts) && sizeof($arrayScripts) > 0) {
			for($w=0; $w<sizeof($arrayScripts); $w++) {
				?><script type="text/javascript" src="<?php echo $arrayScripts[$w]; ?>"></script><?php
			}
		}
		?>
		<script type="text/javascript">
			loaded();
		</script>

	</body>
</html>