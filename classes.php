<?php

include_once(ROOT_CMS . "classes/models/model.php");
include_once(ROOT_CMS . "classes/controllers/controller.php");
include_once(ROOT_CMS . "classes/views/view.php");

carrega_classe("cms_grupos_usuarios");
carrega_classe("cms_usuarios");
carrega_classe("cms_modulos");

//TODO CARREGAR CLASSES ESPECÍFICAS
carrega_classe("idiomas");
carrega_classe("configuracoes");
carrega_classe("links_uteis");
carrega_classe("conteudos");
carrega_classe("contatos");

?>