<?php

include_once("config.php"); 

$_ref_page = "noticias";

$pages = new conteudos();
$pages = $pages->localiza(array("nome_arquivo","id_idioma"),array($_ref_page,$_CONFIG["id_lang"]));
if(!is_array($pages) || sizeof($pages) == 0) {
    $pages = new conteudos();
    $pages = $pages->localiza(array("nome_arquivo","id_idioma"),array($_ref_page,1));
}
if(!is_array($pages) || sizeof($pages) == 0) {
    $pages = new conteudos();
    $pages = $pages->localiza(array("nome_arquivo","id_idioma"),array($_ref_page,99));
}

if(is_array($pages))
{
	$page = $pages[0];
}

if($page <> null)
{
	$descr = $page->get_var("texto");
	$titulo = stripslashes(get_output($page->get_var("titulo")));
	$descrRes = stripslashes(get_output($page->get_var("meta_descricao")));
	$keywords = stripslashes(get_output($page->get_var("meta_palavras_chave")));

	$_TITULO_PAGINA = strip_tags($titulo);
	$_DESCRICAO_PAGINA = strip_tags($descrRes);
	$_PALAVRAS_CHAVE_PAGINA = strip_tags($keywords);
}

include("header.php"); 

?>
<?php

//$arrayScripts[sizeof($arrayScripts)] = ROOT_SERVER . ROOT . "_js/dist/site.js";php

?>
<section class="site-section" id="posts" style="padding-top: 200px;">
  <div class="container">
    <div class="row mb-5">
      <div class="col-12 text-center">
        <h3 class="section-sub-title">Blog</h3>
        <h2 class="section-title mb-3"><?php echo get_lang("_NOTICIAS"); ?></h2>
      </div>
    </div>

    <?php

    carrega_classe("noticias");
    
    $sqlWhere = "(true)";

    if(intval($_REQUEST["id_cat"]) > 0) {
      $sqlWhere .= " AND LOCATE(\";" . intval($_REQUEST["id_cat"]) . ";\",CONCAT(\";\",tags,\";\")) ";
    }

    if(trim(rawurldecode($_REQUEST["busca"])) != "") {
      $busca = output_decode(trim(rawurldecode($_REQUEST["busca"])));
      $sqlWhere .= " AND (
        titulo_pt LIKE \"%" . $busca . "%\" OR
        titulo_en LIKE \"%" . $busca . "%\" OR
        titulo_es LIKE \"%" . $busca . "%\" OR
        descricao_pt LIKE \"%" . $busca . "%\" OR
        descricao_en LIKE \"%" . $busca . "%\" OR
        descricao_es LIKE \"%" . $busca . "%\"
      ) "; //tags LIKE \"%" . $busca . "%\"
    }

    $posts = new noticias();
    $posts = $posts->get_array_ativos($sqlWhere,"data_criacao DESC");

    if(is_array($posts) && sizeof($posts) > 0) {
      ?>
      <div class="row">
        <?php

        foreach($posts as $item) {
          $it_id = intval($item->get_var("id"));
          $it_tit = stripslashes(get_output($item->get_var("titulo_" . $_CONFIG["ref_lang"])));
          if(trim($it_tit) == "") {$it_tit = stripslashes(get_output($banner->get_var("titulo_pt")));}
          $it_titurl = gera_titulo_amigavel(get_output($item->get_var("titulo_pt")));
          $it_descr = stripslashes(get_output($item->get_var("descricao_" . $_CONFIG["ref_lang"])));
          if(trim($it_descr) == "") {$it_descr = stripslashes(get_output($item->get_var("descricao_pt")));}
          $it_descr = abrev_texto($it_descr,200,"p","...",true);
          $it_bg = stripslashes(get_output($item->get_var("imagem")));
          $it_data = stripslashes(get_output($item->get_var("data_criacao")));
          $cats = stripslashes(get_output($item->get_var("tags")));
  
          $it_img = $it_bg;
          $it_folder = ROOT_SERVER . ROOT . $item->get_upload_folder("imagem");
  
          $it_categorias = "";
          $cats = explode(";",$cats);
          foreach($cats as $cid) {
              $cat = new noticias_tags();
              $cat->inicia_dados();
              $cat->set_var("id",intval($cid));
              $cat->carrega_dados();
  
              if($cat != null) {
              $cat_id = intval($cat->get_var("id"));
              $cat_tit = stripslashes(get_output($cat->get_var("titulo_" . $_CONFIG["ref_lang"])));
              $cat_titurl = gera_titulo_amigavel(get_output($cat->get_var("titulo_pt")));
              if(trim($it_categorias) != "") {
                  $it_categorias .= " <span class=\"mx-2\">&bullet;</span> ";  
              }
              $it_categorias .= " <a href=\"" . ROOT_SERVER . ROOT . "posts/categoria/" . $cat_titurl . "/" . $cat_id . "\">" . $cat_tit . "</a>";
              }
          }
          
          ?>
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
            <div class="h-entry">
              <a href="<?php echo ROOT_SERVER . ROOT; ?>posts/<?php echo $it_titurl; ?>/<?php echo $it_id; ?>"><img src="<?php echo $it_folder . $it_img; ?>" alt="<?php echo $it_tit; ?>" class="img-fluid"></a>
              <h2 class="font-size-regular"><a href="<?php echo ROOT_SERVER . ROOT; ?>posts/<?php echo $it_titurl; ?>/<?php echo $it_id; ?>" class="text-black"><?php echo $it_tit; ?></a></h2>
              <div class="meta mb-4">
                <?php echo converte_data($it_data,3); ?> <span class="mx-2">&bullet;</span>
                <?php if(trim($it_aut) != "") { echo $it_aut; ?> <span class="mx-2">&bullet;</span> <?php } ?> <?php echo $it_categorias; ?>
              </div>
              <p><?php echo $it_descr; ?></p>
              <p><a href="<?php echo ROOT_SERVER . ROOT; ?>posts/<?php echo $it_titurl; ?>/<?php echo $it_id; ?>"><?php echo get_lang("_VER_MAIS"); ?></a></p>
            </div> 
          </div>
          <?php
        }

        ?>
      </div>
      <?php 
    } else {
        ?>
        <h2 class="align-center"><?php echo get_lang("_NENHUM_REGISTRO_ENCONTRADO"); ?></h2>
        <?php
    }
    ?>
  </div>
</section>
<?php

include_once("section-contato.php");

include_once("footer.php");

?>