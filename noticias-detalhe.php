<?php

include_once("config.php"); 

$_ref_page = "noticias";

$pages = new conteudos();
$pages = $pages->localiza(array("nome_arquivo","id_idioma"),array($_ref_page,$_CONFIG["id_lang"]));
if(!is_array($pages) || sizeof($pages) == 0) {
    $pages = new conteudos();
    $pages = $pages->localiza(array("nome_arquivo","id_idioma"),array($_ref_page,1));
}
if(!is_array($pages) || sizeof($pages) == 0) {
    $pages = new conteudos();
    $pages = $pages->localiza(array("nome_arquivo","id_idioma"),array($_ref_page,99));
}

if(is_array($pages))
{
	$page = $pages[0];
}

if($page <> null)
{
	$descr = $page->get_var("texto");
	$titulo = stripslashes(get_output($page->get_var("titulo")));
	$descrRes = stripslashes(get_output($page->get_var("meta_descricao")));
	$keywords = stripslashes(get_output($page->get_var("meta_palavras_chave")));

	$_TITULO_PAGINA = strip_tags($titulo);
	$_DESCRICAO_PAGINA = strip_tags($descrRes);
	$_PALAVRAS_CHAVE_PAGINA = strip_tags($keywords);
}

if(intval($_REQUEST["id"]) > 0) {
  carrega_classe("noticias");
  carrega_classe("noticias_tags");

  $item = new noticias();
  $item->inicia_dados();
  $item->set_var("id",intval($_REQUEST["id"]));
  $item->carrega_dados();

  if($item != null) {
      $item_id = intval($item->get_var("id"));
      $item_tit = stripslashes(get_output($item->get_var("titulo_" . $_CONFIG["ref_lang"])));
      if(trim($item_tit) == "") {$item_tit = stripslashes(get_output($item->get_var("titulo_pt")));}
      $item_titurl = gera_titulo_amigavel(get_output($item->get_var("titulo_pt")));
      $item_autor = get_output($item->get_var("autor"));
      if(trim($item_autor) == "") {
        $item_autor = $_CONFIG["titulo"];
      }
      $item_descr = stripslashes(get_output($item->get_var("descricao_" . $_CONFIG["ref_lang"])));
      if(trim($item_descr) == "") {$item_descr = stripslashes(get_output($item->get_var("descricao_pt")));}
      $item_res = abrev_texto($item_descr,200,"p","...",true);
      $item_bg = stripslashes(get_output($item->get_var("imagem")));
      $item_data = stripslashes(get_output($item->get_var("data_criacao")));
      $cats = stripslashes(get_output($item->get_var("tags")));          
      
      $item_img = $item_bg;
      $item_folder = ROOT_SERVER . ROOT . $item->get_upload_folder("imagem");

      $item_categorias = "";
      $cats = explode(";",$cats);
      foreach($cats as $cid) {
        $cat = new noticias_tags();
        $cat->inicia_dados();
        $cat->set_var("id",intval($cid));
        $cat->carrega_dados();

        if($cat != null) {
          $cat_id = intval($cat->get_var("id"));
          $cat_tit = stripslashes(get_output($cat->get_var("titulo_" . $_CONFIG["ref_lang"])));
          $cat_titurl = gera_titulo_amigavel(get_output($cat->get_var("titulo_pt")));
          if(trim($item_categorias) != "") {
            $item_categorias .= " <span class=\"mx-2\">&bullet;</span> ";  
          }
          $item_categorias .= " <a href=\"" . ROOT_SERVER . ROOT . "posts/categoria/" . $cat_titurl . "/" . $cat_id . "\">" . $cat_tit . "</a>";
        }
      }

      $shareURL_FB = ROOT_SERVER . ROOT . $_ref_page . "/" . $_REQUEST["titulo"] . "/" . $_REQUEST["id"];
      
      $_TITULO_PAGINA = $item_tit;
      $_DESCRICAO_PAGINA = strip_tags($item_descr);
      //$_PALAVRAS_CHAVE_PAGINA = $item_tags;
      if(trim($item_img) != "") {
          $_IMG_SHARE = ROOT_SERVER . ROOT . UPLOAD_FOLDER . $item_img;
      }
  }
}

include_once("header.php"); 

?>

<div class="site-blocks-cover blog-cover overlay lazy" data-src="<?php echo $item_folder . $item_img; ?>" data-aos="fade">
  <div class="overlay-shadow"></div>
  <div class="container">
    <div class="row align-items-center justify-content-center">

      
          <div class="col-md-6 mt-lg-5 text-center">
            <h1><?php echo $item_tit; ?></h1>
            <p class="post-meta"><?php echo converte_data($item_data,3); ?> <span class="mx-2">&bullet;</span> <?php if(trim($item_aut) != "") { echo $item_aut; ?> <span class="mx-2">&bullet;</span> <?php } ?> <?php echo $item_categorias; ?></p>
            
          </div>
        
    </div>
  </div>
</div>  



<section class="site-section">
  <div class="container">
    <div class="row">
      <div class="col-md-8 blog-content">
        <div class="lead"><?php echo $item_res; ?></div>
        <div class="pt-5">
          <?php echo $item_descr; ?>
        </div>


        <?php
        //if(intval($item_vis) == 1)
        { ?>
          <div class="pt-5 w-100">
            <div class="fb-comments" data-href="<?php echo $shareURL_FB; ?>" data-width="100%" data-numposts="10"></div>
          </div>
        <?php } ?>

      </div>
      <div class="col-md-4 sidebar">
        <div class="sidebar-box gray-box">
          <form action="<?php echo ROOT_SERVER . ROOT; ?>posts/" class="search-form" method="GET">
            <div class="form-group">
              <span class="icon fa fa-search"></span>
              <input type="text" name="busca" class="form-control" placeholder="<?php echo get_lang("_BUSCA"); ?>">
            </div>
          </form>
        </div>
        <?php if(trim($item_categorias) != "") { ?>
        <div class="sidebar-box">
          <div class="categories">
            <h3><?php echo get_lang("_CATEGORIAS"); ?></h3>
            <?php echo $item_categorias; ?>
          </div>
        </div>
        <?php } ?>
        <?php
        if(trim($item_autor) != "") {
          ?>
          <div class="sidebar-box">
            <h3><?php echo get_lang("_POSTPOR"); ?><?php echo $item_autor; ?></h3>
          </div>
          <?php
        }
        ?>

        <?php /*<div class="sidebar-box">
          <h3>Paragraph</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus itaque, autem necessitatibus voluptate quod mollitia delectus aut, sunt placeat nam vero culpa sapiente consectetur similique, inventore eos fugit cupiditate numquam!</p>
        </div>*/ ?>
      </div>
    </div>
  </div>
</section>

<?php

include_once("section-contato.php");

include_once("footer.php");

?>