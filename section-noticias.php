<?php

include_once("config.php");

?>
<section class="site-section" id="noticias">
  <div class="anchor posts" data-tpad="0"></div>
  <div class="container">
    <div class="row mb-5">
      <div class="col-12 text-center">
        <h3 class="section-sub-title">Blog</h3>
        <h2 class="section-title mb-3"><?php echo get_lang("_NOTICIAS"); ?></h2>
      </div>
    </div>

    <div class="row">
      <?php

      carrega_classe("noticias");

      $itens = new noticias();
      $itens = $itens->get_array_ativos("","data_criacao DESC",3);

      if(is_array($itens) && sizeof($itens) > 0) {
        foreach($itens as $item) {
          $it_id = intval($item->get_var("id"));
          $it_tit = stripslashes(get_output($item->get_var("titulo_" . $_CONFIG["ref_lang"])));
          if(trim($it_tit) == "") {$it_tit = stripslashes(get_output($banner->get_var("titulo_pt")));}
          $it_titurl = gera_titulo_amigavel(get_output($item->get_var("titulo_pt")));
          $it_descr = stripslashes(get_output($item->get_var("descricao_" . $_CONFIG["ref_lang"])));
          if(trim($it_descr) == "") {$it_descr = stripslashes(get_output($item->get_var("descricao_pt")));}
          $it_descr = abrev_texto($it_descr,200,"p","...",true);
          $it_bg = stripslashes(get_output($item->get_var("imagem")));
          $it_data = stripslashes(get_output($item->get_var("data_criacao")));
          $cats = stripslashes(get_output($item->get_var("tags")));

          $it_img = $it_bg;
          $it_folder = ROOT_SERVER . ROOT . $item->get_upload_folder("imagem");

          $it_categorias = "";
          $cats = explode(";",$cats);
          foreach($cats as $cid) {
            $cat = new noticias_tags();
            $cat->inicia_dados();
            $cat->set_var("id",intval($cid));
            $cat->carrega_dados();

            if($cat != null) {
              $cat_id = intval($cat->get_var("id"));
              $cat_tit = stripslashes(get_output($cat->get_var("titulo_" . $_CONFIG["ref_lang"])));
              $cat_titurl = gera_titulo_amigavel(get_output($cat->get_var("titulo_pt")));
              if(trim($it_categorias) != "") {
                $it_categorias .= " <span class=\"mx-2\">&bullet;</span> ";  
              }
              $it_categorias .= " <a href=\"" . ROOT_SERVER . ROOT . "posts/categoria/" . $cat_titurl . "/" . $cat_id . "\">" . $cat_tit . "</a>";
            }
          }
          
          ?>
      <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
        <div class="h-entry">
          <a href="<?php echo ROOT_SERVER . ROOT; ?>posts/<?php echo $it_titurl; ?>/<?php echo $it_id; ?>"><img data-src="<?php echo $it_folder . $it_img; ?>" alt="<?php echo $it_tit; ?>" class="img-fluid lazy"></a>
          <h2 class="font-size-regular"><a href="<?php echo ROOT_SERVER . ROOT; ?>posts/<?php echo $it_titurl; ?>/<?php echo $it_id; ?>" class="text-black"><?php echo $it_tit; ?></a></h2>
          <div class="meta mb-4">
            <?php echo converte_data($it_data,3); ?> <span class="mx-2">&bullet;</span>
            <?php if(trim($it_aut) != "") { echo $it_aut; ?> <span class="mx-2">&bullet;</span> <?php } ?> <?php echo $it_categorias; ?>
          </div>
          <p><?php echo $it_descr; ?></p>
          <p><a href="<?php echo ROOT_SERVER . ROOT; ?>posts/<?php echo $it_titurl; ?>/<?php echo $it_id; ?>"><?php echo get_lang("_VER_MAIS"); ?></a></p>
        </div> 
      </div>
          <?php
        }
      }

      ?>

      <div class="row align-items-center justify-content-center w-100"><a href="<?php echo ROOT_SERVER . ROOT; ?>posts/" class="btn btn-primary mr-2 mb-2"><?php echo get_lang("_VER_MAIS_NOTICIAS"); ?></a></div>
      
    </div>
  </div>
</section>