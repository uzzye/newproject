<?php

include_once("config.php");

if(!$_REQUEST["lgpd_confirm"]) {
    if(!$_COOKIE[$projectName . "_lgpd_confirm"]) {
        ?>
        <div class="lgpd_confirm" id="lgpd_confirm">
            <div class="lgpd_text"><?php
            if(trim($arrayConteudos["lgpd"]["descricao"]) == "") { ?>Este site utiliza recursos como <strong>cookies</strong> ou armazenamento de dados através de formulários e campos de input. Ao continuar com a navegação você está de acordo com a utilização destes recursos. Caso desejar, você pode verificar a configuração de bloqueio de cookies do seu navegador e não utilizar os formulários deste site.<?php } else { echo $arrayConteudos["lgpd"]["descricao"]; } 
            ?></div>
            <div class="lgpd_button">
                <button onclick="lgpd_confirm_send();"><?php
            if(trim($arrayConteudos["lgpd"]["titulo"]) == "") { ?>Certo, entendi.<?php } else { echo $arrayConteudos["lgpd"]["titulo"]; } 
            ?></button>
            </div>
        </div>
        <style type="text/css">
            .lgpd_confirm {position: fixed; z-index: 999999; bottom: 0px; left: 0px; width: 100%; height: auto; padding: 20px; box-sizing: border-box; background: rgba(0,0,0,.95); color: #fff;}
            .lgpd_text {position: relative; float: left; width: 85%;}
            .lgpd_button {position: relative; float: right; width: 12%;}
            .lgpd_button button {width: 100%; background: #fff; color: #000; border: 0px; padding: 10px; box-sizing: border-box; cursor: pointer; }
            .lgpd_button button:hover {background: #e0e0e0; color: #000;}
            @media screen and (max-width: 768px) {
                .lgpd_text {width: 100%;}
                .lgpd_button {width: 100%; margin-top: 10px;}
            }
        </style>
        <script type="text/javascript" language="javascript">
        function lgpd_confirm_send() {
            var xhttp = new XMLHttpRequest();
            xhttp.open("POST", "<?php echo ROOT_SERVER . ROOT; ?>_lgpd_confirm.php", true);
            xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            xhttp.onreadystatechange = function() { // Call a function when the state changes.
                if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
                    // Request finished. Do processing here.
                    document.getElementById('lgpd_confirm'). parentNode.removeChild(document.getElementById('lgpd_confirm'));
                }
            }
            xhttp.send("lgpd_confirm=true");
        }
        </script>
        <?php
    }
} else {
    @setcookie($projectName . "_lgpd_confirm", true, (time()+86400*30), "/");
}

?>