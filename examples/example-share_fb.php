<?php 

include_once("config.php");

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:og="http://ogp.me/ns#"
      xmlns:fb="http://www.facebook.com/2008/fbml">
<head prefix="og: http://ogp.me/ns#">
<!-- CSS -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="<?php echo ROOT_SERVER . ROOT; ?>_estilo/geral.css" />
<!-- MetaTags -->
<?php

carrega_classe("produtos");
carrega_classe("imagens_produtos");

// FB LIKE TRICK
if(isset($_REQUEST["share"])){
	$params = explode("_",$_REQUEST["share"]);

	$_REQUEST["share"] = $params[0];
	$_REQUEST["titulo"] = $params[1];
	$_REQUEST["id"] = $params[2];
}

// SE TEM PRODUTOS

if(intval($_REQUEST["id"]) > 0 && strtoupper($_REQUEST["share"]) == "PRODUTOS"){
	$it = new produtos();
	$it->inicia_dados();
	$it->set_var("id",intval($_REQUEST["id"]));
	if($it <> null)
	{
		$it->carrega_dados();

		$imagemFacebook = $it->get_var("imagem_galeria");

		$titCat = "";
		$catAux = $it->get_objeto_referencia("id_categoria");
		if($catAux != null) {
			$titCat = " | " . stripslashes(get_output($catAux->get_var("titulo")));
		}

		$_TITULO_PAGINA = strip_tags(stripslashes(get_output($it->get_var("nome")))) . $titCat;
		$_DESCRICAO_PAGINA = strip_tags(stripslashes(get_output($it->get_var("descricao"))));

		if(trim($imagemFacebook) == "")
		{
			// CATEGORIAS

			// Busca imagens da categoria, caso o post não tenha.

			/*$sqlCat = $mysql->exec_query("SELECT c.nome, c.imagem_mosaico, c.id FROM ".DBTABLE_BLOG_CATEGORIAS." c, ".DBTABLE_BLOG_CATEGORIAS_POSTS." cp WHERE c.id = cp.id_categoria AND cp.id_post = ".intval($it->get_var("id")) . " ORDER BY RAND()");
			if(@mysql_num_rows($sqlCat) > 0){
				$iIndexAux = rand(0,mysql_num_rows($sqlCat)-1);

				$imagemFacebook = stripslashes(get_output(mysql_result($sqlCat,$iIndexAux,"imagem_share")));
			}*/
		}
		
		$imagensFacebook = new imagens_produtos();
		$imagensFacebook = $imagensFacebook->get_array_ativos("id_produto = " . intval($it->get_var("id")),"RAND(), data_atualizacao DESC, data_criacao DESC");

		$urlAux = ROOT_SERVER . ROOT . "produtos/" . gera_titulo_amigavel(get_output($it->get_var("nome"))) . "/" . intval($it->get_var("id"));
		
		?>
<link rel="shortcut icon" href="<?php echo ROOT_SERVER . ROOT; ?>favicon.ico?<?php echo Date("YmdHis"); ?>" type="image/x-icon" />
<link rel="icon" href="<?php echo ROOT_SERVER . ROOT; ?>favicon.ico?<?php echo Date("YmdHis"); ?>" type="image/x-icon" />
<?php
if(is_array($imagensFacebook) && sizeof($imagensFacebook) > 0){
	foreach($imagensFacebook as $imgAux) {
?>
<link rel="image_src" href="<?php echo ROOT_SERVER . ROOT . UPLOAD_FOLDER . stripslashes(get_output($imgAux->get_var("imagem"))); ?>" />
<?php		
	}
}
else if(trim($imagemFacebook) != ""){
?>
<link rel="image_src" href="<?php echo ROOT_SERVER . ROOT . UPLOAD_FOLDER . $imagemFacebook; ?>" />
<?php
} else if(file_exists("_estilo/images/imgShare.png")){
?>
<link rel="image_src" href="<?php echo ROOT_SERVER . ROOT; ?>_estilo/images/imgShare.png" />
<?php
}
else{?>
<link rel="image_src" href="<?php echo ROOT_SERVER . ROOT; ?>cms/_estilo/images/geral/logo_cms.png" />
<?php } ?>

<?php

if(is_array($imagensFacebook) && sizeof($imagensFacebook) > 0){
	foreach($imagensFacebook as $imgAux) {
?>
<meta property="og:image" content="<?php echo ROOT_SERVER . ROOT . UPLOAD_FOLDER . stripslashes(get_output($imgAux->get_var("imagem"))); ?>" />
<?php		
	}
}
else if(trim($imagemFacebook) != ""){
?>
<meta property="og:image" content="<?php echo ROOT_SERVER . ROOT . UPLOAD_FOLDER . $imagemFacebook; ?>" />
<?php
}
else if(file_exists("_estilo/images/imgShare.png")){
?>
<meta property="og:image" content="<?php echo ROOT_SERVER . ROOT; ?>_estilo/images/imgShare.png" />
<?php
}
else{?>
<meta property="og:image" content="<?php echo ROOT_SERVER . ROOT; ?>cms/_estilo/images/geral/logo_cms.png" />
<?php }
?> 
<!-- <meta property="og:url" content="<?php echo $urlAux; ?>" /> -->

<title><?php
if(isset($_TITULO_PAGINA) && trim($_TITULO_PAGINA) <> ""){
	echo $_TITULO_PAGINA . " - ";
} 
echo $_CONFIG["titulo"]; ?></title>
<meta property="og:title" content="<?php
if(isset($_TITULO_PAGINA) && trim($_TITULO_PAGINA) <> ""){
	echo $_TITULO_PAGINA . " - ";
} 
echo $_CONFIG["titulo"]; ?>" />
<meta property="og:description" content="<?php
if(isset($_DESCRICAO_PAGINA) && trim($_DESCRICAO_PAGINA) <> ""){
	echo $_DESCRICAO_PAGINA;
} else {
	echo $_CONFIG["descricao"];
} ?>" /> 
<meta name="Description" content="<?php
if(isset($_DESCRICAO_PAGINA) && trim($_DESCRICAO_PAGINA) <> ""){
	echo $_DESCRICAO_PAGINA;
} else {
	echo $_CONFIG["descricao"];
} ?>" />
<!-- <link rel="canonical" href="<?php echo ($urlAux); ?>" /> -->
<script> //location.href = '<?php echo ($urlAux); ?>'; </script>
		<?php
	}
}

// SE NÃO ENCONTROU O POST

else{
?>
<title><?php
if(isset($_TITULO_PAGINA) && trim($_TITULO_PAGINA) <> ""){
	echo $_TITULO_PAGINA . " - ";
} 
echo $_CONFIG["titulo"]; ?></title>
<link rel="shortcut icon" href="<?php echo ROOT_SERVER . ROOT; ?>favicon.ico?<?php echo Date("YmdHis"); ?>" type="image/x-icon" />
<link rel="icon" href="<?php echo ROOT_SERVER . ROOT; ?>favicon.ico?<?php echo Date("YmdHis"); ?>" type="image/x-icon" />
<?php if(file_exists("_estilo/images/imgShare.png")){
?>
<link rel="image_src" href="<?php echo ROOT_SERVER . ROOT; ?>_estilo/images/imgShare.png" />
<?php
}
else{?>
<link rel="image_src" href="<?php echo ROOT_SERVER . ROOT; ?>cms/_estilo/images/geral/logo_cms.png" />
<?php } ?>
<meta property="og:title" content="<?php
if(isset($_TITULO_PAGINA) && trim($_TITULO_PAGINA) <> ""){
	echo $_TITULO_PAGINA . " - ";
} 
echo $_CONFIG["titulo"]; ?>" />
<meta property="og:description" content="<?php
if(isset($_DESCRICAO_PAGINA) && trim($_DESCRICAO_PAGINA) <> ""){
	echo $_DESCRICAO_PAGINA;
} else {
	echo $_CONFIG["descricao"];
} ?>" /> 
<?php if(file_exists("_estilo/images/imgShare.png")){
?>
<meta property="og:image" content="<?php echo ROOT_SERVER . ROOT; ?>_estilo/images/imgShare.png" /><?php
}
else{?>
<meta property="og:image" content="<?php echo ROOT_SERVER . ROOT; ?>cms/_estilo/images/geral/logo_cms.png" />
<?php } ?>
<meta property="og:url" content="<?php echo (ROOT_SERVER . ROOT); ?>blog" />
<link rel="canonical" href="<?php echo (ROOT_SERVER . ROOT); ?>blog" />
<script> location.href = '<?php echo (ROOT_SERVER . ROOT); ?>blog'; </script>
<meta name="Description" content="<?php
if(isset($_DESCRICAO_PAGINA) && trim($_DESCRICAO_PAGINA) <> ""){
	echo $_DESCRICAO_PAGINA;
} else {
	echo $_CONFIG["descricao"];
} ?>" />
<?php } ?>
<!-- MetaTags --> 
<meta http-equiv="pragma" content="no-cache" /> 
<meta name="resource-types" content="document" /> 
<meta name="classification" content="Internet" /> 
<meta name="robots" content="None" /> 
<meta name="distribution" content="Global" /> 
<meta name="rating" content="General" /> 
<meta name="author" content="Uzzye" /> 
</head>
<body>
<?php //include_once("blog.php");?>
</body>
</html> 