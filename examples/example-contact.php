<?php

include_once("config.php");

$idPaginaAux = "contato";

$pagina = new conteudos();
$paginas = $pagina->localiza(array("nome_arquivo"),array($idPaginaAux));

if(is_array($paginas))
{
	$pagina = $paginas[0];
}

if($pagina <> null)
{
	$descr = $pagina->get_var("texto");
	$titulo = stripslashes(get_output($pagina->get_var("titulo")));
	$descrRes = stripslashes(get_output($pagina->get_var("meta_descricao")));
	$keywords = stripslashes(get_output($pagina->get_var("meta_palavras_chave")));

	$_TITULO_PAGINA = $titulo;
	$_DESCRICAO_PAGINA = $descrRes;
	$_PALAVRAS_CHAVE_PAGINA = $keywords;
}

include("_inc/base_topo.php"); 

// GoogleMaps key: AIzaSyDLgXx-Xv5YfCPYrCACeZQ8qD8kT-ozqGw

$arrayScripts[sizeof($arrayScripts)] = ROOT_SERVER . ROOT . "_js/dist/site.min.js";

?>
<section id="contato">
	<div class="form">
        <form name="formContato" id="formContato" class="formPadrao" data-url="<?php echo ROOT_SERVER . ROOT; ?>ajax/contato/enviar" method="post" action="javascript:void(0);">
            <input type="hidden" name="acao" id="acao_contato" value="enviar" />
            <input type="hidden" name="verifier" id="verifier_contato" value="" />
            <input type="text" class="form-control" name="nome" id="nomeContato" req="true" placeholder="<?php echo get_lang("_NOME"); ?>" required/>
            <input type="text" class="form-control" placeholder="<?php echo get_lang("_EMAIL"); ?>" name="email" id="emailContato" req="true" required data-alias="email">
            <input type="text" class="form-control" placeholder="<?php echo get_lang("_TELEFONE"); ?>" name="telefone" id="telefoneContato" req="true" required data-pattern="(99)999999999" >
	        <?php /*<input type="file" class="dropify dropify-work" name="arquivo" id="work-arquivo" />*/ ?>
            <textarea placeholder="<?php echo get_lang("_MENSAGEM"); ?>" name="mensagem" id="mensagemContato" req="true" required></textarea>
            <div id="retornoPostContato" class="retornoPost"></div>
            <a id="btnEnviarContato" class="btnEnviar" title="<?php echo get_lang("_ENVIAR_MENSAGEM"); ?>" data-label-wait="<?php echo get_lang("_AGUARDE"); ?>..." data-label-send="<?php echo get_lang("_ENVIAR_MENSAGEM"); ?>">
                <input type="submit" class="" value="&nbsp;"/>
                <span><?php echo get_lang("_ENVIAR_MENSAGEM"); ?></span>
            </a>
        </form>
    </div>
	<div class="mapa">
	<?php
		echo $_CONFIG["mapa_embed"];
	?>
	</div>
</section>
<?php 

include("_inc/base_rodape.php");

?>