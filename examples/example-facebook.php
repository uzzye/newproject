<?php

include_once("config.php"); 

// CONFIG DO FACEBOOK
// SDK 4
/*$FB_config = array(
  'app_id' => FACEBOOK_ID,
  'app_secret' => FACEBOOK_SECRET,
  'default_graph_version' => 'v2.5'
);
require_once("classes/Facebook/autoload.php");*/

// OLD SDK
$FB_config = array(
	'appId' => FACEBOOK_ID,
	'secret' => FACEBOOK_SECRET,
	'fileUpload' => false, // optional
	'allowSignedRequest' => false, // optional, but should be set to false for non-canvas apps
);
require_once("classes/facebook.php");

// FACEBOOK INI

// SDK 4
/*$permissions = array("scope" => "public_profile,user_friends,email,user_relationship_details,user_birthday,user_location"); // Optional permissions
$facebook = new Facebook\Facebook($FB_config);	    
$helper = $facebook->getRedirectLoginHelper();
$loginUrl = $helper->getLoginUrl(ROOT_SERVER . ROOT . "area-restrita", $permissions);*/

// OLD SDK
$permissions = array("scope" => "public_profile,user_friends,email,user_relationship_details,user_birthday,user_location"); // Optional permissions
$facebook = new Facebook($FB_config);
$loginUrl = $facebook->getLoginUrl($permissions);

// FACEBOOK END 

?>