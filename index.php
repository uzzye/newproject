<?php

include_once("config.php"); 

$_ref_page = "home";

$pages = new conteudos();
$pages = $pages->localiza(array("nome_arquivo","id_idioma"),array($_ref_page,$_CONFIG["id_lang"]));
if(!is_array($pages) || sizeof($pages) == 0) {
    $pages = new conteudos();
    $pages = $pages->localiza(array("nome_arquivo","id_idioma"),array($_ref_page,1));
}
if(!is_array($pages) || sizeof($pages) == 0) {
    $pages = new conteudos();
    $pages = $pages->localiza(array("nome_arquivo","id_idioma"),array($_ref_page,99));
}

if(is_array($pages))
{
	$page = $pages[0];
}

if($page <> null)
{
	$descr = $page->get_var("texto");
	$titulo = stripslashes(get_output($page->get_var("titulo")));
	$descrRes = stripslashes(get_output($page->get_var("meta_descricao")));
	$keywords = stripslashes(get_output($page->get_var("meta_palavras_chave")));

	$_TITULO_PAGINA = strip_tags($titulo);
	$_DESCRICAO_PAGINA = strip_tags($descrRes);
	$_PALAVRAS_CHAVE_PAGINA = strip_tags($keywords);
}

include_once("header.php"); 

?>
<?php

// Uncomment to add custom JS below
//$arrayScripts[sizeof($arrayScripts)] = ROOT_SERVER . ROOT . "js/dist/[PUT_FILENAME_HERE]";

// Default object get sample
/* carrega_classe("nomedaclasse");
$its = new nomedaclasse();
$its = $its->get_array_ativos("","ranking DESC, data_atualizacao DESC, data_criacao DESC");
if(is_array($its) && sizeof($its) > 0) {
    //for($w=0;$w<10;$w++) // For multiple instances test
    {foreach($its as $it) {
        $it_id = intval($it->get_var("id"));
        $it_img = stripslashes(get_output($it->get_var("imagem")));
        $it_tit = stripslashes(get_output($it->get_var("titulo_" . $_CONFIG["ref_lang"])));
        if(trim($it_tit) == "") {$it_tit = stripslashes(get_output($it->get_var("titulo_pt")));}
        $it_titurl = gera_titulo_amigavel(stripslashes(get_output($it->get_var("titulo_pt"))));
    }}
} */

if(intval($_REQUEST["replica"]) == 1) {
    include_once("section-banner.php");
    include_once("section-highlights-right.php");
    include_once("section-diferenciais.php");
    include_once("section-lojas.php");
    include_once("section-contato.php");
    include_once("section-noticias.php");
} else {
    include_once("section-banner.php");
    include_once("section-produtos.php");
    include_once("section-lojas.php");
    include_once("section-highlights-left.php");
    include_once("section-noticias.php");
    include_once("section-contato.php");
    include_once("section-map.php");
}

include_once("footer.php");

?>