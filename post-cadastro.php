<?php 

include_once("config.php");

carrega_classe("cadastros");

if(trim($_REQUEST["verifier"]) != "") {
	$msg_erro = get_lang("_FORM_RETURN_ERRO_1");
}
else if($_REQUEST["acao"] == "enviar")
{
	if(//trim($_REQUEST["nome"]) == "" ||
	   trim($_REQUEST["email"]) == "")
	{
		$ERRO = 1;
		$msg_erro = "<span class=\"erro\">" . get_lang("_FORM_RETURN_ERRO_2") . "</span>";
	}
	else if(!valida_email($_REQUEST["email"]))
	{
		$ERRO = 2;
		$msg_erro = "<span class=\"erro\">" . get_lang("_FORM_RETURN_ERRO_4") . "</span>";		
	}
	else
	{	
		$ERRO = 0;
			
		$ip = get_ip();
		
		$tituloAux = "Novo Cadastro em " . $_CONFIG["titulo"];
		
		$cadastros = new cadastros();
		$cadastros->inicia_dados();
		//$cadastros->set_var("nome",output_decode($_REQUEST["nome"]));		
		$cadastros->set_var("email",output_decode($_REQUEST["email"]));	
		/*$cadastros->set_var("id_empreendimento",output_decode($_REQUEST["id_empreendimento"]));
		$empAux = "";
		if(intval($_REQUEST["id_empreendimento"]) > 0) {
			carrega_classe("empreendimentos");
			$itAux = new empreendimentos();
			$itAux->inicia_dados();
			$itAux->set_var("id",intval($_REQUEST["id_empreendimento"]));
			$itAux->carrega_dados();
			$empAux = stripslashes(get_output($itAux->get_var("nome")));
			$cadastros->set_var("empreendimento",$itAux->get_var("nome"));
		}*/
		$cadastros->set_var("ip",output_decode($ip));
		$cadastros->set_var("ativo",1);
		$cadastros->set_var("usuario_criacao",99);
		$id_aux = $cadastros->inclui(false,true);

		$strMensagem = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
		<html xmlns=\"http://www.w3.org/1999/xhtml\" style=\"background: #fff!important;\">
			<head>
				<title>" . $_CONFIG["titulo"] . "</title>
				<!-- CSS -->
				<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
				<link rel=\"stylesheet\" type=\"text/css\" href=\"" . ROOT_SERVER . ROOT . "css/geral.css\" />
				<link rel=\"stylesheet\" type=\"text/css\" href=\"" . ROOT_SERVER . ROOT . "css/helpers.css\" />
				<link rel=\"stylesheet\" type=\"text/css\" href=\"" . ROOT_SERVER . ROOT . "css/tipografia.css\" />
				<style>
					a {color: #000!important;}
				</style>
			</head>
			<body style=\"color: #000!important; background: #fff!important;\">
			";

		$strMensagem .= "<img src=\"" . ROOT_SERVER . ROOT . "img/logo_email.png\" border=\"0\"><br />";
		$strMensagem .= "<br />";
		$strMensagem .= "Um novo cadastro foi realizado em " . $_CONFIG["titulo"] . ". Para acessar o CMS, clique <a href=\"" . ROOT_SERVER . ROOT . ROOT_CMS . "\" target=\"_blank\">aqui</a>.<br />";
		$strMensagem .= "<br />";
		//$strMensagem .= "<b>Assunto:</b> ". stripslashes($_REQUEST["assunto"]) ."<br>";
		//$strMensagem .= "<b>Nome:</b> ". stripslashes($_REQUEST["nome"]) ."<br>";
		$strMensagem .= "<b>E-mail:</b> ". stripslashes($_REQUEST["email"]) ."<br>";
		/*if(trim($empAux) != ""){
			$strMensagem.= "<b>Empreendimento:</b> ". $empAux ."<br>";
		}*/
		$strMensagem .= "<br>";
		$strMensagem .= "<b>IP:</b> ". $ip ."<br>";
		$strMensagem .= "<b>Data de Envio:</b> ". date("d/m/Y H:i:s") ."<br>";		
				
		$de = EMAIL_DEFAULT_FROM;
		$para = EMAIL_PADRAO_CONTATO;

		//sendMail($de, $para, $tituloAux, $strMensagem);
		sendMailSMTP($de, $para, $tituloAux, $strMensagem);

		$msg_erro = "<!--SUCESSO-->" . get_lang("_FORM_RETURN_OK_1");
	}
	echo $msg_erro;
}
?>