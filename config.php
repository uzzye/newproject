<?php

$projectName = "newproject";
$projectColors = array(
	"main" => "dark",
	"theme" => "#000"
);

// inicializa
//$root = dirname( $_SERVER["PHP_SELF"] ) == DIRECTORY_SEPARATOR ? "" : dirname( $_SERVER["PHP_SELF"] );
$root = dirname( $_SERVER["PHP_SELF"] ) == DIRECTORY_SEPARATOR ? "" : dirname( $_SERVER["PHP_SELF"] );
if(substr($root, 0, 1) != "/" && substr($root, 0, 1) != "\\" && trim($root) != "") {
	$root = "/" . $root;
}
$_noSQL = false;
if(isset($_CONFIG)) {
	$_noSQL = $_CONFIG["noSQL"];
}
$_CONFIG = array();
$_CONFIG["noSQL"] = $_noSQL;

// compression
@ini_set('zlib.output_compression','On');
@ini_set('zlib.output_compression_level','1');
@ini_set('implicit_flush', 1);
@ini_set('output_buffering', 1);

// configura sessão
@ini_set('session.gc_maxlifetime',24*60*60); // 1 dia
@ini_set('session.cookie_httponly',true);

//@session_save_path(getcwd() . "/_sessions");
@session_name($projectName . "_site");
session_start();
ob_start();

@set_time_limit(0);

// configura constantes
if(!defined("ROOT")){
	define("ROOT",$root . "/"); 	// UTILIZADO PARA INCLUDES/REFERENCIAS CLIENT-SIDE (LINKS, IMAGENS, JS E DEMAIS ARQUIVOS "EXTERNOS")
}

if(!defined("ROOT_CMS")){
	define("ROOT_CMS","cms/");	// UTILIZADO PARA INCLUDES/REFERÊNCIAS SERVER-SIDE (PHP) DO CMS
}
if(!defined("ROOT_SITE")){
	define("ROOT_SITE","");		// UTILIZADO PARA INCLUDES/REFERÊNCIAS SERVER-SIDE (PHP) DO SITE EM ESPECÍFICO
}
if(!defined("ROOT_SERVER")){
	//$rootServer = "http://" . $_SERVER["SERVER_NAME"];		// UTILIZADO PARA COMPARTILHAMENTO DE CONTEÚDO
	$rootServer = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]"; //:$_SERVER[SERVER_PORT]
	/*if(substr($rootServer,-1) != "/") {
		$rootServer .= "/";
	}*/
	define("ROOT_SERVER",$rootServer);
}

// configura pasta de uploads
$_CONFIG["pasta_upload"] = "upload/";
define("UPLOAD_FOLDER",$_CONFIG["pasta_upload"]);

// configura úteis cms
include_once(ROOT_CMS . "utils.php");

// configura classes
include_once(ROOT_SITE . "classes.php");

// configura úteis site
include_once(ROOT_SITE . "utils.php");

// configura banco de dados
//$_CONFIG["db_padrao"] = "DB_MySQL";
$_CONFIG["db_padrao"] = "DB_MySQLi";
$mySQL_prefix = $projectName;
include_once(ROOT_CMS . "classes/" . $_CONFIG["db_padrao"] . ".php");
$db = $mysql = new $_CONFIG["db_padrao"]();

$DB_persistent = true;
include(ROOT_CMS . "_common/conexao.php");
include(ROOT_CMS . "_common/tabelas.php");
//include_once(ROOT_SITE . "_common/tabelas.php");

// armazena configs
if($_REQUEST["lang"] == "" && (!is_array($_SESSION["_config"]) || trim($_SESSION["_config"]["lang"]) == "")) {
	$_REQUEST["lang"] = get_country_lang();
}
configura($_REQUEST["lang"]);

$lang_site = array();

// configura idioma
if(file_exists(ROOT_SITE . "lang/" . $_CONFIG["lang"] . ".php"))
{
	include_once(ROOT_SITE . "lang/" . $_CONFIG["lang"] . ".php");
} else {
	include_once(ROOT_SITE . "lang/pt-br.php");
}
if(file_exists(ROOT_CMS . "lang/" . $_CONFIG["lang"] . ".php"))
{
	include_once(ROOT_CMS . "lang/" . $_CONFIG["lang"] . ".php");
} else {
	include_once(ROOT_CMS . "lang/pt-br.php");
}

// configura endereços de e-mails
define("EMAIL_PADRAO_CONTATO",$_CONFIG["email_padrao_contato"]);
define("EMAIL_DEFAULT_FROM",$_CONFIG["email_default_from"]);

// configura chave aleatória
if(!isset($_SESSION["GLOBAL_RANDOM_KEY"]) || trim($_SESSION["GLOBAL_RANDOM_KEY"]) == "")
{
	$_SESSION["GLOBAL_RANDOM_KEY"] = intval($_SESSION["idLogin_site"]) . str_pad(rand(0,99999),5,"0",STR_PAD_LEFT);
}

// configura charset
header("Content-Type: text/html; charset=" . $_CONFIG["charset_padrao"]);
header('X-XSS-Protection:0');

// IDIOMA
$sqlWhereIdiomas = " (id_idioma = 99 OR ISNULL(id_idioma) OR id_idioma = " . intval($_CONFIG["id_lang"]) . ") ";

// FACEBOOK
//define("FACEBOOK_ID",""); // FINAL
define("FACEBOOK_ID","482714205393066");
//define("FACEBOOK_SECRET",""); // FINAL
define("FACEBOOK_SECRET","34432196736a966d2af37d88b333b4da");

?>