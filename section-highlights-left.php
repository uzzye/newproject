<?php

include_once("config.php");

?>
<section class="site-section">
  <div class="anchor destaques" data-tpad="0"></div>
  <div class="container">
    
    <div class="row">
      <div class="col-lg-6">

        <div class="owl-carousel slide-one-item-alt">
          <?php

          carrega_classe("destaques");

          $itens = new destaques();
          $itens = $itens->get_array_ativos("","ranking ASC");

          if(is_array($itens) && sizeof($itens) > 0) {
            foreach($itens as $item) {
              $it_id = intval($item->get_var("id"));
              $it_tit = stripslashes(get_output($item->get_var("titulo_" . $_CONFIG["ref_lang"])));
              if(trim($it_tit) == "") {$it_tit = stripslashes(get_output($item->get_var("titulo_pt")));}
              $it_titurl = gera_titulo_amigavel(get_output($item->get_var("titulo_pt")));
              $it_bg = stripslashes(get_output($item->get_var("imagem")));
              
              $it_img = $it_bg;
              $it_folder = ROOT_SERVER . ROOT . $item->get_upload_folder("imagem");
              
              ?>
              <img data-src="<?php echo $it_folder . $it_img; ?>" alt="" class="img-fluid lazy">
              <?php
            }
          }

          ?>
        </div>
        <div class="custom-direction">
          <a href="#" class="custom-prev"><?php echo get_lang("_LIGHTBOX_ANT"); ?></a><a href="#" class="custom-next"><?php echo get_lang("_LIGHTBOX_PROX"); ?></a>
        </div>

      </div>
      <div class="col-lg-5 ml-auto">
        
        <h2 class="section-title mb-3"><?php echo $arrayConteudos["destaques-titulo"]["descricao"]; ?></h2>
            <p class="lead"><?php echo $arrayConteudos["destaques-chamada"]["descricao"]; ?></p>
            <p><?php echo $arrayConteudos["destaques-descricao"]["descricao"]; ?></p>

            <p><a href="#/contato" class="nav-link btn btn-primary mt-4 mr-2 mb-2"><?php echo get_lang("_FALE_CONOSCO"); ?></a></p>          
        
      </div>
    </div>
  </div>
</section>