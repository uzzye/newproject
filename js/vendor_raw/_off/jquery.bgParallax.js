/*
Plugin: jQuery Parallax
Version 1.1.3
Author: Ian Lunn
Customized by: @mausalamon
Twitter: @IanLunn
Author URL: http://www.ianlunn.co.uk/
Plugin URL: http://www.ianlunn.co.uk/plugins/jquery-parallax/

Dual licensed under the MIT and GPL licenses:
http://www.opensource.org/licenses/mit-license.php
http://www.gnu.org/licenses/gpl.html
*/

(function( $ ){
	var $window = $(window);
	var windowHeight = $window.height();

	$window.resize(function () {
		windowHeight = $window.height();
	});

	$.fn.bgParallax = function(xpos, speedFactor, ypad, outerHeight) {
		var $this = $(this);
		var getHeight;
		var firstTop;
		
		//get the starting position of each element to have parallax applied to it		
		$this.each(function(){
		    firstTop = $this.offset().top;
		});

		if (outerHeight) {
			getHeight = function(jqo) {
				return jqo.outerHeight(true);
			};
		} else {
			getHeight = function(jqo) {
				return jqo.height();
			};
		}
			
		// setup defaults if arguments aren't specified
		if (arguments.length < 1 || xpos === null) xpos = "50%";
		if (arguments.length < 2 || speedFactor === null) speedFactor = 0.1;
		if (arguments.length < 3 || ypad === null) ypad = 0;
		if (arguments.length < 4 || outerHeight === null) outerHeight = true;
		
		// function to be called whenever the window is scrolled or resized
		function update(){
			var pos = $window.scrollTop();	

			var elAux = $this;			

			$this.each(function(){
				var $element = $(this);
				var top = $element.offset().top;
				var height = getHeight($element);
				var paddingTop = 0;

				if(ypad != 0) {
					if(!isNaN(ypad + top)){
						paddingTop = ypad + top;
					}
				}

				var matrixRegex = /matrix\((-?\d*\.?\d+),\s*0,\s*0,\s*(-?\d*\.?\d+),\s*0,\s*0\)/,
    				matches = $this.css('-webkit-transform').match(matrixRegex);

    			if(matches != undefined && matches.length > 0) {
					// Check if totally above or totally below viewport
					if (top + height < pos || top > pos + windowHeight || (pos -speedFactor < 0 && matches[2] > 1)) {
						return;
					}
				}

				$this.css('backgroundPosition', xpos + " " + Math.round(((firstTop - pos) * speedFactor) + (paddingTop)) + "px");
			});
		}		 

		/*$window.on("resize",function () {
			update();
			setTimeout(function(e){
				update();
			},100);
		});//.trigger('resize');

		$window.bind('scroll', update).resize(update);*/
		update();
	};
})(jQuery);
