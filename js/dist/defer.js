function loadScript(script,callback) {
	// get some kind of XMLHttpRequest
	var xhrObj = createXMLHTTPObject();
	// open and send a synchronous request
	xhrObj.open('GET', script, false);
	xhrObj.setRequestHeader('Access-Control-Allow-Origin','*');
    //xhrObj.setRequestHeader('Content-type','application/x-www-form-urlencoded');
    xhrObj.setRequestHeader('Content-type','text/javascript');
    /*try {
    	xhrObj.setRequestHeader('User-Agent','XMLHTTP/1.0');
    } catch(e) {
    	console.log(e.message);
    }*/
	xhrObj.send('');
	// add the returned content to a newly created script tag
	var se = document.createElement('script');
	se.type = "text/javascript";
	//se.src = script;
	se.text = xhrObj.responseText;
	document.getElementsByTagName('head')[0].appendChild(se);
	//document.body.appendChild(se);

	if(callback && callback != undefined) {
		callback();
	}
}

function sendRequest(url,callback,postData) {
    var req = createXMLHTTPObject();
    if (!req) return;
    var method = (postData) ? "POST" : "GET";
    req.open(method,url,true);
    // Setting the user agent is not allowed in most modern browsers It was
    // a requirement for some Internet Explorer versions a long time ago.
    // There is no need for this header if you use Internet Explorer 7 or
    // above (or any other browser)
    // req.setRequestHeader('User-Agent','XMLHTTP/1.0');
    if (postData)
        req.setRequestHeader('Content-type','application/x-www-form-urlencoded');
    req.onreadystatechange = function () {
        if (req.readyState != 4) return;
        if (req.status != 200 && req.status != 304) {
//          alert('HTTP error ' + req.status);
            return;
        }
        callback(req);
    }
    if (req.readyState == 4) return;
    req.send(postData);
}

var XMLHttpFactories = [
    function () {return new XMLHttpRequest()},
    function () {return new ActiveXObject("Msxml3.XMLHTTP")},
    function () {return new ActiveXObject("Msxml2.XMLHTTP.6.0")},
    function () {return new ActiveXObject("Msxml2.XMLHTTP.3.0")},
    function () {return new ActiveXObject("Msxml2.XMLHTTP")},
    function () {return new ActiveXObject("Microsoft.XMLHTTP")}
];

function createXMLHTTPObject() {
    var xmlhttp = false;
    for (var i=0;i<XMLHttpFactories.length;i++) {
        try {
            xmlhttp = XMLHttpFactories[i]();
        }
        catch (e) {
            continue;
        }
        break;
    }
    return xmlhttp;
}