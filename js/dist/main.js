var bFinishLoad;

var flipsnaps = [];
var swipers = [];

var arrayTOBanners = new Array();
arrayTOBanners["banner"] = null;
var arrayIndex = new Array();
arrayIndex["banner"] = -1;
var easingBanner;

// MAPA
var map_sID_1 = "mapa";

var map_ib = new Array();
try {
	map_ib[map_sID_1] = new InfoBox();
} catch(err) {
	//console.log(err.message);
}
var map_allMarkers = new Array();
map_allMarkers[map_sID_1] = [];

var map_maps = new Array();
var map_pins = new Array();
map_pins[map_sID_1] = [["Uzzye", "-29.154908", "-51.1868077", 1, "", ROOT_SERVER + ROOT + "img/pin.png", ROOT_SERVER + ROOT + "img/pin.png"]];

var map_geocoder = new Array();
var map_geoPadrao = new Array();
map_geoPadrao[map_sID_1] = "-29.154908,-51.1868077";
var map_zoomPadrao = new Array();
map_zoomPadrao[map_sID_1] = 16;
var map_bMapLoaded = new Array();
map_bMapLoaded[map_sID_1] = false; 

var MAP_style_array = [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}];

// VIDEOS

var myPlayers = [];
var yPlayers = []; 
var yVideoDones = [];
var iFixVid;

// CARREGA YOUTUBE

var bZoom = false;

/*var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag); */

AJAXCallback = function(e, AJAX_tipoAux, AJAX_titAux, AJAX_idAux){
	// PADRÃO

	loadieEl.loadie(0.50);
	$("#loadie-percent").html("50%");

	if(atualizaConteudo){
		atualizaConteudo(e);
	} else {
		if(atualizaSections){
			atualizaSections(e);
		}
	}

	if(atualizaBG){
		atualizaBG(e);
	} 

	if(atualizaBGInterna){
		atualizaBGInterna(e);
	}

	ajustaScrollers(e);

	if(myScrolls != undefined & Object.size(myScrolls) > 0){
		for (var key in myScrolls) {
		    if (key === 'length' || !myScrolls.hasOwnProperty(key)) continue;
		    myScrolls[key].refresh();
		}
	}

	if(scrollController){
		//scrollController.triggerCheckAnim();
		scrollController.update(true);
	}

    $('.dragging').unbind("click").click(function (e) {
        e.preventDefault();
	});
	
	// LAX
	
	// For new version of Lax (not included in this project) use this instead of the one in finishLoad()
	/*if(lax) {
		lax.init()

		// Add a driver that we use to control our animations
		lax.addDriver('scrollY', function() {
			return window.scrollY;
		});

		// Add animation bindings to elements
		//lax.addElements('.lax', {
			//scrollY: {
				//translateX: [
					//["elInY", "elCenterY", "elOutY"],
					//[0, 'screenWidth/2', 'screenWidth'],
				//]
			//}
		//});
	}*/

	// ZOOM

    /*if($(".img-zoom").length > 0) {
	    $(".img-zoom").each(function(e){
			var urlAux = $(this).data("zoom");
			$(this).parent().zoom({
				url: urlAux,
				touch: true,
				//on: "grab",
				magnify: 1,
				onZoomIn: function(){
					// ENTROU NO ZOOM
					
					//bZoom = true;
    			},
    			onZoomOut: function(){
    				// SAIU DO ZOOM

    				//bZoom = false;
    			}
			});
		});
	}*/

	// VIDEOS

	if($(".video-js").not(".banners .video-js").length > 0) {

		$(".video-js").not(".banners .video-js").each(function(e){
			var idAux = $(this).attr("id");
			var autoplay = 0;
			if($(this).hasClass("autoplay")) {
				autoplay = 1;
			}
			try {
		    	vidAux = videojs(idAux, {
		            controlBar: {
		                playToggle: true
		            },
		            fluid: true, 
		            autoplay: autoplay,
		            aspectRatio: "16:9" 
		    	});
		    	myPlayers[idAux] = vidAux;
		    } catch (e) {
	        	console.log(e.message);
	      	}
	    });
	}
    	
	//LIGHTBOX

    var vidAux;

	var options = {
	    variant:        null,                  /* Class that will be added to change look of the lightbox */
	    resetCss:       false,                 /* Reset all css */
	    openTrigger:    'click',               /* Event that triggers the lightbox */
	    closeTrigger:   'click',               /* Event that triggers the closing of the lightbox */
	    filter:         null,                  /* Selector to filter events. Think $(...).on('click', filter, eventHandler) */
	    openSpeed:      250,                   /* Duration of opening animation */
	    closeSpeed:     250,                   /* Duration of closing animation */
	    closeOnClick:   'background',          /* Close lightbox on click ('background', 'anywhere', or false) */
	    closeOnEsc:     true,                  /* Close lightbox when pressing esc */
	    loading:        '',                    /* Content to show while initial content is loading */
	    otherClose:     null,                  /* Selector for alternate close buttons (e.g. "a.close") */
	    closeIcon:      "&nbsp;",
	    beforeOpen:     $.noop,                /* Called before open. can return false to prevent opening of lightbox. Gets event as parameter, this contains all data */
	    beforeContent:  $.noop,                /* Called when content is about to be presented. `this` is the featherlight instance. Gets event as parameter */
	    beforeClose:    $.noop,                /* Called before close. can return false to prevent opening of lightbox. `this` is the featherlight instance. Gets event as parameter  */
	    previousIcon: "", //'&#9664;',     /* Code that is used as previous icon */
		nextIcon: "", //'&#9654;',     
	    //iframeMaxWidth: '80%',
	    //iframeWidth: '80%',
	    afterOpen:      function(e){
	    	if(this.$content.find(".youtube-player").length > 0) {

	    	} 
	    	else if(this.$content.find(".video-js").length > 0) {
	    		try {
	            	vidAux = videojs(this.$content.find(".video-js").attr("id"), {
			            controlBar: {
			                playToggle: true
			            }
		        	});
		        } catch (e) {
		        	console.log(e.message);
		      	}
            }

            atualizaConteudo(e);

	        //$("body").css({'overflow-y':'hidden'});
	    },                /* Called after open. `this` is the featherlight instance. Gets event as parameter  */
	    afterContent:   function(e){
	    	// URL AMIGAVEL
	    	if(this.$currentTarget.data('url') && this.$currentTarget.data('url') != undefined && this.$currentTarget.data('url') != "") {
	    		window.location = this.$currentTarget.data('url');
	    	}

	    	if(this.$currentTarget.data('caption') && this.$currentTarget.data('caption') != undefined && this.$currentTarget.data('caption') != "") {
		    	//var caption = this.$currentTarget.find('img').attr('alt');
		    	var caption = this.$currentTarget.data('caption');
				this.$instance.find('.caption').remove();
				$('<div class="caption">').text(caption).appendTo(this.$instance.find('.featherlight-content'));
			}
	    }, /* Called after content is ready and has been set. Gets event as parameter, this contains all data */
	    afterClose:     function(e){
	    	if(vidAux) {
	    		vidAux.dispose();
	    	}
	        //$("body").css({'overflow-y':'visible'});
	    }                /* Called after close. `this` is the featherlight instance. Gets event as parameter  */
	};

    if($("a.lightbox-gallery").length > 0) {
    	$("a.lightbox-gallery").featherlightGallery(options);
    }

    if($("a.lightbox").length > 0) {
    	$("a.lightbox").featherlight(options);

	    if($("a.lightbox.toClick").length > 0){
	        $("a.lightbox.toClick").eq(0).trigger("click");
	    }
	}

    // AJAX

    // APENAS UMA VEZ
    if(AJAX_bInicial) {

    	// Ajax Data Load
    	if($(".ajax-data").length > 0) {
    		var loads = $(".ajax-data").length;
    		var iAux = 0;
    		$(".ajax-data").each(function(e) {
    			var elAux = $(this);
				$.ajax({
    				url: elAux.data("url"),
				}).done(function(data) {
					elAux.html(elAux.html() + data);
					elAux.data("url","");

					atualizaConteudo(e);

					iAux++;
					//console.log(iAux.toString() + " -> " + loads.toString());
					if(iAux >= loads) {
						$(".ajax-data").removeClass("ajax-data");
						AJAXCallback(e);
						finishLoad(e);
					}
				});
    		});
    	}

    	//$.address.crawlable(true);
	    $.address.init(function(event) {        
	        // Initializes plugin support for links
	        $("a[rel=\"address\"]").address();
	        $('a[rel=\"address\"]').click(function(){
	            event.preventDefault();
	        });
	    }).change(function(event) {
	    	if(event.path == "" || event.path == "/"){
	        	event.path = "/"
	        }
    		var array_aux = event.path.split("/");

	        verificaAJAX(array_aux);
	    });
	}

    // YOUTUBE VIDEOS
	/* window.onYouTubePlayerAPIReady = function(){
	    if($(".youtube-player").length > 0) {
	    	$(".youtube-player").each(function(e){
	    		if($(this).data("yid").trim() != "") {
		    		var idAux = $(this).attr("id");
		    		var autoplay = 0;
		    		if($(this).hasClass("autoplay")) {
		    			autoplay = 1;
		    		}
					yPlayers[idAux] = new YT.Player(idAux, {
						height: '100%',
						width: '100%',
						videoId: $(this).data("yid"),
						playerVars: { 'autoplay': autoplay, 'controls': 1, 'rel': 0, 'hd': 1 },
						events: {
							'onReady': onPlayerReady,
							'onStateChange': onPlayerStateChange
						}
					});
					//yPlayers[idAux].loadVideoById($(this).data("yid"), 0, "large");
				}
			});
		}
	} */

    //initialize(map_sID_1);
    if($("#" + map_sID_1).length > 0) {
		$(window).load(function(e){
			try {
				initialize(map_sID_1);
			} catch(e) {
				console.log(e.message);
			}
		});
		initialize(map_sID_1);

		var selectMap = document.getElementById(map_sID_1);
		selectMap.addEventListener('touchstart', function(e) {
	    	e.stopPropagation();
		}, false);
		selectMap.addEventListener('mousedown', function(e) {
	    	e.stopPropagation();
		}, false);

		if(isMobile) {
			if (window.innerWidth > window.innerHeight) {
		      	// landscape
		      	if(map_maps[map_sID_1] != undefined) {
		    		map_maps[map_sID_1].set('draggable', false);
		    	}
		    } else {
		      	// portrait
		    	if(map_maps[map_sID_1] != undefined) {
		    		map_maps[map_sID_1].set('draggable', true);
		    	}
		    }
		}
	}

	// FORMS

	// DROPIFY

	if($(".dropify").length > 0) {
		$('.dropify').dropify({
			messages: dropify_messages,
			error: dropify_error
		});
	}

	// SWIPERS 

	if($(".swiper").length > 0) {
		$(".swiper").each(function(e){
			var elAux = $(this);

			//swipers[elAux.attr("id")] = new Swiper('.swiper-container', {
			swipers[elAux.attr("id")] = new Swiper('#' + elAux.attr("id"), {
				lazy: true,
				slideToClickedSlide: true,
				centeredSlides: false,
				slidesPerView: 1,
				spaceBetween: 20,
				slidesPerGroup: 1,
				direction: 'horizontal',
				loop: false,
				loopFillGroupWithBlank: false,
				updateOnWindowResize: true,
				navigation: {
					nextEl: '.swiper-next',
					prevEl: '.swiper-prev',
				},
				pagination: {
					el: '.swiper-nav',
					clickable: true,
				},
				breakpoints: {
					1199: {
					  slidesPerView: 4,
					  slidesPerGroup: 4
					},
					767: {
					  slidesPerView: 2,
					  slidesPerGroup: 2
					}
				  }
		    });
		});
	}

	// FORM PADRAO
	if($(".formPadrao").length > 0) {
		$(".formPadrao").each(function(e) {
			var formAux = $(this);

			var options = { 
			    url: formAux.data("url"),
			    beforeSubmit: function(e) {
			    	if(formAux.hasClass("blocked")) {
			    		event.preventDefault();
			    		event.stopPropagation();
			    		return false;
			    	} else {
			    		formAux.find(".btnEnviar span").html(formAux.find(".btnEnviar").data("label-wait"));
						formAux.addClass("blocked");
					}
			    },
			    success: function(data) { 
			    	formAux.find(".btnEnviar span").html(formAux.find(".btnEnviar").data("label-send"));
					formAux.removeClass("blocked");

			        formAux.find(".retornoPost").html(data);

			        if (data.search('<!--SUCESSO-->') > -1) {
						formAux.find("input").each(function(e){
							if($(this).attr("type") == "button" ||
							   ($(this).attr("type") == "hidden" && !$(this).hasClass("clearable")) ||
							   $(this).attr("type") == "submit")
							{
								// NADA
							}
							else if($(this).attr("type") == "checkbox" ||
							   $(this).attr("type") == "radio")
							{
								//$(this).iCheck('uncheck');
								$(this).val("");
								$(this).removeAttr('checked');
	        					$(this).attr('checked', false);
							} else {
								$(this).val("");
							}
						});

						formAux.find("textarea").val("");

						formAux.find("select").each(function(e){
							$(this).find("option").eq(0).attr('selected','selected');
							$(this).trigger('update.fs');
							if($(this).hasClass("selectric")) {
								$(this).selectric('refresh');
							}
						});

						if(formAux.find(".dropify-clear").length > 0) {
							formAux.find(".dropify-clear").trigger("click");
						}
					}
					
					// Redirect
					if(formAux.find(".url").length > 0) {
						window.location = formAux.find(".url").html();
						formAux.find(".url").remove();
					}
			    } 
			}; 
			formAux.ajaxForm(options);

			formAux.find(".btnEnviar").unbind("click").click(function(e){
				e.preventDefault();
				e.stopPropagation();

		       	formAux.trigger("submit");
		    });
		});
	}

	// INPUTS

	$("input").each(function(e){
		if($(this).data("pattern") || $(this).data("alias")) {
			var maskPattern = $(this).data("pattern");
			var maskAlias = $(this).data("alias");
			if(maskAlias) {
				$(this).inputmask({alias: maskAlias, placeholder: '', showMaskOnHover: false});
			} else if(maskPattern) {
				$(this).inputmask({mask: maskPattern, placeholder: '', showMaskOnHover: false});
			}
		}

		if($(this).attr("type") == "checkbox" ||
		   $(this).attr("type") == "radio") {
			/*$(this).iCheck({
    			checkboxClass: 'icheckbox',
    			radioClass: 'iradio'
    		});*/
		}
	});

	// INVIEW
	if($(".inview").length > 0) {
		//$("body").on("inview", "img[data-src]", function() {
	    $(".inview").unbind("inview").bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
	    	if (isInView) {
		        // element is now visible in the viewport

		        $(this).addClass("isInView");

		        if($(this).hasClass("video-js")) {
	            	var idAux = $(this).attr("id");
	            	if(myPlayers && myPlayers[idAux] != undefined) {
	            		myPlayers[idAux].play();
	            		$(this).find(".vjs-big-play-button").hide();
	            	}
		        }

		        if (visiblePartY == 'top') {
	                // top part of element is visible
	            } else if (visiblePartY == 'bottom') {
	                // bottom part of element is visible
	            } else {
	                // whole part of element is visible
	            }
	        } else {
	            // element has gone out of viewport

	            if($(this).hasClass("video-js")) {
	            	var idAux = $(this).attr("id");
	            	if(myPlayers && myPlayers[idAux] != undefined) {
		            	myPlayers[idAux].pause();
				    	myPlayers[idAux].currentTime(0); // 2 minutes into the video 
				    	$(this).find(".vjs-big-play-button").show();
				    }
	            }

	            $(this).removeClass("isInView");
	        }
	    });
	}

    // SELECTRICS

    if($(".selectric").length > 0) {
    	$(".selectric").selectric({
    		 disableOnMobile: false
    	});
    }
    if($(".selectric-scroll").length > 0) {
   		$(".selectric-scroll").bind( 'mousewheel DOMMouseScroll', function (e) {
    		var e0 = e.originalEvent,
        	delta = e0.wheelDelta || -e0.detail;

    		this.scrollTop += ( delta < 0 ? 1 : -1 ) * 30;
    		e.preventDefault();
    		e.stopPropagation();
		});
	}

    // LOAD
	// Speed
	if(!bFinishLoad) {
    	loadieEl.loadie(1);
		$("#loadie-percent").html("100%");
		finishLoad(e);
	}
	
    //loadieEl.loadie(0.75);
	/* if($("img").length > 0) {
		var iPass = 0.5 / $("img").length;
		var iTotal = 0.5;
		$("img").imagesLoaded().progress(function( instance, image ) {
		    var result = image.isLoaded ? 'loaded' : 'broken';
		    //console.log( 'image is ' + result + ' for ' + image.img.src );
		    iTotal += iPass;
    		loadieEl.loadie(iTotal);
			$("#loadie-percent").html((Math.ceil(iTotal*100)).toString() + "%");
    		//sleep(100);

    		$(instance).removeClass("showOnLoad");
		}).always( function( instance ) {
	    	loadieEl.loadie(1);
			$("#loadie-percent").html("100%");
			//finishLoad(e);
		});
	}
	else {
    	loadieEl.loadie(1);
		$("#loadie-percent").html("100%");
		//finishLoad();
	} */

	$(window).focus(function(e){
		if(!bFinishLoad) {
			finishLoad(e);
		}
	});
}

function atualizaConteudo(e){
	if(atualizaSections){
		atualizaSections(e);
	}

	// SCROLLERS

	if($(".wrapper.withIScroll").length > 0) {
		$(".wrapper.withIScroll").each(function(e){
			$(this).css("width",$(this).parent().innerWidth().toString() + "px");
			$(this).css("height",$(this).parent().innerHeight().toString() + "px");
			
			if($(this).find(".itPag").length > 0) {
				$(this).find(".itPag").css("width",$(this).innerWidth().toString() + "px");
				$(this).find(".itPag").css("height",$(this).innerHeight().toString() + "px");

				/*$(this).find(".itPag").css("height","auto");
				var hMaior = 0;
				$(this).find(".itPag").each(function(e){
					var hAux = $(this).innerHeight();
					if(hAux > hMaior) {
						hMaior = hAux;
					}
				});	
				$(this).find(".itPag").css("height",hMaior.toString() + "px");*/
			}
		});
	}

	// VIDEOS DA HOME
	if($(".banners video").length > 0) {
		var elAux = $(".banners .video video");

		var iTopAux = elAux.closest(".video").height()/2 - elAux.height()/2;
		elAux.css("margin-top",(iTopAux).toString() + "px");
	}

	ajustaScrollers(e);  		

	if(myScrolls != undefined & Object.size(myScrolls) > 0){
		for (var key in myScrolls) {
		    if (key === 'length' || !myScrolls.hasOwnProperty(key)) continue;
		    myScrolls[key].refresh();
		}
	}

	if(lax) {
		lax.updateElements();
	}

	// OUTROS EVENTOS
 
	if(atualizaSections){
		atualizaSections(e);
	}
}

function finishLoad(e) {

	// LAX
	
	if(lax) {
		lax.setup({
			breakpoints: {
				small: 0,
				large: 992
			}
		}); // init

		const updateLax = () => {
			lax.update(window.scrollY);
			window.requestAnimationFrame(updateLax);
		}
	
		window.requestAnimationFrame(updateLax);
	}

	// SCROLLER

	if($('.wrapper.withIScroll').length > 0){
		$('.wrapper.withIScroll').each(function(e){
			var elAux = $(this);
			var arrAux = $(this).attr("id").split("-");
			var idAux = arrAux[1].toString();// + "-" + arrAux[2].toString();
			var sSnap = true;
			if($(this).data("snap") && $(this).data("snap") != "" && $(this).data("snap") != undefined) {
				sSnap = $(this).data("snap");
			}

			myScrolls[idAux] = new IScroll('#wrapper-' + idAux, {
				snap: sSnap,
				momentum: false,
				hScrollbar: false,
				vScrollbar: false,
				hScroll: true,
				vScroll: false,
				scrollX: true,
				scrollY: false,
				wheelAction : 'none',
				eventPassthrough: true,
				preventDefault: false
			});
			
			myScrolls[idAux].on('scrollStart',function(e){
            	//$('#wrapper-' + idAux + ' img').addClass("dragging");
            	//$('#wrapper-' + idAux + ' a').addClass("dragging");
        	});
			
			myScrolls[idAux].on('scrollEnd',function(e){
            	//$('#wrapper-' + idAux + ' img').removeClass("dragging");
            	//$('#wrapper-' + idAux + ' a').removeClass("dragging");
            	
				if($("#nav-" + idAux + " .liNav").length > 0){
					$("#nav-" + idAux + " .liNav").removeClass("ativo");
					$("#nav-" + idAux + " .liNav").eq(this.currentPage.pageX).addClass("ativo");
				}

				if(idAux == "banner") {
					ativaBanner(this.currentPage.pageX, "banner", $("#banner .banners"), myScrolls["banner"]);
				}
				verificaSetas(idAux,this.currentPage.pageX);
			});

			if($("#nav-" + idAux + " .liNav").length > 0){
				$("#nav-" + idAux + " .liNav").removeClass("ativo");
				$("#nav-" + idAux + " .liNav").eq(0).addClass("ativo");
			}
 
			if($("#nav-" + idAux + " .liNav").length > 0){
				if($(".itPag").length > 0){
					if($(".itPag").length == 1){
						$("#nav-" + idAux + "").css("visibility","hidden");
					}

					$("#nav-" + idAux + " .liNav a").unbind("click").click(function(e){
						var iIndexAux = $("#nav-" + idAux + " .liNav").index($(this).parent());

						myScrolls[idAux].goToPage(iIndexAux,0);
					});
				} else {
					$("#nav-" + idAux).hide();
				}
			}

			$(".setas #btnAnt-" + idAux + "").unbind("click").click(function(e){
				myScrolls[idAux].prev();
			});
			$(".setas #btnProx-" + idAux + "").unbind("click").click(function(e){
				myScrolls[idAux].next();
			});

			ajustaScrollers(e);

			verificaSetas(idAux,myScrolls[idAux].currentPage.pageX);

			if(myScrolls[idAux]){
				myScrolls[idAux].refresh();
			}
		});
	}

	// BG PARALLAX

	verificaParallax();

	// FINISH

	atualizaConteudo(e);

	clearTimeout(arrayTOBanners["banner"]);
	arrayTOBanners["banner"] = setTimeout(function(e){
		arrayIndex["banner"] = -1;
		ativaBanner(0, "banner", $("#banner .banners"), myScrolls["banner"]);
	},0);

	loadieEl.loadie(1.0); 
	$("#loadie-percent").html("100%");
			
	hideLoading(true);

	$(".showOnLoad").removeClass("showOnLoad");
	$(".removeOnLoad").remove();

	// AJAX EVENTS

	var hash = window.location.hash; 
	var hash = hash.split("/");
	
	if(hash.length > 0) {
		/*if(hash[1] == "lookbook") {
			if($("#lookbook .it-" + hash[3]).length > 0) {
				var iAux = $("#lookbook .itLB").index($("#lookbook .it-" + hash[3]));
				$("#lookbook .it-" + hash[3]).find("a").trigger("click");
				myScrolls["lookbook"].goToPage(iAux,0);
			}
		}*/
	}
 
	// SCROLLORAMA

	if(scrollController) {
		prepareScrollorama();

		setTimeout(function(e){
			//scrollController.triggerCheckAnim();
			scrollController.update(true);
		},iTimeAuxFade);

		//scrollController.triggerCheckAnim();
		scrollController.update(true);
	}

	bFinishLoad = true;
}

// MOSTRA INFOS DO ISCROLL ITEM ATIVO NA HOME
function ativaBanner(iIndexAux, sIndexIndex, elPai, scrollerAux){
	if(easingBanner == undefined) {
		easingBanner = Quint.easeInOut;
	}

	var bFirst = false;
	if(!sIndexIndex || sIndexIndex == undefined) { 
		sIndexIndex = "banner";
	}
	if(arrayIndex[sIndexIndex] == -1) {
		bFirst = true;
	}
	if(!elPai || elPai == undefined) { 
		elPai = $("#banner .banners");
	}
	if(!scrollerAux || scrollerAux == undefined) {
		scrollerAux = myScrolls["banner"];
	}

	//console.log("ativou banner: " + iIndexAux + " != " + arrayIndex[sIndexIndex]);

	if(iIndexAux != arrayIndex[sIndexIndex]) {
		if(elPai.length > 0) {
			clearTimeout(arrayTOBanners[sIndexIndex]);

			arrayIndex[sIndexIndex] = iIndexAux;

			var bChangeInfo = false;
			
			var bVideo = false;
			var bEmbed = false;
			var bAutoplay = false;

			var bInfo = false;
			var bCinema = false;

			if(iIndexAux != undefined){
				var elAux = elPai.find(".itBanner").eq(iIndexAux);

				var titAux;
				var contAux;
				var linkAux;
				var btnAux;
				var vidAux; 
				var cinemaAux; 
				var descrAux; 
				var tempoDurAux; 
				var tempoTransAux; 

				if(elAux.find(".titAux").length > 0) {
					titAux = elAux.find(".titAux").html();
				}
				if(elAux.find(".imgContAux").length > 0) {
					//contAux = elAux.find(".imgContAux").html();
					if(contAux = elAux.find(".imgContAux").find("img").length > 0) {
						contAux = elAux.find(".imgContAux").find("img").attr("src");
						if(contAux == undefined || contAux == "none" || contAux == "url(\"none\")" || contAux == "url(none)" || contAux == "url(\"undefined\")" || contAux == "url(undefined)") {
							contAux = ROOT_SERVER + ROOT + "_estilo/images/blank.gif";
						}
					}
				}
				if(elAux.find(".cinemaAux").length > 0) {
					if(elAux.find(".cinemaAux").find("img").length > 0) {
						cinemaAux = elAux.find(".cinemaAux").find("img");
						bCinema = true;
					}
				}
				if(elAux.find(".descrAux").length > 0) {
					descrAux = elAux.find(".descrAux").html();
				}
				if(elAux.find(".tempoDurAux").length > 0) {
					tempoDurAux = elAux.find(".tempoDurAux").html();
				}
				if(elAux.find(".tempoTransAux").length > 0) {
					tempoTransAux = elAux.find(".tempoTransAux").html();
				}
				if(elAux.find(".linkAux").length > 0) {
					linkAux = elAux.find(".linkAux").html();
				}
				if(elAux.find(".btnAux").length > 0) {
					btnAux = elAux.find(".btnAux").html();
				}
				if(elAux.find(".vidAux").length > 0) {
					vidAux = elAux.find(".vidAux").html();
				}

				// VERIFICA ATRIBUTOS DO VIDEO
				if(elAux.find(".vidAux").length > 0 && elAux.find(".vidAux").hasClass("embed")){
					bEmbed = true;
				}

				if(elAux.find(".vidAux").length > 0 && elAux.find(".vidAux").hasClass("autoplay")){
					bAutoplay = true;
				}

				// VERIFICA SE TEM TROCA DE CONTEUDO
				if(contAux) {
					if(contAux.trim() != "") {
						bInfo = true;
					} 
				}
				if(descrAux) {
					if(descrAux.trim() != "") {
						bInfo = true;
					} 
				}
				if(titAux) {
					if(titAux.trim() != "") {
						bInfo = true;
					} 
				}

				if((elPai.find(".infos .titulo").length > 0 && titAux.trim() != elPai.find(".infos .titulo").html()) ||
				   (elPai.find(".infos .texto").length > 0 && descrAux.trim() != elPai.find(".infos .texto").html())) {
					bChangeInfo = true;
				}

				// VERIFICA VIDEO
				if(vidAux) {
					if(vidAux.trim() != "") {
						bVideo = true;
						bInfo = false; 
					}
				}
			}

			if(elPai.find(".liNav").length > 0) {
				elPai.find(".liNav").removeClass("ativo");
			}

			var iTimeEaseAux = 0.5;
			var iTimeAux = iTimeEaseAux*1000;
			var iFTimeAux = iTimeAux;
			var iTimeAuxScale = iTimeEaseAux*2;
			if(parseFloat(tempoTransAux) > 0) {
				iTimeEaseAux = tempoTransAux/1000;
				iTimeAux = tempoTransAux;
				iFTimeAux = iTimeAux;
				iTimeAuxScale = (tempoTransAux/1000)*2;
			}
			
			if(bFirst) {
				iFTimeAux = 0;
			}

			var iTimeAuxBanner = 10000;
			if(parseFloat(tempoDurAux) > 0) {
				iTimeAuxBanner = tempoDurAux;
			}

			// ANIMA IMAGEM DE CONTEUDO OUT
			if(elPai.find(".infos .imagem").length > 0) {
				TweenMax.fromTo(elPai.find(".imagem"), iTimeEaseAux,
					{css:{bottom: "0px", opacity: "1"}, ease: easingBanner, immediateRender:true},
					{css:{bottom: "-100%", opacity: "0"}, ease: easingBanner, immediateRender:true});
			}

			// SE INFOS DIFERENTES, ANIMA INFOS OUT
			if(bChangeInfo) { 
				if(elPai.find(".infos .info").length > 0) {
					TweenMax.fromTo(elPai.find(".infos .info"), iTimeEaseAux,
						{css:{right: "10%", opacity: "1"}, ease: easingBanner, immediateRender:true},
						{css:{right: "-15%", opacity: "0"}, ease: easingBanner, immediateRender:true});
				}

				/*if(elPai.find(".infos .titulo").length > 0) {
					TweenMax.fromTo(elPai.find(".infos .titulo"), iTimeEaseAux,
						{css:{top: "0px", opacity: "1"}, ease: easingBanner, immediateRender:true},
						{css:{top: "100px", opacity: "0"}, ease: easingBanner, immediateRender:true});
				}

				if(elPai.find(".infos .texto").length > 0) {
					TweenMax.fromTo(elPai.find(".texto"), iTimeEaseAux,
						{css:{top: "0px", opacity: "1"}, ease: easingBanner, immediateRender:true},
						{css:{top: "150px", opacity: "0"}, ease: easingBanner, immediateRender:true});
				}

				if(elPai.find(".infos .link").length > 0) {
					TweenMax.fromTo(elPai.find(".link").parent(), iTimeEaseAux,
						{css:{top: "0px", opacity: "1"}, ease: easingBanner, immediateRender:true},
						{css:{top: "200px", opacity: "0"}, ease: easingBanner, immediateRender:true});
				}*/
			} else {
				iFTimeAux = 0;
			}

			// ATRASA TROCA PARA DEPOIS DA ANIMACAO OUT
			setTimeout(function(e){

				// TROCA IMAGEM
				if(elPai.find(".infos .imagem").length > 0) {
					elPai.find(".infos .imagem").html("");
				}

				if(elPai.find(".infos .imagem").length > 0) {
					if(contAux != ""){
						//elPai.find(".infos .imagem").html(contAux);
						//elPai.find(".infos .imagem").html("<img src=\"" + contAux + "\" border=\"0\" />");
						try {
							elPai.find(".infos .imagem").css("background-image","url(" + contAux + ")");
						} catch(e) {
							console.log(e.message);
						}
						elPai.find(".infos .imagem").show();
						bExib = true;
					} else {
						elPai.find(".infos .imagem").hide();
					}
				}
				
				// SE INFOS DIFERENTES, TROCA INFOS
				if(bChangeInfo) {
					if(elPai.find(".infos .titulo").length > 0) {
						elPai.find(".infos .titulo").html("");
					}
					if(elPai.find(".infos .texto").length > 0) {
						elPai.find(".infos .texto").html("");
					}
					if(elPai.find(".infos .link").length > 0) {
						elPai.find(".infos .link").html("");
					}
					if(elPai.find(".video").length > 0) {
						elPai.find(".video").html("");
					}

					if(bInfo && iIndexAux != undefined){
						
						var bExib = false;

						if(elPai.find(".infos .titulo").length > 0) {
							if(titAux != ""){
								elPai.find(".infos .titulo").html(titAux);
								elPai.find(".infos .titulo").show();
								bExib = true;
							} else {
								elPai.find(".infos .titulo").hide();
							}
						}

						if(elPai.find(".infos .texto").length > 0) {
							if(descrAux != ""){
								elPai.find(".infos .texto").html(descrAux);
								elPai.find(".infos .texto").show();
								bExib = true;
							} else {
								elPai.find(".infos .texto").hide();
							}
						}
					}
				}

				if(elPai.find(".infos .link").length > 0) {
					if(linkAux != ""){
						elPai.find(".infos .link").html(btnAux);

						//elPai.find(".infos .link").parent().show();
						elPai.find(".infos .link").parent().attr("href",$(linkAux).attr("href"));
						elPai.find(".infos .link").show();
						bExib = true;
					} else {
						elPai.find(".infos .link").html("");

						//elPai.find(".infos .link").parent().hide();
						elPai.find(".infos .link").parent().attr("href","");
						elPai.find(".infos .link").hide();
					}
				}

				// ATUALIZA NAVEGACAO
				if(elPai.find(".liNav").length > 0) {
					elPai.find(".liNav").eq(iIndexAux).addClass("ativo");
				}

				var imgBGAtual = elPai.find(".bg .new").css("background-image");				
				if(imgBGAtual == undefined || imgBGAtual == "none" || imgBGAtual == "url(\"none\")" || imgBGAtual == "url(none)" || imgBGAtual == "url(\"undefined\")" || imgBGAtual == "url(undefined)") {
					imgBGAtual = ROOT_SERVER + ROOT + "_estilo/images/blank.gif";
				}
				var imgBGNova = "url(\"" + elPai.find(".itBanner").eq(iIndexAux).find(".imgAux").eq(0).attr("src") + "\")";
				if(bCinema) {
					imgBGNova = "url(\"" + elPai.find(".itBanner").eq(iIndexAux).find(".cinemaAux").find("img").eq(0).attr("src") + "\")";
				}
				if(imgBGNova == undefined || imgBGNova == "none" || imgBGNova == "url(\"none\")" || imgBGNova == "url(none)" || imgBGNova == "url(\"undefined\")" || imgBGNova == "url(undefined)") {
					imgBGNova = ROOT_SERVER + ROOT + "_estilo/images/blank.gif";
				}

				// TROCA BG POR CSS
				if(elPai.find(".bg").length > 0) {
					try {
						elPai.find(".bg .old").css("background-image",imgBGAtual);
						elPai.find(".bg .old").html("<img src=" + imgBGAtual.replace("url(","").replace(")","") + " border='0' />");
						elPai.find(".bg .new").css("background-image",imgBGNova);
						elPai.find(".bg .new").html("<img src=" + imgBGNova.replace("url(","").replace(")","") + " border='0' />");
					} catch(e) {
						console.log(e.message);
					}
					elPai.find(".bg .old").css("opacity","1");

					// SET OVERLAY COM A COR MAIS PRESENTE
					elPai.find(".overlay").css("opacity",1)
					/*var topColor = elPai.find(".itBanner").eq(iIndexAux).find(".imgAux").eq(0).data("color");
					if(topColor.trim() != "") {
						elPai.find(".overlay").css("color",topColor);
						elPai.find(".overlay").css("opacity",1)
					} else {
						elPai.find(".overlay").css("opacity",0);
						elPai.find(".overlay").css("color","rgba(0,0,0,0)");
					}*/
				}

				atualizaConteudo();

				// SE TEM VIDEO, PREPARA PARA TROCA		
				if(elPai.find(".video").length > 0) {
					if(bVideo) {
						//clearTimeout(arrayTOBanners[sIndexIndex]);
					} else {
						elPai.find(".video").css("opacity","0").html("").hide();
					}
				}

				if(bVideo) {
					elPai.find(".infos").fadeOut(iTimeAux);
				} else {
					elPai.find(".infos").fadeIn(iTimeAux);
				}

				// TROCA BG E APLICA ZOOM
				if(elPai.find(".bg").length > 0) {
					TweenMax.to(elPai.find(".bg .old"), iTimeEaseAux, {delay: 0, css:{opacity: 0}}); //, right:'-100%'
					//TweenMax.to(elPai.find(".bg .new"), 0, {delay: 0, css:{scale: 1}});
					elPai.find(".bg .new").removeClass("zoom");
					TweenMax.fromTo(elPai.find(".bg .new"), iTimeEaseAux, {delay: 0, css:{opacity: 0}},{delay: 0, css:{opacity: 1}}); //, right:'-100%' , right:'0%'
				}
				
				// FAZ DELAY PARA DEPOIS DA TROCA DO BG
				setTimeout(function(e){

					// BACKUP DO BG PARA PROXIMA TROCA
					if(elPai.find(".bg").length > 0) {
						elPai.find(".bg .old").css("background-image",elPai.find(".bg .new").css("background-image"));
					}

					// SE TEM VIDEO PROCESSA
					if(bVideo) {
						if(elPai.find(".video").length > 0) {
							//clearTimeout(arrayTOBanners[sIndexIndex]);

							elPai.find(".video").html(vidAux).show(); 
							if(bEmbed) {
								/*if(bYouTube) {
							    }*/
							}
							else {
								// HTML5 VIDEO

								elPai.find(".video .video-js").attr("id",elPai.find(".video .video-js").attr("id") + "_vid");

							    if(elPai.find(".video .video-js").length > 0) {
							    	elPai.find(".video .video-js").each(function(e){
								    	var idAux = $(this).attr("id");

								    	if(myPlayers[idAux]) {
								    		myPlayers[idAux].dispose();
								    		myPlayers[idAux] = "";
								    	}

								    	try {
									        myPlayers[idAux] = videojs(idAux, {
									            controlBar: {
									                playToggle: true
									            },
									            fluid: true, 
									            aspectRatio: "16:9" 
									        });
									    } catch (e) {
								        	console.log(e.message);
								      	}

								        //elPai.find(".vjs-poster").addClass("fullScreen");
								        //elPai.find(".vjs-poster").addClass("fixed");
								        atualizaConteudo();

								        /*elPai.find(".vjs-poster").unbind("click").click(function(e){
								        	var iCountFix = 0;
								        	var iTimeFix = 1;
								        	clearInterval(iFixVid);
								        	iFixVid = setInterval(function(e){
										        myPlayers[idAux].pause();
										    	myPlayers[idAux].currentTime(0); // 2 minutes into the video            
										        elPai.find(".video .vjs-poster").show();
										        elPai.find(".video .vjs-big-play-button").show();        
											    elPai.find(".btnCloseVid").hide();
											    iCountFix++;

											    if(iCountFix >= 1000/iTimeFix) {
											    	clearInterval(iFixVid);
											    }
								        	},iTimeFix);
								        });
									    
									    elPai.find(".video .vjs-big-play-button").unbind("click").click(function(e){         
										    myPlayers[idAux].pause();
									    	myPlayers[idAux].currentTime(0); // 2 minutes into the video 
								        	elPai.find(".vjs-poster").removeClass("fixed");
											clearInterval(iFixVid);
									    	myPlayers[idAux].play();
									    });*/

								        myPlayers[idAux].on("play", function(){
											clearTimeout(arrayTOBanners[sIndexIndex]);
									        elPai.find(".video .vjs-poster").hide();
									        elPai.find(".video .vjs-big-play-button").hide();   
								        	elPai.find(".btnCloseVid").show();
								        }); 
								        
								        myPlayers[idAux].on("ended", function(){
								        	// PROXIMA TROCA
											clearTimeout(arrayTOBanners[sIndexIndex]);
											arrayTOBanners[sIndexIndex] = setTimeout(function(e){
												if(scrollerAux.currentPage.pageX >= elPai.find(".itBanner").length-1){
													scrollerAux.goToPage(0,0);
												} else {
													scrollerAux.next();
												}
											},0);
								        	elPai.find(".btnCloseVid").hide();
								        }); 

								        if(bAutoplay) {
								        	myPlayers[idAux].play();
								        }

									    elPai.find(".btnCloseVid").unbind("click").click(function(e){
									    	myPlayers[idAux].currentTime(0); // 2 minutes into the video            
									        myPlayers[idAux].pause();
									        elPai.find(".video .vjs-poster").show();
									        elPai.find(".video .vjs-big-play-button").show();        
										    elPai.find(".btnCloseVid").hide();
									    });
								    });
							    }
							}
							elPai.find(".video").css("opacity","1");
						}
					}
					//else 
					{
						// PROXIMA TROCA
						clearTimeout(arrayTOBanners[sIndexIndex]);
						arrayTOBanners[sIndexIndex] = setTimeout(function(e){
							if(scrollerAux.currentPage.pageX >= elPai.find(".itBanner").length-1){
								scrollerAux.goToPage(0,0);
							} else {
								scrollerAux.next();
							}
						},iTimeAuxBanner);
					}

					// ADICIONA ZOOM
					if(elPai.find(".bg").length > 0) {
						//TweenMax.to(elPai.find(".bg div"), iTimeAuxScale,
						//	{delay: 0, css:{scale: 1.1}});
						elPai.find(".bg div").addClass("zoom");
					}

					atualizaConteudo();
				},iTimeAux);
			
				// ATUALIZA NAVEGACAO
				if(elPai.find(".navegacao li").length > 1) {
					elPai.find(".navegacao").show();
				}

				atualizaConteudo();
				
				// SE TEM INFO, MOSTRA
				if(elPai.find(".infos").length > 0 && bInfo) {
					elPai.find(".infos").css("opacity",1);
				}

				// MOSTRA IMAGEM

				if(elPai.find(".infos .imagem").length > 0) {
					TweenMax.fromTo(elPai.find(".infos .imagem"), iTimeEaseAux,
						{css:{bottom: "-100%", opacity: "0"}, ease: easingBanner, immediateRender:true},
						{css:{bottom: "0px", opacity: "1"}, ease: easingBanner, immediateRender:true});
				}
				
				// SE TEM INFO, MOSTRA INFOS		
				if(bChangeInfo) {

					if(elPai.find(".infos .info").length > 0) {
						TweenMax.fromTo(elPai.find(".infos .info"), iTimeEaseAux,
							{css:{right: "15%", opacity: "0"}, ease: easingBanner, immediateRender:true},
							{css:{right: "10%", opacity: "1"}, ease: easingBanner, immediateRender:true});
					}

					/*if(elPai.find(".infos .titulo").length > 0) {
						TweenMax.fromTo(elPai.find(".infos .titulo"), iTimeEaseAux,
							{css:{top: "-100px", opacity: "0"}, ease: easingBanner, immediateRender:true},
							{css:{top: "0px", opacity: "1"}, ease: easingBanner, immediateRender:true});
					}

					if(elPai.find(".infos .texto").length > 0) {
						TweenMax.fromTo(elPai.find(".infos .texto"), iTimeEaseAux,
							{css:{top: "-150px", opacity: "0"}, ease: easingBanner, immediateRender:true},
							{css:{top: "0px", opacity: "1"}, ease: easingBanner, immediateRender:true});
					}

					if(elPai.find(".infos .link").length > 0) {
						TweenMax.fromTo(elPai.find(".infos .link").parent(), iTimeEaseAux,
							{css:{top: "-200px", opacity: "0"}, ease: easingBanner, immediateRender:true},
							{css:{top: "0px", opacity: "1"}, ease: easingBanner, immediateRender:true});
					}*/
				}

				setTimeout(function(e){
					//atualizaConteudo();
				},iTimeAux);
			},iFTimeAux);
		} else {
			clearTimeout(arrayTOBanners[sIndexIndex]);
		}
	}
}

function onPlayerReady(event) {
	//event.target.playVideo();
	//setTimeout(function(e){
	//	event.target.pauseVideo();
	//},2000);*/
}

function onPlayerStateChange(event) {
	if (event.data == YT.PlayerState.ENDED) {
		clearTimeout(arrayTOBanners["banner"]);
		arrayTOBanners["banner"] = setTimeout(function(e){
			if(myScrolls["banner"].currentPage.pageX >= $("#banner .itBanner").length-1){
				myScrolls["banner"].goToPage(0,0);
			} else {
				myScrolls["banner"].next();
			}
		},0);
	}
	if (event.data == YT.PlayerState.PLAYING) {
		ajustaVideo();
		
		atualizaConteudo(event);

		loadieEl.loadie(1.0);
	}
}

function ajustaVideo() {
	// Find all YouTube videos
	var $allVideos = $("iframe[src^='//www.youtube.com']"),

    // The element that is fluid width
    $fluidEl = $("body");

	// Figure out and save aspect ratio for each video
	$allVideos.each(function() {
	  $(this)
	    .data('aspectRatio', this.height / this.width)

	    // and remove the hard coded width/height
	    .removeAttr('height')
	    .removeAttr('width');

	});

	// When the window is resized
	$(window).resize(function() {

	  var newWidth = $fluidEl.width();

	  // Resize all videos according to their own aspect ratio
	  $allVideos.each(function() {

	    var $el = $(this);
	    $el
	      .width(newWidth)
	      .height(newWidth * $el.data('aspectRatio'));

	  });

	// Kick off one resize to fix all videos on page load
	}).resize();
}


function initialize(sIndexAux) {
	var geoAux = map_geoPadrao[sIndexAux];
	if(geoAux == "") {
		geoAux = "-29.154908,-51.1887077";
	}
	var zoomAux = map_zoomPadrao[sIndexAux];
	if(zoomAux == "") {
		zoomAux = 15;
	}
	var arrAux = geoAux.split(",");

	var latIni = parseFloat(arrAux[0]);
	var lngIni = parseFloat(arrAux[1]);

	$("#" + sIndexAux).html("");

	var latlng = new google.maps.LatLng(latIni + 0.0, lngIni - 0.0);

	var mapOptions = {
	 	zoom: zoomAux,
    	center: latlng,
    	mapTypeId: google.maps.MapTypeId.ROADMAP,
    	styles: MAP_style_array,
		scrollwheel: false,
        disableDoubleClickZoom: false,
    	draggable: !("ontouchend" in document)
	};

    map_geocoder[sIndexAux] = new google.maps.Geocoder();

	map_maps[sIndexAux] = new google.maps.Map(document.getElementById(sIndexAux),mapOptions);

	setMarkers(map_maps[sIndexAux], map_pins[sIndexAux], sIndexAux);

	if(map_ib[sIndexAux] != undefined) {
		google.maps.event.addListener(map_maps[sIndexAux], "click", function() { map_ib[sIndexAux].close() });
	}

	map_bMapLoaded[sIndexAux] = true;
		
	google.maps.event.trigger(map_maps[sIndexAux], "resize");
}

function createMarker(site, map, sIndexAux){
    var siteLatLng = new google.maps.LatLng(site[1], site[2]);

    var marker = new google.maps.Marker({
        position: siteLatLng,
        map: map,
        title: site[0],
        zIndex: site[3],
        html: site[4]
        ,icon: site[5] //zoomIcons[map.getZoom()]
        ,iconAtivo: site[6]
        ,iconOff: site[6]
        ,paises: site[7]
        ,estados: site[8]
        ,cidades: site[9]
    });

    map_allMarkers[sIndexAux].push(marker);

    // Begin example code to get custom infobox
    var boxText = document.createElement("div");
    boxText.className = "infobox";
    //boxText.style.cssText = "border: 1px solid black; margin-top: 8px; background: yellow; padding: 5px;";
    boxText.innerHTML = marker.html;

    var myOptions = {
         content: boxText
        ,disableAutoPan: false
        ,maxWidth: 0
        ,pixelOffset: new google.maps.Size(0, 0)
        ,zIndex: null
        ,closeBoxMargin: "0px"
        //,closeBoxURL: ROOT_SERVER + ROOT + "_estilo/images/fechar.png"
        ,closeBoxURL: ""
        ,infoBoxClearance: new google.maps.Size(1, 1)
        ,isHidden: false
        ,pane: "floatPane"
        ,enableEventPropagation: false
    };
    // end example code for custom infobox

    google.maps.event.addListener(marker, "click", function (e) {
		if(map_ib[sIndexAux] != undefined) {
			map_ib[sIndexAux].close();
			map_ib[sIndexAux].setOptions(myOptions);
			map_ib[sIndexAux].open(map, this);
		}

		marker.map.setCenter(marker.getPosition());
    });

	if(map_ib[sIndexAux] != undefined) {
		google.maps.event.addListener(map_ib[sIndexAux], 'domready', function() {
			//do something
			var hAux = $("#" + sIndexAux + " .box").height() + 90;
			$("#" + sIndexAux + " .box").css("margin-top","-" + (hAux/2).toString() + "px");
		});

		google.maps.event.addListener(map, "click", function (e) {
			map_ib[sIndexAux].close();
		});

		google.maps.event.addListener(map, "drag", function (e) {
			map_ib[sIndexAux].close();
		});
	}

    /*google.maps.event.addListener(map, 'zoom_changed', function() {
        marker.setIcon(zoomIcons[map.getZoom()]);
    });*/

    return marker;
}

function setMarkers(map, markers, sIndexAux) {
	for (var i = 0; i < markers.length; i++) {
		createMarker(markers[i], map, sIndexAux);
	}
}

function doMarker(markers, i, map, siteLatLng, sIndexAux) {
	var site = markers[i];

	var marker = new MarkerWithLabel({
        position: siteLatLng,
        map: map,
        title: site[0],
        zIndex: site[5],
        html: site[1],
        draggable: false,
        icon: ROOT_SERVER + ROOT + "img/pin.png"
	    //,labelAnchor: new google.maps.Point(20,30)
	    //,labelClass: "mapLabel" // the CSS class for the label
        //,labelContent: '<span class="pin"></span>' 
        //,refIcon: site[5] //zoomIcons[map.getZoom()]
    });

    if(i == 0) {
    	map.setCenter(siteLatLng);
    	map.setZoom(13);
	}

	map_allMarkers[sIndexAux].push(marker);

    // Begin example code to get custom infobox
    var boxText = document.createElement("div");
    boxText.className = "infoBox";
    //boxText.style.cssText = "border: 1px solid black; margin-top: 0px; background: black; padding: 5px; color: #fff";
    boxText.innerHTML = marker.html;

    var myOptions = {
         content: boxText
        ,disableAutoPan: false
        ,maxWidth: 0
        ,pixelOffset: new google.maps.Size(0, 0)
        ,zIndex: null
        ,closeBoxMargin: "0px"
        //,closeBoxURL: ROOT_SERVER + ROOT + "_estilo/images/fechar.png"
        ,closeBoxURL: ""
        ,infoBoxClearance: new google.maps.Size(1, 1)
        ,isHidden: false
        ,pane: "floatPane"
        ,enableEventPropagation: false
    };
	// end example code for custom infobox
	
	if(map_ib[sIndexAux] != undefined) {
		google.maps.event.addListener(marker, "mouseover", function (e) {
			map_ib[sIndexAux].close();
			map_ib[sIndexAux].setOptions(myOptions);
			map_ib[sIndexAux].open(map, this);
		});

		google.maps.event.addListener(marker, "mouseout", function (e) {
			map_ib[sIndexAux].close();
		});
	}

    return marker;
}

verificaRolagem = function(e) {
	
	/*let holderMenu = $(".topo");
	if(!holderMenu.hasClass("static")) {
		if(window.scrollY > holderMenu.height()){
			holderMenu.addClass("fixed");
			$("body").addClass("fixed");
		}
		if(window.scrollY < holderMenu.height()){
			holderMenu.removeClass("fixed");
			$("body").removeClass("fixed");
		}
	} */

	verificaParallax();

	if(window.scrollY > 1) {
		$(".topo").addClass("fixed");
		$("body").addClass("fixed");
	} else {
		$(".topo").removeClass("fixed");
		$("body").removeClass("fixed");
	}

	/*var elAll;
	var sCor = "#000";

	if($(".inview").length > 0) {
		elAll = $(".inview");
	}
	
	if(elAll != undefined && elAll.length > 0) {
		for(var i=0; i<elAll.length; i++) {
			var iTopRef = $(window).scrollTop();
			var iTopSec = elAll.eq(i).offset().top;
			
			var offsetAux = $(".logo").position().top;

			var iDiff = (iTopSec - iTopRef); //position of the ele w.r.t window

			//console.log(iTopRef.toString() + ", " + iTopSec.toString());
			//console.log(iDiff.toString());

			if(iDiff <= offsetAux) {
				var colorAux = elAll.eq(i).data("hover-color");
				if(colorAux == "1") {
					sCor = "#000";
				} else if(colorAux == "2") {
					sCor = "#fff";
				}

				//console.log(elAll.eq(i).attr("id"));
			}
		}
	}

	$(".logo a").css("color",sCor);
	if($(".menu").hasClass("opened")) {
		$(".burger-menu-piece").css("border-color","inherit");
	} else {
		$(".burger-menu-piece").css("border-color",sCor);
	}*/
}

function prepareScrollorama() {
	var bFromTo = false;
	if(window.innerWidth > 990 && !bScrollorama)
	{
		bFromTo = false;
	}

	bScrollorama = true;

	// BUILD CARDS
	/*if($("section#sobre .card").length > 0) {
		var iDelayAux = 0;
		$("section#sobre .card").each(function(e){
			var elAux = $(this); 

			var tween = TweenMax.fromTo(elAux, 1, 
				{css: {opacity: 0, marginTop: "100px", marginLeft: "0px"}, delay: 0, immediateRender: true},
				{css: {opacity: 1, marginTop: "-100px", marginLeft: "0px"}, delay: iDelayAux, ease: Linear.easeNone, immediateRender: true});
 
			var scene = new ScrollMagic.Scene({triggerElement: elAux, triggerHook: 'onEnter', offset: 0, duration: 1000})
				.setTween(tween)
				.addTo(scrollController);

			iDelayAux += 0.5;
		});
	} */
}

function verificaAJAX(hashes) {
	let AJAX_modAux = hashes[0];
    let AJAX_tipoAux = hashes[1];
    let AJAX_titAux = hashes[2];
    let AJAX_idAux = hashes[3];

    let locAux = window.location.toString().split("/");

	gtagAJAX();

    if(!AJAX_bSkip) {
    	if(AJAX_tipoAux != "" && AJAX_tipoAux != undefined){
    		if(AJAX_tipoAux != "menu" && $(".menu").hasClass("open")) {
		        $(".menu").removeClass("open");
		        $(".geral").removeClass("inMenu");
		        $(".openMenu").fadeIn(300);
			}
			
			/* if(AJAX_tipoAux == "orcamento") {
				// Start new fancybox instance
				$.fancybox.open([
					{
						src  : ROOT_SERVER + ROOT + "ajax/" + AJAX_tipoAux + "/",
						type : 'ajax',
						opts : {
							baseClass: "fancybox-case",
							arrows: false,
							infobar: false,
							toolbar: false,
							touch: false,
							afterLoad: function( instance, current ) {
								AJAXCallback(event, AJAX_tipoAux, AJAX_titAux, AJAX_idAux);
								finishLoad(event);
							},
							beforeClose : function( instance, current, e ) {
								window.location = "#/close/";
							}
						}
					}
				]);
			} else */ {
				rolaPagina(AJAX_tipoAux);
			}
        }

		AJAX_bInicial = false;
    }
    AJAX_bSkip = false;
}

// Custom Project

AOS.init({
	duration: 800,
	easing: 'slide',
	once: true
});

jQuery(document).ready(function($) {

	"use strict";

	if($("#lightgallery").length > 0){
		$("#lightgallery").lightGallery({
			"selector": ".img-gal-item a",
			//"controls": false,
			"thumbnail": false,
		}); 
	}

	var siteMenuClone = function() {

		$('.js-clone-nav').each(function() {
			var $this = $(this);
			$this.clone().attr('class', 'site-nav-wrap').appendTo('.site-mobile-menu-body');
		});


		setTimeout(function() {
			
			var counter = 0;
			$('.site-mobile-menu .has-children').each(function(){
				var $this = $(this);
				
				$this.prepend('<span class="arrow-collapse collapsed">');

				$this.find('.arrow-collapse').attr({
				'data-toggle' : 'collapse',
				'data-target' : '#collapseItem' + counter,
				});

				$this.find('> ul').attr({
				'class' : 'collapse',
				'id' : 'collapseItem' + counter,
				});

				counter++;

			});

		}, 1000);

		$('body').on('click', '.arrow-collapse', function(e) {
			var $this = $(this);
			if ( $this.closest('li').find('.collapse').hasClass('show') ) {
				$this.removeClass('active');
			} else {
				$this.addClass('active');
			}
			e.preventDefault();  	
		});

		$(window).resize(function() {
			var $this = $(this),
				w = $this.width();

			if ( w > 768 ) {
				if ( $('body').hasClass('offcanvas-menu') ) {
					$('body').removeClass('offcanvas-menu');
				}
			}
		});

		$('body').on('click', '.js-menu-toggle', function(e) {
			var $this = $(this);
			e.preventDefault();

			if ( $('body').hasClass('offcanvas-menu') ) {
				$('body').removeClass('offcanvas-menu');
				$this.removeClass('active');
			} else {
				$('body').addClass('offcanvas-menu');
				$this.addClass('active');
			}
		}) 

		// click outisde offcanvas
		$(document).mouseup(function(e) {
			var container = $(".site-mobile-menu");
			if (!container.is(e.target) && container.has(e.target).length === 0) {
			if ( $('body').hasClass('offcanvas-menu') ) {
					$('body').removeClass('offcanvas-menu');
				}
			}
		});
	}; 
	siteMenuClone();

	var sitePlusMinus = function() {
		$('.js-btn-minus').on('click', function(e){
			e.preventDefault();
			if ( $(this).closest('.input-group').find('.form-control').val() != 0  ) {
				$(this).closest('.input-group').find('.form-control').val(parseInt($(this).closest('.input-group').find('.form-control').val()) - 1);
			} else {
				$(this).closest('.input-group').find('.form-control').val(parseInt(0));
			}
		});
		$('.js-btn-plus').on('click', function(e){
			e.preventDefault();
			$(this).closest('.input-group').find('.form-control').val(parseInt($(this).closest('.input-group').find('.form-control').val()) + 1);
		});
	};
	// sitePlusMinus();

	var siteSliderRange = function() {
    $( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: 500,
      values: [ 75, 300 ],
      slide: function( event, ui ) {
        $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
      }
    });
    $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
      " - $" + $( "#slider-range" ).slider( "values", 1 ) );
	};
	// siteSliderRange();

	var siteCarousel = function () {
		if ( $('.nonloop-block-13').length > 0 ) {
			$('.nonloop-block-13').owlCarousel({
				center: false,
				items: 1,
				loop: true,
					stagePadding: 0,
				margin: 0,
				autoplay: true,
				nav: true,
					navText: ['<span class="icon-arrow_back">', '<span class="icon-arrow_forward">'],
				responsive:{
					600:{
						margin: 0,
						nav: true,
						items: 2
					},
					1000:{
						margin: 0,
						stagePadding: 0,
						nav: true,
						items: 3
					},
					1200:{
						margin: 0,
						stagePadding: 0,
						nav: true,
						items: 4
					}
				}
			});
		}

		$('.slide-one-item').owlCarousel({
			center: false,
			items: 1,
			loop: true,
				stagePadding: 0,
			margin: 0,
			autoplay: true,
			pauseOnHover: false,
			nav: true,
			navText: ['<span class="icon-keyboard_arrow_left">', '<span class="icon-keyboard_arrow_right">']
		});

		$('.slide-one-item-alt').owlCarousel({
			center: false,
			items: 1,
			loop: true,
				stagePadding: 0,
				smartSpeed: 700,
			margin: 0,
			autoplay: true,
			pauseOnHover: false,
		});

	  	$('.custom-next').click(function(e) {
		  	e.preventDefault();
	  		$('.slide-one-item-alt').trigger('next.owl.carousel');
	  	});
	  	$('.custom-prev').click(function(e) {
		  	e.preventDefault();
	  		$('.slide-one-item-alt').trigger('prev.owl.carousel');
	  	});	  
	};
	siteCarousel();

	var siteStellar = function() {
		$(window).stellar({
			responsive: false,
			parallaxBackgrounds: true,
			parallaxElements: true,
			horizontalScrolling: false,
			hideDistantElements: false,
			scrollProperty: 'scroll'
		});
	};
	siteStellar();

	var siteCountDown = function() {

		$('#date-countdown').countdown('2020/10/10', function(event) {
		  var $this = $(this).html(event.strftime(''
		    + '<span class="countdown-block"><span class="label">%w</span> weeks </span>'
		    + '<span class="countdown-block"><span class="label">%d</span> days </span>'
		    + '<span class="countdown-block"><span class="label">%H</span> hr </span>'
		    + '<span class="countdown-block"><span class="label">%M</span> min </span>'
		    + '<span class="countdown-block"><span class="label">%S</span> sec</span>'));
		});		
	};
	siteCountDown();

	var siteDatePicker = function() {
		if ( $('.datepicker').length > 0 ) {
			$('.datepicker').datepicker();
		}
	};
	siteDatePicker();

	var siteSticky = function() {
		$(".js-sticky-header").sticky({topSpacing:0});
	};
	siteSticky();

	// navigation
  	var OnePageNavigation = function() {
		var navToggler = $('.site-menu-toggle');
		$("body").on("click", ".main-menu li a[href^='#'], .smoothscroll[href^='#'], .site-mobile-menu .site-nav-wrap li a", function(e) {
			e.preventDefault();

			var hash = this.hash;

			$('html, body').animate({
				'scrollTop': $(hash).offset().top
			}, 600, 'easeInOutCirc', function(){
				window.location.hash = hash;
			});
		});
	};
	//OnePageNavigation();

	var siteScroll = function() {
		$(window).scroll(function() {
			var st = $(this).scrollTop();

			if (st > 100) {
				$('.js-sticky-header').addClass('shrink');
			} else {
				$('.js-sticky-header').removeClass('shrink');
			}
		});
	};
	siteScroll();

});