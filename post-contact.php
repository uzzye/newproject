<?php 
include_once("config.php");

carrega_classe("contatos");

//$arrayAux = explode(".",$_FILES["arquivo"]["name"]);
//$extAux = $arrayAux[sizeof($arrayAux)-1];

if(trim($_REQUEST["verifier"]) != "") {
	$msg_erro = "<span class=\"erro\">" . get_lang("_FORM_RETURN_ERRO_1") . "</span>";
}
else if($_REQUEST["acao"] == "enviar")
{
	//trim($_FILES["arquivo"]["name"]) == "" ||
	if(trim($_REQUEST["nome"]) == "" ||
	   trim($_REQUEST["email"]) == "" ||
	   trim($_REQUEST["telefone"]) == "" ||
	   //trim($_REQUEST["cidade"]) == "" ||
	   //trim($_REQUEST["estado"]) == "" ||
	   //trim($_REQUEST["assunto"]) == "" ||
	   trim($_REQUEST["mensagem"]) == "")
	{
		$ERRO = 1;
		$msg_erro = "<span class=\"erro\">" . get_lang("_FORM_RETURN_ERRO_2") . "</span>";
	}
	else if(!valida_email($_REQUEST["email"]))
	{
		$ERRO = 2;
		$msg_erro = "<span class=\"erro\">" . get_lang("_FORM_RETURN_ERRO_4") . "</span>";
	}
	//else if ( $_FILES["arquivo"]["tmp_name"] == NULL || $_FILES["arquivo"]["error"]!==0 || !preg_match("/(PDF|DOC|DOCX|RTF|TXT)/", strtoupper($extAux)) )
	/*else if ( trim($_FILES["arquivo"]["name"]) != "" && ($_FILES["arquivo"]["error"]!==0 || !preg_match("/(PDF|DOC|DOCX|RTF|TXT)/", strtoupper($extAux)) ))
	{
		$ERRO = 3;
		$msg_erro = "<span class=\"erro\">" . get_lang("_FORM_RETURN_ERRO_5") . "</span>";
	}*/
	else
	{	
		$ERRO = 0;
			
		$ip = get_ip();
		
		$tituloAux = "Novo Contato em " . $_CONFIG["titulo"];
		
		$contatos = new contatos();
		$contatos->inicia_dados();
		$contatos->set_var("nome",output_decode($_REQUEST["nome"]));		
		$contatos->set_var("email",output_decode($_REQUEST["email"]));	
		$contatos->set_var("telefone",output_decode($_REQUEST["telefone"]));
		//$contatos->set_var("cidade",output_decode($_REQUEST["cidade"]));
		//$contatos->set_var("estado",output_decode($_REQUEST["estado"]));
		//$contatos->set_var("assunto",output_decode($_REQUEST["assunto"]));
		$contatos->set_var("mensagem",output_decode($_REQUEST["mensagem"]));
		$contatos->set_var("ip",output_decode($ip));
		$contatos->set_var("ativo",1);
		$contatos->set_var("usuario_criacao",99);
		$id_aux = $contatos->inclui(false,true);

		/*if(trim($_FILES["arquivo"]["name"]) != "") {
			$newName = "";
			$arrayAux = explode(".",$_FILES["arquivo"]["name"]);
			$newName = "contatos_" . $id_aux . "_arquivo." . $arrayAux[sizeof($arrayAux)-1];
			move_uploaded_file($_FILES["arquivo"]["tmp_name"], "upload/contatos/".$newName);

			$contatos = new contatos();
			$contatos->inicia_dados();
			$contatos->set_var("id",$id_aux);
			$contatos->carrega_dados();
			$contatos->set_var("arquivo",$newName);
			$contatos->edita(false);
		}*/
		
		$strMensagem = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
		<html xmlns=\"http://www.w3.org/1999/xhtml\" style=\"background: #fff!important;\">
			<head>
				<title>" . $_CONFIG["titulo"] . "</title>
				<!-- CSS -->
				<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
				<link rel=\"stylesheet\" type=\"text/css\" href=\"" . ROOT_SERVER . ROOT . "css/geral.css\" />
				<link rel=\"stylesheet\" type=\"text/css\" href=\"" . ROOT_SERVER . ROOT . "css/helpers.css\" />
				<link rel=\"stylesheet\" type=\"text/css\" href=\"" . ROOT_SERVER . ROOT . "css/tipografia.css\" />
				<style>
					a {color: #000!important;}
				</style>
			</head>
			<body style=\"color: #000!important; background: #fff!important;\">
			";

		$strMensagem .= "<img src=\"" . ROOT_SERVER . ROOT . "img/logo_email.png\" border=\"0\"><br />";
		$strMensagem .= "<br />";
		$strMensagem .= "Um novo contato foi realizado em " . $_CONFIG["titulo"] . ". Para acessar o CMS, clique <a href=\"" . ROOT_SERVER . ROOT . ROOT_CMS . "\" target=\"_blank\">aqui</a>.<br />";
		$strMensagem .= "<br />";
		//$strMensagem .= "<b>Assunto:</b> ". stripslashes($_REQUEST["assunto"]) ."<br>";
		$strMensagem .= "<b>Nome:</b> ". stripslashes($_REQUEST["nome"]) ."<br>";
		$strMensagem .= "<b>E-mail:</b> ". stripslashes($_REQUEST["email"]) ."<br>";
		$strMensagem .= "<b>Telefone:</b> ". stripslashes($_REQUEST["telefone"]) ."<br>";
		//$strMensagem .= "<b>Cidade:</b> ". stripslashes($_REQUEST["cidade"]) ."<br>";
		//$strMensagem .= "<b>Estado:</b> ". stripslashes($_REQUEST["estado"]) ."<br>";
		/*if(trim($_REQUEST["referencia"]) != ""){
			$strMensagem.= "<b>Referência:</b> ". $_REQUEST["referencia"] ."<br>";
			$tituloAux = $_REQUEST["assunto"] . " - " . $_REQUEST["referencia"];
		}*/
		$strMensagem .= "<b>Mensagem:</b> ". nl2br(stripslashes($_REQUEST["mensagem"])) ."<br>"; 
		/*if(trim($_FILES["arquivo"]["name"]) != "") {
			$strMensagem .= "<br/>";
			$strMensagem .= "<b>Arquivo:</b> <a href=\"" . ROOT_SERVER . ROOT . "download/contatos/" . $id_aux . "/arquivo/nome\" target=\"_blank\">". $newName ."</a><br>";
			//$strMensagem .= "<br/>";
		}*/
		$strMensagem .= "<br>";
		$strMensagem .= "<b>IP:</b> ". $ip ."<br>";
		$strMensagem .= "<b>Data de Envio:</b> ". date("d/m/Y H:i:s") ."<br>";		
				
		$de = EMAIL_DEFAULT_FROM;
		$para = EMAIL_PADRAO_CONTATO;

		/*if($_REQUEST["assunto"] == "RH") {
			// MUDA O $para
		}
		else if($_REQUEST["assunto"] == "Dúvidas, reclamações e sugestões") {
		}
		else if($_REQUEST["assunto"] == "Cotações") {
		}
		else if($_REQUEST["assunto"] == "Fale com o Diretor") {
		}
		else if($_REQUEST["assunto"] == "Financeiro") {
		}*/
		//sendMail($de, $para, $tituloAux, $strMensagem);
		sendMailSMTP($de, $para, $tituloAux, $strMensagem);

		$msg_erro = "<!--SUCESSO-->" . get_lang("_FORM_RETURN_OK_2") . "";
		//$msg_erro .= "<br/><br/>" . $strMensagem;
	}
	echo $msg_erro;
}
?>