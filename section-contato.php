<?php

include_once("config.php");

?>
<section class="site-section bg-light" id="contato">
  <div class="anchor contato" data-tpad="0"></div>
  <div class="container">
    <div class="row mb-5">
      <div class="col-12 text-center">
        <h3 class="section-sub-title"><?php echo get_lang("_FALE_CONOSCO"); ?></h3>
        <h2 class="section-title mb-3 text-white"><?php echo get_lang("_CONTATO"); ?></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-7 mb-5">

        <form class="p-5 bg-white formPadrao" id="form-contato" method="post" action="javascript:return false;" data-url="<?php echo ROOT_SERVER . ROOT; ?>ajax/contato/enviar">

          <input type="hidden" name="acao" id="acao_contato" value="enviar" />
      	  <input type="hidden" name="verifier" id="verifier_contato" value="" />
          
          <?php /*<h2 class="h4 text-black mb-5">Contact Form</h2>*/ ?>

          <?php /*<div class="row form-group">
            <div class="col-md-6 mb-3 mb-md-0">
              <label class="text-black" for="fname">First Name</label>
              <input type="text" id="fname" class="form-control">
            </div>
            <div class="col-md-6">
              <label class="text-black" for="lname">Last Name</label>
              <input type="text" id="lname" class="form-control">
            </div>
          </div>*/ ?>

          <div class="row form-group">
            
            <div class="col-md-12">
              <?php /*<label class="text-black" for="nome"><?php echo get_lang("_NOME"); ?></label>*/ ?>
              <input type="nome" id="nome" class="form-control" placeholder="<?php echo get_lang("_NOME"); ?>">
            </div>
          </div>

          <div class="row form-group">
            
            <div class="col-md-12">
              <?php /*<label class="text-black" for="email"><?php echo get_lang("_EMAIL"); ?></label>*/ ?>
              <input type="email" id="email" class="form-control" placeholder="<?php echo get_lang("_EMAIL"); ?>">
            </div>
          </div>

          <div class="row form-group">
            
            <div class="col-md-12">
              <?php /*<label class="text-black" for="telefone"><?php echo get_lang("_TELEFONE"); ?></label>*/ ?>
              <input type="telefone" id="telefone" class="form-control" placeholder="<?php echo get_lang("_TELEFONE"); ?>">
            </div>
          </div>

          <div class="row form-group">
            <div class="col-md-12">
              <?php /*<label class="text-black" for="mensagem"><?php echo get_lang("_MENSAGEM"); ?></label>*/ ?>
              <textarea name="mensagem" id="mensagem" cols="30" rows="7" class="form-control" placeholder="<?php echo get_lang("_MENSAGEM"); ?>"></textarea>
            </div>
          </div>

          <div class="row form-group">
            <div class="col-md-12">
              <button type="submit" data-label-send="<?php echo get_lang("_ENVIAR"); ?>" data-label-wait="<?php echo get_lang("_AGUARDE"); ?>" class="btn btn-primary btn-md btnEnviar"><span><?php echo get_lang("_ENVIAR"); ?></span></button>
            </div>
          </div>

          <div class="row form-group">
            <div class="col-md-12 retornoPost">
            </div>
          </div>


        </form>
      </div>
      <div class="col-md-5">
        
        <div class="p-4 mb-3 bg-white">
          <p class="mb-0 font-weight-bold"><?php echo get_lang("_ENDERECO"); ?></p>
          <p class="mb-4"><?php echo $arrayConteudos["endereco"]["descricao"]; ?></p>

          <p class="mb-0 font-weight-bold"><?php echo get_lang("_TELEFONE"); ?></p>
          <p class="mb-4"><a href="tel:<?php str_replace(" ","",$arrayConteudos["telefone"]["descricao"]); ?>"><?php echo $arrayConteudos["telefone"]["descricao"]; ?></a></p>

          <p class="mb-0 font-weight-bold"><?php echo get_lang("_EMAIL"); ?></p>
          <p class="mb-0"><a href="mailto:<?php str_replace(" ","",$arrayConteudos["email"]["descricao"]); ?>"><?php echo $arrayConteudos["email"]["descricao"]; ?></a></p>

        </div>
        
      </div>
    </div>
  </div>
</section>

<?php 

?>